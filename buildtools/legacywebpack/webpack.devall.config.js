const path = require("path")
const webpack = require("webpack")
const ExtractTextPlugin = require("extract-text-webpack-plugin")
const HtmlWebpackPlugin = require("html-webpack-plugin") // installed via npm
const CleanWebpackPlugin = require("clean-webpack-plugin")
const BrowserSyncPlugin = require("browser-sync-webpack-plugin")
const nodeExternals = require("webpack-node-externals")
// const StartServerPlugin = require("start-server-webpack-plugin")
const NodemonPlugin = require( "nodemon-webpack-plugin" )

// Clean and Regenerate Files

const filesToClean = ["dist"]
const buildEnv = process.env.NODE_ENV || "development"

const clientConfig = {
  mode: "development",
  devtool: "source-map", // any "source-map"-like devtool is possible
  node: {
    fs: "empty"
  },
  resolve: {
    alias: {
      AppComp: path.resolve(__dirname, "client/src/app/FunctionalComponents"),
      Global: path.resolve(__dirname, "client/src/app/GlobalComponents"),
      GlobalUtils: path.resolve(__dirname, "client/src/globalutilities"),
      AppState: path.resolve(__dirname, "client/src/app/StateManagement"),
      Auth: path.resolve(__dirname, "client/src/app/LoginAuthentication"),

      Validation : path.resolve(__dirname,"client/src/app/Validations"),
      TranRFP: path.resolve(__dirname, "client/src/app/FunctionalComponents/TransactionRFP"),
      TranDeals: path.resolve(__dirname, "client/src/app/FunctionalComponents/TransactionDeals"),
      Task: path.resolve(__dirname, "client/src/app/FunctionalComponents/TaskManagement"),
      CRM: path.resolve(__dirname, "client/src/app/FunctionalComponents/Relationships"),
      Platform: path.resolve(__dirname, "client/src/app/FunctionalComponents/PlatformManagement"),
      Tenant: path.resolve(__dirname, "client/src/app/FunctionalComponents/TenantConfiguration"),
      Admin: path.resolve(__dirname,"client/src/app/FunctionalComponents/AdminManagement")
    }
  },
  entry: [
    "babel-polyfill",
    path.resolve(__dirname, "client/src/index.js"),
    path.resolve(__dirname, "client/src/scss/main.scss")
  ],
  output: {
    path: path.resolve(__dirname, "dist"),
    publicPath: "/"
  },
  devServer: {
    contentBase: path.join(__dirname, "dist"),
    compress: true,
    open: true,
    port: 3101,
    hot: true,
    watchContentBase: true,
    historyApiFallback: true
  },
  module: {
    rules: [
      {
        test: /\.(js)$/,
        exclude: /node_modules/,
        use: ["babel-loader"]
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: ["css-loader"]
        })
      },
      {
        test: /\.(sass|scss)$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: ["css-loader", "sass-loader"]
        })
      },
      {
        test: /\.(ttf|eot|woff|woff2|svg|otf)?(\?v=\d+\.\d+\.\d+)?$/,
        loader: "file-loader",
        options: {
          name: "fonts/[name].[ext]"
        }
      },
      {
        test: /\.(jpg|png|svg|gif)$/,
        use: {
          loader: "file-loader",
          options: {
            name: "images/[name].[ext]"
          }
        }
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./client/src/public/index.html",
      inject: true
    }),
    //    new webpack.HotModuleReplacementPlugin(),
    new ExtractTextPlugin({
      filename: "bundled.css",
      disable: false,
      allChunks: true
    }),
    new CleanWebpackPlugin(filesToClean),
    new webpack.HotModuleReplacementPlugin(),
    new BrowserSyncPlugin({
      host: "localhost",
      port: 3001,
      server: { baseDir: ["dist"] }
    })
  ]
}

const serverConfig = {
  entry: ["./server/serversrc/index"],
  watch: true,
  devtool: "sourcemap",
  mode:buildEnv,
  target: "node",
  node: {
    __filename: true,
    __dirname: true
  },
  externals: [nodeExternals({ whitelist: ["webpack/hot/poll?1000"] })],
  module: {
    rules: [
      {
        test: /\.js?$/,
        use: [
          {
            loader: "babel-loader",
            options: {
              babelrc: false,
              presets: [["env", { modules: false }], "stage-0"],
              plugins: ["transform-regenerator", "transform-runtime"]
            }
          }
        ],
        exclude: /node_modules/
      },
      {
        test: /\.(graphql|gql)$/,
        exclude: /node_modules/,
        use: {
          loader: "raw-loader"
        }
      }
    ]
  },
  plugins: [
    //     new CleanWebpackPlugin(filesToClean),
    // new StartServerPlugin("server.js"),
    new webpack.NamedModulesPlugin(),
    //      new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
      "process.env": { BUILD_TARGET: JSON.stringify("server") }
    }),
    new webpack.BannerPlugin({ banner: "require(\"source-map-support\").install();", raw: true, entryOnly: false }),
    new NodemonPlugin({
      // What to watch.
      watch: path.resolve("./dist/server.js"),
      ignore: ["*.js.map"],
      verbose: true,
      script: "./dist/server.js"
    })
  ],
  output: { path: path.join(__dirname, "dist"), filename: "server.js" }
}

module.exports = [ clientConfig, serverConfig ]
