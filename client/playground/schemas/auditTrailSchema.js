// eslint-disable-next-line no-unused-vars
const auditTrailSchema = {
  resourceLevel1:String,  // TransactionDeals, RFP, etc etc.
  resourceLevel1ObjectId:String,  // The ID of the Deal, RFP, Entity, Relationship, User etc that was updated.
  resourceLevel2:String, // TransactionDeals.Participants ; RFP.Distribute ; RFP.checklists
  resourceLevel2ObjectId:String,  // Any applicable ID nested under level 1. This may be blank.
  actionType: String,   // Insert, Update, UPload, Delete
  userId: String,       // The User who actually made the modification
  ipAddress:String,
  message: String,      // Updating address for Firm Entity ID
  updatedAt: Date       // When did the Update take Place
}

/*
 {
  RFPTransactionCreation
  RFPTransactionUpdate
  Distribute
    RFPTeam:[Add, Modify, Delete]
    RFPProcessContactsL:[ Add, Modify, Delete]
    RFPParticipants
    RFPBidPacketUpload
    RFPChecklists
    RFPDistribute
  RFPManage
    ResponseSubmission
    Questions
    Posts
    responses
  RFPSelect
    rating
    finalrating
    contractsubmission
  }
 */
