const entityAddressSchema = {
  addressName:String,
  isPrimary:Boolean,
  isHeadQuarter:Boolean,
  isActive:Boolean,
  firmName:String,
  website:String,
  officePhone:[
    {countryCode:String,phoneNumber:Number, extension:Number}
  ],
  officeFax:[
    { faxNumber:Number }
  ],
  officeEmails:[{emailId:String}],
  addressLine1:String,
  addressLine2:String,
  country:String,
  state:String,
  city:String,
  zipCode:{zip1:String, zip2:String}
}

const firmAddOnsSchema ={
  serviceName:String,
  serviceEnabled:Boolean
}

const entityFlagSchema = {
  marketRole:{
    underwriter:Boolean,
    registeredInvestmentAdvisor:Boolean,
    underwriterCounsel:Boolean,
    arbitrageServices:Boolean,
    escrowAgent:Boolean,
    bank:Boolean,
    trustee:Boolean,
    payingAgent:Boolean
  },

  issuerFlags:{
    stateIssuer:Boolean,
    stateAuth:Boolean,
    cityIssuer:Boolean,
    countryIssuer:Boolean,
    conduitIssuer:Boolean,
    localAuth:Boolean,
    locality:Boolean,
    Other:Boolean
  }
}

const entityLinkCusipSchema = {
  debtType:String,
  associatedCusip6:String,
}

const entityLinkBorrowersObligorsSchema = {
  borOblRel:String,
  borOblFirmName:String,
  borOblDebtType:String,
  borOblCusip6:String,
  borOblStartDate:Date,
  borOblEndDate:Date
}

const userAddressSchema = {
  addressName:String,
  addressType:String, // Office address, headquarter or residence
  addressLine1:String,
  addressLine2:String,
  country:String,
  state:String,
  city:String,
  zipCode:{zip1:String, zip2:String}
}

const userAddonSchema = {
  serviceType:String,
  serviceEnabled:String
}

// eslint-disable-next-line no-unused-vars
const entitySchema = {
  _id: String,
  entityId:String,
  entityFlags:entityFlagSchema,
  isMuniVisorClient:Boolean,
  msrbFirmName:String,
  msrbRegistrantType:String,
  msrbId:String,
  entityAliases:[String],
  firmName:String,
  taxId:String,
  businessStructure:String,
  numEmployees:String,
  annualRevenue:String,
  addresses:[entityAddressSchema],
  firmAddOns:firmAddOnsSchema,
  entityLinkedCusips:[entityLinkCusipSchema],
  entityBorObl:[entityLinkBorrowersObligorsSchema] // these are obptional
}

// eslint-disable-next-line no-unused-vars
const userCredentials = {
  _id:String,
  userId:String, // Should come from the user Collection
  userEmailId:String, // We can get rid of this if we are able to just use the email ID.
  password:String,  // This is the salted password.
  passwordConf:String
}

// eslint-disable-next-line no-unused-vars
const userLoginSession = {
  _id:String,
  userId:String,
  ipAddress:String,
  sessionStartTime:Date,
  sessionEndTime:Date,
}

// eslint-disable-next-line no-unused-vars
const userSchema = {
  _id: String,
  userId:String, // Should we make this the same as the primary email
  entityId:String, // Needs to Link to the entity
  userFlags: [String], // [series50,msrbcontact, emmacontact, supprinsipal, compofficer, mfp, primarycontact, procurementcont, fincontact, distmember, electedofficial, invstcontact, attorney, cpa ]
  userRole:String, // [ Admin, ReadOnly, Backup ]
  userEntitlement:String, // [ Global, Transaction ]
  userFirstName:String,
  userMiddleName:String,
  userLastName:String,
  userEmails:[{
    emailId:String,
    emailPrimary:Boolean,
  }],
  userPhones:[{
    countryCode:String,
    phoneNumber:Number,
    extension:Number,
    phonePrimary:Boolean
  }],
  userFaxNumbers:[{
    faxNumber:Number,
    faxPrimary:Boolean
  }],
  userEmployeeID:String,
  userJobTitle:String,
  userManagerEmail:String,
  userJoiningDate:Date,
  userExitDate:Date,
  userCostCenter:String,
  userAddresses:[userAddressSchema],
  userAddOns:[userAddonSchema],
  userAddDate:Date,
  userUpdateDate:Date
}

// eslint-disable-next-line no-unused-vars
const entityRelationships = {
  _id: String,
  entityRelId:String,
  entityParty1: String,
  entityParty2:String,
  relationshipType:String // Client relationship, 3rd Party Entity
}

// eslint-disable-next-line no-unused-vars
const entityContracts = {
  _id: String,
  contractId:String,
  entityRelId:String, // refers to the relationship from the above table
  entityParty1:String,  // Should Link to Entity - Majority of these are going will be pointing the FA client who gets onboarded.
  entityParty2:String,  // Should Link to the Entity
  contractName:String,
  contractType:String,
  securityType:String,
  startDate:Date,
  endDate:Date,

  contractDocuments:[{
    docType:String,
    docAwsLocation:String,
    docAddDate:Date,
    docUpdateDate:Date
  }],

  scheduleOfFeesTransactional:[{
    feeRange:String,
    feeThreshold:Number,
    feeDealType:String,
    feeMin:Number,
    feeMax:Number
  }],

  scheduleOfFeesNonTransactional:[{
    feeRange:String,
    feeThreshold:Number,
    feeDealType:String,
    feeMin:Number,
    feeMax:Number
  }],

  scheduleRetainerEscalator:[{
    feeRange:String,
    feeThreshold:Number,
    feeDealType:String,
    feeMin:Number,
    feeMax:Number
  }],
  addDate:Date,
  updateDate:Date,
}
// eslint-disable-next-line no-unused-vars
const userRoleEntitlementConfig = {
  _id: String,
  entityId:String,        // This will come from the entity Schema
  userRole:String,        // Read, Edit, Admin, Platadmin, External - Links to the User Schema
  userEntitlement:String, // Global or Transaction
  moduleUILevel1:String,
  moduleUILevel2:String,
  moduleUILevel3:String,
  actions:[{
    actionType:String,
    appResLevel1:String,
    appResLevel2:String,
  }]
}

// eslint-disable-next-line no-unused-vars
const pickListItemsSchema = {
  itemId:String,
  itemName:String,
  itemDescription:String,
  addDate:Date,
  addUser:String,
  updateDate:Date,
  updateUser:String,
  subLists:this
}

// eslint-disable-next-line no-unused-vars
const pickListConfig = {
  _id: String,
  entityId:String, //
  pickListName:String,
  pickListSystemName:String,
  params: {
    subListLevel2:Boolean,
    subListLevel3:Boolean,
    systemConfig:Boolean,
    restrictedList:Boolean,
    externalList:Boolean,
    bestPractice:Boolean
  },
  pickListDetails:[pickListItemsSchema]
}

// eslint-disable-next-line no-unused-vars
const checkListConfig =
{
  _id: String,
  entityId: String,
  rfpChecklists: {
    type: String,
    data: [
      {
        title: String,
        items: [
          {
            label: String,
          }
        ]
      }
    ]
  },
  dealCheckLists: {
    type: String,
    data: [
      {
        title: String,
        items: [
          {
            label: String,
          }
        ]
      }
    ]
  }
}

