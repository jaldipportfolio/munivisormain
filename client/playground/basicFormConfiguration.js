// The Input Config

const dbDisplayFielsMap = [
  {
    name: "testField1",
    fieldLabel: "Select Transaction Team",
    component: "renderMultiSelect",
    lkupdata: ["BANANA", "ORANGE", "APPLE"],
    row:1,col:1
  },
  {
    name: "testField2",
    fieldLabel: "Date Control",
    component: "renderBulmaDateComponent",
    row:1,col:2
  },
  {
    name: "testField3",
    fieldLabel: "Primary Sector",
    component: "renderBulmaDropDown",
    cssClass: "is-small",
    lkupdata: [
      "PLEASE SELECT",
      "SECTOR1",
      "SECTOR 2",
      "SECTOR 3",
      "SECTOR 4",
      "SECTOR 5",
      "SECTOR 6",
      "SECTOR 7"
    ],
    row:1,col:3
  },
  {
    fieldLabel: "CheckBox control",
    name: "testField5",
    component: "renderCheckBox",
    row:2,col:1
  }
]

// Sorts the data based on column and row

const dbDisplayFielsMapSorted = dbDisplayFielsMap.sort( (a1, a2) => {
  if(a1.row === a2.row) {
    return ( (a1.col < a2.col) ? -1 : ((a1.col > a2.col) ? 1:0 ))
  }
    
  return a1.row < a2.row ? -1 : 1 
    
})

// convert this into an object that groups data by rows
const finalDisplayFieldsMap = dbDisplayFielsMapSorted.reduce( (displayArray, {row,col,...other}) => {
  // eslint-disable-next-line no-unused-expressions
  displayArray[row] ? displayArray[row].push(other) : displayArray[row] =[other]
  return displayArray
},{})

console.log("test", JSON.stringify(finalDisplayFieldsMap,null, 2))
