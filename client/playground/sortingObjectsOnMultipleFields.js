const sortObj = [
  {
    name:"R2:C1",
    row:2,
    col:1,
  },
  {
    name:"R1:C3",
    row:1,
    col:3
  },
  {
    name:"R1:C2",
    row:1,
    col:2
  },
  {
    name:"R2:C2",
    row:2,
    col:2
  },
  {
    name:"R4:C2",
    row:4,
    col:2
  }
]

const revisedSorted = sortObj.sort( (a1, a2) => {
  if(a1.row === a2.row) {
    return ( (a1.col < a2.col) ? -1 : ((a1.col > a2.col) ? 1:0 ))
  }
   
  return a1.row < a2.row ? -1 : 1 
   
})

console.log(JSON.stringify(revisedSorted,null, 2))