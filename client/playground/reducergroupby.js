import moment from "moment"

const projWithDates = [
  {
    projId: 1,
    projName: "Project 1",
    startDate: moment("2018-01-01")
  },
  {
    projId: 2,
    projName: "Project 2",
    startDate: moment("2018-02-01")
  },
  {
    projId: 3,
    projName: "Project 3",
    startDate: moment("2017-06-01")
  },

  {
    projId: 4,
    projName: "Project 4",
    startDate: moment("2017-09-01")
  }
]



// Sort Ascending or Decending depending on the start date
// eslint-disable-next-line no-unused-vars
const sortedProjectList = projWithDates.sort((p1, p2) => -(p1.startDate - p2.startDate))


// For a given category of months...the following will return an object with projects properly organized into given intervals.
// eslint-disable-next-line no-unused-vars
const projectCategorizedIntoDateBuckets = projWithDates.reduce(
  (dateRangeObject, proj) => {
    [
      { intervalLabel: "6 Months", intervalValue: 6 },
      { intervalLabel: "12 Months", intervalValue: 12 }
    ].map(({ intervalLabel, intervalValue }) => {
      const lowerRangeDate = moment().subtract(intervalValue, "months")
      if (proj.startDate.isSameOrAfter(lowerRangeDate)) {
        if (!dateRangeObject[intervalLabel]) {
          dateRangeObject[intervalLabel] = {}
          dateRangeObject[intervalLabel].projects = []
          dateRangeObject[intervalLabel].projects.push(proj)
        } else {
          dateRangeObject[intervalLabel].projects.push(proj)
        }
      }
      return 1
    })
    return dateRangeObject
  },
  {}
)

// Return an array of all the elements that fall into the given category. this should be easier to manipulate
const projectCategorizedIntoDateBucketsRevised = projWithDates.reduce(
  (dateRangeObject, proj) => {
    [
      { intervalLabel: "6 Months", intervalValue: 6 },
      { intervalLabel: "12 Months", intervalValue: 12 }
    ].map(({ intervalLabel, intervalValue }, intervalIndex) => {
      const lowerRangeDate = moment().subtract(intervalValue, "months")
      if (proj.startDate.isSameOrAfter(lowerRangeDate)) {
        if (!dateRangeObject[intervalIndex]) {
          dateRangeObject[intervalIndex] = { [intervalLabel]: { projects: [] } }
          dateRangeObject[intervalIndex][intervalLabel].projects.push(proj)
        }
        else {
          dateRangeObject[intervalIndex][intervalLabel].projects.push(proj)
        }
      }
      return 1
    })
    return dateRangeObject
  },
  []
)

console.log(JSON.stringify(projectCategorizedIntoDateBucketsRevised, null, 2))
