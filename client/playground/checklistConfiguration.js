const checkListDbConfig = [{
  RFPCheckList: [{
    checkListId: "purposeOfRequest",
    checkListDisplayName: "Name and Brief Descripton of the Opportunity",
    checkListItems: [{
      "Item 1": "Item 1.1",
      "Item 2": "Item 1.2",
      "Item 3": "Item 1.3"
    },
    {
      "Item 1": "Item 2.1",
      "Item 2": "Item 2.2",
      "Item 3": "Item 2.3"
    },
    {
      "Item 1": "Item 3.1",
      "Item 2": "Item 3.2",
      "Item 3": "Item 3.3"
    },
    {
      "Item 1": "Item 4.1",
      "Item 2": "Item 3.2",
      "Item 3": "Item 4.3"
    }
    ]
  }]
}]

const checkListRenderConfig = [{
  RFPCheckList: {
    purposeOfRequest: {
      displayContainer: "AccordionTable",
      displayFields: [{
        fieldItem: -1,
        fieldName: "consider",
        fieldHeader: "Consider?",
        fieldToolTip: "Do we need to consider to be saved",
        componentType: "input",
        params: {
          a: "askdl",
          b: "askdflk"
        }
      },
      {
        fieldItem: 1,
        fieldToolTip: "This is Field1 from DB",
          
      },
      {
        fieldItem: -1,
        fieldName: "assignedTo",
        fieldHeader: "Assigned To",
        fieldToolTip: "The Person or Group to Which the Item is assigned to",
        componentType: "input",
        params: {
          a: "askdl",
          b: "askdflk",
          c: "askdjflasdkfjadf"
        }
      },
      {
        fieldItem: 2,
        fieldToolTip: "This is Field2 from DB"
      },
      {
        fieldItem: 3,
        fieldToolTip: "This is Field3 from DB"
      }
      ]
    }
  }
}]

export const renderDynamicChecklistConfig = () => {
  const {
    checkListId,
    checkListDisplayName,
    checkListItems
  } = checkListDbConfig[0].RFPCheckList[0]
  const {
    displayFields
  } = checkListRenderConfig[0].RFPCheckList[checkListId]

  // Get the distinct set of keys for the Header from the checklist items

  const checkListItemHeaders = checkListItems.reduce(
    (allCheckListHeaders, checklist) => Object.values({
      ...allCheckListHeaders,
      ...Object.keys(checklist)
    }), []
  )
  // Get all the Header Values to be Displayed in the Right Order
  const headerFields = displayFields.reduce((finalHeaders, field) => {
    const {
      fieldItem,
      fieldHeader
    } = field
    return fieldItem > 0 ?
      [...finalHeaders, checkListItemHeaders[fieldItem - 1]] :
      [...finalHeaders, fieldHeader]
  }, [])

  const headerFieldsWithToolTips = displayFields.reduce(
    (finalHeaders, field) => {
      const {
        fieldItem,
        fieldToolTip,
        fieldHeader
      } = field
      return fieldItem > 0 ?
        [
          ...finalHeaders,
          {
            fieldHeader: checkListItemHeaders[fieldItem - 1],
            fieldToolTip
          }
        ] :
        [...finalHeaders, {
          fieldHeader,
          fieldToolTip
        }]
    }, []
  )

  const consolidatedDisplayConfig = displayFields.reduce(
    (consolidatedDispConf, field) => {
      const {
        fieldItem,
        fieldName,
        componentType,
        params={}
      } = field
      return fieldItem > 0 ?
        [
          ...consolidatedDispConf,
          {
            fieldName: checkListItemHeaders[fieldItem - 1],
            componentType: "LabelBox",
            editable: false,
            params
          }
        ] :
        [...consolidatedDispConf, {
          fieldName,
          componentType,
          editable: true,
          params
        }]
    }, []
  )

  const finalDisplayValues = checkListItems.reduce(
    (finalDecoratedValues, checkListItem) => {
      const generateRow = displayFields.map(
        ({
          fieldItem,
          fieldName,
          componentType,
          params = {}
        },
        ) => {
          const componentToBeRendered =
            fieldItem > 0 ?
              {
                fieldName: checkListItemHeaders[fieldItem - 1],
                fieldValue: checkListItem[checkListItemHeaders[fieldItem - 1]],
                componentType: "LabelBox",
                editable: false,
                params
              } :
              {
                fieldName,
                fieldValue: "",
                componentType,
                editable: true,
                params
              }
          return componentToBeRendered
        }
      )
      return [...finalDecoratedValues, generateRow]
    }, []
  )

  return {
    displayName: checkListDisplayName,
    displayConfig:consolidatedDisplayConfig,
    displayHeadersWithToolTips: headerFieldsWithToolTips,
    displayHeadersOnly: headerFields,
    displayComponents: finalDisplayValues
  }
}

// Render the Dynamic Fields
// console.log(JSON.stringify(renderDynamicChecklistConfig(), null, 2));

