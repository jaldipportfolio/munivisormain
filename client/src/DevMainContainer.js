import React from "react"
import {  Link } from "react-router-dom"

export const DevMainContainer = () => (
  <div>
    <ul>
      <li>
        <Link to="/dashboard">Dashboard AppBase</Link>
      </li>
      <li>
        <Link to="/rfpRating">Transaction Rating</Link>
      </li>
      <li>
        <Link to="/splash">splashscreen</Link>
      </li>
      <li>
        <Link to="/signin">Sign to MuniVisor</Link>
      </li>
      <li>
        <Link to="/signup?type=iss">SignUp</Link>
      </li>
      <li>
        <Link to="/signin">Sign In to Application</Link>
      </li>
      <li>
        <Link to="/forgotpass">Forgot Password</Link>
      </li>
      <li>
        <Link to="/">Transaction Example</Link>
      </li>
      <li>
        <Link to="/ratingsform">RFP Ratings Form Example </Link>
      </li>
      <li>
        <Link to="/modal">Modal </Link>
      </li>
      <li>
        <Link to="/calendar">Calendar</Link>
      </li>
      <li>
        <Link to="/tasks">Task Management Screen</Link>
      </li>
      <li>
        <Link to="/checklist">Show the checklist component</Link>
      </li>
      <li>
        <Link to="/newaccordion">Accordion Components</Link>
      </li>
      <li>
        <Link to="/newaccordionkg">Accordion Component KG</Link>
      </li>
      <li>
        <Link to="/newaccordionalt">Accordion Component Alternate </Link>
      </li>
      <li>
        <Link to="/checklistKG">Checklist KG</Link>
      </li>
      <li>
        <Link to="/auditlog">Audit Log</Link>
      </li>
      <li>
        <Link to="/docs">Documents With List</Link>
      </li>
      <li>
        <Link to="/upload">Documents Upload</Link>
      </li>
      <li>
        <Link to="/deals">Deals</Link>
      </li>
      <li>
        <Link to="/dealRatings">Deal Ratings</Link>
      </li>
      <li>
        <Link to="/seriesratings">Series Deal Ratings</Link>
      </li>
      <li>
        <Link to="/costofissuance">Deal Cost of Issuance</Link>
      </li>
      <li>
        <Link to="/scheduleofevents">Schedule of events</Link>
      </li>
      <li>
        <Link to="/attach-checklist">Attach Checklist</Link>
      </li>
    </ul>
    {/* <Route path="/tasks" component={TaskMain} />

    <Route
      exact
      path="/ratingsform"
      component={() => (
        <div>
          <RenderRadioGroupInTable
            radioGroupInTableConfig={radioGroupInTableConfig}
          />
          <br />
          <RenderRatingSection ratingConfigObject={ratingSectionsForTenant} />
        </div>
      )}
    />

    <Route path="/modal" component={BulmaModalCard} />
    <Route path="/rfpRating" component={TransactionRFP} />
    <Route path="/calendar" component={BigCalendarComponent} />
    <Route
      exact
      path="/checklist"
      component={() => (
        <div>
          <CheckListComponent onSubmit={showResults} />
        </div>
      )}
    />
    <Route
      exact
      path="/newaccordion"
      component={() => <AccordionNewListRevised onSubmit={showResults}/>}
    />
    <Route
      exact
      path="/newaccordionkg"
      component={AccordionKG}
    />
    <Route
      exact
      path="/newaccordionalt"
      component={() => <AccordionGlobalAlt />} />
    <Route
      exact
      path="/checklistKG"
      component={ChecklistKG}
    />
    <Route
      exact
      path="/auditlog"
      component={AuditLog}
    />
    <Route
      exact
      path="/signup"
      component={SignUp}
    /> */}
  </div>
)
