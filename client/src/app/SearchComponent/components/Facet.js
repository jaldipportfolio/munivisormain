/* eslint-disable jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events */
import React, { Component } from "react"
import PropTypes from "prop-types"
import {
  DynamicRangeSlider,
  SingleDataList,
  MultiDataList,
  DateRange
} from "@appbaseio/reactivesearch"
import { FACET_TYPES } from "../../../constants"

class Facet extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isOpen: props.data.isOpen || false
    }

    this.handleToggle = this.handleToggle.bind(this)
  }

  handleToggle() {
    this.setState({
      isOpen: !this.state.isOpen
    })
  }

  render() {
    const { data } = this.props
    const { isOpen } = this.state
    const iconClass = isOpen ? "up" : "down"
    return (
      <div className="facet-item-wrapper">
        <div
          className="facet-item-title-wrapper"
          onClick={this.handleToggle}
        >
          <span className="search-filter-title"> {data.title} </span>
          <span className={`fas fa-chevron-${iconClass}`} />
        </div>
        {
          data.type === FACET_TYPES.DYNAMIC_RANGE_SLIDER && isOpen && (
            <DynamicRangeSlider
              {...data.props}
            />
          )
        }
        {
          data.type === FACET_TYPES.SINGLE_DATA_LIST && isOpen && (
            <SingleDataList
              {...data.props}
            />
          )
        }
        {
          data.type === FACET_TYPES.MULTI_DATA_LIST && isOpen && (
            <MultiDataList
              {...data.props}
            />
          )
        }
        {
          data.type === FACET_TYPES.DATE_RANGE && isOpen && (
            <DateRange
              {...data.props}
            />
          )
        }
      </div>
    )
  }
}

Facet.propTypes = {
  data: PropTypes.object.isRequired
}

export default Facet
