/* eslint-disable jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events */
import React, { Component } from "react"
import PropTypes from "prop-types"
import styled from "styled-components"

import {
  ReactiveBase,
  DataSearch,
  ReactiveList
} from "@appbaseio/reactivesearch"
import {
  ELASTIC_SEARCH_URL,
  ELASTIC_SEARCH_INDEX,
  ELASTIC_SEARCH_CREDENTIALS
} from "../../../constants"
import PaginationDropDown from "../../FunctionalComponents/Dashboard/components/PaginationDropDown"
import Facet from "./Facet"
import ReactiveFacet from "./ReactiveFacet"
import "../scss/search.scss"

const Icon = () => (
  <span className="icon is-left searchIcon">
    <i className="fas fa-search" />
  </span>
)

const Result = styled(ReactiveList)`
  overflow: auto;
`

const Base = styled(ReactiveBase)`
  padding: 0px;
  font-family: 'GoogleSans' !important
`


class Search extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isFilterOpen: this.props.isFilterOpen || false,
      pageSize: 10,
    }

    this.handleFilterToggle = this.handleFilterToggle.bind(this)
  }

  handleFilterToggle() {
    this.setState({
      isFilterOpen: !this.state.isFilterOpen
    })
  }

  handlePageChange = pageSize => {
    this.setState({
      pageSize,
    })
  }

  render() {
    const {
      esTypes,
      facets,
      textSearchFields,
      searchBarPlaceHolder,
      SearchResultItems,
      fieldWeights,
      defaultSearchQuery,
      reactiveFacets
    } = this.props
    const { isFilterOpen, pageSize } = this.state
    const iconClass = isFilterOpen ? "left" : "right"

    return (
      <Base
        app={ELASTIC_SEARCH_INDEX}
        url={ELASTIC_SEARCH_URL}
        credentials={ELASTIC_SEARCH_CREDENTIALS}
        type={esTypes}
      >
        <div>
          <div className="column is-one-third is-offset-8">
            <div className="searchContainer">
              <DataSearch
                componentId="mainSearch"
                dataField={textSearchFields}
                autosuggest={false}
                iconPosition="left"
                queryFormat="and"
                placeholder={searchBarPlaceHolder}
                fieldWeights={fieldWeights}
                className="searchbox"
                highlight
                innerClass={{
                  input: "input customInput"
                }}
                showIcon={false}
              />
              <Icon />
            </div>
          </div>
        </div>

        <div className="columns">
          {
            Boolean(facets.length) && (
              isFilterOpen ? (
                <div className="column is-one-quarter">
                  <div className="facet-wrapper">
                    <div className="facet-title">
                      <span>Filters</span>
                      <span
                        className={`icons filter-close fas fa-chevron-${iconClass}`}
                        onClick={this.handleFilterToggle}
                      />
                    </div>
                    <br />
                    {
                      facets.map((data) => (
                        <Facet
                          key={data.id}
                          data={data}
                        />
                      ))
                    }
                  </div>
                </div>
              ) : (
                <div>
                  <span
                    className="icons filter-open fas fa-filter"
                    onClick={this.handleFilterToggle}
                  />
                </div>
              )
            )
          }
          {
            Boolean(reactiveFacets.length) && (
              <div className="column is-one-quarter">
                <div className="facet-wrapper">
                  <div className="facet-title">
                    <span>Filters</span>
                    <span
                      className={`icons filter-close fas fa-chevron-${iconClass}`}
                      onClick={this.handleFilterToggle}
                    />
                  </div>
                  <br />
                  {
                    reactiveFacets.map((data) => (
                      <ReactiveFacet
                        key={data.componentId}
                        data={data}
                      />
                    ))
                  }
                </div>
              </div>
            )
          }
          <div className="column">

            <div className="search-result-container" style={{ position: "relative"}}>
              <div
                style={{
                  position: "absolute",
                  top: "10px",
                  right: "20px"
                }}
              >
                <PaginationDropDown
                  defaultValue={pageSize}
                  handlePageChange={this.handlePageChange}
                />
              </div>
              <ReactiveList
                componentId="ResultCard"
                dataField="created_at"
                size={pageSize}
                pagination
                loader=""
                urlParams={false}
                defaultQuery={defaultSearchQuery}
                react={{
                  and: [
                    "mainSearch",
                    ...facets.map(facet => facet.props.componentId),
                    ...reactiveFacets.map(f => f.props.componentId)
                  ]
                }}
                paginationAt="both"
                renderAllData={(res) => (
                  <div>
                    <SearchResultItems
                      data={res}
                    />
                  </div>
                )}
                showResultStats
                onResultStats={
                  (total, time) => (
                    <h3 style={{ position: "absolute", top: "15px", left: "10px"}}>Found {total} records in {time} ms</h3>
                  )
                }
              />
            </div>
          </div>
        </div>
      </Base>
    )
  }
}

Search.propTypes = {
  esTypes: PropTypes.string.isRequired,
  facets: PropTypes.array,
  textSearchFields: PropTypes.array.isRequired,
  searchBarPlaceHolder: PropTypes.string,
  SearchResultItems: PropTypes.func.isRequired,
  fieldWeights: PropTypes.array,
  defaultSearchQuery: PropTypes.func,
  reactiveFacets: PropTypes.array
}

Search.defaultProps = {
  facets: [],
  reactiveFacets: [],
  searchBarPlaceHolder: "Search..",
  fieldWeights: [],
  defaultSearchQuery: () => ({})
}

export default Search
