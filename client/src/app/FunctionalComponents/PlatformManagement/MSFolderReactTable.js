import React from "react"
import "react-table/react-table.css"
import ReactTable from "react-table"

const pdfFile = "https://spoprod-a.akamaihd.net/files/fabric/assets/item-types/20/pdf.svg?refresh1"
const pptxFile = "https://spoprod-a.akamaihd.net/files/fabric/assets/item-types/20/pptx.svg?refresh1"
const xlsxFile = "https://spoprod-a.akamaihd.net/files/fabric/assets/item-types/20/xlsx.svg?refresh1"
const image = "https://spoprod-a.akamaihd.net/files/fabric/assets/item-types/20/photo.svg?refresh1"
const docx = "https://spoprod-a.akamaihd.net/files/fabric/assets/item-types-fluent/20/docx.svg?refresh1"
const other = "https://spoprod-a.akamaihd.net/files/fabric/assets/item-types-fluent/20/txt.svg?refresh1"

const MSFolderReactTable = ({
  driveList,
  onPathSelect,
  onSelect,
  selected,
  currentStep
}) => {
  const columns = [
    {
      Header: <div className="hpTablesTd wrap-cell-text" style={{justifyContent: "center", display: "flex"}}>
        <label className="checkbox-button" style={{paddingLeft: 5}}>
          <input
            type="checkbox"
            name="all"
            style={{position: "inherit"}}
            checked={driveList.length === 0 ?  false : driveList.length === selected.length || false}
            onChange={(e) => onSelect(e, currentStep)}
          /><span className="checkmark" style={{border: "1px solid black"}}/>
        </label>
      </div>,
      id: "select",
      className: "multiExpTblVal",
      sortable: false,
      width: 100,
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text" style={{justifyContent: "center", display: "flex"}}>
            <label className="checkbox-button">
              <input
                type="checkbox"
                name="selected"
                checked={selected.some(s => s.id === item.id) || false}
                onChange={(e) => onSelect(e, item.id)}
              /><span className="checkmark" style={{border: "1px solid black"}}/>
            </label>
          </div>
        )
      }
    },
    {
      Header: "Name",
      id: "name",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        const fileType = item && item.name && item.name.split(".").pop()
        return (
          <div className="hpTablesTd wrap-cell-text">
            { item && Object.keys(item).includes("folder") ?
              <img
                style={{height: 25, width: 50, marginBottom: -7}}
                src="https://spoprod-a.akamaihd.net/files/fabric/assets/item-types/20/folder.svg?refresh1"/> :
              <img
                style={{height: 25, width: 50, marginBottom: -7}}
                src={fileType === "pdf" ? pdfFile : fileType === "xlsx" ? xlsxFile : fileType === "pptx" ? pptxFile : (fileType === "jpeg" || fileType === "jpg") ? image : fileType === "docx" ? docx : other }/>
            }
            <span style={{cursor: "pointer"}} onClick={() => onPathSelect(item)}>{item && item.name}</span>
          </div>
        )
      },
      sortMethod: (a, b) => a.name - b.name
    }
  ]

  return (
    <ReactTable
      columns={columns}
      data={driveList}
      showPaginationBottom
      defaultPageSize={10}
      // pageSizeOptions={pageSizeOptions}
      className="-striped -highlight is-bordered"
      style={{ overflowX: "auto" }}
      showPageJump
      minRows={2}
    />
  )
}

export default MSFolderReactTable
