import React, { Component } from "react"
import {withRouter} from "react-router-dom"
import connect from "react-redux/es/connect/connect"
import {toast} from "react-toastify"
import { getOneDriveFilesAndFolders, getOneDriveFileOrFolderDetails, getOneDriveFileOrFolderDetailsSecond } from "GlobalUtils/helpers"
import {ContextType} from "../../../globalutilities/consts"
import Loader from "../../GlobalComponents/Loader"
import MSFolderReactTable from "./MSFolderReactTable"
import CONST from "../../../globalutilities/consts"


class MSFolderAndFilesList extends Component {
  constructor(){
    super()
    this.state = {
      data: {
        firstLevel: [],
        secondLevel: [],
        thirdLevel: [],
        fourthLevel: [],
        fifthLevel: [],
        sixthLevel: [],
        seventhLevel: [],
      },
      select: {
        firstLevel: [],
        secondLevel: [],
        thirdLevel: [],
        fourthLevel: [],
        fifthLevel: [],
        sixthLevel: [],
        seventhLevel: [],
      },
      pathLevel: [],
      step: 1,
      loading: true,
    }
  }

  async componentWillMount() {
    const msDocsDetails = await this.onGetMSDrives()
    this.setState({
      pathLevel: [{id: 0, name: "Files"}],
      data: {
        ...this.state.data,
        firstLevel: msDocsDetails && msDocsDetails.driveAndDocs || []
      },
      loading: false
    })
  }

  getCurrentPathData = (step, type) => {
    const object = this.state[type]
    const { firstLevel, secondLevel, thirdLevel, fourthLevel, fifthLevel, sixthLevel, seventhLevel } = object
    return step === 1 ? firstLevel :
      step === 2 ? secondLevel :
        step === 3 ? thirdLevel :
          step === 4 ? fourthLevel :
            step === 5 ? fifthLevel :
              step === 6 ? sixthLevel :
                step === 7 ? seventhLevel :
                  []
  }

  onGetMSDrives = async () => {
    const msCredential = await localStorage.getItem("ms_credential")
    const code = await localStorage.getItem("code")
    const data = await getOneDriveFilesAndFolders(code, msCredential)
    return data
  }

  onPathSelect = async (value) => {
    const { pathLevel, data } = this.state
    let { step } = this.state
    let { secondLevel, thirdLevel, fourthLevel, fifthLevel, sixthLevel, seventhLevel } = data
    this.setState({
      loading: true
    }, async () => {
      let msCredential = await localStorage.getItem("ms_credential")
      msCredential = msCredential ? JSON.parse(msCredential) : {}
      const type = value && Object.keys(value) && Object.keys(value).includes("folder") ? "/children" : ""
      const fileDetails = await getOneDriveFileOrFolderDetails(msCredential && msCredential.access_token, value && value.id, type)

      if(!type){
        this.setState({
          loading: false
        })
        window.open(fileDetails && fileDetails.webUrl)
      } else {
        const { id, name } = value || {}
        pathLevel.push({id, name})
        step += 1
        if(step === 2){
          secondLevel = fileDetails || []
        } else if(step === 3){
          thirdLevel = fileDetails || []
        } else if(step === 4){
          fourthLevel = fileDetails || []
        } else if(step === 5){
          fifthLevel = fileDetails || []
        } else if(step === 6){
          sixthLevel = fileDetails || []
        } else if(step === 7){
          seventhLevel = fileDetails || []
        }
        this.setState({
          data: {
            ...data,
            secondLevel,
            thirdLevel,
            fourthLevel,
            fifthLevel,
            sixthLevel,
            seventhLevel,
          },
          pathLevel,
          step,
          loading: false
        })
      }
    })
  }

  onPathChange = (value) => {
    const { pathLevel, select } = this.state
    let { secondLevel, thirdLevel, fourthLevel, fifthLevel, sixthLevel, seventhLevel } = select
    const array = []
    const index = pathLevel.findIndex(path => path.id === value.id)
    pathLevel.map((p, i) => {
      if(index >= i){
        array.push(p)
      }
    })
    if(value && value.id === 0){
      secondLevel = []
      thirdLevel = []
      fourthLevel = []
      fifthLevel = []
      sixthLevel = []
      seventhLevel = []

    }
    this.setState({
      pathLevel: array || [],
      step: index + 1,
      select: {
        ...select,
        secondLevel,
        thirdLevel,
        fourthLevel,
        fifthLevel,
        sixthLevel,
        seventhLevel
      }
    })
  }

  onSelect = async (e, value) => {
    const { checked, name } = e.target
    const { step } = this.state
    const selected = step === 1 ? "firstLevel" :
      step === 2 ? "secondLevel" :
        step === 3 ? "thirdLevel" :
          step === 4 ? "fourthLevel" :
            step === 5 ? "fifthLevel" :
              step === 6 ? "sixthLevel" :
                step === 7 ? "seventhLevel" : ""
    let selectedArray = await this.getCurrentPathData(step, "select")
    if(name === "all"){
      const array = await this.getCurrentPathData(step, "data")
      if(array.length === selectedArray.length){
        selectedArray = []
      } else {
        const data = []
        array && array.forEach(m => {
          if(m.hasOwnProperty("file")){
            data.push({
              id: m.id,
              file: m.file
            })
          } else {
            data.push({
              id: m.id,
              folder: m.folder
            })
          }
        } )
        selectedArray = data
      }
    } else if(checked){
      const array = await this.getCurrentPathData(step, "data")
      const data = array.find(a => a.id === value)
      if(data.hasOwnProperty("file")){
        selectedArray.push({
          id: data.id,
          file: data.file
        })
      } else {
        selectedArray.push({
          id: data.id,
          folder: data.folder
        })
      }
    } else {
      const index = selectedArray.findIndex(s => s.id === value)
      selectedArray.splice(index, 1)
    }
    this.setState({
      select: {
        ...this.state.select,
        [selected]: selectedArray
      }
    })
  }

  onSave = async () => {
    const { step } = this.state
    const { user } = this.props
    const userDetails = {
      tenantId: user && user.tenantId,
      entityId: user && user.entityId,
      userId: user && user.userId,
      userName: user && `${user.userFirstName} ${user.userLastName}`,
      contextType: ContextType.oneDrive
    }
    this.setState({
      loading: true
    }, async () => {
      let msCredential = await localStorage.getItem("ms_credential")
      msCredential = msCredential ? JSON.parse(msCredential) : {}
      const data = await this.getCurrentPathData(step, "select")
      let number = 0
      data.map(async (d) => {
        const fileDetails = await getOneDriveFileOrFolderDetailsSecond(msCredential, userDetails, [d])

        if(fileDetails && fileDetails.done){
          toast("Files and folders copied successfully!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS })
          number += 1
        } else {
          toast(fileDetails && fileDetails.message || "something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR })
        }

        if(data.length === number){
          this.setState({
            loading: false,
            select: {
              firstLevel: [],
              secondLevel: [],
              thirdLevel: [],
              fourthLevel: [],
              fifthLevel: [],
              sixthLevel: [],
              seventhLevel: [],
            }
          })
        }
      })
    })
  }

  render() {
    const { loading, step, pathLevel } = this.state
    return (
      <div>
        { loading ? <Loader/> : null }

        <div className="column multiExpTblVal">
          { pathLevel && pathLevel.map((path, i) =>
            <span
              key={i.toString()}
              style={i + 1 === pathLevel.length ? {fontSize: 20, fontStyle: "inherit", color: "#333333"} : {fontSize: 20, fontStyle: "inherit", color: "#333333", cursor: "pointer"}}
              onClick={i + 1 === pathLevel.length ? null : () => this.onPathChange(path, i)}>
              {path && path.name}&nbsp;
              { i + 1 === pathLevel.length ? null : <i className="fas fa-chevron-right" style={{fontSize: 18}}/> }&nbsp;
            </span>) }
        </div>

        <div className="column">
          <MSFolderReactTable
            driveList={this.getCurrentPathData(step, "data")}
            selected={this.getCurrentPathData(step, "select") || []}
            onPathSelect={this.onPathSelect}
            onSelect={this.onSelect}
            currentStep={step}
          />
        </div>

        <div className="field is-grouped-center">
          <div className="control">
            <button
              className="button is-link"
              onClick={this.onSave}
              disabled={!this.getCurrentPathData(step, "select").length || false}
            >
              Copy Documents
            </button>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  loggedInContactId: (state.auth && state.auth.userEntities && state.auth.userEntities.userId) || "",
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

export default withRouter(connect(mapStateToProps, null)(MSFolderAndFilesList))
