import React from "react"
import { Link } from "react-router-dom"

import Picklists from "Global/Picklists"

const TABS = [
  {path: "picklists", label: "Picklists"},
]

const ReferenceDataMain = props => {
  const option = props.option || "picklists"

  const renderTabs = (tabs, option) => tabs.map(t =>
    <li key={t.path} className={option === t.path ? "is-active" : "inactive-tab"}>
      <Link to={`/configuration/reference-data/${t.path}`}>{t.label}</Link>
    </li>
  )

  const renderViewSelection = (option) => (
    <div className="tabs">
      <ul>
        {renderTabs(TABS, option)}
      </ul>
    </div>
  )

  const renderSelectedView = (option) => {
    switch(option) {
    case "picklists" :
      return <Picklists />
    default:
      return <p>{option}</p>
    }
  }

  return (
    <div id="main">
      <section>
        {renderViewSelection(option)}
        {renderSelectedView(option)}
      </section>
    </div>
  )
}

export default ReferenceDataMain
