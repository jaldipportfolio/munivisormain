import React, { Component } from "react"
import swal from "sweetalert"
import connect from "react-redux/es/connect/connect"
import {withRouter} from "react-router-dom"
import * as qs from "query-string"
import { toast } from "react-toastify"
import CONST from "GlobalUtils/consts"
import Loader from "../../GlobalComponents/Loader"
import RatingSection from "../../GlobalComponents/RatingSection"
import Accordion from "../../GlobalComponents/Accordion"
import {getOutLookContacts, oAuthSignIn, getOutLookContactsFromFolder, importContacts} from "../../StateManagement/actions/OutlookSerivce"
import OneDriveFileManage from "./OneDriveFileManage"


class SyncWithOtherDrives extends Component {

  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      selectedDir: "",
      confirmAlert: CONST.confirmAlert
    }
  }

  componentWillMount() {
    const queryString = qs.parse(location.search)
    const code = queryString.code || localStorage.getItem("code") || ""
    console.log(queryString)
    localStorage.setItem("code", code)
    const msCredential = localStorage.getItem("ms_credential")
    if(code){
      this.getOutLookContacts(code, msCredential)
    }
    this.setState({
      msCredential: JSON.parse(msCredential) || {},
      code
    })
  }

  getOutLookContacts = (code, msCredential) => {
    const {match} = this.props
    const {confirmAlert} = this.state
    this.setState({
      loading: true
    }, async () => {
      const contacts = await getOutLookContacts(code, msCredential)
      localStorage.setItem("ms_credential", JSON.stringify((contacts && contacts.accessCredential) || {}))
      this.setState({
        msCredential: (contacts && contacts.accessCredential) || {},
        loading: false
      }, () => {
        if(contacts && contacts.done && match && match.url && !match.url.split("/").includes("tools-docs")){
          confirmAlert.title = `Total ${(contacts && contacts.totalResults) || 0} contacts found`
          confirmAlert.text = "Do you want to sync up outlook contact with our application?"
          swal(confirmAlert).then(will => {
            if (will) {
              console.log("Sync")
              if(contacts && contacts.contacts && contacts.contacts.length){
                this.setState({
                  loading: true
                }, async () => {
                  const res = await importContacts(contacts.contacts, "outlook")
                  toast(res.message, {
                    autoClose: CONST.ToastTimeout,
                    type: res && res.done ? toast.TYPE.SUCCESS : toast.TYPE.ERROR || "Something went wrong"
                  })
                  this.setState({
                    loading: false
                  })
                })
              }
            }
          })
        }
      })
    })
  }

  authSignIn = async () => {
    const {code, msCredential} = this.state
    if(msCredential && msCredential.access_token){
      this.getOutLookContacts(code, JSON.stringify(msCredential))
    }else {
      const response = await oAuthSignIn()
      if(response && response.signInUrl) {
        window.location = response.signInUrl
      }
    }
  }

  onChange = (e) => {
    const {name, value} = e.target
    this.setState({
      [name]: value
    }, () => {
      if(name === "selectedDir" && value){
        this.onDirectoryChange()
      }
    })
  }

  onDirectoryChange = () => {
    const {selectedDir, msCredential, code} = this.state
    if(!selectedDir) return
    this.setState({
      loading: true
    }, async () => {
      const res = await getOutLookContactsFromFolder(selectedDir, code, msCredential)
      localStorage.setItem("ms_credential", JSON.stringify((res && res.accessCredential) || {}))
      this.setState({
        msCredential: (res && res.accessCredential) || {},
        loading: false
      })
    })
  }

  render() {
    const {loading, code, msCredential} = this.state
    const {folderList, folderPropList, user, match} = this.props

    return (
      <div>
        {loading ? <Loader/> : null}
        <Accordion
          multiple activeItem={[0]} boxHidden render={({ activeAccordions, onAccordion }) =>
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="Data Sync Up With Other Drives"
                style={{ overflowY: "unset" }}>
                {activeAccordions.includes(0) &&
                  <div>
                    <div className="columns">
                      <div className="column margin-topTen is-full">
                        <button className="button is-info text-center" onClick={this.authSignIn}>Sync Outlook Contact</button>
                      </div>
                    </div>
                  </div>
                }

                { (code || (msCredential && Object.keys(msCredential).length)) && match && match.url && match.url.split("/").includes("tools-docs") ?
                  <div>
                    <br/>
                    <OneDriveFileManage
                      folderList={folderList || []}
                      folderPropList={folderPropList || []}
                      user={user}
                    />
                  </div>: null
                }
              </RatingSection>
            </div>
          }
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  loggedInContactId: (state.auth && state.auth.userEntities && state.auth.userEntities.userId) || "",
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

export default withRouter(connect(mapStateToProps, null)(SyncWithOtherDrives))
