import React, { Component } from "react"
import {toast} from "react-toastify"
import connect from "react-redux/es/connect/connect"
import {withRouter} from "react-router-dom"
import { GraphFileBrowser } from "@microsoft/file-browser"
import { copyDocsOneDriveToMuni } from "GlobalUtils/helpers"
import CopyDocumentsModal from "../Document/components/ManageDocuments/component/CopyDocumentsModal"
import MSFolderAndFilesList from "./MSFolderAndFilesList"
import { createDocFolders } from "../../StateManagement/actions/docs_actions/docs"
import Loader from "../../GlobalComponents/Loader"
import CONST from "../../../globalutilities/consts"


class OneDriveFileManage extends Component {
  constructor(){
    super()
    this.state = {
      keys: [],
      folderPropList: [],
      folderList: [],
      folderId: "",
      saveModal: false,
      loading: true,
    }
  }

  componentWillMount() {
    const { folderList, folderPropList } = this.props
    this.setState({
      folderList,
      folderPropList,
      loading: false
    })
  }

  getAuthenticationToken = async () => {
    let msCredential = await localStorage.getItem("ms_credential")
    msCredential = msCredential ? JSON.parse(msCredential) : {}
    this.setState({
      token: msCredential.access_token || ""
    })
    return Promise.resolve(msCredential.access_token)
  }

  onSuccess = async (keys) => {
    const { folderList, folderPropList } = this.state
    const { user } = this.props
    keys = keys.map(k => k && k.driveItem_203)

    if(folderPropList && !folderPropList.length && folderList && !folderList.length){
      const docs = {
        folderName: "My Files Without Folder",
        entityId: user.entityId,
        userId: user.userId,
        createdDate: new Date(),
        updatedDate: new Date(),
        myDocuments: true
      }
      await createDocFolders(docs, res => {
        this.setState({
          folderPropList: [res && res.data] || [],
          folderList: ["My Files Without Folder"] || []
        }, () => {
          this.setState({
            keys,
            saveModal: !this.state.saveModal
          })
        })
      })
    } else {
      this.setState({
        keys,
        saveModal: !this.state.saveModal
      })
    }
  }

  onCancel = (err) => {
    console.log("onCancel", err.message)
  }

  closeModal = () => {
    this.setState({
      folderId: "",
      folderName: "",
      saveModal: false
    })
  }

  onChange = (e) => {
    const { folderPropList } = this.state
    const { name, value } = e.target
    const folder = folderPropList && folderPropList.find(f => f.folderName === value) || {}
    const folderId = folder && folder._id || ""
    this.setState({
      [name]: value,
      folderId
    })
  }

  onFolderCopy = async () => {
    const { token, folderId, keys } = this.state
    const { user } = this.props
    const userDetails = {
      tenantId: user && user.tenantId,
      entityId: user && user.entityId,
      folderId,
      userId: user && user.userId,
      userName: user && `${user.userFirstName} ${user.userLastName}`
    }

    this.setState({
      loading: true,
      saveModal: !this.state.saveModal
    }, async () => {
      const response = await copyDocsOneDriveToMuni({keys, token, userDetails})
      if(response && response.done){
        toast("Documents Copied Successfully!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS})
        setTimeout(() => {
          this.props.history.push("/tools-docs")
        }, 2000)
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR})
        this.setState({
          loading: false
        })
      }
    })

  }

  render() {
    const { saveModal, keys, folderName, folderList, loading } = this.state
    return (
      <div>
        { loading ? <Loader/> : null }
        <CopyDocumentsModal
          keys={keys}
          saveModal={saveModal}
          folderList={folderList}
          folderName={folderName}
          onFolderCopy={this.onFolderCopy}
          closeModal={this.closeModal}
          onChange={this.onChange}
        />
        {/* <GraphFileBrowser
          getAuthenticationToken={this.getAuthenticationToken}
          onSuccess={this.onSuccess}
          onCancel={this.getAuthenticationToken}/> */}
        <MSFolderAndFilesList/>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  loggedInContactId: (state.auth && state.auth.userEntities && state.auth.userEntities.userId) || "",
  userEmail: (state.auth && state.auth.userEmail) || "",
})

export default withRouter(connect(mapStateToProps, null)(OneDriveFileManage))
