/* eslint-disable jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events, no-underscore-dangle */

import React from "react"
import { connect } from "react-redux"
// import "react-table/react-table.css"
import DealsResultSet from "./DealsResultSet"
import RfpResultSet from "./RfpsResultSet"
import MarfpResultSet from "./MarfpResultSet"

export const leadManager = (participants, nameKey, idKey, typeKey) => {
  const filteredList = (participants || []).find(f => f[typeKey] === "Municipal Advisor" || f[typeKey] === "Tenant")
  return filteredList ? {
    name: filteredList[nameKey],
    id: filteredList[idKey]
  } : {}
}


class SearchResultItems extends React.Component {
  constructor(props) {
    super(props)

    const res = props.data
    this.state = {
      isDealOpen: true,
      isRFPOpen: true,
      isMarfpOpen: true,
      res,
    }
  }

  componentWillReceiveProps(nextProps) {
    const res = nextProps.data
    this.updateState(res)
  }

  updateState = (res) => {
    this.setState({
      isDealOpen: true,
      isRFPOpen: true,
      isMarfpOpen: true,
      res,
    })
  }

  handleToggle = (group) => {
    this.setState({
      [group]: !this.state[group]
    })
  }

  render() {
    const { res } = this.state
    const { activeGroup } = this.props

    return (
      <div className="accordions" style={{ marginTop: "20px"}}>
        {activeGroup === "deals" && (<div className={`accordion ${this.state.isDealOpen && "is-active"}`}>
          <div className="accordion-header" onClick={() => { this.handleToggle("isDealOpen") }}>
            <span>Transactions</span>
            <div>
              {
                this.state.isDealOpen
                  ? <i className="fas fa-chevron-up" />
                  : <i className="fas fa-chevron-down" />
              }
            </div>
          </div>
          {
            this.state.isDealOpen && (
              <div className="accordion-body">
                <div className="accordion-content">
                  <div>
                    <DealsResultSet data={res} allUrls={this.props.allUrls} hasSubComponent />
                  </div>
                </div>
              </div>
            )
          }
        </div>
        )}
        {activeGroup === "rfps" && (<div className={`accordion ${this.state.isRFPOpen && "is-active"}`}>
          <div className="accordion-header" onClick={() => { this.handleToggle("isRFPOpen") }}>
            <span>Activity (Non-Transactions)</span>
            <div>
              {
                this.state.isRFPOpen
                  ? (<i className="fas fa-chevron-up" />)
                  : (<i className="fas fa-chevron-down" />)
              }
            </div>
          </div>
          {
            this.state.isRFPOpen && (
              <div className="accordion-body">
                <div className="accordion-content">
                  <div>
                    <RfpResultSet data={res} allUrls={this.props.allUrls} hasSubComponent/>
                  </div>
                </div>
              </div>
            )
          }
        </div>
        )}
        {activeGroup === "marfps" && (<div className={`accordion ${this.state.isMarfpOpen && "is-active"}`}>
          <div className="accordion-header" onClick={() => { this.handleToggle("isMarfpOpen") }}>
            <span>MA-RFP</span>
            <div>
              {
                this.state.isMarfpOpen
                  ? (<i className="fas fa-chevron-up" />)
                  : (<i className="fas fa-chevron-down" />)
              }
            </div>
          </div>
          {
            this.state.isMarfpOpen && (
              <div className="accordion-body">
                <div className="accordion-content">
                  <div>
                    <MarfpResultSet data={res} allUrls={this.props.allUrls} hasSubComponent/>
                  </div>
                </div>
              </div>
            )
          }
        </div>
        )}
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  allUrls: state.urls.allurls
})

export default connect(mapStateToProps)(SearchResultItems)
