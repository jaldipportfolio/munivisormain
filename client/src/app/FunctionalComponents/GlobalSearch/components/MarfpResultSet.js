import React from "react"
import moment from "moment"
import { Link } from "react-router-dom"
import ReactTable from "react-table"

import dateFormat from "../../../../globalutilities/dateFormat"

import MafpTranGraph from "./tranGraphs/MaRfpTranGraph"

const MarfpResultSet = ({ data, allUrls, hasSubComponent }) => {
  const others = {}

  if (hasSubComponent) {
    others.SubComponent = row => (
      <MafpTranGraph row={row.original} allUrls={allUrls} />
    )
  }
  return (
    <ReactTable
      data={data}
      columns={[
        {
          id: "issuer",
          Header: "Issuer Name",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd wrap-cell-text">
                <Link to={(allUrls[item.actIssuerClient] || {}).entity || ""} dangerouslySetInnerHTML={{ __html: (item.actIssuerClientEntityName || "-") }} />

              </div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => (a.actIssuerClientEntityName || "").localeCompare((b.actIssuerClientEntityName || ""))
        },
        {
          id: "issueDescription",
          Header: "Activity Description/Name",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd wrap-cell-text">
                <Link to={(allUrls[item._id] || {}).summary || ""} dangerouslySetInnerHTML={{ __html: (item.actProjectName || "-") }} />

              </div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => (a.actProjectName || "").localeCompare((b.actProjectName || ""))
        },
        {
          id: "type",
          Header: "Type",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (item.actType || "-") }} />
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => (a.actType || "").localeCompare((b.actType || ""))
        },
        {
          id: "leadManager",
          Header: "Lead Manager",
          accessor: item => item,
          Cell: row => { // eslint-disable-line
            const item = row.value
            return (
              <div className="hpTablesTd wrap-cell-text">
                <Link to={allUrls[item.actLeadFinAdvClientEntityId].users}>{item.actLeadFinAdvClientEntityName || "-"}</Link>
              </div>
            )
          },
          maxWidth: 150,
          sortMethod: (a, b) => (a.actLeadFinAdvClientEntityName || "").localeCompare((b.actLeadFinAdvClientEntityName || ""))
        },
        {
          id: "purposeSector",
          Header: "Purpose/Sector",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd wrap-cell-text">
                <p dangerouslySetInnerHTML={{ __html: (item.actPrimarySector || "-") }} />


              </div>
            )
          },
          sortMethod: (a, b) => (a.actPrimarySector || "").localeCompare(b.actPrimarySector || ""),
          maxWidth: 150
        },
        {
          id: "dueDate",
          Header: "Due Date",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd wrap-cell-text">
                {dateFormat(
                  item.actExpAwaredDate
                )}
              </div>
            )
          },
          sortMethod: (a, b) => {
            a = moment(
              a.actExpAwaredDate
            )
            b = moment(
              b.actExpAwaredDate
            )
            return a.diff(b)
          },
          maxWidth: 200
        },
        {
          id: "status",
          Header: "Status",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (item.actStatus || "-") }} />
            )
          },
          sortMethod: (a, b) => (a.actStatus || "").localeCompare((b.actStatus || "")),
          maxWidth: 250
        },
      ]}
      showPagination={false}
      // PaginationComponent={Pagination}
      pageSize={data.length}
      minRows={2}
      className="-striped -highlight is-bordered"
      {...others}
    />
  )
}

export default MarfpResultSet
