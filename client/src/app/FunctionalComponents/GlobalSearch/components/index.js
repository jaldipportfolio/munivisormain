/* eslint-disable jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events, no-underscore-dangle */

import React from "react"
import { connect } from "react-redux"
import styled from "styled-components"
import axios from "axios"
import _ from "lodash"
import { ReactiveBase, ReactiveList } from "@appbaseio/reactivesearch"
import {
  ELASTIC_SEARCH_INDEX,
  ELASTIC_SEARCH_URL,
  ELASTIC_SEARCH_CREDENTIALS,
  ELASTIC_SEARCH_TYPES
} from "../../../../constants"
import { muniApiBaseURL } from "../../../../globalutilities/consts"
import { fetchGlobalSearchPrefs } from "../../../StateManagement/actions"
import PageFilter from "./filters/PageFilter"
import NarrowBy from "./filters/NarrowByFilter"
import { getAllTransactions, getTransactionIds } from "../../Dashboard/helper"
import resultConfig from "../resultConfig"
import searchConfig from "../searchConfig"
import "./index.scss"
import Loader from "../../../GlobalComponents/Loader"
import ReactiveLoader from "../../../GlobalComponents/ReactiveLoader"
import PaginationDropDown from "../../Dashboard/components/PaginationDropDown"

export const TABS = {
  TRANSACTIONS: "TRANSACTIONS",
  ENTITIES: "ENTITIES",
  USERS: "USERS",
  DOCUMENTS: "DOCUMENTS"
}

const mapStateToProps = state => ({
  auth: state.auth,
  searchPref: state.globalSearchPref[state.selectedGlobalPref]
})

const mapDispatchToProps = dispatch => ({
  fetchPrefs: () => {
    dispatch(fetchGlobalSearchPrefs())
  }
})

const Base = styled(ReactiveBase)`
  padding: 0px;
  font-family: "GoogleSans" !important;
`

class GlobalSearch extends React.Component {
  state = {
    isFetching: true,
    isNarrowByOpen: true,
    entitlements: {},
    pageSize: 50
  }

  componentDidMount() {
    this.props.fetchPrefs()
    this.fetchEntitledList()
  }

  handlePageChange = pageSize => {
    this.setState({
      pageSize
    })
  }

  fetchEntitledList = () => {
    // const { auth } = this.props
    // const loggedInUserId = auth.userEntities.userId
    axios({
      method: "GET",
      url: `${muniApiBaseURL}entitlements/all`,
      headers: { Authorization: this.props.auth.token }
    }).then(res => {
      const { data } = res.data

      this.setState({
        isFetching: false,
        entitlements: data
      })
    })
  }

  handleNarrowByChange = () => {
    this.setState({
      isNarrowByOpen: !this.state.isNarrowByOpen
    })
  }

  render() {
    const { isFetching, entitlements, isNarrowByOpen, pageSize } = this.state
    const { searchPref } = this.props
    const type = (searchPref || {}).typeQuery || "transactions"
    const rConfig = resultConfig[type]
    const sConfig = searchConfig[type]

    const entitledIds =
      type === "transactions" || type === "documents"
        ? getTransactionIds(
          getAllTransactions(
            _.pick(entitlements, [
              "deals",
              "marfps",
              "derivatives",
              "rfps",
              "bankloans",
              "others",
              "busdev"
            ])
          )
        )
        : type === "entities"
          ? ((entitlements || {}).entities || {}).all || []
          : type === "tasks"
            ? [
              ...(((entitlements || {}).users || {}).edit || []),
              ...(((entitlements || {}).entities || {}).edit || [])
            ]
            : ((entitlements || {}).users || {}).all || []

    const entitilementKey =
      type === "transactions" || type === "entities" || type === "users"
        ? "_id"
        : type === "documents"
          ? "tranId.raw"
          : "taskDetails.taskAssigneeUserId.raw"

    let searchTypes = sConfig.index
    let activeGroup = null
    if (type === "transactions") {
      searchTypes =
        (searchPref || {}).tranTypeQuery ||
        `${ELASTIC_SEARCH_TYPES.DEALS},${ELASTIC_SEARCH_TYPES.BANK_LOAN},${
          ELASTIC_SEARCH_TYPES.DERIVITIVE
        }`

      if (
        searchTypes ===
        `${ELASTIC_SEARCH_TYPES.DEALS},${ELASTIC_SEARCH_TYPES.BANK_LOAN},${
          ELASTIC_SEARCH_TYPES.DERIVITIVE
        }`
      ) {
        activeGroup = "deals"
      }

      if (
        searchTypes ===
        `${ELASTIC_SEARCH_TYPES.RFPS},${ELASTIC_SEARCH_TYPES.OTHERS},${
          ELASTIC_SEARCH_TYPES.BUSINESSDEVELOPMENT
        }`
      ) {
        activeGroup = "rfps"
      }

      if (searchTypes === `${ELASTIC_SEARCH_TYPES.MARFPS}`) {
        activeGroup = "marfps"
      }
    }
    return (
      <Base
        app={ELASTIC_SEARCH_INDEX}
        url={ELASTIC_SEARCH_URL}
        credentials={ELASTIC_SEARCH_CREDENTIALS}
        type={searchTypes}
      >
        {isFetching ? (
          <Loader />
        ) : (
          <div id="main" className="tool-globalsearch">
            <PageFilter filterType={type} />
            <br />
            <div className="switchContainer">
              <span>Filters&nbsp;</span>
              <label className="customswitch">
                <input
                  type="checkbox"
                  onChange={this.handleNarrowByChange}
                  checked={isNarrowByOpen}
                />
                <span className="customslider round" />
              </label>
            </div>
            <div className="columns">
              {isNarrowByOpen && (
                <div className="column is-3">
                  <NarrowBy handleNarrowByChange={this.handleNarrowByChange} />
                </div>
              )}
              <div
                className="column overflow-auto"
                style={{ position: "relative" }}
              >
                <div className="dashPaginationRight">
                  <PaginationDropDown
                    defaultValue={pageSize}
                    handlePageChange={this.handlePageChange}
                  />
                </div>
                <ReactiveList
                  innerClass={{
                    noResults: "tools-globalsearch-no-results",
                    resultsInfo: "tool-globalsearch-results-info",
                    pagination: "tool-globalsearch-pagination"
                  }}
                  componentId="ResultCard"
                  dataField="title"
                  size={pageSize}
                  pagination
                  paginationAt="both"
                  loader={<ReactiveLoader />}
                  defaultQuery={() => {
                    if (!entitledIds.length) {
                      return {
                        match_none: {}
                      }
                    }
                    return {
                      [entitilementKey === "_id" ? "ids" : "terms"]: {
                        [entitilementKey === "_id"
                          ? "values"
                          : entitilementKey]: entitledIds
                      }
                    }
                  }}
                  renderAllData={res =>
                    Boolean(res.length) && rConfig.resultSet(res, activeGroup)
                  }
                  react={{
                    and: rConfig.react
                  }}
                  showResultStats
                  onResultStats={(total, time) => (
                    <h3>
                      Found {total} records in {time} ms
                    </h3>
                  )}
                />
              </div>
            </div>
          </div>
        )}
      </Base>
    )
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GlobalSearch)
