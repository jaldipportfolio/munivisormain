/* eslint-disable jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events, no-underscore-dangle */

import React from "react"
import { DataSearch } from "@appbaseio/reactivesearch"
import { connect } from "react-redux"
import {
  deleteGlobalSearchPref,
  saveGlobalSearchFilter,
  setSelectedGlobalSearchPref,
  resetGlobalSearch
} from "../../../../StateManagement/actions"
import TypeFilter from "./TypeFilter"
import TranTypeFilter from "./TranTypeFilter"
import searchConfig from "../../searchConfig"
import { ELASTIC_SEARCH_TYPES } from "../../../../../constants"

const mapStateToProps = state => {
  const { globalSearchPref, selectedGlobalPref, searchPref } = state

  return {
    globalSearchPref,
    selectedGlobalPref,
    searchPref,
    spref: state.globalSearchPref[state.selectedGlobalPref]
  }
}

const mapDispatchToProps = dispatch => ({
  saveFilter: name => {
    dispatch(saveGlobalSearchFilter(name))
  },
  setSelectedPref: key => {
    dispatch(setSelectedGlobalSearchPref(key))
  },
  deletePref: () => {
    dispatch(deleteGlobalSearchPref())
  },
  reset: () => {
    dispatch(resetGlobalSearch())
  }
})

class PageFilter extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      pageFilterToggle: true,
      filterName: ""
    }
  }

  handlePageFilterToggle = () => {
    this.setState({
      pageFilterToggle: !this.state.pageFilterToggle
    })
  }

  handleFilterNameChange = e => {
    this.setState({
      filterName: e.target.value
    })
  }

  saveFilter = () => {
    const { globalSearchPref, selectedGlobalPref } = this.props
    const filterName =
      this.state.filterName.trim() ||
      globalSearchPref[selectedGlobalPref].filterName ||
      ""
    if (filterName) {
      this.props.saveFilter(filterName)
      this.setState({
        filterName: ""
      })
    }
  }

  handlePrefChange = e => {
    this.props.setSelectedPref(e.target.value)
  }

  render() {
    const { pageFilterToggle, filterName } = this.state
    const {
      globalSearchPref,
      deletePref,
      searchPref,
      selectedGlobalPref,
      spref,
      reset
    } = this.props
    const { isFetching, isSaving, isDeleting } = searchPref
    const type = (spref || {}).typeQuery || "transactions"
    const sConfig = searchConfig[type]
    const { searchFields, fieldWeights } = sConfig.fieldsAndWeigts
    const defaultTranType = (spref || {}).tranTypeQuery || `${ELASTIC_SEARCH_TYPES.DEALS},${ELASTIC_SEARCH_TYPES.BANK_LOAN},${ELASTIC_SEARCH_TYPES.DERIVITIVE}`
    return (
      <div className="accordions">
        <div className="accordion is-active">
          <div
            className="accordion-header"
            onClick={this.handlePageFilterToggle}
          >
            <p>Page filters</p>
            {pageFilterToggle ? (
              <i
                className="fas fa-chevron-up"
                style={{ cursor: "pointer" }}
                onClick={this.handlePageFilterToggle}
              />
            ) : (
              <i
                className="fas fa-chevron-down"
                style={{ cursor: "pointer" }}
                onClick={this.handlePageFilterToggle}
              />
            )}
          </div>
          {pageFilterToggle &&
            (isFetching ? (
              <div> Fetching Preferences... </div>
            ) : (
              <div className="accordion-body">
                <div className="accordion-content">
                  <div className="columns">
                    <div className="column">
                      <div className="field has-addons">
                        <div className="select is-fullwidth is-link is-small">
                          <select
                            onChange={this.handlePrefChange}
                            value={selectedGlobalPref}
                          >
                            <option value="0">Select Saved Preferences</option>
                            {Object.keys(globalSearchPref).map(key => {
                              if (key !== "0") {
                                return (
                                  <option key={key} value={key}>
                                    {globalSearchPref[key].filterName}
                                  </option>
                                )
                              }
                              return null
                            })}
                          </select>
                        </div>

                        <div className="control">
                          <button
                            className="button is-dark is-small"
                            onClick={deletePref}
                            disabled={isDeleting}
                          >
                            Delete
                          </button>
                        </div>
                      </div>
                    </div>
                    <div className="column">
                      <div className="field has-addons">
                        <input
                          className="input is-fullwidth is-link is-small"
                          type="text"
                          placeholder="Name your search to save it"
                          onChange={this.handleFilterNameChange}
                          value={filterName}
                        />

                        <div className="control">
                          <button
                            className="button is-link is-small"
                            onClick={this.saveFilter}
                            disabled={isSaving}
                          >
                            {isSaving ? "Saving ..." : "Save"}
                          </button>
                        </div>
                      </div>
                    </div>
                    <div className="column">
                      <div className="searchContainer">
                        <DataSearch
                          componentId="mainSearch"
                          dataField={searchFields}
                          fieldWeights={fieldWeights}
                          autosuggest={false}
                          iconPosition="left"
                          queryFormat="and"
                          placeholder="Enter search phrases"
                          showIcon={false}
                          className="searchbox"
                          highlight
                          innerClass={{
                            input: "input customInput is-link"
                          }}
                        />
                        <span className="icon is-left searchIcon">
                          <i className="fas fa-search" />
                        </span>
                      </div>
                    </div>
                    <div className="column">
                      <TypeFilter defaultValue={type} />
                    </div>
                    <div className="column">
                      {
                        type === "transactions" && (
                          <TranTypeFilter defaultValue={defaultTranType}/>
                        )
                      }
                    </div>
                    <div className="column">
                      <button className="button is-small is-dark" type="button" onClick={reset}>Reset</button>
                    </div>
                  </div>
                </div>
              </div>
            ))}
        </div>
      </div>
    )
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PageFilter)
