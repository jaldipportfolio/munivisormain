import React from "react"
import styled from "styled-components"
import { getPicklistValues } from "GlobalUtils/helpers"

const FilterContainer = styled.div`
  display: flex;
`

const prepareQuery = (value, queryType = "must") => {
  let query = {}

  if (value) {
    query = {
      bool: {
        [queryType]: {
          bool: {
            should: [
              {
                match: {
                  "dealIssueTranSecondarySector.raw": value
                }
              },
              {
                match: {
                  "rfpTranSecondarySector.raw": value
                }
              },
              {
                match: {
                  "actTranSecondarySector.raw": value
                }
              },
              {
                match: {
                  "actTranSecondarySector.raw": value
                }
              },
              {
                match: {
                  "actSecondarySector.raw": value
                }
              }
            ]
          }
        }
      }
    }
  }


  return query
}

class SecondarySectorFilter extends React.Component {

  constructor() {
    super()

    this.state = {
      options: [],
      selectedPrimarySector: ""
    }
  }

  componentDidMount() {
    this.getOptions()
  }

  componentDidUpdate(prevProps) {
    const oldPref = { ...prevProps.pref }
    const pref = { ...this.props.pref }

    if (JSON.stringify((pref.secondarySectorFilter || {})) !== JSON.stringify((oldPref.secondarySectorFilter || {}))) {
      this.props.setQuery({
        query: (pref.secondarySectorFilter || {}).query || {},
        value: (pref.secondarySectorFilter || {}).value || ""
      })
    }

    if (pref.primarySectorFilter) {
      this.setSelectedPrimarySector(pref.primarySectorFilter.value)
    }
  }

  componentWillUnmount() {
    this.isCancelled = true
  }

  setSelectedPrimarySector = (selectedPrimarySector) => {
    if (this.state.selectedPrimarySector !== selectedPrimarySector) {
      this.setState({
        selectedPrimarySector
      }, () => {
        this.getOptions()
      })
    }
  }

  getOptions = async () => {
    const { pref } = this.props
    const primaryValue = this.state.selectedPrimarySector || ""

    const [primarySectors] = await getPicklistValues(["LKUPPRIMARYSECTOR"])
    let secondaryOptions = []

    if (primaryValue) {
      secondaryOptions = primarySectors[2][primaryValue]
    } else {
      const secondarySectors = (primarySectors ? primarySectors[1] : []).map(item => primarySectors[2][item])
      secondaryOptions = secondarySectors.reduce((secotrs, item) => [...secotrs, ...item], [])
    }
    if (!this.isCancelled) {
      this.setState({
        options: secondaryOptions
      })
    }
  }

  handleQueryChange = (e) => {
    const { target: { value } } = e
    const pref = { ...this.props.pref }

    const secondarySectorFilter = pref.secondarySectorFilter || {}
    secondarySectorFilter.queryType = secondarySectorFilter.queryType || "must"
    secondarySectorFilter.value = value
    secondarySectorFilter.query = prepareQuery(secondarySectorFilter.value, secondarySectorFilter.queryType)

    this.props.setQuery({
      query: secondarySectorFilter.query,
      value: secondarySectorFilter.value,
    })

    this.props.handleFilterChange("secondarySectorFilter", secondarySectorFilter)
  }

  handleQueryTypeChange = (e) => {
    const { target: { value } } = e
    const pref = { ...this.props.pref }

    const secondarySectorFilter = pref.secondarySectorFilter || {}
    secondarySectorFilter.queryType = value
    secondarySectorFilter.value = secondarySectorFilter.value || ""
    secondarySectorFilter.query = prepareQuery(secondarySectorFilter.value, secondarySectorFilter.queryType)

    this.props.setQuery({
      query: secondarySectorFilter.query,
      value: secondarySectorFilter.value,
    })

    this.props.handleFilterChange("secondarySectorFilter", secondarySectorFilter)
  }

  render() {
    const { pref } = this.props
    return (
      <FilterContainer>
        <div className="select is-link is-small" style={{ marginRight: "10px" }}>
          <select onChange={this.handleQueryTypeChange} value={((pref || {}).secondarySectorFilter || {}).queryType || "must"}>
            <option value="must">Is</option>
            <option value="must_not">Is not</option>
          </select>
        </div>
        <div className="select is-fullwidth is-link is-small">
          <select onChange={this.handleQueryChange} value={((pref || {}).secondarySectorFilter || {}).value || ""}>
            <option value="">Secondary Sector</option>
            {
              this.state.options.map(state => (
                <option key={state} value={state}>{state}</option>
              ))
            }
          </select>
        </div>
      </FilterContainer>
    )
  }
}

export default SecondarySectorFilter
