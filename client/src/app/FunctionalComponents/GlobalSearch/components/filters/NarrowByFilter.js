import React from "react"
import styled from "styled-components"
import { connect } from "react-redux"
import { ReactiveComponent } from "@appbaseio/reactivesearch"
import { updateGlobalSearchPref } from "../../../../StateManagement/actions"
import DateFilter from "./DateFilter"
import ParAmountFilter from "./ParAmount"
import StateFilter from "./StateFilter"
import CountyFilter from "./CountyFilter"
import PrimarySectorFilter from "./PrimarySectoryFilter"
import SecondarySectorFilter from "./SecondarySectoryFilter"
import StatusFilter from "./StatusFilter"
import UseOfProceedsFilter from "./UseOfProceedsFilter"
import MarkerRoleFilter from "./MarketRoleFilter"
import EntitiesFilter from "./EntitiesFilter"
import UserStateFilter from "./UserStateFilter"
import TaskDueDateFilter from "./TaskDueDateFilter"
import TaskTypeFilter from "./TaskTypeFilter"
import DocTranTypeFilter from "./DocTranTypeFilter"
import DocTypeFilter from "./DocTypeFilter"


const mapStateToProps = state => {
  const { globalSearchPref, selectedGlobalPref } = state
  const searchPref = globalSearchPref[selectedGlobalPref]
  return ({
    globalSearchPref: searchPref,
    typeFilter: (searchPref || {}).typeQuery || "transactions"
  })
}

const mapDispatchToProps = dispatch => ({
  updatePrefs: (filter, data) => dispatch(updateGlobalSearchPref(filter, data))
})

const NarrowBy = ({ globalSearchPref, updatePrefs, typeFilter }) => (
  <div className="card">
    <header className="card-header hero is-link">
      <p className="card-header-title has-text-white" style={{ justifyContent: "center" }}>
        Narrow by
      </p>

    </header>
    <div className="card-content">
      <div>
        {
          typeFilter === "transactions" && (
            <div>
              <ReactiveComponent componentId="dateFilter">
                <DateFilter
                  handleFilterChange={updatePrefs}
                  pref={globalSearchPref}
                />
              </ReactiveComponent>
              <br />
              <ReactiveComponent componentId="parAmountFilter">
                <ParAmountFilter
                  handleFilterChange={updatePrefs}
                  pref={globalSearchPref}
                />
              </ReactiveComponent>
              <br />
              <ReactiveComponent componentId="countyFilter">
                <CountyFilter
                  handleFilterChange={updatePrefs}
                  pref={globalSearchPref}
                />
              </ReactiveComponent>
              <br />
              <ReactiveComponent componentId="stateFilter">
                <StateFilter
                  handleFilterChange={updatePrefs}
                  pref={globalSearchPref}
                />
              </ReactiveComponent>
              <br />
              <ReactiveComponent componentId="primarySectorFilter">
                <PrimarySectorFilter
                  handleFilterChange={updatePrefs}
                  pref={globalSearchPref}
                />
              </ReactiveComponent>
              <br />
              <ReactiveComponent componentId="secondarySectorFilter">
                <SecondarySectorFilter
                  handleFilterChange={updatePrefs}
                  pref={globalSearchPref}
                />
              </ReactiveComponent>
              <br />
              <ReactiveComponent componentId="statusFilter">
                <StatusFilter
                  handleFilterChange={updatePrefs}
                  pref={globalSearchPref}
                />
              </ReactiveComponent>
              <br />
              <ReactiveComponent componentId="useOfProceedsFilter">
                <UseOfProceedsFilter
                  handleFilterChange={updatePrefs}
                  pref={globalSearchPref}
                />
              </ReactiveComponent>
            </div>
          )
        }

        {
          typeFilter === "entities" && (
            <ReactiveComponent componentId="marketRole">
              <MarkerRoleFilter
                handleFilterChange={updatePrefs}
                pref={globalSearchPref}
              />
            </ReactiveComponent>
          )
        }

        {
          typeFilter === "users" && (
            <div>
              <ReactiveComponent componentId="entitiesFilter">
                <EntitiesFilter
                  handleFilterChange={updatePrefs}
                  pref={globalSearchPref}
                />
              </ReactiveComponent>
              <br />
              <ReactiveComponent componentId="userStateFilter">
                <UserStateFilter
                  handleFilterChange={updatePrefs}
                  pref={globalSearchPref}
                />
              </ReactiveComponent>
            </div>
          )
        }

        {
          typeFilter === "tasks" && (
            <div>
              <ReactiveComponent componentId="taskDueDateFilter">
                <TaskDueDateFilter
                  handleFilterChange={updatePrefs}
                  pref={globalSearchPref}
                />
              </ReactiveComponent>
              <br />
              <ReactiveComponent componentId="taskTypeFilter">
                <TaskTypeFilter
                  handleFilterChange={updatePrefs}
                  pref={globalSearchPref}
                />
              </ReactiveComponent>
            </div>
          )
        }

        {
          typeFilter === "documents" && (
            <div>
              <ReactiveComponent componentId="docTranTypeFilter">
                <DocTranTypeFilter
                  handleFilterChange={updatePrefs}
                  pref={globalSearchPref}
                />
              </ReactiveComponent>
              <br />
              <ReactiveComponent componentId="docTypeFilter">
                <DocTypeFilter
                  handleFilterChange={updatePrefs}
                  pref={globalSearchPref}
                />
              </ReactiveComponent>
            </div>
          )
        }
      </div>

    </div>
  </div>)

export default connect(mapStateToProps, mapDispatchToProps)(NarrowBy)
