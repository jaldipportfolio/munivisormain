import React from "react"
import styled from "styled-components"
import {getPicklistValues} from "GlobalUtils/helpers"

const FilterContainer = styled.div`
  display: flex;
`

const prepareQuery = (value, queryType="must") => {
  let query = {}

  if (value) {
    query = {
      bool: {
        [queryType]: {
          bool: {
            should: [
              {
                match: {
                  "entityFlags.marketRole.raw": value
                }
              }
            ]
          }
        }
      }
    }
  }


  return query
}

class MarketRole extends React.Component {

  constructor() {
    super()

    this.state = {
      options: []
    }
  }

  componentDidMount() {
    this.getOptions()
  }

  componentDidUpdate(prevProps) {
    const oldPref = { ...prevProps.pref }
    const pref = { ...this.props.pref }

    if (JSON.stringify((pref.marketRole || {})) !== JSON.stringify((oldPref.marketRole || {}))) {
      this.props.setQuery({
        query: (pref.marketRole || {}).query || {},
        value: (pref.marketRole || {}).value || ""
      })
    }
  }


  componentWillUnmount() {
    this.isCancelled = true
  }


  getOptions = async () => {
    const [marketRole] = await getPicklistValues(["LKUPPARTICIPANTTYPE"])

    if(!this.isCancelled) {
      this.setState({
        options: marketRole[1]
      })
    }
  }

  handleQueryChange = (e) => {
    const { target: { value }} = e
    const pref = {...this.props.pref}

    const marketRole = pref.marketRole || {}
    marketRole.queryType = marketRole.queryType || "must"
    marketRole.value = value
    marketRole.query = prepareQuery(marketRole.value, marketRole.queryType)

    this.props.setQuery({
      query: marketRole.query,
      value: marketRole.value,
    })

    this.props.handleFilterChange("marketRole", marketRole)
  }

  handleQueryTypeChange = (e) => {
    const { target: { value }} = e
    const pref = {...this.props.pref}

    const marketRole = pref.marketRole || {}
    marketRole.queryType = value
    marketRole.value = marketRole.value || ""
    marketRole.query = prepareQuery(marketRole.value, marketRole.queryType)

    this.props.setQuery({
      query: marketRole.query,
      value: marketRole.value,
    })

    this.props.handleFilterChange("marketRole", marketRole)
  }

  render() {
    const { pref } = this.props
    return (
      <FilterContainer>
        <div className="select is-small is-link" style={{ marginRight: "10px"}}>
          <select onChange={this.handleQueryTypeChange} value={((pref || {}).marketRole || {}).queryType || "must"}>
            <option value="must">Is</option>
            <option value="must_not">Is not</option>
          </select>
        </div>
        <div className="select is-small is-link">
          <select onChange={this.handleQueryChange} style={{ maxWidth: "200px" }} value={((pref || {}).marketRole || {}).value || ""}>
            <option value="">Market Role</option>
            {
              this.state.options.map(state => (
                <option key={state} value={state}>{state}</option>
              ))
            }
          </select>
        </div>
      </FilterContainer>
    )
  }
}

export default MarketRole
