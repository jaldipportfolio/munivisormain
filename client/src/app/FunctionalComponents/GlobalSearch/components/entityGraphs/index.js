/* eslint-disable jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events, no-underscore-dangle, no-case-declarations, default-case */

import React from "react"
import axios from "axios"
import ReactTable from "react-table"
import styled from "styled-components"
import { Link } from "react-router-dom"
import { ELASTIC_SEARCH_TYPES, ELASTIC_SEARCH_URL, ELASTIC_SEARCH_INDEX, ELASTIC_SEARCH_CREDENTIALS } from "../../../../../constants"
import { numberWithCommas } from "../../../../../globalutilities/helpers"
import dateFormat from "../../../../../globalutilities/dateFormat"
import getParAmount from "../../../../../globalutilities/amountDecorator"
import { getHeaders } from "../../../../../globalutilities"

const Container = styled.div`
  padding: 20px;
`

const getLink = (item) => {
  if (item.dealIssueTranIssueName || item.dealIssueTranProjectDescription) return `/deals/${item._id}/summary`
  if (item.bankLoanParticipants) return `/loan/${item._id}/summary`
  if (item.derivativeSummary) return `/derivative/${item._id}/summary`
  if (item.maRfpParticipants) return `/marfp/${item._id}/summary`
  if (item.rfpParticipants) return `/rfp/${item._id}/distribute`
  return ""
}

const userColumns = [
  {
    id: "userFirstName",
    Header: "First Name",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd">
          <Link to={`/addnew-contact/${item._id}`}>{item.userFirstName || "-"}</Link>
        </div>
      )
    },
    maxWidth: 250,
    sortMethod: (a, b) => (a.userFirstName || "").localeCompare((b.userFirstName || ""))
  },
  {
    id: "userLastName",
    Header: "Last Name",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd">
          {item.userLastName || "-"}
        </div>
      )
    },
    maxWidth: 250,
    sortMethod: (a, b) => (a.userLastName || "").localeCompare((b.userLastName || ""))
  },
  {
    id: "phone",
    Header: "Phone Number",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      const phones = item.userPhone.filter(i => i.phonePrimary === true)[0] || {}
      return (
        <div className="hpTablesTd">
          {phones.phoneNumber || "-"}
        </div>
      )
    },
    maxWidth: 250,
    sortMethod: (a, b) => {
      const aPhone = a.userPhone.filter(i => i.phonePrimary === true)[0] || {}
      const bPhone = b.userPhone.filter(i => i.phonePrimary === true)[0] || {}
      return (aPhone.phoneNumber || "").localeCompare((bPhone.phoneNumber || ""))
    }
  },
  {
    id: "email",
    Header: "Email",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      const email = item.userEmails.filter(i => i.emailPrimary === true)[0] || {}
      return (
        <div className="hpTablesTd">
          {email.emailId || "-"}
        </div>
      )
    },
    maxWidth: 250,
    sortMethod: (a, b) => {
      const aemail = a.userEmails.filter(i => i.emailPrimary === true)[0] || {}
      const bemail = b.userEmails.filter(i => i.emailPrimary === true)[0] || {}
      return (aemail.emailId || "").localeCompare((bemail.emailId || ""))
    }
  },
]

const transactionColumns = [
  {
    id: "issuerName",
    Header: "Issuer Name",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd">
          <Link to={`/clients-propects/${item.dealIssueTranIssuerId || item.actTranClientId || item.rfpTranClientId || item.actIssuerClient}/entity`}>
            {item.dealIssueTranIssuerFirmName || item.actTranClientName || item.rfpTranIssuerFirmName || item.actIssuerClientEntityName || "-"}
          </Link>
        </div>
      )
    },
    maxWidth: 250,
  },
  {
    id: "actType",
    Header: "Activity Type",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd">
          {item.dealIssueTranType || item.rfpTranType || item.actType || (`${item.actTranType} / ${item.actTranSubType}`)}
        </div>
      )
    },
    maxWidth: 250,
  },
  {
    id: "projectDescription",
    Header: "Project Description",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <Link to={getLink(item)}>
          {(item.dealIssueTranIssueName || item.dealIssueTranProjectDescription) || (item.actTranIssueName || item.actTranProjectDescription) || item.actProjectName || item.rfpTranProjectDescription}
        </Link>
      )
    },
    maxWidth: 250,
  },
  {
    id: "status",
    Header: "Transaction Status",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd">
          {
            item.dealIssueTranStatus ||
            (item.bankLoanSummary || {}).actTranStatus ||
            (item.derivativeSummary || {}).tranStatus ||
            item.rfpTranStatus || item.actStatus
          }
        </div>
      )
    },
    maxWidth: 250,
  },
  {
    id: "amount",
    Header: ()=> (<span title="Principal Amount($)">Principal Amount($)</span>),
    accessor: item => item,
    Cell: row => {
      const item = row.value
      const amt = getParAmount(item)
      return (
        <div className="hpTablesTd">
          {numberWithCommas(amt > 0 ? amt : "-" )}
        </div>
      )
    },
    maxWidth: 250,
  },
  {
    id: "createdAt",
    Header: "Created At",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd">
          {
            dateFormat(item.rfpTranStartDate ||
            item.dealIssuePricingDate ||
            (item.bankLoanSummary || {}).actTranClosingDate ||
            (item.derivativeSummary || {}).tranTradeDate)
          }
        </div>
      )
    },
    maxWidth: 250,
  },
]

class EntityGraph extends React.Component {
  state = {
    isFetching: false,
    users: [],
    transactions: []
  }

  componentDidMount() {
    this.fetchUsers()
  }

  fetchUsers = async () => {
    this.setState({
      isFetching: true
    })
    const { row } = this.props

    const tranRes = await axios.post(`${ELASTIC_SEARCH_URL}${ELASTIC_SEARCH_INDEX}/${ELASTIC_SEARCH_TYPES.BANK_LOAN},${ELASTIC_SEARCH_TYPES.MARFPS},${ELASTIC_SEARCH_TYPES.DEALS},${ELASTIC_SEARCH_TYPES.RFPS},${ELASTIC_SEARCH_TYPES.DERIVITIVE}/_search`, {
      "from" : 0,
      "size" : 1000,
      "query": {
        "bool": {
          "should": [
            { match: { "dealIssueParticipants.dealPartFirmId.raw": row._id }},
            { match: { "dealIssueTranIssuerId.raw": row._id }},
            { match: { "rfpTranClientId.raw": row._id }},
            { match: { "actTranClientId.raw": row._id }},
            { match: { "bankLoanParticipants.partFirmId.raw": row._id }},
            { match: { "actIssuerClient.raw": row._id }},
            { match: { "derivativeParticipants.partFirmId.raw": row._id }}
          ]
        }
      }
    }, {headers: getHeaders()})

    const tranData = await tranRes.data.hits.hits

    const userRes = await axios.post(`${ELASTIC_SEARCH_URL}${ELASTIC_SEARCH_INDEX}/entityusers/_search`, {
      "from" : 0,
      "size" : 1000,
      "query": {
        "bool": {
          "should": [
            { match: { "entityId.raw": row._id }},
          ]
        }
      }
    }, {headers: getHeaders()})

    const userData = await userRes.data.hits.hits

    this.setState({
      transactions: tranData.map(item => ({ _id: item._id, ...item._source})),
      users: userData.map(item => ({ _id: item._id, ...item._source})),
      isFetching: false
    })

  }

  render() {
    const { isFetching, transactions, users } = this.state
    return (
      <Container>
        {
          isFetching
            ? <div>Fetching data ....</div>
            : (
              <div>
                <b> Associated users </b>
                <ReactTable
                  data={users}
                  columns={userColumns}
                  style={{
                    maxHeight: "300px"
                  }}
                  minRows={2}
                  pageSize={(users || []).length}
                  showPagination={false}
                />
                <br/>
                <b> Associated Transactions </b>
                <ReactTable
                  data={transactions}
                  columns={transactionColumns}
                  minRows={2}
                  style={{
                    maxHeight: "300px"
                  }}
                  pageSize={(transactions || []).length}
                  showPagination={false}
                  sortable={false}
                />
              </div>
            )
        }
      </Container>
    )
  }
}

export default EntityGraph
