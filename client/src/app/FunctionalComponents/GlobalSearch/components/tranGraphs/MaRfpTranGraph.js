import React from "react"
import styled from "styled-components"
import ReactTable from "react-table"
import {Link} from "react-router-dom"
import { appendTranIds } from "./DealsTranGraph"

const Container = styled.div`
  padding: 20px;
`
const documents = [
  {
    id: "fileName",
    Header: "File Name",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd">
          <Link to={item.docLink}>{item.docFileName || "-"}</Link>
        </div>
      )
    },
    maxWidth: 250,
    sortMethod: (a, b) => (a.docFileName || "").localeCompare((b.docFileName || ""))
  },
  {
    id: "docType",
    Header: "Document Type",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd">
          {item.docType || "-"}
        </div>
      )
    },
    maxWidth: 250,
    sortMethod: (a, b) => (a.docType || "").localeCompare((b.docType || ""))
  },
  {
    id: "docCategory",
    Header: "Category",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd">
          {item.docCategory || "-"}
        </div>
      )
    },
    maxWidth: 250,
    sortMethod: (a, b) => (a.docCategory || "").localeCompare((b.docCategory || ""))
  },
  {
    id: "docSubCategory",
    Header: "Sub Category",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd">
          {item.docSubCategory || "-"}
        </div>
      )
    },
    maxWidth: 250,
    sortMethod: (a, b) => (a.docSubCategory || "").localeCompare((b.docSubCategory || ""))
  }
]

const TranGraph = ({ row, allUrls }) => (
  <Container>
    <b> Associated Participants </b>
    <ReactTable
      data={row.maRfpParticipants || []}
      columns={[
        {
          id: "participantName",
          Header: "Name",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd">
                <Link to={allUrls[item.partContactId].users || ""} dangerouslySetInnerHTML={{ __html: (item.partContactName ||"-") }} />
              </div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => (a.partContactName || "").localeCompare((b.partContactName || ""))
        },
        {
          id: "participantFirmName",
          Header: "Firm Name",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd">
                <Link to={allUrls[item.partFirmId].entity || allUrls[item.partFirmId].firms || ""}>{item.partFirmName || "-"}</Link>
              </div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => (a.partFirmName || "").localeCompare((b.partFirmName || ""))
        },
        {
          id: "participantType",
          Header: "Type",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd">
                {item.partType || "-"}
              </div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => (a.partType ||"").localeCompare((b.partType || ""))
        },
        {
          id: "participantState",
          Header: "State",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd">
                {item.participantState || "-"}
              </div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => (a.participantState || "").localeCompare((b.participantState || ""))
        },
      ]}
      minRows={2}
      pageSize={(row.maRfpParticipants || []).legnth}
      showPagination={false}
      style={{
        maxHeight: "300px"
      }}
    />
    <br/>
    <b> Associated Documents </b>
    <ReactTable
      data={appendTranIds(row.maRfpDocuments || [], row, allUrls)}
      columns={documents}
      minRows={2}
      pageSize={(appendTranIds(row.maRfpDocuments || [], row, allUrls)).legnth}
      style={{
        maxHeight: "300px"
      }}
      showPagination={false}
    />
  </Container>
)

export default TranGraph
