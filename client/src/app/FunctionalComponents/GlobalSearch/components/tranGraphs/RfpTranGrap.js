import React from "react"
import styled from "styled-components"
import { Link } from "react-router-dom"
import ReactTable from "react-table"
import { appendTranIds } from "./DealsTranGraph"

const Container = styled.div`
  padding: 20px;
`

const getEvalMembers = (row) => {
  let members = []

  if (row.rfpMemberEvaluations && row.rfpMemberEvaluations.length) {
    members = row.rfpMemberEvaluations
  }

  if (row.rfpEvaluationTeam && row.rfpEvaluationTeam.length) {
    members = row.rfpEvaluationTeam
  }
  return members
}
const TranGraph = ({ row, allUrls }) => (
  <Container>
    <b> Associated Process Contacts </b>
    <ReactTable
      data={row.rfpProcessContacts || []}
      columns={[
        {
          id: "participantName",
          Header: "Name",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd">
                <Link to={allUrls[item.rfpProcessContactId].users || ""}>{item.rfpContactName || item.rfpSelEvalContactName || "-"}</Link>
              </div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => (a.rfpContactName || "").localeCompare((b.rfpContactName || ""))
        },
        {
          id: "contactEmail",
          Header: "Contact Email",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd">
                {item.rfpContactEmail || "-"}
              </div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => (a.rfpContactEmail || "").localeCompare((b.rfpContactEmail || ""))
        },
        {
          id: "rfpContactPhone",
          Header: "Contact Phone",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd">
                {item.rfpContactPhone || "-"}
              </div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => (a.rfpContactPhone ||"").localeCompare((b.rfpContactPhone || ""))
        }
      ]}
      minRows={2}
      pageSize={(row.rfpProcessContacts || []).length}
      showPagination={false}
      style={{
        maxHeight: "300px"
      }}
    />
    <br/>
    <b> Associated Participants </b>
    <ReactTable
      data={row.rfpParticipants || row.participants || []}
      columns={[
        {
          id: "participantName",
          Header: "Name",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd">
                <Link to={allUrls[item.rfpParticipantContactId || item.partContactId].users || allUrls[item.rfpParticipantContactId || item.partContactId].thirdparties || ""}>{item.rfpParticipantContactName || item.partContactName || "-"}</Link>
              </div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => (a.rfpParticipantContactName || a.partContactName || "").localeCompare((b.rfpParticipantContactName || b.partContactName || ""))
        },
        {
          id: "participantFirmName",
          Header: "Firm Name",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd">
                <Link to={allUrls[(item.rfpParticipantFirmId || item.partFirmId)].entity || allUrls[item.rfpParticipantFirmId || item.partFirmId].firms || ""}>{item.rfpParticipantFirmName || item.partFirmName || "-"}</Link>
              </div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => (a.rfpParticipantFirmName || "").localeCompare((b.rfpParticipantFirmName || ""))
        },
        {
          id: "participantType",
          Header: "Type",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd">
                {item.rfpParticipantRealMSRBType || item.partType || "-"}
              </div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => (a.rfpParticipantRealMSRBType || a.partType || "").localeCompare((b.rfpParticipantRealMSRBType || b.partType || ""))
        }
      ]}
      minRows={2}
      pageSize={(row.rfpParticipants || []).length}
      showPagination={false}
      style={{
        maxHeight: "300px"
      }}
    />
    <br/>
    <b> Associated Evaluation Memebers </b>
    <ReactTable
      data={getEvalMembers(row)}
      columns={[
        {
          id: "participantName",
          Header: "Name",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd">
                <Link to={allUrls[item.rfpEvaluationCommitteeMemberId || item.rfpSelEvalContactId].users || ""}>{item.rfpCommitteeMemberFirstName || item.rfpSelEvalContactName || "-"}</Link>
              </div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => (a.rfpCommitteeMemberFirstName || a.rfpSelEvalContactName || "").localeCompare((b.rfpCommitteeMemberFirstName || b.rfpSelEvalContactName || ""))
        },
        {
          id: "participantFirmName",
          Header: "Firm Name",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd">
                <Link to={allUrls[item.rfpParticipantFirmId || item.rfpSelEvalFirmId].entity || ""}>{item.rfpParticipantFirmName || "-"}</Link>
              </div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => (a.rfpParticipantFirmName || "").localeCompare((b.rfpParticipantFirmName || ""))
        },
        {
          id: "participantType",
          Header: "Type",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd">
                {item.rfpParticipantMSRBType || "-"}
              </div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => (a.rfpParticipantMSRBType ||"").localeCompare((b.rfpParticipantMSRBType || ""))
        }
      ]}
      minRows={2}
      pageSize={getEvalMembers(row).length}
      showPagination={false}
      style={{
        maxHeight: "300px"
      }}
    />
    <br/>
    <b> Associated Documents </b>
    <ReactTable
      data={appendTranIds(row.rfpBidDocuments || row.actTranDocuments || [], row, allUrls)}
      columns={[
        {
          id: "fileName",
          Header: "File Name",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd">
                <Link to={item.docLink}>{item.docFileName || "-"}</Link>
              </div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => (a.docFileName || "").localeCompare((b.docFileName || ""))
        },
        {
          id: "docType",
          Header: "Document Type",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd">
                {item.docType || "-"}
              </div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => (a.docType || "").localeCompare((b.docType || ""))
        },
        {
          id: "docCategory",
          Header: "Category",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd">
                {item.docCategory || "-"}
              </div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => (a.docCategory || "").localeCompare((b.docCategory || ""))
        },
        {
          id: "docSubCategory",
          Header: "Sub Category",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd">
                {item.docSubCategory || "-"}
              </div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => (a.docSubCategory || "").localeCompare((b.docSubCategory || ""))
        }
      ]}
      minRows={2}
      pageSize={appendTranIds(row.rfpBidDocuments || row.actTranDocuments || [], row, allUrls).length}
      style={{
        maxHeight: "300px"
      }}
      showPagination={false}
    />
  </Container>
)

export default TranGraph
