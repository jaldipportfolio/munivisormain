/* eslint-disable jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events, no-underscore-dangle, no-case-declarations, default-case */

import React from "react"
import ReactTable from "react-table"
import { Link } from "react-router-dom"
import EntityGraph from "./entityGraphs"

const columns = [
  {
    id: "entityName",
    Header: "Entity Name",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd">
          <Link to={`/clients-propects/${item._id}/entity`} dangerouslySetInnerHTML={{__html: (item.firmName || "-")}} />
        </div>
      )
    },
    maxWidth: 250,
    sortMethod: (a, b) => (a.firmName || "").localeCompare((b.firmName || ""))
  },
  {
    id: "entityType",
    Header: "Entity Type",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd" dangerouslySetInnerHTML={{__html: (item.msrbRegistrantType || "-")}} />
      )
    },
    maxWidth: 250,
    sortMethod: (a, b) => (a.msrbRegistrantType || "").localeCompare((b.msrbRegistrantType || ""))
  },
  {
    id: "primarySector",
    Header: "Primary Sector",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd" dangerouslySetInnerHTML={{__html: (item.primarySectors || "-")}} />
      )
    },
    maxWidth: 250,
    sortMethod: (a, b) => (a.primarySectors || "").localeCompare((b.primarySectors || ""))
  },
  {
    id: "secondarySector",
    Header: "Secondary Sector",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd" dangerouslySetInnerHTML={{__html: (item.secondarySectors || "-")}} />
      )
    },
    maxWidth: 250,
    sortMethod: (a, b) => (a.secondarySectors || "").localeCompare((b.secondarySectors || ""))
  },
  {
    id: "marketRole",
    Header: "Entity Roles",
    accessor: item => item,
    Cell: row => {
      const item = row.value
      return (
        <div className="hpTablesTd" dangerouslySetInnerHTML={{__html: (((item.entityFlags || {}).marketRole || []).join(",") || "-")}} />
      )
    },
    maxWidth: 250,
    sortMethod: (a, b) => ((a.marketRole || []).join(",") || "").localeCompare(((b.marketRole || []).join(",") || ""))
  },
]

const EntityResultSet = ({ data }) => (
  <div style={{ marginTop: "20px"}}>
    <ReactTable
      data={data}
      pageSize={data.length}
      columns={columns}
      showPagination={false}
      style={{
        maxHeight: "400px"
      }}
      minRows={2}
      className="-striped -highlight is-bordered"
      SubComponent={
        (data) => (<EntityGraph row={data.original} />)
      }
    />
  </div>
)

export default EntityResultSet
