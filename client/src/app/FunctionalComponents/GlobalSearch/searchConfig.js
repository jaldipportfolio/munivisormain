import { ELASTIC_SEARCH_TYPES, DOCUMENT_TYPE, SEARCH_FIELDS } from "../../../constants"
import { getFieldsAndWeights as documentsFieldsAndWeights } from "../Document/searchConfig"

const tranFields = SEARCH_FIELDS

const entitiesFields = [
  {field:"addresses.addressName", weight:75},
  {field:"addresses.officeEmails", weight:75},
  {field:"addresses.state", weight:85},
  {field:"addresses.city", weight:85},
  {field:"entityFlags.marketRole", weight:95},
  {field:"entityFlags.issuerFlags", weight:95},
  {field:"entityLinkedCusips.associatedCusip6", weight:95},
  {field:"entityAliases", weight:80},
  {field:"firmName", weight:95},
  {field: "msrbRegistrantType", weight: 99},
  {field:"primarySectors", weight:95},
  {field:"secondarySectors", weight:95}
]

const userFields = [
  {field:"userFirmName", weight:95},
  {field:"userFlags", weight:85},
  {field:"userFirstName", weight:100},
  {field:"userLastName", weight:100},
  {field:"userEmails.emailId", weight:90},
]

const taskFields = [
  {field:"relatedActivityDetails.activityIssuerClientName", weight:95},
  {field:"relatedActivityDetails.activityProjectName", weight:85},
  {field:"relatedActivityDetails.activityType", weight:100},
  {field:"taskDetails.taskDescription", weight:100},
  {field:"taskDetails.taskAssigneeName", weight:90},
  {field:"taskDetails.taskStatus", weight: 85},
  {field:"taskDetails.taskType", weight: 100},
  {field: "relatedEntityDetails.entityName", weight: 95},
]

export const getFieldsAndWeights = (fieldList) => {
  const fields = fieldList.filter(f => f.weight >= 90 && f)
  const ones = fields.map(() => 1)

  const searchFields = [
    ...fields.map(f => f.field),
    ...fields.map(f => `${f.field}.raw`),
    ...fields.map(f => `${f.field}.searchable`),
    ...fields.map(f => `${f.field}.autosuggest`),
  ]

  const fieldWeights = [
    ...fields.map(f => parseFloat(((f.weight/100) * 3).toFixed(2))),
    ...fields.map(f => f.weight),
    ...ones,
    ...ones,
  ]

  return ({
    searchFields,
    fieldWeights
  })
}

export default {
  transactions: {
    index: `${ELASTIC_SEARCH_TYPES.DEALS},${ELASTIC_SEARCH_TYPES.RFPS},${ELASTIC_SEARCH_TYPES.MARFPS},${ELASTIC_SEARCH_TYPES.BANK_LOAN},${ELASTIC_SEARCH_TYPES.DERIVITIVE},${ELASTIC_SEARCH_TYPES.OTHERS},${ELASTIC_SEARCH_TYPES.BUSINESSDEVELOPMENT}`,
    fieldsAndWeigts: getFieldsAndWeights(tranFields)
  },
  entities: {
    index: "entities",
    fieldsAndWeigts: getFieldsAndWeights(entitiesFields)
  },
  users: {
    index: "entityusers",
    fieldsAndWeigts: getFieldsAndWeights(userFields)
  },
  tasks: {
    index: ELASTIC_SEARCH_TYPES.TASKS,
    fieldsAndWeigts: getFieldsAndWeights(taskFields)
  },
  documents: {
    index: DOCUMENT_TYPE,
    fieldsAndWeigts: documentsFieldsAndWeights(),
  }
}
