import React from "react"
import {SelectLabelInput, TextLabelInput} from "../../../../../GlobalComponents/TextViewBox"
import ThirdPartyLookup from "Global/ThirdPartyLookup"


const CounterParties = ({participant, index, isEditable, onItemChange, canEditTran, isSaveDisabled, onSave, dropDown, onCancel, onEdit, onRemove, errors = {}, onBlur, category, autoFocus, tabIndex}) => {

  const onChange = (event, name) => {
    if (name && name === "partType") {
      return onItemChange({
        ...participant,
        [name]: event.id,
      }, category, index)
    }
    return onItemChange({
      ...participant,
      [event.target.name]: event.target.type === "checkbox" ? event.target.checked : event.target.value || "",
    }, category, index)
  }

  const onPartChange = (firm) => {
    onItemChange({
      ...participant,
      cntrPartyFirmName: "",
    }, category, index)
  }

  const counterPartyName = {
    _id: "",
    firmName: participant.cntrPartyFirmName || "",
    participantType: "Governmental Entity / Issuer",
    isExisting: true
  }

  const onBlurInput = (event) => {
    if(event.target.title && event.target.value){
      onBlur(category, `${event.target.title || "empty"} change to ${event.target.type === "checkbox" ? event.target.checked : event.target.value || "empty"}`)
    }
  }

  const onSaveBtnClick = (e, category, participant, index) =>{
    if(e.keyCode === 13){
      onSave(category, participant, index)
    }
  }

  const onCancelBtnClick = (e, category) =>{
    if(e.keyCode === 13){
      onCancel(category)
    }
  }

  isEditable = (isEditable[category] === index)

  return (
    <tbody>
      <tr>
        <td>
          {
            isEditable ?
              /* <div className="select is-small is-link">
                <select title="Firm Type" value={participant.cntrPartyFirmtype} name="cntrPartyFirmtype" onChange={onChange} onBlur={onBlurInput} disabled={!isEditable || !canEditTran}>
                  <option value="" disabled="">Pick Firm Type</option>
                  {
                    dropDown.Firm && Object.keys(dropDown.Firm).map((key,i) => (<option key={i} value={key}>{key}</option>))
                  }
                </select>
                {errors.cntrPartyFirmtype && <p className="text-error">{errors.cntrPartyFirmtype}</p>}
              </div> */
              <SelectLabelInput
                autoFocus={true}
                tabIndex={tabIndex}
                error={errors.cntrPartyFirmtype || ""}
                list={dropDown && dropDown.firm || []}
                name="cntrPartyFirmtype"
                value={participant.cntrPartyFirmtype || ""}
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={!isEditable || !canEditTran}
              />
              : <small>{participant.cntrPartyFirmtype}</small>
          }
        </td>
        <td style={{ width: "300px" }}>
          {
            isEditable ?
              <div className="is-small is-link">
                {/* <select title="Firm Name" value={participant.cntrPartyFirmName} name="cntrPartyFirmName" onChange={onChange} onBlur={onBlurInput} disabled={!isEditable || !canEditTran}>
                  <option value="" disabled="">Pick Firm Name</option>
                  {
                    dropDown.Firm && dropDown.Firm[participant.cntrPartyFirmtype] ?
                      dropDown.Firm[participant.cntrPartyFirmtype].map((name,i)=> (
                        <option key={i} value={name}>{name}</option>
                      )) : null
                  }
                </select> */}
                <ThirdPartyLookup
                  tabIndex={tabIndex + 1}
                  entityName={counterPartyName}
                  onChange={(e) => onChange({ target: { name: "cntrPartyFirmName", value: e.firmName } })}
                  type="other"
                  style={{ fontSize: 12 }}
                  error={errors.cntrPartyFirmName || ""}
                  notEditable={!canEditTran || false}
                  isHide={participant.cntrPartyFirmName && canEditTran}
                />
              </div>
              : <small>{participant.cntrPartyFirmName}</small>
          }
        </td>
        <td>
          {
            isEditable ?
              <SelectLabelInput
                tabIndex={tabIndex + 2}
                title="Moody's"
                error={errors.cntrPartyMoodysRating || ""}
                list={dropDown.partRating && dropDown.partRating.Moodys}
                name="cntrPartyMoodysRating"
                value={participant.cntrPartyMoodysRating || ""}
                disabled={!isEditable || !canEditTran}
                onChange={onChange}
                onBlur={onBlurInput}
              />
              : <small>{participant.cntrPartyMoodysRating}</small>
          }
        </td>
        <td>
          {
            isEditable ?
              <SelectLabelInput
                title="Fitch"
                tabIndex={tabIndex + 3}
                error={errors.cntrPartyFitchRating || ""}
                list={dropDown.partRating && dropDown.partRating.Fitch}
                name="cntrPartyFitchRating"
                value={participant.cntrPartyFitchRating || ""}
                disabled={!isEditable || !canEditTran}
                onChange={onChange}
                onBlur={onBlurInput}
              />
              : <small>{participant.cntrPartyFitchRating}</small>
          }
        </td>
        <td>
          {
            isEditable ?
              <SelectLabelInput
                title="Kroll"
                tabIndex={tabIndex + 4}
                error={errors.cntrPartyKrollRating || ""}
                list={dropDown.partRating && dropDown.partRating.Kroll}
                name="cntrPartyKrollRating"
                value={participant.cntrPartyKrollRating || ""}
                disabled={!isEditable || !canEditTran}
                onChange={onChange}
                onBlur={onBlurInput}
              />
              : <small>{participant.cntrPartyKrollRating}</small>
          }
        </td>
        <td>
          {
            isEditable ?
              <SelectLabelInput
                title="Kroll"
                tabIndex={tabIndex + 5}
                error={errors.cntrPartySPRating || ""}
                list={dropDown.partRating && dropDown.partRating["S&P"]}
                name="cntrPartySPRating"
                value={participant.cntrPartySPRating || ""}
                disabled={!isEditable || !canEditTran}
                onChange={onChange}
                onBlur={onBlurInput}
              />
              : <small>{participant.cntrPartySPRating}</small>
          }
        </td>
        {
          canEditTran ?
            <td>
              <div className="field is-grouped">
                <div className="control">
                  <a onClick={isEditable ? () => onSave(category, participant, index) : () => onEdit(category, index)}
                     className={`${isSaveDisabled ? "isDisabled" : ""}`}> {/* eslint-disable-line */}
                    <span className="has-text-link">
                      {isEditable ?
                        <i title="Save"
                           tabIndex={tabIndex + 6}
                           className="far fa-save"
                           onKeyDown={(e) => onSaveBtnClick(e, category, participant, index)}/> :
                        <i title="Edit" className="fas fa-pencil-alt"/>}
                    </span>
                  </a>
                </div>
                <div className="control">
                  <a
                    onClick={isEditable ? () => onCancel(category) : () => onRemove(participant._id, category, index, participant.cntrPartyFirmName)}
                    className={`${isSaveDisabled ? "isDisabled" : ""}`}> {/* eslint-disable-line */}
                    <span className="has-text-link">
                      {isEditable ?
                        <i title="Cancel"
                           tabIndex={tabIndex + 7}
                           className="fa fa-times"
                           onKeyDown={(e) => onCancelBtnClick(e, category)}/> :
                        <i title="Delete" className="far fa-trash-alt"/>}
                    </span>
                  </a>
                </div>
              </div>
            </td>
            : null
        }
      </tr>
    </tbody>
  )}

export default CounterParties
