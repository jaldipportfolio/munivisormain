import React from "react"
import ManagementConsole from "./ManagementConsole"
import "./scss/admin.scss"

const AdminManagement = props => (<ManagementConsole links = {props.links}/>)

export default AdminManagement
