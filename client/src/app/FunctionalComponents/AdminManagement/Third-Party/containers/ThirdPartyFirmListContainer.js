import React, { Component } from "react"
import "react-table/react-table.css"
import { withRouter } from "react-router-dom"
import Fuse from "fuse.js"
import { connect } from "react-redux"
import "../../scss/entity.scss"
import ThirdPartyNew from "../../../ThirdParty/ThirdParty"
import EntityPageFilter from "../../../EntityManagement/CommonComponents/EntityPageFilter"
import Loader from "../../../../GlobalComponents/Loader"

const searchOptions = {
  shouldSort: true,
  threshold: 0.3,
  location: 0,
  distance: 100,
  maxPatternLength: 32,
  minMatchCharLength: 1,
  keys: [
    {
      name: "firmType",
      weight: 0.8
    },
    {
      name: "firmName",
      weight: 0.9
    },
    {
      name: "state",
      weight: 0.89
    },
    {
      name: "city",
      weight: 0.98
    },
    {
      name: "addressName",
      weight: 0.86
    },
    {
      name: "primaryContact",
      weight: 0.6
    }
  ]
}

export const Modal = ({ children, closeModal, modalState, title }) => {
  if (!modalState) {
    return null
  }
  return (
    <div className="modal is-active">
      <div
        className="modal-background"
        onClick={closeModal}
        role="presentation"
        onKeyPress={() => { }}
      />
      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-title">{title}</p>
          <button className="delete" onClick={closeModal} />
        </header>
        <section className="modal-card-body">
          <div className="content">{children}</div>
        </section>
      </div>
    </div>
  )
}
class ThirdPartyFirmListContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      search: "",
      viewList: true
    }
  }

  fuse = e => {
    const { search } = this.state
    const fuse = new Fuse(e, searchOptions)
    return fuse.search(search)
  }

  addClientFirm = () => {
    this.setState({
      viewList: false
    })
  }

  render() {
    return (
      <div className="firms">
        {this.state.loading ? <Loader /> : ""}
        {!this.state.viewList && <ThirdPartyNew />}
        {this.state.viewList && (
          <div>
            <EntityPageFilter
              listType="third-party"
              auth={this.props.auth}
              nav2={this.props.nav2 || ""}
              nav1={this.props.nav1 || ""}
              searchPref="adm-thirdparty"
              title="Third Parties"
              addClientFirm={this.addClientFirm}
            />
          </div>
        )}
      </div>
    )
  }
}

const mapStateToProps = state => {
  const { auth } = state
  return { auth }
}

const mapDispatchToProps = {}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ThirdPartyFirmListContainer)
)
