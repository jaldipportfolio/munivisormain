import React from "react"
import { Route, Switch } from "react-router-dom"
import moment from "moment"
import {toast} from "react-toastify"
import UserData from "./containers/UserData"
import DealFiles from "./containers/DealFiles"
import "./index.scss"
import {getAuditLogByType} from "../../../StateManagement/actions/audit_log_actions"
import Loader from "../../../GlobalComponents/Loader"
import MigrationAudit from "./components/MigrationAudit"
import Accordion from "../../../GlobalComponents/Accordion"
import RatingSection from "../../../GlobalComponents/RatingSection"
import DataMigrationModal from "../../../GlobalComponents/DataMigrationModal"
import {postJSONData} from "../../../StateManagement/actions/Common"
import CONST from "../../../../globalutilities/consts"

class MigrationTools extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      tiles: [
        { path: "user-data", label: "Entities/Contacts Combined", row: 1 },
        { path: "deal-files", label: "Deal List", row: 1 }
      ],
      loading: true,
      errorSummary: {},
      modalState: false
    }
  }

  componentWillMount() {
    this.getAuditLogs()
  }

  /**
   * Select option handler
   */
  migrationToolSelectHandler = (selectedTool) => {
    this.props.history.push(`/admin-migtools/${selectedTool.path}`)
  }

  getAuditLogs = () => {
    getAuditLogByType("migrationTools", "Data Migration", res => {
      if (res && res._id) {
        res &&
        res.changeLog.forEach(audit => {
          audit.timeStamp = audit.date ? moment(audit.date).unix() : 0
        })
        res && res.changeLog.sort((a, b) => b.timeStamp - a.timeStamp)
        console.log(res.changeLog)
        this.setState({
          auditLogs: res.changeLog || [],
          loading: false
        })
      } else {
        this.setState({
          loading: false
        })
      }
    })
  }

  /**
   * Main page view
   */
  renderView = () => {
    const { tiles } = this.state
    // const { auditLogs } = this.state
    const rows = [...new Set(this.state.tiles.map(a => a.row))]
    // const successUsers = auditLogs ? auditLogs.find(audit => audit.type === "user-data" && audit.status === "Success") : {}
    // const successDeals = auditLogs ? auditLogs.find(audit => audit.type === "deal-files" && audit.status === "Success") : {}
    return (
      <div>
        <section className="container has-text-centered">
          <p className="title innerPgTitle">Migration Tools</p>
          <p className="multiExpLbl ">Select type of data which you want to upload and migrate.</p>
        </section>
        <hr />
        <section className="container">
          {rows.map(r =>
            <div key={r} className="tile is-ancestor">
              {
                tiles.filter(({ row }) => row === r).map(tile => {
                  const display = /* (successDeals && successDeals.type === tile.path) || (successUsers && successUsers.type === tile.path) */ false
                  const style ={
                    cursor: display ? "not-allowed" : "pointer"
                  }
                  return (
                    <div key={tile.path} className="tile is-parent" style={style} onClick={!display ? () => this.migrationToolSelectHandler(tile) : () => {}}>
                      <article className="tile is-child box has-text-centered">
                        <p className="subtitle">{tile.label}</p>
                      </article>
                    </div>
                  )
                })
              }
            </div>
          )}
        </section>
        <hr />
      </div>
    )
  }

  toggleModal = (item) => {
    this.setState({
      errorSummary: (item && item.errorCount) || {},
      modalState: !this.state.modalState
    })
  }

  onDownload = async (item) => {
    const { newTransactions, duplicateTransactions, newClientsAndProspects, newThirdParties, newUsers, newMigratedEntities, newMigratedUsers } = item
    const dataArray = []
    if(newTransactions && newTransactions.length){
      dataArray.push({
        name: "New Transactions",
        headers: ["ID", "Client Name", "Issue Name", "Type", "Subtype"],
        data: newTransactions.map(tran => ({
          "ID": tran._id || "",
          "Client Name": tran.dealIssueTranIssuerFirmName || tran.actTranClientName || "",
          "Issue Name": tran.dealIssueTranIssueName || tran.actTranIssueName || "",
          "Type": tran.dealIssueTranType || tran.actTranType || "",
          "Subtype": tran.dealIssueTranSubType || tran.actTranSubType || "",
        }))
      })
    }
    if(duplicateTransactions && duplicateTransactions.length){
      dataArray.push({
        name: "Duplicate Transactions",
        headers: ["Client Name", "Issue Name", "Type", "Subtype"],
        data: duplicateTransactions.map(tran => ({
          "Client Name": tran.clientEntityName || "", "Issue Name": tran.issueName || "", "Type": tran.type || "", "Subtype": tran.subType || ""
        }))
      })
    }
    if(newClientsAndProspects && newClientsAndProspects.length){
      dataArray.push({
        name: "New Clients",
        headers: ["ID", "Firm Name", "Firm Type"],
        data: newClientsAndProspects.map(client => ({
          "ID": client._id || "",
          "Firm Name": client.firmName || "",
          "Firm Type": client.entityType || "",
        }))
      })
    }
    if(newThirdParties && newThirdParties.length){
      dataArray.push({
        name: "New Third Parties",
        headers: ["ID", "Firm Name", "Firm Type"],
        data: newThirdParties.map(client => ({
          "ID": client._id || "",
          "Firm Name": client.firmName || "",
          "Firm Type": client.entityType || "",
        }))
      })
    }
    if(newUsers && newUsers.length){
      dataArray.push({
        name: "New Users",
        headers: ["ID", "Firm Name", "First Name", "Last Name", "Email"],
        data: newUsers.map(user => ({
          "ID": user._id || "",
          "Firm Name": user.userFirmName || "",
          "First Name": user.userFirstName || "",
          "Last Name": user.userLastName || "",
          "Email": (user.userLoginCredentials && user.userLoginCredentials.userEmailId) || "",
        }))
      })
    }
    if(newMigratedEntities && newMigratedEntities.length){
      dataArray.push({
        name: "New Migrated Entities",
        headers: ["ID", "Firm Name"],
        data: newMigratedEntities.map(client => ({
          "ID": client._id || "",
          "Firm Name": client.firmName || ""
        }))
      })
    }
    if(newMigratedUsers && newMigratedUsers.length){
      dataArray.push({
        name: "New Migrated Users",
        headers: ["ID", "Firm Name", "First Name", "Last Name", "Email"],
        data: newMigratedUsers.map(user => ({
          "ID": user._id || "",
          "Firm Name": user.userFirmName || "",
          "First Name": user.userFirstName || "",
          "Last Name": user.userLastName || "",
          "Email": (user.userLoginCredentials && user.userLoginCredentials.userEmailId) || "",
        }))
      })
    }
    if(dataArray.length){
      await postJSONData({dataArray}, "Migrated Data List")
    }else {
      toast("Items not found", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.ERROR
      })
    }
  }

  /**
   * Component render method
   */
  render() {
    const {loading, auditLogs, errorSummary, modalState} = this.state
    const {nav2} = this.props
    return (
      <div className="migtools box">
        <DataMigrationModal data={errorSummary} toggleModal={this.toggleModal} modalState={modalState}/>
        { loading ? <Loader /> : null }
        <Switch>
          <Route path="/admin-migtools/user-data" component={UserData} />
          <Route path="/admin-migtools/deal-files" component={DealFiles} />
          <Route path="/admin-migtools" exact children={this.renderView()} />  {/* eslint-disable-line */}
        </Switch>
        {!nav2 ?
          <Accordion
            multiple
            activeItem={[]}
            boxHidden
            render={({ activeAccordions, onAccordion }) => (
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="Activity log"
              >
                {activeAccordions.includes(0) && (
                  <MigrationAudit
                    logs={auditLogs || []}
                    search=""
                    openModal={this.toggleModal}
                    onDownload={this.onDownload}
                  />
                )}
              </RatingSection>
            )}
          />
          : null}
      </div>
    )
  }
}

export default MigrationTools
