import React, { Component } from "react"
import swal from "sweetalert"

class MyInput extends Component {
  constructor(props) {
    super(props)

    this.state = {
      text: props.value,
      isNewMappingObj: false
    }
  }

  changeText = (e) => {
    const text = e.target.value

    this.setState({
      text,
    })

    /*
     * This will update the value that the confirm
     * button resolves to:
     */
    swal.setActionValue(text)
  }

  onCheck = (event) => {
    const {name, checked} = event.target
    this.setState({
      [name]: checked
    }, () => this.props.onChange(checked))
  }

  render() {
    return (
      <div>
        <input
          className="input is-small is-link"
          value={this.state.text}
          onChange={this.changeText}
        />
        <div className="is-pulled-left">
          <label className="checkbox">
            <p className="multiExpLbl">
              <input
                type="checkbox"
                name="isNewMappingObj"
                checked={this.state.isNewMappingObj || false}
                onClick={this.onCheck}
                onChange={() => {}}
              />
              &nbsp;&nbsp; Is new mapping ?
            </p>
          </label>
        </div>
      </div>
    )
  }
}

export default MyInput
