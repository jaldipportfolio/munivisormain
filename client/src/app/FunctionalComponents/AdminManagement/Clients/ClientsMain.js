import React from "react"
import ClientsView from "./ClientsView"
import ClientFirmList from "./containers/ClientFirmListContainer"

const ClientsMain = props => {
  const { nav1, nav2, nav3 } = props
  if (nav2) {
    return (
      <ClientsView nav1={nav1} id={nav2} navComponent={nav3} />
    )
  }
  return(
    <ClientFirmList userType="admin"/>
  )
}

export default  ClientsMain
