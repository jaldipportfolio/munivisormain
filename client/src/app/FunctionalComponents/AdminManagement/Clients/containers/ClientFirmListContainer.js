import React, { Component } from "react"
import "react-table/react-table.css"
import Fuse from "fuse.js"
import { withRouter } from "react-router-dom"
import { connect } from "react-redux"
import Loader from "./../../../../GlobalComponents/Loader"
import "./../../scss/entity.scss"
import ClientsProspects from "../../../ClientsProspects/ClientsProspects"
import EntityPageFilter from "./../../../EntityManagement/CommonComponents/EntityPageFilter"

const searchOptions = {
  shouldSort: true,
  threshold: 0.3,
  location: 0,
  distance: 100,
  maxPatternLength: 32,
  minMatchCharLength: 1,
  keys: [
    {
      name: "firmType",
      weight: 0.8
    },
    {
      name: "firmName",
      weight: 0.9
    },
    {
      name: "state",
      weight: 0.89
    },
    {
      name: "city",
      weight: 0.98
    },
    {
      name: "addressName",
      weight: 0.86
    },
    {
      name: "primaryContact",
      weight: 0.6
    }
  ]
}

class ClientFirmListContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      search: "",
      viewList: true
    }
  }

  fuse = e => {
    const { search } = this.state
    const fuse = new Fuse(e, searchOptions)
    return fuse.search(search)
  }

  addClientFirm = () => {
    this.setState({
      viewList: false
    })
  }

  render() {
    const loading = () => <Loader />
    return (
      <div className="firms">
        {this.state.loading ? loading() : null}
        {!this.state.viewList && <ClientsProspects />}
        {this.state.viewList && (
          <div>
            <EntityPageFilter
              listType="client-prospect"
              auth={this.props.auth}
              nav2={this.props.nav2 || ""}
              nav1={this.props.nav1 || ""}
              searchPref="adm-cltprosp"
              title="Clients / Prospects"
              addClientFirm={this.addClientFirm}
            />
          </div>
        )}
      </div>
    )
  }
}

const mapStateToProps = state => {
  const { auth } = state
  return { auth }
}

const mapDispatchToProps = {}
export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ClientFirmListContainer)
)
