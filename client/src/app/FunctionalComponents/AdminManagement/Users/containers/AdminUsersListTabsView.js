import React, { Component } from "react"
import "react-table/react-table.css"
import { withRouter } from "react-router-dom"
import { connect } from "react-redux"
import * as qs from "query-string"
import Users from "Admin/Users/UsersMain"
import "./../../scss/entity.scss"
import UsersListContainer from "./UsersListContainer"
import MigratedUsersListContainer from "../../../EntityManagement/users/containers/MigratedUsersListContainer"

class AdminUsersListTabsView extends Component {
  constructor(props) {
    super(props)
    this.state = {
      tabActiveIndex: 0,
      TABS: [
        { label: "Users" },
        { label: "Migrated Users" },
      ]
    }
  }

  componentWillMount() {
    const queryString = qs.parse(location.search)
    const tabActiveIndex = parseInt(queryString.active, 10) || 0
    this.setState({
      tabActiveIndex
    })
  }

  identifyComponentToRender = (index) => {
    this.setState({
      tabActiveIndex:index
    },() => this.props.history.push(`admin-users?active=${index}`))
  }

  renderTabContents = () => {
    const {TABS,tabActiveIndex} =this.state
    const { auth} = this.props
    const relationshipToTenant = (auth && auth.userEntities && auth.userEntities.relationshipToTenant ) || ""
    let NEWTABS = []
    if ( (relationshipToTenant || "") === "Self") {
      NEWTABS = [...TABS]
    } else {
      NEWTABS= [ { label: "Users" }]
    }

    const tabActive = TABS[tabActiveIndex].label
    return (<ul>
      {
        NEWTABS.map ( (t,index) =>
          <li key={t.label} className={tabActive === t.label ? "is-active" : ""} onClick={() => this.identifyComponentToRender(index)}>
            <a className="tabSecLevel">  {/* eslint-disable-line */}
              <span>{t.label}</span>
            </a>
          </li>
        )
      }
    </ul>)
  }

  renderSelectedView = (option) => {
    switch (option) {
    case 0 :
      return <UsersListContainer listType="users" activeTab="Users" nav1={this.props.nav1}/>
    case 1 :
      return <MigratedUsersListContainer listType="migratedusers" activeTab="Migrated Users" nav1={this.props.nav1} />
    default:
      return option
    }
  }
  render() {
    const {nav2, nav3} = this.props
    if(nav2 && nav3){
      return <Users {...this.props}/>
    }
    return (
      <div id="main">
        <div className="tabs">
          {this.renderTabContents()}
        </div>
        {this.renderSelectedView(this.state.tabActiveIndex)}
      </div>
    )
  }
}

const mapStateToProps = state => {
  const { auth } = state
  return { auth }
}

export default withRouter(connect(mapStateToProps,null)(AdminUsersListTabsView))
