import React, { Component } from "react"
import * as qs from "query-string"
import Loader from "../../../GlobalComponents/Loader"
import RatingSection from "../../../GlobalComponents/RatingSection"
import Accordion from "../../../GlobalComponents/Accordion"
import {getOutLookContacts, oAuthSignIn, getOutLookContactsFromFolder} from "../../../StateManagement/actions/OutlookSerivce"
import {ContactList} from "../../../GlobalComponents/ContactList"

class Contacts extends Component {

  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      contacts: [],
      dirContacts: [],
      selectedDir: ""
    }
  }

  componentWillMount() {
    const queryString = qs.parse(location.search)
    const code = queryString.code || ""
    console.log(queryString)
    const msCredential = localStorage.getItem("ms_credential")
    if(code || msCredential){
      this.getOutLookContacts(code, msCredential)
    }
    this.setState({
      msCredential: JSON.parse(msCredential) || {},
      code
    })
  }

  getOutLookContacts = (code, msCredential) => {
    this.setState({
      loading: true
    }, async () => {
      const contacts = await getOutLookContacts(code, msCredential)
      localStorage.setItem("ms_credential", JSON.stringify((contacts && contacts.accessCredential) || {}))
      this.setState({
        msCredential: (contacts && contacts.accessCredential) || {},
        contacts: (contacts && contacts.contacts) || [],
        directories: (contacts && contacts.directories) || [],
        loading: false
      })
    })
  }

  authSignIn = async () => {
    const response = await oAuthSignIn()
    if(response && response.signInUrl) {
      window.location = response.signInUrl
    }
  }

  actionButtons = () => {
    const {code, msCredential} = this.state
    return (
      <div className="field is-grouped">
        {
          (msCredential && msCredential.access_token)  ?
            <div className="control">
              <a className="fa fa-refresh"  // eslint-disable-line
                style={{
                  fontSize: 25,
                  cursor: "pointer",
                  padding: "0 0 0 10px"
                }}
                onClick={() => this.getOutLookContacts(code, JSON.stringify(msCredential))}
              />
            </div>
            : null
        }
      </div>
    )
  }

  onChange = (e) => {
    const {name, value} = e.target
    this.setState({
      [name]: value
    }, () => {
      if(name === "selectedDir" && value){
        this.onDirectoryChange()
      }
    })
  }

  onDirectoryChange = () => {
    const {selectedDir, msCredential, code} = this.state

    if(!selectedDir) return

    this.setState({
      loading: true
    }, async () => {
      const res = await getOutLookContactsFromFolder(selectedDir, code, msCredential)
      localStorage.setItem("ms_credential", JSON.stringify((res && res.accessCredential) || {}))
      this.setState({
        msCredential: (res && res.accessCredential) || {},
        dirContacts: (res && res.dirContacts) || [],
        loading: false
      })
    })
  }

  render() {
    const {loading, msCredential, contacts, directories, selectedDir, dirContacts} = this.state

    return (
      <div id="main">
        {loading ? <Loader/> : null}
        <Accordion
          multiple activeItem={[0]} boxHidden render={({ activeAccordions, onAccordion }) =>
            <div>
              {
                !(msCredential && msCredential.access_token) ?
                  <div className="columns margin-topTen">
                    <div className="column margin-topTen is-full is-grouped-center">
                      <button className="button is-info text-center" onClick={this.authSignIn}>Sign In To Outlook</button>
                    </div>
                  </div>
                  :
                  <div>
                    <RatingSection
                      onAccordion={() => onAccordion(0)}
                      title="Contacts"
                      actionButtons={this.actionButtons()}
                      style={{ overflowY: "unset" }}>
                      {activeAccordions.includes(0) &&
                      <div>
                        <ContactList rowData={contacts}/>
                      </div>
                      }
                    </RatingSection>
                    <RatingSection
                      onAccordion={() => onAccordion(1)}
                      title="Folder Contact List"
                      style={{ overflowY: "unset" }}>
                      {activeAccordions.includes(1) &&
                      <div>
                        <div style={{marginBottom: 10}}>
                          <p className="multiExpLblBlk">Select Folder</p>
                          <div className="select is-small is-link">
                            <select
                              value={selectedDir || ""}
                              name="selectedDir"
                              onChange={this.onChange}>
                              <option value="">Select Folder</option>
                              { directories && (directories || []).map(dir => <option key={dir.id} value={dir.id} >{dir.displayName}</option>) }
                            </select>
                          </div>
                        </div>
                        <ContactList rowData={dirContacts}/>
                      </div>
                      }
                    </RatingSection>
                  </div>
              }
            </div>
          }
        />
      </div>
    )
  }
}

export default Contacts
