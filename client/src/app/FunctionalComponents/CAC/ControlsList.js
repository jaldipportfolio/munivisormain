import React, { Component } from "react"
import { Link } from "react-router-dom"
import ReactTable from "react-table"
import { fetchAllControls } from "../../StateManagement/actions/cac"
import Loader from "../../GlobalComponents/Loader"
import Disclaimer from "../../GlobalComponents/Disclaimer"

class ControlsList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedListId: "", selectedAction: "", searchString: "", waiting: false, loadingAction: true, sorted: { "_id": -1 }, page: 0, total: 0,
      stringToMatch: "", pageSizeOptions: [5, 10, 20, 25, 50], pageSize: 5, controlStatus: "enabled", pages: 0
    }
    this.addNewControl = this.addNewControl.bind(this)
    this.copyControl = this.copyControl.bind(this)
    this.changeSearchString = this.changeSearchString.bind(this)
  }

  async componentWillMount() {
    this.onGridOperations()
  }

  onGridOperations = async () => {
    const {pageSize, page, searchString, sorted, controlStatus} = this.state

    const body = {
      page,
      pageSize,
      sortedFields: sorted,
      filter: {searchString}
    }

    const res = await fetchAllControls(controlStatus, body)
    if (res && res.data && res.data.length) {
      this.setState({
        controls: res.data || [],
        total: (res.metadata[0] && res.metadata[0].total) || 0,
        pages: (res.metadata[0] && res.metadata[0].pages) || 0,
        loadingAction: false
      })
    } else {
      this.setState({
        loadingAction: false,
        controls: []
      })
    }
  }

  changeSearchString = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
      page: 0,
      loadingAction: true
    }, () => {
      this.onGridOperations()
    })
  }

  performAction() {
    const { selectedListId, selectedAction } = this.state
    switch (selectedAction) {
    case "edit":
      this.props.history.push(`/cac/controls-list/edit-control/${selectedListId}`)
      break
    case "copy":
      console.log("copying : ", selectedListId)
      this.copyControl(selectedListId)
      break
    default:
      console.log("unknown action")
      break
    }
  }

  addNewControl() {
    const newListId = `CTRL-${Math.random()
      .toString(36)
      .toUpperCase()
      .substring(2, 17)}`
    this.props.history.push(`/cac/controls-list/edit-control/${newListId}?src=new`)
  }

  copyControl(id) {
    const controls = [...this.state.controls]
    const ids = controls.filter(c => /^CTRL\d/.test(c.id)).map(c => +c.id.split("CTRL")[1])
    ids.sort((a, b) => b - a)
    const newListId = `CTRL-${Math.random()
      .toString(36)
      .toUpperCase()
      .substring(2, 17)}`
    this.props.history.push(`/cac/controls-list/edit-control/${newListId}?src=copy&id=${id}`)
    // this.props.addNewControl(id, "copy")
  }

  changeSelectedAction(selectedListId, e) {
    console.log("selectedListId : ", selectedListId)
    console.log("e : ", e)
    const selectedAction = e.target.value
    this.setState({ selectedListId, selectedAction }, this.performAction)
  }

  renderTable(controls) {
    const { pageSize, pageSizeOptions, selectedAction, page, pages } = this.state
    return (
      <ReactTable
        columns={[
          {
            Header: "ID",
            id: "_id",
            className: "multiExpTblVal",
            accessor: e => e,
            Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
              <Link to={`/cac/controls-list/edit-control/${e.value.id}`}>{e.value.id}</Link></div>
          },
          {
            Header: "Control Name",
            id: "name",
            className: "multiExpTblVal",
            accessor: e => e.name,
            Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
              <small>{e.value}</small></div>
          },
          {
            Header: "Set up by",
            id: "createdBy",
            className: "multiExpTblVal",
            accessor: e => e.createdBy,
            Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
              <small>{e.value}</small></div>
          },
          {
            Header: "Compliance Topic",
            id: "topic",
            className: "multiExpTblVal",
            accessor: e => e.topic,
            Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
              <small>{e.value}</small></div>
          },
          {
            Header: "Notes",
            id: "notes",
            className: "multiExpTblVal",
            accessor: e => e.notes,
            Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
              <small>{e.value}</small></div>
          },
          {
            Header: "Last Updated",
            id: "lastUpdated",
            className: "multiExpTblVal",
            accessor: e => e,
            Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
              <small>{e.value.lastUpdated && e.value.lastUpdated.date &&
                new Date(e.value.lastUpdated.date).toLocaleString()} {e.value.lastUpdated && e.value.lastUpdated.by}
              </small></div>
          },
          {
            Header: "Action",
            id: "action",
            sortable: false,
            className: "multiExpTblVal",
            accessor: e => e,
            Cell: e => <div className="hpTablesTd wrap-cell-text tooltips">
              <div className="select is-small is-link">
                <select value={selectedAction} onChange={this.changeSelectedAction.bind(this, (e && e.value && e.value.id))}>
                  <option value="" disabled>Action</option>
                  <option value="edit" disabled={(e && e.value && e.value.state === "disabled")}>Edit</option>
                  <option value="copy">Make a copy</option>
                </select>
              </div>
            </div>
          }
        ]}
        data={controls}
        manual
        getTdProps={(state, rowInfo) => ({
          style: {
            background: (rowInfo && rowInfo.row && rowInfo.row._id && rowInfo.row._id.state === "disabled") ? "#dedede" : null
          }
        })}
        defaultPageSize={pageSize}
        pageSizeOptions={pageSizeOptions}
        className="-striped -highlight is-bordered"
        pageSize={pageSize}
        loading={this.state.loadingAction}
        minRows={2}
        showPageJump
        page={page}
        pages={pages}
        onPageChange={page => {
          this.setState({
            page: page,
            loadingAction: true
          }, () => {
            this.onGridOperations()
          })
        }}
        onPageSizeChange={pageSize => {
          this.setState({
            pageSize,
            loadingAction: true,
            page: 0
          }, () => {
            this.onGridOperations()
          })
        }}
        onSortedChange={(newSort) => {
          let sortedFields = {}
          if (newSort && newSort[0] && newSort[0].id) {
            sortedFields[newSort[0].id] = newSort[0].desc ? -1 : 1
          }
          this.setState({
            sorted: sortedFields,
            loadingAction: true
          }, () => {
            this.onGridOperations()
          })
        }}
      />
    )
  }

  changeControlStatus = () => {
    const {controlStatus} = this.state
    this.setState({
      controlStatus: controlStatus === "enabled" ? "all" : "enabled",
      page: 0,
      waiting : true
    }, () => {
      this.onGridOperations()
    })
  }

  renderControls(controls) {
    const {controlStatus} = this.state
    return (
      <section className="box">
        {/* <section className="accordions box top-bottom-margin"> */}
        {/* <p className="title innerPgTitle has-text-centered">Search by control id, name, topic or notes</p> */}
        <div className="columns">

          <div className="column is-three-quarters">
            <p className="control has-icons-left">
              <input className="input is-small is-link"
                type="text"
                placeholder="search by control id, name, topic or notes"
                name="searchString"
                value={this.state.searchString}
                onChange={this.changeSearchString}
              />
              <span className="icon is-small is-left has-background-dark has-text-white">
                <i className="fas fa-search" />
              </span>
            </p>
          </div>
          <div className="column">
            <button className="button is-link is-small" onClick={this.addNewControl}>Add new control</button> {/* eslint-disable-line */}
          </div>
          {
            <div className="column">
              <div className="switchContainer is-pulled-right">
                <span className="filterText">{controlStatus === "enabled" ? "Enabled" : "All"}</span>
                <label className="switch" style={{ marginLeft: "10px" }}> {/* eslint-disable-line */}
                  <input
                    type="checkbox"
                    onChange={this.changeControlStatus}
                    checked={controlStatus !== "enabled"}
                  />
                  <span className="slider round" />
                </label>
              </div>
            </div>
          }
        </div>

        {this.renderTable(controls)}
      </section>

    )
  }

  render() {
    const { controls } = this.state
    if (controls) {
      return (
        <div id="main">
          {this.renderControls(controls)}
          <hr />
          <Disclaimer />
        </div>
      )
    }
    return <Loader />
  }
}

export default ControlsList
