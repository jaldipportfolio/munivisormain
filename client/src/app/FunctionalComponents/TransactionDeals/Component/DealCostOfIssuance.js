/* 
1. Get the cost of issuance elements configured for the tenant - DONE
2. if the cost of issuance data already exists then do not get the tenant configuration
2.1. Display the data in many accordions
2.2. Fix the accordions and make it fit the Bulma standards in the design.
3. Manage the state of the accordions - Make it configurable - anything can open, only can one can open etc, based on the flag.
4. Save the data to the backend
5. Update the the data
6. Build aggregates for the aggregate section within the component
7. Manage the state of the form 
8. Build the ability to reset the form and initialize that to the previous save.
9. Ensure that the fields take only numeric inputs
10. Validate if the data is getting saved in the database.
11. Build a deal reducer that is easy to update.
12. Build a mongo insert query that is easy to update at nested levels.
13. Brainstorm on potential validations related to the model
14. Try out both nested and normalized mongo models.
15. Write a generic helper function to get the configured picklists for the tenant.
16. Write a generic helper function to get the configured checklists for the tenant.
17. Manage the fetched checklists on the state so that we don't have to keep fetching them again.
18. Pay special attention to component re-rendering so that there is no unnecessary rendring of the components
19. Write Async loading of the drop downs based on what needs to be rendered.
20. Persist Information in the Audit Trail Schema based on all the actions that have been taken.
21. Weigh of updating the checklist configuration in the application state.
22. Write memoized selectors to get specific aspects of the application state.
23. Make sure that not all the information is saved in the application state. Be very careful in terms what can be saved.

GET_DEAL_CHECKLIST_ITEMS - Cost Of Issuance.
GET_PICKLIST_ITEMS_LEVEL1
GET_PICKLIST_ITEMS_LEVEL2 // Pass Level 1 to this List
GET_PICKLIST_ITEMS_LEVEL3 // Pass Level1 and Level to this service

DEAL_COI_SAVE_ALLSECTIONS   // Ability to save all sections to the backend db
DEAL_COI_ADD_ALLSECTIONS    // Ability to add all the sections in one go to the backend db
DEAL_COI_SAVE_SECTION       // Ability to save an entire section
DEAL_COI_ADD_SECTION        // Ability to add a complete section
DEAL_COI_SAVE_SECTION_ITEM  // Ability to save an item
DEAL_COI_ADD_SECTION_ITEM   // Ability to add an item

*/

