import React from "react"
import {NumberInput, SelectLabelInput} from "../../../../../GlobalComponents/TextViewBox"
import DropDownListClear from "../../../../../GlobalComponents/DropDownListClear"

const UnderWriters = ({underWriter, index, isEditable, onSave, firms, dropDown, onEdit, onCancel, onItemChange, onRemove, errors = {}, onBlur, category,
  isSaveUnderWriter, canEditTran, tableTitle, onCheckAddTODL, tabIndex}) => {

  const onChange = (event) => {
    if(event.target.name === "roleInSyndicate" && event.target.value === "Sole Manager") {
      underWriter.liabilityPerc = 100
      underWriter.managementFeePerc = 100
    }
    onItemChange({
      ...underWriter,
      [event.target.name]: event.target.type === "checkbox" ? event.target.checked : event.target.value,
    }, category, index)
  }

  const onUserChange = (firm) => {
    onItemChange({
      ...underWriter,
      dealPartFirmId: firm.id,
      dealPartFirmName: firm.name,
    }, category, index)
  }

  const onBlurInput = (event, name) => {
    if(event && event.target && event.target.title && event.target.value){
      onBlur(category, `${event.target.title || name || "empty"} change to ${event.target.type === "checkbox" ? event.target.checked : name ? underWriter.dealPartFirmName : event.target.value || "empty"}`)
    }
    if(event && event.name) {
      onBlur(category, `Underwriters participant firm change to ${event.name}`)
    }
  }
  isEditable = canEditTran ? (isEditable[category] === index) : ""

  const onSaveBtnClick = (e, category, underWriter, index) => {
    if(e.keyCode === 13 || e.keyCode === 32){
      onSave(category, underWriter, index)
    }
  }

  const onEditBtnClick = (e, category, index) =>{
    if(e.keyCode === 13){
      onEdit(category, index)
    }
  }

  const onCancelBtnClick = (e) => {
    if(e.keyCode === 13){
      onCancel(category)
    }
  }

  const onDeleteBtnClick = (e, id, category, index, dealPartFirmName, tableTitle ) =>{
    if(e.keyCode === 13){
      onRemove(id, category, index, dealPartFirmName, tableTitle)
    }
  }

  return (
    <tr>
      <td>
        <label className="checkbox"> {/* eslint-disable-line */}
          <input type="checkbox" title="Add To DL" name="dealPartContactAddToDL" checked={underWriter.dealPartContactAddToDL || false} disabled={!underWriter._id || !canEditTran} onChange={() => {}} onClick={(e) => onCheckAddTODL(e, underWriter, "underWriter")} />
          {errors.dealPartContactAddToDL && <p className="text-error">{errors.dealPartContactAddToDL}</p>}
        </label>
      </td>
      <td style={{width: "300px"}}>
        <div className="control">
          {/* <DropdownList filter data={firms} value={underWriter.dealPartFirmName} textField="name" valueField="id" onChange={onUserChange} onBlur={(e) => onBlurInput(e, "dealPartFirmName")}  disabled={!isEditable} /> */}
          <DropDownListClear
            filter
            tabIndex={tabIndex}
            data={firms}
            value={underWriter.dealPartFirmName}
            textField="name"
            valueField="id"
            onChange={onUserChange}
            onBlur={(e) => onBlurInput(e, "dealPartFirmName")}
            disabled={!isEditable}
            onClear={() => {onUserChange({id: "", name: ""})}}
            isHideButton={underWriter.dealPartFirmName && isEditable}
          />
          {errors.dealPartFirmName && <p className="text-error">Required</p>}
        </div>
        {/* <SelectLabelInput error= {errors.dealPartFirmName || ""} name="dealPartFirmName" value={underWriter.dealPartFirmName} list={firms} onChange={onChange} onBlur={onBlurInput}  disabled={!isEditable}/> */}
      </td>
      <td>
        <SelectLabelInput
          tabIndex={tabIndex + 1}
          title="Role In Syndicate"
          error= {errors.roleInSyndicate || ""}
          name="roleInSyndicate"
          value={underWriter.roleInSyndicate}
          list={dropDown.syndicates}
          onChange={onChange}
          onBlur={onBlurInput}
          disabled={!isEditable}/>
      </td>
      <td>
        <NumberInput
          tabIndex={tabIndex + 2}
          title="Liability Percentage"
          suffix="%"
          error={errors.liabilityPerc/* || isEditable && liabilityTotalPerc > 100 ?  "Total Liability is should be less than or equal to 100%" : "" */}
          name="liabilityPerc"
          placeholder="%"
          value={underWriter.liabilityPerc}
          onChange={onChange}
          onBlur={onBlurInput}
          disabled={!isEditable}/>
      </td>
      <td>
        <NumberInput
          tabIndex={tabIndex + 3}
          title="Management Fee Percentage"
          suffix="%"
          error={errors.managementFeePerc/* || isEditable && managementFeeTotalPerc > 100 ? "Total Management Fee is should be less than or equal to 100%" : "" */}
          name="managementFeePerc"
          placeholder="%"
          value={underWriter.managementFeePerc}
          onChange={onChange}
          onBlur={onBlurInput}
          disabled={!isEditable}/>
      </td>
      {
        canEditTran ?
          <td>
            <div className="field is-grouped">
              <div className="control">
                <a tabIndex={isEditable ? tabIndex + 4 : null} onKeyDown={isEditable ? (e) => onSaveBtnClick(e, category, underWriter, index) : (e) => onEditBtnClick(e, category, index)} onClick={isEditable ? () => onSave(category, underWriter, index) : () => onEdit(category, index)} className={`${isSaveUnderWriter ? "isDisabled" : ""}`}> {/* eslint-disable-line */}
                  <span className="has-text-link">
                    {isEditable ? <i className="far fa-save" title="Save"/> : <i className="fas fa-pencil-alt" title="Edit"/>}
                  </span>
                </a>
              </div>
              <div className="control">
                <a tabIndex={isEditable ? tabIndex + 5 : null} onClick={isEditable ? () => onCancel(category) :() => onRemove(underWriter._id, category, index, underWriter.dealPartFirmName, tableTitle)} className={`${isSaveUnderWriter ? "isDisabled" : ""}`}
                   onKeyDown={isEditable ? onCancelBtnClick : (e) => onDeleteBtnClick(e, underWriter._id, category, index, underWriter.dealPartFirmName, tableTitle ) }> {/* eslint-disable-line */}
                  <span className="has-text-link">
                    {isEditable ? <i className="fa fa-times" title="Cancel"/> : <i className="far fa-trash-alt" title="Delete"/>}
                  </span>
                </a>
              </div>
              {underWriter.isNew && !isEditable ? <small className="text-error">New(Not Save)</small> : null}
            </div>
          </td> : null
      }

    </tr>
  )}

export default UnderWriters
