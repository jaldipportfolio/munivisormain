import React from "react"
import DealView from "./Component/DealView"

const DealsMain  = (props) => (
  <div>
    <DealView {...props} option={props.nav3} />
  </div>
)

export default DealsMain
