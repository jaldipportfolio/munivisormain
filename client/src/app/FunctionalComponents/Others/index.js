import React, { Component } from "react"
import Loader from "../../GlobalComponents/Loader"
import OthersView from "./Component"

class Others extends Component {
  render() {
    const { nav2, nav3 } = this.props
    if(nav2) {
      return (
        <div>
          <OthersView {...this.props} option={nav3} />
        </div>
      )
    }
    return <Loader />
  }
}

export default Others
