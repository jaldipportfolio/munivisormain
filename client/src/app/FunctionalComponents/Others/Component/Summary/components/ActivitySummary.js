import React from "react"
import { Link } from "react-router-dom"
import NumberFormat from "react-number-format"
import BorrowerLookup from "Global/BorrowerLookup"
import ThirdPartyLookup from "Global/ThirdPartyLookup"
import { getTranEntityUrl, getTranUserUrl } from "GlobalUtils/helpers"
import {
  SelectLabelInput,
  TextLabelInput
} from "../../../../../GlobalComponents/TextViewBox"

const ActivitySummary = ({tabIndex, transaction = {}, securityType, onChange, onBlur, disabled, onTextInputChange, tranTextChange, canEditTran }) => {

  const borrowerName = {
    _id: "",
    firmName: transaction.actTranBorrowerName || "",
    participantType: "Governmental Entity / Issuer",
    isExisting: true
  }

  const guarantor = {
    _id: "",
    firmName: transaction.actTranGuarantorName || "",
    participantType: "Governmental Entity / Issuer",
    isExisting: true
  }

  const length = transaction && transaction.tranNotes && transaction.tranNotes.length ? transaction && transaction.tranNotes && transaction.tranNotes.length - 1 : 0
  const actNotes = transaction && transaction.tranNotes && transaction.tranNotes.length ? transaction.tranNotes && transaction.tranNotes[length] && transaction.tranNotes[length].note : transaction && transaction.actTranNotes || "--"
  const entityUrl = getTranEntityUrl("Issuer", transaction.actTranClientName, transaction.actTranClientId, "") || ""
  return (
    <div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Client Name</p>
        </div>
        <div className="column">
          {transaction.actTranClientName ?
            <Link to={entityUrl.props.to} tabIndex={tabIndex} style={{fontSize: 15}}>{entityUrl}</Link> : <small>--</small>
          }

        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Issue name (as in OS)</p>
        </div>
        <div className="column">
          <TextLabelInput title="Issue name" name="actTranIssueName" value={tranTextChange.actTranIssueName} placeholder="Issue Name" onBlur={onBlur} onChange={onTextInputChange}
            disabled={!canEditTran || transaction.actTranStatus === "Cancelled" || transaction.actTranStatus === "Closed" || false}  tabIndex={tabIndex + 1}
          />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">
            Project Description (internal)
          </p>
        </div>
        <div className="column">
          <TextLabelInput title="Project Description" name="actTranProjectDescription" value={tranTextChange.actTranProjectDescription} placeholder="Issue Name" tabIndex={tabIndex + 2}
            onBlur={onBlur} onChange={onTextInputChange} disabled={!canEditTran || transaction.actTranStatus === "Cancelled" || transaction.actTranStatus === "Closed" || false}
          />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Borrower or Obligor Name</p>
        </div>
        <div className="column">
          {/* <SelectLabelInput disabled={disabled} list={borrowerOrObligorName || []} name="actTranBorrowerName" value={transaction.actTranBorrowerName || ""}
                            onChange={onChange} inputStyle={{ width: 300 }} /> */}
          <BorrowerLookup
            tabIndex={tabIndex + 3}
            entityName={borrowerName}
            onChange={(e) => onChange({ target: { name: "actTranBorrowerName", value: e.firmName } })}
            type="other"
            isWidth
            notEditable={!canEditTran || transaction.actTranStatus === "Cancelled" || transaction.actTranStatus === "Closed" || false}
            isHide={transaction.actTranBorrowerName && canEditTran}
          />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Guarantor Name</p>
        </div>
        <div className="column">
          {/* <SelectLabelInput disabled={disabled} list={guarantorName || []} name="actTranGuarantorName" value={transaction.actTranGuarantorName || ""}
            onChange={onChange} inputStyle={{ width: 300 }} /> */}
          <ThirdPartyLookup
            tabIndex={tabIndex + 4}
            entityName={guarantor}
            onChange={(e) => onChange({ target: { name: "actTranGuarantorName", value: e.firmName } })}
            type="other"
            isWidth
            notEditable={!canEditTran || transaction.actTranStatus === "Cancelled" || transaction.actTranStatus === "Closed" || false}
            isHide={transaction.actTranGuarantorName && canEditTran}
          />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Schedule highlights</p>
        </div>
        <div className="column">
          <small>
            <p>---</p>
          </small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Principal Amount / Par Value</p>
        </div>
        <div className="column">
          <small>
            {transaction.actTranParAmount ? (
              <NumberFormat className="input is-fullwidth is-small" thousandSeparator style={{ background: "transparent", border: "none" }}
                decimalScale={2} prefix="$" disabled value={transaction.actTranParAmount} />
            ) : ("---")
            }
          </small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Security</p>
        </div>
        <div className="column">
          <SelectLabelInput tabIndex={tabIndex + 5} disabled={!canEditTran || transaction.actTranStatus === "Cancelled" || transaction.actTranStatus === "Closed" || false} list={securityType || []} name="actTranSecurityType" value={transaction.actTranSecurityType || ""}
            onChange={onChange}
          />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Coupon / Rate Type:</p>
        </div>
        <div className="column">
          <small className="innerSummaryTitle">{"--"} Rate </small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Tenor / Maturities</p>
        </div>
        <div className="column">
          <small>---</small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Participants Firm</p>
        </div>
        <div className="column">
          <small>
            {transaction.participants ?
              transaction.participants.map((item, i) => {
                const getPartiTranUserUrl = getTranUserUrl(item.partType, `${item.partType} : ${item.partFirmName} (${item.partContactName})${transaction.participants.length - 1 === i ? "" : ","}`, item.partContactId, "")
                const getPartiEntityUrl = getTranEntityUrl(item.partType, `${item.partType} : ${item.partFirmName}${transaction.participants.length - 1 === i ? "" : ","}`, item.partFirmId, "")
                return(
                  <a href={item.partContactId ? (getPartiTranUserUrl.props.to || "") : (getPartiEntityUrl.props.to || "")}  tabIndex={tabIndex + 6 + i} key={i.toString()}>
                    {item.partContactId ?
                      <div style={{fontSize: 15}}>{getPartiTranUserUrl}</div> :
                      <div style={{fontSize: 15}}>{getPartiEntityUrl}</div>}
                  </a>
                )
              }) : null}
          </small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Notes / Instructions</p>
        </div>
        <div className="column">
          <small>{actNotes || "--"}</small>
        </div>
      </div>
    </div>
  )
}


export default ActivitySummary
