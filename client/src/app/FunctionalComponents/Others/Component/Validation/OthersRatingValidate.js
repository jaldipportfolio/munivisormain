import Joi from "joi-browser"

const AgencyRatingSchema = Joi.object().keys({
  seriesId: Joi.string().label("Series Name").required(),
  seriesName: Joi.string().label("Series Name").required(),
  ratingAgencyName: Joi.string().label("Rating Agency Name").required(),
  longTermRating: Joi.string().label("Long Term Rating").required(),
  longTermOutlook: Joi.string().label("Long Term Out Look").required(),
  shortTermOutlook: Joi.string().label("Short Term Out Look").required(),
  shortTermRating: Joi.string().label("Short Term Rating").required(),
  _id: Joi.string().required().optional(),
})

export const DealsAgencyRatingValidate = (inputTransDetail) => Joi.validate(inputTransDetail, AgencyRatingSchema, { abortEarly: false, stripUnknown:false })

const CepRatingSchema = Joi.object().keys({
  cepName: Joi.string().label("Cep Name").required(),
  seriesId: Joi.string().label("Series Name").required(),
  seriesName: Joi.string().label("Series Name").required(),
  cepType: Joi.string().label("Cep Type").required(),
  longTermRating: Joi.string().label("Long Term Rating").required(),
  longTermOutlook: Joi.string().label("Long Term Out Look").required(),
  shortTermOutlook: Joi.string().label("Short Term Out Look").required(),
  shortTermRating: Joi.string().label("Short Term Rating").required(),
  _id: Joi.string().required().optional(),
})

export const DealsCepRatingValidate = (inputTransDetail) => Joi.validate(inputTransDetail, CepRatingSchema, { abortEarly: false, stripUnknown:false })
