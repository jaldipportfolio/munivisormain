import React from "react"
import {toast} from "react-toastify"
import ProcessChecklist from "../../../../GlobalComponents/ProcessChecklist"
import CONST from "../../../../../globalutilities/consts"
import Loader from "../../../../GlobalComponents/Loader"
import SendEmailModal from "../../../../GlobalComponents/SendEmailModal"
import { sendEmailAlert } from "../../../../StateManagement/actions/Transaction"
import { putOthersTransaction } from "../../../../StateManagement/actions/Transaction/others"

class CheckNTrack extends React.Component {
  constructor() {
    super()
    this.state = {
      checkLists: [],
      checklistId: "",
      participants: [],
      loading: true,
      modalState: false,
      email: {
        category: "",
        message: "",
        subject: "",
      }
    }
  }

  componentWillMount() {
    const {transaction} = this.props
    if (transaction) {
      const participants = transaction && transaction.participants && transaction.participants.map(part => ({
        _id: part.partContactId, // eslint-disable-line
        name: part.partContactName,
        type: part.partType
      })) || []

      this.setState(prevState => ({
        participants,
        checkLists: (transaction && transaction.actTranChecklists) || [],
        loading: false,
        email: {
          ...prevState.email,
          subject: `Transaction - ${transaction.actTranIssueName || transaction.actTranProjectDescription} - Notification`
        }
      }))
    }else {
      this.setState({
        loading: false,
      })
    }
  }

  saveChecklist = (actTranChecklists, checklistId, checklistStatus) => {
    const {checkLists} = this.state
    if (actTranChecklists && Object.keys(actTranChecklists).length) {
      const data = (checkLists && checkLists.length) ? checkLists.find(e => e.id === actTranChecklists.id) : {}
      if(!(data && Object.keys(data).length)) {
        checkLists.push(actTranChecklists)
      }
    }
    this.setState({
      checkLists,
      actTranChecklists,
      checklistId,
      checklistStatus,
      modalState: true
    })
  }

  onConfirmationSave = () => {
    const {email, actTranChecklists, checklistId, checklistStatus } = this.state
    const tranId = this.props.nav2
    const type = this.props.nav1
    let url = window.location.pathname.replace("/","")
    if(checklistId) {
      url = `${url}?cid=${checklistId}`
    }
    const emailParams = {
      tranId,
      type,
      sendEmailUserChoice:true,
      emailParams: {
        url,
        ...email,
      }
    }
    console.log("==============email send to ==============", emailParams)
    this.setState({
      modalState: false
    }, () => {
      putOthersTransaction(this.props.nav3, {actTranChecklists, _id: this.props.nav2, checklistId, checklistStatus}, (res)=> {
        if (res && res.status === 200) {
          toast("Other check-n-track has been updated!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
          this.setState({
            checkLists: (res.data && res.data.actTranChecklists) || [],
          }, async () => {
            await sendEmailAlert(emailParams)
            if (checklistStatus === "archive") {
              this.props.history.push(`/others/${tranId}/check-track`)
            }
          })
        } else {
          toast((res && res.response && res.response.data) || "Something went wrong!",
            { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        }
      })
    })
  }

  onModalChange = (state, name) => {
    if(name === "message"){
      state = {
        email: {
          ...this.state.email,
          ...state,
        }
      }
    }
    this.setState({
      ...state
    })
  }

  render() {
    const {modalState, email, loading} = this.state
    const {participants, onParticipantsRefresh, transaction, tranAction} = this.props
    if(loading) {
      return <Loader/>
    }
    return (
      <div>
        <SendEmailModal modalState={modalState} email={email} participants={participants} onParticipantsRefresh={onParticipantsRefresh} onModalChange={this.onModalChange} onSave={this.onConfirmationSave}/>
        <ProcessChecklist
          checklists={this.state.checkLists}
          totalThresholds={[0.20, 15000]}
          participants={this.state.participants || []}
          onSaveChecklist={this.saveChecklist}
          tenantId={transaction.actTranFirmId}
          isDisabled={!tranAction.canTranStatusEditDoc}
        />
      </div>
    )
  }
}

export default CheckNTrack
