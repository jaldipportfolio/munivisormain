import React from "react"
import {NumberInput, TextLabelInput, SelectLabelInput} from "../../../../../GlobalComponents/TextViewBox"

const PriceCoordinate = ({
  price = {},
  dropDown,
  onChangeItem,
  errors = {},
  onBlur,
  category,
  canEditTran,
  tabIndex,
}) => {

  const onChange = (event) => {
    onChangeItem({
      ...price,
      [event.target.name]: event.target.type === "checkbox"
        ? event.target.checked
        : event.target.value || null
    }, category)
  }

  const onBlurInput = (event) => {
    if (event.target.title && event.target.value) {
      onBlur(category, `${event.target.title || "empty"} change to ${event.target.type === "checkbox"
        ? event.target.checked
        : event.target.value || "empty"}`)
    }
  }

  return (
    <div>

      <div className="columns">
        <NumberInput
          prefix="$"
          disabled={!canEditTran}
          label="Principal"
          required
          error={errors.seriesPrincipal || ""}
          name="seriesPrincipal"
          value={price.seriesPrincipal || 0}
          placeholder="$"
          onChange={onChange}
          onBlur={onBlurInput}
          tabIndex={tabIndex}/>

        <SelectLabelInput
          label="Security Type"
          required
          disabled={!canEditTran}
          error={errors.seriesSecurityType || ""}
          list={dropDown.securityType}
          name="seriesSecurityType"
          value={price.seriesSecurityType}
          onChange={onChange}
          onBlur={onBlurInput}
          tabIndex={tabIndex + 1}/>

        <TextLabelInput
          disabled={!canEditTran}
          label="Security"
          error={errors.seriesSecurity || ""}
          name="seriesSecurity"
          value={price.seriesSecurity || ""}
          onChange={onChange}
          onBlur={onBlurInput}
          tabIndex={tabIndex + 2}/>

        <TextLabelInput
          disabled={!canEditTran}
          label="Security Details"
          error={errors.seriesSecurityDetails || ""}
          name="seriesSecurityDetails"
          value={price.seriesSecurityDetails || ""}
          onChange={onChange}
          onBlur={onBlurInput}
          tabIndex={tabIndex + 3}/>
      </div>

      <div className="columns">
        <TextLabelInput
          label="Dated Date"
          disabled={!canEditTran}
          error={errors.seriesDatedDate || ""}
          // placeholder="$"
          name="seriesDatedDate"
          type="date"
          // value={(price.seriesDatedDate && moment(price.seriesDatedDate).format("YYYY-MM-DD")) || ""}
          value={(price.seriesDatedDate === "" || !price.seriesDatedDate) ? null : new Date(price.seriesDatedDate)}
          onChange={onChange}
          onBlur={onBlurInput}
          tabIndex={tabIndex + 4}/>

        <TextLabelInput
          label="Settlement Date"
          disabled={!canEditTran}
          error={errors.seriesSettlementDate || ""}
          name="seriesSettlementDate"
          type="date"
          // value={(price.seriesSettlementDate && moment(price.seriesSettlementDate).format("YYYY-MM-DD")) || ""}
          value={(price.seriesSettlementDate === "" || !price.seriesSettlementDate) ? null : new Date(price.seriesSettlementDate)}
          onChange={onChange}
          onBlur={onBlurInput}
          tabIndex={tabIndex + 5}/>

        <TextLabelInput
          label="First Coupon"
          disabled={!canEditTran}
          error={errors.seriesFirstCoupon || ""}
          name="seriesFirstCoupon"
          type="date"
          // value={(price.seriesFirstCoupon && moment(price.seriesFirstCoupon).format("YYYY-MM-DD")) || ""}
          value={(price.seriesFirstCoupon === "" || !price.seriesFirstCoupon) ? null : new Date(price.seriesFirstCoupon)}
          onChange={onChange}
          onBlur={onBlurInput}
          tabIndex={tabIndex + 6}/>

        <TextLabelInput
          label="Put Date"
          disabled={!canEditTran}
          error={errors.seriesPutDate || ""}
          name="seriesPutDate"
          type="date"
          // value={(price.seriesPutDate && moment(price.seriesPutDate).format("YYYY-MM-DD")) || ""}
          value={(price.seriesPutDate === "" || !price.seriesPutDate) ? null : new Date(price.seriesPutDate)}
          onChange={onChange}
          onBlur={onBlurInput}
          tabIndex={tabIndex + 7}/>
      </div>

      <div className="columns">
        <TextLabelInput
          label="Record Date"
          disabled={!canEditTran}
          error={errors.seriesRecordDate || ""}
          type="date"
          name="seriesRecordDate"
          // value={(price.seriesRecordDate && moment(price.seriesRecordDate).format("YYYY-MM-DD")) || ""}
          value={(price.seriesRecordDate === "" || !price.seriesRecordDate) ? null : new Date(price.seriesRecordDate)}
          onChange={onChange}
          onBlur={onBlurInput}
          tabIndex={tabIndex + 8}/>

        <SelectLabelInput
          label="Fed Tax"
          disabled={!canEditTran}
          error={errors.seriesFedTax || ""}
          list={dropDown.fadTax}
          name="seriesFedTax"
          value={price.seriesFedTax}
          onChange={onChange}
          onBlur={onBlurInput}
          tabIndex={tabIndex + 9}/>

        <SelectLabelInput
          label="State Tax"
          disabled={!canEditTran}
          error={errors.seriesStateTax || ""}
          list={dropDown.stateTax}
          name="seriesStateTax"
          value={price.seriesStateTax}
          onChange={onChange}
          onBlur={onBlurInput}
          tabIndex={tabIndex + 10}/>

        <SelectLabelInput
          label="AMT"
          disabled={!canEditTran}
          error={errors.seriesPricingAMT || ""}
          list={dropDown.amt}
          name="seriesPricingAMT"
          value={price.seriesPricingAMT}
          onChange={onChange}
          onBlur={onBlurInput}
          tabIndex={tabIndex + 11}/>
      </div>

      <div className="columns">
        <SelectLabelInput
          label="Bank Qualified"
          disabled={!canEditTran}
          error={errors.seriesBankQualified || ""}
          list={dropDown.bankQualified}
          name="seriesBankQualified"
          value={price.seriesBankQualified}
          onChange={onChange}
          onBlur={onBlurInput}
          tabIndex={tabIndex + 12}/>
        <SelectLabelInput
          label="Accrue Form"
          disabled={!canEditTran}
          error={errors.seriesAccrueFrom || ""}
          list={dropDown.accrueFrom}
          name="seriesAccrueFrom"
          value={price.seriesAccrueFrom}
          onChange={onChange}
          onBlur={onBlurInput}
          tabIndex={tabIndex + 13}/>

        <SelectLabelInput
          label="Form"
          disabled={!canEditTran}
          error={errors.seriesPricingForm || ""}
          list={dropDown.form}
          name="seriesPricingForm"
          value={price.seriesPricingForm}
          onChange={onChange}
          onBlur={onBlurInput}
          tabIndex={tabIndex + 14}/>

        <SelectLabelInput
          label="Coupon Frequency"
          disabled={!canEditTran}
          error={errors.seriesCouponFrequency || ""}
          list={dropDown.couponFrequency}
          name="seriesCouponFrequency"
          value={price.seriesCouponFrequency}
          onChange={onChange}
          onBlur={onBlurInput}
          tabIndex={tabIndex + 15}/>
      </div>

      <div className="columns">
        <SelectLabelInput
          label="Day Count"
          disabled={!canEditTran}
          error={errors.seriesDayCount || ""}
          list={dropDown.dayCount}
          name="seriesDayCount"
          value={price.seriesDayCount}
          onChange={onChange}
          onBlur={onBlurInput}
          tabIndex={tabIndex + 16}/>

        <SelectLabelInput
          label="Rate Type"
          disabled={!canEditTran}
          error={errors.seriesRateType || ""}
          list={dropDown.rateType}
          name="seriesRateType"
          value={price.seriesRateType}
          onChange={onChange}
          onBlur={onBlurInput}
          tabIndex={tabIndex + 17}/>

        <SelectLabelInput
          label="Call Feature"
          disabled={!canEditTran}
          error={errors.seriesCallFeature || ""}
          list={dropDown.callFeatures}
          name="seriesCallFeature"
          value={price.seriesCallFeature}
          onChange={onChange}
          onBlur={onBlurInput}
          tabIndex={tabIndex + 18}/>

        <TextLabelInput
          label="Call Date"
          disabled={!canEditTran}
          error={errors.seriesCallDate || ""}
          name="seriesCallDate"
          type="date"
          // value={(price.seriesCallDate && moment(price.seriesCallDate).format("YYYY-MM-DD")) || ""}
          value={(price.seriesCallDate === "" || !price.seriesCallDate) ? null : new Date(price.seriesCallDate)}
          onChange={onChange}
          onBlur={onBlurInput}
          tabIndex={tabIndex + 19}/>
      </div>

      <div className="columns">
        <NumberInput
          prefix="$"
          label="Call Price"
          disabled={!canEditTran}
          error={errors.seriesCallPrice || ""}
          name="seriesCallPrice"
          placeholder="$"
          value={price.seriesCallPrice || 0}
          onChange={onChange}
          onBlur={onBlurInput}
          tabIndex={tabIndex + 20}/>

        <SelectLabelInput
          label="Insurance"
          disabled={!canEditTran}
          error={errors.seriesInsurance || ""}
          list={dropDown.insurance}
          name="seriesInsurance"
          value={price.seriesInsurance}
          onChange={onChange}
          onBlur={onBlurInput}
          tabIndex={tabIndex + 21}/>

        <SelectLabelInput
          label="Underwriter's Inventory"
          disabled={!canEditTran}
          error={errors.seriesUnderwtiersInventory || ""}
          list={dropDown.underwriterInventory}
          name="seriesUnderwtiersInventory"
          value={price.seriesUnderwtiersInventory || 0}
          onChange={onChange}
          onBlur={onBlurInput}
          tabIndex={tabIndex + 22}/>

        <NumberInput
          label="Minimum Denomination"
          disabled={!canEditTran}
          error={errors.seriesMinDenomination || ""}
          name="seriesMinDenomination"
          value={price.seriesMinDenomination || 0}
          onChange={onChange}
          onBlur={onBlurInput}
          tabIndex={tabIndex + 23}/>
      </div>

      <div className="columns">
        <SelectLabelInput
          label="Syndicate Structure"
          disabled={!canEditTran}
          error={errors.seriesSyndicateStructure || ""}
          list={dropDown.syndicateStructure}
          name="seriesSyndicateStructure"
          value={price.seriesSyndicateStructure || 0}
          onChange={onChange}
          onBlur={onBlurInput}
          tabIndex={tabIndex + 24}/>

        <NumberInput
          prefix="$"
          label="Gross Spread"
          disabled={!canEditTran}
          error={errors.seriesGrossSpread || ""}
          name="seriesGrossSpread"
          value={price.seriesGrossSpread || 0}
          placeholder="$/1000"
          onChange={onChange}
          onBlur={onBlurInput}
          tabIndex={tabIndex + 25}/>

        <NumberInput
          prefix="$"
          label="Estimated Takedown"
          disabled={!canEditTran}
          error={errors.seriesEstimatedTakeDown || ""}
          name="seriesEstimatedTakeDown"
          value={price.seriesEstimatedTakeDown || 0}
          placeholder="$/1000"
          onChange={onChange}
          onBlur={onBlurInput}
          tabIndex={tabIndex + 26}/>

        <NumberInput
          prefix="$"
          label="Insurance Fee"
          disabled={!canEditTran}
          error={errors.seriesInsuranceFee || ""}
          name="seriesInsuranceFee"
          value={price.seriesInsuranceFee || 0}
          placeholder="bps"
          onChange={onChange}
          onBlur={onBlurInput}
          tabIndex={tabIndex + 27}/>
      </div>
    </div>
  )
}

export default PriceCoordinate
