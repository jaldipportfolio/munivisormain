import React from "react"

const Series = ({series={}, index, onChangeItem, onSeriesSave, onEditSeries, isEditable, errors = {}, onBlur, category, onRemove, isSaveDisabled, canEditTran, tabIndex}) => {

  const onUserChange = (e) => {
    const newItem = {
      ...series,
      [e.target.name]: e.target.value,
    }
    onChangeItem(newItem, category, index)
  }

  const onBlurInput = (event) => {
    if(event.target.title && event.target.value){
      onBlur(category, `${event.target.title || "empty"} change to ${event.target.type === "checkbox" ? event.target.checked : event.target.value || "empty"}`)
    }
  }

  const onKeySave = (e, index) => {
    if(e.keyCode === 13){
      onSeriesSave(index)
    }
  }

  const onKeyDelete = (e, id, index, seriesName) =>{
    if(e.keyCode === 13){
      onRemove(id, index, seriesName)
    }
  }

  isEditable = (isEditable === index)
  return (
    <tbody>
      <tr style={{border: "1px solid #dbdbdb"}}>
        <td>
          <input title="Series Code"
                 className="input is-small is-link"
                 type="text"
                 placeholder="Series 2018Q3"
                 name="seriesName"
                 value={series.seriesName || ""}
                 onChange={onUserChange}
                 onBlur={onBlurInput}
                 disabled={!isEditable}
                 tabIndex={tabIndex}/>
        </td>
        <td>
          <input title="Description"
                 className="input is-small is-link"
                 name="description"
                 type="text"
                 placeholder="Series 2018 Quarter 3"
                 disabled={!isEditable}
                 value={series.description || ""}
                 onChange={onUserChange}
                 onBlur={onBlurInput}
                 tabIndex={tabIndex + 1}/>
        </td>
        <td>
          <input title="Tag"
                 className="input is-small is-link"
                 name="tag"
                 type="text"
                 disabled={!isEditable}
                 placeholder="For internal purposes"
                 value={series.tag || ""}
                 onChange={onUserChange}
                 onBlur={onBlurInput}
                 tabIndex={tabIndex + 2}/>
        </td>
        {
          canEditTran ?
            <td>
              <div className="field is-grouped">
                <div className="control">
                  <a onClick={isEditable ? () => onSeriesSave(index) : () => onEditSeries(index)}
                     className={`${isSaveDisabled ? "isDisabled" : ""}`}> {/* eslint-disable-line */}
                    <span className="has-text-link">
                      {isEditable ?
                        <i title="Save"
                           tabIndex={tabIndex + 3}
                           className="far fa-save"
                           onKeyDown={(e)=> onKeySave(e,index)}/> :
                        <i className="fas fa-pencil-alt" title="Edit"/>}
                    </span>
                  </a>
                </div>
                <div className="control">
                  <a onClick={() => onRemove(series._id, index, series.seriesName)}
                     className={`${isSaveDisabled ? "isDisabled" : ""}`}> {/* eslint-disable-line */}
                    <span className="has-text-link">
                      <i title="Delete"
                         tabIndex={tabIndex + 4}
                         className="far fa-trash-alt"
                         onKeyDown={(e) => onKeyDelete(e, series._id, index, series.seriesName )}/>
                    </span>
                  </a>
                </div>
              </div>
            </td> : null
        }
      </tr>
    </tbody>
  )}


export default Series
