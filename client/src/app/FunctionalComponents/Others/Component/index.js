import React from "react"
import { connect } from "react-redux"
import { NavLink } from "react-router-dom"
import Loader from "../../../GlobalComponents/Loader"
import { fetchOthersTransaction } from "../../../StateManagement/actions/Transaction/others"
import Summary from "./Summary"
import OtherDetails from "./Details/OthersDetails"
import Participants from "./Participants"
import Pricing from "./Pricing"
import Rating from "./Ratings"
import Documents from "./Documents"
import CheckAndTrack from "./CheckAndTrack"
import Audit from "../../../GlobalComponents/Audit"
import CONST, { activeStyle } from "../../../../globalutilities/consts"
import { fetchParticipantsAndOtherUsers } from "../../../StateManagement/actions/Transaction"
import { makeEligibleTabView } from "../../../../globalutilities/helpers"

class OthersView extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      tabs: [],
      loading: true,
      transaction: {},
      tranAction: {
        canEditTran: true,
        canViewTran: true
      },
      TABS: [
        { path: "summary", label: "Summary" },
        { path: "details", label: "Details" },
        { path: "participants", label: "Participants" },
        { path: "pricing", label: "Pricing" },
        { path: "ratings", label: "Rating" },
        { path: "documents", label: "Documents" },
        { path: "check-track", label: "Check-n-Track" },
        { path: "audit-trail", label: "Activity Log" }
      ]
    }
  }

  async componentWillMount() {
    const { user, nav, nav1, nav2, nav3, loginEntity } = this.props
    const { TABS } = this.state
    const tranId = nav2 || ""

    let allEligibleNav = []
    if (nav && Object.keys(nav) && Object.keys(nav).length) {
      allEligibleNav =  nav[nav1] ? Object.keys(nav[nav1]).filter(key => nav[nav1][key]) : []
    }
    console.log("transaction eligible tab ", nav[nav1])

    if (tranId) {
      fetchOthersTransaction(nav3, tranId, async transaction => {
        if (transaction && transaction.actTranClientId) {
          const isEligible = await makeEligibleTabView(
            user,
            tranId,
            TABS,
            allEligibleNav,
            transaction.actTranClientId,
            transaction.actTranFirmId,
          )

          isEligible.tranAction.canEditTran = CONST.oppDecline.indexOf(transaction.actTranStatus) !== -1 ? false : isEligible.tranAction.canEditTran

          isEligible.tranAction.canTranStatusEditDoc = CONST.oppDecline.indexOf(transaction.actTranStatus) === -1 && isEligible.tranAction.canEditDocument

          // if(transaction.participants && loginEntity && loginEntity.relationshipToTenant !== "Self"){
          //   transaction.participants = transaction.participants.filter(part => part.partFirmId === loginEntity.entityId)
          // }

          console.log("=================tranAction================", isEligible)

          if (isEligible && isEligible.tranAction && (!isEligible.tranAction.canEditTran && !isEligible.tranAction.canViewTran)) {
            this.props.history.push("/dashboard")
          } else {
            if (isEligible.tranAction.view.indexOf(nav3) === -1) {
              const view = isEligible.tranAction.view[0] || ""
              this.props.history.push(`/${nav1}/${tranId}/${view}`)
            } else {
              const participants = await fetchParticipantsAndOtherUsers(nav2)
              this.setState({
                tranId,
                transaction,
                participants,
                loading: false,
                tranAction: isEligible.tranAction,
                tabs: isEligible.tabs
              })
            }
          }
        } else {
          this.props.history.push("/dashboard")
        }
      })
    }
  }

  onStatusChange = status => {
    const { tranAction } = this.state
    tranAction.canEditTran =
      CONST.oppDecline.indexOf(status) !== -1 ? false : tranAction.canEditTran
    this.setState({
      tranAction
    })
  }

  onParticipantsRefresh = async () => {
    this.setState({
      participants: await fetchParticipantsAndOtherUsers(this.props.nav2)
    })
  }

  renderViewSelection = (tranId, option) => (
    <nav className="tabs is-boxed">
      <ul>{this.renderTabs(this.state.tabs, tranId, option)}</ul>
    </nav>
  )

  renderTabs = (tabs, tranId, option) =>
    tabs.map(t => (
      <li key={t.path}>
        <NavLink to={`/others/${tranId}/${t.path}`} activeStyle={activeStyle}>
          {t.label}
        </NavLink>
      </li>
    ))

  renderSelectedView = (tranId, option) => {
    const { transaction, tranAction, participants } = this.state
    switch (option) {
    case "summary":
      return (
        <Summary
          {...this.props}
          transaction={transaction}
          tranAction={tranAction}
          participants={participants}
          onParticipantsRefresh={this.onParticipantsRefresh}
          onStatusChange={this.onStatusChange}
        />
      )
    case "details":
      return (
        <OtherDetails
          {...this.props}
          transaction={transaction}
          tranAction={tranAction}
          participants={participants}
          onParticipantsRefresh={this.onParticipantsRefresh}
        />
      )
    case "participants":
      return (
        <Participants
          {...this.props}
          transaction={transaction}
          tranAction={tranAction}
          participants={participants}
          onParticipantsRefresh={this.onParticipantsRefresh}
        />
      )
    case "pricing":
      return (
        <Pricing
          {...this.props}
          transaction={transaction}
          tranAction={tranAction}
          participants={participants}
          onParticipantsRefresh={this.onParticipantsRefresh}
        />
      )
    case "ratings":
      return (
        <Rating
          {...this.props}
          transaction={transaction}
          tranAction={tranAction}
          participants={participants}
          onParticipantsRefresh={this.onParticipantsRefresh}
        />
      )
    case "documents":
      return (
        <Documents
          {...this.props}
          transaction={transaction}
          tranAction={tranAction}
          participants={participants}
          onParticipantsRefresh={this.onParticipantsRefresh}
        />
      )
    case "check-track":
      return (
        <CheckAndTrack
          {...this.props}
          transaction={transaction}
          tranAction={tranAction}
          participants={participants}
          onParticipantsRefresh={this.onParticipantsRefresh}
        />
      )
    case "audit-trail":
      return (
        <Audit
          {...this.props}
          transaction={transaction}
          tranAction={tranAction}
        />
      )
    default:
      return <p>{option}</p>
    }
  }

  render() {
    const { option } = this.props
    const { tranId, loading } = this.state

    if (loading) {
      return <Loader />
    }

    return (
      <div>
        <div className="hero is-link">
          <div className="hero-foot hero-footer-padding">
            <div className="container">
              {this.renderViewSelection(tranId, option)}
            </div>
          </div>
        </div>
        <section id="main">{this.renderSelectedView(tranId, option)}</section>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
  nav: state.nav || {},
  loginEntity: (state.auth && state.auth.loginDetails && state.auth.loginDetails.userEntities
    && state.auth.loginDetails.userEntities.length && state.auth.loginDetails.userEntities[0]) || {}
})

export default connect(
  mapStateToProps,
  null
)(OthersView)
