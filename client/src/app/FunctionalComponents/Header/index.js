import React from "react"
import {Link} from "react-router-dom"

class Header extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      activeTab: 1,
    }
  }

    onTab = (activeTab) => {
      this.setState({
        activeTab,
      })
    }

    render() {
      const { activeTab } = this.state
      return(
        <section className="hero is-small">
          <div className="hero-body">
            <div className="columns is-vcentered">
              <div className="column">
                <p className="subtitle" />
                <nav className="breadcrumb has-arrow-separator is-small is-right" aria-label="breadcrumbs">
                  <ul>
                    <li>
                      <Link to="#">Transactions</Link>
                    </li>
                    <li>
                      <Link to="#">RFP Process for Client</Link>
                    </li>
                    <li className="is-active">
                      <Link to="#" aria-current="page">Distribute</Link>
                    </li>
                  </ul>
                </nav>
                <p />
              </div>
            </div>
          </div>
          <div className="hero-foot">
            <div className="container">
              <nav className="tabs is-boxed">
                <ul>
                  <li className={`${activeTab === 1 && "is-active"}`}  onClick={() => this.onTab(1)}>
                    <Link to="/createTrans">
                      <span>Add New Transaction</span>
                    </Link>
                  </li>
                  <li className={`${activeTab === 2 && "is-active"}`}  onClick={() => this.onTab(2)}>
                    <Link to="/rfpDistribute">
                      <span>Distribute</span>
                    </Link>
                  </li>
                  <li className={`${activeTab === 3 && "is-active"}`}  onClick={() => this.onTab(3)}>
                    <Link to="/rfpManage">
                      <span>Manage</span>
                    </Link>
                  </li>
                  <li className={`${activeTab === 4 && "is-active"}`}  onClick={() => this.onTab(4)}>
                    <Link to="/rfpRating">
                      <span>Check-n-Track</span>
                    </Link>
                  </li>
                  <li className={`${activeTab === 5 && "is-active"}`}  onClick={() => this.onTab(5)}>
                    <Link to="/rfpAudit">
                      <span>Activity Log</span>
                    </Link>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
        </section>
      )
    }
}

export default Header