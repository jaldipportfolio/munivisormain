import React from "react"
import { connect } from "react-redux"
import {
  generateTaskManagementProjects,
  testGeneratorConfig
} from "./../../../StateManagement/initialState"
import ProjectList from "./../components/ProjectList"

const createProjectActionCreator = numProjectsToBeAdded => ({
  type: "ADD_PROJECT",
  payload: generateTaskManagementProjects({
    ...testGeneratorConfig,
    ...{
      numProjects: numProjectsToBeAdded
    }
  })
})
/* Have a store listener that listens to changes in the state */

const mapStateToProps = (state, props) => {
  console.log("The state received by Map state to props", state)
  console.log("The props passed to the component are as follows", props)
  return {
    taskProjects: state.taskProjects
  }
}

/* Bind the action creator to props so that they can dispatched on a given event */
const mapDispatchToProps = {
  createProjectActionCreator
}

/* Create the component that retrieves the mapped props and shows the consolidated Information */
const ProjectContainer = props => {
  console.log("THIS HAS ALL THE PROPS FROM THE CONNECTED COMPONENT", props)
  const { taskProjects } = props
  if(!taskProjects) {
    return <div>There are no projects to display</div>
  }
  return (
    <div>
      <h1>This is where all the projects Information is going to </h1>
      <ProjectList projects={taskProjects} />
    </div>
  )
}

const ProjectContainerConnected = connect(mapStateToProps, mapDispatchToProps)(
  ProjectContainer
)

export default ProjectContainerConnected
