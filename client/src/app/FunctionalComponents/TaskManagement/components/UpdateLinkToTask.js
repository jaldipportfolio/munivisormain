import React from "react"
import {DropdownList} from "react-widgets"
import DocUpload from "../../docs/DocUpload"

const transInfoResult = (transInfo)=>{
  const resposeData = []
  transInfo.forEach(item => {
    resposeData.push(item)
  })
  return resposeData
}

const entitiesTransform = (acitivities,entities)=>{
  const itemData=[]
  if(Object.keys(acitivities).length>0){
    Object.values(entities[acitivities.activityId]).forEach(entityItem=>{
      itemData.push({activityType : acitivities.activityType,
        activityName:acitivities.activityDescription,
        activityId:acitivities.activityId,
        entityId:entityItem.participantEntityId,
        ...entityItem
      })
    })
  }
  return itemData
}
const relatedUsersTransform = (entities,relatedUsers)=>{
  const itemData=[]
  if(Object.keys(entities).length>0){
    if(Object.keys(relatedUsers[entities.activityId]).length>0 && relatedUsers[entities.activityId][entities.entityId]!==undefined)
      Object.values(relatedUsers[entities.activityId][entities.entityId]).map(userItem=>{
        itemData.push({...userItem})
      })
  }
  return itemData
}
const UpdateLinkToTask = (props)=>(
  <div>
    <p className="title innerPgTitle">Link the task as applicable</p>
    {props.taskMgt.taskType!=="general" && <div className="columns">
      <div className="column">
        <p className="dashExpLblVal">Related Activity</p>
        <DropdownList
          data={ props.taskMgt.taskType !=="" && props.relatedActivicties && props.relatedActivicties[props.taskMgt.taskType].tranInfo ?  transInfoResult(Object.values(props.relatedActivicties[props.taskMgt.taskType].tranInfo)):[]}
          name = "taskRelatedActivities"
          textField={(row) =>row.activityDescription }
          groupBy={(row) =>row.activityType}
          value={ props.taskMgt.taskRelatedActivities||""}
          onChange={(val)=>{
            const event={
              target:{
                name:"taskRelatedActivities",
                value:val
              }}
            props.onChangeTask(event,val)
          }}
        />
        {props.taskMgtErrors && props.taskMgtErrors.taskRelatedActivities && <small className="text-error">{props.taskMgtErrors.taskRelatedActivities}</small>}
      </div>
    </div>
    }
    { props.taskMgt.taskType!=="general" && <div className="columns">
      <div className="column">
        <p className="dashExpLblVal">Related Entity</p>
        <DropdownList
          data={props.taskMgt.taskType !=="" && props.relatedActivicties && props.taskMgt.taskRelatedActivities ? entitiesTransform(props.taskMgt.taskRelatedActivities,props.relatedActivicties[props.taskMgt.taskType].tranRelatedEntities):[]}
          name = "taskRelatedEntities"
          textField={(row) => row ? (`${row.activityType} | ${row.activityName} | ${row.firmName}`): ""}
          groupBy={(row)=>row.activityType}
          value={props.taskMgt.taskRelatedEntities||""}
          onChange={(val)=>{
            const event={
              target:{
                name:"taskRelatedEntities",
                value:val
              }}
            props.onChangeTask(event,val)
          }}
        />
        {props.taskMgtErrors && props.taskMgtErrors.taskRelatedEntities && <small className="text-error">{props.taskMgtErrors.taskRelatedEntities}</small>}
      </div>
    </div>}
    { props.taskMgt.taskType!=="general"
      ?
      <div className="columns">
        <div className="column">
          <p className="dashExpLblVal">Assigned To</p>
          <DropdownList
            data={props.taskMgt.taskType !=="" && props.relatedActivicties && props.taskMgt.taskRelatedActivities && props.taskMgt.taskRelatedEntities ? relatedUsersTransform(props.taskMgt.taskRelatedEntities,props.relatedActivicties[props.taskMgt.taskType].tranRelatedUsers):[]}
            name = "taskAssignees"
            textField={(row) => row ? (`${row.userEntityName} | ${row.userFirstName} ${row.userLastName}`):""}
            value={props.taskMgt.taskAssignees||""}
            onChange={(val)=>{
              const event={
                target:{
                  name:"taskAssignees",
                  value:val
                }}
              props.onChangeTask(event)
            }}
          />
          {props.taskMgtErrors && props.taskMgtErrors.taskAssignees && <small className="text-error">{props.taskMgtErrors.taskAssignees}</small>}
        </div>
      </div>
      :
      <div className="columns">
        <div className="column">
          <p className="dashExpLblVal">Assigned To</p>
          <DropdownList
            data={ props.relatedUsers ? props.relatedUsers :[]}
            name = "taskAssignees"
            textField={(row) => row ? `${row.userEntityName} | ${row.userFirstName} ${row.userLastName}`:""}
            groupBy={({userEntityName}) => userEntityName}
            value={props.taskMgt.taskAssignees || ""}
            onChange={(val)=>{
              const event={
                target:{
                  name:"taskAssignees",
                  value:val
                }}
              props.onChangeTask(event)
            }}
          />
          {props.taskMgtErrors && props.taskMgtErrors.taskAssignees && <small className="text-error">{props.taskMgtErrors.taskAssignees}</small>}
        </div>
      </div>
    }
    <div className="columns">
      <div className="column is-full">
        <p className="dashExpLblVal">Notes / Instructions</p>
        <textarea className="textarea" placeholder="" name="taskNotes" value={props.taskMgt.taskNotes} onChange = {props.onChangeTask}/>
        {props.taskMgtErrors && props.taskMgtErrors.taskNotes && <small className="text-error">{props.taskMgtErrors.taskNotes}</small>}
      </div>
    </div>
    <div className="control">
      <DocUpload
        bucketName={props.bucketName}
        onUploadSuccess={props.getBidBucket}
        tenantId={props.tenantId}
        contextType={props.contextType}
        contextId={props.contextId}
        showFeedback bidIndex="0"
      />
    </div>
    {
      props.taskMgt.taskRelatedDocuments.map((doc,idx)=>(
        <div className="control" key={`1${idx}`}>
          <p className="dashExpLblVal">{doc.docFileName}</p>
          {doc.docId &&
              <span>
                <span onClick={() => props.onFileAction("open",doc.docId)}><i className="fa fa-download link-pointer"/></span>&nbsp;
                <span onClick={() => props.onFileAction("version",doc.docId)}><i className="fa fa-plus link-pointer" /></span>
              </span>
          }
        </div>
      ))
    }
    <br/>
    <hr/>
    <div className="columns">
      <div className="column is-full">
        <div className="field is-grouped">
          <div className="control">
            <a href="index.html">
              <button className="button is-link is-small"
                onClick = {props.handleSubmit}
              >Save</button>
            </a>
          </div>
          <div className="control">
            <button className="button is-light is-small">Cancel</button>
          </div>
        </div>
      </div>
    </div>
  </div>
)

export default UpdateLinkToTask