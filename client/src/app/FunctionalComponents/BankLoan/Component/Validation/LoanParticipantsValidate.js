import Joi from "joi-browser"

const loanPartDistSchema = Joi.object().keys({
  partFirmId: Joi.string().required().optional(),
  partType: Joi.string().required(),
  partFirmName: Joi.string().required().optional(),
  partContactId: Joi.string().required().optional(),
  partContactName: Joi.string().required(),
  partContactTitle: Joi.string().required(),
  partContactEmail:  Joi.string().email().required(),
  partContactAddrLine1: Joi.string().allow("").optional(),
  partContactAddrLine2: Joi.string().allow("").optional(),
  participantState: Joi.string().allow("").optional(),
  partContactPhone: Joi.string().required(),
  partContactAddToDL: Joi.boolean().required().optional(),
  _id: Joi.string().required().optional(),
})

export const LoanParticipantsValidate = (inputTransDistribute) => Joi.validate(inputTransDistribute, loanPartDistSchema, { abortEarly: false, stripUnknown:false })
