import React from "react"
import {TextLabelInput} from "../../../../../GlobalComponents/TextViewBox"

const LinkCusIp = ({ index, item={}, onChangeItem, errors={}, isEditable, isCusIpSaveDisabled, onRemove, onBlur, category, onEditLinkCusIp, onLinkCusIpSave, tranAction, tabIndex}) => {

  const onChange = (event) => {
    if(event.target.name === "cusip") {
      event.target.value = event.target.value ? event.target.value.toUpperCase() : ""
    }
    onChangeItem({
      ...item,
      [event.target.name]: event.target.type === "checkbox" ? event.target.checked : event.target.value,
    }, category, index)
  }

  const onBlurInput = (event) => {
    if(event.target.title && event.target.value) {
      onBlur(category, `${event.target.title || "empty"} change to ${event.target.type === "checkbox" ? event.target.checked : event.target.value || "empty"}`)
    }
  }

  const onBtnSaveClick = (e) =>{
    if(e.keyCode === 13){
      onLinkCusIpSave(category, item, index)
    }
  }

  const onBtnDeleteClick = (e) => {
    if(e.keyCode === 13){
      onRemove(item._id, index, item.cusip)
    }
  }

  const onBtnEditClick = (e) =>{
    if(e.keyCode === 13){
      onEditLinkCusIp(index, category)
    }
  }

  isEditable = (isEditable === index)

  return (
    <tbody>
      <tr style={{border: "1px solid #dbdbdb"}}>
        <td>
          <TextLabelInput tabIndex={tabIndex} title="CUSIP" error={(errors && errors.cusip) ? "Required (Alphanumeric character length 6 to 9 accepted)" : ""} name="cusip" value={item.cusip || ""} onChange={onChange} onBlur={onBlurInput} disabled={!isEditable || !tranAction.canEditTran} />
        </td>
        <td>
          <TextLabelInput tabIndex={tabIndex + 1} title="Description" name="description" value={item.description || ""} onChange={onChange} onBlur={onBlurInput} disabled={!isEditable || !tranAction.canEditTran} />
        </td>
        <td>
          <TextLabelInput tabIndex={tabIndex + 2} title="Tag" name="tag" value={item.tag || ""} onChange={onChange} onBlur={onBlurInput} disabled={!isEditable || !tranAction.canEditTran}/>
        </td>

        <td>
          {
            tranAction && tranAction.canEditTran ?
              <div className="field is-grouped">
                <div className="control">
                  <a
                    onClick={isEditable ? () => onLinkCusIpSave(category, item, index) : () => onEditLinkCusIp(index, category)}
                    className={`${isCusIpSaveDisabled ? "isDisabled" : ""}`}>
                    <span className="has-text-link">
                      {isEditable ? <i className="far fa-save" title="Save" tabIndex={tabIndex + 3} onKeyDown={onBtnSaveClick}/> : <i className="fas fa-pencil-alt" title="Edit" tabIndex={tabIndex + 4} onKeyDown={onBtnEditClick}/>}
                    </span>
                  </a>
                </div>
                <div className="control">
                  <a onClick={() => onRemove(item._id, index, item.cusip)} className={`${isCusIpSaveDisabled ? "isDisabled" : ""}`}>
                    <span className="has-text-link">
                      <i className="far fa-trash-alt" title="Delete" tabIndex={tabIndex + 5} onKeyDown={onBtnDeleteClick}/>
                    </span>
                  </a>
                </div>
              </div> : null
          }
        </td>
      </tr>
    </tbody>
  )}

export default LinkCusIp
