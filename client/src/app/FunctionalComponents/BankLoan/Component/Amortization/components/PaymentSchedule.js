import React from "react"
import moment from "moment"
import {TextLabelInput, NumberInput} from "../../../../../GlobalComponents/TextViewBox"

const PaymentSchedule = ({amort={}, index, onChangeItem, isSaveDisabled, onSave, onEdit, isEditable, errors = {}, onBlur, category, onCancel, onRemove}) => {

  const onChange = (e) => {
    const newItem = {
      ...amort,
      [e.target.name]: e.target.value,
    }
    onChangeItem(newItem, category, index)
  }

  const onBlurInput = (event) => {
    onBlur(category, `${event.target.name || "empty"} change to ${event.target.type === "checkbox" ? event.target.checked : event.target.value || "empty"}`)
  }
  isEditable = (isEditable === index)
  return (
    <tbody>
      <tr style={{border: "1px solid #dbdbdb"}}>
        <td>
          <TextLabelInput error= {(errors.reductionDate && "Required (must be larger than or equal to today or created date)") || ""} name="reductionDate" type="date" value={(amort.reductionDate && moment(new Date(amort.reductionDate).toISOString().substring(0, 10)).format("YYYY-MM-DD")) || ""} onChange={onChange} onBlur={onBlurInput} disabled={!isEditable}/>
        </td>
        <td>
          <NumberInput prefix="$" decimalScale={2} placeholder="$" error= {errors.prinAmountReduction || ""} name="prinAmountReduction" value={amort.prinAmountReduction || ""} onChange={onChange} onBlur={onBlurInput} disabled={!isEditable}/>
        </td>
        <td>
          <NumberInput prefix="$" decimalScale={2} placeholder="$" error= {errors.reviedPrinAmount || ""} name="reviedPrinAmount" value={amort.reviedPrinAmount || ""} onChange={onChange} onBlur={onBlurInput} disabled={!isEditable}/>
        </td>
        <td>
          <div className="field is-grouped">
            <div className="control">
              <a onClick={isEditable ? () => onSave(category, amort, index) : () => onEdit(amort._id, index)} className={`${isSaveDisabled ? "isDisabled" : ""}`}> {/*eslint-disable-line*/}
                <span className="has-text-link">
                  {isEditable ? <i className="far fa-save"/> : <i className="fas fa-pencil-alt" />}
                </span>
              </a>
            </div>
            <div className="control">
              <a onClick={isEditable ? () => onCancel(category) :() => onRemove(amort._id, category, index)} className={`${isSaveDisabled ? "isDisabled" : ""}`}>
                <span className="has-text-link">
                  {isEditable ? <i className="fa fa-times"/> : <i className="far fa-trash-alt"/>}
                </span>
              </a>
            </div>
            {amort.isNew && !isEditable ? <small className="text-error">New(Not Save)</small> : null}
          </div>
        </td>
      </tr>
    </tbody>
  )}


export default PaymentSchedule
