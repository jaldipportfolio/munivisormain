import React from "react"
import { Link } from "react-router-dom"
import NumberFormat from "react-number-format"
import ThirdPartyLookup from "Global/ThirdPartyLookup"
import BorrowerLookup from "Global/BorrowerLookup"
import { getTranEntityUrl, getTranUserUrl } from "GlobalUtils/helpers"
import { SelectLabelInput, TextLabelInput } from "../../../../../GlobalComponents/TextViewBox"

const ActivitySummary = ({ transaction = {}, securityType, onChange, onTextInputChange, onBlur, tranTextChange, canEditTran, tabIndex }) => {

  const borrowerName = {
    _id: "",
    firmName: transaction.actTranBorrowerName || "",
    participantType: "Governmental Entity / Issuer",
    isExisting: true
  }

  const obligorName = {
    _id: "",
    firmName: transaction.actTranObligorName || "",
    participantType: "Governmental Entity / Issuer",
    isExisting: true
  }

  const length = transaction && transaction.tranNotes && transaction.tranNotes.length ? transaction && transaction.tranNotes && transaction.tranNotes.length - 1 : 0
  const actNotes = transaction && transaction.tranNotes && transaction.tranNotes.length ? transaction.tranNotes && transaction.tranNotes[length] && transaction.tranNotes[length].note : transaction && transaction.actTranNotes || "--"
  const entityUrl = getTranEntityUrl("Issuer", transaction.actTranClientName, transaction.actTranClientId, "")

  const tabInd = (transaction && transaction.bankLoanParticipants && transaction.bankLoanParticipants.length) + tabIndex + 5

  return (
    <div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Client Name</p>
        </div>
        <div className="column">
          {transaction.actTranClientName ? <Link to={entityUrl.props.to} style={{fontSize: 15}} tabIndex={tabIndex}>{entityUrl}</Link> : <small>--</small>}
        </div>
      </div>
      {/* <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Obligor</p>
        </div>
        <div className="column">
          {transaction.bankLoanSummary && transaction.bankLoanSummary.actTranObligorName ? <a><small>{transaction.bankLoanSummary.actTranObligorName}</small></a> : <small>--</small>}
        </div>
      </div> */}
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Issue name (as in OS)</p>
        </div>
        <div className="column">
          <TextLabelInput tabIndex={tabIndex + 1} title="Issue name" name="actTranIssueName" value={tranTextChange.actTranIssueName} onBlur={onBlur} onChange={onTextInputChange}
            disabled={!canEditTran || transaction.actTranStatus === "Cancelled" || transaction.actTranStatus === "Closed" || false} />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Project Description (internal)</p>
        </div>
        <div className="column">
          <TextLabelInput tabIndex={tabIndex + 2} title="Project Description" name="actTranProjectDescription" value={tranTextChange.actTranProjectDescription} onBlur={onBlur} onChange={onTextInputChange}
            disabled={!canEditTran || transaction.actTranStatus === "Cancelled" || transaction.actTranStatus === "Closed" || false} />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Guarantor Name</p>
        </div>
        <div className="column">
          {/* <TextLabelInput title="Guarantor Name" name="actTranObligorName" value={tranTextChange.actTranObligorName} style={{ width: 300 }} placeholder="" onBlur={(e) => onBlur(e, "actTranObligorName")}
            onChange={onTextInputChange} disabled={!canEditTran || false} /> */}
          <ThirdPartyLookup
            tabIndex={tabIndex + 3}
            entityName={obligorName}
            onChange={(e) => onChange({ target: { name: "actTranObligorName", value: e.firmName } })}
            type="other"
            isWidth
            notEditable={!canEditTran || false}
            isHide={transaction.actTranObligorName && canEditTran}
          />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Borrower or Obligor Name</p>
        </div>
        <div className="column">
          {/* <DropdownList filter groupBy={(row) => row.type} style={{ fontSize: 12, width: 300 }} name="actTranBorrowerName" value={transaction.actTranBorrowerName || ""} data={dropDown.borrower || []}
                      message="select borrower" textField="name" valueField="id" key="name" onChange={(e) => onChange({ target: { name: "actTranBorrowerName", value: e.borOblFirmName } })} disabled={!canEditTran || false} /> */}
          <BorrowerLookup
            tabIndex={tabIndex + 4}
            entityName={borrowerName}
            onChange={(e) => onChange({ target: { name: "actTranBorrowerName", value: e.firmName } })}
            type="other"
            isWidth
            notEditable={!canEditTran || false}
            isHide={transaction.actTranBorrowerName && canEditTran}
          />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Borrowing Form</p>
        </div>
        <div className="column">
          <small>{(transaction.bankLoanSummary && transaction.bankLoanSummary.actTranBorrowerName) || "--"}</small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <small className="multiExpLbl">Participants</small>
        </div>
        <div className="column">
          <small className="innerSummaryTitle">RFP Respondents (if applicable)</small>
          <small className="innerSummaryTitle">
            {transaction.bankLoanParticipants
              ? transaction.bankLoanParticipants.map((item, i) => {
                const userUrl = getTranUserUrl(item.partType, `${item.partType} : ${item.partFirmName} (${item.partContactName})`, item.partContactId, "")
                const entityUrlList = getTranEntityUrl(item.partType, `${item.partType} : ${item.partFirmName}`, item.partFirmId, "")
                return(
                  <Link to={item.partContactId ? (userUrl.props.to || "") : (entityUrlList.props.to || "")} key={i.toString()} tabIndex={tabIndex + i + 5}>
                    {item.partContactId ? (
                        <div style={{fontSize: 15}}>{userUrl}</div>
                      ) :
                      <div style={{fontSize: 15}}>{entityUrlList}</div>
                    }
                  </Link>
                )
              })
              : null}
          </small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Schedule highlights</p>
        </div>
        <div className="column">
          <small>
            <p>--</p>
          </small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Lead Advisors</p>
        </div>
        <div className="column">
          <small className="innerSummaryTitle">{transaction.actTranFirmLeadAdvisorName || "--"}</small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Principal Amount / Par Value</p>
        </div>
        <div className="column">
          <small> {transaction.bankLoanTerms && transaction.bankLoanTerms.parAmount ?
            <NumberFormat className="input is-fullwidth is-small" thousandSeparator style={{ background: "transparent", border: "none"}} decimalScale={2} prefix="$" disabled  value={transaction.bankLoanTerms.parAmount} />
            :"---"}
          </small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Security</p>
        </div>
        <div className="column">
          <SelectLabelInput list={securityType || []} tabIndex={tabInd} name="actTranSecurityType" value={(transaction && transaction.actTranSecurityType) || ""}
            onChange={onChange} disabled={!canEditTran || false} />
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Coupon / Rate Type:</p>
        </div>
        <div className="column">
          <small className="innerSummaryTitle">{/* {(transaction.bankLoanTerms && transaction.bankLoanTerms.paymentType) || "--"} */} -- Rate
          </small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Tenor / Maturities</p>
        </div>
        <div className="column">
          <small>--</small>
        </div>
      </div>
      <div className="columns">
        <div className="column is-2">
          <p className="multiExpLbl">Notes / Instructions</p>
        </div>
        <div className="column">
          <small className="innerSummaryTitle">{actNotes || "--"}</small>
        </div>
      </div>
    </div>
  )
}


export default ActivitySummary
