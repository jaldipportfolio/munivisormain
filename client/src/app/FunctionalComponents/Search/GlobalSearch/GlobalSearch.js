import React from "react"
import { ReactiveBase, ReactiveComponent } from "@appbaseio/reactivesearch"
import axios from "axios"
import {toast} from "react-toastify"
import queryString from "query-string"

import {
  saveMasterListSearchPrefs,
  fetchMasterListSearchPrefs,
} from "AppState/actions/AdminManagement/admTrnActions"

import Layout from "../Layout/Layout"
import Container from "../Layout/Container"
import SearchContainer from "../Layout/SearchContainer"
import SideBar from "../Layout/Sidebar"
import Content from "../Layout/Content"
import FilterLabel from "../Layout/FilterLabel"
import SearchBar from "./SearchBar"
import Tab from "./Tab"
import Filters from "./Filters"
import Result from "./Results"
import Loader from "../../../GlobalComponents/Loader"

import { ELASTIC_SEARCH_URL } from "../../../../constants"
import { tabsConfig } from "./searchConfig"
import { muniApiBaseURL } from "../../../../globalutilities/consts"
import { globalSearchEntitlement } from "../utils/entitlement-query"

const calendarStyle = {
  paddingTop: 0,
  paddingLeft: 10,
  paddingRight: 15,
  left: 0,
  width: "100%"
}

const style = { paddingTop: 80, paddingLeft: 10, paddingRight: 15 }

class GlobalSearch extends React.Component {
  state = {
    currentTab: 0,
    isFetching: true,
    defaultQuery: null,
    isFetchingUrls: true,
    isSideBarOpen: false,
    urls: null,
    filterTimeStamp: Date.now(),
    filters: {},
  }

  componentDidMount() {
    const params = queryString.parse(this.props.history.location.search)
    this.setState({
      currentTab: parseInt(params.index, 10) || 0,
      isSideBarOpen: parseInt(params.index, 10) !== 4 && window.innerWidth >= 992
    },() => {
      this.fetchEntitlements()
      this.fetchUrls()
      this.loadDefaultSearch()
    })
  }

  componentWillUnmount() {
    this.isCancelled = true
  }

  fetchUrls = async () => {
    try {
      const {
        auth: {
          loginDetails: { relatedFaEntities }
        }
      } = this.props

      const res = await axios({
        method: "GET",
        url: `${muniApiBaseURL}common/geturlsfortenant/${
          relatedFaEntities[0].entityId
        }`,
        headers: { Authorization: localStorage.getItem("token") }
      })

      const { urlObject } = res.data

      if (!this.isCancelled) {
        this.setState({
          urls: urlObject
        },() => this.setState({isFetchingUrls: false}))
      }
    } catch (err) {
      if (!this.isCancelled) {
        this.setState({
          isFetchingUrls: false
        })
      }
      console.error(err)
    }
  }

  handleTabChange = newTabIndex => {
    const {currentTab} = this.state
    if(newTabIndex === currentTab) return
    this.props.history.replace({ pathname: "dashboard", search: `?index=${newTabIndex}`})
  }

  fetchEntitlements = async () => {
    try {
      const {
        auth: {
          userEntities: { relationshipToTenant }
        }
      } = this.props
      const res = await axios({
        method: "GET",
        url: `${muniApiBaseURL}entitlements/nav`,
        headers: { Authorization: localStorage.getItem("token") }
      })

      const { data } = res.data
      const defaultQuery = globalSearchEntitlement(
        relationshipToTenant,
        data.all || []
      )

      if (!this.isCancelled) {
        this.setState({
          defaultQuery
        },() => this.setState({isFetching: false}))
      }
    } catch (err) {
      if (!this.isCancelled) {
        this.setState({
          isFetching: false
        })
      }
      console.error(err)
    }
  }

  onFiltersChange = (component) => {
    if(Date.now() - this.state.filterTimeStamp > 500){
      this.setState({
        filters: component || {},
        filterTimeStamp: Date.now()
      })
    }
  }

  loadDefaultSearch = async () => {
    const {currentTab} = this.state
    const searchPref = await fetchMasterListSearchPrefs("global-search")
    const filters = (searchPref && Array.isArray(searchPref.data) && searchPref.data.length && searchPref.data[0].searchPreference && JSON.parse(searchPref.data[0].searchPreference)) || {}
    this.setState({
      searchPref: filters,
      filters: (filters && filters[tabsConfig[currentTab].name]) || {}
    })
  }

  saveDefaultFilter = async (reset) => {
    const {currentTab, searchPref} = this.state
    const {filters} = this.state
    const savePrefResult = await saveMasterListSearchPrefs(
      "global-search",
      "default-search",
      {...searchPref, [tabsConfig[currentTab].name]: reset ? {} : filters}
    )
    if (savePrefResult.status === 200) {
      toast("Successfully Saved", {
        autoClose: 2000,
        type: toast.TYPE.SUCCESS
      })
      this.setState({
        searchPref: {...searchPref, [tabsConfig[currentTab].name]: reset ? {} : filters}
      })
      if(reset){
        window.location.reload()
      }
    } else {
      toast("Something went wrong!", {
        autoClose: 2000,
        type: toast.TYPE.ERROR
      })
    }
  }

  handleFiltersBarToggle = (isSideBarOpen) => {
    this.setState({
      isSideBarOpen
    })
  }

  render() {
    const { auth } = this.props
    const {
      currentTab,
      isFetching,
      defaultQuery,
      isFetchingUrls,
      urls,
      filters,
      isSideBarOpen
    } = this.state

    return (
      <div>
        {
          isFetching || isFetchingUrls ? <Loader/> :
            <ReactiveBase
              url={ELASTIC_SEARCH_URL}
              app={`tenant_data_${
                auth.loginDetails.relatedFaEntities[0].entityId
              }`}
              theme={{
                typography: {
                  fontFamily: "GoogleSans, Gravity !important"
                },
                colors: {
                  textColor: "rgba(10, 10, 10, 0.6)"
                }
              }}
              type="doc"
              headers={{
                Authorization: localStorage.getItem("token")
              }}
            >
              <ReactiveComponent
                customQuery={() => tabsConfig[currentTab].query}
                componentId={`viewQuery_${tabsConfig[currentTab].name}`}
                key={`viewQuery_${tabsConfig[currentTab].name}`}
              />
              <ReactiveComponent
                customQuery={() => defaultQuery}
                componentId="globalSearchEntitlement"
                key="globalSearchEntitlement"
              />
              <Container>
                <Tab currentTab={currentTab} view={tabsConfig[currentTab].name} onTabChange={this.handleTabChange} isSideBarOpen={isSideBarOpen}/>
                <SearchContainer hasTopBar view={tabsConfig[currentTab].name} isSideBarOpen={isSideBarOpen}>
                  <SearchBar
                    viewIndex={currentTab}
                    view={tabsConfig[currentTab].name}
                    filters={filters}
                  />
                </SearchContainer>
                <Layout>
                  <SideBar view={tabsConfig[currentTab].name} handleToggle={this.handleFiltersBarToggle}>
                    <FilterLabel>
                      <div className="columns">
                        <div className="column">
                        Filters
                        </div>
                        {
                          tabsConfig[currentTab].name !== "Calendar" ?
                            <div className="column" style={{paddingRight: 0}}>
                              <button // eslint-disable-line
                                className="button is-info is-small has-text-right"
                                onClick={() => this.saveDefaultFilter()}
                              >
                              Save filter as a default
                              </button>
                            </div>
                            : null
                        }
                        {
                          tabsConfig[currentTab].name !== "Calendar" ?
                            <div className="column" style={{paddingLeft: 5}}>
                              <button // eslint-disable-line
                                className="button is-default is-small has-text-right"
                                onClick={() => this.saveDefaultFilter(true)}
                              >
                                Reset
                              </button>
                            </div>
                            : null
                        }
                      </div>
                    </FilterLabel>
                    <Filters view={tabsConfig[currentTab].name} filters={filters}/>
                  </SideBar>
                  <Content style={tabsConfig[currentTab].name === "Calendar" ? calendarStyle : style } isSideBarOpen={isSideBarOpen}>
                    <Result view={tabsConfig[currentTab].name} urls={urls} onFiltersChange={this.onFiltersChange} />
                  </Content>
                </Layout>
              </Container>
            </ReactiveBase>
        }
      </div>
    )
  }
}

export default GlobalSearch
