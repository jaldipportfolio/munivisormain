import React from "react"

import StyledMultiList from "../../../Layout/StyledMultiList"

export default ({filters}) => (
  <StyledMultiList
    componentId="Assignee"
    dataField="data.taskDetails.taskAssigneeName.keyword"
    title="Assignee"
    filter="Assignee"
    showCount={false}
    defaultValue={(filters && filters.Assignee && filters.Assignee.value) || []}
    react={{
      and: [
        "Search_Tasks",
        "viewQuery_Tasks",
        "TaskStartDate",
        "TaskDueDate",
        "TaskPriority",
        "TaskRead",
        "TaskStatus",
        "TaskType",
        "TaskCategory",
        "globalSearchEntitlement"
      ]
    }}
    renderNoResults={() => <div>No data found</div>}
    loader="Fetching ..."
  />
)
