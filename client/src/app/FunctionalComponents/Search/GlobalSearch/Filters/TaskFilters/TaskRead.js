import React from "react"
import { MultiDataList } from "@appbaseio/reactivesearch"

export default ({filters}) => (
  <MultiDataList
    componentId="TaskRead"
    dataField="data.taskDetails.taskUnread"
    defaultValue={(filters && filters.TaskRead && filters.TaskRead.value) || []}
    data={[
      {
        label: "Read",
        value: true
      },
      {
        label: "Unread",
        value: false
      }
    ]}
    title="Task Read"
    filter="Task Read"
    showSearch={false}
  />
)
