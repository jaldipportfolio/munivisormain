import React from "react"
import styled from "styled-components"
import { DateRange } from "@appbaseio/reactivesearch"
import moment from "moment"

const StyledDate = styled(DateRange)`
  div {
    border-radius: 3px;
  }

  input {
    height: 27px !important;
    font-size: 0.8rem !important;
  }
`

const DateFilter = ({filters}) => {
  const defaultValue = {
    start: null,
    end: null
  }
  if (filters && filters.TaskStartDate && Array.isArray(filters.TaskStartDate.value) && filters.TaskStartDate.value.length) {
    defaultValue.start = filters.TaskStartDate.value[0] || null
    defaultValue.end = filters.TaskStartDate.value[1] || null
  }
  console.log(defaultValue)
  return (
    <StyledDate
      componentId="TaskStartDate"
      dataField="data.taskDetails.taskStartDate"
      title="Task Start Date"
      showFilter
      filterLabel="Start Date"
      numberOfMonths={1}
      URLParams={false}
      clickUnselectsDay
      // defaultValue={defaultValue}
      customQuery={(value, props) => {
        let query = null
        if (value) {
          query = [
            {
              range: {
                [props.dataField]: {
                  gte: moment(value.start).startOf("day"),
                  lte: moment(value.end).endOf("day")
                }
              }
            }
          ]
        }
        return query ? {query: {bool: {must: query}}} : null
      }}
      queryFormat="date_time_no_millis"
    />
  )
}
export default DateFilter
