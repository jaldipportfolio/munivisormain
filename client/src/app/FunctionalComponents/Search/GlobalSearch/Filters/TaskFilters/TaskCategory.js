import React from "react"

import StyledMultiList from "../../../Layout/StyledMultiList"

export default ({filters}) => (
  <StyledMultiList
    componentId="TaskCategory"
    dataField="data.taskCategory.keyword"
    title="Task Category"
    filter="Task Category"
    showCount={false}
    defaultValue={(filters && filters.TaskCategory && filters.TaskCategory.value) || []}
    react={{
      and: [
        "Search_Tasks",
        "viewQuery_Tasks",
        "TaskStartDate",
        "TaskDueDate",
        "TaskPriority",
        "TaskRead",
        "TaskStatus",
        "TaskType",
        "Assignee",
        "globalSearchEntitlement"
      ]
    }}
    renderNoResults={() => <div>No data found</div>}
    loader="Fetching ..."
  />
)
