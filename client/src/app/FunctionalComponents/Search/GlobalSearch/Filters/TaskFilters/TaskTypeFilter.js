import React from "react"

import StyledMultiList from "../../../Layout/StyledMultiList"

export default ({filters}) => (
  <StyledMultiList
    componentId="TaskType"
    dataField="data.taskDetails.taskType.keyword"
    title="Task Type"
    filter="Task Type"
    showCount={false}
    defaultValue={(filters && filters.TaskType && filters.TaskType.value) || []}
    react={{
      and: [
        "Search_Tasks",
        "viewQuery_Tasks",
        "TaskStartDate",
        "TaskDueDate",
        "TaskPriority",
        "TaskRead",
        "TaskStatus",
        "Assignee",
        "TaskCategory",
        "globalSearchEntitlement"
      ]
    }}
    renderNoResults={() => <div>No data found</div>}
    loader="Fetching ..."
  />
)
