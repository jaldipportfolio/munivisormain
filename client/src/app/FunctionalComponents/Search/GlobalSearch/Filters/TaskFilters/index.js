import React from "react"

import TaskStatus from "./TaskStatusFilter"
import TaskType from "./TaskTypeFilter"
import TaskDueDate from "./DueDateFilter"
import TaskStartDate from "./StartDateFilter"
import TaskPriority from "./TaskPriority"
import TaskRead from "./TaskRead"
import AssigneeFilter from "./AssigneeFilter"
import TaskCategory from "./TaskCategory"

const Filters = ({filters}) => (
  <div>
    <TaskStartDate filters={filters} />
    <hr/>
    <TaskDueDate filters={filters} />
    <hr/>
    <TaskStatus filters={filters} />
    <hr/>
    <TaskRead filters={filters} />
    <hr/>
    <TaskType filters={filters} />
    <hr/>
    <TaskCategory filters={filters} />
    <hr/>
    <TaskPriority filters={filters} />
    <hr/>
    <AssigneeFilter filters={filters} />
  </div>
)

export default Filters
