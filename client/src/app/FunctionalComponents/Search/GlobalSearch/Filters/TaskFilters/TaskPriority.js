import React from "react"

import StyledMultiList from "../../../Layout/StyledMultiList"

export default ({filters}) => (
  <StyledMultiList
    componentId="TaskPriority"
    dataField="data.taskDetails.taskPriority.keyword"
    title="Task Priority"
    filter="Task Priority"
    showCount={false}
    defaultValue={(filters && filters.TaskPriority && filters.TaskPriority.value) || []}
    react={{
      and: [
        "Search_Tasks",
        "viewQuery_Tasks",
        "TaskStartDate",
        "TaskDueDate",
        "TaskRead",
        "TaskStatus",
        "TaskType",
        "Assignee",
        "TaskCategory",
        "globalSearchEntitlement"
      ]
    }}
    renderNoResults={() => <div>No data found</div>}
    loader="Fetching ..."
  />
)
