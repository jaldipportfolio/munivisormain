import React from "react"

import StyledMultiList from "../../../Layout/StyledMultiList"

export default ({filters}) => (
  <StyledMultiList
    componentId="TaskStatus"
    dataField="data.taskDetails.taskStatus.keyword"
    title="Task Status"
    filter="Task Status"
    showCount={false}
    defaultValue={(filters && filters.TaskStatus && filters.TaskStatus.value) || []}
    react={{
      and: [
        "Search_Tasks",
        "viewQuery_Tasks",
        "TaskStartDate",
        "TaskDueDate",
        "TaskPriority",
        "TaskRead",
        "TaskType",
        "Assignee",
        "TaskCategory",
        "globalSearchEntitlement"
      ]
    }}
    renderNoResults={() => <div>No data found</div>}
    loader="Fetching ..."
  />
)
