import React from "react"

import StyledMultiList from "../../../Layout/StyledMultiList"

export default ({filters}) => (
  <StyledMultiList
    componentId="TenantRelationShip"
    dataField="data.relationshipToTenant.keyword"
    title="Tenant Relation"
    filter="Tenant Relation"
    showCount={false}
    defaultValue={(filters && filters.TenantRelationShip && filters.TenantRelationShip.value) || []}
    react={{
      and: [
        "Search_Entities",
        "viewQuery_Entities",
        "MarketRole",
        "globalSearchEntitlement"
      ]
    }}
    renderNoResults={() => <div>No data found</div>}
    loader="Fetching ..."
  />
)
