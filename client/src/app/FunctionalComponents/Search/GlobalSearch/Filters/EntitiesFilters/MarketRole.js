import React from "react"

import StyledMultiList from "../../../Layout/StyledMultiList"

export default ({filters}) => (
  <StyledMultiList
    componentId="MarketRole"
    dataField="data.entityFlags.marketRole.keyword"
    title="Market Role"
    filter="Market Role"
    showCount={false}
    defaultValue={(filters && filters.MarketRole && filters.MarketRole.value) || []}
    react={{
      and: [
        "Search_Entities",
        "viewQuery_Entities",
        "TenantRelationShip",
        "globalSearchEntitlement"
      ]
    }}
    renderNoResults={() => <div>No data found</div>}
    loader="Fetching ..."
  />
)
