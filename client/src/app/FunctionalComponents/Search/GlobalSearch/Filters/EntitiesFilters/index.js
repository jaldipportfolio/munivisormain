import React from "react"

import MarketRole from "./MarketRole"
import TenantRelation from "./TenantRelationFilter"

const Filters = ({filters}) => (
  <div>
    <MarketRole filters={filters}/>
    <hr/>
    <TenantRelation filters={filters}/>
  </div>
)

export default Filters
