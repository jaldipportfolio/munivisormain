import React from "react"

import CreateDateFilter from "./CreateDateFilter"
import CreateDateRangeFilter from "./CreateDateRange"
import ParAmountFilter from "./ParAmountFilter"
import StatusFilter from "./StatusFilter"
import TransactionTypeFilter from "./TransactionTypeFilter"
import ActivityClientFilter from "./ActivityClientFilter"

const Filters = ({filters}) => (
  <div>
    <TransactionTypeFilter filters={filters} />
    <hr/>
    <ActivityClientFilter filters={filters} />
    <hr/>
    <ParAmountFilter filters={filters} />
    <hr/>
    <StatusFilter filters={filters} />
    <hr/>
    <CreateDateFilter filters={filters} />
    <CreateDateRangeFilter filters={filters} />
  </div>
)

export default Filters
