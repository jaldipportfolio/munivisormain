import React from "react"

import StyledMultiList from "../../../Layout/StyledMultiList"

export default ({filters}) => (
  <StyledMultiList
    componentId="Status"
    dataField="data.tranStatus.keyword"
    title="Transaction Status"
    filter="Transaction Status"
    showCount={false}
    defaultValue={(filters && filters.Status && filters.Status.value) || []}
    react={{
      and: [
        "Search_Transactions",
        "viewQuery_Transactions",
        "TransactionType",
        "ActivityClient",
        "PrincipalAmount",
        "CreatedDate",
        "CreatedAtRange",
        "globalSearchEntitlement"
      ]
    }}
    renderNoResults={() => <div>No data found</div>}
    loader="Fetching ..."
  />
)
