import React from "react"

import StyledMultiList from "../../../Layout/StyledMultiList"

export default ({filters}) => (
  <StyledMultiList
    componentId="TransactionType"
    dataField="category.keyword"
    title="Transaction Type"
    filter="Transaction Type"
    defaultValue={(filters && filters.TransactionType && filters.TransactionType.value) || []}
    react={{
      and: [
        "Search_Transactions",
        "viewQuery_Transactions",
        "Status",
        "ActivityClient",
        "PrincipalAmount",
        "CreatedDate",
        "CreatedAtRange",
        "globalSearchEntitlement"
      ]
    }}
    showCount={false}
    renderNoResults={() => <div>No data found</div>}
    loader="Fetching ..."
  />
)
