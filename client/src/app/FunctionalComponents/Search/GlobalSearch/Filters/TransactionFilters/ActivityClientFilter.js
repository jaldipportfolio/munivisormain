import React from "react"

import StyledList from "../../../Layout/StyledMultiList"

const Activity = () => (
  <StyledList
    componentId="ActivityClient"
    dataField="data.tranClientName.keyword"
    filterLabel="Activity Client"
    title="Activity Client"
    showCount={false}
    react={{
      and: [
        "TransactionType",
        "Search_Transactions",
        "viewQuery_Transactions",
        "Status",
        "PrincipalAmount",
        "CreatedDate",
        "CreatedAtRange",
        "globalSearchEntitlement"
      ]
    }}
    renderNoResults={() => <div>Please upload docs to get Activity filter</div>}
    loader="Fetching document categories ..."
  />
)

export default Activity
