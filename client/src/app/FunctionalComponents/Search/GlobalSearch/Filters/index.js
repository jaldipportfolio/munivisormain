import React from "react"

import TransactionFilters from "./TransactionFilters"
import EntitiesFilter from "./EntitiesFilters"
import TasksFilter from "./TaskFilters"
import UserFilter from "./UserFilters"

const Filters = ({ view, filters }) => (
  <div style={{marginBottom: 50}}>
    {view === "Transactions" && <TransactionFilters filters={filters} />}
    {view === "Entities" && <EntitiesFilter filters={filters} />}
    {view === "Users" && <UserFilter filters={filters} />}
    {view === "Tasks" && <TasksFilter filters={filters} />}
  </div>
)

export default Filters
