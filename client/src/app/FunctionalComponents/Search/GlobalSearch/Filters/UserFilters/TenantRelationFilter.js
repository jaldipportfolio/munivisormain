import React from "react"

import StyledMultiList from "../../../Layout/StyledMultiList"

export default ({filters}) => (
  <StyledMultiList
    componentId="UserTenantRelationShip"
    dataField="data.entityRelationshipToTenant.keyword"
    title="Tenant Relation"
    filter="Tenant Relation"
    showCount={false}
    defaultValue={(filters && filters.UserTenantRelationShip && filters.UserTenantRelationShip.value) || []}
    react={{
      and: [
        "Search_Users",
        "viewQuery_Users",
        "FirmFilter",
        "globalSearchEntitlement",
        "UserStatus"
      ]
    }}
    renderNoResults={() => <div>No data found</div>}
    loader="Fetching ..."
  />
)
