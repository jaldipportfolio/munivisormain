import React from "react"

import StyledMultiList from "../../../Layout/StyledMultiList"

export default ({filters}) => (
  <StyledMultiList
    componentId="FirmFilter"
    dataField="data.userFirmName.keyword"
    title="Firm"
    filter="Firm"
    showCount={false}
    defaultValue={(filters && filters.FirmFilter && filters.FirmFilter.value) || []}
    react={{
      and: [
        "Search_Users",
        "viewQuery_Users",
        "UserTenantRelationShip",
        "globalSearchEntitlement",
        "UserStatus"
      ]
    }}
    renderNoResults={() => <div>No data found</div>}
    loader="Fetching ..."
  />
)
