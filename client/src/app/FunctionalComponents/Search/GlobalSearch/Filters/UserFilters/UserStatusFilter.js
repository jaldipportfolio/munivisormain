import React from "react"

import StyledMultiList from "../../../Layout/StyledMultiList"

export default ({filters}) => (
  <StyledMultiList
    componentId="UserStatus"
    dataField="data.userStatus.keyword"
    title="User Status"
    filter="User Status"
    showCount={false}
    defaultValue={(filters && filters.UserStatus && filters.UserStatus.value) || []}
    react={{
      and: [
        "Search_Users",
        "viewQuery_Users",
        "FirmFilter",
        "globalSearchEntitlement"
      ]
    }}
    renderNoResults={() => <div>No data found</div>}
    loader="Fetching ..."
  />
)
