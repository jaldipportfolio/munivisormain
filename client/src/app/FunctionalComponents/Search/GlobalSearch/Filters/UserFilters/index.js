import React from "react"

import FirmFilter from "./FirmFilter"
import TenantRelation from "./TenantRelationFilter"
import UserStatusFilter from "./UserStatusFilter"

const Filters = ({filters}) => (
  <div>
    <FirmFilter filters={filters} />
    <hr/>
    <TenantRelation filters={filters}/>
    <hr/>
    <UserStatusFilter filters={filters}/>
  </div>
)

export default Filters
