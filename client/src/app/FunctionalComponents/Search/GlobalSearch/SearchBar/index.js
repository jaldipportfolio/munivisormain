import React from "react"

import StyledSearchBar from "../../Layout/StyledSearchBar"
import { getWeights, getSearchableFields } from "../searchConfig"

const Search = ({ view, viewIndex, filters }) => (
  <StyledSearchBar
    key={`Search_${view}`}
    componentId={`Search_${view}`}
    defaultValue={(filters && filters[`Search_${view}`] && filters[`Search_${view}`].value) || ""}
    dataField={[...getSearchableFields(viewIndex)]}
    fieldWeights={[...getWeights(viewIndex)]}
    autosuggest={false}
    debounce={300}
    highlight
  />
)

export default Search
