/* eslint-disable react/no-danger, no-underscore-dangle, indent */
import React from "react"
import { SelectedFilters, ReactiveList } from "@appbaseio/reactivesearch"
import ReactTable from "react-table"
import styled from "styled-components"
import _ from "lodash"
import { Link } from "react-router-dom"

import Loader from "../../Layout/Loader"
import NoResult from "../../Layout/NoResult"
import Error from "../../Layout/Error"
import HistoricalData from "./HistoricalData"
import Stats from "../../Layout/Stats"
import Flex from "../../Layout/Flex"
import {sortOptions} from "../../NavigationSearch/searchConfig"

const Card = styled.div`
  background: #fff;
  border-radius: 3px;
  padding: 5px 0px 8px 0px;
  align-items: center;
  border: 1px solid #e1e4e8;
`

const InfoContainer = styled(Flex)`
  font-size: small;
  margin-top: 5px;
`

const InfoItem = styled(Flex)`
  padding-left: 10px;
  border-left: ${props => (props.noborder ? 0 : "1px solid #e1e4e8")};
  font-size: small;
  width: 32%;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;

  @media (max-width: 769px) {
    flex-wrap: wrap !important;
  }
  
  > div {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }
`

const defaultUrl = "/dashboard"

const Result = ({ urls, pageSize, onFiltersChange }) => (
  <div>
    <SelectedFilters onChange={(component, value) => onFiltersChange(component, value)} />
    <div style={{ position: "relative" }}>
      <ReactiveList
        componentId="EntitySearchResult"
        dataField="_doc"
        react={{
          and: [
            "Search_Entities",
            "viewQuery_Entities",
            "MarketRole",
            "TenantRelationShip",
            "globalSearchEntitlement"
          ]
        }}
        pagination
        paginationAt="both"
        excludeFields={["content"]}
        size={pageSize}
        loader={<Loader />}
        renderResultStats={stats => (
          <Stats
            style={{
              position: "absolute",
              left: 4,
              fontSize: "small",
              top: 18
            }}
          >
            Showing{" "}
            <b>
              {stats.currentPage * pageSize + 1} -{" "}
              {stats.currentPage * pageSize + stats.displayedResults}
            </b>{" "}
            of total <b>{stats.totalResults}</b> records
          </Stats>
        )}
        innerClass={{
          pagination: "reactive-search-pagination"
        }}
        sortOptions={sortOptions.entities}
        renderNoResults={NoResult}
        renderError={() => <Error />}
        renderAllData={({ results }) => (
          <div style={{ marginTop: 10 }}>
            {Boolean(results.length) && (
              <ReactTable
                showPagination={false}
                pageSize={results.length}
                minRows={1}
                className="-striped -highlight is-bordered"
                data={results}
                SubComponent={(row) => {
                  const data = row.original && row.original.data && row.original.data
                  const phoneNumber = (data.entityPrimaryAddress && data.entityPrimaryAddress.officePhone.length) ?
                    data.entityPrimaryAddress.officePhone && data.entityPrimaryAddress.officePhone[0] &&
                    data.entityPrimaryAddress.officePhone[0].phoneNumber : null

                  const emailId =  (data.entityPrimaryAddress && data.entityPrimaryAddress.officeEmails.length) ?
                      data.entityPrimaryAddress.officeEmails && data.entityPrimaryAddress.officeEmails[0] &&
                      data.entityPrimaryAddress.officeEmails[0].emailId : null

                  return (
                    <Card>
                      <InfoContainer alignItems="center">
                        <InfoItem noborder>
                          <b>Entity Address: &nbsp;</b>
                          <div title={(data.entityPrimaryAddress && data.entityPrimaryAddress.formatted_address) || ""}>
                            {data.url ?
                              <a
                                href={data.url}
                                target="_blank"
                                rel="noopener noreferrer"
                                dangerouslySetInnerHTML={{
                                  __html: _.get(row.original, "data.entityPrimaryAddress.formatted_address")
                                }}
                              /> :
                              <a href={`https://maps.google.com/?q=${data.entityPrimaryAddress && data.entityPrimaryAddress.formatted_address}`} target="_blank">
                                <span
                                  dangerouslySetInnerHTML={{
                                    __html: _.get(row.original, "data.entityPrimaryAddress.formatted_address")
                                  }}
                                />
                              </a>
                            }
                          </div>
                        </InfoItem>

                        <InfoItem>
                          <b>Entity Aliases: &nbsp;</b>
                          <div title={data.entityAliases && data.entityAliases.length ? data.entityAliases.toString() : null}>
                            {data.entityAliases && data.entityAliases.length ? data.entityAliases.toString() : "-" }
                          </div>
                        </InfoItem>

                        <InfoItem>
                          <b>Market Role: &nbsp;</b>
                          <div title={data.entityFlags && data.entityFlags.marketRole && data.entityFlags.marketRole.toString()}>
                            {
                              row.original ? <span
                                dangerouslySetInnerHTML={{
                                  __html: _.get(row.original, "data.entityFlags.marketRole", "-")
                                }}
                              /> : "-"
                            }
                          </div>
                        </InfoItem>

                      </InfoContainer>

                      <InfoContainer alignItems="center">
                        <InfoItem noborder>
                          <b>Office Phone: &nbsp;</b>
                          {
                            phoneNumber ?
                              <a href={`tel:${phoneNumber}`}>
                                {phoneNumber}
                              </a> : "-"
                          }
                        </InfoItem>

                        <InfoItem>
                          <b>Office Emails: &nbsp;</b>
                          {
                            emailId ?  <a href={`mailto:${emailId}`}>{emailId}</a> : "-"
                          }
                        </InfoItem>

                        <InfoItem>
                          <b>Fax Number: &nbsp;</b>
                          <div>
                            {(data.entityPrimaryAddress &&
                              data.entityPrimaryAddress.officeFax.length) ?
                              data.entityPrimaryAddress.officeFax &&
                              data.entityPrimaryAddress.officeFax[0] &&
                              data.entityPrimaryAddress.officeFax[0].faxNumber : "-" }
                          </div>
                        </InfoItem>
                      </InfoContainer>
                    </Card>
                  )
                }}
                columns={[
                  {
                    Header: "More",
                    expander: true,
                    width: 65,
                    Expander: ({isExpanded}) => (
                      <div>{isExpanded ? <i style={{color: "#4288dd"}} className="fas fa-caret-down"/> :
                        <i style={{color: "#4288dd"}} className="fas fa-caret-right"/>}</div>
                    ),
                    style: {
                      cursor: "pointer",
                      fontSize: 25,
                      padding: "0",
                      textAlign: "center",
                      userSelect: "none"
                    }
                  },
                  {
                    id: "Entity Name",
                    Header: "Entity Name",
                    accessor: item => item,
                    Cell: ({ value }) => (
                      <div className="hpTablesTd wrap-cell-text">
                        <Link
                          to={urls.defaultUrls[value.data._id] || defaultUrl}
                          dangerouslySetInnerHTML={{
                            __html: _.get(value, "data.entityName")
                          }}
                        />
                      </div>
                    ),
                    sortMethod: (a, b) => (a.data.entityName || "").localeCompare(b.data.entityName || "")
                  },
                  {
                    id: "Entity Type",
                    Header: "Entity Type",
                    accessor: item => item,
                    Cell: ({ value }) => (
                      <div className="hpTablesTd wrap-cell-text">
                        <span
                          dangerouslySetInnerHTML={{
                            __html: _.get(value, "data.entityType")
                          }}
                        />
                      </div>
                    ),
                    sortMethod: (a, b) => (a.data.entityType || "").localeCompare(b.data.entityType || "")
                  },
                  // {
                  //   id: "Entity Address",
                  //   Header: "Entity Address",
                  //   accessor: item => item,
                  //   Cell: ({ value }) => (
                  //     <div className="hpTablesTd wrap-cell-text">
                  //       {value &&
                  //       value.data &&
                  //       value.data.entityPrimaryAddress &&
                  //       value.data.entityPrimaryAddress &&
                  //       value.data.entityPrimaryAddress.url ? (
                  //         <a
                  //           href={value.data.entityPrimaryAddress.url}
                  //           target="_blank"
                  //           rel="noopener noreferrer"
                  //           dangerouslySetInnerHTML={{
                  //             __html: _.get(
                  //               value,
                  //               "data.entityPrimaryAddress.formatted_address"
                  //             )
                  //           }}
                  //         />
                  //       ) : (
                  //         <span
                  //           dangerouslySetInnerHTML={{
                  //             __html: _.get(
                  //               value,
                  //               "data.entityPrimaryAddress.formatted_address"
                  //             )
                  //           }}
                  //         />
                  //       )}
                  //     </div>
                  //   ),
                  //   sortMethod: (a, b) =>
                  //     (
                  //       (a &&
                  //         a.data &&
                  //         a.data.entityPrimaryAddress &&
                  //         a.data.entityPrimaryAddress.formatted_address) ||
                  //       ""
                  //     ).localeCompare(
                  //       (b &&
                  //         b.data &&
                  //         b.data.entityPrimaryAddress &&
                  //         b.data.entityPrimaryAddress.formatted_address) ||
                  //         ""
                  //     )
                  // },
                  // {
                  //   id: "Entity Aliases",
                  //   Header: "Entity Aliases",
                  //   accessor: item => item,
                  //   Cell: ({ value }) => (
                  //     <div className="hpTablesTd wrap-cell-text">
                  //       {value.data.entityAliases.length ? (
                  //         <span
                  //           dangerouslySetInnerHTML={{
                  //             __html: _.get(value, "data.entityAliases")
                  //           }}
                  //         />
                  //       ) : (
                  //         "-"
                  //       )}
                  //     </div>
                  //   ),
                  //   sortMethod: (a, b) =>
                  //     ((a && a.data && a.data.entityAliases) || [])
                  //       .join(",")
                  //       .localeCompare(
                  //         ((b && b.data && b.data.entityAliases) || []).join(
                  //           ","
                  //         )
                  //       )
                  // },
                  // {
                  //   id: "Market Role",
                  //   Header: "Market Role",
                  //   accessor: item => item,
                  //   Cell: ({ value }) => (
                  //     <div className="hpTablesTd wrap-cell-text">
                  //       {((value.data.entityFlags || {}).marketRole || [])
                  //         .length ? (
                  //         <span
                  //           dangerouslySetInnerHTML={{
                  //             __html: _.get(
                  //               value,
                  //               "data.entityFlags.marketRole",
                  //               "-"
                  //             )
                  //           }}
                  //         />
                  //       ) : (
                  //         "-"
                  //       )}
                  //     </div>
                  //   ),
                  //   sortMethod: (a, b) =>
                  //     (
                  //       (a &&
                  //         a.data &&
                  //         a.data.entityFlags &&
                  //         a.data.entityFlags.marketRole) ||
                  //       []
                  //     )
                  //       .join(",")
                  //       .localeCompare(
                  //         (
                  //           (b &&
                  //             b.data &&
                  //             b.data.entityFlags &&
                  //             b.data.entityFlags.marketRole) ||
                  //           []
                  //         ).join(",")
                  //       )
                  // },
                  {
                    id: "Historical Data",
                    Header: "Historical Data",
                    accessor: item => item,
                    Cell: ({ value }) => {
                      const val = _.get(value, "data.historicalData")
                      return (
                        <div className="hpTablesTd wrap-cell-text">
                          {val && val !== "{}" ? (
                            <HistoricalData
                              data={(value.data || {}).historicalData || "{}"}
                            />
                          ) : "-"}
                        </div>
                      )
                    }
                  }
                ]}
              />
            )}
          </div>
        )}
      />
    </div>
  </div>
)

export default Result
