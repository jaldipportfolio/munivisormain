import React from "react"

import TransactionResult from "./TransactionResult"
import EntityResult from "./EntityResult"
import TaskResult from "./TaskResult"
import UserResult from "./UserResult"

const PAGE_SIZE = 25
const EventsCalendar = React.lazy(() => import("../../../../FunctionalComponents/TaskManagement/components/EventsCalendar"))

const Result = ({ view, urls, onFiltersChange }) => (
  <div>
    {view === "Transactions" && (
      <TransactionResult pageSize={PAGE_SIZE} urls={urls} onFiltersChange={onFiltersChange} />
    )}
    {view === "Entities" && <EntityResult pageSize={PAGE_SIZE} urls={urls} onFiltersChange={onFiltersChange} />}
    {view === "Users" && <UserResult pageSize={PAGE_SIZE} urls={urls} onFiltersChange={onFiltersChange} />}
    {view === "Tasks" && <TaskResult pageSize={PAGE_SIZE} urls={urls} onFiltersChange={onFiltersChange} />}
    {view === "Calendar" && <EventsCalendar />}
  </div>
)

export default Result
