/* eslint-disable react/no-danger, no-underscore-dangle */
import React from "react"
import { SelectedFilters, ReactiveList } from "@appbaseio/reactivesearch"
import ReactTable from "react-table"
import _ from "lodash"
import styled from "styled-components"
import { Link } from "react-router-dom"

import Loader from "../../Layout/Loader"
import NoResult from "../../Layout/NoResult"
import Error from "../../Layout/Error"
import HistoricalData from "./HistoricalData"
import Stats from "../../Layout/Stats"

import Flex from "../../Layout/Flex"
import {sortOptions} from "../../NavigationSearch/searchConfig"

const Card = styled.div`
  background: #fff;
  border-radius: 3px;
  padding: 5px 0px 8px 0px;
  align-items: center;
  border: 1px solid #e1e4e8;
`

const InfoContainer = styled(Flex)`
  font-size: small;
  margin-top: 5px;
`

const InfoItem = styled(Flex)`
  padding-left: 10px;
  border-left: ${props => (props.noborder ? 0 : "1px solid #e1e4e8")};
  font-size: small;
  width: 32%;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;

  @media (max-width: 769px) {
    flex-wrap: wrap !important;
  }
  
  > div {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }
`

const defaultUrl = "/dashboard"

const Result = ({ urls, pageSize, onFiltersChange }) => (
  <div>
    <SelectedFilters onChange={(component, value) => onFiltersChange(component, value)} />
    <div style={{ position: "relative" }}>
      <ReactiveList
        componentId="UserSearchResult"
        dataField="_doc"
        react={{
          and: [
            "Search_Users",
            "viewQuery_Users",
            "FirmFilter",
            "UserStatus",
            "UserTenantRelationShip",
            "globalSearchEntitlement"
          ]
        }}
        pagination
        paginationAt="both"
        excludeFields={["content"]}
        size={pageSize}
        loader={<Loader />}
        renderResultStats={stats => (
          <Stats
            style={{
              position: "absolute",
              left: 4,
              fontSize: "small",
              top: 18
            }}
          >
            Showing{" "}
            <b>
              {stats.currentPage * pageSize + 1} -{" "}
              {stats.currentPage * pageSize + stats.displayedResults}
            </b>{" "}
            of total <b>{stats.totalResults}</b> records
          </Stats>
        )}
        innerClass={{
          pagination: "reactive-search-pagination"
        }}
        sortOptions={sortOptions.users}
        renderNoResults={NoResult}
        renderError={() => <Error />}
        renderAllData={({ results }) => (
          <div style={{ marginTop: 10 }}>
            {Boolean(results.length) && (
              <ReactTable
                showPagination={false}
                pageSize={results.length}
                minRows={1}
                className="-striped -highlight is-bordered"
                data={results}
                SubComponent={(row) => {
                  const data = row.original && row.original.data && row.original.data
                  const userAddress = data.userPrimaryAddress ?
                    `${(data.userPrimaryAddress && data.userPrimaryAddress.addressLine1) || ""} ${(data.userPrimaryAddress && data.userPrimaryAddress.addressLine2) || ""} ${(data.userPrimaryAddress && data.userPrimaryAddress.city) || ""} ${(data.userPrimaryAddress && data.userPrimaryAddress.state) || ""} ${(data.userPrimaryAddress && data.userPrimaryAddress.zipCode && data.userPrimaryAddress.zipCode.zip1) || ""} ${(data.userPrimaryAddress && data.userPrimaryAddress.zipCode && data.userPrimaryAddress.zipCode.zip2) || ""}` : "-"
                  return (
                    <Card>
                      <InfoContainer alignItems="center">
                        <InfoItem noborder>
                          <b>User Address: &nbsp;</b>
                          <div title={userAddress}>
                            <a href={`https://maps.google.com/?q=${userAddress}`} target="_blank">
                              <span
                                dangerouslySetInnerHTML={{
                                  __html: _.get(row.original, "data.userPrimaryAddress.addressLine1")
                                }}
                              />
                              &nbsp;
                              <span
                                dangerouslySetInnerHTML={{
                                  __html: _.get(row.original, "data.userPrimaryAddress.addressLine2")
                                }}
                              />
                              &nbsp;
                              <span
                                dangerouslySetInnerHTML={{
                                  __html: _.get(row.original, "data.userPrimaryAddress.city")
                                }}
                              />
                              &nbsp;
                              <span
                                dangerouslySetInnerHTML={{
                                  __html: _.get(row.original, "data.userPrimaryAddress.state")
                                }}
                              />
                              &nbsp;
                              <span
                                dangerouslySetInnerHTML={{
                                  __html: _.get(row.original, "data.userPrimaryAddress.zipCode.zip1")
                                }}
                              />
                              -
                              <span
                                dangerouslySetInnerHTML={{
                                  __html: _.get(row.original, "data.userPrimaryAddress.zipCode.zip2")
                                }}
                              />
                            </a>
                          </div>
                        </InfoItem>

                        <InfoItem>
                          <b>User Flags: &nbsp;</b>
                          <div title={data && data.userFlags.toString()}>
                            {data.userFlags.length ? (
                              <span
                                dangerouslySetInnerHTML={{
                                  __html: _.get(row.original, "data.userFlags")
                                }}
                              />
                            ) : "-"}
                          </div>
                        </InfoItem>

                        <InfoItem>
                          <b>Historical Data: &nbsp;</b>
                          <div title="View">
                            {data.historicalData !== "{}" ? (
                              <HistoricalData
                                data={(data || {}).historicalData || "{}"}
                              />
                            ) : null}
                          </div>
                        </InfoItem>
                      </InfoContainer>
                    </Card>
                  )
                }}
                columns={[
                  {
                    Header: "More",
                    expander: true,
                    width: 65,
                    Expander: ({isExpanded}) => (
                      <div>
                        {isExpanded ? <i style={{color: "#4288dd"}} className="fas fa-caret-down"/> :
                          <i style={{color: "#4288dd"}} className="fas fa-caret-right"/>}
                      </div>
                    ),
                    style: {
                      cursor: "pointer",
                      fontSize: 25,
                      padding: "0",
                      textAlign: "center",
                      userSelect: "none"
                    }
                  },
                  {
                    id: "User Name",
                    Header: "User Name",
                    accessor: item => item,
                    Cell: ({ value }) => (
                      <div className="hpTablesTd wrap-cell-text">
                        <Link
                          to={urls.defaultUrls[value.data._id] || defaultUrl}
                        >
                          <span
                            dangerouslySetInnerHTML={{
                              __html: _.get(value, "data.userFirstName")
                            }}
                          />
                          &nbsp;
                          <span
                            dangerouslySetInnerHTML={{
                              __html: _.get(value, "data.userLastName")
                            }}
                          />
                        </Link>
                      </div>
                    ),
                    sortMethod: (a, b) =>
                      ((a && a.data && a.data.userFirstName) || "").localeCompare((b && b.data && b.data.userFirstName) || "")
                  },
                  {
                    id: "Entity Name",
                    Header: "Entity Name",
                    accessor: item => item,
                    Cell: ({ value }) => (
                      <div className="hpTablesTd wrap-cell-text">
                        <Link
                          to={
                            urls.defaultUrls[value.data.userEntityId] ||
                            defaultUrl
                          }
                          dangerouslySetInnerHTML={{
                            __html: _.get(value, "data.userFirmName")
                          }}
                        />
                      </div>
                    ),
                    sortMethod: (a, b) =>
                      ((a && a.data && a.data.userFirmName) || "").localeCompare((b && b.data && b.data.userFirmName) || "")
                  },
                  {
                    id: "Type",
                    Header: "Type",
                    accessor: item => item,
                    Cell: ({ value }) => (
                      <div className="hpTablesTd wrap-cell-text">
                        {
                          value.data && value.data.entityRelationshipToTenant ?
                            <span
                              dangerouslySetInnerHTML={{
                                __html: _.get(value, "data.entityRelationshipToTenant")
                              }}
                            /> : null
                        }
                      </div>
                    ),
                    sortMethod: (a, b) =>
                      (
                        (a && a.data && a.data.entityRelationshipToTenant) ||
                        ""
                      ).localeCompare(
                        (b && b.data && b.data.entityRelationshipToTenant) || ""
                      )
                  },
                  // {
                  //   id: "User Address",
                  //   Header: "User Address",
                  //   accessor: item => item,
                  //   Cell: ({ value }) => (
                  //     <div className="hpTablesTd wrap-cell-text">
                  //       <span
                  //         dangerouslySetInnerHTML={{
                  //           __html: _.get(
                  //             value,
                  //             "data.userPrimaryAddress.addressLine1"
                  //           )
                  //         }}
                  //       />
                  //       &nbsp;
                  //       <span
                  //         dangerouslySetInnerHTML={{
                  //           __html: _.get(
                  //             value,
                  //             "data.userPrimaryAddress.addressLine2"
                  //           )
                  //         }}
                  //       />
                  //       &nbsp;
                  //       <span
                  //         dangerouslySetInnerHTML={{
                  //           __html: _.get(value, "data.userPrimaryAddress.city")
                  //         }}
                  //       />
                  //       &nbsp;
                  //       <span
                  //         dangerouslySetInnerHTML={{
                  //           __html: _.get(
                  //             value,
                  //             "data.userPrimaryAddress.state"
                  //           )
                  //         }}
                  //       />
                  //       &nbsp;
                  //       <span
                  //         dangerouslySetInnerHTML={{
                  //           __html: _.get(
                  //             value,
                  //             "data.userPrimaryAddress.zipCode.zip1"
                  //           )
                  //         }}
                  //       />
                  //       -
                  //       <span
                  //         dangerouslySetInnerHTML={{
                  //           __html: _.get(
                  //             value,
                  //             "data.userPrimaryAddress.zipCode.zip2"
                  //           )
                  //         }}
                  //       />
                  //     </div>
                  //   ),
                  //   sortMethod: (a, b) =>
                  //     (
                  //       (a &&
                  //         a.data &&
                  //         a.data.userPrimaryAddress &&
                  //         a.data.userPrimaryAddress.city) ||
                  //       ""
                  //     ).localeCompare(
                  //       (b &&
                  //         b.data &&
                  //         b.data.userPrimaryAddress &&
                  //         b.data.userPrimaryAddress.city) ||
                  //         ""
                  //     )
                  // },
                  // {
                  //   id: "User Flags",
                  //   Header: "User Flags",
                  //   accessor: item => item,
                  //   Cell: ({ value }) => (
                  //     <div className="hpTablesTd wrap-cell-text">
                  //       {value.data.userFlags.length ? (
                  //         <span
                  //           dangerouslySetInnerHTML={{
                  //             __html: _.get(value, "data.userFlags")
                  //           }}
                  //         />
                  //       ) : (
                  //         "-"
                  //       )}
                  //     </div>
                  //   ),
                  //   sortMethod: (a, b) =>
                  //     ((a && a.data && a.data.userFlags) || [])
                  //       .join(",")
                  //       .localeCompare(
                  //         ((a && a.data && b.data.userFlags) || []).join(",")
                  //       )
                  // },
                  {
                    id: "User Phone",
                    Header: "User Phone",
                    accessor: item => item,
                    Cell: ({ value }) => (
                      <div className="hpTablesTd wrap-cell-text">
                        {
                          (value.data && value.data.userPrimaryPhone && value.data.userPrimaryPhone.phoneNumber) ?
                            <a
                              href={`tel:${(value.data.userPrimaryPhone.phoneNumber) || null}`}
                              dangerouslySetInnerHTML={{
                                __html: _.get(value, "data.userPrimaryPhone.phoneNumber", "-")
                              }}
                            /> : "-"
                        }
                      </div>
                    ),
                    sortMethod: (a, b) =>
                      ((a && a.data && a.data.userPrimaryPhone && a.data.userPrimaryPhone.phoneNumber) || "").localeCompare(
                        (b && b.data && b.data.userPrimaryPhone && b.data.userPrimaryPhone.phoneNumber) || "")
                  },
                  // {
                  //   id: "Historical Data",
                  //   Header: "Historical Data",
                  //   accessor: item => item,
                  //   Cell: ({ value }) => {
                  //     const val = _.get(value, "data.historicalData")
                  //     return (
                  //       <div className="hpTablesTd wrap-cell-text">
                  //         {val && val !== "{}" ? (
                  //           <HistoricalData
                  //             data={(value.data || {}).historicalData || "{}"}
                  //           />
                  //         ) : (
                  //           <>-</>
                  //         )}
                  //       </div>
                  //     )
                  //   }
                  // }
                ]}
              />
            )}
          </div>
        )}
      />
    </div>
  </div>
)

export default Result
