/* eslint-disable react/no-danger, no-underscore-dangle */
import React from "react"
import { SelectedFilters, ReactiveList } from "@appbaseio/reactivesearch"
import ReactTable from "react-table"
import _ from "lodash"
import styled from "styled-components"
import { Link } from "react-router-dom"
import {toast} from "react-toastify"

import Loader from "../../Layout/Loader"
import NoResult from "../../Layout/NoResult"
import Error from "../../Layout/Error"
import HistoricalData from "./HistoricalData"
import Stats from "../../Layout/Stats"

import { unitConvertor } from "../../../../../globalutilities/amountDecorator"
import {CloneTransaction} from "../../../../GlobalComponents/CloneTransaction"
import CONST from "../../../../../globalutilities/consts"
import { createTranEmailAlert, makeCopyOfTransaction } from "../../../../StateManagement/actions/CreateTransaction"
import Flex from "../../Layout/Flex"
import {sortOptions} from "../../NavigationSearch/searchConfig"

const Card = styled.div`
  background: #fff;
  border-radius: 3px;
  padding: 5px 0px 8px 0px;
  align-items: center;
  border: 1px solid #e1e4e8;
`

const InfoContainer = styled(Flex)`
  font-size: small;
  margin-top: 5px;
`

const InfoItem = styled(Flex)`
  padding-left: 10px;
  border-left: ${props => (props.noborder ? 0 : "1px solid #e1e4e8")};
  font-size: small;
  width: 32%;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;

  @media (max-width: 769px) {
    flex-wrap: wrap !important;
  }
  
  > div {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }
`
const defaultUrl = "/dashboard"

class Result extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
      copyModalState: false, // eslint-disable-line
      copyFrom: {},
      makeCopy: {
        issueName: "",
        projectName: "",
        tabs: [
          "summary",
          "details",
          "participants"
        ],
        emailMessage: ""
      }
    }
  }

  changeSelectedAction = (value, item) => {
    const {makeCopy} = this.state
    let copyFrom = {}
    if(value === "copy"){
      this.cloneToggleModal()
      copyFrom = item
      console.log(copyFrom)
      makeCopy.issueName = item.tranIssueName
      makeCopy.projectName = item.tranProjectName
    }
    this.setState({
      copyFrom,
      makeCopy
    })
  }

  cloneToggleModal = () => {
    this.setState(prev => {
      const newState = !prev.copyModalState
      return { copyModalState: newState }
    })
  }

  onChange = (event) => {
    const {makeCopy} = this.state
    let stateObject = {}
    const {name, value, checked, type} = event.target
    if(name === "tabs" && makeCopy && makeCopy.tabs){
      if(checked && makeCopy.tabs.indexOf(value) === -1){
        makeCopy.tabs.push(value)
      }
      if(!checked && makeCopy.tabs.indexOf(value) !== -1){
        const findIndex = makeCopy.tabs.indexOf(value)
        makeCopy.tabs.splice(findIndex,1)
      }
      if(checked && value === "check-track" && makeCopy.tabs.indexOf("participants") === -1){
        makeCopy.tabs.push("participants")
      }
      if(makeCopy.tabs.indexOf("participants") === -1 && makeCopy.tabs.indexOf("check-track") !== -1){
        const findIndex = makeCopy.tabs.indexOf("check-track")
        makeCopy.tabs.splice(findIndex,1)
      }
      stateObject = makeCopy
    }else {
      stateObject = {
        ...makeCopy,
        [name]: type === "checkbox" ? checked : value,
      }
    }
    this.setState({
      makeCopy: stateObject
    })
  }

  onSave = () => {
    const {copyFrom, makeCopy} = this.state
    const {tranSubType, tranType} = copyFrom
    const payload = {
      tranId: (copyFrom && copyFrom.tranId) || "",
      tranSubType: (copyFrom && copyFrom.tranSubType) || "",
      tranType: (copyFrom && copyFrom.tranType) || "",
      ...makeCopy,
    }
    console.log("Copy Transaction ====>", copyFrom)
    if(!makeCopy.projectName){
      return toast("Project name is required.", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.WARNING
      })
    }
    const type =
      tranSubType === "BOND ISSUE" ? "deals" :
        tranSubType === "BANK LOAN" ? "loan" :
          tranType === "DERIVATIVE" ? "derivative" :
            tranType === "RFP" ? "rfp" :
              tranType === "OTHERS" ? "others" :
                tranType === "BUSINESSDEVELOPMENT" ? "bus-development" :
                  tranType === "MA-RFP" ? "marfp" : "loan"

    this.setState({
      loading: true,
    }, async () => {
      console.log(payload)
      const res = await makeCopyOfTransaction(payload)
      if(res && res.data && res.data.done){
        const {newTransaction} = res.data
        toast("Transaction Created Successfully.", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.SUCCESS
        })
        this.sendEmailAlertCreateTransaction(newTransaction, type, tranType)
      } else {
        toast(res.data.error || "Error in creating transaction. Please try again.", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.WARNING
        })
      }
      this.setState({
        loading: false,
        makeCopy: {
          issueName: "",
          projectName: "",
          emailMessage: "",
          tabs: [
            "summary",
            "details",
            "participants",
            // "check-track"
          ]
        },
        copyFrom: {}
      },async () => this.cloneToggleModal())
    })
  }

  sendEmailAlertCreateTransaction = async (newTransaction, type, tranType) => {
    const {svControls, makeCopy} = this.state
    const tranId = (newTransaction && newTransaction._id) || ""
    const emailPayload = {
      ...svControls,
      tranId,
      type,
      sendEmailUserChoice: true,
      emailParams: {
        ...this.state.email,
        url:
          tranType === "BUSINESSDEVELOPMENT"
            ? `bus-development/${tranId}`
            : `${type}/${tranId}/summary`,
        message: (makeCopy && makeCopy.emailMessage) || "",
        subject: `Create Transaction - ${makeCopy.issueName || makeCopy.projectName}`
      }
    }
    await createTranEmailAlert(emailPayload)
  }

  render() {
    const {urls, pageSize, onFiltersChange} = this.props
    return (
      <div>
        <CloneTransaction
          closeModal={this.cloneToggleModal}
          state={this.state}
          onChange={this.onChange}
          onSave={this.onSave}
        />
        <SelectedFilters onChange={(component, value) => onFiltersChange(component, value)} />
        <div style={{ position: "relative" }}>
          <ReactiveList
            componentId="TransactionSearchResult"
            dataField="_score"
            react={{
              and: [
                "Search_Transactions",
                "viewQuery_Transactions",
                "Status",
                "TransactionType",
                "ActivityClient",
                "PrincipalAmount",
                "CreatedDate",
                "CreatedAtRange",
                "globalSearchEntitlement"
              ]
            }}
            pagination
            paginationAt="both"
            excludeFields={["content"]}
            size={pageSize}
            loader={<Loader />}
            sortOptions={sortOptions.transactions}
            renderResultStats={stats => (
              <Stats
                style={{
                  position: "absolute",
                  left: 4,
                  fontSize: "small",
                  top: 18
                }}
              >
                Showing{" "}
                <b>
                  {stats.currentPage * pageSize + 1} -{" "}
                  {stats.currentPage * pageSize + stats.displayedResults}
                </b>{" "}
                of total <b>{stats.totalResults}</b> records
              </Stats>
            )}
            innerClass={{
              pagination: "reactive-search-pagination"
            }}
            renderNoResults={NoResult}
            renderError={() => <Error />}
            renderAllData={({ results }) => {
              if(this.state.loading) return <Loader />
              return (
                <div style={{ marginTop: 10 }}>
                  {Boolean(results.length) && (
                    <ReactTable
                      showPagination={false}
                      pageSize={results.length}
                      minRows={1}
                      className="-striped -highlight is-bordered"
                      data={results}
                      SubComponent={(row) => {
                        const data = row.original && row.original.data && row.original.data
                        return (
                          <Card>
                            <InfoContainer alignItems="center">
                              <InfoItem noborder>
                                <b>Primary Sector: &nbsp;</b>
                                <div title={data.tranPrimarySector}>
                                  {
                                    row.original ? <span
                                      dangerouslySetInnerHTML={{
                                        __html: _.get(row.original, "data.tranPrimarySector", "-")
                                      }}
                                    /> : "-"
                                  }
                                </div>
                              </InfoItem>

                              <InfoItem>
                                <b>Secondary Sector: &nbsp;</b>
                                <div title={data.tranSecondarySector}>
                                  {
                                    row.original ? <span
                                      dangerouslySetInnerHTML={{
                                        __html: _.get(row.original, "data.tranSecondarySector", "-")
                                      }}
                                    /> : "-"
                                  }
                                </div>
                              </InfoItem>

                              <InfoItem>
                                <b>Historical Data: &nbsp;</b>
                                <div title="View">
                                  {data.historicalData !== "{}" ? (
                                    <HistoricalData
                                      data={(data || {}).historicalData || "{}"}
                                    />
                                  ) : null}
                                </div>
                              </InfoItem>

                              <InfoItem>
                                <b>Principal Amount($): &nbsp;</b>
                                <div title={data.tranParAmount}>
                                  {
                                    data.tranParAmount && data.tranParAmount !== "-" ?
                                      unitConvertor(data.tranParAmount) : "-"
                                  }
                                </div>
                              </InfoItem>
                            </InfoContainer>
                          </Card>
                        )
                      }}
                      columns={[
                        {
                          Header: "More",
                          expander: true,
                          width: 65,
                          Expander: ({isExpanded}) => (
                            <div>{isExpanded ? <i style={{color: "#4288dd"}} className="fas fa-caret-down"/> :
                              <i style={{color: "#4288dd"}} className="fas fa-caret-right"/>}</div>
                          ),
                          style: {
                            cursor: "pointer",
                            fontSize: 25,
                            padding: "0",
                            textAlign: "center",
                            userSelect: "none"
                          }
                        },
                        {
                          id: "Issuer",
                          Header: "Issuer",
                          accessor: item => item,
                          Cell: ({ value }) => (
                            <div className="hpTablesTd wrap-cell-text">
                              <Link
                                to={
                                  urls.defaultUrls[value.data.clientEntityId] ||
                                  defaultUrl
                                }
                                dangerouslySetInnerHTML={{
                                  __html: _.get(value, "data.tranClientName")
                                }}
                              />
                            </div>
                          ),
                          sortMethod: (a, b) =>
                            (a.data.tranClientName || "").localeCompare(
                              b.data.tranClientName || ""
                            )
                        },
                        {
                          id: "Description",
                          Header: "Issue Name",
                          accessor: item => item,
                          Cell: ({ value }) => (
                            <div className="hpTablesTd wrap-cell-text">
                              <Link
                                to={urls.defaultUrls[value.data._id] || defaultUrl}
                                dangerouslySetInnerHTML={{
                                  __html: _.get(value, "data.activityDescription")
                                }}
                              />
                            </div>
                          ),
                          sortMethod: (a, b) =>
                            (a.data.activityDescription || "").localeCompare(
                              b.data.activityDescription || ""
                            )
                        },
                        {
                          id: "Type",
                          Header: "Type / Sub Type",
                          accessor: item => item,
                          Cell: ({ value }) => (
                            <div className="hpTablesTd wrap-cell-text">
                              <span
                                dangerouslySetInnerHTML={{
                                  __html: _.get(value, "data.tranType", "-")
                                }}
                              />&nbsp; /&nbsp;
                              {value.data.tranSubType && (
                                <span
                                  dangerouslySetInnerHTML={{
                                    __html: _.get(value, "data.tranSubType", "-")
                                  }}
                                />
                              )}
                            </div>
                          ),
                          sortMethod: (a, b) =>
                            (a.data.tranType || "").localeCompare(
                              b.data.tranType || ""
                            )
                        },
                        // {
                        //   id: "ParAmount",
                        //   Header: "Principal Amount($)",
                        //   accessor: item => item,
                        //   Cell: ({ value }) => (
                        //     <div className="hpTablesTd wrap-cell-text">
                        //       {value.data.tranParAmount &&
                        //       value.data.tranParAmount !== "-"
                        //         ? `$ ${unitConvertor(value.data.tranParAmount)}`
                        //         : "-"}
                        //     </div>
                        //   ),
                        //   maxWidth: 250
                        // },
                        // {
                        //   id: "Primary Sector",
                        //   Header: "Primary Sector",
                        //   accessor: item => item,
                        //   Cell: ({ value }) => (
                        //     <div className="hpTablesTd wrap-cell-text">
                        //       <span
                        //         dangerouslySetInnerHTML={{
                        //           __html: _.get(value, "data.tranPrimarySector", "-")
                        //         }}
                        //       />
                        //     </div>
                        //   ),
                        //   maxWidth: 250,
                        //   sortMethod: (a, b) =>
                        //     (a.data.tranPrimarySector || "").localeCompare(
                        //       b.data.tranPrimarySector || ""
                        //     )
                        // },
                        // {
                        //   id: "Secondary Sector",
                        //   Header: "Secondary Sector",
                        //   accessor: item => item,
                        //   Cell: ({ value }) => (
                        //     <div className="hpTablesTd wrap-cell-text">
                        //       <span
                        //         dangerouslySetInnerHTML={{
                        //           __html: _.get(
                        //             value,
                        //             "data.tranSecondarySector",
                        //             "-"
                        //           )
                        //         }}
                        //       />
                        //     </div>
                        //   ),
                        //   maxWidth: 250,
                        //   sortMethod: (a, b) =>
                        //     (a.data.tranSecondarySector || "").localeCompare(
                        //       b.data.tranSecondarySector || ""
                        //     )
                        // },
                        {
                          id: "Status",
                          Header: "Status",
                          accessor: item => item,
                          Cell: ({ value }) => (
                            <div className="hpTablesTd wrap-cell-text">
                              <span
                                dangerouslySetInnerHTML={{
                                  __html: _.get(value, "data.tranStatus", "-")
                                }}
                              />
                            </div>
                          ),
                          sortMethod: (a, b) =>
                            (a.data.tranStatus || "").localeCompare(
                              b.data.tranStatus || ""
                            )
                        },
                        // {
                        //   id: "Historical Data",
                        //   Header: "Historical Data",
                        //   accessor: item => item,
                        //   Cell: ({ value }) => {
                        //     const val = _.get(value, "data.historicalData")
                        //     return (
                        //       <div className="hpTablesTd wrap-cell-text">
                        //         {val && val !== "{}" ? (
                        //           <HistoricalData
                        //             data={(value.data || {}).historicalData || "{}"}
                        //           />
                        //         ) : (
                        //           <>""</>
                        //         )}
                        //       </div>
                        //     )
                        //   }
                        // },
                        {
                          id: "action",
                          Header: "Action",
                          accessor: item => item,
                          maxWidth: 150,
                          Cell: ({ value }) => (
                            <div className="hpTablesTd wrap-cell-text tooltips">
                              <div className="select is-small is-link">
                                <select value="" onChange={(e) => this.changeSelectedAction(e.target.value, value.data)} disabled={!(value && value.data && value.data.tranProjectName)}>
                                  <option value="">Action</option>
                                  <option value="copy">Make a copy</option>
                                </select>
                              </div>
                            </div>
                          )
                        }
                      ]}
                    />
                  )}
                </div>
              )
            }}
          />
        </div>
      </div>
    )
  }
}

export default Result
