/* eslint-disable indent, react/no-danger, no-underscore-dangle */
import React from "react"
import { SelectedFilters, ReactiveList } from "@appbaseio/reactivesearch"
import ReactTable from "react-table"
import _ from "lodash"
import { toast } from "react-toastify"
import { NavLink } from "react-router-dom"
import ReactTooltip from "react-tooltip"

import { updateTask } from "GlobalUtils/helpers"

import styled from "styled-components"
import swal from "sweetalert"
import Loader from "../../Layout/Loader"
import NoResult from "../../Layout/NoResult"
import Error from "../../Layout/Error"
import Stats from "../../Layout/Stats"
import dateFormat from "../../../../../globalutilities/dateFormat"
import CONST from "../../../../../globalutilities/consts"
import Flex from "../../Layout/Flex"
import {sortOptions} from "../../NavigationSearch/searchConfig"

const defaultUrl = "/dashboard?index=3"


const Card = styled.div`
  background: #fff;
  border-radius: 3px;
  padding: 5px 0px 8px 0px;
  align-items: center;
  border: 1px solid #e1e4e8;
`

const InfoContainer = styled(Flex)`
  font-size: small;
  margin-top: 5px;
`

const InfoItem = styled(Flex)`
  padding-left: 10px;
  border-left: ${props => (props.noborder ? 0 : "1px solid #e1e4e8")};
  font-size: small;
  width: 32%;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;

  @media (max-width: 769px) {
    flex-wrap: wrap !important;
  }
  
  > div {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }
`

class Result extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      eventStatus: ["Open", "Closed"],
      loading: false
    }
  }

  changeTaskStatus = (value, idx) => {
    console.log(value, idx)
    const payload = { "taskDetails.taskStatus": value }
    updateTask(idx, payload, "", this.statusChangeCallback)
  }

  statusChangeCallback = (err) => {
    this.setState({ loading: false })
    if (err) {
      toast("Error in updating task status. Please try again.", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.WARNING
      })
    } else {
      toast("Task Status Changed successfully", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.SUCCESS
      })
    }
  }

  isArchived = () => (
    swal("Warning", "Your Checklist for this task is Archived/Deleted", "warning")
  )

  render() {
    const { urls, pageSize, onFiltersChange } = this.props
    return (
      <div>
        <SelectedFilters onChange={(component, value) => onFiltersChange(component, value)} />
        <div style={{ position: "relative" }}>
          <ReactiveList
            componentId="TaskSearchResult"
            dataField="_doc"
            react={{
              and: [
                "Search_Tasks",
                "viewQuery_Tasks",
                "TaskStartDate",
                "TaskDueDate",
                "TaskPriority",
                "TaskRead",
                "TaskStatus",
                "TaskType",
                "Assignee",
                "TaskCategory",
                "globalSearchEntitlement"
              ]
            }}
            pagination
            paginationAt="both"
            excludeFields={["content"]}
            size={pageSize}
            loader={<Loader />}
            // sortOptions={sortOptions.tasks}
            renderResultStats={stats => (
              <Stats
                style={{
                  position: "absolute",
                  left: 4,
                  fontSize: "small",
                  top: 18
                }}
              >
                Showing{" "}
                <b>
                  {stats.currentPage * pageSize + 1} -{" "}
                  {stats.currentPage * pageSize + stats.displayedResults}
                </b>{" "}
                of total <b>{stats.totalResults}</b> records
              </Stats>
            )}
            innerClass={{
              pagination: "reactive-search-pagination"
            }}
            renderNoResults={NoResult}
            renderError={() => <Error />}
            renderAllData={({ results }) => {
              const onChange = (e, idx) => {
                const { value } = e.target
                const taskIndex = results.findIndex(t => t._id === idx)
                results[taskIndex].data.taskDetails.taskStatus = value
                this.setState({
                  loading: true
                },() => this.changeTaskStatus(value, idx))
              }

              if(this.state.loading) return <Loader />
              return (
                <div style={{ marginTop: 10 }}>
                  {
                    results.length ?
                      <ReactTable
                        showPagination={false}
                        pageSize={results.length}
                        minRows={1}
                        className="-striped -highlight is-bordered"
                        data={results}
                        columns={[
                          {
                            Header: "More",
                            expander: true,
                            width: 65,
                            Expander: ({isExpanded}) => (
                              <div>{isExpanded ? <i style={{color: "#4288dd"}} className="fas fa-caret-down"/> :
                                <i style={{color: "#4288dd"}} className="fas fa-caret-right"/>}</div>
                            ),
                            style: {
                              cursor: "pointer",
                              fontSize: 25,
                              padding: "0",
                              textAlign: "center",
                              userSelect: "none"
                            }
                          },
                          {
                            id: "Task",
                            Header: "Task",
                            accessor: item => item,
                            Cell: ({ value }) => (
                              <div>
                                <span
                                style={{
                                  width: "140px",
                                  height: "18px"
                                }}
                                className="overflow"
                                data-tip
                                data-for={`task${value._id}`}
                              >
                                  {value && !value.data.relatedActivityDetails ? (
                                    value.data.taskDetails.taskType === "Compliance" ? (
                                      <div className="hpTablesTd wrap-cell-text overflow">
                                        <NavLink
                                          to={
                                            value.data.taskDetails.taskNotes
                                              ? `/cac/view-control/view-detail?id=${
                                                value.data.taskDetails.taskNotes
                                                }`
                                              : "/cac/view-control"
                                          }
                                          dangerouslySetInnerHTML={{
                                            __html: _.get(
                                              value,
                                              "data.taskDetails.taskDescription",
                                              "-"
                                            )
                                          }}
                                        />
                                      </div>
                                    ) : (
                                      <span className="hpTablesTd wrap-cell-text overflow">
                                        <NavLink
                                          to={`/tasks/${value._id}`}
                                          dangerouslySetInnerHTML={{
                                            __html: _.get(
                                              value,
                                              "data.taskDetails.taskDescription",
                                              "-"
                                            )
                                          }}
                                        />
                                      </span>
                                    )
                                  ) : value.data.relatedActivityDetails ? (
                                    <div className="hpTablesTd wrap-cell-text overflow">
                                      {
                                        value.data.relatedActivityDetails.activityType ? (
                                          value.data.taskDetails.taskArchive ?
                                            <small className="has-text-link" onClick={this.isArchived}>{value.data.taskDetails.taskDescription}</small> :
                                            <NavLink
                                              to={`${(urls.allurls[value.data.tranId] || {})[
                                                "check-track"
                                                ] || defaultUrl}?cid=${
                                                value.data.relatedActivityDetails
                                                  .activityContext
                                                }`}
                                              dangerouslySetInnerHTML={{
                                                __html: _.get(
                                                  value,
                                                  "data.taskDetails.taskDescription",
                                                  "-"
                                                )
                                              }}
                                            />
                                        ) : (
                                          <span className="hpTablesTd wrap-cell-text overflow">
                                            <NavLink
                                              to={`/tasks/${value._id}`}
                                              dangerouslySetInnerHTML={{
                                                __html: _.get(
                                                  value,
                                                  "data.taskDetails.taskDescription",
                                                  "-"
                                                )
                                              }}
                                            />
                                          </span>
                                        )
                                      }
                                    </div>
                                  ) : (
                                    <span className="hpTablesTd wrap-cell-text overflow">
                                      <NavLink
                                        to={`/tasks/${value._id}`}
                                        dangerouslySetInnerHTML={{
                                          __html: _.get(
                                            value,
                                            "data.taskDetails.taskDescription",
                                            "-"
                                          )
                                        }}
                                      />
                                    </span>
                                  )}
                                </span><ReactTooltip
                                id={`task${value._id}`}
                                className="custom-tooltip"
                                place="right"
                              >
                                <div
                                  dangerouslySetInnerHTML={{
                                    __html: _.get(
                                      value,
                                      "data.taskDetails.taskDescription",
                                      "-"
                                    )
                                  }}
                                />
                              </ReactTooltip></div>
                            ),
                            maxWidth: 250,
                            sortMethod: (a, b) =>
                              (a.data.taskDetails.taskDescription || "").localeCompare(
                                b.data.taskDetails.taskDescription || ""
                              )
                          },
                          {
                            id: "Assignee",
                            Header: "Assignee",
                            accessor: item => item,
                            Cell: ({ value }) => (
                              <div className="hpTablesTd wrap-cell-text">
                                <NavLink
                                  to={
                                    urls.defaultUrls[
                                      value.data.taskDetails.taskAssigneeUserId
                                      ] || defaultUrl
                                  }
                                  dangerouslySetInnerHTML={{
                                    __html: _.get(
                                      value,
                                      "data.taskDetails.taskAssigneeName"
                                    )
                                  }}
                                />
                              </div>
                            ),
                            maxWidth: 250,
                            sortMethod: (a, b) =>
                              (a.data.taskDetails.taskAssigneeName || "").localeCompare(
                                b.data.taskDetails.taskAssigneeName || ""
                              )
                          },
                          {
                            id: "Task Type",
                            Header: "Task Type",
                            accessor: item => item,
                            Cell: ({ value }) => (
                              <div className="hpTablesTd wrap-cell-text">
                                <span
                                  dangerouslySetInnerHTML={{
                                    __html: _.get(
                                      value,
                                      "data.taskDetails.taskType",
                                      "-"
                                    )
                                  }}
                                />
                              </div>
                            ),
                            maxWidth: 250,
                            sortMethod: (a, b) =>
                              (a.data.taskDetails.taskType || "").localeCompare(
                                b.data.taskDetails.taskType || ""
                              )
                          },
                          {
                            id: "Task Status",
                            Header: "Task Status",
                            accessor: item => item,
                            Cell: ({ value }) => {
                              if(!(value && value.data && value.data.taskDetails && value.data.taskDetails.taskStatus)) return null
                              const task = value.data
                              const status = task.taskDetails.taskStatus
                              const taskDisabled = task.taskDetails.complianceTask
                              if (taskDisabled) {
                                return (
                                  <small>{status === "complete" ? "Closed" : status}</small>
                                )
                              }
                              return (
                                <div className="select is-small is-link">
                                  <select
                                    value={status}
                                    onChange={(event) => onChange(event, task._id)}
                                    disabled={status === "Closed" || task.complianceTask}
                                  >
                                    {this.state.eventStatus.map((e, i) => (<option key={i.toString()} value={e}>{e}</option>))}
                                  </select>
                                </div>
                              )
                            },
                            maxWidth: 250,
                            sortMethod: (a, b) =>
                              (a.data.taskDetails.taskStatus || "").localeCompare(
                                b.data.taskDetails.taskStatus || ""
                              )
                          },
                          // {
                          //   id: "Task Priority",
                          //   Header: "Task Priority",
                          //   accessor: item => item,
                          //   Cell: ({ value }) => (
                          //     <div className="hpTablesTd wrap-cell-text">
                          //       <span
                          //         dangerouslySetInnerHTML={{
                          //           __html: _.get(
                          //             value,
                          //             "data.taskDetails.taskPriority",
                          //             "-"
                          //           )
                          //         }}
                          //       />
                          //     </div>
                          //   ),
                          //   maxWidth: 250,
                          //   sortMethod: (a, b) =>
                          //     (a.data.taskDetails.taskPriority || "").localeCompare(
                          //       b.data.taskDetails.taskPriority || ""
                          //     )
                          // },
                          {
                            id: "startDate",
                            Header: "Start date",
                            accessor: item => item,
                            Cell: row => {
                              const item = row.value.data
                              return (
                                <div className="hpTablesTd -striped wrap-cell-text">
                                  {dateFormat(
                                    item.taskDetails && item.taskDetails.taskStartDate
                                  )}
                                </div>
                              )
                            },
                            sortMethod: (a, b) => {
                              a = new Date(a.data.taskDetails.taskStartDate).getTime()
                              b = new Date(b.data.taskDetails.taskStartDate).getTime()
                              return b > a ? 1 : -1
                            }
                          },
                          {
                            id: "dueDate",
                            Header: "Due date",
                            accessor: item => item,
                            Cell: row => {
                              const item = row.value.data
                              return (
                                <div className="hpTablesTd -striped wrap-cell-text">
                                  {dateFormat(
                                    item.taskDetails && item.taskDetails.taskEndDate
                                  )}
                                </div>
                              )
                            },
                            sortMethod: (a, b) => {
                              a = new Date(a.data.taskDetails.taskEndDate).getTime()
                              b = new Date(b.data.taskDetails.taskEndDate).getTime()
                              return b > a ? 1 : -1
                            }
                          },
                          // {
                          //   id: "Related Transaction",
                          //   Header: "Related Transaction",
                          //   accessor: item => item,
                          //   Cell: ({ value }) => (
                          //     <div className="hpTablesTd wrap-cell-text">
                          //       {value.data.activityDescription ? (
                          //         <NavLink
                          //           to={
                          //             urls.defaultUrls[value.data.tranId] || defaultUrl
                          //           }
                          //         >
                          //           <span
                          //             dangerouslySetInnerHTML={{
                          //               __html: _.get(
                          //                 value,
                          //                 "data.activityDescription",
                          //                 "-"
                          //               )
                          //             }}
                          //           />
                          //         </NavLink>
                          //       ) : (
                          //         "-"
                          //       )}
                          //     </div>
                          //   ),
                          //   maxWidth: 250,
                          //   sortMethod: (a, b) =>
                          //     (a.data.taskDetails.taskPriority || "").localeCompare(
                          //       b.data.taskDetails.taskPriority || ""
                          //     )
                          // }
                        ]}
                        SubComponent={(row) => {
                          const data = row.original && row.original.data && row.original.data
                          return (
                            <Card>
                              <InfoContainer alignItems="center">
                                <InfoItem noborder>
                                  <b>Task Priority: &nbsp;</b>
                                  <div title={(data.taskDetails && data.taskDetails.taskPriority) || ""}>
                                     <span
                                       dangerouslySetInnerHTML={{
                                         __html: _.get(row.original, "data.taskDetails.taskPriority", "-")
                                       }}
                                     />
                                  </div>
                                </InfoItem>

                                <InfoItem>
                                  <b>Related Transaction: &nbsp;</b>
                                  <div>
                                    {data.activityDescription ? (
                                      <NavLink to={urls.defaultUrls[data.tranId] || defaultUrl}>
                                    <span
                                      dangerouslySetInnerHTML={{
                                        __html: _.get(row.original, "data.activityDescription", "-")
                                      }}
                                    />
                                    </NavLink>) : "-"}
                                  </div>
                                </InfoItem>
                              </InfoContainer>
                            </Card>
                          )
                        }}
                      />
                    : null
                  }
                </div>
              )
            }}
          />
        </div>
      </div>
    )
  }
}

export default Result
