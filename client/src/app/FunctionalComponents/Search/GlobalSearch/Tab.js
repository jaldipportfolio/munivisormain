/* eslint-disable jsx-a11y/no-noninteractive-element-interactions, jsx-a11y/click-events-have-key-events */
import React from "react"

import StyledTab from "../Layout/StyledTab"
import { tabsConfig } from "./searchConfig"

export default ({ currentTab, onTabChange, view, isSideBarOpen }) => (
  <StyledTab view={view} alignItems="center" isSideBarOpen={isSideBarOpen}>
    {tabsConfig.map((item, i) => (
      <li
        key={item.name}
        className={i === currentTab ? "active" : ""}
        onClick={() => onTabChange(i)}
      >
        {item.name}
      </li>
    ))}
  </StyledTab>
)
