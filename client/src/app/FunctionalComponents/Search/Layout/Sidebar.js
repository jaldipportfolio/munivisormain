import React from "react"
import styled from "styled-components"
import media from "../utils/media"
// import {tabsConfig} from "../GlobalSearch/searchConfig";

const Wrapper = styled.div`
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 4;
  background-color: rgba(0, 0, 0, 0.8);
  box-sizing: border-box;
  ${media.desktop`
    display: none;
    z-index: 0;
    position: relative;
  `};
`

const StyledSideBar = styled.div`
  width: 320px;
  height: calc(100%);
  overflow-y: auto;
  padding: 15px 20px;
  position: fixed;
  left: 0;
  top: 52px;
  padding-bottom: 100px;
  z-index: 5;
  display: ${props => (props.isSideBarOpen && props.view !== "Calendar" ? "block" : "none")};
  background-color: white;
`

const Toggle = styled.div`
  position: fixed;
  z-index: 10;
  bottom: 10px;
  left: 10px;
  height: 50px;
  width: 50px;
  display: flex;
  justify-content: center;
  color: white;
  background: #4288dd;
  border-radius: 50%;
  display: ${props => (props.view === "Calendar" ? "none" : "flex")};
  align-items: center;
  cursor: pointer;
`

class SideBar extends React.Component {
  state = {
    isSideBarOpen: this.props.view !== "Calendar" && window.innerWidth >= 992
  }

  toggleState = () => {
    this.setState(prevState => ({
      isSideBarOpen: !prevState.isSideBarOpen
    }), () => this.props.handleToggle(this.state.isSideBarOpen))
  }

  render() {
    const { children, view } = this.props
    const { isSideBarOpen } = this.state
    return (
      <div>
        {isSideBarOpen && <Wrapper />}
        <StyledSideBar view={view} isSideBarOpen={isSideBarOpen}>{children}</StyledSideBar>
        <Toggle onClick={this.toggleState} view={view}>
          <i className={`fa ${isSideBarOpen ? "fa-close" : "fa-filter"}`} />
        </Toggle>
      </div>
    )
  }
}

export default SideBar
