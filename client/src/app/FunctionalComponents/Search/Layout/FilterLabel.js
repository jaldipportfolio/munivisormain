import styled from "styled-components"

const FilterLabel = styled.div`
  font-size: 18px;
  padding-bottom: 14px;
  margin-bottom: 15px;
  border-bottom: 1px solid #dfdfdf;
`

export default FilterLabel
