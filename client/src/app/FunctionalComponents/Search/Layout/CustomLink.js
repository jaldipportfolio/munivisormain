import styled from "styled-components"

const CustomLink = styled.span`
  color: #4288dd;
  cursor: pointer;
  &:hover {
    color: rgba(66, 136, 221, 0.8);
  }
`
export default CustomLink
