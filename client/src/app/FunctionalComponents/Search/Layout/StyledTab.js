import styled from "styled-components"
import Flex from "./Flex"
import media from "../utils/media"

const Tab = styled(Flex)`
  width: ${props => (props.isSidebar && props.view !== "Calendar" ? "calc(100% - 320px)" : "100%")};
  left: ${props => (props.isSidebar && props.view !== "Calendar" ? "320px" : "0")};
  position: fixed;
  top: 52px;
  z-index: 5;
  height: 56px;
  background: #fff;
  padding: 20px;
  overflow-y: auto;

  li {
    list-style: none;
    margin-right: 25px;
    cursor: pointer;
  }

  li.active {
    color: #4288dd;
  }

  li:hover {
    color: #4288dd;
  }
  transition: 0.5s;
  
  ${media.desktop`
  width: ${props => (props.isSideBarOpen && props.view !== "Calendar" ? "calc(100% - 320px)" : "100%")};
  left: ${props => (props.isSideBarOpen && props.view !== "Calendar" ? "320px" : "0")};
  padding: 10px 100px;
  `}
`

export default Tab
