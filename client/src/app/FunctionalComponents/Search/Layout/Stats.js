import styled from "styled-components"
import media from "../utils/media"

const Stats = styled.div`
  display: none;

  ${media.desktop`
    display: block;
  `}
`

export default Stats
