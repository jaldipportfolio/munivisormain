import styled from "styled-components"
import media from "../utils/media"

const SearchContainer = styled.div`
  display: ${props => (props.view === "Calendar" ? "none !important" : "block")};
  width: 100%;
  left: 0;
  position: fixed;
  top: ${props => (props.hasTopBar ? "106px" : "52px")};
  z-index: 5;
  display: flex;
  align-items: center;
  background-color: #4288dd;
  height: 60px;
  font-size: 15px;
  letter-spacing: 0.05rem;
  justify-content: center;
  transition: 0.5s;
  ${media.desktop`
    width: ${props => (props.isSideBarOpen ? "calc(100% - 320px)" : "100%")};
    left: ${props => (props.isSideBarOpen ? "320px" : "0")};
  `}
`

export default SearchContainer
