import styled from "styled-components"

import media from "../utils/media"

const Content = styled.div`
  width: 100%;
  left: 0;
  position: relative;
  padding: 30px;
  background-color: #f5f6f5;
  transition: 0.5s;
  
  ${media.desktop`
  width: ${props => (props.isSideBarOpen ? "calc(100% - 320px)" : "100%")};
  left: ${props => (props.isSideBarOpen ? "320px" : "0")};
  padding: 30px 100px;
  `}
`

export default Content
