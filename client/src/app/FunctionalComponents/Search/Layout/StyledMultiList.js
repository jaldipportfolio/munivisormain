import styled from "styled-components"
import { MultiList } from "@appbaseio/reactivesearch"

const StyledList = styled(MultiList)`
  input {
    border-radius: 3px;
  }
`

export default StyledList
