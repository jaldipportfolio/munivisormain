import styled from "styled-components"
import { DataSearch } from "@appbaseio/reactivesearch"
import media from "../utils/media"

const StyledSearch = styled(DataSearch)`
  width: 100%;

  input {
    border-radius: 3px;
  }
  ${media.tablet`
    padding: 0 20px;
  `}
`

export default StyledSearch
