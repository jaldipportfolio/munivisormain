import React from "react"
import { ReactiveBase, ReactiveComponent } from "@appbaseio/reactivesearch"
import axios from "axios"

import Layout from "../Layout/Layout"
import Container from "../Layout/Container"
import SearchContainer from "../Layout/SearchContainer"
import SearchBar from "./SearchBar"
import SideBar from "../Layout/Sidebar"
import Content from "../Layout/Content"
import FilterLabel from "../Layout/FilterLabel"
import Result from "./Result"
import Filters from "./Filters"
import Loader from "../../../GlobalComponents/Loader"

import { ELASTIC_SEARCH_URL } from "../../../../constants"
import { muniApiBaseURL } from "../../../../globalutilities/consts"
import { docParserEntitlement } from "../utils/entitlement-query"

class DocSearch extends React.Component {
  state = {
    isFetching: true,
    defaultQuery: null,
    isFetchingUrls: true,
    isSideBarOpen: false,
    urls: null
  }

  componentDidMount() {
    this.fetchEntitlements()
    this.fetchUrls()
    this.setState({
      isSideBarOpen: window.innerWidth >= 992
    })
  }

  componentWillUnmount() {
    this.isCancelled = true
  }

  fetchUrls = async () => {
    try {
      const {
        auth: {
          loginDetails: { relatedFaEntities }
        }
      } = this.props

      const res = await axios({
        method: "GET",
        url: `${muniApiBaseURL}common/geturlsfortenant/${
          relatedFaEntities[0].entityId
        }`,
        headers: { Authorization: localStorage.getItem("token") }
      })

      const { urlObject } = res.data

      if (!this.isCancelled) {
        this.setState({
          isFetchingUrls: false,
          urls: urlObject
        })
      }
    } catch (err) {
      if (!this.isCancelled) {
        this.setState({
          isFetchingUrls: false
        })
      }
      console.error(err)
    }
  }

  fetchEntitlements = async () => {
    try {
      const {
        auth: {
          userEntities: { relationshipToTenant }
        }
      } = this.props
      const res = await axios({
        method: "GET",
        url: `${muniApiBaseURL}entitlements/nav`,
        headers: { Authorization: localStorage.getItem("token") }
      })

      const { data } = res.data
      const defaultQuery = docParserEntitlement(
        relationshipToTenant,
        data.all || []
      )

      if (!this.isCancelled) {
        this.setState({
          isFetching: false,
          defaultQuery
        })
      }
    } catch (err) {
      if (!this.isCancelled) {
        this.setState({
          isFetching: false
        })
      }
      console.error(err)
    }
  }

  handleFiltersBarToggle = (isSideBarOpen) => {
    this.setState({
      isSideBarOpen
    })
  }

  render() {
    const { auth } = this.props
    const { isFetching, defaultQuery, isFetchingUrls, urls, isSideBarOpen } = this.state

    return (
      <div>
        {isFetching || isFetchingUrls ? (
          <Loader />
        ) : (
          <ReactiveBase
            url={ELASTIC_SEARCH_URL}
            app={`tenant_docs_${
              auth.loginDetails.relatedFaEntities[0].entityId
            }`}
            headers={{
              Authorization: localStorage.getItem("token")
            }}
          >
            <ReactiveComponent
              componentId="docEntitlement"
              key="docEntitlement"
              customQuery={() => defaultQuery}
            />
            <ReactiveComponent
              componentId="docStatus"
              key="docStatus"
              customQuery={
                () => ({
                  "query": {
                    bool: {
                      must_not: {
                        bool: {
                          should: [
                            {
                              match: {
                                "meta.isInActive": true
                              }
                            }
                          ]
                        }
                      }
                    }
                  }
                }
                )
              }
            />
            <Container>
              <SearchContainer isSideBarOpen={isSideBarOpen}>
                <SearchBar />
              </SearchContainer>
              <Layout>
                <SideBar handleToggle={this.handleFiltersBarToggle}>
                  <FilterLabel>
                    <b>Filters</b>
                  </FilterLabel>
                  <Filters />
                </SideBar>
                <Content isSideBarOpen={isSideBarOpen}>
                  <Result urls={urls} />
                </Content>
              </Layout>
            </Container>
          </ReactiveBase>
        )}
      </div>
    )
  }
}

export default DocSearch
