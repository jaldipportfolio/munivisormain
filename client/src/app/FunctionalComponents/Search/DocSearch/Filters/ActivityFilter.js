import React from "react"

import StyledList from "../../Layout/StyledMultiList"

const Activity = () => (
  <StyledList
    componentId="ActivityFilter"
    dataField="details.activityDescription.keyword"
    filterLabel="Activity"
    title="Activity"
    showCount={false}
    react={{
      and: [
        "docStatus",
        "Search",
        "ContextType",
        "CreatedDate",
        "UpdateDate",
        "Category",
        "User",
        "Type",
        "UpdatedAtRange",
        "CreatedAtRange",
        "docEntitlement",
        "ClientFilter"
      ]
    }}
    renderNoResults={() => <div>Please upload docs to get Activity filter</div>}
    loader="Fetching document categories ..."
  />
)

export default Activity
