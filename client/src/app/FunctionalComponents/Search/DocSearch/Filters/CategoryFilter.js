import React from "react"

import StyledList from "../../Layout/StyledMultiList"

const Category = () => (
  <StyledList
    componentId="Category"
    dataField="meta.category.keyword"
    filterLabel="Category"
    title="Category"
    showCount={false}
    react={{
      and: [
        "docStatus",
        "Search",
        "ContextType",
        "CreatedDate",
        "UpdateDate",
        "User",
        "Type",
        "UpdatedAtRange",
        "CreatedAtRange",
        "docEntitlement",
        "ActivityFilter",
        "ClientFilter"
      ]
    }}
    renderNoResults={() => <div>Please upload docs to get category filter</div>}
    loader="Fetching document categories ..."
  />
)

export default Category
