import React from "react"

import StyledList from "../../Layout/StyledMultiList"

const ClientFilter = () => (
  <StyledList
    componentId="ClientFilter"
    dataField="details.tranClientName.keyword"
    filterLabel="Client Filter"
    title="Client Filter"
    showCount={false}
    react={{
      and: [
        "docStatus",
        "Search",
        "ContextType",
        "CreatedDate",
        "UpdateDate",
        "Category",
        "User",
        "Type",
        "UpdatedAtRange",
        "CreatedAtRange",
        "docEntitlement",
        "ActivityFilter"
      ]
    }}
    renderNoResults={() => (
      <div>Please upload docs to get ClientFilter filter</div>
    )}
    loader="Fetching document categories ..."
  />
)

export default ClientFilter
