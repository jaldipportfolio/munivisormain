/* eslint-disable no-underscore-dangle */
import React from "react"
import { SelectedFilters, ReactiveList } from "@appbaseio/reactivesearch"

import ResultCard from "./ResultCard"
import Loader from "../../Layout/Loader"
import NoResult from "../../Layout/NoResult"
import Error from "../../Layout/Error"
import Stats from "../../Layout/Stats"
import {sortOptions} from "../../NavigationSearch/searchConfig"

const PAGE_SIZE = 5

const Result = ({ urls }) => (
  <div>
    <SelectedFilters />
    <div style={{ position: "relative" }}>
      <ReactiveList
        componentId="SearchResult"
        dataField="_score"
        react={{
          and: [
            "docStatus",
            "Search",
            "ContextType",
            "CreatedDate",
            "UpdateDate",
            "Category",
            "User",
            "Type",
            "UpdatedAtRange",
            "CreatedAtRange",
            "docEntitlement",
            "ActivityFilter",
            "ClientFilter"
          ]
        }}
        pagination
        paginationAt="both"
        renderData={res => <ResultCard key={res._id} data={res} urls={urls} />}
        excludeFields={["content"]}
        size={PAGE_SIZE}
        sortOptions={sortOptions.documents}
        loader={<Loader />}
        renderResultStats={stats => (
          <Stats
            style={{
              position: "absolute",
              left: 4,
              fontSize: "small",
              top: 18
            }}
          >
            Showing{" "}
            <b>
              {stats.currentPage * PAGE_SIZE + 1} -{" "}
              {stats.currentPage * PAGE_SIZE + stats.displayedResults}
            </b>{" "}
            of total <b>{stats.totalResults}</b> records
          </Stats>
        )}
        innerClass={{
          pagination: "reactive-search-pagination"
        }}
        renderNoResults={NoResult}
        renderError={() => <Error />}
      />
    </div>
  </div>
)

export default Result
