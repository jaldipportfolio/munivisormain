import React from "react"

import PathLevelFilter from "./PathLevelFilter"

const levels = [
  "path_level_0.keyword",
  "path_level_1.keyword",
  "path_level_2.keyword",
  "path_level_3.keyword",
  "path_level_4.keyword",
]

export default () => (
  <div>
    {
      levels.map((level, index) =>
        <div>
          <PathLevelFilter key={index.toString()} levelNumber={index+1} level={level} levels={levels}/>
          <br/>
        </div>
      )
    }
  </div>
)
