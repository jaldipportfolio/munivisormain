import React from "react"

import StyledList from "../../Layout/StyledMultiList"

const PathLevelFilter = ({ levels, level, levelNumber }) => (
  <StyledList
    componentId={`PathLevel${levelNumber}Filter`}
    dataField={level}
    filterLabel={`Directory Level ${levelNumber}`}
    title={`Directory Level ${levelNumber}`}
    showCount={false}
    react={{
      and: [
        "Search",
        "FileBrowserCategory",
        ...(levels.map((l, i) => l !== level && `PathLevel${i+1}Filter`))
      ]
    }}
  />
)

export default PathLevelFilter
