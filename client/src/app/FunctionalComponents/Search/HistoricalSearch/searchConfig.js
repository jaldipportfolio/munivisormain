
export const searchFields = [
  "meta.originalName",
  "doc_preview",
  "s3Key",
  "path_level_0",
  "path_level_1",
  "path_level_2"
]

export const getFileIcon = fileName => {
  const ext = fileName.split(".").pop()

  switch (ext.toLowerCase()) {
  case "pdf":
    return "fa-file-pdf-o"
  case "doc":
  case "docx":
    return "fa-file-word-o"
  case "ppt":
  case "pptx":
    return "fa-file-powerpoint-o"
  case "png":
  case "jpg":
  case "jpeg":
  case "gif":
  case "tiff":
  case "svg":
    return "fa-file-picture-o"
  case "xls":
  case "xlsx":
  case "xlsm":
  case "xltx":
  case "xltm":
  case "csv":
    return "fa-file-excel-o"
  default:
    return "fa-file"
  }
}

export const getSearchableFields = () => {
  const fields = []
  searchFields.forEach(item => {
    fields.push(item)
    fields.push(`${item}.keyword`)
    fields.push(`${item}.search`)
  })

  return fields
}

export const getWeights = () => {
  const weights = []
  searchFields.forEach(() => {
    weights.push(3)
    weights.push(1)
    weights.push(1)
  })

  return weights
}
