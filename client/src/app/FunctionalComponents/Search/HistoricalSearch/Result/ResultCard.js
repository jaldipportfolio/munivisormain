/* eslint-disable react/no-danger, no-underscore-dangle */
import React from "react"
import styled from "styled-components"
import {Link} from "react-router-dom"
import dateFormat from "dateformat"
import _ from "lodash"
import ReactTooltip from "react-tooltip"
import { getFile } from "GlobalUtils/helpers"

import CustomLink from "../../Layout/CustomLink"
import SmallLink from "../../Layout/SmallLink"

import hightlightPhrase from "../../utils/phrase-highlighter"
import {getDocUrl, getFileIcon} from "../../DocSearch/searchConfig"
import Flex from "../../Layout/Flex"
import media from "../../utils/media"

const Card = styled.div`
  background: #fff;
  margin: 20px 0;
  border-radius: 3px;
  align-items: center;
  border: 1px solid #e1e4e8;
`

const InfoContainer = styled(Flex)`
  font-size: small;
  margin-top: 5px;
`

const InfoItem = styled(Flex)`
  padding-left: 10px;
  border-left: ${props => (props.noborder ? 0 : "1px solid #e1e4e8")};
  font-size: small;
  width: 32%;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;

  > div {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }
`

const Content = styled.div`
  padding: 10px;
  border-top: 1px solid #e1e4e8;
  font-size: small;
  padding-bottom: 30px;
`

const Icon = styled.i`
  display: none;
  ${media.desktop`display: block;`};
`

const defaultUrl = "/dashboard"

const ResultCard = ({ data }) => {
  const splitRoute = (data.s3Key || "").split("/")
  splitRoute.splice(0, 2)
  let mainRoute = ""
  if(splitRoute.length > 1){
    mainRoute = splitRoute.join("/")
  }
  return (
    <Card>
      <Flex
        style={{
          padding: "10px 10px 15px",
          width: "100%"
        }}
        wrap="nowrap"
      >
        <Icon
          className={`far ${getFileIcon(data.meta.originalName)}`}
          style={{
            fontSize: 24,
            marginTop: 7,
            width: "3%"
          }}
        />
        <Flex
          flexDirection="column"
          style={{
            width: "85%"
          }}
        >
          <Flex alignItems="center">
            <Link
              to={
                getDocUrl(data.meta.contextType, data.meta.contextId) ||
                defaultUrl
              }
            >
              <CustomLink
                style={{
                  fontWeight: "bold",
                  marginLeft: 10
                }}
                dangerouslySetInnerHTML={{
                  __html:
                  data["meta.originalName.search"] ||
                  _.get(data, "meta.originalName")
                }}
              />
            </Link>
            {data.meta.uploadedBy && (
              <div>
                {" "}
                <span
                  style={{
                    marginLeft: 10
                  }}
                >
                -{"  "}
                </span>{" "}
                <Link
                  to={defaultUrl}
                >
                  {" "}
                  <SmallLink
                    style={{
                      fontSize: "small"
                    }}
                    dangerouslySetInnerHTML={{
                      __html:
                      data["meta.uploadedBy.search"] ||
                      _.get(data, "meta.uploadedBy")
                    }}
                  />{" "}
                </Link>
              </div>
            )}
          </Flex>
          <InfoContainer alignItems="center">
            {
              mainRoute ?
                <InfoItem noborder>
                  <b>File Path: &nbsp;</b>
                  <Link to={defaultUrl}>
                    <SmallLink>
                      <div
                        data-tip
                        data-for={`file${data._id}`}
                        dangerouslySetInnerHTML={{
                          __html: hightlightPhrase(mainRoute, "")
                        }}
                      />
                      <ReactTooltip id={`file${data._id}`}>
                        <span
                          dangerouslySetInnerHTML={{
                            __html: mainRoute
                          }}
                        />
                      </ReactTooltip>
                    </SmallLink>
                  </Link>
                </InfoItem>
                : null
            }
          </InfoContainer>
        </Flex>
        <div
          style={{
            fontSize: "small",
            textAlign: "right",
            width: "10%"
          }}
        >
          {dateFormat(data.updatedAt, "yyyy-mm-dd")}
        </div>
      </Flex>
      <Content>
        <p
          dangerouslySetInnerHTML={{
            __html:
            data["doc_content.search"] ||
            data.doc_content ||
            `${data.doc_preview}...`
          }}
          style={{
            overflowX: "hidden",
            textOverflow: "ellipsis"
          }}
        />
      </Content>
      <Flex
        justifyContent="flex-end"
        style={{
          padding: "10px",
          borderTop: "1px solid #e1e4e8"
        }}
      >
        <a  // eslint-disable-line
          className="button is-info is-small has-text-right"
          style={{background: "#4288dd"}}
          onClick={() => getFile(`?objectName=${encodeURIComponent(data.s3Key)}`, data.meta.originalName)}
          download={data.meta.originalName}>
          Download
        </a>
      </Flex>
    </Card>
  )
}


export default ResultCard
