/* eslint-disable no-underscore-dangle, indent */
import React from "react"
import {
  ReactiveBase,
  ReactiveComponent
} from "@appbaseio/reactivesearch"

import { ELASTIC_SEARCH_URL } from "../../../../constants"
import Container from "../Layout/Container"
import Layout from "../Layout/Layout"
import Content from "../Layout/Content"
import Result from "./Result/Result"
import SideBar from "../Layout/Sidebar"
import Filters from "./Filters"
import FilterLabel from "../Layout/FilterLabel"

import SearchContainer from "../Layout/SearchContainer"
import SearchBar from "./SearchBar"

class HistoricalSearch extends React.Component {
  state = {
    isSideBarOpen: true
  }

  handleFiltersBarToggle = (isSideBarOpen) => {
    this.setState({
      isSideBarOpen
    })
  }

  render() {
    const {isSideBarOpen} = this.state
    return (
      <ReactiveBase
        url={ELASTIC_SEARCH_URL}
        app="tenant_docs_historical_5d85b5a4a04e974464aa6a87"
        theme={{
          typography: {
            fontFamily: "GoogleSans, Gravity !important"
          },
          colors: {
            textColor: "rgba(10, 10, 10, 0.6)"
          }
        }}
        type="doc"
        headers={{
          Authorization: localStorage.getItem("token")
        }}
      >
        <ReactiveComponent
          customQuery={() => ({ query: {} })}
          componentId="FileBrowserCategory"
          key="viewQuery_FileBrowser"
        />
        {/* <DataSearch componentId="FileBrowser" dataField={[
          "doc_preview",
          "meta.originalName.keyword",
          "meta.originalName.search"
        ]} /> */}
        <Container>
          <SearchContainer isSideBarOpen={isSideBarOpen}>
            <SearchBar />
          </SearchContainer>
          <Layout>
            <SideBar handleToggle={this.handleFiltersBarToggle}>
              <FilterLabel>
                <b>Filters</b>
              </FilterLabel>
              <Filters />
            </SideBar>
            <Content isSideBarOpen={isSideBarOpen}>
              <Result />
            </Content>
          </Layout>
        </Container>
      </ReactiveBase>
    )
  }
}

export default HistoricalSearch
