import { connect } from "react-redux"
import { withRouter } from "react-router-dom"
import NavigationSearch from "./NavigationSearch"

const mapStateToProps = state => ({
  auth: state.auth
})

export default connect(mapStateToProps)(withRouter(NavigationSearch))
