export const config = {
  query: {
    query: {
      terms: {
        "category.keyword": [
          "Deals",
          "Manage Rfps",
          "MA RFPs",
          "Bank Loans",
          "Derivatives",
          "Business Development Activities",
          "Other Transactions",
          "Entity Details",
          "User Details"
        ]
      }
    }
  },
  searchFields: [
    "data.tranClientName",
    "data.activityDescription",
    "data.entityName",
    "data.userFirstName",
    "data.userLastName",
    "category"
  ],
  weights: [10, 9, 8, 7, 6, 8]
}

export const getSearchableFields = () => {
  const fields = []
  config.searchFields.forEach(item => {
    fields.push(item)
    fields.push(`${item}.keyword`)
    fields.push(`${item}.search`)
  })

  return fields
}

export const getWeights = () => {
  const weights = []
  config.searchFields.forEach((item, index) => {
    weights.push(index)
    weights.push(index)
    weights.push(1)
  })

  return weights
}

export const sortOptions =  {
  transactions: [
    {
      label: "Show latest updated transactions first",
      dataField: "data.updatedAt",
      sortBy: "desc"
    },
    {
      label: "Show recently created transactions first ",
      dataField: "data.createdAt",
      sortBy: "desc"
    },
    {
      label: "Sort transactions from earliest to latest",
      dataField: "data.createdAt",
      sortBy: "asc"
    }
  ],
  entities: [
    {
      label: "Show latest updated entities first",
      dataField: "data.updatedAt",
      sortBy: "desc"
    },
    {
      label: "Show recently created entities first",
      dataField: "data.createdAt",
      sortBy: "desc"
    },
    {
      label: "Sort entities from earliest to latest",
      dataField: "data.createdAt",
      sortBy: "asc"
    }
  ],
  users: [
    {
      label: "Show latest updated users first",
      dataField: "data.userUpdatedAt",
      sortBy: "desc"
    },
    {
      label: "Show recently created users first",
      dataField: "data.userCreatedAt",
      sortBy: "desc"
    },
    {
      label: "Sort users from earliest to latest",
      dataField: "data.userCreatedAt",
      sortBy: "asc"
    }
  ],
  tasks: [
    {
      label: "Show latest updated tasks first",
      dataField: "data.updatedAt",
      sortBy: "desc"
    },
    {
      label: "Show recently created tasks first",
      dataField: "data.createdAt",
      sortBy: "desc"
    },
    {
      label: "Sort tasks from earliest to latest",
      dataField: "data.createdAt",
      sortBy: "asc"
    }
  ],
  documents: [
    {
      label: "Show latest updated documents first",
      dataField: "updatedAt",
      sortBy: "desc"
    },
    {
      label: "Show recently created documents first",
      dataField: "createdAt",
      sortBy: "desc"
    },
    {
      label: "Sort documents from earliest to latest",
      dataField: "createdAt",
      sortBy: "asc"
    }
  ]
}
