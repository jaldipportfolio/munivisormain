import React from "react"
import {toast} from "react-toastify"
import {withRouter} from "react-router-dom"
import connect from "react-redux/es/connect/connect"
import Documents from "../component/Document"
import Loader from "../../../../../GlobalComponents/Loader"
import {getDocFolders, getDocsByContextId} from "../../../../../StateManagement/actions/docs_actions/docs"
import {fetchAssigned} from "../../../../../StateManagement/actions/CreateTransaction"
import CONST, {ContextType} from "../../../../../../globalutilities/consts"

class SharedDocuments extends React.Component {
  constructor(){
    super()
    this.state = {
      showDocuments: [],
      userDocsList: [],
      folderPropList: [],
      folderList: [],
      firmUsersList: [],
      loading: true,
    }
  }

  async componentWillMount() {
    const {user} = this.props
    const docFolder = await getDocFolders("")
    const folderPropList = []
    docFolder.data.map(doc => {
      const matchFolder = doc.shareFolder.filter(f => f.id === user.userId)
      const matchDocument = doc.shareDocuments.filter(s => s.id === user.userId)
      if (matchFolder.length > 0 || matchDocument.length > 0) {
        folderPropList.push(doc)
      } else {
        if (doc.myDocuments === false) {
          folderPropList.push(doc)
        }
      }
    })
    const firmFolder = folderPropList.filter(u => u.entityId === user.entityId)
    const folderList = firmFolder.map(f => f.folderName)
    fetchAssigned(this.props.user.entityId, res => {
      this.setState({
        firmUsersList: res && res.usersList || [],
        folderPropList: firmFolder,
        folderList,
        loading: false
      })
    })
  }

  onSave = async (folderId, callback) => {
    const query = `?contextType=SHAREDOCS&&folderId=${folderId}`
    const getDocs = await getDocsByContextId(query)
    if (getDocs && getDocs.status === 200) {
      toast("documents saved successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
      callback({
        status: true,
        documentsList: getDocs && getDocs.data || [],
      })
    } else {
      toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
      callback({
        status: false
      })
    }
  }

  onDeleteDoc = async (documentId, folderId, callback) => {
    const query = `?contextType=SHAREDOCS&&folderId=${folderId}`
    const getDocs = await getDocsByContextId(query)
    if (getDocs && getDocs.status === 200) {
      toast("documents removed successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
      callback({
        status: true,
        documentsList: getDocs && getDocs.data || [],
      })
    } else {
      toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
      callback({
        status: false
      })
    }
  }


  render() {
    const { loading, showDocuments, folderPropList, userDocsList, folderList, firmUsersList } = this.state
    const { svControls } = this.props
    if(loading) {
      return <Loader />
    }
    return (
      <div>
        <Documents {...this.props} title="My Documents" pickCategory="LKUPDOCCATEGORIES" pickSubCategory="LKUPCORRESPONDENCEDOCS" pickAction="LKUPDOCACTION" onSave={this.onSave} onDeleteDoc={this.onDeleteDoc}
          showDocuments={showDocuments} folderPropList={folderPropList || []} userDocsList={userDocsList || []} folderList={folderList || []} firmUsersList={firmUsersList} contextType={ContextType.shareDocs} svControls={svControls}/>
      </div>
    )
  }
}


const mapStateToProps = state => ({
  loggedInContactId: (state.auth && state.auth.userEntities && state.auth.userEntities.userId) || "",
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

export default withRouter(connect(mapStateToProps, null)(SharedDocuments))
