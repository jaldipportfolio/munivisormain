import React from "react"
import { Modal } from "../../../../../FunctionalComponents/TaskManagement/components/Modal"

export const CopyDocumentsModal = (props) => (
  <Modal
    closeModal={props.closeModal}
    saveModal={props.onFolderCopy}
    modalState={props.saveModal}
    title="Copy Documents"
    buttonName="Copy"
    disabled={props && !props.folderName}
  >
    <div>
      <p className="multiExpLbl">Selected Documents : {props.keys.length}</p>
      <br/>
      <div className="columns">
        <div className="column">
          <p className="multiExpLbl">Select Folder<span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span></p>
          <div className="select is-small is-fullwidth is-link">
            <select name="folderName" value={props.folderName || ""} onChange={props.onChange} className="is-link">
              <option value="">Pick</option>
              {props.folderList.map((value) => <option key={value} value={value}>{value}</option>)}
            </select>
          </div>
        </div>
      </div>
    </div>
  </Modal>
)

export default CopyDocumentsModal
