import React from "react"
import {DropDownSelect} from "../../../../../GlobalComponents/TextViewBox"


const ManageDocumentView = ({
  doc = {},
  index,
  dropDown,
  errors = {},
  onRemove,
  chooseMultiple,
  onFolderChange,
  clickUpload,
  onClickUpload,
  duplicateList,
  contextType,
  onChangeUpload,
  tabIndex
}) => {

  const onFileBtnClick = (e, index) => {
    if(e.keyCode === 13 || e.keyCode === 32){
      clickUpload(index)
    }
  }

  const onDeleteBtnClick = (e) =>{
    if(e.keyCode === 13){
      onRemove(index)
    }
  }

  return (
    <tbody className="is-marginless is-paddingless doc-view">
      <tr>

        <td>
          <div className="complain-details">
            <DropDownSelect
              tabIndex={tabIndex + 1}
              name="folderName"
              data={contextType === "SHAREDOCS" ? duplicateList : dropDown.folderList || []}
              value={doc.folderName || ""}
              onChange={value => onFolderChange("folderName", value, "", index)}
            />
          </div>
        </td>
        <td>
          <div style={{display: "table-cell"}}>
            <input
              type="file"
              style={{ display: "none" }}
              onClick={onClickUpload}
              id={index}
              onChange={(event) => onChangeUpload(event, index)}
              disabled={chooseMultiple}
            />
            <div
              tabIndex={tabIndex + 4}
              className={`file is-small ${chooseMultiple ? "disabled" : ""}`}
              style={{ cursor: chooseMultiple ? "not-allowed" : "pointer" }}
              onClick={() => clickUpload(index)}
              onKeyDown={(e)=> onFileBtnClick(e, index)}
            >
              <span className="file-cta">
                <span className="file-icon">
                  <i className="fas fa-upload" />
                </span>
                <span className="file-label">
                 Choose a file…
                </span>
              </span>
            </div>
          </div>
          {errors.docFileName && <p className="text-error">{errors.docFileName}</p>}
        </td>
        <td>
          <div className="control">
            <div className="multiExpTblVal">
              {doc.docFileName}
            </div>
          </div>
        </td>

        <td>
          <div className="field is-grouped">
            <div className="control">
            <a tabIndex={tabIndex + 5} onKeyDown={onDeleteBtnClick} onClick={() => onRemove(index)}> {/* eslint-disable-line */}
                <span className="has-text-link">
                  <i className="far fa-trash-alt" title="Delete"/>
                </span>
              </a>
            </div>
          </div>
        </td>
      </tr>
    </tbody>
  )
}

export default ManageDocumentView
