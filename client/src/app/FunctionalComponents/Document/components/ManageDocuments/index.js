import React from "react"
import { NavLink, withRouter } from "react-router-dom"
import { connect } from "react-redux"
import Loader from "Global/Loader"
import MyDocuments from "./component/MyDocuments"
import SharedDocuments from "./component/SharedDocuments"
import Audit from "../../../../GlobalComponents/Audit"
import {checkSupervisorControls} from "../../../../StateManagement/actions/CreateTransaction"
import {activeStyle} from "../../../../../globalutilities/consts"


const TABS = [
  { path: "my-docs", label: "My Documents" },
  { path: "shared-docs", label: "Shared Documents" },
  { path: "audit-trail", label: "Activity Log" }
]

class Index extends React.Component {
  constructor() {
    super()
    this.state = {
      loading:true,
      auditLogs: [],
      svControls: {},
    }
  }

  async componentWillMount() {
    const {user, nav1, nav2} = this.props
    const svControls = await checkSupervisorControls()
    const submitAudit = user && user.settings && user.settings.auditFlag === "no" ? false
      : user && user.settings && user.settings.auditFlag === "currentState" ? true
        : (user && user.settings && user.settings.auditFlag === "supervisor" && (svControls && svControls.supervisor)) || false
    if (!submitAudit) {
      const index = TABS.findIndex(tab => tab.label === "Activity Log")
      if(index !== -1){
        TABS.splice(index, 1)
      }
    }
    this.setState({
      loading: false,
      TABS,
      svControls
    }, () => {
      if(!nav2){
        this.props.history.push(`/${nav1}/my-docs`)
      }
    })
  }

  renderViewSelection = () => (
    <nav className="tabs is-boxed">
      <ul>{this.renderTabs(this.state.TABS)}</ul>
    </nav>
  )

  renderTabs = (tabs) =>
    tabs.map(t => (
      <li key={t.path}>
        <NavLink to={`/tools-docs/${t.path}`} activeStyle={activeStyle}>
          {t.label}
        </NavLink>
      </li>
    ))

  renderSelectedView = (option) => {
    const { auditLogs, svControls } = this.state
    switch (option) {
    case "my-docs":
      return (
        <MyDocuments
          {...this.props}
          auditLogs={auditLogs}
          svControls={svControls}
        />
      )
    case "shared-docs":
      return (
        <SharedDocuments
          {...this.props}
          auditLogs={auditLogs}
          svControls={svControls}
        />
      )
    case "audit-trail":
      return (
        <Audit
          {...this.props}
          svControls={svControls}
        />
      )
    default:
      return <p>{option}</p>
    }
  }


  render() {
    const { nav2 } = this.props
    const loading = () => <Loader/>
    if (this.state.loading) {
      return loading()
    }

    return (
      <div>
        <div className="hero is-link">
          <div className="hero-foot hero-footer-padding">
            <div className="container">
              {this.renderViewSelection(nav2)}
            </div>
          </div>
        </div>
        <section id="main" className="bankloan">
          {this.renderSelectedView(nav2)}
        </section>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  loggedInContactId: (state.auth && state.auth.userEntities && state.auth.userEntities.userId) || "",
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

export default withRouter(connect(mapStateToProps, null)(Index))
