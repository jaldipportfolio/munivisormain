import React from "react"
import { Link } from "react-router-dom"
import ReactTable from "react-table"
import { connect } from "react-redux"
import DownloadButton from "./DownloadButton"

export const TRANSACTION_TYPES = {
  DEALS: "Deal",
  RFPS: "RFP",
  BANKLOANS: "Bank Loans",
  DERIVATIVE: "Derivative",
  MARFP: "MA-RFP",
  OTHER: "others",
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  allurls: state.urls.allurls
})

const ResultSet = ({ items, allurls }) => (
  <div style={{ marginTop: "20px"}}>
    <ReactTable
      data={items}
      pageSize={(items || []).length}
      columns={[
        {
          id: "client",
          Header: "Client",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd">
                <Link to={`/clients-propects/${item.issuerId}/entity`} dangerouslySetInnerHTML={{__html: item.issuerName || "-"}} />
              </div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => {
            const x = a.issuerName || "-"
            const y = b.issuerName || "-"
            return x.localeCompare(y)
          }
        },
        {
          id: "doc",
          Header: "Document",
          accessor: item => item,
          Cell: row => {
            const item = row.value.docDetails
            return (
              <div className="hpTablesTd"><div dangerouslySetInnerHTML={{__html: item.fileName || "-"}} /></div>
            )
          },
          maxWidth: 250,
          sortMethod: (a, b) => a.docDetails.fileName.localeCompare(b.docDetails.fileName)
        },
        {
          id: "context",
          Header: "Context",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd "><div dangerouslySetInnerHTML={{__html: item.tranType || "-"}} /></div> //eslint-disable-line
            )
          },
          maxWidth: 150,
          sortMethod: (a, b) => a.tranType.localeCompare(b.tranType)
        },
        {
          id: "desc",
          Header: "Description",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div className="hpTablesTd "><Link to={allurls[item.tranId].summary} dangerouslySetInnerHTML={{__html: item.activityDescription || "-"}} /></div> //eslint-disable-line
            )
          },
          maxWidth: 150,
          sortMethod: (a, b) => a.activityDescription.localeCompare(b.activityDescription)
        },
        {
          id: "uploadedUser",
          Header: "Uploaded By",
          accessor: item => item,
          Cell: row => {
            const item = row.value.docDetails
            return (
              <div className="hpTablesTd "><Link to={`/addnew-contact/${item.docUploadUserId}`} dangerouslySetInnerHTML={{__html: item.uploadedBy || "-"}} /></div>
            )
          },
          maxWidth: 150,
          sortMethod: (a, b) => a.docDetails.uploadedBy.localeCompare(b.docDetails.uploadedBy)
        },
        {
          id: "content",
          Header: "Content",
          accessor: item => item,
          Cell: row => {
            const item = row.value
            return (
              <div
                style={{ height: "auto", overflowX: "hidden", overflowY: "auto"}}
                className="hpTablesTd"
                dangerouslySetInnerHTML={{
                  __html: item["attachment.content"] || "Enter search phrase to query file content in addition to related information"
                }}
              />
            )
          },
        },
        {
          id: "download",
          Header: "Download",
          accessor: item => item,
          Cell: row => {
            const item = row.value.docDetails
            return (
              <DownloadButton docId={item.docAWSReference} />
            )
          },
        },
      ]}
      showPagination={false}
      minRows={2}
      className="-striped -highlight is-bordered DocumentTable"
    />
  </div>
)

export default connect(mapStateToProps)(ResultSet)
