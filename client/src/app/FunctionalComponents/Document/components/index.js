/* eslint-disable jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events, no-underscore-dangle */
import React from "react"
import axios from "axios"
import { connect } from "react-redux"
import _ from "lodash"
import { muniApiBaseURL } from "../../../../globalutilities/consts"
import { getAllTransactions, getTransactionIds } from "../../Dashboard/helper"
import TransactionDocumentSearch, { TRANSACTION_CONTEXT } from "./TransactionDocumentSearch"
import { ELASTIC_SEARCH_URL, ELASTIC_SEARCH_INDEX } from "../../../../constants"
import "./result.css"
import ContentDocs from "../../ContentDocs"
import { getHeaders } from "../../../../globalutilities"
import EntityDocumentSearch, { ENTITY_CONTEXT } from "./EntityDocumentSearch"

const mapStateToProps = (state) => ({
  auth: state.auth
})

class Document extends React.Component {
  constructor() {
    super()
    this.state = {
      isFetching: true,
      entitledIds: [],
      lastCronTime: null,
      entityEntitledIds: [],
      context: TRANSACTION_CONTEXT,
    }
  }

  componentDidMount() {
    this.fetchEntitledList()
  }


  handleContextChange = (e) => {
    this.setState({
      context: e.target.value,
    })
  }

  fetchEntitledList = async () => {
    const { auth } = this.props

    axios({
      method: "GET",
      url: `${muniApiBaseURL}entitlements/all`,
      headers: { Authorization: auth.token },
    }).then(async res => {
      const { data } = res.data
      const transactionIds = getTransactionIds(getAllTransactions(_.pick(data, ["deals", "marfps", "derivatives", "rfps", "bankloans", "others"])))
      this.setState({
        isFetching: false,
        entitledIds: transactionIds,
        entityEntitledIds: data.entities.all,
      })
      try {
        const resawait = await axios.get(`${ELASTIC_SEARCH_URL}${ELASTIC_SEARCH_INDEX}/cron-keeper/last_cron`, { headers: getHeaders() })
        const cronData = await resawait.data
        const lastCronTime = cronData._source.time
        this.setState({
          lastCronTime,
        })
      } catch (exr) {
        console.log(exr)
      }

    }).catch(() => {
      this.setState({
        isFetching: false,
        entitledIds: []
      })
    })
  }

  render() {
    const { entitledIds, isFetching, lastCronTime, context, entityEntitledIds } = this.state
    let contextKey = "tranId.raw"
    let defaultQueryValues = entitledIds

    if (context === TRANSACTION_CONTEXT) {
      contextKey = "tranId.raw"
      defaultQueryValues = entitledIds
    }

    if (context === ENTITY_CONTEXT) {
      contextKey = "entityId.raw"
      defaultQueryValues = entityEntitledIds
    }

    return (
      <div id="main">
        <section>
          {
            isFetching
              ? (
                <h3> Loading data ...</h3>
              )
              : (
                <div>
                  <div className="select is-link is-small" style={{ display: "flex", justifyContent: "flex-end", alignItems: "center" }}>
                    <b> Context &nbsp;</b>
                    <select value={context} onChange={this.handleContextChange}>
                      <option value={TRANSACTION_CONTEXT}>{TRANSACTION_CONTEXT}</option>
                      <option value={ENTITY_CONTEXT}>{ENTITY_CONTEXT}</option>
                    </select>
                  </div>
                  {
                    context === TRANSACTION_CONTEXT && (
                      <TransactionDocumentSearch
                        defaultQueryKey={contextKey}
                        defaultQueryValues={defaultQueryValues}
                        lastCronTime={lastCronTime}
                        context={context}
                      />
                    )
                  }
                  {
                    context === ENTITY_CONTEXT && (
                      <EntityDocumentSearch
                        defaultQueryKey={contextKey}
                        defaultQueryValues={defaultQueryValues}
                        lastCronTime={lastCronTime}
                        context={context}
                      />
                    )
                  }

                </div>

              )
          }
          <ContentDocs />
        </section>
      </div>
    )
  }
}

export default connect(mapStateToProps)(Document)
