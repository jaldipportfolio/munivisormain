const ORIGINAL_SEARCH_FIELD = [
  {
    field: "issuerName",
    weight: 8
  },
  {
    field: "activityDescription",
    weight: 7
  },
  {
    field: "docDetails.uploadedBy",
    weight: 6,
  },
  {
    field: "docDetails.fileName",
    weight: 7,
  },
  {
    field: "entityName",
    wieght: 9,
  }
]

export const getFieldsAndWeights = () => {
  const ones = ORIGINAL_SEARCH_FIELD.map(() => 1)

  const searchFields = [
    ...ORIGINAL_SEARCH_FIELD.map(f => f.field),
    "attachment.content",
    ...ORIGINAL_SEARCH_FIELD.map(f => `${f.field}.raw`),
    ...ORIGINAL_SEARCH_FIELD.map(f => `${f.field}.searchable`),
    ...ORIGINAL_SEARCH_FIELD.map(f => `${f.field}.autoSuggest`)
  ]

  const fieldWeights = [
    ...ORIGINAL_SEARCH_FIELD.map(f => parseFloat(((f.weight*3)/10).toFixed(2))),
    3,
    ...ORIGINAL_SEARCH_FIELD.map(f => f.weight),
    ...ones,
    ...ones,
  ]

  return ({
    searchFields,
    fieldWeights
  })
}
