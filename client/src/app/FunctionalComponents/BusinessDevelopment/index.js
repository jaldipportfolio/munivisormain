import React, { Component } from "react"
import { withRouter } from "react-router-dom"
import { connect } from "react-redux"
import cloneDeep from "lodash.clonedeep"
import * as qs from "query-string"
import CONST from "GlobalUtils/consts"
import { toast } from "react-toastify"
import moment from "moment"
import Loader from "../../GlobalComponents/Loader"
import SearchActivity from "./components/SearchActivity"
import ActiityList from "./components/ActivityList"
import TaskActivity from "./components/TaskActivity"
import Nav from "../Dashboard/components/Nav"
import { fetchAssigned } from "../../StateManagement/actions/CreateTransaction"
import { getPicklistByPicklistName } from "GlobalUtils/helpers"
import {
  getBussinessDevTask,
  bussinessActivityDetails,
  postBussinessActivityDetails,
  putTaskDocumentTransaction,
  delTransactionDocStatuse,
  putTaskDocumentTransactionStatus,
  searchTaskByTranEntity
} from "../../StateManagement/actions/BussinessDevelopment"
import { BusinessDevelopmentTaskValidation } from "./Validation/BusinessDevelopmentTaskValidation"

class BusinessDevelopment extends Component {
  constructor(props) {
    super(props)
    this.state = {
      taskDetails: cloneDeep(CONST.activityTask),
      dropDown: {
        assignee: [],
        contacts: [],
        taskType: [],
        taskStatus: [],
        issuerList: [],
      },
      activity: {},
      activityDetails: {},
      sectionGroup: {},
      allSectionGroup: {},
      errorMessages: {},
      taskId: "",
      searchText: "",
      documentsUpload: false,
      loading: true,
      isSaveDisabled: false,
      isNewTask:false,
      disabled: false
    }
  }

  async componentDidMount() {
    const { nav2 } = this.props
    let { sectionGroup } = this.state
    let activity = {}
    let activityDetails = {}
    if (nav2) {
      activity = await bussinessActivityDetails(nav2)
      const tasks = await getBussinessDevTask(nav2)
      sectionGroup = tasks.taskGroupBy || {}
      activityDetails = tasks.taskDetails || {}
    } else {
      this.onTaskSearch()
    }
    const picResult = await getPicklistByPicklistName([
      "LKUPBUSINESSTASKTYPE",
      "LKUPBUSINESSTASKSTATUS",
    ])
    const result = (picResult.length && picResult[1]) || {}
    this.setState(prevState => ({
      dropDown: {
        ...prevState.dropDown,
        taskType: (result && result.LKUPBUSINESSTASKTYPE) || [],
        taskStatus: (result && result.LKUPBUSINESSTASKSTATUS) || []
      },
      activityDetails,
      activity,
      taskDetails: {
        ...prevState.taskDetails,
        relatedEntityDetails: {
          entityId: activity.actIssuerClient || "",
          entityName: activity.actIssuerClientEntityName || "",
        },
      },
      sectionGroup,
      allSectionGroup: cloneDeep(sectionGroup),
      loading: false
    }), () => {
      this.fetchContactsAndAssignee(this.props.user.entityId, "assignee")
      if (activity.actIssuerClient) this.fetchContactsAndAssignee(activity.actIssuerClient || "", "contacts")
    })
  }

  onTaskSearch = (searchText) => {
    const queryString = qs.parse(this.props.history.location.search)
    const {id} = queryString
    const allSectionGroup = cloneDeep(this.state.allSectionGroup)
    this.setState({
      searchText: searchText || ""
    }, async () => {
      if (!this.props.nav2) {
        const tasks = await searchTaskByTranEntity(searchText || "")
        if(id && (tasks && tasks.tasks && tasks.tasks.length)){
          const taskDetails = tasks.tasks.find(f => f._id === id) || {}
          this.onEditSection(taskDetails)
        }
        this.setState({
          sectionGroup: tasks.taskGroupBy || {},
          addSection: tasks.taskDetails || {},
          loading: false
        })
      } else {
        const activityKey = allSectionGroup && Object.keys(allSectionGroup).length && Object.keys(allSectionGroup)[0]
        let tasks = activityKey ? allSectionGroup[activityKey] : []
        if (tasks) {
          tasks = tasks.filter(obj => ["taskDetails"].some(key => obj[key].taskNotes.toLowerCase().includes(searchText.toLowerCase())))
        }
        allSectionGroup[activityKey] = tasks || []
        this.setState({
          sectionGroup: allSectionGroup || {},
          loading: false
        })
      }
    })
  }

  onChangeItem = (item, category) => {
    this.setState({
      [category]: item,
    })
  }

  onSave = () => {
    const { user } = this.props
    const { taskDetails, activity } = this.state
    if (!taskDetails._id) {
      taskDetails.taskCreatedBy = {
        userId: user.userId,
        userFirstName: user.userFirstName,
        userLastName: user.userLastName,
        userEntityId: user.entityId,
        userEntityName: user.firmName
      }
      taskDetails.relatedActivityDetails = {
        activityId: activity._id || activity.activityId || "",
        activityType: activity.actType || activity.activityType || "",
        activitySubType: activity.actSubType || activity.activitySubType || "",
        activityProjectName: activity.actProjectName || activity.activityProjectName || "",
        activityIssuerClientId: activity.actIssuerClient || activity.activityIssuerClientId || "",
        activityIssuerClientName: activity.actIssuerClientEntityName || activity.activityIssuerClientName || "",
      }
    }

    taskDetails.taskHistory = {
      userId: user.userId,
      userFirstName: user.userFirstName,
      userLastName: user.userLastName,
      taskNotes: taskDetails.taskDetails.taskNotes,
      taskDescription: taskDetails.taskDetails.taskDescription
    }
    const errors = BusinessDevelopmentTaskValidation(taskDetails, taskDetails.createdAt || "")

    if (errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.forEach(err => {
        errorMessages[err.context.key] = "Required" || err.message
      })
      console.log(errorMessages)
      return this.setState({ errorMessages })
    }

    this.setState({
      errorMessages: {},
      isSaveDisabled: true,
      isNewTask:false,
      loading: true
    }, async () => {
      const res = await postBussinessActivityDetails(taskDetails)
      if (res.status === 200) {
        toast("Task Updated Successfully!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.setState({
          taskDetails: {
            ...cloneDeep(CONST.activityTask),
            relatedEntityDetails: {
              entityId: activity.actIssuerClient || activity.activityIssuerClientId || "",
              entityName: activity.actIssuerClientEntityName || activity.activityIssuerClientName || "",
            }
          },
          sectionGroup: (res.data && res.data.taskGroupBy) || {},
          allSectionGroup: (res.data && cloneDeep(res.data.taskGroupBy)) || {},
          isSaveDisabled: false,
          documentsUpload: false,
          disabled: false,
          loading: false,
          taskId: ""
        }, () => {
          if (!this.props.nav2) {
            this.setState({
              loading: true
            }, () => this.onTaskSearch(this.state.searchText))
          }
        })
      } else {
        toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        this.setState({
          isSaveDisabled: false,
          loading: false
        })
      }
    })
  }

  onCancel = () => {
    let state = {}
    const { activity } = this.state
    if (!this.props.nav2) {
      state = {
        activity: {}
      }
    }
    this.setState({
      taskDetails: {
        ...cloneDeep(CONST.activityTask),
        relatedEntityDetails: {
          entityId: activity.actIssuerClient || "",
          entityName: activity.actIssuerClientEntityName || "",
        }
      },
      documentsUpload: false,
      errorMessages: {},
      taskId: "",
      isNewTask:false,
      ...state
    })
  }

  onEditSection = async (section) => {
    let state = {}
    section.relatedContacts.forEach((item) => {
      item.id = item.userId
      item.name = `${item.userName}`
    })
    section.taskHistory.forEach(history => {
      history.timeStamp = history.createdAt ? moment(history.createdAt).unix() : 0
    })
    this.fetchContactsAndAssignee(section.relatedEntityDetails.entityId, "contacts")

    if (!this.props.nav2) {
      state = {
        activity: section.relatedActivityDetails
      }
    }
    const disabled = section && section.taskDetails && section.taskDetails.taskStatus === "Closed"
    this.setState({
      taskDetails: section || {},
      taskId: section._id,
      errorMessages: {},
      isNewTask:true,
      disabled,
      ...state
    })
  }

  onHandleUploadDocuments = () => {
    this.setState(prevState => ({
      documentsUpload: !prevState.documentsUpload
    }))
  }

  onStatusChange = async (e, doc, callback) => {
    const { taskId } = this.state
    putTaskDocumentTransactionStatus(taskId, { _id: doc._id, [e.target.name]: e.target.value }, (res) => {
      if (res && res.status === 200) {
        toast("Task Document status has been updated!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        callback({
          status: true,
          documentsList: (res.data && res.data.taskRelatedDocuments) || [],
        })
      } else {
        toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onDocSave = (docs, callback) => {
    putTaskDocumentTransaction(docs, (res) => {
      if (res && res.status === 200) {
        toast("Task Documents has been updated!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        callback({
          status: true,
          documentsList: (res.data && res.data.taskRelatedDocuments) || [],
        })
      } else {
        toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onDeleteDoc = (documentId, callback) => {
    const { taskId } = this.state
    delTransactionDocStatuse(taskId, documentId, (res) => {
      if (res && res.status === 200) {
        toast("Task Documents removed successfully", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        callback({
          status: true,
          documentsList: (res.data && res.data.taskRelatedDocuments) || [],
        })
      } else {
        toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  fetchContactsAndAssignee = (entityId, name) => {
    fetchAssigned(entityId, (res) => {
      this.setState(prevState => ({
        dropDown: {
          ...prevState.dropDown,
          [name]: res.usersList || [],
        },
        loading: false
      }))
    })
  }

  addNewTask = (activity) => {
    if (/* !this.props.nav2 && */activity) {
      this.setState(prevState => ({
        activity,
        isNewTask:true,
        disabled: false,
        taskDetails: {
          ...cloneDeep(CONST.activityTask),
          relatedEntityDetails: {
            entityId: activity.activityIssuerClientId || "",
            entityName: activity.activityIssuerClientName || "",
          }
        },
        dropDown: {
          ...prevState.dropDown,
          contacts: [],
        },
        loading: true
      }), () => this.fetchContactsAndAssignee(activity.activityIssuerClientId || "", "contacts"))
    }
  }

  render() {
    const { dropDown, documentsUpload, searchText, activityDetails, errorMessages, isSaveDisabled, taskDetails, sectionGroup, addSection, activity, isNewTask, disabled } = this.state
    const { user, nav2 } = this.props
    const taskHistory = taskDetails ? taskDetails.taskHistory : null
    if (taskDetails._id && Array.isArray(taskHistory)) {
      taskHistory.sort((a, b) => b.timeStamp - a.timeStamp)
    }

    const tags = {
      clientId: activity.actIssuerClient || activity.activityIssuerClientId || "",
      clientName: activity.actIssuerClientEntityName || activity.activityIssuerClientName || "",
      tenantId: activity.actTranFirmId || "",
      tenantName: activity.actSubType || activity.activitySubType || "",
      contextName: activity.actProjectName || activity.activityProjectName || ""
    }
    const loading = () => <Loader />
    if (this.state.loading) {
      return loading()
    }
    return (
      <div>
        <Nav />
        <div id="main">
          <SearchActivity searchText={searchText} nav2={nav2} onSearch={(e) => this.onTaskSearch(e.target.value)} />
          <br />
          <div className="columns">
            <div className="column">
              <div className="box">
                <div style={{textAlign:"center"}}>
                  <b className="is-size-4">Business Development Activity by Client/Prospect</b>
                  <p className="is-size-12">
                    <small>Click on a task below to view related activity summary on the right.</small>
                  </p>
                </div>
                {
                  sectionGroup && Object.keys(sectionGroup || {}).map((p, index) => (
                    <ActiityList key={index.toString()} nav2={nav2} item={(sectionGroup && sectionGroup[p]) || {}} addSection={(addSection && addSection[p]) || {}}
                      header={p} index={index} addNewTask={this.addNewTask} activityDetails={activityDetails}
                      onEditSection={this.onEditSection}/>
                  ))
                }
              </div>
            </div>
            {
              isNewTask && <div className="column">
                <TaskActivity user={user} dropDown={dropDown || {}} category="taskDetails" errors={errorMessages || {}} onChangeItem={this.onChangeItem} item={taskDetails || {}} onDeleteDoc={this.onDeleteDoc}
                  onStatusChange={this.onStatusChange} tags={tags} onSave={this.onSave} onCancel={this.onCancel} taskHistory={taskHistory} onUploadDocuments={this.onHandleUploadDocuments}
                  fetchContactsAndAssignee={this.fetchContactsAndAssignee} isSaveDisabled={isSaveDisabled} disabled={disabled} onDocSave={this.onDocSave} activity={activity} documentsUpload={documentsUpload} />
              </div>
            }
          </div>
        </div>
      </div>

    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

export default withRouter(connect(mapStateToProps, null)(BusinessDevelopment))
