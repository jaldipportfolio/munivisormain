import Joi from "joi-browser"

const taskSchema = (minDate) => Joi.object().keys({
  taskDetails: Joi.object({
    taskAssigneeUserId: Joi.string().required(),
    taskAssigneeName: Joi.string().required(),
    taskNotes: Joi.string().required(),
    taskAssigneeType: Joi.string().allow("").optional(),
    taskDescription: Joi.string().allow("").optional(),
    taskStartDate: Joi.date().example(new Date("2016-01-01"))/* .min(dateFormat(minDate || new Date(), "yyyy-mm-dd")) */.required().optional(),
    taskEndDate: Joi.date().example(new Date("2016-01-01")).min(Joi.ref("taskStartDate")).required().optional(),
    taskStatus: Joi.string().required(),
    taskType: Joi.string().required(),
    taskFollowing: Joi.boolean().optional(),
    taskUnread: Joi.boolean().optional(),
  }),
  relatedActivityDetails: Joi.object({
    activityId: Joi.string().required(),
    activityType: Joi.string().required(),
    activitySubType: Joi.string().required(),
    activityProjectName: Joi.string().required(),
    activityIssuerClientId: Joi.string().required(),
    activityIssuerClientName: Joi.string().required()
  }),
  taskHistory: Joi.object({
    userId: Joi.string().required(),
    userFirstName: Joi.string().required(),
    userLastName: Joi.string().required(),
    taskNotes: Joi.string().required(),
    taskDescription: Joi.string().optional()
  }),
  taskAssigneeUserDetails: Joi.object({
    userId: Joi.string().required(),
    userFirstName: Joi.string().required(),
    userLastName: Joi.string().required(),
    userEntityId: Joi.string().required(),
    userEntityName: Joi.string().required(),
  }),
  relatedEntityDetails: Joi.object({
    entityId: Joi.string().required(),
    entityName: Joi.string().required()
  }),
  taskCreatedBy: Joi.object({
    userId: Joi.string().required(),
    userFirstName: Joi.string().required(),
    userLastName: Joi.string().required(),
    userEntityId: Joi.string().required(),
    userEntityName: Joi.string().required()
  }),
  relatedContacts: Joi.array().required(),
  updatedAt: Joi.string().optional(),
  createdAt: Joi.string().optional(),
  taskRelatedDocuments: Joi.array().optional(),
  __v: Joi.number().optional(),
  _id: Joi.string().optional(),
})

export const BusinessDevelopmentTaskValidation = (inputTransDistribute, minDate) => Joi.validate(inputTransDistribute, taskSchema(minDate), { abortEarly: false, stripUnknown:false })

