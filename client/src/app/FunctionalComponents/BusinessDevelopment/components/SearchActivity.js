import React from "react"

const SearchActivity = ({searchText, nav2, onSearch}) => (
  <div className="columns">
    {
      !nav2 ?
        <div className="column is-two-thirds">
          <p className="control has-icons-left">
            <input className="input is-small is-link" type="text" placeholder="search" value={searchText} onChange={onSearch}/>
            <span className="icon is-left has-background-dark">
              <i className="fas fa-search" />
            </span>
          </p>
        </div> : null
    }

    <div className="column">
      <a href="/createTran/businessDevelopment" target="new">
        <button className="button is-link is-small">Add New Activity</button>
      </a>
    </div>
  </div>
)

export default SearchActivity
