import React from "react"
import Autosuggest from "react-autosuggest"
import "./autosuggest.css"
import { throttle, debounce } from "throttle-debounce"
/* ---------- */
/*    Data    */
/* ---------- */

const languages = [
  {
    name: "C",
    year: 1972
  },
  {
    name: "C#",
    year: 2000
  },
  {
    name: "C++",
    year: 1983
  },
  {
    name: "Ford & Associates Pvt Ltd",
    year: 1983
  },
  {
    name: "Clojure",
    year: 2007
  },
  {
    name: "Elm",
    year: 2012
  },
  {
    name: "Go",
    year: 2009
  },
  {
    name: "Haskell",
    year: 1990
  },
  {
    name: "Java",
    year: 1995
  },
  {
    name: "Javascript",
    year: 1995
  },
  {
    name: "Perl",
    year: 1987
  },
  {
    name: "PHP",
    year: 1995
  },
  {
    name: "Python",
    year: 1991
  },
  {
    name: "Ruby",
    year: 1995
  },
  {
    name: "Scala",
    year: 2003
  }
]
function escapeRegexCharacters(str) {
  return str.replace(/[.*+?^${}()|[\]\\]/g, "\\$&")
}

function getSuggestionValue(suggestion) {
  console.log("=======>>>",suggestion)
  return suggestion
}

function renderSuggestion(suggestion) {
  return (
    <span>{suggestion}</span>
  )
}
const renderInputComponent = inputProps => (
  <input
    {...inputProps}
    className="input is-small is-link"
  />
)

class AutoSuggest extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      value: "",
      suggestions: [],
      isLoading: false,
      noSuggestions: false
    }
    this.lastRequestId = null
    this.autocompleteSearchDebounced = debounce(500, this.loadSuggestions)
    this.autocompleteSearchThrottled = throttle(200, this.loadSuggestions)
  }

  onChange = (event, { newValue }) => {
    this.setState({ value: newValue}, () => {
      const {value} = this.state
      if (value.length < 5 || value.endsWith(" ")) {
        this.autocompleteSearchThrottled(this.state.value.trim())
      } else {
        this.autocompleteSearchDebounced(this.state.value.trim())
      }
    })
  }

  onSuggestionsFetchRequested = ({ value }) => {
    this.autocompleteSearchDebounced(value)
  }

  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: []
    })
  }

  getMatchingLanguages=(value)=>{
    const escapedValue = escapeRegexCharacters(value.trim())
    if (escapedValue === "") {
      return []
    }
    const regex = new RegExp(`^${  escapedValue}`, "i")
    return this.props.firmList.filter(
      firm =>
        firm.toLowerCase().indexOf(escapedValue.toLowerCase()) > -1
    )
  }

  loadSuggestions(value) {
    // Cancel the previous request
    if (this.lastRequestId !== null) {
      clearTimeout(this.lastRequestId)
    }

    this.setState({
      isLoading: true
    })

    // Fake request
    const suggestions = this.getMatchingLanguages(value)
    const isInputBlank = value.trim() === ""
    const noSuggestions = !isInputBlank && suggestions.length===0
    this.lastRequestId = setTimeout(() => {
      this.setState({
        isLoading: false,
        suggestions,
        noSuggestions
      })
    }, 1000)
  }
  render() {
    const { value, suggestions, isLoading,noSuggestions } = this.state
    const inputProps = {
      placeholder: "Type ex : 'a'",
      value,
      onChange: this.onChange
    }
    const status = (isLoading ? "Loading..." : "Type to load suggestions")

    return (
      <div className="control">
        <Autosuggest
          suggestions={suggestions}
          onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
          onSuggestionsClearRequested={this.onSuggestionsClearRequested}
          getSuggestionValue={getSuggestionValue}
          renderSuggestion={renderSuggestion}
          renderInputComponent={renderInputComponent}
          inputProps={inputProps} />
        {
          noSuggestions &&
            <div className="react-autosuggest__suggestions-container--open">
              <p>No result found</p>
            </div>
        }
      </div>
    )
  }
}
export default AutoSuggest