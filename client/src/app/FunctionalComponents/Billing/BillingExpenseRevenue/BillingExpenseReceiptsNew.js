import React from "react"
import {withRouter} from "react-router-dom"
import {connect} from "react-redux"
import {toast} from "react-toastify"
import isEmpty from "lodash/isEmpty"
import cloneDeep from "lodash.clonedeep"
import DropdownList from "react-widgets/lib/DropdownList"

import CONST, {ContextType,muniApiBaseURL} from "../../../../globalutilities/consts"
import Loader from "../../../GlobalComponents/Loader"
import withAuditLogs from "../../../GlobalComponents/withAuditLogs"
import  DocumentPage from "../../../GlobalComponents/DocumentPage"
import axios from "axios"
// Move this to the helpers
import SingleAccordion from "./../CommonComponents/SingleAccordion"

export const managebillingexpensesstate = async( payload, callback, token) => {
  const response = await axios.post(`${muniApiBaseURL}billing/managebillingexpensesstate`,payload, {headers:{"Authorization":token}})
  console.log("there is data", response && response.data)
  callback(response.data.newstatedetails)
}

export const putBillingReceiptsForUsers = async(invoiceId,userId, receiptDocuments,token) => {
  try {
    const response = await axios.post(`${muniApiBaseURL}billing/${invoiceId}/expensereceipt/${userId}`, {receiptDocuments}, {headers:{"Authorization":token}})
    return response
  }
  catch (e) {
    console.log(e)
  }
}

export const pullBillingReceiptForUser = async(invoiceId, userId, documentId, token) => {
  try {
    const response = await axios.delete(`${muniApiBaseURL}billing/${invoiceId}/expensereceipt/${userId}?docId=${documentId}`, {headers:{"Authorization":token}})
    return response
  } catch (error) {
    console.log(e)
  }
}


class BillingExpenseReceipts extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: true
    }
  }

  async componentDidMount(){
    // Get the initial state of the Billing Module
    let {selectedInvoice} = this.props
    selectedInvoice = selectedInvoice || {"selectedClient":{},
      "selectedActivity": {},
      "selectedInvoice": {},
      "selectedUser": {}
    }
    await managebillingexpensesstate(
      {stateObj:selectedInvoice},
      ({selectedData, dropDownValues}) => {
        this.setState({ selectedData, dropDownValues, loading:false})
      },
      this.props.token )
  }



  onDocSave = async (docs, callback) => {
    const { selectedData } = this.state
    const selectedInvoice = (selectedData && selectedData.selectedInvoice) || {}
    const selectedUser = (selectedData && selectedData.selectedUser) || {}

    this.setState({loading:true})
    const res = await putBillingReceiptsForUsers(selectedInvoice.invoiceId, selectedUser.userId, docs, this.props.token)
    if (res && res.status === 200) {
      toast("Document has been updated!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
      this.props.submitAuditLogs(selectedInvoice.invoiceId)
      const documentList = (res.data && res.data.billingReceipts) || []
      this.setState( (ps) => ({...ps,loading:false,selectedData:{...ps.selectedData,billingReceipts:[...documentList]}}),
        () => {
          callback({
            status: true,
            documentsList: documentList,
          })
        })
    } else {
      toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
      callback({
        status: false
      })
    }
  }

  onDeleteDoc = async (documentId, callback) => {
    const { selectedData } = this.state
    const selectedInvoice = (selectedData && selectedData.selectedInvoice) || {}
    const selectedUser = (selectedData && selectedData.selectedUser) || {}

    const res = await pullBillingReceiptForUser(selectedInvoice.invoiceId, selectedUser.userId, documentId,this.props.token)
    if (res && res.status === 200) {
      toast("Receipt removed successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
      this.props.submitAuditLogs(selectedInvoice.invoiceId)
      const documentList = (res.data && res.data.billingReceipts) || []
      this.setState( (ps) => ({...ps,loading:false,selectedData:{...ps.selectedData,billingReceipts:[...documentList]}}),
        () => {
          callback({
            status: true,
            documentsList: documentList,
          })
        })
    } else {
      toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
      callback({
        status: false
      })
    }
  }

  onStatusChange = async (e, doc, callback) => {
    console.log(e, doc)
    callback({
      status: true,
    })
  }

  handleSelectionChange = async (field, val, depFields) => {
    const selectedData = cloneDeep(this.state.selectedData || {})
    const newSelectedData = { ...selectedData, ...depFields, [field]:val}
    const {selectedUserExpenses,...stateObj} = newSelectedData
    this.setState({loading:true})
    await managebillingexpensesstate(
      {stateObj},
      ({selectedData, dropDownValues}) => {
        console.log("WHAT IS BEING RETURNED", selectedData, dropDownValues)
        this.setState({ selectedData, dropDownValues, loading:false})
      },
      this.props.token )
  }


  renderSummaryData =()=> {
    const { selectedData, dropDownValues } = this.state
    const selectedClient = (selectedData && selectedData.selectedClient) || {}
    const selectedActivity = (selectedData && selectedData.selectedActivity) || {}
    const selectedInvoice = (selectedData && selectedData.selectedInvoice) || {}
    const selectedUser = (selectedData && selectedData.selectedUser) || {}
    // Get the users who are part of the transaction

    const clientDetails = (dropDownValues && dropDownValues.clientDetails) || []
    const activityDetails = (dropDownValues && dropDownValues.activityDetails) || []
    const invoiceDetails = (dropDownValues && dropDownValues.invoiceDetails) || []
    const userDetails = (dropDownValues && dropDownValues.userDetails) || []

    return ( <div className="columns">
      <div className="column">
        <div className="Select Firm User select-hack">
          <p className="multiExpLbl ">Select Issuer</p>
          <DropdownList filter="contains"
            data={(clientDetails)}
            value={selectedClient || {}}
            valueField={( (a) => a )}
            textField={({activityClientName}) => activityClientName}
            onChange={(val) => {
              console.log("ON CHANGE TRANSACTION SELECTION", val)
              this.handleSelectionChange("selectedClient", val, {selectedActivity:"", selectedInvoice:"", selectedUser:""})
            }}
          />
        </div>
      </div>

      <div className="column">
        <div className="Select Firm User select-hack">
          <p className="multiExpLbl ">Transaction / Issue Name</p>
          <DropdownList filter="contains"
            data={(activityDetails)}
            value={selectedActivity || {}}
            disabled={isEmpty((activityDetails))}
            valueField={( (a) => a )}
            textField={({activityDescription}) => activityDescription}
            groupBy={({activityTranType,activityTranSubType}) => `${activityTranType} - ${activityTranSubType}`}
            onChange={(val) => {
              console.log("ON CHANGE TRANSACTION SELECTION", val)
              this.handleSelectionChange("selectedActivity", val, {selectedInvoice:"", selectedUser:""})
            }}
          />
        </div>
      </div>
      <div className="column">
        <div className="Select Firm User select-hack">
          <p className="multiExpLbl ">Invoice Number</p>
          <DropdownList filter="contains"
            data={(invoiceDetails)}
            disabled={isEmpty((invoiceDetails))}
            value={selectedInvoice || {}}
            valueField={( (a) => a )}
            textField={({invoiceNumber}) => invoiceNumber}
            onChange={(val) => {
              console.log("ON CHANGE TRANSACTION SELECTION", val)
              this.handleSelectionChange("selectedInvoice", val, {selectedUser:""})
            }}
          />
        </div>
      </div>
      <div className="column">
        <div className="Select Firm User select-hack">
          <p className="multiExpLbl ">Select Firm User</p>
          <DropdownList filter="contains"
            data={userDetails}
            disabled={isEmpty((userDetails))}
            value={selectedUser || {}}
            valueField={({userId}) => userId }
            textField={({userFirstName, userLastName}) => userFirstName ? `${userLastName}-${userFirstName}`:null}
            onChange={(val) => {
              console.log("ON CHANGE INVOICE SELECTION", val)
              this.handleSelectionChange("selectedUser", val, {})
            }}
          />
        </div>
      </div>

    </div>
    )
  }

  render() {
    const { selectedData,dropDownValues } = this.state
    const selectedActivity = (selectedData && selectedData.selectedActivity) || {}

    const selectedUser = (selectedData && selectedData.selectedUser) || {}
    const selectedInvoice = (selectedData && selectedData.selectedInvoice) || {}
    const billingReceipts = (selectedData && selectedData.billingReceipts) || []

    let  showReceiptEnrySection = false
    if(!isEmpty(selectedUser) && !isEmpty(selectedInvoice)) {
      showReceiptEnrySection = true
    }

    const loading = (
      <Loader/>
    )

    if (this.state.loading) {
      return loading
    }

    return <div className="dealSoE" >
      <section className="accordions">
        <SingleAccordion
          accIndex="summary"
          accOpen
          accHeader="Select Invoice and Other Details"
          toggleAccordion={this.toggleAccordion}
          accordionButtons={null}
        >
          {this.renderSummaryData()}
        </SingleAccordion>
        {
          showReceiptEnrySection ? ( <div className="bank-loan-documents">
            <DocumentPage {...this.props}
              isNotTransaction
              onSave={this.onDocSave}
              tranId={selectedInvoice.invoiceId}
              title="Receipts Documents"
              pickCategory="LKUPDOCCATS"
              pickSubCategory="LKUPCORRESPONDENCEDOCS"
              pickAction="LKUPDOCACTION"
              documents={billingReceipts}
              contextType={ContextType.billing.receipts}
              onStatusChange={this.onStatusChange}
              onDeleteDoc={this.onDeleteDoc}/>
          </div>) : !isEmpty(selectedActivity) && isEmpty(dropDownValues.invoiceDetails) ?
            <div><h1>No Invoice has been created for the Selected Transaction.Please submit expenses first before uploading receipts</h1></div>
            : null
        }
        {/* <pre>
          {JSON.stringify(this.state,null,2)}
        </pre> */}
      </section>
    </div>
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
  token:state.auth.token
})

const WrappedComponent = withAuditLogs(BillingExpenseReceipts)
export default withRouter(connect(mapStateToProps, null)(WrappedComponent))
