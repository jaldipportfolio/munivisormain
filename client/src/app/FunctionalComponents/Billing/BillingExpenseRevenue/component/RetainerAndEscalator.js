import React from "react"
import {NumberInput, SelectLabelInput, TextLabelInput} from "../../../../GlobalComponents/TextViewBox"

const RetainerAndEscalator = ({category, item, types, scheduleIndex, retAndEscDateValid, error, index, onChangeItem, onRemove, dateOverlap, tabIndex}) => {
  const onChange = (event) => {
    if(event.target.name === "effStartDate" || event.target.name === "effEndDate"){
      const effStartDate = event.target.name === "effStartDate" ? event.target.value : item.effStartDate
      const effEndDate =  event.target.name === "effEndDate" ? event.target.value : item.effEndDate
      retAndEscDateValid(item.type, effStartDate, effEndDate, index)
      /* if(!status){
        return
      } */
    }
    onChangeItem({
      ...item,
      [event.target.name]: event.target.value
    }, category, index)
  }

  const onDeleteBtnClick = (e) =>{
    if(e.keyCode === 13){
      onRemove(category, index)
    }
  }

  return(
    <tr key={scheduleIndex}>
      <td>
        <SelectLabelInput tabIndex={tabIndex} error={error.type || ""} list={types || []} name="type" value={item.type} onChange={onChange}/>
      </td>
      <td>
        <NumberInput tabIndex={tabIndex + 1} prefix="$" error={error.hoursCovered || ""} name="hoursCovered" placeholder="$1.00" value={item.hoursCovered} onChange={onChange} />
      </td>
      <td>
        <TextLabelInput tabIndex={tabIndex + 2} error={dateOverlap ? error.effStartDate ? "Select non overlapping periods" : "" : error.effStartDate || ""} name="effStartDate" type="date"
          /* value={item.effStartDate ? moment(item.effStartDate).format("YYYY-MM-DD") : ""} */ value={(item.effStartDate === "" || !item.effStartDate) ? null : new Date(item.effStartDate)} onChange={onChange} disabled={!item.type}/>
      </td>
      <td>
        <TextLabelInput tabIndex={tabIndex + 3} error={dateOverlap ? error.effEndDate ? "Select non overlapping periods" : "" : error.effEndDate || ""} name="effEndDate" type="date"
          /* value={item.effEndDate ? moment(item.effEndDate).format("YYYY-MM-DD") : ""} */ value={(item.effEndDate === "" || !item.effEndDate) ? null : new Date(item.effEndDate)} onChange={onChange} disabled={!item.type}/>
      </td>
      <td>
        <a onKeyDown={onDeleteBtnClick} tabIndex={tabIndex + 4} className="has-text-link"  style={{cursor: "pointer"}} onClick={() => onRemove(category, index)}>
          <i className="far fa-trash-alt" />
        </a>
      </td>
    </tr>
  )
}
export default RetainerAndEscalator
