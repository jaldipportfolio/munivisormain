import React from "react"
import {NumberInput, SelectLabelInput} from "../../../../GlobalComponents/TextViewBox"

const TransactionalFees = ({category, item, roles, scheduleIndex, error, index, onChangeItem, onRemove, tabIndex}) => {
  const onChange = (event) => {
    onChangeItem({
      ...item,
      [event.target.name]: event.target.value
    }, category, index)
  }

  const onDeleteBtnClick = (e) =>{
    if(e.keyCode === 13){
      onRemove(category, index)
    }
  }

  return(
    <tr key={scheduleIndex}>
      <td>
        <SelectLabelInput tabIndex={tabIndex} error={error.role || ""} list={roles || []} name="role" value={item.role} onChange={onChange}/>
      </td>
      <td>
        <NumberInput tabIndex={tabIndex + 1} prefix="$" error={error.stdHourlyRate || ""} name="stdHourlyRate" placeholder="$1000" value={item.stdHourlyRate || ""} onChange={onChange}/>
      </td>
      <td>
        <NumberInput tabIndex={tabIndex + 2} prefix="$" error={error.quoatedRate || ""} name="quoatedRate" placeholder="$1000" value={item.quoatedRate || ""} onChange={onChange}/>
      </td>
      <td>
        <a tabIndex={tabIndex + 3} onKeyDown={onDeleteBtnClick} className="has-text-link"  style={{cursor: "pointer"}} onClick={() => onRemove(category, index)}>
          <i className="far fa-trash-alt" />
        </a>
      </td>
    </tr>
  )
}
export default TransactionalFees
