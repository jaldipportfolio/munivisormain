import React from "react"
import { Modal } from "../../../FunctionalComponents/TaskManagement/components/Modal"
import DragAndDropFile from "../../../GlobalComponents/DragAndDropFile"

export const ReceiptsModal  = (props) => {
  const toggleModal = () => {
    props.onModalChange({ receiptsModal: !props.state.receiptsModal })
  }

  const onMultipleFileUpload = (uploadedFiles) => {
    console.log(uploadedFiles)
    props.onModalChange({ uploadedFiles }, "upload")
  }

  return(
    <Modal
      closeModal={toggleModal}
      modalState={props.state.receiptsModal}
      title="Add Receipts"
    >
      <div>
        <DragAndDropFile bucketName={props.state.bucketName} contextId={(props.state.selectedTran && props.state.selectedTran.activityId) || ""}
          contextType={props.state.contextType || ""}
          tenantId={props.user.entityId}
          onMultipleFileUpload={onMultipleFileUpload}
        />
      </div>
    </Modal>
  )
}

export default ReceiptsModal
