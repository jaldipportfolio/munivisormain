import React from "react"

const SingleAccordion = (props) => {
  const {
    accIndex,
    accOpen,
    accHeader,
    toggleAccordion,
    accordionButtons,
    children
  } = props
  return (
    <article
      key={accIndex}
      className={accOpen
        ? "accordion is-active"
        : "accordion"}>
      <div className="accordion-header">
        <p
          onClick={() => toggleAccordion(accIndex)}
          style={{
            width: "100%"
          }}>{accHeader}</p>
        {accordionButtons || null}
      </div>
      {accOpen && <div className="accordion-body">
        <div className="accordion-content">
          <div>
            {children}
          </div>
        </div>
      </div>}
    </article>
  )
}

export default SingleAccordion