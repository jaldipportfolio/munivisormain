import React from "react";
import cloneDeep from "lodash.clonedeep"
import { Modal } from "../EntityManagement/CommonComponents/EntityPageGrid";
import {
fetchMasterListSearchPrefs,
saveMasterListSearchPrefs,
updateGridTableData
} from "../../StateManagement/actions/AdminManagement";
import { toast } from "react-toastify";
import TableHeader from "../../GlobalComponents/TableHeader";

const defaultFieldList = [
  { fieldName: "tranClientName", isVisible: true },
  { fieldName: "activityDescription", isVisible: true },
  { fieldName: "tranType", isVisible: true },
  { fieldName: "tranParAmount", isVisible: true },
  { fieldName: "tranAssigneeDetails.userFullName", isVisible: true },
  { fieldName: "middleName", isVisible: true },
  { fieldName: "tranPrimarySector", isVisible: true },
  { fieldName: "tranPricingDate", isVisible: true },
  { fieldName: "tranExpectedAwardDate", isVisible: true },
  { fieldName: "tranStatus", isVisible: true }]

class GridProjectDashboardModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      gridTableColumn: [],
      gridTableId: ""
    };
  }

  componentWillMount(){
    this.setState({
      gridTableColumn: cloneDeep(defaultFieldList)
    },()=>{
      this.getGridTableData()
    })
  }

  getGridTableData = async () =>{
    const res = await fetchMasterListSearchPrefs("transactionsProjectDash")
    if(res && res.data && res.data.length){
      this.setState({
        gridTableColumn: (res.data[0] && res.data[0].tableGridLists),
        gridTableId: (res.data[0] && res.data[0]._id) || "",
      },()=>{
        this.props.getProjectDashGridTableData(this.state.gridTableColumn)
      })
    }else {
      this.props.getProjectDashGridTableData(this.state.gridTableColumn)
    }
  }

  handleSubmit = async () => {
    const {gridTableColumn, gridTableId} = this.state
    if(gridTableId){
      const res = await updateGridTableData("transactionsProjectDash", gridTableColumn);
      if(res && res.data && res.data.length){
        this.setState({
          gridTableColumn: (res.data[0] && res.data[0].tableGridLists),
          gridTableId: (res.data[0] && res.data[0]._id) || "",
        },()=>{
          this.props.getProjectDashGridTableData(this.state.gridTableColumn)
          this.props.onSettingModal()
          toast("Grid Setting update successfully!", {
            autoClose: 2000,
            type: toast.TYPE.SUCCESS
          })
        })
      }
    }else {
      const res = await saveMasterListSearchPrefs("transactionsProjectDash", "", "", gridTableColumn);
      if(res.status === 200){
        this.getGridTableData()
        this.props.onSettingModal()
        toast("Grid Setting save successfully!", {
          autoClose: 2000,
          type: toast.TYPE.SUCCESS
        })
      }
    }
  };

  onReset = () =>{
    this.setState({
      gridTableColumn: cloneDeep(defaultFieldList),
      isReset: true
    })
  }

  handleUpDown = (key, i) =>{
    const {gridTableColumn} = this.state
    let reserve = {}
    if(key === "up"){
      reserve = gridTableColumn[i-1]
      gridTableColumn[i-1] = gridTableColumn[i]
      gridTableColumn[i] = reserve
    }else if(key === "down"){
      reserve = gridTableColumn[i+1]
      gridTableColumn[i+1] = gridTableColumn[i]
      gridTableColumn[i] = reserve
    }
    this.setState({ gridTableColumn: cloneDeep(gridTableColumn) })
  }

  handleOnChange = (e, i) =>{
    const {gridTableColumn} = this.state
    gridTableColumn[i].isVisible = e.target.checked
    this.setState({ gridTableColumn: cloneDeep(gridTableColumn) })
  }

  render() {
    const { isModalState } = this.props;
    const { gridTableColumn } = this.state;
    const isActive = gridTableColumn.filter(p => p.isVisible)
    return (
      <Modal
        closeModal={this.props.onSettingModal}
        modalState={isModalState}
        saveModal={(e) => (e)}
        title="Grid Configuration">
        <React.Fragment>
          <table className="table is-bordered is-striped is-hoverable is-fullwidth">
            <TableHeader cols={[{name: "Field"}, {name: "Visible"}, {name: "Action"}]} />
            <tbody>
            {gridTableColumn.map((el, i) =>{
              return(
                <tr key={i}>
                  <td>
                    { el.fieldName === "tranClientName" ? "Client Name" : null }
                    { el.fieldName === "tranType" ? "Type" : null }
                    { el.fieldName === "activityDescription" ? "Description" : null }
                    { el.fieldName === "tranParAmount" ? "Principal Amount($)" : null }
                    { el.fieldName === "tranAssigneeDetails.userFullName" ? "Lead Manager" : null }
                    { el.fieldName === "middleName" ? "Coupon Type" : null }
                    { el.fieldName === "tranPrimarySector" ? "Purpose/Sector" : null }
                    { el.fieldName === "tranPricingDate" ? "Expected Pricing Date" : null }
                    { el.fieldName === "tranExpectedAwardDate" ? "Expected Closing Date" : null }
                    { el.fieldName === "tranStatus" ? "Status" : null }
                  </td>
                  <td>
                    <input
                      name="isVisible"
                      type="checkbox"
                      checked={el.isVisible}
                      onChange={(e) => this.handleOnChange(e, i)}/>
                  </td>
                  <td>
                    <a className={i === 0 ? "isDisabled" : ""} onClick={()=> this.handleUpDown("up", i)}>
                      <span className="has-text-link">&nbsp;
                        <i className="fa fa-arrow-up" title="up"/>
                      </span>
                    </a>&nbsp;
                    <a className={i === 9 ? "isDisabled" : ""} onClick={()=> this.handleUpDown("down", i)}>
                      <span className="has-text-link">
                        <i className="fa fa-arrow-down" title="down"/>
                      </span>
                    </a>
                  </td>
                </tr>
              )
            })}
            </tbody>
          </table>
        </React.Fragment>
        <a className="button" onClick={this.onReset}>Reset</a>
        <div className="field is-grouped">
          <div className="control">
            <button className="button is-link is-small"
                    disabled={!isActive.length}
                    onClick={this.handleSubmit}>Save</button>
          </div>
          <div className="control">
            <button className="button is-light is-small" onClick={this.props.onSettingModal}>Cancel</button>
          </div>
        </div>
      </Modal>
    );
  }
}

export default GridProjectDashboardModal;
