import React from "react";
import cloneDeep from "lodash.clonedeep"
import { Modal } from "../EntityManagement/CommonComponents/EntityPageGrid";
import {
fetchMasterListSearchPrefs,
saveMasterListSearchPrefs,
updateGridTableData
} from "../../StateManagement/actions/AdminManagement";
import { toast } from "react-toastify";
import TableHeader from "../../GlobalComponents/TableHeader";

const defaultTaskFieldList = [
  { fieldName: "taskUnreadStatus", isVisible: true },
  { fieldName: "tranClientName", isVisible: true },
  { fieldName: "activityDescription", isVisible: true },
  { fieldName: "taskEndDate", isVisible: true },
  { fieldName: "tranType", isVisible: true },
  { fieldName: "taskDescription", isVisible: true },
  { fieldName: "taskAssigneeName", isVisible: true },
  { fieldName: "taskStatus", isVisible: true }
  ]

const defaultFieldComplianceList = [
  { fieldName: "taskUnreadStatus", isVisible: true },
  { fieldName: "taskCategory", isVisible: true },
  { fieldName: "taskDescription", isVisible: true },
  { fieldName: "taskEndDate", isVisible: true },
  { fieldName: "taskAssigneeName", isVisible: true },
  { fieldName: "taskStatus", isVisible: true }]

class GridTaskDashboardModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      gridTableColumn: [],
      taskType: "",
      gridTableId: "",
    };
  }
  componentWillMount(){
    if(this.props.taskType === "tasks"){
      this.setState({
        gridTableColumn: cloneDeep(defaultTaskFieldList) || [],
        gridTableId: ""
      },()=>{
        this.getGridTableData()
      })
    }else if(this.props.taskType === "compliance"){
      this.setState({
        gridTableColumn: cloneDeep(defaultFieldComplianceList) || [],
        gridTableId: ""
      },()=>{
        this.getGridTableData()
      })
    }
  }

  async componentWillReceiveProps(newProps) {
    if (this.props.taskType !== newProps.taskType) {
      if(newProps.taskType === "tasks"){
        this.setState({
          gridTableColumn: cloneDeep(defaultTaskFieldList) || [],
          gridTableId: "",
        },()=>{
          this.getGridTableData()
        })
      }else if(newProps.taskType === "compliance"){
        this.setState({
          gridTableColumn: cloneDeep(defaultFieldComplianceList),
          gridTableId: "",
        },()=>{
          this.getGridTableData()
        })
      }
    }
  }

  getGridTableData = async () =>{
    const res = await fetchMasterListSearchPrefs(this.props.taskType === "tasks" ? "tasksProjectDash" : "tasksComplianceDash")
    if(res && res.data && res.data.length){
      this.setState({
        gridTableColumn: (res.data[0] && res.data[0].tableGridLists),
        gridTableId: (res.data[0] && res.data[0]._id) || "",
      },()=>{
        this.props.getTaskDashboardGridData(this.state.gridTableColumn)
      })
    }else {
      this.props.getTaskDashboardGridData(this.state.gridTableColumn)
    }
  }

  handleSubmit = async () => {
    const {gridTableColumn, gridTableId} = this.state
    if(gridTableId){
      const res = await updateGridTableData(this.props.taskType === "tasks" ? "tasksProjectDash" : "tasksComplianceDash", gridTableColumn);
      if(res && res.data && res.data.length){
        this.setState({
          gridTableColumn: (res.data[0] && res.data[0].tableGridLists),
          gridTableId: (res.data[0] && res.data[0]._id) || ""
        },()=>{
          this.props.getTaskDashboardGridData(this.state.gridTableColumn)
          this.props.onSettingModal()
          toast("Grid Setting update successfully!", {
            autoClose: 2000,
            type: toast.TYPE.SUCCESS
          })
        })
      }
    }else {
      const res = await saveMasterListSearchPrefs(this.props.taskType === "tasks" ? "tasksProjectDash" : "tasksComplianceDash", "", "", gridTableColumn);
      if(res.status === 200){
        this.getGridTableData()
        this.props.onSettingModal()
        toast("Grid Setting save successfully!", {
          autoClose: 2000,
          type: toast.TYPE.SUCCESS
        })
      }
    }
  };

  onReset = () =>{
    const dataList = this.props.taskType === "tasks" ? cloneDeep(defaultTaskFieldList) : cloneDeep(defaultFieldComplianceList)
    this.setState({
      gridTableColumn: dataList || [],
    })
  }

  handleUpDown = (key, i) =>{
    const {gridTableColumn} = this.state
    let reserve = {}
    if(key === "up"){
      reserve = gridTableColumn[i-1]
      gridTableColumn[i-1] = gridTableColumn[i]
      gridTableColumn[i] = reserve
    }else if(key === "down"){
      reserve = gridTableColumn[i+1]
      gridTableColumn[i+1] = gridTableColumn[i]
      gridTableColumn[i] = reserve
    }

    this.setState({ gridTableColumn: cloneDeep(gridTableColumn)})
  }

  handleOnChange = (e, i) =>{
    const {gridTableColumn} = this.state
    gridTableColumn[i].isVisible = e.target.checked
    this.setState({ gridTableColumn: cloneDeep(gridTableColumn) })
  }

  render() {
    const { isModalState, taskType } = this.props;
    const { gridTableColumn } = this.state;
    let index = taskType === "tasks" ? 7 : taskType === "compliance" ? 5 : ""
    const isActive = gridTableColumn.filter(p => p.isVisible)
    return (
      <Modal
        closeModal={this.props.onSettingModal}
        modalState={isModalState}
        saveModal={(e) => (e)}
        title="Grid Configuration">
        <React.Fragment>
          <table className="table is-bordered is-striped is-hoverable is-fullwidth">
            <TableHeader cols={[{name: "Field"}, {name: "Visible"}, {name: "Action"}]} />
            <tbody>
            {gridTableColumn.map((el, i) =>{
              return(
                <tr key={i}>
                  <td>
                    { el.fieldName === "taskUnreadStatus" ? "Message Status" : null }
                    { el.fieldName === "tranClientName" ? "Client/Prospect" : null }
                    { el.fieldName === "activityDescription" ? "Transaction" : null }
                    { el.fieldName === "taskEndDate" ? "Due Date" : null }
                    { el.fieldName === "tranType" ? "Activity" : null }
                    { el.fieldName === "taskDescription" ? "To Do" : null }
                    { el.fieldName === "taskAssigneeName" ? "Assigned To" : null }
                    { el.fieldName === "taskStatus" ? "Activity Status" : null }
                    { el.fieldName === "taskCategory" ? "Task Category" : null }

                    { el.fieldName === "taskCategory" ? "Task Category" : null }
                  </td>

                  <td>
                    <input
                      name="isVisible"
                      type="checkbox"
                      checked={el.isVisible}
                      onChange={(e) => this.handleOnChange(e, i)}/>
                  </td>

                  <td>
                    <a className={i === 0 ? "isDisabled" : ""} onClick={()=> this.handleUpDown("up", i)}>
                      <span className="has-text-link">&nbsp;
                        <i className="fa fa-arrow-up" title="up"/>
                      </span>
                    </a>&nbsp;
                    <a className={index === i ? "isDisabled" : ""} onClick={()=> this.handleUpDown("down", i)}>
                      <span className="has-text-link">
                        <i className="fa fa-arrow-down" title="down"/>
                      </span>
                    </a>
                  </td>
                </tr>
              )
            })}
            </tbody>
          </table>
        </React.Fragment>
        <a className="button" onClick={this.onReset}>Reset</a>
        <div className="field is-grouped">
          <div className="control">
            <button className="button is-link is-small" disabled={!isActive.length} onClick={this.handleSubmit}>Save</button>
          </div>
          <div className="control">
            <button className="button is-light is-small" onClick={this.props.onSettingModal}>Cancel</button>
          </div>
        </div>
      </Modal>
    );
  }
}

export default GridTaskDashboardModal;
