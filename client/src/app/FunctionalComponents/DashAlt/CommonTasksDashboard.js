import React, { Component } from "react"
import { toast } from "react-toastify"
import { DropdownList } from "react-widgets"
import {DebounceInput} from "react-debounce-input"
import {
  getPicklistValues
} from "GlobalUtils/helpers"
import {
  saveMasterListSearchPrefs,
  deleteMasterListSearchPrefs,
  fetchMasterListSearchPrefs,
  getTasksTransactions,
  getTasksOthers,
  toggleReadStatus
} from "AppState/actions/AdminManagement/admTrnActions"
import Loader from "../../GlobalComponents/Loader"
import EntityPageGrid from "../EntityManagement/CommonComponents/EntityPageGrid"
import GridTaskDashboardModal from "./GridTaskDashboardModal";

export const Modal = ({ message, closeModal, modalState, onConfirmed }) => {
  if (!modalState) {
    return null
  }
  return (
    <div className="modal is-active">
      <div
        className="modal-background"
        onClick={closeModal}
        role="presentation"
        onKeyPress={() => { }}
      />
      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-title">Confirmation!</p>
        </header>
        <section className="modal-card-body">
          <div className="content">{message}</div>
        </section>
        <footer className="modal-card-foot">
          <div className="field is-grouped-center">
            <button className="button is-link" onClick={onConfirmed}> {/* eslint-disable-line */}
              Yes
            </button>
            <button className="button is-light" onClick={closeModal}> {/* eslint-disable-line */}
              No
            </button>
          </div>
        </footer>
      </div>
    </div>
  )
}

class CommonTasksDashboard extends Component {
  constructor(props) {
    super(props)
    this.state = this.initialState()
    this.changeFilter = this.changeFilter.bind(this)
  }

  initialState() {
    return {
      waiting: true,
      selectedTemplate: {
        searchName: ""
      },
      templates: [
      ],
      masterData: {
        pageSizeOptions: [5, 10, 20, 25, 50],
        eventStatuses: []
      },
      filters: {
        groupBy: "",
        pageSizePref: 25,
        defaultSearch: false
      },
      page: {
        global: 0,
        groups: {}
      },
      sort: {
        global: {},
        groups: {}
      },
      accordion: {
        filters: false,
        groups: {}
      },
      modalProps: {
        visible: false,
        message: ""
      },
      results: {
        global: {
          totalResults: 0,
          data: [],
          urls: []
        },
        groups: {}
      },
      gridTableData: [],
    }
  }

  async componentWillMount() {
    await this.initialPull()
  }

  componentWillReceiveProps(newProps) {
    if (this.props.type !== newProps.type) {
      this.setState(this.initialState(),async () =>
        await this.initialPull())
    }
  }

  initialPull = async () => {
    // let eventStatuses = await this.loadEventStatuses();
    const eventStatuses = ["Open", "Closed"]

    const templates = await this.loadSearchTemplates()
    let defaultTemplate = templates.data.find(template => JSON.parse(template.searchPreference).defaultSearch === true)
    let filters = {
      activityCategory: "self",
      activityTime: "",
      taskStatus: "",
      freeTextSearchTerm: "",
      taskReadUnread: "",
      groupBy: "",
      pageSizePref: 25,
      defaultSearch: false
    }
    if (defaultTemplate) {
      filters = JSON.parse(defaultTemplate.searchPreference)
    } else {
      defaultTemplate = {
        searchName: ""
      }
    }
    this.setState(prevState => ({
      ...prevState, templates, masterData: { ...prevState.masterData, eventStatuses }, filters,
      selectedTemplate: {...defaultTemplate}
    }), async () => this.getFullResult())
  };

  async getFullResult() {
    const { filters, page, sort } = this.state
    const {type} = this.props

    this.setState(prevState => ({ ...prevState, waiting: true }))
    const request = this.createFullRequest()
    const { results } = this.state
    let result = {}
    if (type === "tasks") {
      result = await getTasksTransactions(request)
    } else {
      result = await getTasksOthers(request)
    }

    if (filters.groupBy && filters.groupBy !== "" && (result.data && result.data.taskData && result.data.taskData[filters.groupBy] && result.data.taskData[filters.groupBy].length > 0)) {
      result.data.taskData[filters.groupBy].forEach(group => {
        results.groups[group._id] = {
          data: group.data,
          urls: result.data.urls,
          totalResults: group.count
        }
        page.groups[group._id] = 0
        sort.groups[group._id] = this.getSortFields()
      })

      if (result.data.taskData[filters.groupBy].length > 0) {
        await this.toggleGroupAccordion(result.data.taskData[filters.groupBy][0]._id)
      }
    } else {
      results.global.data = result.data.taskData.data
      results.global.totalResults = result.data.taskData.count
      results.global.urls = result.data.urls
    }

    this.setState(prevState => ({
      ...prevState,
      results,
      waiting: false
    }))

  }

  getGroupResult = async (groupId) => {
    const {type} = this.props
    this.setState(prevState => ({ ...prevState, waiting: true }))
    const { filters } = this.state
    const request = this.createPartialRequest(groupId)
    const { results } = this.state
    let result = {}
    if (type === "tasks") {
      result = await getTasksTransactions(request)
    } else {
      result = await getTasksOthers(request)
    }

    let taskList = result.data.taskData
    taskList = taskList[filters.groupBy].filter(taskList => taskList._id === groupId)

    results.groups[groupId].data = taskList && taskList.length == 1 ? taskList[0].data : []
    results.groups[groupId].urls = result.data.urls
    this.setState(prevState => ({ ...prevState, results, waiting: false }))
  }

  createFullRequest() {
    const { filters, page, sort } = this.state
    const request = {
      "groupBy": filters.groupBy,
      "freeTextSearchTerm": filters.freeTextSearchTerm,
      "activityTime": filters.activityTime,
      "activityCategory": filters.activityCategory,
      "taskReadUnread": filters.taskReadUnread,
      "taskStatus": filters.taskStatus,
    }
    request.pagination = {
      "serverPerformPagination": true,
      "size": filters.pageSizePref,
      "currentPage": page.global,
      "sortFields": this.getSortFields((!filters.groupBy || filters.groupBy === "") ? sort.global : sort.groups[Object.keys(this.state.accordion.groups)[0]])
    }
    return request
  }

  createPartialRequest(groupId) {
    const { filters, page, sort } = this.state
    const request = this.createFullRequest()
    request.groups = [{
      "value": groupId,
      "currentPage": page.groups[groupId],
      "size": filters.pageSizePref,
      "sortFields": this.getSortFields(sort.groups[groupId])
    }]
    return request
  }

  getSortFields(sort) {
    const {type} = this.state
    const sortFields = {}
    if (sort && sort[0]) {
      sortFields[sort[0].id] = sort[0].desc ? -1 : 1
    } else {
      if (type === "tasks") {
        sortFields.tranClientName = 1
      } else {
        sortFields.taskCategory = 1
      }
    }
    return sortFields
  }

  loadEventStatuses = async () => {
    const [eventStatusList] = await getPicklistValues(["LKUPSOEEVENTSTATUS"])
    return eventStatusList[1]
  }

  loadSearchTemplates = async () => await fetchMasterListSearchPrefs(`task-dashboard${  this.props.type}`)

  changeFilter(e) {
    const { name, value } = e.target
    const { filters } = this.state
    filters[name] = value
    this.setState(prevState => ({
      ...prevState,
      page: {
        global: 0,
        groups: {}
      },
      sort: {
        global: {},
        groups: {}
      },
      accordion: {
        filters: true,
        groups: {}
      },
      results: {
        global: {
          totalResults: 0,
          data: [],
          urls: []
        },
        groups: {}
      }
    }), async () => await this.getFullResult())
  }

  onGridGet = (changedPage, sorted, pageSize, groupTitle) => {
    const { page, sort } = this.state
    changedPage = changedPage || 0
    if (groupTitle && groupTitle !== "") {
      const sortGroups = sort.groups
      sortGroups[groupTitle] = sorted

      const pageGroups = page.groups
      pageGroups[groupTitle] = changedPage

      this.setState(prevState => ({
        ...prevState, sort: { ...prevState.sort, groups: sortGroups }, page: { ...prevState.page, groups: pageGroups, global: changedPage }
      }), async () => this.getGroupResult(groupTitle))

    } else {
      this.setState(prevState => ({
        ...prevState, sort: { ...prevState.sort, global: sorted }, page: { ...prevState.page, global: changedPage }
      }), async () => this.getFullResult())
    }
  }

  toggleFilterAccordion = () => {
    const { accordion } = this.state
    accordion.filters = !accordion.filters
    this.setState(prevState => ({
      ...prevState, accordion
    }))
  }

  toggleGroupAccordion = async (groupId) => {
    const { accordion } = this.state
    accordion.groups[groupId] = !accordion.groups[groupId]
    this.setState(prevState => ({
      ...prevState, accordion
    }))
    if (accordion.groups[groupId]) {
      await this.getGroupResult(groupId)
    }
  }

  toggleModal = () => {
    this.setState(prevState => {
      const newState = !prevState.modalProps.visible

      return {
        ...prevState, modalProps: {
          ...prevState.modalProps,
          visible: newState
        }
      }
    })
  }

  handleContextMenuClick = async (e) => {
    const {taskId:id, tranClientName, taskCategory} = e
    const { type } = this.props
    try {
      const { results, filters } = this.state
      this.setState({ waiting: true })
      const status = await toggleReadStatus(id)
      if (status.data.done) {
        if(!filters.groupBy || filters.groupBy === "") {
          results.global.data.filter(tran => tran.taskId === id)[0].taskUnreadStatus = !results.global.data.filter(tran => tran.taskId === id)[0].taskUnreadStatus
        } else {
          if(type === "tasks") {
            results.groups[tranClientName].data.filter(tran => tran.taskId === id)[0].taskUnreadStatus = !results.groups[tranClientName].data.filter(tran => tran.taskId === id)[0].taskUnreadStatus
          } else {
            results.groups[taskCategory].data.filter(tran => tran.taskId === id)[0].taskUnreadStatus = !results.groups[taskCategory].data.filter(tran => tran.taskId === id)[0].taskUnreadStatus
          }
        }
        toast(status.data.message, {
          autoClose: 2000,
          type: toast.TYPE.SUCCESS
        })
        this.setState(prevState => ({
          ...prevState,
          results,
          waiting: false
        }))
      }
    } catch {
      toast("Something went wrong. Please try again later.", {
        autoClose: 2000,
        type: toast.TYPE.WARNING,
        waiting: false
      })
    }
  }

  handleDefaultSearch = (e) => {
    const { checked } = e.target
    const { filters } = this.state
    filters.defaultSearch = checked
    this.setState(prevState => ({
      ...prevState, filters
    }))
  }

  handleNameSearch = (e) => {
    const { value } = e.target
    const { selectedTemplate } = this.state
    selectedTemplate.searchName = value
    this.setState(prevState => ({
      ...prevState, selectedTemplate
    }))
  }

  handleSavePref = async (defaultOverWrite = false) => {
    const { filters } = this.state
    let { templates, selectedTemplate } = this.state
    const { type } = this.props

    let save = true

    if (filters.defaultSearch) {
      const defaultSearches = templates.data.filter(template => JSON.parse(template.searchPreference).defaultSearch === true && template.searchName !== selectedTemplate.searchName)
      if (defaultSearches.length > 0 && !defaultOverWrite) {
        save = false
        this.setState(prevState => ({
          ...prevState,
          modalProps: {
            visible: true,
            message: "Do you want to override existing default search?"
          }
        }))
      } else {
        defaultSearches.forEach(async (search) => {
          await saveMasterListSearchPrefs(
            `task-dashboard${ type}`,
            search.searchName,
            {...JSON.parse(search.searchPreference), defaultSearch: false}
          )
        })

      }
    }

    if (save) {
      const savePrefResult = await saveMasterListSearchPrefs(
        `task-dashboard${type}`,
        selectedTemplate.searchName,
        filters
      )

      if (savePrefResult.status === 200) {
        if (!savePrefResult.data.data.nModified) {
          templates = await this.loadSearchTemplates()
        }
        toast("Successfully Saved", {
          autoClose: 2000,
          type: toast.TYPE.SUCCESS
        })
      } else {
        toast("Something went wrong!", {
          autoClose: 2000,
          type: toast.TYPE.ERROR
        })
      }
      selectedTemplate = templates.data.find(template => template.searchName === selectedTemplate.searchName)

      this.setState(prevState => ({
        ...prevState,
        templates,
        selectedTemplate: {...selectedTemplate},
        modalProps: {
          visible: false,
          message: ""
        }
      }))
    }
  }

  onSearchPrefChange = (e) => {
    const { value } = e.target
    this.setState(prevState => ({
      ...prevState,
      selectedTemplate: {...value},
      filters: JSON.parse(value.searchPreference)
    }), async () => await this.getFullResult())
  };

  handleDeletePref = async () => {
    const { selectedTemplate } = this.state
    let { templates } = this.state

    if (selectedTemplate._id && selectedTemplate._id !== "") {
      const savePrefResult = await deleteMasterListSearchPrefs(selectedTemplate._id)
      if (savePrefResult.status === 200) {
        if (!savePrefResult.data.data.nModified) {
          templates = await this.loadSearchTemplates()
          const selectedTemplate = {
            searchName: ""
          }
          this.setState(prevState => ({
            ...prevState, selectedTemplate, templates
          }))
        }
        toast("Successfully Deleted", {
          autoClose: 2000,
          type: toast.TYPE.SUCCESS
        })
      } else {
        toast("Something went wrong!", {
          autoClose: 2000,
          type: toast.TYPE.ERROR
        })
      }
    }
  }

  changeSearchString = (e) => {
    const searchString = e.target.value

    this.setState(prevState => ({
      ...prevState,
      filters: {
        ...prevState.filters,
        freeTextSearchTerm: searchString
      },
      page: {
        global: 0,
        groups: {
        }
      },
      sort: {
        global: {},
        groups: {
        }
      },
      accordion: {
        filters: true,
        groups: {
        }
      },
      results: {
        global: {
          totalResults: 0,
          data: [],
          urls: []
        },
        groups: {}
      }
    }), async () => await this.getFullResult())
  }

  renderGroupTable(key) {
    const { waiting, accordion, results, filters, masterData, page, gridTableData } = this.state
    const { auth, type, loginDetails } = this.props

    return (
      <div className="accordions" key={key}>
        <div className={`${accordion.groups[key] ? "accordion is-active" : "accordion"}`}>
          <div className="accordion-header" onClick={() => this.toggleGroupAccordion(key)} >
            <span>
              {key}
            </span>
            <div>({results.groups[key].totalResults}&nbsp;found) &nbsp;
              <i className={`${accordion.groups[key] ? "fas fa-chevron-up" : "fas fa-chevron-down"}`} />
            </div>
          </div>
          {accordion.groups[key] && (
            <EntityPageGrid
              gridTableData={gridTableData || []}
              key={key}
              search={filters.freeTextSearchTerm}
              entityList={
                results.groups[key].data
              }
              listType="task"
              auth={auth}
              getFilteredData={this.onGridGet}
              routeType={type}
              total={results.groups[key].totalResults}
              pages={Math.ceil((results.groups[key].totalResults + 1) / filters.pageSizePref)}
              page={page.groups[key]}
              pageSize={filters.pageSizePref}
              urls={results.groups[key].urls}
              loading={waiting}
              loginDetails={loginDetails}
              handleContextMenuClick={(data) => this.handleContextMenuClick(data)}
              groupBy={key}
              groupTitle={key}
              eventStatus={masterData.eventStatuses}
            />
          )}
        </div>
      </div>
    )
  }

  renderTable() {
    const {
      filters,
      waiting,
      masterData,
      results,
      page,
      gridTableData
    } = this.state
    const { auth, type, loginDetails } = this.props

    const idx = 25215
    return (
      <div className={`search-table-block box ${!filters.groupBy || filters.groupBy === "" ? "" : " table-group-by"}`}>
        <div className="columns">
          <div className="column"/>
          <div className="column">
            <div className="field is-grouped">
              <div className="control"/>
              <div className="control">
                <span className="has-text-link">
                  <i title="Grid setting"
                     onClick={this.onSettingModal}
                     className="far fa-2x fa fa-cog"
                     aria-hidden="true"/>
                </span>
              </div>
            </div>
          </div>
        </div>
        {
          !filters.groupBy || filters.groupBy === "" ?
            <EntityPageGrid
              gridTableData={gridTableData || []}
              key={idx}
              search={filters.freeTextSearchTerm}
              entityList={results.global.data}
              listType="task"
              auth={auth}
              getFilteredData={this.onGridGet}
              routeType={type}
              total={results.global.totalResults}
              pages={Math.ceil((results.global.totalResults + 1) / filters.pageSizePref)}
              page={page.global}
              pageSize={filters.pageSizePref}
              urls={results.global.urls}
              loading={waiting}
              handleContextMenuClick={(data) => this.handleContextMenuClick(data)}
              groupBy=""
              eventStatus={masterData.eventStatuses}
              loginDetails={loginDetails}
            /> : Object.keys(results.groups).map(key => this.renderGroupTable(key))
        }
      </div>
    )
  }

  renderGroupByFilter() {
    const { filters } = this.state
    const { type } = this.props
    const { groupBy } = filters
    return (
      <div className="select is-fullwidth is-link is-small">
        {type === "tasks" ?
          <select
            value={groupBy}
            name="groupBy"
            onChange={this.changeFilter}
          >
            <option value="">Group By</option>
            <option value="tranClientName">Client</option>
            <option value="tranStatus">Transaction Status</option>
            <option value="tranType">Transaction Type</option>
          </select>
          :
          <select
            value={groupBy}
            name="groupBy"
            onChange={this.changeFilter}
          >
            <option value="">Group By</option>
            <option value="taskCategory">Task Category</option>
            <option value="taskStatus">Task Status</option>
          </select>
        }
      </div>
    )
  }

  renderActivityCategoryFilter() {
    const { filters } = this.state
    const { activityCategory } = filters
    return (
      <div className="select is-fullwidth is-link is-small">
        <select
          value={activityCategory}
          name="activityCategory"
          onChange={this.changeFilter}
        >
          <option value="">All activities</option>
          <option value="self">All my activities</option>
          <option value="dealteam">All my deal activities</option>
          <option value="myfirm">All my firm activities</option>
        </select>
      </div>
    )
  }

  renderActivityTimeFilter() {
    const { filters } = this.state
    const { activityTime } = filters
    return (
      <div className="select is-fullwidth is-link is-small">
        <select
          value={activityTime}
          name="activityTime"
          onChange={this.changeFilter}
        >
          <option value="">All activities</option>
          <option value="day">Due today</option>
          <option value="week">Due this week</option>
          <option value="month">Due this month</option>
        </select>
      </div>
    )
  }

  renderTaskStatusFilter() {
    const { filters } = this.state
    const { taskStatus } = filters
    return (
      <div className="select is-fullwidth is-link is-small">
        <select
          value={taskStatus}
          name="taskStatus"
          onChange={this.changeFilter}
        >
          <option value="">All activities</option>
          <option value="closedcancelled">Closed or cancelled</option>
          <option value="openactive">Open or active</option>
        </select>
      </div>
    )
  }

  renderReadStatusFilter() {
    const { filters } = this.state
    const { taskReadUnread } = filters
    return (
      <div className="select is-fullwidth is-link is-small">
        <select
          value={taskReadUnread}
          name="taskReadUnread"
          onChange={this.changeFilter}
        >
          <option value="">Read & Unread</option>
          <option value="Read">Read</option>
          <option value="Unread">Unread</option>
        </select>
      </div>
    )
  }

  renderSearchTerm() {
    const { filters } = this.state
    const { freeTextSearchTerm } = filters
    return (
      <p className="control has-icons-left">
        <DebounceInput
          className="input is-fullwidth is-link is-small"
          placeholder="search"
          minLength={2}
          debounceTimeout={1000}
          value={this.freeTextSearchTerm}
          onChange={this.changeSearchString} />
        <span className="icon is-small is-left has-background-dark has-text-white">
          <i className="fas fa-search" />
        </span>
      </p>
    )
  }

  renderSelectSavedSearches() {
    const { selectedTemplate, templates, masterData, filters } = this.state
    return (
      <span>
        <div className="columns">
          <div className="column field-saved-search">
            <p className="multiExpLbl is-capitalized">Saved Search</p>
            <div className="field has-addons">
              <div className="control w-100">
                <div
                  className="select is-link is-fullwidth is-small third-party-select-hack">
                  <DropdownList
                    filter
                    value={selectedTemplate}
                    data={(templates && templates.data) ? templates.data : []}
                    valueField="_id"
                    textField="searchName"
                    onChange={val => {const event = {target: {name: "savedSearches",value: val}};this.onSearchPrefChange(event)}}
                  />
                </div>
              </div>
              <div className="control">
                <button // eslint-disable-line
                  className="button is-dark is-fullwidth is-small"
                  onClick={(e) => this.handleDeletePref(e)}
                  title="Delete selected search"
                >
                  Delete
                </button>
              </div>
            </div>
          </div>
          <div className="column is-4">
            <p className="multiExpLbl is-capitalized">Page size</p>
            <DropdownList dropUp
              filter={false}
              data={masterData.pageSizeOptions}
              defaultValue={25}
              value={filters.pageSizePref === "" ? "Page Size" : filters.pageSizePref}
              onChange={val => {const event = {target: {name: "pageSizePref",value: val}};this.changeFilter(event)}}
            />
          </div>
        </div>
      </span>
    )
  }

  renderSaveSearchElements = () => {
    const { selectedTemplate, filters, modalProps } = this.state
    return (
      <div className="columns">
        <Modal
          closeModal={this.toggleModal}
          modalState={modalProps.visible}
          message={modalProps.message}
          onConfirmed={() => this.handleSavePref(true)}
        />
        <div className="column is-fullwidth">
          <p className="multiExpLbl is-capitalized">Name Search</p>
          <div className="fieldis-fullwidth">
            <div className="control">
              <input
                className="input  is-link is-fullwidth is-small"
                type="text"
                name="searchPreferenceName"
                value={selectedTemplate.searchName}
                placeholder="Name your search"
                onChange={e => this.handleNameSearch(e)}
                title="Save this search criteria"
              />
            </div>
          </div>
        </div>
        <div className="column is-narrow">
          <div className="control">
            <p className="multiExpLbl is-capitalized">Default</p>
            <input
              className="checkbox is-link"
              type="checkbox"
              name="defaultSearch"
              checked={filters.defaultSearch}
              onChange={e => this.handleDefaultSearch(e)}
              title="Make this a default search"
              disabled={selectedTemplate.searchName === ""}
            />
          </div>
        </div>
        <div className="column is-narrow field-save-search-btn">
          <div className="control">
            <p className="multiExpLbl is-capitalized">&nbsp;</p>
            <button // eslint-disable-line
              className="button is-link is-small"
              onClick={() => this.handleSavePref(false)}>
              Save
            </button>

          </div>
        </div>
      </div>
    )
  };

  renderFilters() {
    const {accordion, modalState, modalMessage} = this.state
    const { filters } = accordion
    return (
      <div className="accordions common-tasks-dashboard">
        <div className={`accordion ${filters ? "is-active" : ""}`}>
          <div
            className="accordion-header toggle"
            onClick={this.toggleFilterAccordion}
          >
            <p>Search Data As</p>
            <i className={`fas fa-chevron-${filters ? "down" : "up"}` } style={{ cursor: "pointer" }} />

          </div>
          <div className="accordion-body">
            <div className="accordion-content">
              <div className="columns">
                <div className="column">
                  <p className="multiExpLbl">Group By</p>
                  {this.renderGroupByFilter()}
                </div>
                <div className="column">
                  <p className="multiExpLbl">Activity Category</p>
                  {this.renderActivityCategoryFilter()}
                </div>
                <div className="column">
                  <p className="multiExpLbl">Due Date</p>
                  {this.renderActivityTimeFilter()}
                </div>
              </div>
              <div className="columns">
                <div className="column">
                  <p className="multiExpLbl">Filter By Status</p>
                  {this.renderTaskStatusFilter()}
                </div>
                <div className="column">
                  <p className="multiExpLbl">Read Status</p>
                  {this.renderReadStatusFilter()}
                </div>
                <div className="column">
                  &nbsp;
                </div>
              </div>
              <div className="columns">
                <div className="column">
                  <p className="multiExpLbl">Search Tasks</p>
                  {this.renderSearchTerm()}
                </div>
                <Modal
                  closeModal={this.toggleModal}
                  modalState={modalState}
                  message={modalMessage}
                  onConfirmed={() => this.handleSavePref(true)}
                />
                <div className="column">{this.renderSelectSavedSearches()}</div>
                <div className="column">{this.renderSaveSearchElements()}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }

  onSettingModal = (e) =>{
    const { isModalState } = this.state
    this.setState({
      isModalState: !isModalState
    })
  }

  getTaskDashboardGridData = (data) => {
    if(data) {
      this.setState({
        gridTableData: data || []
      })
    }
  }

  render() {
    const { waiting, isModalState } = this.state
    const { type } = this.props
    return (
      <React.Fragment>
        <GridTaskDashboardModal
          taskType={type}
          isModalState={isModalState}
          onSettingModal={this.onSettingModal}
          getTaskDashboardGridData={this.getTaskDashboardGridData}/>
        {waiting && <Loader />}
        {this.renderFilters()}
        {this.renderTable()}
      </React.Fragment>
    )
  }
}
export default CommonTasksDashboard
