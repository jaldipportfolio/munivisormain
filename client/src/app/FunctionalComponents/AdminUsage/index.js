import React, { Component } from "react"
import {NavLink} from "react-router-dom"
import Loader from "../../GlobalComponents/Loader"
import Documents from "./components/Documents"
import CRMDetails from "./components/CRMDetails"
import Transaction from "./components/Transactions"
import Tasks from "./components/Tasks"
import {activeStyle} from "../../../globalutilities/consts"

const TABS = [
  { path: "tasks", label: "Tasks" },
  { path: "transactions", label: "Transactions" },
  { path: "crm-details", label: "CRM" },
  { path: "documents", label: "Documents" },
]

class AdminUsage extends Component {

  constructor(props) {
    super(props)
    this.state = {
      tabs: TABS,
      loading: true
    }
  }

  async componentDidMount() {
    const { nav2 } = this.props
    if(nav2){
      this.setState({
        selected: nav2 || "transactions",
        loading: false
      })
    } else {
      this.props.history.push("/admin-usage/tasks")
    }
  }

  renderViewSelection = (loanId, option) => (
    <nav className="tabs is-boxed">
      <ul>{this.renderTabs(this.state.tabs, loanId, option)}</ul>
    </nav>
  )

  renderTabs = (tabs) =>
    tabs.map(t => (
      <li key={t.path}>
        <NavLink to={`/admin-usage/${t.path}`} activeStyle={activeStyle}>
          {t.label}
        </NavLink>
      </li>
    ))

  renderSelectedView = (option) => {
    switch (option) {
    case "tasks":
      return (
        <Tasks
          {...this.props}
        />
      )
    case "documents":
      return (
        <Documents
          {...this.props}
        />
      )
    case "crm-details":
      return (
        <CRMDetails
          {...this.props}
        />
      )
    case "transactions":
      return (
        <Transaction
          {...this.props}
        />
      )
    default:
      return <p>{option}</p>
    }
  }

  render() {
    const { loading, selected } = this.state
    return (
      <div>
        { loading ? <Loader/> : null }
        <div className="hero is-link">
          <div className="hero-foot hero-footer-padding">
            <div className="container">
              {this.renderViewSelection(selected)}
            </div>
          </div>
        </div>
        <section id="main">
          {this.renderSelectedView(selected)}
        </section>
      </div>
    )
  }
}

export default AdminUsage

