import React, { Component } from "react"
import { Link, NavLink } from "react-router-dom"
import swal from "sweetalert"
import moment from "moment"
import { DateRangePicker } from "react-dates"
import "react-dates/initialize"
import "react-dates/lib/css/_datepicker.css"
import ReactTable from "react-table"
import { fetchTransactionAndComplianceTasks, transactionUrl } from "GlobalUtils/helpers"
import Accordion from "../../../GlobalComponents/Accordion"
import RatingSection from "../../../GlobalComponents/RatingSection"
import Loader from "../../../GlobalComponents/Loader"

const isArchived = () => (
  swal("Warning", "Your Checklist is Archived/Deleted", "warning")
)

const userUrl = (userType, userName, userId, search, taskAssigneeType) => {
  let url = ""

  if( taskAssigneeType === "entity") {
    switch (userType) {
    case "Firm":
    case "Self":
      url = `/admin-firms/${userId || ""}/firms`
      break
    case "Client":
    case "Prospect":
      url = `/clients-propects/${userId || ""}/entity`
      break
    case "Third Party":
      url = `/thirdparties/${userId || ""}/entity`
      break
    case "Undefined":
      url = `/migratedentities/${userId || ""}/entity`
      break
    default:
      url = `/thirdparties/${userId || ""}/entity`
      break
    }

  }
  else {
    switch (userType) {
    case "Firm":
    case "Self":
      url = `/admin-users/${userId || ""}/users`
      break
    case "Client":
      url = `/contacts/${userId || ""}/cltprops`
      break
    case "Prospect":
      url = `/contacts/${userId || ""}/cltprops`
      break
    case "Third Party":
      url = `/contacts/${userId || ""}/thirdparties`
      break
    case "Undefined":
      url = `/contacts/${userId || ""}/migratedentities`
      break
    default:
      url = ""
      break
    }
  }
  return (
    <Link to={url}>
      <small>{userName || "-"}</small>
    </Link>
  )
}


class Tasks extends Component {

  constructor(props) {
    super(props)
    this.state = {
      tasks: {},
      tranTask: [],
      complianceTask: [],
      loading: true
    }
  }

  async componentDidMount() {
    const tasks = await fetchTransactionAndComplianceTasks({
      "groupBy":"",
      "activityCategory":"",
      "activityTime":"",
      "taskStatus":"",
      "searchTerm":"",
      "loading":false
    })

    this.setEventState(tasks && tasks.allData)
    this.updateDimensions()
    window.addEventListener("resize", this.updateDimensions)
    this.setState({
      tasks: tasks && tasks.allData || {},
      loading: false
    })
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)
  }

  updateDimensions = () => {
    const windowWidth = window.innerWidth
    this.setState({
      orientation: windowWidth < 620 ? "vertical" : "horizontal",
      openDirection: windowWidth < 620 ? "down" : "down"
    })
  }

  setEventState = (transactions) => {
    const tasks = []
    const events = []
    transactions.deals.forEach(tran => {
      const newObject = {
        tranClientName: tran && tran.tranClientName || "",
        taskActivityContext: tran && tran.taskActivityContext || "",
        taskType: tran && tran.taskType || "",
        taskNotes: tran && tran.taskNotes || "",
        taskDescription: tran && tran.taskDescription || "",
        clientEntityId: tran && tran.clientEntityId || "",
        sortTranValue: tran && tran.sortTranValue || "",
        tranSubType: tran && tran.tranSubType || "",
        tranType: tran && tran.tranType || "",
        activityDescription: tran && tran.activityDescription || "",
        tranId: tran && tran.tranId || "",
        taskId: tran && tran.taskId || "",
        taskAssigneeName: tran && tran.taskAssigneeName || "",
        taskAssigneeId: tran && tran.taskAssigneeId || "",
        taskAssigneeType: tran && tran.taskAssigneeType || "",
        taskStatus: tran && tran.taskStatus || "",
        updatedAt: tran && tran.updatedAt || "",
        createdAt: tran && tran.createdAt || "",
      }
      newObject.type = `Debt / ${tran.tranSubType}`
      events.push(newObject)
    })

    Object.keys(transactions).forEach(keyTypes => {
      if (
        transactions &&
        transactions[keyTypes] &&
        transactions[keyTypes].length &&
        [
          "rfps",
          "marfps",
          "busdevs",
          "others",
          "bankloans",
          "derivatives"
        ].indexOf(keyTypes) !== -1
      ) {
        transactions[keyTypes].forEach(tran => {
          const newObject = {
            tranClientName: tran && tran.tranClientName || "",
            taskActivityContext: tran && tran.taskActivityContext || "",
            taskType: tran && tran.taskType || "",
            taskNotes: tran && tran.taskNotes || "",
            taskDescription: tran && tran.taskDescription || "",
            clientEntityId: tran && tran.clientEntityId || "",
            sortTranValue: tran && tran.sortTranValue || "",
            tranSubType: tran && tran.tranSubType || "",
            tranType: tran && tran.tranType || "",
            activityDescription: tran && tran.activityDescription || "",
            tranId: tran && tran.tranId || "",
            taskId: tran && tran.taskId || "",
            taskAssigneeName: tran && tran.taskAssigneeName || "",
            taskAssigneeId: tran && tran.taskAssigneeId || "",
            taskAssigneeType: tran && tran.taskAssigneeType || "",
            taskStatus: tran && tran.taskStatus || "",
            updatedAt: tran && tran.updatedAt || "",
            createdAt: tran && tran.createdAt || "",
          }
          newObject.type =
            keyTypes === "derivatives"
              ? `Derivative / ${tran.tranSubType}`
              : keyTypes === "bankloans"
                ? `Debt / ${tran.tranSubType}`
                : keyTypes === "rfps"
                  ? "Manage RFP for Client"
                  : keyTypes === "busdevs"
                    ? "Business Development" : tran.tranSubType
          events.push(newObject)
        })
      }
    })

    transactions && (transactions.catchalltasks || []).forEach(tran => {
      const newObject = {
        tranClientName: tran && tran.tranClientName || "",
        taskActivityContext: tran && tran.taskActivityContext || "",
        taskType: tran && tran.taskType || "",
        taskNotes: tran && tran.taskNotes || "",
        taskDescription: tran && tran.taskDescription || "",
        clientEntityId: tran && tran.clientEntityId || "",
        sortTranValue: tran && tran.sortTranValue || "",
        tranSubType: tran && tran.tranSubType || "",
        tranType: tran && tran.tranType || "",
        activityDescription: tran && tran.activityDescription || "",
        tranId: tran && tran.tranId || "",
        taskId: tran && tran.taskId || "",
        taskAssigneeName: tran && tran.taskAssigneeName || "",
        taskAssigneeId: tran && tran.taskAssigneeId || "",
        taskAssigneeType: tran && tran.taskAssigneeType || "",
        taskStatus: tran && tran.taskStatus || "",
        updatedAt: tran && tran.updatedAt || "",
        createdAt: tran && tran.createdAt || "",
      }
      newObject.type = tran.tranSubType
      tasks.push(newObject)
    })

    this.setState({
      complianceTask: tasks,
      tranTask: events,
      loading: false
    })
  }

  onChange = (startDate, endDate, type) => {
    let createStartDate = null
    let createEndDate = null
    let updateStartDate = null
    let updateEndDate = null
    if(type === "crate"){
      createStartDate = startDate !== undefined ? startDate : this.state.createStartDate
      createEndDate = endDate !== undefined ? endDate : this.state.createEndDate
    } else {
      updateStartDate = startDate !== undefined ? startDate : this.state.updateStartDate
      updateEndDate = endDate !== undefined ? endDate : this.state.updateEndDate
    }

    this.setState({
      createStartDate,
      createEndDate,
      updateStartDate,
      updateEndDate
    }, () => {
      const { tranTask, complianceTask } = this.state
      if(createStartDate && createEndDate || updateStartDate && updateEndDate){
        let task = []
        let compliance = []
        if(createStartDate && createEndDate){
          task = tranTask.filter(t => moment(startDate).format("YYYY-MM-DD") <= moment(t.createdAt).format("YYYY-MM-DD") && moment(endDate).format("YYYY-MM-DD") >= moment(t.createdAt).format("YYYY-MM-DD"))
          compliance = complianceTask.filter(t => moment(startDate).format("YYYY-MM-DD") <= moment(t.createdAt).format("YYYY-MM-DD") && moment(endDate).format("YYYY-MM-DD") >= moment(t.createdAt).format("YYYY-MM-DD"))
        } else if(updateStartDate && updateEndDate){
          task = tranTask.filter(t => moment(startDate).format("YYYY-MM-DD") <= moment(t.updatedAt).format("YYYY-MM-DD") && moment(endDate).format("YYYY-MM-DD") >= moment(t.updatedAt).format("YYYY-MM-DD"))
          compliance = complianceTask.filter(t => moment(startDate).format("YYYY-MM-DD") <= moment(t.updatedAt).format("YYYY-MM-DD") && moment(endDate).format("YYYY-MM-DD") >= moment(t.updatedAt).format("YYYY-MM-DD"))
        }
        this.setState({
          task,
          compliance
        })
      }
    })
  }


  render() {
    const {
      loading,
      // tranTask,
      // complianceTask,
      createStartDate,
      createEndDate,
      updateStartDate,
      updateEndDate,
      createFocusedInput,
      updateFocusedInput,
      orientation,
      openDirection,
      task,
      compliance
    } = this.state
    const { loginDetails } = this.props
    const tranTask = (createStartDate && createEndDate || updateStartDate && updateEndDate) ? task : this.state.tranTask || []
    const complianceTask = (createStartDate && createEndDate || updateStartDate && updateEndDate) ? compliance : this.state.complianceTask || []
    return (
      <div >
        { loading ? <Loader/> : null }
        <Accordion
          multiple activeItem={[0, 1]} boxHidden render={({ activeAccordions, onAccordion }) =>
            <div>
              <div className="columns">
                <div className="column">
                  <p className="multiExpLbl">Crated Date</p>
                  <DateRangePicker
                    startDate={createStartDate}
                    startDateId="createStartDate"
                    endDate={createEndDate}
                    endDateId="createEndDate"
                    isOutsideRange={() => false}
                    small
                    showDefaultInputIcon
                    showClearDates
                    hideKeyboardShortcutsPanel
                    onDatesChange={({startDate, endDate}) => this.onChange(startDate, endDate, "crate")}
                    focusedInput={createFocusedInput}
                    onFocusChange={createFocusedInput => this.setState({createFocusedInput})}
                    orientation={orientation}
                    openDirection={openDirection}
                  />
                </div>

                <div className="column">
                  <p className="multiExpLbl">Updated Date</p>
                  <DateRangePicker
                    startDate={updateStartDate}
                    startDateId="updateStartDate"
                    endDate={updateEndDate}
                    endDateId="updateEndDate"
                    isOutsideRange={() => false}
                    small
                    showDefaultInputIcon
                    showClearDates
                    hideKeyboardShortcutsPanel
                    onDatesChange={({startDate, endDate}) => this.onChange(startDate, endDate)}
                    focusedInput={updateFocusedInput}
                    onFocusChange={updateFocusedInput => this.setState({updateFocusedInput})}
                    orientation={orientation}
                    openDirection={openDirection}
                  />
                </div>
              </div>

              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="Transaction Tasks"
                style={{ overflowY: "unset" }}>
                {activeAccordions.includes(0) &&
              <div>

                <ReactTable
                  columns={[
                    {
                      Header: "Client/Prospect",
                      id: "tranClientName",
                      className: "multiExpTblVal",
                      accessor: e => e,
                      sortMethod: (a, b) => a.tranClientName.localeCompare(b.tranClientName),
                      Cell: e =>
                        (e.value && e.value.taskActivityContext === "Non Transaction Tasks") ? (
                          e.value.taskType === "Compliance" ? (
                            <div className="hpTablesTd wrap-cell-text">
                              <NavLink
                                to={
                                  e.value.taskNotes
                                    ? `/cac/view-control?cid=${e.value.taskNotes}`
                                    : "/cac/view-control"
                                }
                              >{e.value.taskDescription || "notes"}</NavLink>
                            </div>
                          ) : (
                            <span className="hpTablesTd wrap-cell-text">
                              <NavLink
                                to={`/clients-propects/${e.value.clientEntityId || ""}/entity`}
                              >{e.value.tranClientName || "all"}</NavLink>
                            </span>
                          )
                        ) : (
                          <div className="hpTablesTd wrap-cell-text tooltips">
                            <NavLink
                              to={`/clients-propects/${e.value.clientEntityId || ""}/entity`}
                            >{e.value.tranClientName || "something"}</NavLink>
                          </div>
                        )
                    },
                    {
                      Header: "Transaction",
                      id: "activityDescription",
                      className: "multiExpTblVal",
                      accessor: e => e,
                      sortMethod: (a, b) =>
                        a.sortTranValue && a.sortTranValue.localeCompare(b.sortTranValue),
                      Cell: e =>
                        // groupBy ||
                        (e.value && e.value.taskActivityContext === "Non Transaction Tasks") ? (
                          e.value.tranSubType === "Compliance" ? (
                            <div className="hpTablesTd wrap-cell-text tooltips">
                              <NavLink
                                to="/cac/view-control"
                              >{e.value.tranSubType || "-"}</NavLink>
                            </div>
                          ) :
                            <div className="hpTablesTd wrap-cell-text tooltips">
                              {
                                transactionUrl(e.value.tranType, e.value.tranSubType, e.value.activityDescription, e.value.tranId, "")
                              }
                            </div>
                        ) :
                          <div className="hpTablesTd wrap-cell-text tooltips">
                            {
                              transactionUrl(e.value.tranType, e.value.tranSubType, e.value.activityDescription, e.value.tranId, "")
                            }
                          </div>
                    },
                    {
                      id: "issuer",
                      Header: "Activity",
                      className: "multiExpTblVal",
                      accessor: item => item,
                      Cell: row => {
                        const item = row.value
                        return (
                          <div className="hpTablesTd wrap-cell-text">
                            { item && item.tranType || "" }
                          </div>
                        )
                      },
                      sortMethod: (a, b) => a.fileName - b.fileName,
                    },
                    {
                      Header: "To Do",
                      id: "taskDescription",
                      className: "multiExpTblVal",
                      accessor: e => e,
                      Cell: e =>
                        (e.value && e.value.taskActivityContext === "Non Transaction Tasks") ? (
                          e.value.tranSubType === "Compliance" ? (
                            <div className="hpTablesTd wrap-cell-text">
                              <NavLink
                                to={
                                  e.value.taskNotes
                                    ? `/cac/view-control/view-detail?id=${e.value.taskNotes}`
                                    : "/cac/view-control"
                                }
                              >{e.value.taskDescription || "-"}</NavLink>
                            </div>
                          ) : (
                            <span className="hpTablesTd wrap-cell-text">
                              <NavLink
                                to={`/tasks/${e.value.taskId}`}
                              >{e.value.taskDescription || "-"}</NavLink>
                            </span>
                          )
                        ) : e.value.taskActivityContext ? (
                          <div className="hpTablesTd wrap-cell-text">
                            <div>
                              {
                                e.value.taskArchive ? <small className="has-text-link" onClick={isArchived}>{e.value.taskDescription}</small>
                                  : transactionUrl(e.value.tranType, e.value.tranSubType, e.value.taskDescription, e.value.tranId, "", `check-track?cid=${e.value.taskActivityContext}`)
                              }
                            </div>
                          </div>
                        ) : (
                          <span className="hpTablesTd wrap-cell-text">
                            <NavLink
                              to={`/tasks/${e.value.taskId}`}
                            >{e.value.taskDescription || "-"}</NavLink>
                          </span>
                        )
                    },
                    {
                      Header: "Assigned To",
                      id: "taskAssigneeName",
                      className: "multiExpTblVal",
                      accessor: e => e,
                      Cell: e => {
                        const relationType = loginDetails && loginDetails.userEntities && loginDetails.userEntities.length && loginDetails.userEntities[0] && loginDetails.userEntities[0].relationshipToTenant
                        return(
                          <div className="hpTablesTd wrap-cell-text">
                            {userUrl(relationType, e.value.taskAssigneeName, e.value.taskAssigneeId, "",e.value.taskAssigneeType)}
                          </div>
                        )
                      },
                      sortMethod: (a, b) => a.taskAssigneeName - b.taskAssigneeName,
                    },
                    {
                      Header: "Activity Status",
                      id: "taskStatus",
                      className: "multiExpTblVal",
                      accessor: e => e,
                      Cell: e => (e.value.taskStatus === "Closed" ? e.value.taskStatus : "Open")
                    }
                  ]}
                  data={tranTask || []}
                  showPaginationBottom
                  defaultPageSize={10}
                  className="-striped -highlight is-bordered"
                  style={{ overflowX: "auto" }}
                  showPageJump
                  minRows={2}
                />

              </div>
                }
              </RatingSection>

              <RatingSection
                onAccordion={() => onAccordion(1)}
                title="Compliance/Others Tasks"
                style={{ overflowY: "unset" }}>
                {activeAccordions.includes(1) &&
              <div>

                <ReactTable
                  columns={[
                    {
                      Header: "Task Category",
                      id: "taskCategory",
                      className: "multiExpTblVal",
                      accessor: e => e,
                      sortMethod: (a, b) => a.taskCategory.localeCompare(b.taskCategory),
                      Cell: e =>
                        e.value.taskType === "Compliance" ? (
                          <div className="hpTablesTd wrap-cell-text">
                            <NavLink
                              to={
                                e.value.taskNotes
                                  ? `/cac/view-control/view-detail?id=${e.value.taskNotes}`
                                  : "/cac/view-control"
                              }
                            >{e.value.taskCategory || "notes"}</NavLink>
                          </div>
                        ) : (
                          <span className="hpTablesTd wrap-cell-text">
                            <NavLink
                              to={`/tasks/${e.value.taskId}`}
                            >{e.value.taskCategory || "all"}</NavLink>
                          </span>
                        )
                    },
                    {
                      Header: "To Do",
                      id: "taskDescription",
                      className: "multiExpTblVal",
                      accessor: e => e,
                      Cell: e =>
                        // groupBy ||
                        (e.value && e.value.taskActivityContext === "Non Transaction Tasks") ? (
                          e.value.taskType === "Compliance" ? (
                            <div className="hpTablesTd wrap-cell-text">
                              <NavLink
                                to={
                                  e.value.taskNotes
                                    ? `/cac/view-control/view-detail?id=${e.value.taskNotes}`
                                    : "/cac/view-control"
                                }
                              >{e.value.taskDescription || "-"}</NavLink>
                            </div>
                          ) : (
                            <span className="hpTablesTd wrap-cell-text">
                              <NavLink
                                to={`/tasks/${e.value.taskId}`}
                              >{e.value.taskDescription || "-"}</NavLink>
                            </span>
                          )
                        ) : e.value.tranType === "BusinessDevelopment" ? (
                          <span className="hpTablesTd wrap-cell-text">
                            <NavLink
                              to={`/bus-development?id=${e.value.taskId}`}
                            >{e.value.taskDescription || "-"}</NavLink>
                          </span>
                        ) : (
                          <span className="hpTablesTd wrap-cell-text">
                            <NavLink
                              to={`/tasks/${e.value.taskId}`}
                            >{e.value.taskDescription || "-"}</NavLink>
                          </span>
                        )
                    },
                    {
                      Header: "Assigned To",
                      id: "taskAssigneeName",
                      className: "multiExpTblVal",
                      accessor: e => e,
                      Cell: e => {
                        const relationType = loginDetails && loginDetails.userEntities && loginDetails.userEntities.length && loginDetails.userEntities[0] && loginDetails.userEntities[0].relationshipToTenant
                        return (
                          <div className="hpTablesTd wrap-cell-text">
                            {userUrl(relationType, e.value.taskAssigneeName, e.value.taskAssigneeId, "")}
                          </div>
                        )
                      }
                    },
                    {
                      Header: "Activity Status",
                      id: "taskStatus",
                      className: "multiExpTblVal",
                      accessor: e => e,
                      Cell: e => (e.value.taskStatus === "Closed" ? e.value.taskStatus : "Open")
                    }
                  ]}
                  data={complianceTask || []}
                  showPaginationBottom
                  defaultPageSize={10}
                  className="-striped -highlight is-bordered"
                  style={{ overflowX: "auto" }}
                  showPageJump
                  minRows={2}
                />

              </div>
                }
              </RatingSection>

            </div>
          }
        />
      </div>
    )
  }
}

export default Tasks

