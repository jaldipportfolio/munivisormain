import React, { Component } from "react"
import ReactTable from "react-table"
import fileSize from "file-size"
import { fetchAWSS3Documents, fetchS3DataByContextWise, getAWSS3DocDetails, getFile } from "GlobalUtils/helpers"
import Accordion from "../../../GlobalComponents/Accordion"
import RatingSection from "../../../GlobalComponents/RatingSection"
import Loader from "../../../GlobalComponents/Loader"

class Documents extends Component {

  constructor(props) {
    super(props)
    this.state = {
      loading: true
    }
  }

  async componentDidMount() {
    const { loginDetails } = this.props
    const tenantId = loginDetails && loginDetails.relatedFaEntities && loginDetails.relatedFaEntities.length && loginDetails.relatedFaEntities[0] && loginDetails.relatedFaEntities[0].entityId
    const context = await fetchAWSS3Documents(tenantId)
    this.setState({
      tenantId,
      context,
      loading: false
    })
  }

  onContextSelect = (value) => {
    this.setState({
      loading: true,
      selectedContext: value
    }, async () => {
      const data = await fetchS3DataByContextWise(this.state.tenantId, {contextType: value})
      this.setState({data, loading: false})
    })
  }

  onFile = async (key) => {
    this.setState({
      loading: true
    }, async () => {
      const docs = await getAWSS3DocDetails(key)
      const { tenantId, contextType, contextId, name, originalName } = docs
      const objectName = `${tenantId}/${contextType}/${contextType}_${contextId}/${name}`
      this.setState({
        loading: false, objectName, originalName
      }, () => getFile(`?objectName=${encodeURIComponent(this.state.objectName)}`, this.state.originalName))
    })
  }


  render() {
    const { loading, context, data, selectedContext } = this.state
    return (
      <div>
        { loading ? <Loader/> : null }
        <Accordion
          multiple activeItem={[0, 1]} boxHidden render={({ activeAccordions, onAccordion }) =>
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="Documents Context Type"
                style={{ overflowY: "unset" }}>
                {activeAccordions.includes(0) &&
                  <div>
                    { context && context.contextList && context.contextList.map((obj, index) => (
                      <span key={index.toString()}>
                        <a
                          className="button is-hovered"
                          onClick={() => this.onContextSelect(obj)}
                          style={selectedContext === obj ? {width: 240, marginTop: 5, border: "#4288dd 1px solid", backgroundColor: "gainsboro"} : {width: 240, marginTop: 5, border: "#4288dd 1px solid"} }>{obj}</a>&nbsp;&nbsp;&nbsp;
                      </span>
                    )) }

                    { selectedContext ?
                      <RatingSection
                        onAccordion={() => onAccordion(1)}
                        title={selectedContext || ""}
                        style={{overflowY: "unset"}}>
                        {activeAccordions.includes(1) &&
                        <div>
                          <ReactTable
                            columns={[
                              {
                                id: "issuer",
                                Header: "File Name",
                                className: "multiExpTblVal",
                                accessor: item => item,
                                Cell: row => {
                                  const item = row.value
                                  return (
                                    <div className="hpTablesTd wrap-cell-text">
                                      <a onClick={() => this.onFile(item.fileName)}>{item.fileName}</a>
                                    </div>
                                  )
                                },
                                sortMethod: (a, b) => a.fileName - b.fileName,
                              },
                              {
                                id: "Size",
                                Header: "Size",
                                className: "multiExpTblVal",
                                accessor: item => item,
                                Cell: row => {
                                  const item = row.value
                                  return (
                                    <div className="hpTablesTd wrap-cell-text">
                                      { item && item.Size && fileSize(item.Size).human("jedec") || "" }
                                    </div>
                                  )
                                },
                                sortMethod: (a, b) => a.Size - b.Size,
                                Footer: (
                                  <span>
                                    <b>Total Size:</b>&nbsp;
                                    { data && data.totalSize || "0.0 Bytes" }
                                  </span>
                                )
                              },
                            ]}
                            data={data && data.Versions || []}
                            showPaginationBottom
                            defaultPageSize={10}
                            className="-striped -highlight is-bordered"
                            style={{ overflowX: "auto" }}
                            showPageJump
                            minRows={2}
                          />
                        </div>
                        }
                      </RatingSection> : null
                    }

                  </div>
                }
              </RatingSection>

            </div>
          }
        />
      </div>
    )
  }
}

export default Documents

