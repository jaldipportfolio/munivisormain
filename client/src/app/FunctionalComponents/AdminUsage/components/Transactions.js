import React, { Component } from "react"
import ReactTable from "react-table"
import moment from "moment"
import { fetchTransactionsTypeAndSubTypeWise } from "GlobalUtils/helpers"
import { DateRangePicker } from "react-dates"
import "react-dates/initialize"
import "react-dates/lib/css/_datepicker.css"
import Accordion from "../../../GlobalComponents/Accordion"
import RatingSection from "../../../GlobalComponents/RatingSection"
import Loader from "../../../GlobalComponents/Loader"

class Transactions extends Component {

  constructor(props) {
    super(props)
    this.state = {
      transactions: [],
      modifiedTrans: [],
      loading: true
    }
  }

  async componentDidMount() {
    const { loginDetails } = this.props
    const tenantId = loginDetails && loginDetails.relatedFaEntities && loginDetails.relatedFaEntities.length && loginDetails.relatedFaEntities[0] && loginDetails.relatedFaEntities[0].entityId
    const entityDetails = await fetchTransactionsTypeAndSubTypeWise(tenantId)
    this.setState({
      transactions: entityDetails && entityDetails.transactions || [],
      loading: false
    }, () => {
      this.onSeparateTransaction(this.state.transactions)
    })
  }

  onSeparateTransaction = (transaction) => {
    const groupType = _.groupBy(transaction, "type")
    const groupSubType = []
    Object.keys(groupType).map(m => groupSubType.push({ [m]: _.groupBy(groupType[m], "subType")}))
    const modifiedTrans = []
    groupSubType.forEach(f => Object.keys(f).map(m => Object.keys(f[m]).map(o => modifiedTrans.push({type: m, subType: o, length: f[m][o].length})) ))
    this.setState({
      modifiedTrans
    })
  }

  onChange = (startDate, endDate, type) => {
    let createStartDate = null
    let createEndDate = null
    let updateStartDate = null
    let updateEndDate = null
    if(type === "crate"){
      createStartDate = startDate !== undefined ? startDate : this.state.createStartDate
      createEndDate = endDate !== undefined ? endDate : this.state.createEndDate
    } else {
      updateStartDate = startDate !== undefined ? startDate : this.state.updateStartDate
      updateEndDate = endDate !== undefined ? endDate : this.state.updateEndDate
    }

    this.setState({
      createStartDate,
      createEndDate,
      updateStartDate,
      updateEndDate
    }, () => {
      const { transactions } = this.state
      if(createStartDate && createEndDate || updateStartDate && updateEndDate){
        let trans = []
        if(createStartDate && createEndDate){
          trans = transactions.filter(t => moment(startDate).format("YYYY-MM-DD") <= moment(t.createdAt).format("YYYY-MM-DD") && moment(endDate).format("YYYY-MM-DD") >= moment(t.createdAt).format("YYYY-MM-DD"))
        } else if(updateStartDate && updateEndDate){
          trans = transactions.filter(t => moment(startDate).format("YYYY-MM-DD") <= moment(t.updatedAt).format("YYYY-MM-DD") && moment(endDate).format("YYYY-MM-DD") >= moment(t.updatedAt).format("YYYY-MM-DD"))
        }
        this.setState({
          trans
        }, () => {
          this.onSeparateTransaction(this.state.trans)
        })
      } else {
        this.onSeparateTransaction(this.state.transactions)
      }
    })
  }



  render() {
    const {
      loading,
      modifiedTrans,
      createStartDate,
      createEndDate,
      createFocusedInput,
      orientation,
      openDirection,
      updateStartDate,
      updateEndDate,
      updateFocusedInput,
    } = this.state
    return (
      <div >
        { loading ? <Loader/> : null }
        <Accordion
          multiple activeItem={[0]} boxHidden render={({ activeAccordions, onAccordion }) =>
            <div>

              <div className="columns">
                <div className="column">
                  <p className="multiExpLbl">Crated Date</p>
                  <DateRangePicker
                    startDate={createStartDate}
                    startDateId="createStartDate"
                    endDate={createEndDate}
                    endDateId="createEndDate"
                    isOutsideRange={() => false}
                    small
                    showDefaultInputIcon
                    showClearDates
                    hideKeyboardShortcutsPanel
                    onDatesChange={({startDate, endDate}) => this.onChange(startDate, endDate, "crate")}
                    focusedInput={createFocusedInput}
                    onFocusChange={createFocusedInput => this.setState({createFocusedInput})}
                    orientation={orientation}
                    openDirection={openDirection}
                  />
                </div>

                <div className="column">
                  <p className="multiExpLbl">Updated Date</p>
                  <DateRangePicker
                    startDate={updateStartDate}
                    startDateId="updateStartDate"
                    endDate={updateEndDate}
                    endDateId="updateEndDate"
                    isOutsideRange={() => false}
                    small
                    showDefaultInputIcon
                    showClearDates
                    hideKeyboardShortcutsPanel
                    onDatesChange={({startDate, endDate}) => this.onChange(startDate, endDate)}
                    focusedInput={updateFocusedInput}
                    onFocusChange={updateFocusedInput => this.setState({updateFocusedInput})}
                    orientation={orientation}
                    openDirection={openDirection}
                  />
                </div>
              </div>

              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="Transactions"
                style={{ overflowY: "unset" }}>
                {activeAccordions.includes(0) &&
                  <div>

                    <ReactTable
                      columns={[
                        {
                          id: "type",
                          Header: "Transaction Type",
                          className: "multiExpTblVal",
                          accessor: item => item,
                          Cell: row => {
                            const item = row.value
                            return (
                              <div className="hpTablesTd wrap-cell-text">
                                { item && item.type || "" }
                              </div>
                            )
                          },
                          sortMethod: (a, b) => (a.type || "").localeCompare(b.type || "")
                        },
                        {
                          id: "subType",
                          Header: "Transaction Subtype",
                          className: "multiExpTblVal",
                          accessor: item => item,
                          Cell: row => {
                            const item = row.value
                            return (
                              <div className="hpTablesTd wrap-cell-text">
                                { item && item.subType || 0 }
                              </div>
                            )
                          },
                          sortMethod: (a, b) => (a.subType || "").localeCompare(b.subType || "")
                        },
                        {
                          id: "length",
                          Header: "Number Of Transaction",
                          className: "multiExpTblVal",
                          accessor: item => item,
                          Cell: row => {
                            const item = row.value
                            return (
                              <div className="hpTablesTd wrap-cell-text">
                                { item && item.length || 0 }
                              </div>
                            )
                          },
                          sortMethod: (a, b) => a.length - b.length
                        }
                      ]}
                      data={modifiedTrans || []}
                      showPaginationBottom
                      defaultPageSize={10}
                      className="-striped -highlight is-bordered"
                      style={{ overflowX: "auto" }}
                      showPageJump
                      minRows={2}
                    />

                  </div>
                }
              </RatingSection>

            </div>
          }
        />
      </div>
    )
  }
}

export default Transactions

