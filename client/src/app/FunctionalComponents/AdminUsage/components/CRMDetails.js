import React, { Component } from "react"
import _ from "lodash"
import ReactTable from "react-table"
import { fetchCRMDetails } from "GlobalUtils/helpers"
import Accordion from "../../../GlobalComponents/Accordion"
import RatingSection from "../../../GlobalComponents/RatingSection"
import Loader from "../../../GlobalComponents/Loader"

class CRMDetails extends Component {

  constructor(props) {
    super(props)
    this.state = {
      loading: true
    }
  }

  async componentDidMount() {
    const { loginDetails } = this.props
    const tenantId = loginDetails && loginDetails.relatedFaEntities && loginDetails.relatedFaEntities.length && loginDetails.relatedFaEntities[0] && loginDetails.relatedFaEntities[0].entityId
    const entityDetails = await fetchCRMDetails({tenantId})
    this.setState({
      entityDetails,
      loading: false
    })
  }


  render() {
    const { loading, entityDetails } = this.state
    return (
      <div >
        { loading ? <Loader/> : null }
        <Accordion
          multiple activeItem={[0, 1]} boxHidden render={({ activeAccordions, onAccordion }) =>
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="CRM Details"
                style={{ overflowY: "unset" }}>
                {activeAccordions.includes(0) &&
                  <div>

                    <ReactTable
                      columns={[
                        {
                          id: "issuer",
                          Header: "Type Of Entity",
                          className: "multiExpTblVal",
                          accessor: item => item,
                          Cell: row => {
                            const item = row.value
                            return (
                              <div className="hpTablesTd wrap-cell-text">
                                { item && item.type || "" }
                              </div>
                            )
                          },
                          sortMethod: (a, b) => a.type - b.type,
                        },
                        {
                          id: "length",
                          Header: "Number Of Entity",
                          className: "multiExpTblVal",
                          accessor: item => item,
                          Cell: row => {
                            const item = row.value
                            return (
                              <div className="hpTablesTd wrap-cell-text">
                                { item && item.length || 0 }
                              </div>
                            )
                          },
                          sortMethod: (a, b) => a.length - b.length,
                          Footer: (
                            <span>
                              <strong>Total:</strong>&nbsp;
                              {Number(_.sum(_.map(entityDetails && entityDetails.crmDetails, d => d.length))).toLocaleString()}
                            </span>
                          )
                        },
                        {
                          id: "row.value.active",
                          Header: "Active Users",
                          className: "multiExpTblVal",
                          accessor: item => item,
                          Cell: row => {
                            const item = row.value
                            return (
                              <div className="hpTablesTd wrap-cell-text">
                                { item && item.active || 0 }
                              </div>
                            )
                          },
                          sortMethod: (a, b) => a.active - b.active,
                          Footer: (
                            <span>
                              <strong>Total:</strong>&nbsp;
                              {Number(_.sum(_.map(entityDetails && entityDetails.crmDetails, d => d.active))).toLocaleString()}
                            </span>
                          )
                        },
                        {
                          id: "inactive",
                          Header: "Inactive Users",
                          className: "multiExpTblVal",
                          accessor: item => item,
                          Cell: row => {
                            const item = row.value
                            return (
                              <div className="hpTablesTd wrap-cell-text">
                                { item && item.inactive || 0 }
                              </div>
                            )
                          },
                          sortMethod: (a, b) => a.inactive - b.inactive,
                          Footer: (
                            <span>
                              <strong>Total:</strong>&nbsp;
                              {Number(_.sum(_.map(entityDetails && entityDetails.crmDetails, d => d.inactive))).toLocaleString()}
                            </span>
                          )
                        },
                      ]}
                      data={entityDetails && entityDetails.crmDetails || []}
                      showPaginationBottom
                      defaultPageSize={10}
                      className="-striped -highlight is-bordered"
                      style={{ overflowX: "auto" }}
                      showPageJump
                      minRows={2}
                    />

                  </div>
                }
              </RatingSection>

            </div>
          }
        />
      </div>
    )
  }
}

export default CRMDetails

