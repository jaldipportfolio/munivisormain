import React, { Component } from "react"

import DocsMain from "./DocsMain"
import DocUpload from "./DocUpload"

class DocsActions extends Component {
  constructor(props) {
    super(props)
    this.state = {view: "", fileName: "" }
    this.onUploadSuccess = this.onUploadSuccess.bind(this)
  }

  // componentWillReceiveProps(nextProps) {
  //   //const { fileName } = this.state
  //   const { contextIndex, contextFields } = nextProps
  //   const { DocumentType1, AccessEntitlement1, DocumentStatus1 } = contextFields
  //   if(DocumentType1 && DocumentType1.value && AccessEntitlement1 &&
  //     AccessEntitlement1.value && DocumentStatus1 && DocumentStatus1.value) {
  //     nextProps.docs.some(d => {
  //       if(d.meta.hasOwnProperty("contextIndex") && d.meta.DocumentType1 &&
  //         d.meta.DocumentType1.value && d.meta.AccessEntitlement1 &&
  //         d.meta.AccessEntitlement1.value && d.meta.DocumentStatus1 &&
  //         d.meta.DocumentStatus1.value) {
  //         if(d.meta.contextIndex === contextIndex &&
  //           d.meta.DocumentType1.value === DocumentType1.value &&
  //           d.meta.AccessEntitlement1.value === AccessEntitlement1.value &&
  //           d.meta.DocumentStatus1.value === DocumentStatus1.value &&
  //           d.meta.DocumentStatus1.value === DocumentStatus1.value) {
  //           if(!this.state.view) {
  //             this.setState({ view: "show-uploaded-doc", fileName: d.name })
  //           }
  //           return true
  //         }
  //       }
  //     })
  //   }
  // }

  onUploadSuccess(fileName) {
    this.setState({ view: "post-upload", fileName })
    this.props.docSubmitButton.click()
  }

  renderPostUploadView() {
    return (
      <DocsMain
        rowCells={this.props.rowCells}
        tenantId={this.props.tenantId}
        contextType={this.props.contextType}
        contextId={this.props.contextId}
        fileName={this.state.fileName}
        showUploadButton={false} />
    )
  }

  render() {
    return (
      !this.state.view ?
        <DocUpload
          tenantId={this.props.tenantId}
          showFeedback={this.props.showFeedback}
          contextType={this.props.contextType}
          contextId={this.props.contextId}
          onUploadSuccess={this.onUploadSuccess} /> :
        this.renderPostUploadView()
    )
  }
}

export default DocsActions
