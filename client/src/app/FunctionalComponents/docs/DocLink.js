import React, { Component } from "react"
import CONST from "../../../globalutilities/consts"
import { getDocDetails, getFile, getPlatFormDocDetails, getPlatFormFile } from "GlobalUtils/helpers"
import Loader from "../../GlobalComponents/LineLoader"

class DocLink extends Component {
  constructor(props) {
    super(props)
    this.state = { fileName: "", waiting: true, error: "", objectName: "", bucketName: CONST.platFormBucketName }
  }

  async componentDidMount() {
    await this.getDocInfo(this.props.docId)
  }

  async componentWillReceiveProps(nextProps) {
    if((nextProps.docId !== this.props.docId) ||
      (nextProps.name !== this.props.name) ||
      (nextProps.isUploaded !== this.props.isUploaded)) {
      await this.getDocInfo(nextProps.docId)
    }
  }

  async getDocInfo(_id) {
    const { platForm } = this.props
    let res = {}
    if(platForm){
      res = await getPlatFormDocDetails(_id)
    } else {
      res = await getDocDetails(_id)
    }
    console.log("res : ", res)
    const [error, doc] = res
    if(error) {
      this.setState({ error, waiting: false })
    } else {
      let err = ""
      let objectName = ""
      const { contextId, contextType, tenantId, name, meta } = doc
      const matchVersion = meta.versions.find(vr => vr.name === doc.name) || {}
      const historical = matchVersion && matchVersion.isHistorical || false
      if(platForm && tenantId && contextType && name && contextType === "INVOICES") {
        objectName = `${contextType}/${name}`
      } else if(historical) {
        objectName = matchVersion && matchVersion.path
      } else if(tenantId && contextId && contextType && name) {
        objectName = `${tenantId}/${contextType}/${contextType}_${contextId}/${name}`
      } else {
        err = "invalid doc details"
      }
      this.setState({ fileName: doc.originalName, waiting: false, error: err, objectName })
    }
  }

  render() {
    const { fileName, waiting, error, objectName, bucketName } = this.state
    const { platForm } = this.props

    return (
      <div className="multiExpTblVal">

        { fileName && platForm ?

          <a
            className="fa fa-download"
            onClick={() => getPlatFormFile(`?bucketName=${bucketName}&objectName=${encodeURIComponent(objectName)}`, fileName)}
            title="download"
          /> :
          <a
            onClick={() => getFile(`?objectName=${encodeURIComponent(objectName)}`, fileName)}
            download={fileName}>{fileName}</a>
        }

        {(waiting || !fileName) && <span><Loader /></span>}
        {error && <strong>{error}</strong>}
      </div>
    )
  }
}

export default DocLink
