import React, { Component } from "react"
import axios from "axios"
import swal from "sweetalert"
import { getDocDetails, getToken, getFile } from "GlobalUtils/helpers"
import { muniApiBaseURL } from "GlobalUtils/consts"
import { Modal } from "Global/BulmaModal"
import Loader from "Global/Loader"

class DocModalDetails extends Component {
  constructor(props) {
    super(props)
    this.state = { modalState: this.props.showModal, error: "", doc: {},
      waiting: true, objectName: "", versionId: "", selectedVersions: [], downloadDocName: "", selectAll: false }
  }

  async componentDidMount() {
    await this.getDocInfo(this.props.docId)
  }

  async componentWillReceiveProps(nextProps) {
    if(nextProps.docId !== this.props.docId) {
      await this.getDocInfo(nextProps.docId)
    }
  }

  onClickDownload(versionId, name, historical, path) {
    console.log("in onClickDownload : ", versionId, name)
    const { doc } = this.state
    const { contextId, contextType, tenantId } = doc
    let objectName = ""
    if(historical){
      objectName = path
    } else {
      objectName = `${tenantId}/${contextType}/${contextType}_${contextId}/${name}`
    }
    this.setState({ versionId, objectName, downloadDocName: name }, this.downloadFile)
  }

  getUploadDate(v) {
    const { uploadDate, name } = v
    if(uploadDate && new Date(uploadDate).toLocaleString() !== "Invalid Date") {
      return new Date(uploadDate)
    }
    const extnIdx = name.lastIndexOf(".")
    const name1 = name.substr(0, extnIdx)
    const idx1 = name1.lastIndexOf("_")
    const timestamp = name1.substr(idx1+1)
    if(timestamp) {
      return new Date(+timestamp)
    }
    return ""
  }

  async getDocInfo(_id) {
    if(!_id) {
      _id = this.props.docId
    }
    const res = await getDocDetails(_id)
    console.log("res : ", res)
    const [error, doc] = res
    if(error) {
      this.setState({ waiting: false, error })
    } else {
      const { contextId, contextType, tenantId, name } = doc
      const objectName = `${tenantId}/${contextType}/${contextType}_${contextId}/${name}`
      this.setState({ doc, error: "", objectName, waiting: false, fileName: name })
    }
  }

  downloadFile() {
    this.downloadAnchor.click()
  }

  deleteDocVersion = () => {
    const { doc, selectedVersions } = this.state
    const { contextId, contextType, tenantId } = doc

    swal({
      title: "Are you sure?",
      text: "You want to delete selected versions ?",
      buttons: true,
      dangerMode: true
    }).then(willDelete => {
      if(willDelete){
        const { bucketName } = this.props
        if((!doc && !doc.name) || !selectedVersions.length) {
          this.setState({ error: "No bucket/file name/versionId provided", selectedVersions: [] })
          return
        }

        this.setState({
          waiting: true
        }, () => {
          const files = []
          selectedVersions.forEach(ver => files.push({ Key: `${tenantId}/${contextType}/${contextType}_${contextId}/${ver.name}`, VersionId: ver.id }))

          /* let files
          if(versionId === "All") {
            files = doc.meta.versions.map(e => ({
              Key: `${tenantId}/${contextType}/${contextType}_${contextId}/${e.name}`,
              VersionId: e.versionId
            }))
          } else {
            files = [{ Key: objectName, VersionId: versionId }]
          } */

          axios.post(`${muniApiBaseURL}docs/update-versions`, { _id: doc._id,
            versions: selectedVersions.map(v => v.id), option: "remove"},{headers:{"Authorization":getToken()}})
            .then(res => {
              console.log("version removed ok : ", res)
              this.setState({ error: "", selectedVersions: [], waiting: false }, this.getDocInfo)
            }).catch(err => {
              console.log("err in deleting version : ", err)
              this.setState({ error: "err in deleting version", selectedVersions: [] })
            })

          /* axios.post(`${muniApiBaseURL}s3/delete-s3-object`, { bucketName, files }, {headers:{"Authorization":getToken()}})
            .then(res => {
              console.log("res : ", res)
              if(res.error) {
                const error = "Error in deleting"
                this.setState({ error, selectedVersions: [] })
              } else if(doc.meta.versions.length > 1 /!* && versionId !== "All" *!/) {
                axios.post(`${muniApiBaseURL}docs/update-versions`, { _id: doc._id,
                  versions: selectedVersions.map(v => v.id), option: "remove"},{headers:{"Authorization":getToken()}})
                  .then(res => {
                    console.log("version removed ok : ", res)
                    this.setState({ error: "", selectedVersions: [], waiting: false }, this.getDocInfo)
                  }).catch(err => {
                    console.log("err in deleting version : ", err)
                    this.setState({ error: "err in deleting version", selectedVersions: [] })
                  })
              }
            }).catch(err => {
              console.log("err : ", err)
              this.setState({ error: "Error!!", selectedVersions: [], waiting: false })
            }) */
        })
      }
    })
  }

  toggleModal = () => {
    this.setState((prev) => {
      const newState = !prev.modalState
      return { modalState: newState }
    }, this.props.closeDocDetails)
  }

  selectVersion = (event, version) => {
    const { selectedVersions } = this.state
    const {checked} = event.target
    if(checked){
      selectedVersions.push({id: version.versionId, name: version.name})
    }else {
      const findIndex = selectedVersions.findIndex(v => v.id === version.versionId)
      if(findIndex !== -1){
        selectedVersions.splice(findIndex, 1)
      }
    }
    console.log(selectedVersions)
    this.setState({
      selectedVersions
    })

  }

  selectAllVersions = () => {
    const { doc, selectAll } = this.state
    let docVersions = [ ...doc.meta.versions ]
    docVersions = docVersions.filter(f => !f.isHistorical)
    docVersions.sort((a, b) => {
      const date1 = this.getUploadDate(a)
      const date2 = this.getUploadDate(b)
      return (new Date(date2) - new Date(date1))
    })
    this.setState({
      selectedVersions: !selectAll ? docVersions.slice(1).map(v => ({id: v.versionId, name: v.name})) : [],
      selectAll: !selectAll
    })
  }

  render() {
    const { waiting, error, doc, versionId, fileName, objectName, selectedVersions, selectAll, downloadDocName } = this.state
    const { docMetaToShow, versionMetaToShow } = this.props
    const hideContextType = ["CONTROLS", "SVOBLIGATIONS", "SVPROFQUALIFICATIONS", "SVGENADMIN", "SVCLIENTCOMPLAINTS", "SVBUSINESSCONDUCT",
      "POLICIESPROCEDURES", "msrbSubmissions", "SVPOLICONTRELATEDDOCS", "SVGIFTSGRATUITIES", "ENTITYCONTRACT", "ENTITYLOGO"]

    if(waiting) {
      return (
        <Modal closeModal={this.toggleModal}
          modalState={this.state.modalState}
          title={doc.name}>
          <Loader />
        </Modal>
      )
    }
    if(doc && doc.name) {
      const docVersions = [ ...doc.meta.versions ]
      let activeDocVersions = (docVersions && docVersions.length) ? docVersions.filter(d=> !d.isInActive) : []
      activeDocVersions = activeDocVersions.filter(f => !f.isHistorical)
      docVersions.sort((a, b) => {
        const date1 = this.getUploadDate(a)
        const date2 = this.getUploadDate(b)
        return (new Date(date2) - new Date(date1))
      })
      return (
        <Modal
          closeModal={this.toggleModal}
          modalState={this.state.modalState}
          title={doc.originalName}>
          <div className="columns">
            <div className="column">
              {docMetaToShow && docMetaToShow.length ?
                docMetaToShow.map((e, i) => {
                  if(doc.meta && doc.meta[e]) {
                    return (
                      <div className="word-break-space" key={i.toString()}>
                        <strong>{` ${e.toUpperCase()} : `}</strong>
                        {doc.meta[e]}
                      </div>
                    )
                  }
                  return null
                })
                : undefined
              }
              <div><strong>Total versions: </strong>{docVersions.length}</div>
            </div>
            {
              ((hideContextType.indexOf(doc && doc.contextType) === -1) && (selectedVersions.length || (activeDocVersions && activeDocVersions.length > 1))) ?
                <div className="column has-text-right">
                  <div className="field is-grouped">
                    {
                      selectedVersions.length ?
                        <a className="button is-link is-small" onClick={this.deleteDocVersion}> {/* eslint-disable-line */}
                          <span><i className="far fa-trash-alt"/></span>
                        </a>
                        : null
                    }
                    {
                      (activeDocVersions && activeDocVersions.length > 1) ?
                        <button className="button is-light is-small" onClick={this.selectAllVersions}>
                          { selectAll ? "Unselect All" : "Select All" }
                        </button>
                        :null
                    }
                  </div>
                </div>
                : null
            }
          </div>

          {doc.status &&
          <div>
            <span><strong>status : </strong></span>
            <span>{doc.status}</span>
          </div>
          }
          <br />

          <div>
            {docVersions.map((v, i) => {
              const uploadDate = this.getUploadDate(v)
              if(v.isInActive) return null
              return (
                <div className="card"  key={i.toString()}>
                  <div className="card-content">
                    <div className="content">
                      <div>
                        <div className="word-break-space"><strong>version : </strong><span>{v.versionId}</span></div>
                        <div className="word-break-space"><strong>Name : </strong><span>{v.originalName}</span></div>
                        <div className="word-break-space"><strong>size : </strong><span>{v.size}</span></div>
                        {
                          uploadDate &&
                          <div>
                            <span><strong>upload date : </strong></span>
                            <span>{this.getUploadDate(v).toLocaleString()}</span>
                          </div>
                        }
                        {versionMetaToShow && versionMetaToShow.length ?
                          <div>
                            {versionMetaToShow.map((e, i) => {
                              if(v && v[e]) {
                                return (
                                  <span key={i.toString()}>
                                    <strong>{` ${e} : `}</strong>
                                    {v[e]}
                                  </span>
                                )
                              }
                              return null
                            })}
                          </div>
                          : undefined
                        }
                        <div className="field is-grouped">
                          <span>
                            <strong>Actions : </strong>
                            <a className="icon has-text-info"> {/*eslint-disable-line*/}
                              <i className="fa fa-download"
                                onClick={this.onClickDownload.bind(this, v.versionId, v.name, v.isHistorical, v.path)} title="Download" /></a>
                            {
                              ((hideContextType.indexOf(doc && doc.contextType) !== -1) || v.isHistorical) ? null :
                                <span>
                                  <input
                                    className="checkbox"
                                    type="checkbox"
                                    checked={selectedVersions.findIndex(ver => ver.id === v.versionId) !== -1}
                                    disabled={i === 0}
                                    onChange={(event) => this.selectVersion(event, v)}
                                  />
                                </span>
                            }
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              )
            })
            }
            {error ?
              <p className="bg-danger error">{error}</p>
              : undefined
            }
          </div>
          <a className="hidden-download-anchor" // eslint-disable-line
            style={{ display: "none" }}
            ref={ el => { this.downloadAnchor = el } }
            onClick={() => getFile(`?objectName=${objectName}&versionId=${versionId}`, downloadDocName)}
            download={downloadDocName} >
            hidden
          </a>
        </Modal>
      )
    }
    return null
  }
}

export default DocModalDetails
