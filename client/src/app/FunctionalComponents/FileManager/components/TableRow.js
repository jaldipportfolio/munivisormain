import React from "react"
import styled from "styled-components"
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu"

// Helper Functions
import {getFileIcon} from "../utils/fileManagerHelpers"
import { frontedURL } from "../../../../globalutilities/consts"

const tdStyle = {
  whiteSpace: "nowrap",
  width: "25%",
  cursor: "pointer",
  overflow: "hidden",
  textOverflow: "ellipsis"
}

const Content = styled.div`
  padding: 10px;
  font-size: small;
`

const contextMenu = (props) => (
  <ContextMenu id={`list_view_manager${props.index}${props.name}menu`}>
    <MenuItem disabled={props.type !== "file"} onClick={() => {window.open(`https://docs.google.com/gview?url=${frontedURL}s3/get-s3-object-as-stream?objectName=${encodeURIComponent(props.rootPath)}`)}}>
      <i className="fas fa-eye"/>&nbsp;
      Preview
    </MenuItem>
    <MenuItem onClick={props.onViewProperTies} disabled={props.type !== "file"}>
      &nbsp;<i className="fas fa-info"/>&nbsp;&nbsp;&nbsp;
      Properties
    </MenuItem>
    <MenuItem onClick={props.openFile} disabled={props.type !== "file"}>
      <i className="fas fa-arrow-down"/>&nbsp;&nbsp;
      Download
    </MenuItem>
    <MenuItem onClick={() => props.onAttachDoc(props, "Entities")}>
      <i className="fas fa-link"/>&nbsp;&nbsp;
      Attach With Entities
    </MenuItem>
    <MenuItem onClick={() => props.onAttachDoc(props, "Transactions")}>
      <i className="fas fa-link"/>&nbsp;&nbsp;
      Attach With Transactions
    </MenuItem>
  </ContextMenu>
)

const TableRow = props => {
  const file = getFileIcon(props.name)
  return (
    <tr style={props.selectedFiles.indexOf(props.rootPath) !== -1 ? { backgroundColor: "#d5ebff" } : {}}>
      <td>
        <div className="control" style={{width: "50%"}} onClick={props.onSelect}>
          <label className="checkbox-button" > {/* eslint-disable-line */}
            <input type="checkbox" name="tabs" value={props.rootPath} checked={props.selectedFiles.indexOf(props.rootPath) !== -1} onChange={() => {}} />
            <span className="checkmark" />
          </label>
        </div>
      </td>

      { props.type === "file" ?
        <td style={tdStyle} title={props.name} className="has-text-left">
          <ContextMenuTrigger id={`list_view_manager${props.index}${props.name}menu`} holdToDisplay={1000} className="has-text-left">
            <i className={`far ${file.icon}`} style={file.style}/>
            &nbsp;&nbsp;{props.name}
          </ContextMenuTrigger>
          {contextMenu(props)}
        </td>
        :
        <td style={tdStyle} title={props.name} className="has-text-left">
          <div onClick={props.openFolder} className="has-text-left">
            <ContextMenuTrigger id={`list_view_manager${props.index}${props.name}menu`} holdToDisplay={1000} className="has-text-left">
              <i className="fa fa-folder" aria-hidden="true" style={{color: "#ffb300"}} />
              &nbsp;&nbsp;{props.name}
            </ContextMenuTrigger>
          </div>
          {contextMenu(props)}
        </td>
      }

      <td className="has-text-left">{props.type}</td>
      <td className="has-text-left">
        {props.meta.size}
      </td>
      <td className="has-text-left">
        <Content>
          <p
            dangerouslySetInnerHTML={{
              __html: `${props.doc_preview}...`
            }}
            style={{
              overflowX: "hidden",
              textOverflow: "ellipsis"
            }}
          />
        </Content>
      </td>
    </tr>
  )
}

export default TableRow
