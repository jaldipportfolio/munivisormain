import React from "react"
import styled from "styled-components"
import ReactTooltip from "react-tooltip"
import { ContextMenu, MenuItem, ContextMenuTrigger } from "react-contextmenu"
import {getFileIcon} from "../utils/fileManagerHelpers"
import { frontedURL } from "../../../../globalutilities/consts"

const Icon = styled.i`
 display: block
`

const Card = props => {
  const file = getFileIcon(props.name)
  return (
    <div className="item text-wrap" style={props.selectedFiles && props.selectedFiles.indexOf(props.rootPath) !== -1 ? { cursor: "pointer", backgroundColor: "#d5ebff" } : { cursor: "pointer" }}>
      {props.type === "file" ?
        <div className="control" style={{width: "50%"}}
          onClick={props.type === "file" ? (e) => props.onSelect(e) : () => {
          }}>
          <label className="checkbox-button"> {/* eslint-disable-line */}
            <input type="checkbox" name="tabs" value={props.rootPath}
              checked={props.selectedFiles.indexOf(props.rootPath) !== -1} onClick={() => {
              }} onChange={() => {
              }}/>
            <span className="checkmark"/>
          </label>
        </div>
        : null
      }
      <ContextMenuTrigger id={`manager${props.index}${props.name}menu`} holdToDisplay={1000}>
        <div className="item__thumbnail">
          {props.type === "file" ? <Icon
            className={`far ${file.icon} fa-3x`}
            style={file.style}
          /> :
            <i className="fa fa-folder fa-4x" aria-hidden="true" style={{color: "#ffb300", cursor: "pointer"}}
              onClick={props.openFolder}/>}
        </div>
        <p className="item__name overflow" style={{fontSize: 12}} data-tip
          data-for={`manager${props.index}${props.name}`}>{props.name}</p>
        <ReactTooltip
          id={`manager${props.index}${props.name}`}
          className="custom-tooltip"
          place="bottom"
        >
          <p>Name: {props.name} </p>
          <p>Type: {props.type}</p>
          {props.type === "file" ? <p>Size: {props.meta.size || ""}</p> : null}
          {/* { props.type === "file" ? <p>Last modified Date: {(props.lastModified && moment(props.lastModified).format("MM/DD/YYYY hh:mm A")) || ""}</p> : null } */}
        </ReactTooltip>
      </ContextMenuTrigger>
      <ContextMenu id={`manager${props.index}${props.name}menu`}>
        <MenuItem disabled={props.type !== "file"} onClick={() => {window.open(`https://docs.google.com/gview?url=${frontedURL}s3/get-s3-object-as-stream?objectName=${encodeURIComponent(props.rootPath)}`)}}>
          <i className="fas fa-eye"/>&nbsp;&nbsp;
          Preview
        </MenuItem>
        <MenuItem onClick={props.onViewProperTies} disabled={props.type !== "file"}>
          &nbsp;<i className="fas fa-info"/>&nbsp;&nbsp;&nbsp;
          Properties
        </MenuItem>
        <MenuItem onClick={props.openFile} disabled={props.type !== "file"}>
          <i className="fas fa-arrow-down"/>&nbsp;&nbsp;
          Download
        </MenuItem>
        <MenuItem onClick={(item) => props.onAttachDocToEnt(item, "Entities")}>
          <i className="fas fa-link"/>&nbsp;&nbsp;
          Attach With Entities
        </MenuItem>
        <MenuItem onClick={(item) => props.onAttachDocToTran(item, "Transactions")}>
          <i className="fas fa-link"/>&nbsp;&nbsp;
          Attach With Transactions
        </MenuItem>
      </ContextMenu>
    </div>
  )
}
export default Card
