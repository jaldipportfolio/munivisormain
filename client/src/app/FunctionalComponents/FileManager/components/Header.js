import React from "react"

export const Header = ({onSelect, style, customStyles, node}) => {
  const select = style.currentDirectory === node.path ? { backgroundColor: "rgb(210, 225, 251)", borderRadius: 10, color: "cornflowerblue" } : ""
  return(
    <div style={style.base} onClick={onSelect}>
      <div className="type" style={node.selected ? {...style.title, ...customStyles.header.title } : {...style.title, ...select }} title={node.name}>
        {node.name}
      </div>
    </div>
  )
}
