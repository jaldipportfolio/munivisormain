import React, { Component } from "react"
import { Treebeard, decorators } from "react-treebeard"
import { getDefaultQuery, isFile, searchAPI, sizes } from "../utils/fileManagerHelpers"
import { Header } from "../components/Header"
// import { Tree } from "./Tree"

// const constructChildParentsRel = (data = []) => {
//   const clonedData = cloneDeep(data)
//   if (!clonedData.length) return []
//   let level = 0
//   const newData = [{ name: clonedData[0], parent: "root", level }]
//   level++
//   let currParent = clonedData[0]
//   clonedData.shift()
//   clonedData.forEach(dataItem => {
//     newData.push({ name: dataItem, parent: currParent, level })
//     if (!isFile(dataItem)) {
//       currParent = dataItem
//       level++
//     }
//   })
//   return newData
// }

// const buildTree = data => {
//   const finalTreeData = []
//   data.forEach(nestedData => {
//     const tree = new Tree()
//     constructChildParentsRel(nestedData).forEach(nestedDataItem => {
//       tree.add(nestedDataItem)
//     })
//     finalTreeData.push(tree.root)
//   })
//   return finalTreeData
// }

// const getTreeData = aggData => {
//   const data = flatAggregations(aggData)
//   const root = { name: "root", toggle: true, children: [], level: 0 }
//   const treeStruct = JSON.parse(JSON.stringify(buildTree(data)))
//   treeStruct.forEach(data => root.children.push(data))
//   return root
// }

// const flatAggregations = aggregations => {
//   if (!aggregations) return []
//   function flat(array = [], level = 2) {
//     let result = []
//     const keyWord = `path_level_${level}.keyword`
//     array.forEach(a => {
//       result.push(a.key)
//       if (Array.isArray(get(a, [keyWord, "buckets"]))) {
//         result = result.concat(flat(get(a, [keyWord, "buckets"]), level + 1))
//       }
//     })
//     return result
//   }

//   const flatTreeData = []
//   aggregations["path_level_0.keyword"].buckets.forEach(agg => {
//     flatTreeData.push([
//       agg.key,
//       ...flat(get(agg, ["path_level_1.keyword", "buckets"]))
//     ])
//   })
//   return flatTreeData
// }

const getMustQuery = node => {
  const { path } = node
  return path.split("/").map((item, i) => ({
    terms: {
      [`path_level_${i}.keyword`]: [item]
    }
  }))
}

const createChildren = (aggs, node) => {
  const { level } = node
  if (level === 0 && aggs && aggs["path_level_0.keyword"] && aggs["path_level_0.keyword"].buckets) {
    const folders = aggs["path_level_0.keyword"].buckets.filter(
      item => !isFile(item.key, item, 0)
    )
    return folders.map(bucket => ({
      name: bucket.key,
      children: isFile(bucket.key, bucket, 0) ? null : [],
      level: level + 1,
      path: bucket.key
    }))
  }
  const paths = (node && node.path && node.path.split("/")) || []
  let iterLevel = 0
  let currBucket = aggs && aggs[`path_level_${iterLevel}.keyword`] && aggs[`path_level_${iterLevel}.keyword`].buckets && aggs[`path_level_${iterLevel}.keyword`].buckets.find(
    buck => buck.key === paths[0] && !isFile(buck.key, buck, iterLevel)
  )
  iterLevel++
  while (iterLevel <= level - 1) {
    currBucket = currBucket[`path_level_${iterLevel}.keyword`].buckets.find(
      buck => buck.key === paths[iterLevel] && !isFile(buck.key, buck, iterLevel)
    )
    iterLevel++
  }
  const folders = currBucket && currBucket[`path_level_${level}.keyword`] && currBucket[`path_level_${level}.keyword`].buckets && currBucket[`path_level_${level}.keyword`].buckets.filter(
    item => !isFile(item.key, item, level)
  )
  return (folders && folders.map(bucket => ({
    name: bucket.key,
    children: isFile(bucket.key, bucket, level) ? null : [],
    level: level + 1,
    path: `${node.path}/${bucket.key}`
  }))) || []
}

class TreeBeardRender extends Component {
  state = {
    data: [],
    cursor: null
  }

  async componentDidMount() {
    this.setState({ loading: true })
    const { response } = await searchAPI(
      getDefaultQuery(0),
      this.props.tenantId
    )
    if (response && response.status >= 400) {
      throw new Error("Bad response from server")
    }
    const jsonData = await response.json()
    this.setState({
      data: createChildren(jsonData.responses[0].aggregations, { level: 0 }),
      loading: false
    })
  }

  onToggle = async (node, toggled) => {
    const { cursor } = this.state
    console.log(node, toggled)
    if (cursor) cursor.active = false
    node.active = true
    if (node.children) {
      node.toggled = toggled
      node.loading = true
    }
    this.setState({ cursor: node })
    const propState = {
      currentDirectory: node.name,
      currentLevel: `path_level_${node.level - 1}`,
      query: { bool: { must: getMustQuery(node) } }
    }
    if (!node.toggled) {
      if (node.level - 1 === 0) {
        propState.currentDirectory = ""
        propState.query = { bool: { must: [{ match_all: {} }] } }
      }
      this.props.handleTreeViewChange(propState)
      return
    }
    const query = getDefaultQuery(node.level)
    const { response } = await searchAPI(query, this.props.tenantId)
    if (response && response.status >= 400) {
      throw new Error("Bad response from server")
    }
    node.loading = false
    const jsonData = await response.json()
    node.children = createChildren(jsonData.responses[0].aggregations, node)
    console.log("node", node, propState)

    this.setState({ cursor: node })
    this.props.handleTreeViewChange(propState)
  }

  render() {
    if (this.state.loading)
      return (
        <div style={{ color: "#E2C089" }}>
          loading...
        </div>
      )
    return (
      <Treebeard
        data={this.state.data}
        onToggle={this.onToggle}
        decorators={{...decorators, Header}}
        style={{
          tree: {
            base: {
              listStyle: "none",
              backgroundColor: "rgb(245, 246, 245)",
              border: "1px solid rgb(226, 228, 226)",
              margin: 0,
              padding: 10,
              color: "#9DA5AB",
              fontSize: "14px",
              display: window.innerWidth <= sizes.phone ? "none" : "block",
              width:  window.innerWidth >= sizes.desktop ? "15%" : "30%",
              overflowY: "auto",
              height: "75vh"
            },
            node: {
              base: {
                position: "relative",
                textOverflow: "ellipsis",
                whiteSpace: "nowrap",
                // padding: "5px 0",
                overflow: "hidden",
                fontFamily: "GoogleSans, Gravity !important",
                colors: { textColor: "rgba(10, 10, 10, 0.6)" }
              },
              link: {
                cursor: "pointer",
                position: "relative",
                padding: "0px 5px",
                display: "block"
              },
              activeLink: { fontWeight: "bolder" },
              toggle: {
                base: {
                  position: "relative",
                  display: "inline-block",
                  verticalAlign: "top",
                  marginLeft: "-5px",
                  height: "24px",
                  width: "24px"
                },
                wrapper: {
                  position: "absolute",
                  top: "50%",
                  left: "50%",
                  margin: "-7px 0 0 -7px",
                  height: "14px"
                },
                height: 14,
                width: 14,
                arrow: { fill: "#9DA5AB", strokeWidth: 0 }
              },
              header: {
                base: {
                  display: "inline-block",
                  verticalAlign: "top",
                  color: "#9DA5AB",
                  width: "80%"
                },
                connector: {
                  width: "2px",
                  height: "12px",
                  borderLeft: "solid 2px black",
                  borderBottom: "solid 2px black",
                  position: "absolute",
                  top: "0px",
                  left: "-21px"
                },
                title: {
                  lineHeight: "20px",
                  verticalAlign: "middle",
                  overflow: "hidden",
                  textOverflow: "ellipsis",
                  whiteSpace: "nowrap",
                  paddingLeft: "5px",
                  fontWeight: "normal",
                  padding: "3px 14px"
                },
                currentDirectory: this.props.currentDirectory
              },
              subtree: { listStyle: "none", paddingLeft: "19px" },
              loading: { color: "#E2C089" }
            }
          }
        }}
      />
    )
  }
}

export default TreeBeardRender
