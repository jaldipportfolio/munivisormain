import React from "react"
import styled from "styled-components"

import { getSearchableFields, getWeights, searchFields } from "../searchConfig"

import StyledSearch from "../../../Search/Layout/StyledSearchBar"
import Flex from "../../../Search/Layout/Flex"
import {getFileIcon} from "../../utils/fileManagerHelpers"

const getfields = () => {
  const fields = getSearchableFields()
  const weights = getWeights()
  return fields.map((f, i) => `${f}^${weights[i]}`)
}

const getHighlightFields = (fields = searchFields) =>
  fields.reduce(
    (obj, f) => ({
      ...obj,
      [f]: {},
      [`${f}.search`]: {}
    }),
    {}
  )


const Suggestions = styled.div`
  position: absolute;
  color: #424242;
  font-size: 16px !important;
  border: 1px solid #ddd;
  background: white;
  border-radius: 2;
  margin-top: 0;
  width: 100%;
  z-index: 10;
  max-height: 300px;
  overflow: auto;
`

const Icon = styled.i`
 display: block
`

const RenderSuggestion = ({ suggestion, getItemProps, isActive }) => {
  const source = suggestion._source || {}
  const nestedPath = source.s3Key.split("/")
  const name = nestedPath[nestedPath.length-1]
  nestedPath.splice(nestedPath.length-1, nestedPath.length)
  nestedPath.splice(0, 2)
  const pathLevels =  {
    path_level_0: name
  }
  nestedPath.forEach((lvl,i) => {
    pathLevels[`path_level_${i}`] = lvl
  })
  nestedPath.unshift("Root")
  const location = nestedPath.join("/")
  suggestion._source.location = location
  suggestion.highlight = location
  const file = getFileIcon(source.meta.originalName)
  return (
    <div
      {...getItemProps({
        item: { value: name, source }
      })}
      style={{
        background: isActive ? "#eee" : "transparent",
        borderBottom: "1px solid #dfdfdf",
        cursor: "pointer"
      }}
    >
      <Flex wrap="no-wrap" justifyContent="space-between" alignItems="center"
        style={{
          padding: 10,
          paddingBottom: 10,
        }}>

        <small style={{width: 40, paddingRight: 10}}>
          <span>
            <b>
              <Icon
                className={`far ${file.icon} fa-3x`}
                style={file.style}
              />
            </b>
          </span>
        </small>

        <div style={{ minWidth: "97%" }}>
          <p>
            <i>Content: </i>
            <small
              dangerouslySetInnerHTML={{
                __html:
                suggestion.highlight &&
                suggestion.highlight.doc_preview
                  ? _.get(suggestion.highlight, "doc_preview")
                  : _.get(suggestion._source, "doc_preview")
              }}
            />
          </p>
          <p>
            <i>File Name: </i>
            <span
              dangerouslySetInnerHTML={{
                __html:
                suggestion.highlight &&
                suggestion.highlight["meta.originalName"]
                  ? _.get(suggestion.highlight, "meta.originalName")
                  : _.get(suggestion._source, "meta.originalName")
              }}
            />
          </p>
          <p>
            <i>Location: </i>
            <b
              dangerouslySetInnerHTML={{
                __html:
                suggestion.highlight && suggestion.highlight.category
                  ? _.get(suggestion.highlight, "location")
                  : _.get(suggestion._source, "location")
              }}
            />
          </p>
        </div>
      </Flex>
    </div>
  )
}

class SearchBar extends React.Component {
  state = {
    isBlured: true
  }

  handleBlurChange = isBlured => {
    this.setState({
      isBlured
    })
  }

  render() {
    const {onLevelChange} = this.props
    const {isBlured} = this.state
    return (
      <StyledSearch
        componentId="Search"
        dataField={[
          "doc_preview",
          "meta.originalName.keyword",
          "meta.originalName.search"
        ]}
        customQuery={value => {
          if (value) {
            return {
              query: {
                bool: {
                  should: [
                    {
                      match_phrase: {
                        doc_content: value
                      }
                    },
                    {
                      match_phrase: {
                        "doc_content.search": value
                      }
                    },
                    {
                      multi_match: {
                        query: value,
                        fields: [...getfields()]
                      }
                    }
                  ]
                }
              },
              _source: {
                excludes: ["doc_content"]
              },
              highlight: {
                pre_tags: ["<mark>"],
                post_tags: ["</mark>"],
                order: "score",
                fields: {
                  doc_content: {
                    fragment_size: 300,
                    matched_fields: ["doc_content"],
                    type: "fvh"
                  },
                  "doc_content.search": {
                    fragment_size: 300,
                    matched_fields: ["doc_content.search"],
                    type: "fvh"
                  },
                  ...getHighlightFields()
                }
              }
            }
          }
          return {}
        }}
        highlight
        debounce={300}
        react={{
          and: []
        }}
        onValueSelected={(value, cause, source) => {
          if(source && source.s3Key){
            const nestedPath = source.s3Key.split("/")
            const name = nestedPath[nestedPath.length-1]
            nestedPath.splice(nestedPath.length-1, nestedPath.length)
            nestedPath.splice(0, 2)
            const directory = nestedPath[nestedPath.length-1]
            const pathLevels =  {
              path_level_0: name
            }
            nestedPath.forEach((lvl,i) => {
              pathLevels[`path_level_${i}`] = lvl
            })
            const path = nestedPath.join("/")
            onLevelChange({ name: directory, directory, path, ...source})
            // console.log({ name: directory, directory, path, ...source})
            this.handleBlurChange(true)
          }
        }}
        onFocus={() => {
          this.handleBlurChange(false)
        }}
        onBlur={() => {
          this.handleBlurChange(true)
        }}
        renderAllSuggestions={({
          currentValue,
          suggestions,
          getItemProps,
          highlightedIndex
        }) =>
          suggestions && currentValue &&
          Boolean(suggestions.length) &&
          Boolean(currentValue.length) &&
          !isBlured && (
            <Suggestions>
              {suggestions.map((item, index) => (
                <RenderSuggestion
                  suggestion={item}
                  getItemProps={getItemProps}
                  currentValue={currentValue}
                  key={item._id}
                  isActive={highlightedIndex === index}
                />
              ))}
            </Suggestions>
          )
        }
      />
    )
  }
}

export default SearchBar
