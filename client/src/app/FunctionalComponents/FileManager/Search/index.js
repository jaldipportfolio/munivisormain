/* eslint-disable no-underscore-dangle, indent */
import React from "react"
import { ReactiveBase, ReactiveComponent } from "@appbaseio/reactivesearch"
import * as qs from "query-string"

import { ELASTIC_SEARCH_URL } from "../../../../constants"
import Container from "../../Search/Layout/Container"
import Flex from "../../Search/Layout/Flex"
import Loader from "../../../GlobalComponents/Loader"
import Accordion from "../../../GlobalComponents/Accordion"
import RatingSection from "../../../GlobalComponents/RatingSection"
import FileManager from ".."
import SearchBar from "./SearchBar"
import {
  getNestedLevels,
  initQuery,
  levelQuery,
  aggregationQuery, sizes
} from "../utils/fileManagerHelpers"
import TreeView from "../components/TreeView"

const pathLevels = getNestedLevels()

const RenderBrowser = props => {
  const {
    aggregations,
    keyword,
    level,
    hits,
    isLoading,
    onLevelChange,
    currentDirectory,
    selectedFiles
  } = props
  const levelList =
    (aggregations && aggregations[keyword] && aggregations[keyword].buckets) ||
    []
  const results = []
  return (
    <div style={{ width: "100%" }}>
      {isLoading ? (
        <Loader />
      ) : (
        <FileManager
          selectedFiles={selectedFiles}
          results={results}
          view={props.view}
          {...props}
          levelList={levelList}
          level={level}
          data={hits}
          currentDirectory={currentDirectory}
          onLevelChange={onLevelChange}
        />
      )}
    </div>
  )
}

class Search extends React.Component {
  state = {
    currentLevel: "path_level_0",
    currentDirectory: "",
    query: initQuery(),
    view: "grid",
    selectedPath: "",
    tenantId: "",
    selectedFiles: []
  }

  componentWillMount() {
    const parsed = qs.parse(this.props.location.search)
    const levels = {}
    if (parsed.route) {
      const spiltedRoute = parsed.route.split("/")
      spiltedRoute.forEach((item, index) => {
        levels[`path_level_${index}`] = item
        levels.name = spiltedRoute.length === index + 1 ? item : ""
      })
      this.onLevelChange(levels)
    }
    const { loginDetails } = this.props
    let tenantId = ""
    if (
      loginDetails &&
      loginDetails.relatedFaEntities &&
      loginDetails.relatedFaEntities.length
    ) {
      tenantId = loginDetails.relatedFaEntities[0].entityId || ""
    }
    const view = localStorage.getItem("fileBrowserView") || this.state.view
    this.setState({ tenantId, view })
  }

  onLevelChange = (item, callFromSearch) => {
    const selectedFiles = []
    const levels = {}
    pathLevels.forEach(lvl => {
      if (item[lvl]) {
        levels[lvl] = item[lvl]
      }
    })
    const levelIndex = Object.values(levels).indexOf(item.name)
    const level = Object.keys(levels)[levelIndex]
    const pathQuery = []
    Object.values(levels).forEach((name, index) => {
      if (levelIndex >= index) {
        const level = Object.keys(levels)[index]
        pathQuery.push({ terms: { [`${level}.keyword`]: [name] } })
      }
    })
    if(item && item.s3Key && callFromSearch){
      selectedFiles.push(item.s3Key)
    }
    this.setState({
      currentLevel: level || "path_level_0",
      currentDirectory: item.name === "Root" ? "" : item.name,
      query: level ? levelQuery(level, item, pathQuery) : initQuery(),
      selectedFiles
    })
  }

  handleTreeViewChange = data => {
    this.setState(data)
    if(data && data.query){
      const path = []
      data.query.bool.must.forEach((m, i) => {
        if(m && m.terms){
          const pathLevel = `path_level_${i}.keyword`
          path.push(m.terms[pathLevel][0])
        }
      })
      this.setState({selectedPath: path.join("/")})
    }
  }

  onViewChange = view => {
    localStorage.setItem("fileBrowserView", view)
    this.setState({ view })
  }

  render() {
    const { currentDirectory, currentLevel, query, view, tenantId, selectedPath, selectedFiles } = this.state
    return (
      <div id="main">
        <Accordion
          multiple
          activeItem={[0]}
          boxHidden
          render={({ activeAccordions, onAccordion }) => (
            <RatingSection
              onAccordion={() => onAccordion(0)}
              title="File Browser"
            >
              {activeAccordions.includes(0) && (
                <ReactiveBase
                  url={ELASTIC_SEARCH_URL}
                  app={`tenant_docs_historical_${tenantId}`}
                  theme={{
                    typography: { fontFamily: "GoogleSans, Gravity !important" },
                    colors: { textColor: "rgba(10, 10, 10, 0.6)" }
                  }}
                  type="doc"
                  headers={{ Authorization: localStorage.getItem("token") }}
                >
                  <Container>
                    <SearchBar onLevelChange={(item) => this.onLevelChange(item, true)} />
                    <Flex style={{ padding: 20 }}>
                      <TreeView
                        tenantId={tenantId}
                        handleTreeViewChange={this.handleTreeViewChange}
                        currentDirectory={selectedPath || ""}
                      />
                      <div style={{ width: window.innerWidth <= sizes.phone ? "100%" : "85%", borderRadius: 10 }}>
                        <ReactiveComponent
                          componentId="docLevel"
                          key="docLevel"
                          defaultQuery={() => ({
                            query,
                            size: 500,
                            aggs: aggregationQuery(currentLevel)
                          })}
                          react={{ and: [] }}
                        >
                          <RenderBrowser
                            level={currentLevel}
                            keyword={`${currentLevel}.keyword`}
                            onViewChange={this.onViewChange}
                            view={view}
                            selectedFiles={selectedFiles}
                            currentDirectory={currentDirectory}
                            onLevelChange={this.onLevelChange}
                          />
                        </ReactiveComponent>
                      </div>
                    </Flex>
                  </Container>
                </ReactiveBase>
              )}
            </RatingSection>
          )}
        />
      </div>
    )
  }
}

export default Search
