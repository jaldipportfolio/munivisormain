import React, { Component } from "react"
import { toast } from "react-toastify"
// import swal from "sweetalert"
import swal from "@sweetalert/with-react"
import moment from "moment"
import cloneDeep from "lodash.clonedeep"
import {
  getAllTransactionsName,
  getPicklistByPicklistName,
  attachHistoricalDocsWithTranAndEntities,
  getFile
} from "GlobalUtils/helpers"
import Loader from "../../GlobalComponents/Loader"
import { AttachDocsWithTransactions } from "../../GlobalComponents/Modals"

import "./styles/index.scss"
import CONST from "../../../globalutilities/consts"
import {
  setFileManagerObjectView,
  getNestedLevels
} from "./utils/fileManagerHelpers"

const Main = React.lazy(() => import("./sections/Main"))
const Navbar = React.lazy(() => import("./sections/Navbar"))

const pathLevels = getNestedLevels()

const parentStyle = {
  overflowY: "auto",
  backgroundColor: "#f5f6f5",
  border: "1px solid rgb(226, 228, 226)",
}

class FileManager extends Component {
  constructor() {
    super()
    this.state = {
      modalState: false,
      folderData: {},
      dropDown: {},
      tempData: {},
      value: {
        docCategory: "",
        docSubCategory: "",
        docType: ""
      },
      type: "",
      searchText: "",
      path: "",
      loading: false,
      preview: false,
      selectedAll: false,
      view: "grid",
      selectType: "",
      breadCrumbs: [],
      selectedFiles: []
    }
  }

  componentWillMount() {
    this.setState({
      selectedFiles: this.props.selectedFiles || []
    })
  }

  componentWillReceiveProps(nextProps) {
    const { data, level, levelList, currentDirectory, view } = nextProps
    if (levelList && levelList.length !== this.props.levelList) {
      const results = setFileManagerObjectView(
        levelList,
        data,
        level,
        currentDirectory
      )
      console.log("results:-", results)
      this.setState(
        {
          loading: true
        },
        async () => {
          this.setBreadCrumbList(results)
          this.setState({
            tempData: cloneDeep(results),
            folderData: cloneDeep(results),
            searchText: "",
            loading: false,
            view
          })
        }
      )
    }
  }

  setBreadCrumbList = parsed => {
    if (parsed) {
      const breadcrumbList = parsed.routes || []
      if (breadcrumbList.length) {
        breadcrumbList.unshift({
          name: "Root",
          path: "",
          level: "path_level_0"
        })
      }
      // console.log("Breadcrumb List:-", breadcrumbList)
      this.setState({
        breadCrumbs: breadcrumbList
      })
    }
  }

  fetchTransactionsName = () => {
    this.setState(
      {
        loading: true
      },
      async () => {
        let result = await getPicklistByPicklistName([
          "LKUPDOCCATEGORIES",
          "LKUPCORRESPONDENCEDOCS",
          "LKUPDOCTYPE"
        ])
        const multiSubCategory =
          (result[2] && result[2].LKUPDOCCATEGORIES) || {}
        result = (result.length && result[1]) || {}
        const category = result && result.LKUPDOCCATEGORIES.map(e => e.label)
        const disabledCategory =
          result &&
          result.LKUPDOCCATEGORIES.filter(e => !e.included).map(e => e.label)
        const docType = result && result.LKUPDOCTYPE.map(e => e.label)
        const disabledDocType =
          result &&
          result.LKUPDOCTYPE.filter(e => !e.included).map(e => e.label)
        const subCategory = {}
        const subCategoryDisabled = {}
        multiSubCategory &&
          Object.keys(multiSubCategory).forEach(data => {
            subCategory[data] =
              multiSubCategory[data] && multiSubCategory[data].map(e => e.label)
            subCategoryDisabled[data] =
              multiSubCategory[data] &&
              multiSubCategory[data].filter(e => !e.included).map(e => e.label)
          })
        this.setState({
          dropDown: {
            ...this.state.dropDown,
            category,
            disabledCategory,
            subCategory,
            subCategoryDisabled,
            docType,
            disabledDocType
          },
          loading: false
        })
      }
    )
  }

  openFolder = value => {
    this.props.onLevelChange(value)
  }

  openFile = value => {
    const { selectedFiles } = this.state
    if(selectedFiles.length){
      selectedFiles.map(file => {
        const fileName = file.split("/").pop()
        getFile(`?objectName=${encodeURIComponent(file)}`, fileName)
      })
    } else {
      getFile(`?objectName=${encodeURIComponent(value.rootPath)}`, value.name)
    }

  }

  onViewProperTies = (item) => {
    const style = {textAlign: "left", fontSize: 15}
    swal({
      buttons: {
        ok: "Done",
      },
      content: (
        <div>
          <div style={style}>
            Filename: {item.name}
          </div>
          <div style={style}>
            Size: {item && item.meta && item.meta.size || "0 Bytes"}
          </div>
          <div style={style}>
            Type of file: {item.name.split(".").pop()}
          </div>
          <div style={style}>
            Created: {moment(item.createdAt).format("MM.DD.YYYY hh:mm A")}
          </div>
        </div>
      )
    })
  }

  onSearchFileAndFolder = searchText => {
    const { tempData, folderData } = this.state
    const findMatches = tempData.children.filter(child =>
      child.name.toLowerCase().includes((searchText || "").toLowerCase())
    )
    this.setState({
      searchText,
      folderData: {
        ...folderData,
        children: findMatches
      }
    })
  }

  onAttachDoc = (item, type) => {
    const { selectedFiles } = this.state
    if (item.type === "file" && !selectedFiles.length) {
      swal("Warning", "Please Select at least one document!", "warning")
      return
    }
    this.setState(
      {
        loading: true
      },
      () => {
        this.setState(
          {
            modalState: true,
            path:
              item.type === "folder" ? this.handleCopy(item) : item.rootPath,
            type,
            selectType: item.type || ""
          },
          async () => {
            let transactionsName = []
            let relatedEntityList = []
            if (type === "Entities") {
              relatedEntityList = await getAllTransactionsName("Entities")
              relatedEntityList =
                (relatedEntityList &&
                  relatedEntityList.length &&
                  relatedEntityList.map(m => ({
                    id: m.id,
                    name: m.name,
                    type: m.relType,
                    isDisable:
                      !!(m && m.isDisable && m.isDisable.length) || false
                  }))) ||
                []
              if (item.type === "folder") {
                relatedEntityList =
                  relatedEntityList.filter(tran => !tran.isDisable) || []
              }
            }
            if (type === "Transactions") {
              transactionsName = await getAllTransactionsName("Transactions")
              if (item.type === "folder") {
                transactionsName = transactionsName.filter(
                  tran => !tran.isDisable
                )
              }
            }
            if (item.type === "file") {
              this.fetchTransactionsName()
            }

            this.setState({
              dropDown: {
                ...this.state.dropDown,
                transactionsName,
                relatedEntityList
              },
              loading: false
            })
          }
        )
      }
    )
  }

  onCloseModal = () => {
    this.setState({
      dropDown: {
        ...this.state.dropDown,
        selected: []
      },
      modalState: false,
      value: {
        docCategory: "",
        docSubCategory: "",
        docType: ""
      },
      type: "",
      selectType: ""
    })
  }

  onCategoryChanges = (key, value) => {
    if (key === "docCategory") {
      this.state.value.docSubCategory = ""
    }
    this.setState({
      value: {
        ...this.state.value,
        [key]: value
      }
    })
  }

  onChangeSelected = selected => {
    this.setState({
      dropDown: {
        ...this.state.dropDown,
        selected
      }
    })
  }

  onSave = () => {
    const {
      dropDown,
      value,
      path,
      type,
      selectType,
      selectedFiles
    } = this.state
    this.setState(
      {
        loading: true
      },
      async () => {
        const response = await attachHistoricalDocsWithTranAndEntities(
          path,
          type,
          selectType,
          { selected: dropDown.selected, value, selectedFiles, path }
        )
        if (response.done) {
          toast(
            `${
              selectType === "folder" ? "Folder" : "Documents"
            } attached successfully!`,
            { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS }
          )
          this.setState({
            value: {
              docCategory: "",
              docSubCategory: "",
              docType: ""
            },
            dropDown: {
              ...this.state.dropDown,
              selected: []
            },
            selectedFiles: [],
            selectType: "",
            modalState: false,
            selectedAll: false,
            loading: false
          })
        }
      }
    )
  }

  handleCopy = item => {
    const levels = {}
    pathLevels.forEach(lvl => {
      if (item[lvl]) {
        levels[lvl] = item[lvl]
      }
    })
    const levelIndex = Object.values(levels).indexOf(item.name)
    const level = Object.keys(levels)[levelIndex]
    const levelPosition = parseInt(level.split("_")[2], 10)
    let path = ""
    for (let i = 0; i <= levelPosition; i++) {
      // eslint-disable-line
      path = `${path ? `${path}/` : ""}${levels[`path_level_${i}`]}`
    }
    // navigator.clipboard.writeText(path)
    return path
  }

  onSelect = (e, item, type) => {
    const { folderData } = this.state
    let { selectedFiles } = this.state
    if (type === "all") {
      const { checked } = e.target
      selectedFiles = []
      this.setState(
        {
          selectedAll: checked
        },
        () => {
          if (checked) {
            folderData.children.forEach(fd => {
              if (fd.type === "file") {
                selectedFiles.push(fd.rootPath)
              }
            })
          }
          this.setState({ selectedFiles })
        }
      )
    } else {
      e.preventDefault()
      const findIndex = selectedFiles.indexOf(item.rootPath)
      if (findIndex !== -1) {
        selectedFiles.splice(findIndex, 1)
      } else {
        selectedFiles.push(item.rootPath)
      }
      this.setState({
        selectedFiles
      })
    }
  }

  render() {
    const {
      breadCrumbs,
      folderData,
      loading,
      view,
      preview,
      searchText,
      modalState,
      dropDown,
      value,
      type,
      selectType,
      selectedFiles,
      path
    } = this.state
    return (
      <div>
        <div>
          {loading ? <Loader /> : null}
          <AttachDocsWithTransactions
            modalState={modalState}
            closeModal={this.onCloseModal}
            dropDown={dropDown}
            value={value}
            type={type}
            path={path || ""}
            selectType={selectType}
            selectedFiles={selectedFiles}
            onCategoryChanges={this.onCategoryChanges}
            onChangeSelected={this.onChangeSelected}
            onSave={this.onSave}
          />
          <div style={parentStyle}>
            <div className="file-listing">
              <Navbar
                {...this.state}
                toggleView={view =>
                  this.setState({
                    view
                  })
                }
                togglePreview={preview =>
                  this.setState({
                    preview
                  })
                }
                data={folderData}
                openFolder={this.openFolder}
                breadcrumbs={breadCrumbs}
                searchText={searchText}
                onSearch={this.onSearchFileAndFolder}
                view={view}
                onViewChange={this.props.onViewChange}
              />
              <Main
                {...this.state}
                loading={loading}
                data={folderData}
                view={view}
                preview={preview}
                onSelect={this.onSelect}
                openFolder={this.openFolder}
                openFile={this.openFile}
                onViewProperTies={this.onViewProperTies}
                handleCopy={item => this.handleCopy(item)}
                onAttachDoc={this.onAttachDoc}
                togglePreview={preview => this.setState({ preview })}
              />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default FileManager
