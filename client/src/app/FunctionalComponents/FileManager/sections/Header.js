import React from "react"

const Header = (props) => (
  <header className="window__header">
    <span className="window__header__title">{props.title}</span>
  </header>
)

export default Header
