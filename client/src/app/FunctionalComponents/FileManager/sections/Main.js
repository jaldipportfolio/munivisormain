import React from "react"
import _ from "lodash"

// Components
import Card from "../components/Card"
import TableRow from "../components/TableRow"

const Main = props => {
  const [sort, sortBy] = React.useState({
    column: "name",
    order: "asc"
  })
  const items = _.mapValues(_.groupBy(props.data.children, "type"), v =>
    _.orderBy(v, [sort.column], [sort.order])
  )

  const togglePreview = (data, from) => {
    if (from === "fromPreview") {
      props.togglePreview(false)
    }
    if (!props.preview && from !== "fromPreview") {
      props.togglePreview(!props.preview)
    }
  }

  const sortItems = by => {
    sortBy({
      column: by,
      order: sort.order === "asc" ? "desc" : "asc"
    })
  }
  return (
    <div>
      {props.data.length === 0 || Object.keys(items || {}).length === 0 ? (
        <div className="empty__state">
          <h3>This folder is empty.</h3>
        </div>
      ) : (
        <div>
          {props.view === "grid" ? (
            <div className="window__main__grid__view">
              {items.folder &&
                items.folder.map((item, index) => (
                  <Card
                    {...item}
                    item={item}
                    openFolder={() => props.openFolder(item)}
                    handleCopy={() => props.handleCopy(item)}
                    onAttachDocToTran={() =>
                      props.onAttachDoc(item, "Transactions")
                    }
                    onAttachDocToEnt={() => props.onAttachDoc(item, "Entities")}
                    index={index}
                    key={index.toString()}
                    togglePreview={togglePreview}
                  />
                ))}
              {items.file &&
                items.file.map((item, index) => (
                  <Card
                    {...item}
                    selectedFiles={props.selectedFiles}
                    onSelect={e => props.onSelect(e, item)}
                    openFile={() => props.openFile(item)}
                    onViewProperTies={() => props.onViewProperTies(item)}
                    handleCopy={() => props.handleCopy(item)}
                    onAttachDocToTran={() =>
                      props.onAttachDoc(item, "Transactions")
                    }
                    onAttachDocToEnt={() => props.onAttachDoc(item, "Entities")}
                    index={index}
                    key={index.toString()}
                    togglePreview={togglePreview}
                  />
                ))}
            </div>
          ) : (
            <div className="tbl-scroll">
              <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                <thead>
                  <tr>
                    <th style={{ backgroundColor: "#6d6b6ba6" }}>
                      <div className="control" style={{ width: "50%" }}>
                        <label className="checkbox-button">
                          {" "}
                          {/* eslint-disable-line */}
                          <input
                            type="checkbox"
                            name="tabs"
                            checked={props.selectedAll}
                            onClick={e => props.onSelect(e, {}, "all")}
                            onChange={e => props.onSelect(e, {}, "all")}
                          />
                          <span className="checkmark" />
                        </label>
                      </div>
                    </th>
                    <th
                      onClick={() => sortItems("name")}
                      style={{ backgroundColor: "#6d6b6ba6" }}
                    >
                      {" "}
                      Name &nbsp;&nbsp;
                      {sort.column === "name" && (
                        <span>
                          {sort.order === "desc" ? (
                            <i
                              className="fa fa-long-arrow-up"
                              aria-hidden="true"
                              title="ascending"
                            />
                          ) : (
                            <i
                              className="fa fa-long-arrow-down"
                              aria-hidden="true"
                              title="descending"
                            />
                          )}
                        </span>
                      )}
                    </th>
                    <th style={{ backgroundColor: "#6d6b6ba6" }}>Type</th>
                    <th
                      // onClick={() => sortItems("size")}
                      style={{ backgroundColor: "#6d6b6ba6" }}
                    >
                      {" "}
                      Size &nbsp;&nbsp;
                      {sort.column === "size" && (
                        <span>
                          {sort.order === "desc" ? (
                            <i
                              className="fa fa-long-arrow-up"
                              aria-hidden="true"
                              title="ascending"
                            />
                          ) : (
                            <i
                              className="fa fa-long-arrow-down"
                              aria-hidden="true"
                              title="descending"
                            />
                          )}
                        </span>
                      )}
                    </th>
                    <th style={{ backgroundColor: "#6d6b6ba6" }}>
                      {" "}
                      Document Content{" "}
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {items.folder
                    ? items.folder.map((item, index) => (
                      <TableRow
                        {...item}
                        selectedFiles={props.selectedFiles}
                        openFolder={() => props.openFolder(item)}
                        onAttachDoc={props.onAttachDoc}
                        key={index.toString()}
                      />
                    ))
                    : null}
                  {items.file
                    ? items.file.map((item, index) => (
                      <TableRow
                        {...item}
                        onSelect={e => props.onSelect(e, item)}
                        openFile={() => props.openFile(item)}
                        onViewProperTies={() => props.onViewProperTies(item)}
                        selectedFiles={props.selectedFiles}
                        onAttachDoc={props.onAttachDoc}
                        key={index.toString()}
                      />
                    ))
                    : null}
                </tbody>
              </table>
            </div>
          )}
        </div>
      )}
    </div>
  )
}

export default Main
