import React from "react"
import { Treebeard } from "react-treebeard"
import { getFile } from "GlobalUtils/helpers"

const TreeExample = props => {
  const [cursor, setCursor] = React.useState(false)

  const onToggle = (node, toggled) => {
    if (cursor) {
      cursor.active = false
    }
    node.active = true
    if (node.children) {
      node.toggled = toggled
    }
    setCursor(node)
    props.openFolder(node)
  }

  const decorators = {
    Toggle: () => (
      <i className="fa fa-folder" aria-hidden="true" style={{color: "#4288dd"}} />
    ),
    Header: props => <span style={{ marginLeft: "8px" }}>{props.node.name}</span>,
    Container: props => props.node.type !== "file" ? (
      <div
        onClick={props.onClick}
        className="text-wrap"
        style={{
          height: "32px",
          display: "flex",
          alignItems: "center",
          cursor: "pointer",
          wordBreak: "break-all",
          whiteSpace: "normal"
        }}
      >
        <props.decorators.Toggle toggled={props.node.toggled} />
        <props.decorators.Header {...props} />
      </div>
    ) :
      <div style={{
        display: "flex",
        cursor: "pointer",
        fontSize: 12,
        padding: "5px 0",
        wordBreak: "break-all",
        whiteSpace: "normal"
      }} className="is-info is-small has-text-left" title={props.node.name}>
        <a  // eslint-disable-line
          // className="text-wrap"
          onClick={() => getFile(`?objectName=${encodeURIComponent(props.node.rootPath)}`, props.node.name)}
          download={props.node.name}>
          <i className="fa fa-file is-info is-small" aria-hidden="true" style={{marginTop: 4, color: "#6a91ee"}}/>
          &nbsp;&nbsp;
          {props.node.name}
        </a>
      </div>,
  }

  return (
    <Treebeard
      style={{
        tree: {
          base: {
            backgroundColor: "white",
          },
          node: {
            activeLink: {
              background: "transparent",
            },
          },
        },
      }}
      data={props.files}
      onToggle={onToggle}
      decorators={decorators}
    />
  )
}

const Sidebar = props => (
  <aside className="file_sidebar">
    <div className="buttons has-addons">
      <span className={`button is-small ${props.view === "list" ? "is-danger is-selected" : ""}`} onClick={() => props.toggleView("list")} title="List view"><i className="fa fa-th-list"/></span>
      <span className={`button is-small ${props.view === "grid" ? "is-danger is-selected" : ""}`}  onClick={() => props.toggleView("grid")} title="Grid view"><i className="fa fa-th" /></span>
    </div>
    <TreeExample openFolder={props.openFolder} files={props.data} />
  </aside>
)

export default Sidebar
