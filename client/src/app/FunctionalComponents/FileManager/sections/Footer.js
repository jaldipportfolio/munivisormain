import React from "react"

const Footer = props => {
  return (
    <footer className="window__footer">
      <span>
        {props.itemCount &&
					`${props.itemCount} ${
					  props.itemCount === 1 ? "item" : "items"
					}`}
      </span>
    </footer>
  )
}

export default Footer
