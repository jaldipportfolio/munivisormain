import React from "react"

const Navbar = props => (
  <div>
    <nav
      className="breadcrumb"
      aria-label="breadcrumbs"
      style={{
        background: "#4288dd",
        marginBottom: 0,
        display: "flex",
        justifyContent: "space-between"
      }}
    >
      <ul style={{ padding: "5px 10px" }}>
        {props.breadcrumbs.length ? (
          props.breadcrumbs.map((breadcrumb, index) => (
            <React.Fragment key={index.toString()}>
              {index === props.breadcrumbs.length - 1 ? (
                <li className="is-active">
                  <a aria-current="page">{breadcrumb.name}</a>
                </li> // eslint-disable-line
              ) : (
                <li>
                  <a
                    title={breadcrumb.path}
                    data-path={breadcrumb.path}
                    onClick={() => props.openFolder(breadcrumb)}
                  >
                    {breadcrumb.name}
                  </a>
                </li>
              ) // eslint-disable-line
              }
            </React.Fragment>
          ))
        ) : (
          <li className="is-active">
            <a aria-current="page">Root</a>
          </li>
        ) // eslint-disable-line
        }
      </ul>
      <div className="buttons has-addons" style={{ marginRight: 10 }}>
        <span
          className={`button is-small ${
            props.view === "list" ? "is-danger is-selected" : ""
          }`}
          onClick={() => props.onViewChange("list")}
          title="List view"
        >
          <i className="fa fa-th-list" />
        </span>
        <span
          className={`button is-small ${
            props.view === "grid" ? "is-danger is-selected" : ""
          }`}
          onClick={() => props.onViewChange("grid")}
          title="Grid view"
        >
          <i className="fa fa-th" />
        </span>
      </div>
    </nav>
    <div>
      <input
        className="file_search_input"
        type="text"
        placeholder="Search files or folders..."
        value={props.searchText || ""}
        onChange={event => props.onSearch(event.target.value)}
      />
    </div>
  </div>
)

export default Navbar
