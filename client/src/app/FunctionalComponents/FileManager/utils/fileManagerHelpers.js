import { ELASTIC_SEARCH_URL } from "../../../../constants"

export const getNestedLevels = () => {
  const myNum = 10
  const pathLevels = []
  for (let i = 0; i <= myNum; i++) {
    // eslint-disable-line
    pathLevels.push(`path_level_${i}`)
  }
  return pathLevels
}

export const setFileManagerObjectView = (
  levelList,
  data,
  level,
  currentDirectory
) => {
  const browseData = []
  const levelPosition = parseInt(level.split("_")[2], 10)
  const globalBreadCrumb = {
    level,
    currentDirectory,
    routes: []
  }
  data.forEach((item, index) => {
    const details = item._source || {}
    const nestedPath = details.s3Key.split("/")
    const levelIndex = nestedPath.indexOf(details[level])
    nestedPath.splice(levelIndex + 1, nestedPath.length)
    nestedPath.splice(0, 2)
    const path = nestedPath.join("/")
    if (currentDirectory) {
      if (details[`path_level_${levelPosition + 2}`]) {
        const isExists = browseData.find(
          l =>
            details[`path_level_${levelPosition + 1}`] &&
            l.name === details[`path_level_${levelPosition + 1}`]
        )
        if (!isExists) {
          browseData.push({
            name: details[`path_level_${levelPosition + 1}`] || "",
            path: `${path}/`,
            rootPath: details.s3Key || "",
            size: "",
            type: "folder",
            ...details
          })
        }
      } else {
        browseData.push({
          name: details[`path_level_${levelPosition + 1}`] || "",
          path: `${path}/${details[`path_level_${levelPosition + 1}`]}`,
          rootPath: details.s3Key || "",
          size: "",
          type: "file",
          ...details
        })
      }
      if (index === 0) {
        for (let i = 0; i <= levelPosition; i++) {
          // eslint-disable-line
          globalBreadCrumb.routes.push({
            name: details[`path_level_${i}`],
            [`path_level_${i}`]: details[`path_level_${i}`],
            path: `${globalBreadCrumb.path || ""}${details[`path_level_${i}`]}/`
          })
          globalBreadCrumb.path = `${globalBreadCrumb.path || ""}${
            details[`path_level_${i}`]
          }/`
        }
      }
    } else if (
      details[`path_level_${levelPosition + 2}`] ||
      details[`path_level_${levelPosition + 1}`]
    ) {
      const isExists = browseData.find(
        l =>
          details[`path_level_${levelPosition}`] &&
          l.name === details[`path_level_${levelPosition}`]
      )
      if (!isExists) {
        browseData.push({
          name: details[`path_level_${levelPosition}`] || "",
          path: `${path}/`,
          rootPath: details.s3Key || "",
          size: "",
          type: "folder",
          ...details
        })
      }
    } else {
      browseData.push({
        name: details[`path_level_${levelPosition}`] || "",
        path: `${path}`,
        rootPath: details.s3Key || "",
        size: "",
        type: "file",
        ...details
      })
    }
  })
  // console.log(levelPosition, browseData)
  let drive = {
    children: browseData || [],
    name: "Root",
    levelPosition
  }
  if (globalBreadCrumb.path) {
    drive = {
      ...drive,
      ...globalBreadCrumb
    }
    return drive
  }
  return drive
}

export const initQuery = () => ({
  bool: {
    must: [
      {
        bool: {
          must: [
            {},
            {
              match_all: {}
            }
          ]
        }
      }
    ]
  }
})

export const levelQuery = (level, item, pathQuery) => ({
  bool: {
    must: [
      {
        bool: {
          must: [{}, ...pathQuery]
        }
      }
    ]
  }
})

export const aggregationQuery = level => ({
  [`${level}.keyword`]: {
    terms: {
      field: `${level}.keyword`,
      size: 100,
      order: {
        _count: "desc"
      }
    }
    /* ,
    aggs: {
      details: {
        top_hits: {
          _source: {
            include: [
              "meta",
              "s3Key",
              "esIndex",
              "versionId",
              "doc_preview",
              "tenantId",
              "updatedAt",
              ...(getNestedLevels() || [])
            ]
          }
        }
      }
    } */
  }
})

export function getValuesByPrefix(object = {}, prefix) {
  return Object.keys(object).reduce((arrAcc, item) => {
    if (item.startsWith(prefix)) arrAcc.push(object[item])
    return arrAcc
  }, [])
}

export function getPathLevelData(data = []) {
  return data.map(dataItem => getValuesByPrefix(dataItem, "path_level"))
}

export function isFile(fileName, item, currentLevel) {
  const levels = (item && item.details && item.details.hits && item.details.hits.hits && item.details.hits.hits.length && item.details.hits.hits[0]._source) || {}
  return !(Object.values(levels).filter(val => val).length > currentLevel+1)
}

const getPathLevelQuery = level => ({
  [`path_level_${level}.keyword`]: {
    terms: { field: `path_level_${level}.keyword`, size: 1000 },
    aggs: {
      details: {
        top_hits: {
          _source: {
            include: [
              ...(getNestedLevels() || [])
            ]
          }
        }
      }
    }
  }
})

export const getDefaultQuery = level => {
  let lastLevelQuery = getPathLevelQuery(level)
  level--
  while (level >= 0) {
    const currQuery = getPathLevelQuery(level)
    currQuery[`path_level_${level}.keyword`].aggs = { ...lastLevelQuery, details: {
      top_hits: {
        _source: {
          include: [
            ...(getNestedLevels() || [])
          ]
        }
      }
    }
    }
    lastLevelQuery = currQuery
    level--
  }
  return { query: { match_all: {} }, size: 0, aggs: lastLevelQuery }
}

export const searchAPI = async (query, tenantId) => {
  try {
    const response = await fetch(
      `${
        ELASTIC_SEARCH_URL.substr(-1) === "/"
          ? ELASTIC_SEARCH_URL.slice(0, -1)
          : ELASTIC_SEARCH_URL
      }/tenant_docs_historical_${tenantId}/_msearch`,
      {
        method: "POST",
        body: `{"preference":"TreeView"}\n${JSON.stringify(query)}\n`,
        headers: {
          Authorization: localStorage.getItem("token"),
          "Content-Type": "application/x-ndjson"
        }
      }
    )
    return { response }
  } catch (e) {
    return { error: e }
  }
}

export const sizes = {
  desktop: 992,
  tablet: 768,
  phone: 576
}

export const getFileIcon = fileName => {
  const ext = fileName.split(".").pop()

  switch (ext.toLowerCase()) {
  case "pdf":
    return { icon: "fa-file-pdf-o", style: { color: "#ea4335"} }
  case "doc":
  case "docx":
    return { icon: "fa-file-word-o", style: { color: "#4285f4"} }
  case "ppt":
  case "pptx":
    return { icon: "fa-file-powerpoint-o", style: { color: "#fd7541"} }
  case "png":
  case "jpg":
  case "jpeg":
  case "gif":
  case "tiff":
  case "svg":
    return { icon: "fa-file-picture-o", style: { color: "#ea4335"} }
  case "xls":
  case "xlsx":
  case "xlsm":
  case "xltx":
  case "xltm":
  case "csv":
    return { icon: "fa-file-excel-o", style: { color: "#0f9d58"} }
  case "tex":
  case "txt":
    return { icon: "fa-file-alt", style: { color: "#4285f4"} }
  case "msg":
    return { icon: "fa-envelope", style: { color: "#efbe56" } }
  default:
    return { icon: "fa-file", style: { color: "rgba(81, 86, 90, 0.62)"} }
  }
}
