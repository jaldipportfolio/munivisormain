import React, { Component } from "react"
import { connect } from "react-redux"
import {toast} from "react-toastify"
import cloneDeep from "lodash.clonedeep"
import {withRouter} from "react-router-dom"
import DocumentPage from "../../../../../../GlobalComponents/DocumentPage"
import Loader from "../../../../../../GlobalComponents/Loader"
import CONST, {ContextType} from "../../../../../../../globalutilities/consts"
import {
  fetchSupervisoryDocuments, putSupervisoryAuditLogs, putSupervisoryDocument, pullSupervisoryDocument
} from "../../../../../../StateManagement/actions/Supervisor"
import Disclaimer from "../../../../../../GlobalComponents/Disclaimer"
import withAuditLogs from "../../../../../../GlobalComponents/withAuditLogs"

class Document extends Component {
  constructor(props) {
    super(props)

    this.state = {
      loading:true,
      documentsList: [],
    }
  }

  async componentWillMount() {
    fetchSupervisoryDocuments((res) => {
      this.setState({
        documentsList: (res && res.supervisoryObligationsDocuments && res.supervisoryObligationsDocuments.length) ? cloneDeep(res.supervisoryObligationsDocuments) : [],
        loading: false,
      })
    })
  }

  onDocSave = ( docs, callback) => {
    console.log(docs)
    putSupervisoryDocument("" ,docs, (res)=> {
      if (res && res.status === 200) {
        toast("Documents has been updated!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave("documents")
        callback({
          status: true,
          documentsList: (res.data && res.data.supervisoryObligationsDocuments) || [],
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onStatusChange = async (e, doc, callback) => {
    let document = {}
    let type = ""
    const {name, value} = (e && e.target) || {}
    if(name){
      document = {
        _id: doc._id, //eslint-disable-line
        [name]: value
      }
      type = "docStatus"
    }else {
      document = {
        ...doc
      }
      type = "updateMeta"
    }
    putSupervisoryDocument(`?details=${type}`, document, (res)=> {
      if (res && res.status === 200) {
        toast("Document status has been updated!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave("document")
        if(name){
          callback({
            status: true,
          })
        }else {
          callback({
            status: true,
            documentsList: (res.data && res.data.supervisoryObligationsDocuments) || [],
          })
        }
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onDeleteDoc = async (documentId, callback) => {
    const res = await pullSupervisoryDocument(`?docId=${documentId}`)
    if (res && res.status === 200) {
      toast("Document removed successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
      this.onAuditSave("document")
      callback({
        status: true,
        documentsList: (res.data && res.data.supervisoryObligationsDocuments) || [],
      })
    } else {
      toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
      callback({
        status: false
      })
    }
  }

  onAuditSave = (key) => {
    const {user} = this.props
    const auditLogs = this.props.auditLogs.filter(log => log.key === key)
    if(auditLogs.length) {
      auditLogs.forEach(log => {
        log.userName = `${user.userFirstName} ${user.userLastName}`
        log.date = new Date().toUTCString()
        log.superVisorModule = "Supervisory Obligation"
        log.superVisorSubSection = "Supervisory Obligation Documents"
      })
      putSupervisoryAuditLogs(auditLogs, (res)=> {
        if(res && res.status === 200) {
          this.setState({
            loading: false,
          })
        }
      })
      const remainLogs = this.props.auditLogs.filter(log => log.key !== key)
      this.props.updateAuditLog(remainLogs)
    }
  }

  render() {
    const { documentsList, loading } = this.state
    const {nav2} = this.props
    // const loading = () => <Loader/>
    const staticField = {
      docCategory: "Compliance",
      docSubCategory: "Supervisory & Compliance Obligations",
    }
    if(loading){
      return <Loader/>
    }
    return (
      <div>
        <div>
          <DocumentPage {...this.props} isNotTransaction onSave={this.onDocSave} title="Supervisory Obligation Documents" pickCategory="LKUPDOCCATS" pickSubCategory="LKUPCORRESPONDENCEDOCS" onStatusChange={this.onStatusChange}
            pickAction="LKUPCOMPLIANCEDOCACTION" pickType="LKUPSACODOCTYPE" staticField={staticField} documents={documentsList} category="supervisoryObligationsDocuments"
            contextType={ContextType.supervisor.svAndObligation} tranId={nav2} onDeleteDoc={this.onDeleteDoc}/>
        </div>
        <hr />
        <Disclaimer />
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(Document)
export default withRouter(connect(mapStateToProps, null)(WrappedComponent))
