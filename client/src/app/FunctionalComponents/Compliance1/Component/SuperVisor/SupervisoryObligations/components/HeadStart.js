import React from "react"
import Disclaimer from "../../../../../../GlobalComponents/Disclaimer"

export const HeadStart = () =>(
  <div>
    <div>
      <p>Rule G-44 based recommended sections in compliance policy and supervisory procedure document
        <a href="../../../../../../../public/docs/G8-Complaints-Prod-Problem-Codes.pdf" target="_new"><i className="fas fa-info-circle"/></a>
      </p>
    </div>
    <hr />
    <Disclaimer />
  </div>
)
