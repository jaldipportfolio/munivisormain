import React from "react"
import ReactTable from "react-table"
import "react-table/react-table.css"

const highlightSearch = (string, searchQuery) => {
  const reg = new RegExp(
    searchQuery.replace(/[|\\{}()[\]^$+*?.]/g, "\\$&"),
    "i"
  )
  return string.replace(reg, str => `<mark>${str}</mark>`)
}

const DashboardTable = ({
  giftsList,
  disclosures,
  pageSizeOptions = [5, 10, 20, 50, 100],
  search,
  onAction,
}) => {
  const columns = [
    {
      Header: "Name",
      id: "name",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            <div
              dangerouslySetInnerHTML={{
                __html: highlightSearch(`${item.userFirstName} ${item.userLastName}` || "-", search)
              }}
            />
          </div>
        )
      },
      sortMethod: (a, b) => `${a.userFirstName} ${a.userLastName}` - `${b.userFirstName} ${b.userLastName}`
    },
    {
      id: "type",
      Header: "Type",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            <div
              dangerouslySetInnerHTML={{
                __html: highlightSearch(disclosures ? "G-37 Quartetly Obligation" : "G-20 Quartetly Obligation" || "-", search)
              }}
            />
          </div>
        )
      },
    },
    {
      id: "quarter",
      Header: "Quarter",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            <div
              dangerouslySetInnerHTML={{
                __html: highlightSearch(disclosures ? `${item.contributeId && item.contributeId.quarter}` : `${item.quarter}` || "-", search)
              }}
            />
          </div>
        )
      },
      sortMethod: (a, b) => disclosures ? a.contributeId.quarter - b.contributeId.quarter : a.quarter - b.quarter
    },
    {
      id: "year",
      Header: "Year",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            <div
              dangerouslySetInnerHTML={{
                __html: highlightSearch(disclosures ? `${item.contributeId && item.contributeId.year}` : `${item.year}` || "-", search)
              }}
            />
          </div>
        )
      },
      sortMethod: (a, b) => disclosures ? a.contributeId.year - b.contributeId.year : a.year - b.year
    },
    {
      id: "link",
      Header: "Link",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            <a className="text-overflow" onClick={() => onAction(item)} >Disclosure</a>
          </div>
        )
      },
    },
    {
      id: "status",
      Header: "Status",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            <div
              dangerouslySetInnerHTML={{
                __html: highlightSearch(`${item.status}` || "-", search)
              }}
            />
          </div>
        )
      },
      sortMethod: (a, b) => a.status.localeCompare(b.status)
    }
  ]
  return (
    <div className="column">
      <ReactTable
        columns={columns}
        data={giftsList || disclosures}
        showPaginationBottom
        defaultPageSize={10}
        pageSizeOptions={pageSizeOptions}
        className="-striped -highlight is-bordered"
        style={{ overflowX: "auto" }}
        showPageJump
        minRows={2}
      />
    </div>
  )
}

export default DashboardTable
