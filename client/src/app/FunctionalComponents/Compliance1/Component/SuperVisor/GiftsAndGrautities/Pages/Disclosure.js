import React, { Component } from "react"
import cloneDeep from "lodash.clonedeep"
import {toast} from "react-toastify"
import { connect } from "react-redux"
import * as qs from "query-string"
import { getPicklistByPicklistName, updateCACPoliticalActionStatus, getToken } from "GlobalUtils/helpers"
import EditableTable from "../../../../../../GlobalComponents/EditableTable"
import DocumentPage from "../../../../../../GlobalComponents/DocumentPage"
import Accordion from "../../../../../../GlobalComponents/Accordion"
import RatingSection from "../../../../../../GlobalComponents/RatingSection"
import Audit from "../../../../../../GlobalComponents/Audit"
import {SelectLabelInput} from "../../../../../../GlobalComponents/TextViewBox"
import Loader from "../../../../../../GlobalComponents/Loader"
import CONST, {ContextType} from "../../../../../../../globalutilities/consts"
import {fetchSupplierContacts} from "../../../../../../StateManagement/actions/TransactionDistribute"
import {fetchAssigned} from "../../../../../../StateManagement/actions/CreateTransaction"
import {
  getGiftsGrautitiesDetails,
  pullGiftsGratuitiesDetails,
  postAddNewUser,
  putGiftsGrautitiesDocuments,
  postCheckGiftsGratuities,
  putGiftsGrautitiesAuditLogs,
  postGiftsGratuitiesInfo,
  pullContributeDetailsDocuments
} from "../../../../../../StateManagement/actions/Supervisor"
import { RecordGiftAndExpenses } from "../../Validation/GiftsGrautitiesValidate"
import withAuditLogs from "../../../../../../GlobalComponents/withAuditLogs"
import GiftsAndGrautitiesCheck from "../components/GiftsAndGrautitiesCheck"
import {ComplainantDetailsValidate} from "../../Validation/ClientComplaintsValidate"
import moment from "moment"
import {DropdownList} from "react-widgets"
import TableHeader from "../../../../../../GlobalComponents/TableHeader"

const cols = [
  {name: "User"},
  {name: "Notes/Instruction"},
  {name: "Time"}]

class Disclosure extends Component {
  constructor(props) {
    super(props)

    this.state = {
      tables: [
        {
          name:"Record gifts and related expenses",
          key: "giftsToRecipients",
          row: cloneDeep(CONST.SuperVisor.giftsToRecipients) || {},
          header: [
            {name: "Recipient's employer<span class='has-text-danger'>*</span>"},
            {name: "Recipient<span class='has-text-danger'>*</span>"},
            {name: "Gift/Expense Value<span class='has-text-danger'>*</span>"},
            {name: "Date gift/expense made"},
            {name: "Action"}
          ],
          list: [],
          tbody: [
            {name: "recipientEntityName", type: "dropdown", labelName: "recipientEntityName", dropdownKey: "allFirms" },
            {name: "recipientUserFirstName", type: "dropdown", labelName: "recipientUserFirstName", dropdownKey: "users" },
            {name: "giftValue", placeholder: "$", type: "number", prefix: "$"},
            {name: "giftDate", placeholder: "Date", type: "date"},
            {name: "action"},
          ],
          dropDown: {
            userIdKey: "recipientUserId"
          },
          handleError: (payload, minDate) => RecordGiftAndExpenses(payload, minDate),
          children: this.renderChildren()
        }
      ],
      dropDown:{
        polContribQuarterOfDisclosureFirm: [{name: "First", value: 1 }, {name: "Second", value: 2}, {name: "Third", value: 3}, {name: "Fourth", value: 4}],
        polContribYearOfDisclosureFirm: [2017, 2018, 2019],
        polContribOfDisclosureFirm: ["Self", "Someone else", "Firm"],
        supervisorAction: ["Approved", "Rejected", "Canceled"],
        usersList: [],
        allUsers: [],
        entityList: [],
        contributorCategory: [],
        allSuggestions: [],
        someUsers: [],
      },
      isSaveDisabled: {},
      disclosureInfo : CONST.SuperVisor.disclosureInfo || {},
      contributor: {
        complainantDetails: {
          ...cloneDeep(CONST.complaint.complainantDetails),
          entityId: (this.props.user && this.props.user.entityId) || "",
          entityName: (this.props.user && this.props.user.firmName) || "",
        }
      },
      tempContributor: CONST.SuperVisor.contributorDisclosure,
      documentsList: [],
      sections: [
        {
          key: "giftsNotSubjectToGenLimitation",
          title: "Gifts and Gratuities Not Subject to General Limitation",
          titleText: "Rule G-20 Gifts, Gratuities, Non-Cash Compensation and Expenses of Issuance ",
          boldLine: "I/we confirm that I/we have read and understood Gifts and Gratuities Not Subject to General Limitation as defined in Rule G-20 section (d). The gifts as related to this disclosure do fall under the category of Gifts and Gratuities Not Subject to General Limitation as defined in Rule G-20 section (d).",
        },
        {
          key: "giftsOfferingProceedsUseProhibition",
          title: "Prohibition of Use of Offering Proceeds Exclusion",
          titleText: "Rule G-20 Gifts, Gratuities, Non-Cash Compensation and Expenses of Issuance",
          boldLine: "I/we confirm that I/we have read and understood Rule G-20 section (e). The entertainment expenses as related to this disclosure include ordinary and reasonable expenses for meals hosted by the regulated entity and directly related to the offering for which the regulated entity was retained.",
        },
        {
          key: "giftsPermittedNonCashCompArrangements",
          title: "Permitted non-cash compensation arrangements",
          titleText: "Rule G-20 Gifts, Gratuities, Non-Cash Compensation and Expenses of Issuance",
          boldLine: "I/we confirm that I/we have read and understood permitted non-cash compensation arrangements as defined in Rule G-20 section (g). The payments directly or indirectly accepted or made as related to this disclosure do fall under the category of permitted non-cash compensation arrangements as defined in Rule G-20 section (g).",
        },
      ],
      giftsCheckBoxes: {
        giftsNotSubjectToGenLimitation: false,
        giftsOfferingProceedsUseProhibition: false,
        giftsPermittedNonCashCompArrangements: false,
        giftsAffirmNoPayment: false,
        giftsAffirmUseOfProceeds: false,
        giftsAffirmCompletionOfG20ForPeriod: false,
      },
      currentYear: parseInt(moment(new Date()).utc().format("YYYY"), 10),
      currentQuarter: moment(new Date()).utc().quarter(),
      preApprovalQuarter: "",
      preApprovalYear: "",
      status: "",
      supervisorNotesShow: false,
      supervisorNote: "",
      loading: true,
    }
  }

  async componentDidMount(){
    const queryString = qs.parse(location.search)
    let preApprovalQuarter = queryString.quarter || ""
    let preApprovalYear = queryString.year || ""
    const polContribYearOfDisclosureFirm = []
    let min = 2015,max = min + 10
    for (let i = min; i<=max; i++) { polContribYearOfDisclosureFirm.push(i) }

    const picResult = await getPicklistByPicklistName(["LKUPCONTRIBUTORCAT", "LKUPSTATE", "LKUPCOUNTRY"])
    const result = (picResult.length && picResult[1]) || {}
    const stateCountry =  (picResult[3] && picResult[3].LKUPCOUNTRY) || {}
    const {disclosureInfo, currentYear, currentQuarter, dropDown} = this.state
    const statusFlag = (queryString.year > currentYear) || ((queryString.quarter > currentQuarter) && (queryString.year === currentYear)) || queryString.status === "Pre-approval"

    if(statusFlag && !queryString.year && !queryString.quarter){
      if(currentQuarter === 4){
        preApprovalQuarter = 1
        preApprovalYear = currentYear + 1
      }else {
        preApprovalQuarter = currentQuarter + 1
        preApprovalYear = currentYear
      }
    }
    // fetchEntNotEqSelf(this.props.user.entityId, (organizations)=> {
    this.setState({
      dropDown: {
        ...this.state.dropDown,
        contributorCategory: result.LKUPCONTRIBUTORCAT || [],
        thirdPartyCategory: result.LKUPCONTRIBUTORCAT || [],
        state: result.LKUPSTATE || [],
        stateCountry,
        polContribYearOfDisclosureFirm,
      },
      disclosureDetails: {},
      disclosureInfo: {
        ...disclosureInfo,
        year: currentQuarter === 4 && statusFlag ? preApprovalYear : queryString.year || currentYear,
        quarter: statusFlag ? preApprovalQuarter : queryString.quarter || currentQuarter,
        disclosureFor: queryString.disclosureFor || this.props.user.userId || "",
        status: statusFlag ? "Pre-approval" : ""
      },
      preApprovalQuarter,
      preApprovalYear,
      status: statusFlag ? "Pre-approval" : "",
      ...queryString
    },() => {
      this.getEntitiesAndUsers()
      this.getCurrentEntityUsers()
    })
    // })
  }

  renderChildren = () => (
    <div>
      <div className="columns is-vcentered">
        <div className="column">
          <p className="emmaTablesTd"><strong>Please note: </strong>If gifts are given
              to multiple recipients, regulated entities should record the names of
              each recipient and calculate and record the value of the gift on a pro
              rata per recipient basis, for purposes of ensuring compliance with the
              general limitation of Rule G-20 section (c).
          </p>
        </div>
      </div>
      <div className="columns is-vcentered">
        <div className="column">
          <p className="emmaTablesTd"><strong>Cannot find recipient or related firm?</strong></p>
        </div>
        <div className="column">
          <div className="control">
            <button className="button is-text is-small">
              <a href="/addnew-entity" target="new">Add Entity/Firm to CRM first</a>
            </button>
          </div>
        </div>
        <div className="column">
          <div className="control">
            <button className="button is-text is-small">
              <a href="/addnew-contact" target="new">Add Recipient to CRM first</a>
            </button>
          </div>
        </div>
      </div>
    </div>
  )

  getEntitiesAndUsers = () => {
    fetchSupplierContacts(this.props.user.entityId, "All", (allFirms)=> {
      const allUsers = allFirms && allFirms.userList ? allFirms.userList.map(e => ({ ...e, name: e.userFirstName, id: e._id })) : []
      const partFirms = allFirms && allFirms.suppliersList ? allFirms.suppliersList.map(e => ({ ...e, name: e.msrbFirmName, id: e._id })) : []

      this.setState(prevState => ({
        dropDown: {
          ...prevState.dropDown,
          allFirms: partFirms,
          allUsers,
        },
      }))
    })
  }

  getCurrentEntityUsers = () => {
    fetchAssigned(this.props.user.entityId, (users)=> {
      this.setState({
        dropDown: {
          ...this.state.dropDown,
          someUsers: users.usersList || [],
        },
        loading: false,
      }, () => {
        this.onSearch()
      })
    })
  }

  onRemove = (type, removeId, callback) => {
    const {disclosureDetails} = this.state
    const giftsId = disclosureDetails._id
    pullGiftsGratuitiesDetails(type, giftsId, removeId, (res) => {
      if(res && res.status === 200) {
        toast("Removed Gifts And Gratuities Disclosure successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave(type)
        callback({
          status: true,
          list: (res.data && res.data[type]) || [],
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onChange = (e, inputName) => {
    if(inputName) {
      this.setState({
        disclosureInfo: {
          ...this.state.disclosureInfo,
          [inputName]: e.id,
        }
      }, () => this.onSearch())
    }else {
      const { name, value, title} = e.target
      this.setState({
        disclosureInfo: {
          ...this.state.disclosureInfo,
          [name]: e.target.type === "checkbox" ? e.target.checked : value
        }
      }, () => {
        if(name === "disclosureFor" || name === "quarter" || name === "year") {
          this.onSearch()
        }else if(name === "status") {
          this.props.addAuditLog({userName: this.props.user.userFirstName, log: `change ${title} ${value}`, date: new Date(), key: "details" })
          this.onActionSave()
        }
      })
    }
  }

  onChangeItem = (value, name) => {
    this.setState({
      [name]: value,
    })
  }

  onDropDownChange = (inputName, key, select, index, callback) => {
    const user = select
    const {dropDown} = this.state
    if(inputName === "recipientEntityName") {
      this.props.addAuditLog({userName: this.props.user.userFirstName, log: `In Disclosure ${user.name}`, date: new Date(), key })
      this.setState({
        dropDown: {
          ...dropDown,
          usersList: dropDown.allUsers.filter(e => select.id === (e.entityId && e.entityId._id)) || []
        }
      },() => {
        callback({
          status: true,
          object: {
            recipientEntityId: select.id,
            recipientEntityName: select.name,
          },
        })
      })

    }
    if(inputName === "recipientUserFirstName") {
      callback({
        status: true,
        object: {
          recipientUserId: select.id,
          recipientUserFirstName: select.userFirstName,
          recipientUserLastName: select.userLastName,
        },
      })
    }
  }

  onSearch = () => {
    const {disclosureInfo, tables} = this.state
    if (disclosureInfo && disclosureInfo.disclosureFor && disclosureInfo.year && disclosureInfo.quarter) {
      const query = `?disclosureFor=${disclosureInfo.disclosureFor}&year=${disclosureInfo.year}&quarter=${disclosureInfo.quarter}`
      this.setState({
        isSearchDisabled: true
      }, () => {
        getGiftsGrautitiesDetails(query, (res) => {
          if (res) {
            tables.forEach(tbl => {
              tbl.list = (res && res[tbl.key]) || []
            })

            this.setState(prevState => ({
              disclosureDetails: res || {},
              disclosureInfo: {
                ...prevState.disclosureInfo,
                status: typeof res.status !== "boolean" ? res.status || "" : prevState.status,
              },
              contributor: {
                ...prevState.contributor
              },
              tempContributor: {
                ...prevState.contributor,
              },
              documentsList: res.giftDisclosureDocuments || [],
              tables,
              giftsCheckBoxes: {
                giftsNotSubjectToGenLimitation: res.giftsNotSubjectToGenLimitation || false,
                giftsOfferingProceedsUseProhibition: res.giftsNotSubjectToGenLimitation || false,
                giftsPermittedNonCashCompArrangements: res.giftsPermittedNonCashCompArrangements || false,
                giftsAffirmNoPayment: res.giftsAffirmNoPayment || false,
                giftsAffirmUseOfProceeds: res.giftsAffirmUseOfProceeds || false,
                giftsAffirmCompletionOfG20ForPeriod: res.giftsAffirmCompletionOfG20ForPeriod || false,
              },
              isSearchDisabled: false,
              auditLogs: (res && res.auditLogs) || [],
            }))
          } else {
            toast("something went wrong", {autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR,})
            this.setState({
              isSearchDisabled: false
            })
          }
        })
      })
    }
  }

  onSave = (type, item, callback) => {
    const {disclosureDetails, disclosureInfo} = this.state
    const {user} = this.props
    const {disclosureFor, year, quarter} = disclosureInfo
    const giftsId = disclosureDetails._id // eslint-disable-line
    const query = `${disclosureFor}?year=${year}&quarter=${quarter}&type=${type}`
    const queryString = `${giftsId || "none"}?type=${type}`

    if(item && !item._id){
      item.createdUserId = user.userId
      item.createdUserName = `${user.userFirstName} ${user.userLastName}`
    }
    postCheckGiftsGratuities(giftsId ? queryString : query, item, (res)=> {
      if (res && res.status === 200) {
        toast("Record gifts and related expenses updated successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.setState({
          auditLogs: (res && res.data && res.data.auditLogs) || [],
          disclosureDetails: {
            ...disclosureDetails,
            ...res.data
          },
          disclosureInfo: {
            ...disclosureInfo,
            status: res.data.status || disclosureInfo.status || ""
          },
        },() => {
          this.onAuditSave(type)
          callback({
            status: true,
            list: (res && res.data && res.data[type]) || [],
          })
        })
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onContributorSave =(key) => {
    const {contributor} = this.state
    const errors = ComplainantDetailsValidate(contributor.complainantDetails)

    if(errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.forEach(err => {
        errorMessages[err.context.key] = "Required" || err.message
      })
      console.log(errorMessages)
      this.setState(prevState => ({errorMessages: {...prevState.errorMessages, [key]: errorMessages}}))
      return
    }

    this.setState({
      isSaveDisabled: {
        [key]: true
      }
    }, () => {
      postAddNewUser(contributor.complainantDetails, (res)=> {
        if (res && res.status === 200) {
          toast("Added Disclosure Contributor Successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
          this.setState({
            contributor: {
              complainantDetails: {
                ...cloneDeep(CONST.complaint.complainantDetails),
                entityId: (this.props.user && this.props.user.entityId) || "",
                entityName: (this.props.user && this.props.user.firmName) || "",
              }
            },
            isSaveDisabled: {
              [key]: false
            },
            errorMessages: {
              [key]: {},
            },
          },() => {
            this.getCurrentEntityUsers()
          })
        } else {
          toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
          this.setState({
            isSaveDisabled: {
              [key]: false
            },
          })
        }
      })
    })

  }

  onAuditSave = (key) => {
    const {user} = this.props
    const {disclosureDetails} = this.state
    const auditLogs = this.props.auditLogs.filter(log => log.key === key)

    if(disclosureDetails && disclosureDetails._id) {
      if (auditLogs.length) {
        auditLogs.forEach(log => {
          log.userName = `${user.userFirstName} ${user.userLastName}`
          log.date = new Date().toUTCString()
          log.superVisorModule = "Gifts And Gratuities"
          log.superVisorSubSection = "Disclosure"
        })
        putGiftsGrautitiesAuditLogs(`?giftsId=${disclosureDetails._id}`, auditLogs, (res) => {
          if (res && res.status === 200) {
            const remainLogs = this.props.auditLogs.filter(log => log.key !== key)
            this.props.updateAuditLog(remainLogs)
            this.setState({
              auditLogs: (res.data && res.data.auditLogs) || []
            })
          }
        })
      }
    }
  }

  onDocSave = (docs, callback) => {
    console.log(docs)
    const {disclosureDetails} = this.state
    if(disclosureDetails && disclosureDetails._id){
      putGiftsGrautitiesDocuments("",`?giftsId=${disclosureDetails._id}`, docs, (res)=> {
        if (res && res.status === 200) {
          toast("Disclosure Related Documents has been updated!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
          this.onAuditSave("documents")
          callback({
            status: true,
            documentsList: (res.data && res.data.giftDisclosureDocuments) || [],
          })
        } else {
          toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
          callback({
            status: false
          })
        }
      })
    }
  }

  onStatusChange = async (e, doc, callback) => {
    const {disclosureDetails} = this.state
    let document = {}
    let type = ""
    const {name, value} = (e && e.target) || {}
    if(name){
      document = {
        _id: doc._id, //eslint-disable-line
        [name]: value
      }
      type = "docStatus"
    }else {
      document = {
        ...doc
      }
      type = "updateMeta"
    }
    putGiftsGrautitiesDocuments(`&giftsId=${disclosureDetails._id}`, `?details=${type}`, document, (res)=> {
      if (res && res.status === 200) {
        toast("Document status has been updated!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        this.onAuditSave("document")
        if(name){
          callback({
            status: true,
          })
        }else {
          callback({
            status: true,
            documentsList: (res.data && res.data.giftDisclosureDocuments) || [],
          })
        }
      } else {
        toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        callback({
          status: false
        })
      }
    })
  }

  onDeleteDoc = (documentId, callback) => {
    const {disclosureDetails} = this.state
    pullGiftsGratuitiesDetails("giftDisclosureDocuments", disclosureDetails._id, documentId, (res) => {      if (res && res.status === 200) {
      toast("Document removed successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
      this.onAuditSave("document")
      callback({
        status: true,
        documentsList: (res.data && res.data.giftDisclosureDocuments) || [],
      })
    } else {
      toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
      callback({
        status: false
      })
    }
    })
  }

  onCheck = (e) => {
    const {giftsCheckBoxes} = this.state
    const {user} = this.props
    if(e && e.target && e.target.name && e.target.title){
      this.props.addAuditLog({userName: user.userFirstName, log: `Change ${e.target.title} ${giftsCheckBoxes[e.target.name] ? "Yes" : "No"} to ${e.target.checked ? "Yes" : "No" }`, date: new Date(), key: "details"})
    }
    this.setState({
      giftsCheckBoxes: {
        ...giftsCheckBoxes,
        [e.target.name]: e.target.type === "checkbox" ? e.target.checked : e.target.value
      }
    })
  }

  onUserChange = (user) => {
    this.setState({
      disclosureInfo: {
        ...this.state.disclosureInfo,
        userId: user.id,
        userName: user.name
      }
    })
  }

  onActionSave = () => {
    const {disclosureInfo, giftsCheckBoxes} = this.state
    this.setState({
      isSaveDisabled: true
    }, () => {
      postGiftsGratuitiesInfo({ ...disclosureInfo, ...giftsCheckBoxes }, (res) => {
        toast("Disclosure has been updated successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
        if(res && res.status === 200) {
          this.setState(prevState => ({
            isSaveDisabled: false,
            tempDisclosureInfo: disclosureInfo,
            disclosureDetails: {
              ...res.data
            },
            disclosureInfo: {
              ...disclosureInfo,
              status: res.data.status || prevState.disclosureInfo.status || ""
            },
          }), () => {
            this.onAuditSave("details")
          })
        } else {
          toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
          this.setState({
            isSaveDisabled: false,
          })
        }
      })
    })
  }

  onSaveCheckDetails = () => {
    const {giftsCheckBoxes, disclosureDetails, supervisorNote} = this.state
    const {svControls, user} = this.props
    if (supervisorNote) {
      giftsCheckBoxes.notes = {
        userName: `${user.userFirstName} ${user.userLastName}`,
        note: supervisorNote,
        supervisor: svControls.supervisor,
      }
      this.props.addAuditLog({userName: user.userFirstName, log: `add notes ${supervisorNote} by ${user.userFirstName} ${user.userLastName}`, date: new Date(), key: "details"})
    }

    if(disclosureDetails && disclosureDetails._id) {
      this.setState({
        isSaveDisabled: {
          giftsGratuitiesCheck: true
        }
      }, () => {
        postCheckGiftsGratuities(`${disclosureDetails._id}?type=details`, giftsCheckBoxes, (res) => {
          if(res && res.status === 200) {
            toast("Gifts And Gratuities updated disclosure successfully",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
            this.setState(prevState => ({
              isSaveDisabled: {
                ...prevState.isSaveDisabled,
                giftsGratuitiesCheck: false
              },
              giftsCheckBoxes: {
                giftsNotSubjectToGenLimitation: (res && res.data && res.data.giftsNotSubjectToGenLimitation) || false,
                giftsOfferingProceedsUseProhibition: (res && res.data && res.data.giftsOfferingProceedsUseProhibition) || false,
                giftsPermittedNonCashCompArrangements: (res && res.data && res.data.giftsPermittedNonCashCompArrangements) || false,
                giftsAffirmNoPayment: (res && res.data && res.data.giftsAffirmNoPayment) || false,
                giftsAffirmUseOfProceeds: (res && res.data && res.data.giftsAffirmUseOfProceeds) || false,
                giftsAffirmCompletionOfG20ForPeriod: (res && res.data && res.data.giftsAffirmCompletionOfG20ForPeriod) || false,
              },
              disclosureDetails: {
                ...prevState.disclosureDetails,
                notes: res.data.notes || [],
                ...res.data
              },
              supervisorNote: "",
              disclosureInfo: {
                ...prevState.disclosureInfo,
                status: res.data.status || prevState.disclosureInfo.status || ""
              },
            }),() => {
              this.onAuditSave("details")
            })
          } else {
            toast("Something went wrong!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
            this.setState(prevState => ({
              isSaveDisabled: {
                ...prevState.isSaveDisabled,
                giftsGratuitiesCheck: false
              }
            }))
          }
        })
      })
    }else {
      this.onActionSave()
    }
  }

  onUsersListRefresh = () => {
    fetchAssigned(this.props.user.entityId, (users)=> {
      this.setState({
        dropDown: {
          ...this.state.dropDown,
          usersList: users.usersList || [],
        }
      })
    })
  }

  onMakeDisclosureSave = (status) => {
    const { disclosureInfo } = this.state
    const {user} = this.props
    delete disclosureInfo.G37Obligation
    delete disclosureInfo.affirmationNotes
    disclosureInfo.status = status
    this.props.addAuditLog({ userName: `${user.userFirstName} ${user.userLastName}`, log: `${status === "Complete" ? "Make Disclosure" : "Send for Pre-approval"}`, date: new Date(), key: "MakeDisclosure" })
    this.setState({
      loading: true
    }, () => {
      postGiftsGratuitiesInfo(disclosureInfo,async (res) => {
        if (res && res.status === 200) {
          this.onAuditSave("MakeDisclosure")
          try{
            await updateCACPoliticalActionStatus(getToken(), {...disclosureInfo, type: "gifts"})
          } catch (err) {
            console.log(err)
          }
          toast("Disclosure has been updated successfully", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
          this.props.history.push("/compliance/cmp-sup-gifts/dashboard")
        } else {
          toast("Something went wrong!", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
          this.setState({
            loading: false,
          })
        }
      })
    })
  }

  getQuarterDate = (quarter, year) => ({
    start: moment(moment().month(parseInt(quarter, 10)*3 - 3).year(parseInt(year, 10)).startOf("month")).format("YYYY-MM-DD"),
    end: moment(moment().month(parseInt(quarter, 10)*3 - 1).year(parseInt(year, 10)).endOf("month")).format("YYYY-MM-DD")
  })

  render() {
    const { loading, dropDown, sections, giftsCheckBoxes, currentQuarter, currentYear, status, disclosureDetails, tables, auditLogs, documentsList,
      errorMessages, contributor, disclosureInfo, isSearchDisabled, isSaveDisabled, supervisorNotesShow, supervisorNote} = this.state
    const {svControls, user} = this.props
    const flag = (parseInt(disclosureInfo.year,10) > currentYear) || ((parseInt(disclosureInfo.quarter,10) > currentQuarter) && (parseInt(disclosureInfo.year,10) === currentYear))
    let canEdit = disclosureDetails && disclosureDetails.userOrEntityId  && user ? disclosureDetails.userOrEntityId === user.userId && disclosureDetails.status === "Pending" : true
    const isDisclose = disclosureDetails && disclosureDetails.userOrEntityId  && user && disclosureDetails.userOrEntityId === user.userId && (disclosureDetails.status === "Pending" || disclosureDetails.status === "Approved")
    canEdit = svControls.supervisor && disclosureInfo.disclosureFor !== user.userId ? false : canEdit

    const isMakeDisclose = (disclosureDetails && disclosureDetails.giftsToRecipients && !!disclosureDetails.giftsToRecipients.length) || false
    const staticField = {
      docCategory: "Gifts And Gratuities",
      docSubCategory: "Gifts, Gratuities Non-Cash Compensation and Expenses of Issuance",
    }
    const value = (val) => {
      const quarter = [{name: "First", value: 1 }, {name: "Second", value: 2}, {name: "Third", value: 3}, {name: "Fourth", value: 4}]
      const qt = quarter.find(q => q.value === parseInt(val,10))
      return qt.name
    }
    let quarterDate = ""
    if(disclosureInfo && disclosureInfo.quarter && disclosureInfo.year){
      quarterDate = this.getQuarterDate(disclosureInfo.quarter, disclosureInfo.year)
    }
    if(loading) {
      return <Loader/>
    }
    return (
      <div>
        <div>
          <Accordion multiple activeItem={[0]} boxHidden render={({activeAccordions, onAccordion}) =>
            <div>
              <div className="columns">
                <div className="column">
                  <p>
                    <small>Rule G-20 Gifts, Gratuities, Non-Cash Compensation and Expenses of Issuance </small>
                    <a href="https://s3.amazonaws.com/munivisor-platform-static-documents/Rule+G-20+Gifts%2C+Gratuities%2C+Non-Cash+Compensation+and+Expenses+of+Issuance.pdf" target="_blank"><i className="fas fa-info-circle"/></a>
                  </p>
                </div>
              </div>
              <div className="columns">
                <div className="column">
                  <p className="multiExpLbl ">Disclosure for</p>
                  <div className="control">
                    <div style={{fontSize: 12}}>
                      {svControls.supervisor ?
                        <DropdownList filter value={disclosureInfo.disclosureFor || []} data={dropDown.someUsers || []} textField="name" valueField="id" key="name" onChange={(user) => this.onChange(user, "disclosureFor")} />
                        : <span>{`${user.userFirstName} ${user.userLastName}` || []}</span>}
                      {svControls.supervisor &&
                        <div style={{fontSize: 12,marginTop: 5}}>
                          <a className="has-text-link" style={{fontSize: 12,marginTop: -10}} target="_blank" href="/addnew-contact" >Cannot find contact?</a>
                          <i className="fa fa-refresh" style={{fontSize: 15, marginTop: -7, cursor: "pointer", padding: "0 0 0 10px"}} onClick={this.onUsersListRefresh}/>
                        </div>}
                    </div>
                  </div>
                </div>
                <div className="column">
                  <p className="multiExpLbl">Report Quarter</p>
                  <div className="select is-small is-fullwidth is-link">
                    <select name="quarter" value={disclosureInfo.quarter || ""} onChange={this.onChange}>
                      <option value="">Pick</option>
                      {dropDown.polContribQuarterOfDisclosureFirm.map((q) => <option key={q.value} value={q.value} disabled={status === "Pre-approval" ?
                        !(currentYear < disclosureInfo.year) ? q.value <= currentQuarter : false
                        : q.value > currentQuarter}>{q.name}</option>)}
                    </select>
                  </div>
                </div>
                <div className="column">
                  <p className="multiExpLbl">Report Year</p>
                  <div className="select is-small is-fullwidth is-link">
                    <select name="year" value={disclosureInfo.year || ""} onChange={this.onChange}>
                      <option value="">Pick</option>
                      {dropDown.polContribYearOfDisclosureFirm.map((value) => <option key={value} value={value} disabled={status === "Pre-approval" ? currentQuarter === 4 ?  value <= currentYear : value < currentYear : value > currentYear}>{value}</option>)}
                    </select>
                  </div>
                </div>
                {disclosureDetails && disclosureDetails._id && svControls.supervisor && disclosureDetails.status === "Pre-approval" &&
                  <SelectLabelInput title="Disclosure status" label="Supervisor's Action" error= "" list={dropDown.supervisorAction} name="status" value={disclosureInfo.status} disabled={disclosureInfo.status === "Complete"}  onChange={this.onChange} /> }
              </div>
              {
                isSearchDisabled ? <Loader/> :
                  <div>
                    {
                      disclosureDetails.noDisclosure ? <div className="title innerPgTitle"><strong> No disclosure for this quarter  </strong></div> :
                        <div>
                          <EditableTable {...this.props} tables={tables}
                            minDate={(quarterDate && quarterDate.start) || ""}
                            maxDate={(quarterDate && quarterDate.end) || ""}
                            canSupervisorEdit={canEdit}
                            contextType={ContextType.supervisor.giftsAndGrautities} dropDown={dropDown}
                            onSave={this.onSave} onRemove={this.onRemove}
                            onParentChange={this.onDropDownChange}/>
                          {disclosureDetails && disclosureDetails._id ?
                            <DocumentPage {...this.props} onSave={this.onDocSave} tranId={this.props.nav2}
                              title="Related Documents" pickCategory="LKUPDOCCATS"
                              pickSubCategory="LKUPCORRESPONDENCEDOCS" onStatusChange={this.onStatusChange}
                              isDisabled={!canEdit} onDeleteDoc={this.onDeleteDoc}
                              pickAction="LKUPDOCACTION" pickType="LKUPPOLCONTDOCTYPE"
                              staticField={staticField} category="giftDisclosureDocuments"
                              documents={documentsList}
                              contextType={ContextType.supervisor.giftsAndGrautities}
                              tableStyle={{fontSize: "smaller"}}/>
                            : null}
                          {
                            sections.map((section, i) => (
                              <RatingSection key={i} onAccordion={() => onAccordion(i + 1)} title={section.title}>
                                {activeAccordions.includes(i + 1) &&
                              <GiftsAndGrautitiesCheck item={section} onCheck={this.onCheck} state={giftsCheckBoxes}
                                status={status} canEdit={canEdit}/>
                                }
                              </RatingSection>
                            ))
                          }
                          <br/>
                          <div className="columns">
                            <div className="column">
                              <label className="checkbox">
                                <p className="multiExpLbl">
                                  <input type="checkbox" name="giftsAffirmNoPayment" title="Gifts Affirm No Payment"
                                    checked={giftsCheckBoxes.giftsAffirmNoPayment || false} onClick={this.onCheck}
                                    onChange={this.onCheck} disabled={!canEdit}/>
                                &nbsp;I/we confirm that I/we did not directly or indirectly accept or make payments or
                                offers of payments of any non-cash compensation
                                in connection with the sale and distribution of a primary offering of municipal
                                securities.
                                </p>
                              </label>
                            </div>
                          </div>
                          <div className="columns">
                            <div className="column">
                              <label className="checkbox">
                                <p className="multiExpLbl">
                                  <input type="checkbox" name="giftsAffirmUseOfProceeds"
                                    title="Gifts Affirm Use Of Proceeds"
                                    checked={giftsCheckBoxes.giftsAffirmUseOfProceeds || false}
                                    onClick={this.onCheck} onChange={this.onCheck} disabled={!canEdit}/>
                                &nbsp;I/we confirm that I/we have read and understood Prohibition of Use of Offering
                                Proceeds as defined in Rule G-20 section (e).
                                At the time of making this disclosure, I/we are not aware of instances where I/we are
                                requesting or obtaining reimbursement of costs and expenses related
                                to the entertainment of any person, including, but not limited to, any official or other
                                personnel of the municipal entity or personnel of the obligated person,
                                from the proceeds of such offering of municipal securities as defined in Rule G-20
                                section (e).
                                </p>
                              </label>
                            </div>
                          </div>
                          {
                            disclosureDetails && disclosureDetails._id &&
                          <div>
                            <a className="button is-link is-small"
                              onClick={() => this.setState({supervisorNotesShow: !supervisorNotesShow})}>{`${supervisorNotesShow ? "Hide" : "Show"}`} All
                              Notes</a>
                            {
                              supervisorNotesShow && Array.isArray(disclosureDetails.notes) &&
                              <table className="table is-striped is-fullwidth">
                                <TableHeader cols={cols}/>
                                <tbody>
                                  {
                                    disclosureDetails.notes.length ? disclosureDetails.notes.map((note, i) => (
                                      <tr key={i}>
                                        <td className="emmaTablesTd">
                                          <small>{`${note.userName} ${note.supervisor ? "(Supervisor)" : ""}`}</small>
                                        </td>
                                        <td className="emmaTablesTd" style={{width: "70%"}}>
                                          <small>{note.note}</small>
                                        </td>
                                        <td className="emmaTablesTd">
                                          <small>{note.createdDate ? moment(note.createdDate).format("MM-DD-YYYY h:mm A") : ""}</small>
                                        </td>
                                      </tr>
                                    )) : null
                                  }
                                </tbody>
                              </table>
                            }
                            {
                              canEdit || (svControls.supervisor && (disclosureDetails._id || disclosureInfo.disclosureFor === user.userId)) ?
                                <div className="columns">
                                  <div className="column is-full">
                                    <p className="multiExpLbl">Supervisor Notes/Instructions</p>
                                    <div className="control">
                                      <textarea className="textarea" value={supervisorNote || ""} name="supervisorNote" disabled={(!canEdit && !(svControls.supervisor && (disclosureDetails._id || disclosureInfo.disclosureFor === user.userId)) || false)}
                                        onChange={(e) => this.onChangeItem(e.target.value, e.target.name)}/>
                                    </div>
                                  </div>
                                </div> : null
                            }
                          </div>
                          }

                          <div className="column is-full">
                            <div className="field is-grouped-center">
                              <div className="control">
                                {canEdit || (svControls.supervisor && (disclosureDetails._id || disclosureInfo.disclosureFor === user.userId)) ?
                                  <button className="button is-link" onClick={this.onSaveCheckDetails}
                                    disabled={isSaveDisabled.giftsGratuitiesCheck || false}>Save
                                  </button>
                                  : null}
                              </div>
                              {  isDisclose ?
                                <div className="control">
                                  {status === "Pre-approval" && flag && disclosureDetails.status === "Pending" ?
                                    <button className="button is-link"
                                      disabled={isSaveDisabled.giftsGratuitiesCheck || !isMakeDisclose || false}
                                      onClick={() => this.onMakeDisclosureSave("Pre-approval")}>Send for
                                      Pre-approval</button>
                                    : !flag && (disclosureDetails.status === "Pending" || disclosureDetails.status === "Approved") && <button className="button is-link"
                                      disabled={isSaveDisabled.giftsGratuitiesCheck || !isMakeDisclose || false}
                                      onClick={() => this.onMakeDisclosureSave("Complete")}>Make
                                      disclosure</button>
                                  }
                                </div> : null
                              }
                            </div>
                          </div>

                          <RatingSection onAccordion={() => onAccordion(4)} title="Activity Log"
                            style={{overflowY: "scroll", fontSize: "smaller"}}>
                            {activeAccordions.includes(4) &&
                          <Audit auditLogs={auditLogs || []}/>
                            }
                          </RatingSection>
                        </div>
                    }
                  </div>
              }
            </div>
          }
          />
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(Disclosure)
export default connect(mapStateToProps, null)(WrappedComponent)
