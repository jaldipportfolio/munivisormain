import React, { Component } from "react"
import swal from "sweetalert"
import {withRouter} from "react-router-dom"
import Disclaimer from "../../../../../../GlobalComponents/Disclaimer"
import Questions from "../../../../../../GlobalComponents/Questions"
import CONST from "../../../../../../../globalutilities/consts"

class CheckStepTwo extends Component {
  constructor(props) {
    super(props)
    this.state = {
      negativeAnswers: CONST.Section.negativeAnswers,
      positiveAnswers: CONST.Section.positiveAnswers,
      disclosureInfo : CONST.SuperVisor.disclosureInfo || {},
    }
  }

  onChange = (e) => {
    this.setState({
      disclosureInfo: {
        ...this.state.disclosureInfo,
        [e.target.name]: e.target.type === "checkbox" ? e.target.checked : e.target.value
      }
    })
  }

  onChangeItem = (key, rows, value) => {
    if((key === "positiveAnswers" && value === "Yes") || (key === "negativeAnswers" && value === "No")) {
      swal("You will be required to make disclosure", {
        buttons: ["Cancel", "OK"],
      }).then((ok) => {
        if (ok) {
          this.props.history.push("/compliance/cmp-sup-political/disclosure")
        }
      })
    }else {
      this.setState({
        [key]: rows
      })
    }
  }

  render() {
    const { negativeAnswers, positiveAnswers} = this.state
    return (
      <div id="main">
        <Questions
          title="Section 1" onSave={() => {}}
          bodyTitle="Disclosure is required if your response to any of the below questions is NO."
          questions={negativeAnswers} category="negativeAnswers" onChangeItem={this.onChangeItem}
        />
        <Questions
          title="Section 2" onSave={() => {}}
          bodyTitle="Disclosure is required if your response to any of the below questions is Yes."
          questions={positiveAnswers} category="positiveAnswers" onChangeItem={this.onChangeItem}
        />
        <hr />
        <Disclaimer/>
      </div>
    )
  }
}

export default withRouter(CheckStepTwo)
