import React from "react"
import {Link} from  "react-router-dom"

const WhatYouWant = () => (
  <div>
    <div className="columns">
      <div className="column is-vcentered">
        <p className="title link-innerPgTitle">
          <a className="link-hover" href="../../../../../../../public/docs/NAMAG37CHART2.pdf" download target="_blank">See</a>
          <strong> the NAMA published Rule G-37 Snapshot </strong>
        </p>
      </div>
    </div>
    <div className="columns">
      <div className="column is-vcentered">
        <p className="title link-innerPgTitle">
          <Link to="/compliance/check-step-one" className="link-hover" target="_blank">Check</Link>
          <strong> if a contribution you are planning is allowed? </strong>
        </p>
      </div>
    </div>
    <div className="columns">
      <div className="column is-vcentered">
        <p className="title link-innerPgTitle">
          <Link to="/compliance/check-step-two" className="link-hover" target="_blank">Check</Link>
          <strong> if the political contributions you already made or intend to make needs to be disclosed. </strong>
        </p>
      </div>
    </div>
    <div className="columns">
      <div className="column is-vcentered">
        <p className="title link-innerPgTitle">
          <Link to="/compliance/cmp-sup-political/disclosure?status=Pre-approval" className="link-hover" target="_blank">Pre-approval</Link>
          <strong> from your supervisor before you make a contribution.
          </strong>
        </p>
      </div>
    </div>
    <div className="columns">
      <div className="column is-vcentered">
        <p className="title link-innerPgTitle">
          <Link to="/compliance/complete" className="link-hover" target="_blank">Confirm</Link>
          <strong> that you have <u>NOT</u> made any political contributions last
            quarter and complete your G-37 obligation.
          </strong>
        </p>
      </div>
    </div>
    <div className="columns">
      <div className="column is-vcentered">
        <p className="title link-innerPgTitle">
          <Link to="/compliance/cmp-sup-political/disclosure" className="link-hover" target="_blank">Complete</Link>
          <strong> G-37 disclosure obligation.
          </strong>
        </p>
      </div>
    </div>
  </div>
)
export default WhatYouWant
