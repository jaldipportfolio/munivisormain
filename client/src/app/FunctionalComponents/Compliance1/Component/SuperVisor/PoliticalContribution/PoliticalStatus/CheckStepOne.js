import React, { Component } from "react"
import {withRouter} from "react-router-dom"
import swal from "sweetalert"
import Disclaimer from "../../../../../../GlobalComponents/Disclaimer"
import Questions from "../../../../../../GlobalComponents/Questions"
import CONST from "../../../../../../../globalutilities/consts"

class CheckStepOne extends Component {
  constructor(props) {
    super(props)
    this.state = {
      positiveAnswers: CONST.Section.positiveAnswers,
      disclosureInfo : CONST.SuperVisor.disclosureInfo || {},
    }
  }

  onChange = (e) => {
    this.setState({
      disclosureInfo: {
        ...this.state.disclosureInfo,
        [e.target.name]: e.target.type === "checkbox" ? e.target.checked : e.target.value
      }
    })
  }

  onChangeItem = (key, rows, value) => {
    if(key === "positiveAnswers" && value === "Yes") {
      swal("You will be required to make disclosure", {
        buttons: ["Cancel", "OK"],
      }).then((ok) => {
        if (ok) {
          this.props.history.push("/compliance/cmp-sup-political/disclosure")
        }
      })
    }else {
      this.setState({
        [key]: rows
      })
    }
  }

  render() {
    const { positiveAnswers} = this.state
    return (
      <div id="main">
        <Questions
          title="Please answer the below questions." onSave={() => {}}
          bodyTitle="Contribution is prohibited if your response to any of the below questions is YES."
          questions={positiveAnswers} category="positiveAnswers" onChangeItem={this.onChangeItem}
        />
        <hr />
        <Disclaimer/>
      </div>
    )
  }
}

export default withRouter(CheckStepOne)
