import Joi from "joi-browser"
import dateFormat from "dateformat"

const entityByStateSchema = (minDate) => Joi.object().keys({
  // userId: Joi.string().allow(""),
  state: Joi.string().required(),
  /* userName: Joi.string().required(), */
  // contributionReceipientDetails: Joi.object({
  //   userId: Joi.string().allow(""),
  //   userFirstName: Joi.string().required(),
  //   userLastName: Joi.string().required(),
  //   userAddressConsolidated: Joi.string().required(),
  // }).required(),
  userAddressConsolidated: Joi.string().required(),
  contributorCategory: Joi.string().required(),
  contributionAmount: Joi.number().required(),
  contributionDate: Joi.date().example(new Date("2016-01-01")).min(dateFormat(minDate || new Date(), "yyyy-mm-dd")).required(),
  exemptionG37RuleExemption: Joi.string().allow("").optional(),
  createdDate: Joi.date().required().optional(),
  _id: Joi.string().required().optional(),
})

export const EntityByStatePolContribution = (inputTransDistribute, minDate) => Joi.validate(inputTransDistribute, entityByStateSchema(minDate), { abortEarly: false, stripUnknown:false })

const paymentSchema = (minDate) => Joi.object().keys({
  userId: Joi.string().allow(""),
  userEntityId: Joi.string().allow(""),
  state: Joi.string().required(),
  politicalPartyDetails: Joi.object({
    partyName: Joi.string().required(),
    partyAddress: Joi.string().required(),
  }),
  contributorCategory: Joi.string().required(),
  paymentAmount: Joi.number().required(),
  paymentDate: Joi.date().example(new Date("2016-01-01")).min(dateFormat(minDate || new Date(), "yyyy-mm-dd")).required(),
  createdDate: Joi.date().required().optional(),
  _id: Joi.string().required().optional(),
})

export const PaymentPolPartiesValidate = (inputTransDistribute, minDate) => Joi.validate(inputTransDistribute, paymentSchema(minDate), { abortEarly: false, stripUnknown:false })

const ballotSchema = (minDate) => Joi.object().keys({
  userId: Joi.string().allow(""),
  userEntityId: Joi.string().allow(""),
  state: Joi.string().required(),
  ballotDetail: Joi.string().required(),
  contributorCategory: Joi.string().required(),
  contributionAmount:Joi.number().required(),
  contributionDate:Joi.date().example(new Date("2016-01-01")).min(dateFormat(minDate || new Date(), "yyyy-mm-dd")).required(),
  createdDate: Joi.date().required().optional(),
  _id: Joi.string().required().optional(),
})

export const BallotContribValidate = (inputTransDistribute, minDate) => Joi.validate(inputTransDistribute, ballotSchema(minDate), { abortEarly: false, stripUnknown:false })

const reimbursementsSchema = (minDate) => Joi.object().keys({
  /* userId: Joi.string().allow(""), */
  state: Joi.string().required(),
  ballotDetail: Joi.string().required(),
  /* thirdPartyDetails: Joi.object({
    userId: Joi.string().allow(""),
    userFirstName: Joi.string().required(),
    userLastName: Joi.string().required(),
    userAddressConsolidated: Joi.string().required(),
  }).required(), */
  userAddressConsolidated: Joi.string().required(),
  thirdPartyCategory: Joi.string().required(),
  reimbursementAmt: Joi.number().required(),
  reimbursementDate: Joi.date().example(new Date("2016-01-01")).min(dateFormat(minDate || new Date(), "yyyy-mm-dd")).required(),
  createdDate: Joi.date().required().optional(),
  _id: Joi.string().required().optional(),
})

export const ReimbursementsValidate = (inputTransDistribute, minDate) => Joi.validate(inputTransDistribute, reimbursementsSchema(minDate), { abortEarly: false, stripUnknown:false })

const ballotApprovedSchema = (minDate) => Joi.object().keys({
  entityId: Joi.string().allow(""),
  municipalEntityName: Joi.string().required(),
  processDescription: Joi.string().required(),
  reportablesaleDate: Joi.date().example(new Date("2016-01-01")).min(dateFormat(minDate || new Date(), "yyyy-mm-dd")).required(),
  createdDate: Joi.date().required().optional(),
  _id: Joi.string().required().optional(),
})

export const BallotApprovedContribValidate = (inputTransDistribute, minDate) => Joi.validate(inputTransDistribute, ballotApprovedSchema(minDate), { abortEarly: false, stripUnknown:false })
