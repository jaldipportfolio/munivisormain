import Joi from "joi-browser"
import dateFormat from "dateformat"

const pqQualifiedPersons = (minDate) => Joi.object().keys({
  userId: Joi.string().required(),
  userFirstName: Joi.string().allow("").optional(),
  userLastName: Joi.string().allow("").optional(),
  userPrimaryEmailId: Joi.string().email().allow("").optional(),
  userPrimaryPhone: Joi.string().allow("").optional(),
  userOrgRole: Joi.string().required(),
  userLastTrainingAttendDate: !minDate ? Joi.date().example(new Date("2016-01-01")).max(dateFormat(new Date(), "yyyy-mm-dd")).required() :
    Joi.date().example(new Date("2016-01-01")).max(dateFormat(new Date(), "yyyy-mm-dd")).required(),
  userSeries50PassedOnDate: // !minDate ? Joi.date().example(new Date("2016-01-01")).max(dateFormat(new Date(), "yyyy-mm-dd")).required() :
    Joi.date().example(new Date("2016-01-01")).min(dateFormat(minDate || new Date(), "yyyy-mm-dd")).allow(""),
  userSeries50ValidEndDate: Joi.date().example(new Date("2016-01-01")).min(Joi.ref("userSeries50PassedOnDate")).allow(""),
  createdDate: Joi.date().required().optional(),
  _id: Joi.string().required().optional(),
})

export const PqQualifiedPersonsValidate = (inputTransDistribute, minDate) => Joi.validate(inputTransDistribute, pqQualifiedPersons(minDate), { abortEarly: false, stripUnknown:false })

const pqTrainingPrograms = (minDate) => Joi.object().keys({
  trgProgramName: Joi.string().required(),
  trgOrganizationDate: Joi.date().example(new Date("2016-01-01")).min(dateFormat(minDate || new Date(), "yyyy-mm-dd")).allow(""),
  trgConductedBy: Joi.object({
    id: Joi.string().required().optional(),
    name: Joi.string().required().optional(),
    userId: Joi.string().required(),
    userFirstName: Joi.string().required(),
    userLastName: Joi.string().required(),

  }).required(),
  trgAttendees: Joi.array().min(1).required(),
  trgNotes: Joi.string().required(),
  createdDate: Joi.date().required().optional(),
  _id: Joi.string().required().optional(),
})

export const PqTrainingProgramsValidate = (inputTransDistribute, minDate) => Joi.validate(inputTransDistribute, pqTrainingPrograms(minDate), { abortEarly: false, stripUnknown:false })
