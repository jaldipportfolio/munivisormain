import Joi from "joi-browser"
import dateFormat from "dateformat"

const designateFirmSchema = Joi.object().keys({
  userId: Joi.string().allow("").required(),
  userFirstName: Joi.string().required(),
  userLastName: Joi.string().required(),
  userPrimaryEmailId: Joi.string().regex(/^[a-z0-9](\.?[a-z0-9_-]){0,}@[a-z0-9-]+\.([a-z]{1,6}\.)?[a-z]{2,6}$/).required(),
  userPrimaryPhone: Joi.string().required(),
  userAddress: Joi.string().required(),
  userContactType: Joi.string().required(),
  _id: Joi.string().required().optional(),
})

export const PeopleDesignateValidate = (inputTransDistribute) => Joi.validate(inputTransDistribute, designateFirmSchema, { abortEarly: false, stripUnknown:false })

const qualifiedSchema = (minDate) => Joi.object().keys({
  userId: Joi.string().allow("").required(),
  userFirstName: Joi.string().required(),
  userLastName: Joi.string().required(),
  userPrimaryEmailId: Joi.string().regex(/^[a-z0-9](\.?[a-z0-9_-]){0,}@[a-z0-9-]+\.([a-z]{1,6}\.)?[a-z]{2,6}$/).required(),
  userPrimaryPhone: Joi.string().required(),
  profFeePaidOn: // !minDate ? Joi.date().example(new Date("2016-01-01")).max(dateFormat(new Date(), "yyyy-mm-dd")).required() :
    Joi.date().example(new Date("2016-01-01")).max(dateFormat(new Date(), "yyyy-mm-dd")).required(),
  series50PassDate: // !minDate ? Joi.date().example(new Date("2016-01-01")).max(dateFormat(new Date(), "yyyy-mm-dd")).required() :
    Joi.date().example(new Date("2016-01-01")).max(dateFormat(new Date(), "yyyy-mm-dd")).required(),
  series50ValidityEndDate: Joi.date().example(new Date("2016-01-01")).min(Joi.ref("series50PassDate")).required(),
  createdDate: Joi.date().required().optional(),
  _id: Joi.string().required().optional(),
})

export const PeopleQualifiedValidate = (inputTransDistribute, minDate) => Joi.validate(inputTransDistribute, qualifiedSchema(minDate), { abortEarly: false, stripUnknown:false })
