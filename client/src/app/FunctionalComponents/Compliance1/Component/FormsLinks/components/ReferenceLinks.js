import React from "react"
import Disclaimer from "../../../../../GlobalComponents/Disclaimer"
import RatingSection from "../../../../../GlobalComponents/RatingSection"
import Accordion from "../../../../../GlobalComponents/Accordion"

class ReferenceLinks extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  actionButtons = () =>  (
    <div className="field is-grouped">
      <div className="control">
        <button className="button is-link is-small">Add new link</button>
      </div>
    </div>
  )

  render() {

    return(
      <div id="main">

        <Accordion multiple boxHidden
          activeItem={[0,1]}
          render={({activeAccordions, onAccordion}) =>
            <div>
              <RatingSection onAccordion={() => onAccordion(0)} title="Search &amp; Manage Links"
                actionButtons={this.actionButtons} style={{overflowY: "unset"}}>
                {activeAccordions.includes(0) &&
                   <div className="accordion-body">
                     <div className="accordion-content" style={{padding: 0}}>

                       <div className="accordion-body">
                         <div className="accordion-content">

                           <div className="columns">
                             <div className="column">
                               <p className="control has-icons-left">
                                 <input className="input is-small is-link" type="text" placeholder="search"/>
                                 <span className="icon is-left has-background-dark">
                                   <i className="fas fa-search" />
                                 </span>
                               </p>
                             </div>
                           </div>

                           <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                             <thead>
                               <tr>
                                 <th>
                                   <p className="multiExpLbl ">Link Name</p>
                                 </th>
                                 <th>
                                   <p className="multiExpLbl ">Link URL</p>
                                 </th>
                                 <th>
                                   <p className="multiExpLbl ">Last update by</p>
                                 </th>
                                 <th>
                                   <p className="multiExpLbl ">Last update on</p>
                                 </th>
                                 <th>
                                   <p className="multiExpLbl ">Action</p>
                                 </th>
                               </tr>
                             </thead>
                             <tbody>
                               <tr>
                                 <td className="multiExpTblVal">
                                 MSRB EMMA
                                 </td>
                                 <td className="multiExpTblVal">
                                   <a href="https://emma.msrb.org/" target="new">https://emma.msrb.org/</a>
                                 </td>
                                 <td className="multiExpTblVal">
                                 Willy
                                 </td>
                                 <td className="multiExpTblVal">
                                 7.12.2018
                                 </td>
                                 <td className="multiExpTblVal">
                                   <div className="field is-grouped">
                                     <div className="control">
                                       <a href="">
                                         <span className="has-text-link">
                                           <i className="fas fa-pencil-alt" />
                                         </span>
                                       </a>
                                     </div>
                                     <div className="control">
                                       <a href="">
                                         <span className="has-text-link">
                                           <i className="far fa-trash-alt" />
                                         </span>
                                       </a>
                                     </div>
                                   </div>
                                 </td>
                               </tr>
                               <tr>
                                 <td className="multiExpTblVal">
                                 G-20 MSRB Rules and Guidance
                                 </td>
                                 <td className="multiExpTblVal">
                                   <a href="http://www.msrb.org/Rules-and-Interpretations/MSRB-Rules/General/Rule-G-20.aspx"
                                     target="new">http://www.msrb.org/Rules-and-Interpretations/MSRB-Rules/General/Rule-G-20.aspx</a>
                                 </td>
                                 <td className="multiExpTblVal">
                                 Jill Smith
                                 </td>
                                 <td className="multiExpTblVal">
                                 6.15.2018
                                 </td>
                                 <td className="multiExpTblVal">
                                   <div className="field is-grouped">
                                     <div className="control">
                                       <a href="">
                                         <span className="has-text-link">
                                           <i className="fas fa-pencil-alt" />
                                         </span>
                                       </a>
                                     </div>
                                     <div className="control">
                                       <a href="">
                                         <span className="has-text-link">
                                           <i className="far fa-trash-alt" />
                                         </span>
                                       </a>
                                     </div>
                                   </div>
                                 </td>
                               </tr>
                               <tr>
                                 <td className="multiExpTblVal">
                                 SEC FORM MA-I
                                 </td>
                                 <td className="multiExpTblVal">
                                   <a href="https://www.sec.gov/files/formma-i.pdf"
                                     target="new">https://www.sec.gov/files/formma-i.pdf</a>
                                 </td>
                                 <td className="multiExpTblVal">
                                 Jill Smith
                                 </td>
                                 <td className="multiExpTblVal">
                                 6.15.2018
                                 </td>
                                 <td className="multiExpTblVal">
                                   <div className="field is-grouped">
                                     <div className="control">
                                       <a href="">
                                         <span className="has-text-link">
                                           <i className="fas fa-pencil-alt" />
                                         </span>
                                       </a>
                                     </div>
                                     <div className="control">
                                       <a href="">
                                         <span className="has-text-link">
                                           <i className="far fa-trash-alt" />
                                         </span>
                                       </a>
                                     </div>
                                   </div>
                                 </td>
                               </tr>
                               <tr>
                                 <td className="multiExpTblVal">
                                 NAMA intepretation of MSRB RULE G-37
                                 </td>
                                 <td className="multiExpTblVal">
                                   <a href="../assets/docs/NAMAG37DOCUMENTJULY2018.pdf" target="new">NAMA intepretation of MSRB
                                   RULE G-37</a>
                                 </td>
                                 <td className="multiExpTblVal">
                                 System
                                 </td>
                                 <td className="multiExpTblVal">
                                 7.25.2018
                                 </td>
                                 <td className="multiExpTblVal">
                                   <div className="field is-grouped">
                                     <div className="control">
                                       <a href="">
                                         <span className="has-text-link">
                                           <i className="fas fa-pencil-alt" />
                                         </span>
                                       </a>
                                     </div>
                                     <div className="control">
                                       <a href="">
                                         <span className="has-text-link">
                                           <i className="far fa-trash-alt" />
                                         </span>
                                       </a>
                                     </div>
                                   </div>
                                 </td>
                               </tr>
                               <tr>
                                 <td className="multiExpTblVal">
                                 NAMA snapshot/chart of MSRB RULE G-37
                                 </td>
                                 <td className="multiExpTblVal">
                                   <a href="../assets/docs/NAMAG37CHART2.pdf" target="new">NAMA snapshot/chart of MSRB RULE
                                   G-37</a>
                                 </td>
                                 <td className="multiExpTblVal">
                                 System
                                 </td>
                                 <td className="multiExpTblVal">
                                 7.25.2018
                                 </td>
                                 <td className="multiExpTblVal">
                                   <div className="field is-grouped">
                                     <div className="control">
                                       <a href="">
                                         <span className="has-text-link">
                                           <i className="fas fa-pencil-alt" />
                                         </span>
                                       </a>
                                     </div>
                                     <div className="control">
                                       <a href="">
                                         <span className="has-text-link">
                                           <i className="far fa-trash-alt" />
                                         </span>
                                       </a>
                                     </div>
                                   </div>
                                 </td>
                               </tr>
                             </tbody>
                           </table>

                         </div>
                       </div>

                     </div>
                   </div>

                }
              </RatingSection>
            </div>
          }/>

        <hr/>
        <Disclaimer />

      </div>
    )
  }
}

export default ReferenceLinks
