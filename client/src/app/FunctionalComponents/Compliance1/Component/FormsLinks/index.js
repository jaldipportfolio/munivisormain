import React, { Component } from "react"
import { connect } from "react-redux"
import FormsTemplets from "./components/FormsTemplets"
import ReferenceLinks from "./components/ReferenceLinks"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"

class FormsLinks extends Component {

  constructor(props) {
    super(props)
    this.state = {
      tabActiveIndex: this.props.activeIndex || 0,
      TABS: [
        { label: "Forms and Templates",row:1,Component:FormsTemplets},
        { label: "Reference Links",row:1,Component:ReferenceLinks},
      ]
    }
    this.identifyComponentToRender = this.identifyComponentToRender.bind(this)
  }

  componentWillMount() {
    const {TABS} = this.state
    const {activeIndex} = this.props
    const {Component} = TABS[activeIndex || 0]


    this.setState({
      TABS,
      tabActiveIndex:activeIndex || 0,
      SelectedComponent:Component
    })
  }

  identifyComponentToRender(tab, ind){
    this.setState({SelectedComponent:tab.Component,tabActiveIndex:ind})
  }

  renderTabContents() {
    const {TABS,tabActiveIndex} =this.state
    const tabActive = TABS[tabActiveIndex].label
    return (<ul>
      {
        TABS.map ( (t,i) =>
          <li key={t.label} className={tabActive === t.label ? "is-active" : ""} onClick={() => this.identifyComponentToRender(t,i)}>
            <a className="tabSecLevel">
              <span>{t.label}</span>
            </a>
          </li>
        )
      }
    </ul>)
  }

  renderSelectedTabComponent() {
    const {SelectedComponent} = this.state
    return <SelectedComponent {...this.props} />
  }

  render() {
    const {user} = this.props
    return (
      <div id="main" className="column">
        <section className="container has-text-centered">
          <p className="title is-small">{(user && user.firmName) || ""}</p>
          <p className="multiExpLbl"><u>Establish &amp; maintain</u> compliance policies &amp; supervisory procedures.</p>
        </section>
        <hr/>
        <div className="tabs">
          {this.renderTabContents()}
        </div>
        {this.renderSelectedTabComponent()}
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

const WrappedComponent = withAuditLogs(FormsLinks)
export default connect(mapStateToProps, null)(WrappedComponent)
