import React from "react"
import PhoneInput, { formatPhoneNumber, isValidPhoneNumber } from "react-phone-number-input";
import DropDownListClear from "../../../../GlobalComponents/DropDownListClear"

const RfpSupplierSection = ({
  suppliersList,
  users,
  userEmails,
  canEditTran,
  item = {},
  onChangeItem,
  onRemove,
  errors = {},
  onBlur,
  onChangeError,
  index,
  onSave,
  onEdit,
  isEditable,
  category,
  isSaveDisabled,
  onPhoneChange,
  onCancel,
  tabIndex
}) => {
  isEditable = (isEditable[category] === index)

  const onChange = (event, isHide) => {
    if (event.target.name === "rfpParticipantFirmId") {
      onChangeItem({
        ...item,
        rfpParticipantFirmName: "",
        rfpParticipantContactName: "",
        rfpParticipantContactEmail: "",
        rfpParticipantContactPhone: "",
        rfpParticipantContactId: "",
        rfpParticipantRealFirmName: "",
        rfpParticipantRealMSRBType: "",
        rfpParticipantRealFirstName: "",
        rfpParticipantRealLastName: "",
        rfpParticipantRealEmailId: "",
        [event.target.name]:
          event.target.type === "checkbox"
            ? event.target.checked
            : event.target.value
      })
      if (isHide) {
        onChangeError("", "rfpParticipants", index, {
          rfpParticipantFirmName: "",
          rfpParticipantContactName: "",
          rfpParticipantContactEmail: "",
          rfpParticipantContactPhone: "",
          rfpParticipantContactId: "",
          rfpParticipantRealFirmName: "",
          rfpParticipantRealMSRBType: "",
          rfpParticipantRealFirstName: "",
          rfpParticipantRealLastName: "",
          rfpParticipantRealEmailId: ""
        })
      }
    } else {
      onChangeItem({
        ...item,
        [event.target.name]:
          event.target.type === "checkbox"
            ? event.target.checked
            : event.target.value
      }, "", event.target.type)
    }
    if (event.target.name && event.target.value) {
      onChangeError(event.target.name, "rfpParticipants", index)
    }
  }

  const onUserChange = (e, isHide) => {
    const user = users.find(x => x._id === e.target.value) || {}
    let email = ""
    let phone = ""
    if (user && Array.isArray(user.userEmails) && user.userEmails.length) {
      email = user.userEmails.filter(e => e.emailPrimary)
      email = email.length ? email[0].emailId : user.userEmails[0].emailId
    }
    if (user && Array.isArray(user.userPhone) && user.userPhone.length) {
      phone = user.userPhone.find(e => e.phonePrimary)
      phone = phone ? phone.phoneNumber : user.userPhone[0].phoneNumber
    }
    const error = phone ? (isValidPhoneNumber(phone) ? "" : "Invalid phone number") : ""
    onChangeItem(
      {
        ...item,
        rfpParticipantFirmName: (user.entityId && user.entityId.firmName) || "",
        rfpParticipantContactName: Object.keys(user).length === 0 ? "" : `${user.userFirstName} ${user.userLastName}`,
        rfpParticipantContactEmail: email || "",
        rfpParticipantContactPhone: phone || "",
        rfpParticipantContactId: user._id || "",
        rfpParticipantRealFirmName:
          (user.entityId && user.entityId.firmName) || "",
        rfpParticipantRealMSRBType:
          (user.entityId && user.entityId.msrbRegistrantType) || "",
        rfpParticipantRealFirstName: user.userFirstName,
        rfpParticipantRealLastName: user.userLastName,
        rfpParticipantRealEmailId: email || ""
      },
      `user change to ${user.userFirstName}`
    )
    if (user.entityId || isHide) {
      onChangeError("", "rfpParticipants", index, {
        rfpParticipantFirmName: "",
        rfpParticipantContactName: "",
        rfpParticipantContactEmail: "",
        rfpParticipantContactPhone: error || "",
        rfpParticipantContactId: "",
        rfpParticipantRealFirmName: "",
        rfpParticipantRealMSRBType: "",
        rfpParticipantRealFirstName: "",
        rfpParticipantRealLastName: "",
        rfpParticipantRealEmailId: ""
      })
    }
  }

  const onBlurInput = event => {
    if (event.target.title && event.target.value) {
      onBlur(
        `${event.target.title || "empty"} change to ${
          event.target.type === "checkbox"
            ? event.target.checked
            : event.target.name === "rfpParticipantContactId"
              ? item.rfpParticipantContactName
              : event.target.value || "empty"
        }`
      )
    }
  }

  const onSaveBtnClick = (e, index) => {
    if(e.keyCode === 13){
      onSave(true, "rfpParticipants", index)
    }
  }

  const onCancelBtnClick = (e) => {
    if(e.keyCode === 13){
      onCancel("rfpParticipants")
    }
  }

  const value = item && item.rfpParticipantContactPhone && item.rfpParticipantContactPhone.includes("+") || false
  const Internationals = item.rfpParticipantContactPhone && formatPhoneNumber(value ? item.rfpParticipantContactPhone : `+1${item.rfpParticipantContactPhone}`, "International") || "International"

  return (
    <tbody>
      <tr>
        <td>
          {
            isEditable ?
              <div className="field has-addons">
                <div className="control">
                  <DropDownListClear
                    filter
                    tabIndex={tabIndex}
                    data={suppliersList}
                    value={item.rfpParticipantFirmId}
                    textField="name"
                    valueField="id"
                    onChange={e => onChange({target: { name: "rfpParticipantFirmId", value: e.id }}) }
                    onClear={() => onChange({target: { name: "rfpParticipantFirmId", value: "" }}, true) }
                    disabled={!canEditTran}
                    isHideButton={item.rfpParticipantFirmId && canEditTran}
                  />
                  {errors.rfpParticipantFirmId && (
                    <p className="text-error">Required</p>
                  )}
                </div>
              </div>
              : <small>{item.rfpParticipantFirmName}</small>
          }
        </td>
        <td>
          {
            isEditable ?
              <div className="field has-addons">
                <div className="control">
                  <DropDownListClear
                    filter
                    tabIndex={tabIndex + 1}
                    data={users}
                    value={item.rfpParticipantContactId}
                    textField="name"
                    valueField="_id"
                    groupBy="userRole"
                    onChange={e => onUserChange({target: { name: "rfpParticipantContactId", value: e._id }}) }
                    onClear={() => onUserChange({target: { name: "rfpParticipantContactId", value: "" }}, true) }
                    disabled={!canEditTran}
                    isHideButton={item.rfpParticipantContactId && canEditTran}
                  />
                  {errors.rfpParticipantContactId && (
                    <p className="text-error">Required</p>
                  )}
                </div>
              </div>
              : <small>{item.rfpParticipantContactName}</small>
          }

          {/* <div className="select is-small is-link">
            <select
              title="Contact Database"
              value={item.rfpParticipantContactId}
              name="rfpParticipantContactId"
              style={{ width: 180 }}
              onChange={onUserChange}
              onBlur={onBlurInput}
              disabled={!canEditTran || !isEditable}
            >
              <option value="">Pick</option>
              {users.map(e => (
                <option key={e._id} value={e._id}>{`${e.userFirstName} ${
                  e.userLastName
                }`}</option>
              ))}
            </select>
            {errors.rfpParticipantContactId && (
              <p className="text-error">{errors.rfpParticipantContactId}</p>
            )}
          </div> */}
        </td>
        <td>
          {
            isEditable ?
              <div className="control">
                <input
                  tabIndex={tabIndex + 2}
                  title="Name"
                  value={item.rfpParticipantContactName}
                  name="rfpParticipantContactName"
                  onBlur={onBlurInput}
                  onChange={onChange}
                  className="input is-small is-link"
                  type="text"
                  placeholder="Picklist from CRM"
                  disabled={!canEditTran}
                />
                {errors.rfpParticipantContactName && (
                  <p className="text-error">Required</p>
                )}
              </div>
              : <small>{item.rfpParticipantContactName}</small>
          }
        </td>
        <td>
          {
            isEditable ?
              <div className="select is-small is-link">
                <select
                  tabIndex={tabIndex + 3}
                  title="Email"
                  value={item.rfpParticipantContactEmail}
                  name="rfpParticipantContactEmail"
                  style={{ width: 180 }}
                  onChange={onChange}
                  onBlur={onBlurInput}
                  disabled={!canEditTran}
                >
                  {userEmails.map(e => (
                    <option key={e}>{e}</option>
                  ))}
                </select>
                {errors.rfpParticipantContactEmail && (
                  <p className="text-error">Required/Valid</p>
                )}
              </div>
              : <small>{item.rfpParticipantContactEmail}</small>
          }
        </td>
        <td>
          {
            isEditable ?
              <div className="control">
                <PhoneInput
                  tabIndex={tabIndex + 4}
                  value={ Internationals || "" }
                  disabled={!canEditTran}
                  onChange={event => onPhoneChange(event, "rfpParticipants", index, "rfpParticipantContactPhone")}
                />
                {/* <NumberFormat
                  title="Phone"
                  format="+1 (###) ###-####"
                  mask="_"
                  className="input is-small is-link"
                  name="rfpParticipantContactPhone"
                  placeholder="Auto-populated as/if from CRM"
                  value={item.rfpParticipantContactPhone}
                  onBlur={event => onBlurInput(event)}
                  onChange={event => onChange(event)}
                  disabled={!canEditTran}
                /> */}
                {errors.rfpParticipantContactPhone && (
                  <p className="text-error">Required/Valid</p>
                )}
              </div>
              : <PhoneInput
                tabIndex={tabIndex + 4}
                value={ Internationals || "" }
                disabled
              />
          }
        </td>
        <td>
          <label className="checkbox"> {/* eslint-disable-line */}
            <input
              tabIndex={tabIndex + 5}
              title="Add to DL"
              type="checkbox"
              checked={item.rfpParticipantContactAddToDL || false}
              onBlur={onBlurInput}
              name="rfpParticipantContactAddToDL"
              onChange={() => {}}
              onClick={onChange}
              disabled={!canEditTran || !(item && item._id)}
            />
          </label>
        </td>
        {canEditTran ? (
          <td>
            <div className="field is-grouped">
              <div className="control">
                <a onClick={isEditable ? () => onSave(true, "rfpParticipants", index) : () => onEdit(category, index)}
                   className={`${isSaveDisabled ? "isDisabled" : ""}`}> {/* eslint-disable-line */}
                  <span className="has-text-link">
                    {isEditable ?
                      <i title="Save"
                        className="far fa-save"
                        tabIndex={tabIndex + 6}
                        onKeyDown={(e) => onSaveBtnClick(e, index)}/> :
                      <i title="Edit"
                        className="fas fa-pencil-alt"/>}
                  </span>
                </a>
              </div>
              <div className="control">
                <a onClick={isEditable ? () => onCancel("rfpParticipants") : onRemove}
                   className={`${isSaveDisabled ? "isDisabled" : ""}`}> {/* eslint-disable-line */}
                  <span className="has-text-link">
                    {isEditable ?
                      <i title="Cancel"
                        className="fa fa-times"
                        tabIndex={tabIndex + 7}
                        onKeyDown={onCancelBtnClick} /> :
                      <i title="Delete"
                        className="far fa-trash-alt"/>}
                  </span>
                </a>
              </div>
            </div>
          </td>
        ) : null}
      </tr>
    </tbody>
  )
}

export default RfpSupplierSection
