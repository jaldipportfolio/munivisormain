import React from "react"
import PhoneInput, {formatPhoneNumber, isValidPhoneNumber} from "react-phone-number-input"
import { SelectLabelInput } from "../../../../GlobalComponents/TextViewBox"
import DropDownListClear from "../../../../GlobalComponents/DropDownListClear"

const RfpEvaluationSection = ({
  usersList,
  item = {},
  dropDown,
  index,
  userEmails,
  canEditTran,
  onChangeItem,
  onRemove,
  errors = {},
  onBlur,
  onChangeError,
  onSave,
  onEdit,
  isEditable,
  category,
  isSaveDisabled,
  onPhoneChange,
  onCancel,
  tabIndex
}) => {
  isEditable = (isEditable[category] === index)

  const onChange = event => {
    onChangeItem({
      ...item,
      [event.target.name]:
        event.target.type === "checkbox"
          ? event.target.checked
          : event.target.value
    }, "", event.target.type)
    if (event.target.name && event.target.value) {
      onChangeError(event.target.name, "rfpEvaluationTeam", index)
    }
  }

  const onUserChange = (user, isHide) => {
    let email = ""
    let phone = ""
    if (user && Array.isArray(user.userEmails) && user.userEmails.length) {
      email = user.userEmails.filter(e => e.emailPrimary)
      email = email.length ? email[0].emailId : user.userEmails[0].emailId
    }
    if (user && Array.isArray(user.userPhone) && user.userPhone.length) {
      phone = user.userPhone.find(e => e.phonePrimary)
      phone = phone ? phone.phoneNumber : user.userPhone[0].phoneNumber
    }
    const error = phone ? (isValidPhoneNumber(phone) ? "" : "Invalid phone number") : ""
    onChangeItem(
      {
        ...item,
        rfpSelEvalContactId: Object.keys(user).length === 0 ? "" : user.id,
        rfpSelEvalFirmId: Object.keys(user).length === 0 ? "" : user.entityId,
        rfpSelEvalContactName: Object.keys(user).length === 0 ? "" : `${user.userFirstName} ${user.userLastName}`,
        rfpSelEvalEmail: email || "",
        rfpSelEvalPhone: phone || "",
        rfpSelEvalRealFirstName: user.userFirstName,
        rfpSelEvalRealLastName: user.userLastName,
        rfpSelEvalRealEmailId: email
      },
      `user change to ${user.userFirstName}`
    )
    if (user.id || isHide) {
      onChangeError("", "rfpEvaluationTeam", index, {
        rfpSelEvalContactId: "",
        rfpSelEvalFirmId: "",
        rfpSelEvalContactName: "",
        rfpSelEvalEmail: "",
        rfpSelEvalPhone: error || "",
        rfpSelEvalRealFirstName: "",
        rfpSelEvalRealLastName: "",
        rfpSelEvalRealEmailId: ""
      })
    }
  }

  const onBlurInput = event => {
    if (event.target.title && event.target.value) {
      onBlur(
        `${event.target.title || "empty"} change to ${
          event.target.type === "checkbox"
            ? event.target.checked
            : event.target.value || "empty"
        }`
      )
    }
  }

  const onSaveBtnClick = (e, data, rfp, index) =>{
    if(e.keyCode === 13){
      onSave( data, rfp, index)
    }
  }

  const onCancelBtnClick = (e,rfp) =>{
    if(e.keyCode === 13){
      onCancel(rfp)
    }
  }

  const value = item && item.rfpSelEvalPhone && item.rfpSelEvalPhone.includes("+") || false
  const Internationals = item.rfpSelEvalPhone && formatPhoneNumber(value ? item.rfpSelEvalPhone : `+1${item.rfpSelEvalPhone}`, "International") || "International"

  return (
    <tbody>
      <tr>
        <td>
          <div className="field has-addons">
            <div className="control">
              <DropDownListClear
                filter
                tabIndex={tabIndex}
                groupBy="group"
                data={usersList}
                value={item.rfpSelEvalContactName || item.rfpSelEvalContactId}
                textField="name"
                valueField="id"
                onChange={onUserChange}
                disabled={!canEditTran || !isEditable}
                isHideButton={item.rfpSelEvalContactId && isEditable && canEditTran}
                onClear={() => {onUserChange({}, true)}}
              />
              {errors.rfpSelEvalContactId && (
                <p className="text-error">Required</p>
              )}
            </div>
          </div>
        </td>
        <td>
          {
            isEditable ?
              <SelectLabelInput
                tabIndex={tabIndex + 1}
                title="Evaluator Role"
                error={errors.rfpSelEvalRole || ""}
                list={dropDown}
                value={item.rfpSelEvalRole}
                name="rfpSelEvalRole"
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={!canEditTran}
              />
              : <small>{item.rfpSelEvalRole}</small>
          }
        </td>
        <td>
          {
            isEditable ?
              <div className="control">
                <input
                  tabIndex={tabIndex + 2}
                  title="Name"
                  value={item.rfpSelEvalContactName}
                  name="rfpSelEvalContactName"
                  onChange={onChange}
                  onBlur={onBlurInput}
                  className="input is-small is-link"
                  type="text"
                  placeholder="Picklist from CRM"
                  disabled={!canEditTran}
                />
                {errors.rfpSelEvalContactName && (
                  <p className="text-error">Required</p>
                )}
              </div>
              : <small>{item.rfpSelEvalContactName}</small>
          }
        </td>
        <td>
          {
            isEditable ?
              <div className="select is-small is-link">
                <select
                  tabIndex={tabIndex + 3}
                  title="Email"
                  value={item.rfpSelEvalEmail}
                  name="rfpSelEvalEmail"
                  onChange={onChange}
                  onBlur={onBlurInput}
                  style={{ width: 180 }}
                  disabled={!canEditTran}
                >
                  {userEmails.map(e => (
                    <option key={e}>{e}</option>
                  ))}
                </select>
                {errors.rfpSelEvalEmail && (
                  <p className="text-error">Required/Valid</p>
                )}
              </div>
              : <small>{item.rfpSelEvalEmail}</small>
          }
        </td>
        <td>
          {
            isEditable ?
              <div className="control">
                <PhoneInput
                  tabIndex={tabIndex + 4}
                  value={ Internationals || "" }
                  disabled={!canEditTran}
                  onChange={event => onPhoneChange(event, "rfpEvaluationTeam", index, "rfpSelEvalPhone")}
                />
                {/* <NumberFormat
                  title="Phone"
                  format="+1 (###) ###-####"
                  mask="_"
                  className="input is-small is-link"
                  name="rfpSelEvalPhone"
                  placeholder="Auto-populated as/if from CRM"
                  value={item.rfpSelEvalPhone}
                  onBlur={event => onBlurInput(event)}
                  onChange={event => onChange(event)}
                  disabled={!canEditTran}
                /> */}
                {errors.rfpSelEvalPhone && (
                  <p className="text-error">Required/Valid</p>
                )}
              </div>
              : <PhoneInput
                tabIndex={tabIndex + 4}
                value={ Internationals || "" }
                disabled
              />
          }
        </td>
        <td>
          <label className="checkbox"> {/* eslint-disable-line */}
            <input
              tabIndex={tabIndex + 5}
              title="Add to DL"
              type="checkbox"
              checked={item.rfpSelEvalAddToDL || false}
              name="rfpSelEvalAddToDL"
              onBlur={onBlurInput}
              onChange={() => {}}
              onClick={onChange}
              disabled={!canEditTran || !(item && item._id)}
            />
          </label>
        </td>
        {canEditTran ? (
          <td>
            <div className="field is-grouped">
              <div className="control">
                <a onClick={isEditable ? () => onSave(true, "rfpEvaluation", index) : () => onEdit(category, index)}
                   className={`${isSaveDisabled ? "isDisabled" : ""}`}> {/* eslint-disable-line */}
                  <span className="has-text-link">
                    {isEditable ?
                      <i title="Save"
                        tabIndex={tabIndex + 6}
                        className="far fa-save"
                        onKeyDown={(e) => onSaveBtnClick(e, true, "rfpEvaluation", index)}/> :
                      <i title="Edit"
                        className="fas fa-pencil-alt" />}
                  </span>
                </a>
              </div>
              <div className="control">
                <a onClick={isEditable ? () => onCancel("rfpEvaluationTeam") : onRemove}
                   className={`${isSaveDisabled ? "isDisabled" : ""}`}> {/* eslint-disable-line */}
                  <span className="has-text-link">
                    {isEditable ?
                      <i title="Cancel"
                        tabIndex={tabIndex + 7}
                        className="fa fa-times"
                        onKeyDown={(e) => onCancelBtnClick(e, "rfpEvaluationTeam")}/> :
                      <i title="Delete"
                        className="far fa-trash-alt" />}
                  </span>
                </a>
              </div>
            </div>
          </td>
        ) : null}
      </tr>
    </tbody>
  )
}

export default RfpEvaluationSection
