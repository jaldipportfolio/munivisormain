import React from "react"
import PhoneInput, {formatPhoneNumber, isValidPhoneNumber} from "react-phone-number-input"
import { SelectLabelInput } from "../../../../GlobalComponents/TextViewBox"
import DropDownListClear from "../../../../GlobalComponents/DropDownListClear"

const RfpContactsSection = ({
  usersList,
  userEmails,
  dropDown,
  index,
  item = {},
  canEditTran,
  onChangeItem,
  onRemove,
  errors = {},
  onBlur,
  onChangeError,
  onSave,
  onEdit,
  isEditable,
  category,
  isSaveDisabled,
  onPhoneChange,
  onCancel,
  tabIndex
}) => {
  isEditable = (isEditable[category] === index)

  const onChange = event => {
    onChangeItem(
      {
        ...item,
        [event.target.name]:
          event.target.type === "checkbox"
            ? event.target.checked
            : event.target.value
      },
      `${event.target.name || "empty"} change to ${
        event.target.type === "checkbox"
          ? event.target.checked
          : event.target.value
      }`, event.target.type
    )
    if (event.target.name && event.target.value) {
      onChangeError(event.target.name, "rfpProcessContacts", index)
    }
  }

  const onUserChange = (user, isHide) => {
    let email = ""
    let phone = ""
    if (user && Array.isArray(user.userEmails) && user.userEmails.length) {
      email = user.userEmails.filter(e => e.emailPrimary)
      email = email.length ? email[0].emailId : user.userEmails[0].emailId
    }
    if (user && Array.isArray(user.userPhone) && user.userPhone.length) {
      phone = user.userPhone.find(e => e.phonePrimary)
      phone = phone ? phone.phoneNumber : user.userPhone[0].phoneNumber
    }
    const error = phone ? (isValidPhoneNumber(phone) ? "" : "Invalid phone number") : ""
    onChangeItem(
      {
        ...item,
        rfpProcessContactId: Object.keys(user).length === 0 ? "" : user.id,
        rfpProcessFirmId: Object.keys(user).length === 0 ? "" : user.entityId,
        rfpContactName: Object.keys(user).length === 0 ? "" : `${user.userFirstName} ${user.userLastName}`,
        rfpContactEmail: email || "",
        rfpContactPhone: phone || "",
        rfpProcessContactRealFirstName: user.userFirstName || "",
        rfpProcessContactRealLastName: user.userLastName || "",
        rfpProcessContactRealEmailId: email || ""
      },
      `user change to ${user.userFirstName}`
    )

    if (user.id || isHide) {
      onChangeError("", "rfpProcessContacts", index, {
        rfpProcessContactId: "",
        rfpProcessFirmId: "",
        rfpContactName: "",
        rfpContactEmail: "",
        rfpContactPhone: error || "",
        rfpProcessContactRealFirstName: "",
        rfpProcessContactRealLastName: "",
        rfpProcessContactRealEmailId: ""
      })
    }
  }

  const onBlurInput = event => {
    if (event.target.title && event.target.value) {
      onBlur(
        `${event.target.title || "empty"} change to ${
          event.target.type === "checkbox"
            ? event.target.checked
            : event.target.value || "empty"
        }`
      )
    }
  }

  const onSaveBtnClick = (e, index) =>{
    if(e.keyCode === 13){
      onSave(true, "rfpProcess", index)
    }
  }

  const onCancelBtnClick = (e) =>{
    if(e.keyCode === 13){
      onCancel("rfpProcessContacts")
    }
  }

  const value = item && item.rfpContactPhone && item.rfpContactPhone.includes("+") || false
  const Internationals = item.rfpContactPhone && formatPhoneNumber(value ? item.rfpContactPhone : `+1${item.rfpContactPhone}`, "International") || "International"

  return (
    <tbody>
      <tr>
        <td>
          <div className="field has-addons">
            <div className="control">
              <DropDownListClear
                filter
                tabIndex={tabIndex}
                groupBy="group"
                data={usersList}
                value={item.rfpContactName || item.rfpProcessContactId}
                textField="name"
                valueField="id"
                onChange={onUserChange}
                disabled={!canEditTran || !isEditable}
                isHideButton={item.rfpProcessContactId && isEditable && canEditTran}
                onClear={() => {onUserChange({}, true)}}
              />
              {errors.rfpProcessContactId && (
                <p className="text-error">Required</p>
              )}
            </div>
          </div>
        </td>
        <td>
          {
            isEditable ?
              <SelectLabelInput
                tabIndex={tabIndex + 1}
                title="Contact For"
                error={errors.rfpContactFor || ""}
                list={dropDown}
                value={item.rfpContactFor}
                name="rfpContactFor"
                onChange={onChange}
                onBlur={onBlurInput}
                disabled={!canEditTran}
              />
              : <small>{item.rfpContactFor}</small>
          }
        </td>
        <td>
          {
            isEditable ?
              <div className="control">
                <input
                  tabIndex={tabIndex + 2}
                  title="Name"
                  value={item.rfpContactName}
                  name="rfpContactName"
                  onChange={onChange}
                  onBlur={onBlurInput}
                  className="input is-small is-link"
                  type="text"
                  placeholder="Picklist from CRM"
                  disabled={!canEditTran}
                />
                {errors.rfpContactName && <p className="text-error">Required</p>}
              </div>
              : <small>{item.rfpContactName}</small>
          }
        </td>
        <td>
          {
            isEditable ?
              <div className="select is-small is-link">
                <select
                  tabIndex={tabIndex + 3}
                  title="Email"
                  value={item.rfpContactEmail}
                  name="rfpContactEmail"
                  style={{ width: 180 }}
                  onChange={onChange}
                  onBlur={onBlurInput}
                  disabled={!canEditTran}
                >
                  {userEmails.map(e => (
                    <option key={e}>{e}</option>
                  ))}
                </select>
                {errors.rfpContactEmail && (
                  <p className="text-error">Required/Valid</p>
                )}
              </div>
              : <small>{item.rfpContactEmail}</small>
          }
        </td>
        <td>
          {
            isEditable ?
              <div className="control">
                <PhoneInput
                  tabIndex={tabIndex + 4}
                  value={ Internationals || "" }
                  disabled={!canEditTran}
                  onChange={event => onPhoneChange(event, "rfpProcessContacts", index, "rfpContactPhone")}
                />
                {/* <NumberFormat
                  title="Phone"
                  format="+1 (###) ###-####"
                  mask="_"
                  className="input is-small is-link"
                  name="rfpContactPhone"
                  value={item.rfpContactPhone}
                  placeholder="Auto-populated as/if from CRM"
                  onBlur={event => onBlurInput(event)}
                  onChange={event => onChange(event)}
                  disabled={!canEditTran}
                /> */}
                {errors.rfpContactPhone && (
                  <p className="text-error">Required/Valid</p>
                )}
              </div>
              : <PhoneInput
                tabIndex={tabIndex + 4}
                value={ Internationals || "" }
                disabled
              />
          }
        </td>
        <td>
          <label className="checkbox"> {/* eslint-disable-line */}
            <input
              title="Add to DL"
              type="checkbox"
              tabIndex={isEditable ? tabIndex + 5 : null}
              checked={item.rfpContactAddToDL || false}
              name="rfpContactAddToDL"
              onBlur={onBlurInput}
              onChange={() => {}}
              onClick={onChange}
              disabled={!canEditTran || !(item && item._id)}
            />
          </label>
        </td>
        {canEditTran ? (
          <td>
            <div className="field is-grouped">
              <div className="control">
                <a onClick={isEditable ? () => onSave(true, "rfpProcess", index) : () => onEdit(category, index)}
                   className={`${isSaveDisabled ? "isDisabled" : ""}`}> {/* eslint-disable-line */}
                  <span className="has-text-link">
                    {isEditable ?
                      <i title="Save"
                        className="far fa-save"
                        tabIndex={tabIndex + 6}
                        onKeyDown={(e) => onSaveBtnClick(e, index)}/> :
                      <i title="Edit"
                        className="fas fa-pencil-alt"/>}
                  </span>
                </a>
              </div>
              <div className="control">
                <a onClick={isEditable ? () => onCancel("rfpProcessContacts") : onRemove}> {/* eslint-disable-line */}
                  <span className="has-text-link">
                    {isEditable ?
                      <i title="Cancel"
                        className="fa fa-times"
                        tabIndex={tabIndex + 7}
                        onKeyDown={onCancelBtnClick}/> :
                      <i title="Delete"
                        className="far fa-trash-alt"/>}
                  </span>
                </a>
              </div>
            </div>
          </td>
        ) : null}
      </tr>
    </tbody>
  )
}

export default RfpContactsSection
