import React from "react"
import {toast} from "react-toastify"
import {connect} from "react-redux"
import { withRouter } from "react-router-dom"
import ProcessChecklist from "../../../GlobalComponents/ProcessChecklist"
import { updateTransaction } from "../../../StateManagement/actions/TransactionDistribute"
import CONST from "../../../../globalutilities/consts"
import Loader from "../../../GlobalComponents/Loader"
import withAuditLogs from "../../../GlobalComponents/withAuditLogs"
import SendEmailModal from "../../../GlobalComponents/SendEmailModal"
import { sendEmailAlert } from "../../../StateManagement/actions/Transaction"

class CheckInTrack extends React.Component {
  constructor() {
    super()
    this.state = {
      checkLists: [],
      checklistId: "",
      participants: [],
      loading: true,
      modalState: false,
      email: {
        category: "",
        message: "",
        subject: "",
      }
    }
  }

  componentDidMount() {
    const {transaction} = this.props
    if (transaction) {
      const participants = []

      // if(transaction.rfpTranAssignedTo) {
      //   transaction.rfpTranAssignedTo.forEach(part => {
      //     participants.push({
      //       _id: part._id, // eslint-disable-line
      //       name: part.userFirstName,
      //       type: "RFP Assignees"
      //     })
      //   })
      // }
      if(transaction.rfpEvaluationTeam) {
        transaction.rfpEvaluationTeam.forEach(part => {
          participants.push({
            _id: part.rfpSelEvalContactId, // eslint-disable-line
            name: `${part.rfpSelEvalContactName}`,
            type: "Evaluation Team"
          })
        })
      }
      if(transaction.rfpProcessContacts) {
        transaction.rfpProcessContacts.forEach(part => {
          participants.push({
            _id: part.rfpProcessContactId, // eslint-disable-line
            name: `${part.rfpContactName}`,
            type: "RFP Contacts"
          })
        })
      }
      this.setState(prevState => ({
        participants,
        checkLists: (transaction && transaction.rfpBidCheckList) || [],
        loading: false,
        email: {
          ...prevState.email,
          subject: `Transaction - ${transaction.rfpTranIssueName || transaction.rfpTranProjectDescription} - Notification`
        }
      }))
    }else {
      this.setState({
        loading: false,
      })
    }
  }

  saveChecklist = (rfpBidCheckList, checklistId, checklistStatus) => {
    const {checkLists} = this.state
    if (rfpBidCheckList && Object.keys(rfpBidCheckList).length) {
      const data = (checkLists && checkLists.length) ? checkLists.find(e => e.id === rfpBidCheckList.id) : {}
      if(!(data && Object.keys(data).length)) {
        checkLists.push(rfpBidCheckList)
      }
    }
    this.setState({
      checkLists,
      rfpBidCheckList,
      checklistId,
      checklistStatus,
      modalState: true
    })
  }

  onConfirmationSave = () => {
    const {email, rfpBidCheckList, checklistId, checklistStatus } = this.state
    const tranId = this.props.nav2
    const type = this.props.nav1
    let url = window.location.pathname.replace("/","")
    if(checklistId) {
      url = `${url}?cid=${checklistId}`
    }
    const emailParams = {
      tranId,
      type,
      sendEmailUserChoice:true,
      emailParams: {
        url,
        ...email,
      }
    }
    console.log("==============email send to ==============", emailParams)
    this.setState({
      modalState: false
    }, () => {
      updateTransaction(this.props.nav2, this.props.nav3, {rfpBidCheckList, checklistId, checklistStatus}, (res)=> { //eslint-disable-line
        if (res && res.status === 200) {
          toast("RFP check-n-track has been updated!",{ autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
          this.setState({
            checkLists: (res.data && res.data.rfpBidCheckList) || [],
          }, async () => {
            await sendEmailAlert(emailParams)
            if (checklistStatus === "archive") {
              this.props.history.push(`/rfp/${tranId}/check-track`)
            }
          })
        } else {
          toast((res && res.error && res.error.response && res.error.response.data) || "Something went wrong!",
            { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        }
      })
    })
  }

  onModalChange = (state, name) => {
    if(name === "message"){
      state = {
        email: {
          ...this.state.email,
          ...state,
        }
      }
    }
    this.setState({
      ...state
    })
  }

  render() {
    const {loading, modalState, email} = this.state
    const {tranAction, participants, onParticipantsRefresh, transaction} = this.props
    if(loading) {
      return <Loader/>
    }
    return (
      <div>
        <SendEmailModal modalState={modalState} email={email} onModalChange={this.onModalChange} participants={participants} onParticipantsRefresh={onParticipantsRefresh} onSave={this.onConfirmationSave}/>
        <ProcessChecklist checklists={this.state.checkLists}
          totalThresholds={[0.20, 15000]}
          participants={this.state.participants || []}
          onSaveChecklist={this.saveChecklist}
          tenantId={transaction.rfpTranClientId}
          isDisabled={!tranAction.canTranStatusEditDoc}
        />
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})

const WrappedComponent = withAuditLogs(CheckInTrack)

export default withRouter(connect(mapStateToProps, null)(WrappedComponent))
