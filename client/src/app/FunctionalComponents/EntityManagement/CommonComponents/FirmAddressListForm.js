import React from "react"
import {toast} from "react-toastify"
import cloneDeep from "lodash.clonedeep"
import { checkEmptyElObject } from "GlobalUtils/helpers"
import { validatClientsDetail } from "Validation/clients"
import CONST from "GlobalUtils/consts"
import PhoneInput, { formatPhoneNumber, isValidPhoneNumber } from "react-phone-number-input"
import "react-phone-number-input/style.css"
import { emailRegex } from "Validation/common"
import swal from "sweetalert"
import SearchAddress from "../../GoogleAddressForm/GoogleAddressFormComponent"
import SearchCountry from "../../GoogleAddressForm/GoogleCountryComponent"
import {
  TextInput,
  SelectLabelInput,
  ZipCodeNumberDisableInput
} from "../../../GlobalComponents/TextViewBox"

export const Modal = ({ message, closeModal, modalState, onConfirmed }) => {
  if (!modalState) {
    return null
  }
  return (
    <div className="modal is-active">
      <div
        className="modal-background"
        onClick={closeModal}
        role="presentation"
        onKeyPress={() => { }}
      />
      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-title">Confirmation!</p>
        </header>
        <section className="modal-card-body">
          <div className="content">{message}</div>
        </section>
        <footer className="modal-card-foot">
          <div className="field is-grouped-center">
            <button className="button is-link" onClick={onConfirmed} >Yes</button> {/* eslint-disable-line */}
            <button className="button is-light" onClick={closeModal} >No</button> {/* eslint-disable-line */}
          </div>
        </footer>
      </div>
    </div>
  )
}

class AdmTrnAddressListForm extends React.Component {
  constructor(props) {
    super(props)
  }

  componentWillMount() {
    this.setState({ listAddressToggle: false, showAddress: false, modalState: false, confirmAlert: CONST.confirmAlert })
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.addressList === undefined ||
      (nextProps.addressList !== undefined &&
        nextProps.addressList.length === 0)
    ) {
      this.addNew()
    }
    this.setState((prevState) => ({
      ...prevState,
      ...nextProps,
      showAddress: nextProps && nextProps.showAddress || false,
      // listAddressToggle: true,
      addressList: nextProps && nextProps.addressList || [],
      businessAddress: this.initialAddresses()[0]
    }), () => {
      if(this.props.isCompliance && this.state.addressList && this.state.addressList.length && nextProps && nextProps.addressList && nextProps.addressList.length
        && nextProps.addressList[0].addressName){
        this.addNew()
      }
    })
  }

  initialAddresses() {
    return [
      {
        addressName: "",
        isPrimary: false,
        isHeadQuarter: false,
        website: "",
        officePhone: [
          {
            countryCode: "",
            phoneNumber: "",
            extension: ""
          }
        ],
        officeFax: [
          {
            faxNumber: ""
          }
        ],
        officeEmails: [{ emailId: "" }],
        addressLine1: "",
        addressLine2: "",
        country: "",
        state: "",
        city: "",
        zipCode: { zip1: "", zip2: "" }
      }
    ]
  }

  initialAddressPartOnly() {
    return {
      addressName: "",
      isPrimary: false,
      isHeadQuarter: false,
      addressLine1: "",
      addressLine2: "",
      country: "",
      state: "",
      city: "",
      zipCode: { zip1: "", zip2: "" }
    }
  }

  selectAddress = (id) => {
    this.state.isEdit = true
    const address = cloneDeep(this.state.addressList[id])

    this.setState(prevState => {
      let { errorFirmDetail } = prevState
      errorFirmDetail = this.initialAddresses()[0]
      const businessAddress = cloneDeep(address)
      const arr = ["officeEmails", "officePhone", "officeFax"]
      if (!this.state.isUserAddress) {
        arr.forEach(item => {
          if (address[item].length > 1) {
            for (let i = 1; i < address[item].length; i++) {
              errorFirmDetail[item].push(this.initialAddresses()[0][item][0])
            }
          }
        })
      }

      arr.forEach(item => {
        businessAddress[item] = (address[item] && address[item].length && address[item]) || this.initialAddresses()[0][item]
      })

      return {
        businessAddress,
        errorFirmDetail,
        showAddress: true,
        addressId: id
      }
    })
  }

  resetBussinessAddress = (e, showAddress) => {
    const {isCompliance} = this.props
    const addressInitial = this.initialAddresses()[0]
    const { addressList } = this.state
    if (addressList && addressList.filter(add => add.isPrimary).length === 0) {
      addressInitial.isPrimary = true
    } else if (addressList === undefined) {
      addressInitial.isPrimary = true
    }
    this.setState(prevState => ({
      ...prevState,
      businessAddress: isCompliance && addressList && addressList.length ?  addressList[0] : addressInitial ,
      showAddress,
      errorFirmDetail: {},
      isEdit: showAddress === true ? false : null
    }))
  }

  addNew = (e = null) => {
    this.state.isEdit = false
    const { addressList } = this.state
    this.setState({
      listAddressToggle: true
    })
    if (
      addressList &&
      addressList.length === 1 &&
      addressList[0].addressName === ""
    ) {
      addressList.splice(0, 1)
    }
    this.resetBussinessAddress(e, true)
  }

  addMore = (e, type) => {
    const {businessAddress} = this.state
    e.preventDefault()
    // const businessAddress = this.state.businessAddress
    // const errorFirmDetail = this.state.errorFirmDetail
    switch (type) {
    case "email":
      businessAddress.officeEmails.push({ emailId: "" })
      // errorFirmDetail.officeEmails.push({ emailId: "" })
      break
    case "phone":
      businessAddress.officePhone.push({
        countryCode: "",
        phoneNumber: "",
        extension: ""
      })
      /* errorFirmDetail.officePhone.push({
        countryCode: "",
        phoneNumber: "",
        extension: ""
      }) */
      break
    case "fax":
      businessAddress.officeFax.push({ faxNumber: "" })
      // errorFirmDetail.officeFax.push({ faxNumber: "" })
      break
    }
    this.setState(prevState => ({
      ...prevState,
      businessAddress
    }))
  }

  cancelAdd = (e, type) => {
    const {businessAddress, addressList, addressId} = this.state
    e.preventDefault()
    let unmodifiedAddress = this.initialAddresses()[0]
    if (addressId) {
      unmodifiedAddress = cloneDeep(
        addressList.find(item => item._id === addressId)
      )
    }
    // const businessAddress = this.state.businessAddress
    const name = type === "email" ? "officeEmails" : type === "phone" ? "officePhone" : type === "fax" ? "officeFax" : ""
    const localData = businessAddress[name].filter(f => f._id)
    switch (type) {
    case "email":
      businessAddress.officeEmails = (localData) || [{
        countryCode: "",
        phoneNumber: "",
        extension: ""
      }]
      break
    case "phone":
      businessAddress.officePhone = (localData) || [{ emailId: "" }]
      break
    case "fax":
      businessAddress.officeFax = (localData) || [{ faxNumber: "" }]
      break
    }
    this.setState(prevState => ({
      ...prevState,
      businessAddress
    }))
  }

  deleteAliases = (e, type, idx) => {
    e.preventDefault()
    const { businessAddress, errorFirmDetail } = this.state
    switch (type) {
    case "email":
      businessAddress.officeEmails.splice(idx, 1)
      if (errorFirmDetail.officeEmails && Object.keys(errorFirmDetail.officeEmails).length) {
        delete errorFirmDetail.officeEmails[idx]
      }
      break
    case "phone":
      businessAddress.officePhone.splice(idx, 1)
      if (errorFirmDetail.officePhone && Object.keys(errorFirmDetail.officePhone).length) {
        delete errorFirmDetail.officePhone[idx]
      }
      break
    case "fax":
      businessAddress.officeFax.splice(idx, 1)
      if (errorFirmDetail.officeFax && Object.keys(errorFirmDetail.officeFax).length) {
        delete errorFirmDetail.officeFax[idx]
      }
      break
    }
    this.setState(prevState => ({
      ...prevState,
      businessAddress
    }))
  }

  getAddressDetails = (address = "") => {
    if (address !== "") {
      const { businessAddress } = this.state
      let id = ""
      if (businessAddress._id !== "") {
        id = businessAddress._id
      }
      // businessAddress = { ...this.initialAddressPartOnly() }
      businessAddress._id = id
      if (address.addressLine1 !== "") {
        businessAddress.addressName = `${address.addressLine1}, ${address.city}` || ""
        businessAddress.addressLine1 = address.addressLine1.trim()
        /* errorFirmDetail.addressName = ""
        errorFirmDetail.addressLine1 = "" */
      }
      if (address.country !== "") {
        // errorFirmDetail.country = ""
        businessAddress.country = address.country.trim()
      }
      if (address.state !== "") {
        // errorFirmDetail.state = ""
        businessAddress.state = address.state.trim()
      }
      if (address.city !== "") {
        // errorFirmDetail.city = ""
        businessAddress.city = address.city.trim()
      }
      if (address.zipcode !== "") {
        // errorFirmDetail.zipCode.zip1 = ""
        if(businessAddress.zipCode){
          businessAddress.zipCode.zip1 = address.zipcode.trim().trim() || ""
          businessAddress.zipCode.zip2 = ""
        }else {
          businessAddress.zipCode = {
            zip1: address.zipcode.trim().trim() || "",
            zip2: "",
          }
        }
      }
      businessAddress.formatted_address = address.formatted_address
      businessAddress.url = address.url.trim()
      businessAddress.location = { ...address.location }

      this.setState(prevState => ({
        ...prevState,
        businessAddress,
        errorFirmDetail: {}
        /* errorFirmDetail: {
          ...prevState.errorFirmDetail,
          ...errorFirmDetail
        } */
      }))
    }
  }

  getCountryDetails = (cityStateCountry = "") => {
    const { businessAddress } = this.state
    if (cityStateCountry && cityStateCountry.country) {
      businessAddress.country = cityStateCountry.country.trim()
    }else {
      businessAddress.country = ""
    }
    if (cityStateCountry && cityStateCountry.state) {
      businessAddress.state = cityStateCountry.state.trim()
    }else {
      businessAddress.state = ""
    }
    if (cityStateCountry && cityStateCountry.city) {
      businessAddress.city =  cityStateCountry.city.trim()
    }else {
      businessAddress.city =  ""
    }
    if (cityStateCountry && cityStateCountry.zipcode) {
      businessAddress.zipCode.zip1 = cityStateCountry.zipcode.trim()
      businessAddress.zipCode.zip2 = cityStateCountry.zipcode.trim()
    }else {
      businessAddress.zipCode.zip1 = ""
      businessAddress.zipCode.zip2 = ""
    }
    this.setState({
      businessAddress
    })
  }

  deleteAddress = (idx) => {
    this.toggleModal()
    const { addressList } = this.state
    addressList.splice(idx, 1)
    this.setState({ addressList, listAddressToggle: true }, () => this.onSaveAddress())
  }

  onSaveAddress = () => {
    const {isEdit, isUserAddress} = this.state
    this.setState({ modalState: false })
    let { businessAddress } = this.state
    const { addressList, addressId, errorFirmDetail } = this.state
    const emptyErrorFirmDetail = this.initialAddresses()[0]
    // To Be Removed
    const { officeFax, officePhone  } = errorFirmDetail
    const phoneError = []
    if(officeFax !== undefined){
      Object.values(officeFax).map(m => {
        if(m.faxNumber){
          phoneError.push(m.faxNumber)
        }
      })
      if(phoneError && phoneError.length && phoneError.length < 2){
        if(phoneError[0] === "Duplicate values are not allowed"){
          phoneError.splice(0, 1)
        }
      }
    }
    if(officePhone !== undefined){
      Object.values(officePhone).map(m => {
        if(m.phoneNumber){
          phoneError.push(m.phoneNumber)
        }
      })
      if(phoneError && phoneError.length && phoneError.length < 2){
        if(phoneError[0] === "Duplicate values are not allowed"){
          phoneError.splice(0, 1)
        }
      }
    }
    if(phoneError && phoneError.length){
      return
    }
    addressList.map(add => {
      add.zipCode.zip2 = add.zipCode.zip2
        ? add.zipCode.zip2.length > 4
          ? add.zipCode.zip2.substring(0, 4)
          : add.zipCode.zip2
        : add.zipCode.zip2
      add.zipCode.zip1 = add.zipCode.zip1
        ? add.zipCode.zip1.length > 5
          ? add.zipCode.zip1.substring(0, 5)
          : add.zipCode.zip1
        : add.zipCode.zip1
    })
    if (businessAddress) {
      businessAddress.zipCode.zip2 = businessAddress.zipCode.zip2
        ? businessAddress.zipCode.zip2.length > 4
          ? businessAddress.zipCode.zip2.substring(0, 4)
          : businessAddress.zipCode.zip2
        : businessAddress.zipCode.zip2 || ""
      businessAddress.zipCode.zip1 = businessAddress.zipCode.zip1
        ? businessAddress.zipCode.zip1.length > 5
          ? businessAddress.zipCode.zip1.substring(0, 5)
          : businessAddress.zipCode.zip1
        : businessAddress.zipCode.zip1 || ""
      delete businessAddress.isActive
    }
    addressList.map(add => delete add.isActive)
    // To Be Removed

    let errorData
    if (
      businessAddress &&
      !checkEmptyElObject(businessAddress) &&
      isEdit !== null
    ) {
      const addressData = {
        _id: "",
        addresses: [businessAddress]
      }
      errorData = validatClientsDetail(addressData, "address")
    }
    if (errorData && errorData.error) {
      // let errorFirmDetails = { addresses: [emptyErrorFirmDetail] }
      // errorFirmDetails = convertError(errorFirmDetails, errorData)
      const errorMessages = {}
      const setArrIndex = ["officeEmails", "officePhone", "officeFax"]
      console.log("=======>", errorData.error.details)

      errorData.error.details.map(err => { //eslint-disable-line
        //eslint-disable-line
        if (errorMessages.hasOwnProperty(err.path[0])) { //eslint-disable-line
          //eslint-disable-line
          if (
            errorMessages[err.path[0]].hasOwnProperty(err.path[1]) || //eslint-disable-line
            setArrIndex.indexOf(err.path[2]) !== -1
          ) {
            //eslint-disable-line
            if (setArrIndex.indexOf(err.path[2]) !== -1) {
              if (
                errorMessages[err.path[0]][err.path[1]].hasOwnProperty( //eslint-disable-line
                  err.path[2]
                )
              ) {
                if (
                  errorMessages[err.path[0]][err.path[1]][
                    err.path[2]
                  ].hasOwnProperty(err.path[3]) //eslint-disable-line
                ) {
                  errorMessages[err.path[0]][err.path[1]][err.path[2]][
                    err.path[3]
                  ] = {
                    ...errorMessages[err.path[0]][err.path[1]][err.path[2]][
                      err.path[3]
                    ],
                    [err.path[4]]: err.message
                  }
                } else {
                  errorMessages[err.path[0]][err.path[1]][err.path[2]] = {
                    ...errorMessages[err.path[0]][err.path[1]][err.path[2]],
                    [err.path[3]]: {
                      [err.path[4]]: err.message
                    }
                  }
                }
              } else {
                errorMessages[err.path[0]][err.path[1]] = {
                  ...errorMessages[err.path[0]][err.path[1]],
                  [err.path[2]]: {
                    [err.path[3]]: {
                      [err.path[4]]: err.message
                    }
                  }
                }
              }
            } else if(err.path[2] === "zipCode" && (err.path[3] === "zip1" || err.path[3] === "zip2")){
                if(errorMessages[err.path[0]][err.path[1]].hasOwnProperty(err.path[2])){ //eslint-disable-line
                errorMessages[err.path[0]][err.path[1]][err.path[2]] ={
                  ...errorMessages[err.path[0]][err.path[1]][err.path[2]],
                  [err.path[3]]: err.message
                }
              }else {
                errorMessages[err.path[0]][err.path[1]] = {
                  ...errorMessages[err.path[0]][err.path[1]],
                  [err.path[2]]: {
                    [err.path[3]]: err.message
                  }
                }
              }
            }else {
              errorMessages[err.path[0]][err.path[1]][err.path[2]] = err.message
            }
          } else if (err.path[2]) {
            if(err.path[2] === "zipCode"){
              if(errorMessages[err.path[0]].hasOwnProperty(err.path[2])){ //eslint-disable-line
                errorMessages[err.path[0]][err.path[2]][err.path[3]] = err.message
              }else {
                errorMessages[err.path[0]] = {
                  [err.path[2]]: {
                    [err.path[3]]: err.message
                  }
                }
              }
            }else {
              errorMessages[err.path[0]][err.path[1]] = {
                [err.path[2]]: setArrIndex.indexOf(err.path[2]) !== -1 ? {} : err.message
              }
            }
          } else {
            errorMessages[err.path[0]][err.path[1]] = err.message
          }
        } else if (err.path[1] !== undefined && err.path[2] !== undefined) {
          if (setArrIndex.indexOf(err.path[2]) !== -1) {
            errorMessages[err.path[0]] = {
              [err.path[1]]: {
                [err.path[2]]: {
                  [err.path[3]]: {
                    [err.path[4]]: err.message
                  }
                }
              }
            }
          } else {
            if (err.path[2]) { //eslint-disable-line
              if (err.path[3]) {
                errorMessages[err.path[0]] = {
                  [err.path[1]]: {
                    [err.path[2]]: {
                      [err.path[3]]: setArrIndex.indexOf(err.path[3]) !== -1 ? {} : err.message
                    }
                  }
                }
              } else {
                errorMessages[err.path[0]] = {
                  [err.path[1]]: {
                    [err.path[2]]: setArrIndex.indexOf(err.path[2]) !== -1 ? {} : err.message
                  }
                }
              }
            } else {
              errorMessages[err.path[0]][err.path[1]] = err.message
            }
          }
        } else {
          if (err.path[1]) { //eslint-disable-line
            if (errorMessages.hasOwnProperty(err.path[0])) { //eslint-disable-line
              errorMessages[err.path[0]][err.path[1]] = err.message
            } else {
              errorMessages[err.path[0]] = {
                [err.path[1]]: err.message
              }
            }
          } else {
            errorMessages[err.path[0]] = err.message
          }
        }
      })

      console.log("*******errorMessages.addresses********", errorMessages.addresses["0"])
      return this.setState({ errorFirmDetail: errorMessages.addresses["0"] })
    }
    const error = {}
    const types = ["officePhone", "officeEmails", "officeFax"]

    types.forEach(o => {
      const key = o === "officePhone" ? "phoneNumber" : o === "officeEmails" ? "emailId" : o === "officeFax" ? "faxNumber" : ""
      const dupResult = this.checkDupInArray((businessAddress && businessAddress[o] && businessAddress[o].map(d => d[key])) || [])
      if(dupResult && dupResult.length > 1 && dupResult[0] > -1 && dupResult[1] > -1) {
        error[o] = { ...error[o] }
        error[o][dupResult[0]] = { ...error[o][dupResult[0]] }
        error[o][dupResult[0]][key] = "Duplicate values are not allowed"
        error[o][dupResult[1]] = { ...error[o][dupResult[1]] }
        error[o][dupResult[1]][key] = "Duplicate values are not allowed"
      }
    })
    if((error && error.officePhone) || (error && error.officeEmails) || (error && error.officeFax)){
      return this.setState({errorFirmDetail: error, waiting: false})
    }

    this.setState({ errorFirmDetail: emptyErrorFirmDetail })
    const emptyAddressIndex = addressList.findIndex(add =>
      checkEmptyElObject(add)
    )
    if (emptyAddressIndex !== undefined && emptyAddressIndex > -1)
      addressList.splice(emptyAddressIndex, 1)

    businessAddress && businessAddress.officeFax && businessAddress.officeFax.length && businessAddress.officeFax.forEach((item, idx) => {
      const value = item && item.faxNumber && item.faxNumber.includes("+") || false
      const Internationals = item.faxNumber && formatPhoneNumber(value ? item.faxNumber : `+1${item.faxNumber}`, "International") || "International"
      if(!value && item.faxNumber){
        businessAddress.officeFax[idx].faxNumber = Internationals || ""
        this.setState({businessAddress})
      }
    })

    businessAddress && businessAddress.officePhone && businessAddress.officePhone.length && businessAddress.officePhone.forEach((item, idx) => {
      const value = item && item.phoneNumber && item.phoneNumber.includes("+") || false
      const Internationals = item.phoneNumber && formatPhoneNumber(value ? item.phoneNumber : `+1${item.phoneNumber}`, "International") || "International"
      if(!value && item.phoneNumber){
        businessAddress.officePhone[idx].phoneNumber = Internationals || ""
        this.setState({businessAddress})
      }
    })

    businessAddress.officePhone = businessAddress.officePhone && businessAddress.officePhone.filter(phone => phone.phoneNumber || phone.extension)
    businessAddress.officeEmails = businessAddress.officeEmails && businessAddress.officeEmails.filter(email => email.emailId)
    businessAddress.officeFax = businessAddress.officeFax && businessAddress.officeFax.filter(fax => fax.faxNumber)

    if (isUserAddress) {
      businessAddress = this.removePropsForUserAddress(businessAddress)
    }

    if (!isEdit) {
      if (businessAddress && !checkEmptyElObject(businessAddress)) {
        addressList.push(businessAddress)
        if (this.checkForUniqueness(addressList)) {
          this.setState(prevState => ({ ...prevState, isEdit: false }))
          this.setState({
            errorFirmDetail: {}
          }, () => this.props.onSaveAddress(addressList))
        } else {
          addressList.splice(addressList.length - 1, 1)
        }
      } else if (this.checkForUniqueness(addressList))
        this.setState({
          errorFirmDetail: {}
        }, () => this.props.onSaveAddress(addressList))
    } else if (!checkEmptyElObject(businessAddress)) {
      addressList[addressId] = businessAddress
      if (this.checkForUniqueness(addressList))
        this.setState({
          errorFirmDetail: {}
        }, () => this.props.onSaveAddress(addressList))
    }
  }

  checkDupInArray(arr) {
    console.log("arr : ", arr)
    let result = [-1, -1]
    arr.some((e, i) => {
      if(e) {
        const dupIdx = arr.slice(i + 1).findIndex(d => d === e)
        if(dupIdx > -1) {
          result = [i, dupIdx+i+1]
          return true
        }
      }
    })
    console.log("dupResult : ", result)
    return result
  }

  removePropsForUserAddress = (businessAddress) => {
    delete businessAddress.officeEmails
    delete businessAddress.officeFax
    delete businessAddress.officePhone
    delete businessAddress.website
    delete businessAddress.isHeadQuarter

    return businessAddress
  }

  checkForUniqueness = addressList => {
    const {businessAddress} = this.state
    let primaryError = ""
    let uniqueAddressName = ""
    const noOfPrimaryAddresses = addressList.filter(add => add.isPrimary).length
    if (addressList.length !== 0) {
      if (noOfPrimaryAddresses !== 1) {
        primaryError = "We need exact one primary address"
      }
      /* addressList.forEach(add => {
        if (addressList.filter(checkAdd => add.addressName === checkAdd.addressName).length > 1) {
          uniqueAddressName = "Please have a unique address name"
        }
      }) */
      if (addressList.filter(checkAdd => businessAddress.addressName === checkAdd.addressName).length > 1) {
        uniqueAddressName = "Please have a unique address name"
      }
      this.setState(prevState => ({
        ...prevState,
        addressList,
        uniqueAddressName,
        primaryError
      }))
    }
    return primaryError === "" && uniqueAddressName === ""
  }

  onAddressPartChange(e, nameProp, idx, nextToClean) {
    const { value, name, type, checked } = e.target
    const { businessAddress, errorFirmDetail } = this.state
    if (idx === undefined || idx === null) {
      if (type === "checkbox") {
        businessAddress[name] = checked
      } else if (name === "zip1" || name === "zip2") {
        businessAddress.zipCode[name] = value.trim() || ""
      } else {
        businessAddress[name] = value
      }
    } else {
      // if (nameProp === "officePhone") {
      //   businessAddress[nameProp][idx].countryCode = value.split(" ")[0]
      // businessAddress[nameProp][idx].phoneNumber = value.replace("+1 ", "")
      // }
      if (nameProp === "officeEmails") {
        const emailErr = emailRegex.test(value)
        let error = {}
        if (!emailErr) {
          error = {
            ...errorFirmDetail,
            [nameProp]: {
              ...errorFirmDetail[nameProp],
              [idx]: {emailId: "Please enter valid email" || ""}
            }
          }
        } else {
          error = {
            ...errorFirmDetail,
            [nameProp]: {
              ...errorFirmDetail[nameProp],
              [idx]: {emailId: "" || ""}
            }
          }
        }
        this.setState({
          errorFirmDetail: error
        })
      }

      businessAddress[nameProp][idx][name] = value.trim()
    }
    if (nextToClean !== undefined) {
      nextToClean.forEach(item => (businessAddress[item] = ""))
    }
    this.setState(prevState => ({
      ...prevState,
      businessAddress
    }))
  }

  onPhoneNumberChange = (e, nameProp, idx, key) => {
    const { businessAddress } = this.state
    const Internationals = e && formatPhoneNumber(e, "International")
    const error = Internationals ? (isValidPhoneNumber(Internationals) ? undefined : "Invalid phone number") : ""
    businessAddress[nameProp][idx][key] = e || ""
    this.setState(prevState => ({
      ...prevState,
      businessAddress,
      errorFirmDetail: {
        ...prevState.errorFirmDetail,
        [nameProp]: {
          ...prevState.errorFirmDetail[nameProp],
          [idx]: {[key]: error || ""}
        }
      }
    }))
  }

  onPrimaryChange = (idx) => {
    this.toggleModal()
    const { addressList } = this.state
    const primaryIndex = addressList.findIndex(add => add.isPrimary)
    if (primaryIndex >= 0) {
      addressList[primaryIndex].isPrimary = false
    }
    addressList[idx].isPrimary = true
    this.setState(
      prevState => ({
        ...prevState,
        addressList,
        listAddressToggle: true
      }),
      () => this.onSaveAddress()
    )
  }

  useOfficeAddress = () => {
    let { businessAddress, officeAddress } = this.state
    delete officeAddress._id
    delete officeAddress.isPrimary
    businessAddress = cloneDeep(officeAddress)
    businessAddress._id = this.state.businessAddress._id
    businessAddress.isPrimary = this.state.businessAddress.isPrimary
    delete businessAddress.officeEmails
    delete businessAddress.officeFax
    delete businessAddress.officePhone
    delete businessAddress.website
    delete businessAddress.isHeadQuarter
    // businessAddress.officePhone = this.initialAddresses()[0].officePhone
    // businessAddress.officeEmails = this.initialAddresses()[0].officeFax
    // businessAddress.officeFax = this.initialAddresses()[0].officeFax
    if (!businessAddress.addressName) return toast("Office address not exists.", { autoClose: CONST.ToastTimeout, type: toast.TYPE.WARNING })
    this.setState({ businessAddress })
  }

  toggleButton = () => {
    const {addressList, listAddressToggle} = this.state
    if (addressList && addressList.length === 1 && addressList[0].addressName === "") {
      this.addNew()
    } else {
      this.setState({
        listAddressToggle: !listAddressToggle
      })
    }
  }

  toggleModal = () => {
    this.setState(prev => {
      const newState = !prev.modalState

      return { modalState: newState }
    })
  }

  showModal = (operation, id) => {
    const { errorFirmDetail } = this.state
    const { officeFax, officePhone } = errorFirmDetail
    const error = []
    if(officeFax !== undefined){
      Object.values(officeFax).map(m => {
        if(m.faxNumber){
          error.push(m.faxNumber)
        }
      })
    }
    if(officePhone !== undefined){
      Object.values(officePhone).map(m => {
        if(m.phoneNumber){
          error.push(m.phoneNumber)
        }
      })
    }
    if(error && error.length){
      return
    }
    this.setState({
      modalState: true,
      onConfirmed: operation === "primary" ? (() => this.onPrimaryChange(id))
        : ( (operation === "delete") ?  (() => this.deleteAddress(id)) : (() => this.onSaveAddress())),
      modalMessage: operation === "primary" ? "Are you sure you want to make this address as primary?"
        : ((operation === "delete") ? "Are you sure you want to delete this address?"
          : "Are you sure you want to save Address?")
    })
  }

  onReset = (key, index) => {
    const { businessAddress, errorFirmDetail } = this.state
    businessAddress[key].splice(index, 1)
    this.setState({
      businessAddress,
      errorFirmDetail: {
        ...errorFirmDetail,
        [key]: {
          ...errorFirmDetail[key],
          [index]: {[key]: ""}
        }
      }
    })
  }

  onOrganizationChange = (name, value) => {
    const { businessAddress } = this.state
    businessAddress[value] = (name && name.target && name.target.value) || ""
    this.setState({businessAddress})
  }

  onComplianceSave = (address) => {
    const {businessAddress} = this.state
    if(address && address[0] && (address[0].organizationType === undefined || !address[0].organizationType)){
      this.setState({
        complianceError: "Select Organization Type"
      })
      return
    }

    const error = {}
    const types = ["officePhone", "officeEmails", "officeFax"]
    types.forEach(o => {
      const key = o === "officePhone" ? "phoneNumber" : o === "officeEmails" ? "emailId" : o === "officeFax" ? "faxNumber" : ""
      const dupResult = this.checkDupInArray((businessAddress && businessAddress[o] && businessAddress[o].map(d => d[key])) || [])
      if(dupResult && dupResult.length > 1 && dupResult[0] > -1 && dupResult[1] > -1) {
        error[o] = { ...error[o] }
        error[o][dupResult[0]] = { ...error[o][dupResult[0]] }
        error[o][dupResult[0]][key] = "Duplicate values are not allowed"
        error[o][dupResult[1]] = { ...error[o][dupResult[1]] }
        error[o][dupResult[1]][key] = "Duplicate values are not allowed"
      }
    })
    if((error && error.officePhone) || (error && error.officeEmails) || (error && error.officeFax)){
      return this.setState({errorFirmDetail: error, waiting: false})
    }
    this.props.onSaveAddress(address)
  }

  onCheck = (e, key, index) => {
    const value = e.target.checked
    const {confirmAlert, businessAddress} = this.state
    confirmAlert.text = `You want to ${value ? "activate" : "deactivate"} this ${key === "officePhone" ? "Phone number" : 
      key === "officeFax" ? "Fax number" : "Email"} ?`
    swal(confirmAlert)
      .then(async(willDelete) => {
        if (willDelete) {
          businessAddress[key][index] = {
            ...businessAddress[key][index],
            isActive: value
          }
          this.setState({
            businessAddress
          })
        }
      })
  }

  render() {
    const { errorFirmDetail, listAddressToggle, businessAddress, modalState, modalMessage, onConfirmed, isUserAddress, addressList, showAddress, complianceError,
      phonePrimary, primaryError, uniqueAddressName, officeAddress } = this.state
    const {title, isCompliance, organizationTypeList, canEdit, tabIndex} = this.props
    return (
      <article
        className={
          listAddressToggle ? "accordion is-active" : "accordion"
        }
      >
        <div
          className="accordion-header"
          onClick={event => {
            this.toggleButton(event)
          }}
        >
          <Modal
            closeModal={this.toggleModal}
            modalState={modalState}
            message={modalMessage}
            onConfirmed={onConfirmed}
          />
          <p>{isUserAddress ? "Address" : title || "Business Contact Information"}</p>
          { canEdit && (
            <div className="is-pulled-right">
              <div className="field is-grouped">
                <div className="control">
                  <button // eslint-disable-line
                    tabIndex={tabIndex}
                    className="button is-link is-small"
                    onClick={event => {
                      event.stopPropagation()
                      this.addNew(event)
                    }}
                    disabled={isCompliance}
                  >
                    Add
                  </button>
                </div>
                <div className="control">
                  <button // eslint-disable-line
                    tabIndex={tabIndex + 1}
                    className="button is-light is-small"
                    onClick={event => {
                      event.stopPropagation()
                      this.resetBussinessAddress(event, false)
                    }}
                    disabled={isCompliance}
                  >
                    Reset
                  </button>
                </div>
              </div>
            </div>
          )}
          <i className={listAddressToggle ? "fas fa-chevron-down" : "fas fa-chevron-up"} />
        </div>
        {listAddressToggle && (
          <div className="accordion-body">
            {isCompliance ? null :
              <div>
                {addressList && addressList[0] &&
                  addressList[0].addressName !== "" ? (
                    <div className="box overflow-auto">
                      <table className="table is-bordered is-striped is-hoverable is-fullwidth">
                        <thead>
                          <tr>
                            <th>
                              <p className="multiExpLbl ">Address Name</p>
                            </th>
                            <th>
                              <p className="multiExpLbl ">Is Primary?</p>
                            </th>
                            <th>
                              <p className="multiExpLbl ">
                                {!canEdit ? "View" : "Update"}
                              </p>
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                          {addressList && addressList[0] &&
                            addressList[0].addressName !== ""
                            ? addressList.map((address, idx) => (
                              <tr key={idx.toString()}>
                                <td className="multiExpTblVal">
                                  {address.addressName}
                                </td>
                                <td className="multiExpTblVal">
                                  <p className="emmaTablesTd">
                                    <input
                                      name={`tablePrimary${idx}`}
                                      type="checkbox"
                                      value={address.isPrimary}
                                      checked={address.isPrimary}
                                      disabled={!this.state.canEdit}
                                      onChange={event => {
                                        if (!(event.target.checked === false && address.isPrimary === true)) {
                                          this.showModal("primary", idx)
                                        }
                                      }
                                      }
                                    />
                                  </p>
                                </td>
                                <td className="multiExpTblVal">
                                  <a> {/* eslint-disable-line */}
                                    <span className="has-text-link">
                                      {canEdit ? <i
                                        className="fas fa-pencil-alt is-small"
                                        onClick={event => {
                                          event.preventDefault()
                                          this.selectAddress(idx)
                                        }} /> :
                                        <i
                                          className="fas fa-plus-square is-small"
                                          onClick={event => {
                                            event.preventDefault()
                                            this.selectAddress(idx)
                                          }} />
                                      }
                                    </span>
                                    {"\u00A0\u00A0\u00A0"}
                                    {this.state.canEdit && !address.isPrimary ? (
                                      <span className="has-text-link">
                                        <i
                                          className="far fa-trash-alt is-small"
                                          onClick={event => {
                                            event.preventDefault()
                                            this.showModal("delete", idx)
                                          }}
                                        />
                                      </span>
                                    ) : null}
                                  </a>
                                </td>
                              </tr>
                            ))
                            : ""}
                        </tbody>
                      </table>
                    </div>
                  ) : null}
              </div>
            }
            {showAddress ? (
              <div className="box is-12 is-vertical is-parent">
                {canEdit && (
                  <SearchAddress
                    idx={0}
                    tabIndex={tabIndex + 2}
                    getAddressDetails={this.getAddressDetails}
                  />
                )}
                <div className="columns">
                  <TextInput
                    error={
                      (errorFirmDetail && errorFirmDetail.addressName) || ""
                    }
                    tabIndex={tabIndex + 3}
                    required
                    name="addressName"
                    type="text"
                    label="Address Name"
                    value={businessAddress.addressName || ""}
                    disabled={!canEdit}
                    onChange={event => {
                      this.onAddressPartChange(event, "addressName")
                    }}
                  />
                  <div className="column is-half">
                    { !isCompliance ?
                      <div className="column">
                        <div className="column is-half" style={{display: "inline-block"}}>
                          {businessAddress.isPrimary ? ( <b>This is a primary address</b> ) : ""}
                        </div>
                        <div className="column is-half" style={{display: "inline-block"}}>
                          {officeAddress && (
                            <button // eslint-disable-line
                              tabIndex={tabIndex + 4}
                              className="button is-link is-small"
                              onClick={() => {this.useOfficeAddress()}}
                            > Use office address </button>
                          )}
                        </div>
                      </div> :
                      <div className="column is-half" style={{marginTop: -25, marginLeft: -23}}>
                        <SelectLabelInput
                          required
                          tabIndex={tabIndex + 4}
                          label="Organization Type"
                          error={complianceError || ""}
                          list={organizationTypeList || []}
                          name="organizationType"
                          value={businessAddress.organizationType || ""}
                          onChange={event => this.onOrganizationChange(event, "organizationType")}
                          disabled={!canEdit}
                        />
                      </div>
                    }
                  </div>
                </div>

                <div className="columns">
                  <TextInput
                    required
                    error={(errorFirmDetail && errorFirmDetail.addressLine1) || ""}
                    tabIndex={tabIndex + 5}
                    type="text"
                    label="Address Line 1"
                    name="addressLine1"
                    placeholder="address line 1"
                    value={businessAddress.addressLine1 || ""}
                    onChange={event => {
                      this.onAddressPartChange(event, "addressLine1")
                    }}
                    disabled={!canEdit}
                  />
                  <TextInput
                    tabIndex={tabIndex + 6}
                    error={(errorFirmDetail && errorFirmDetail.addressLine2) || ""}
                    type="text"
                    label="Address Line 2"
                    name="addressLine2"
                    placeholder="address line 2"
                    value={businessAddress.addressLine2 || ""}
                    onChange={event => {
                      this.onAddressPartChange(event, "addressLine2")
                    }}
                    disabled={!canEdit}
                  />
                </div>
                <div className="columns">
                  <SearchCountry
                    idx={0}
                    tabIndex={tabIndex + 7}
                    value={businessAddress.country ? `${businessAddress.city}, ${businessAddress.state}, ${businessAddress.country}` : ""}
                    getCountryDetails={this.getCountryDetails}
                    error={
                      (errorFirmDetail && errorFirmDetail.city) || (errorFirmDetail && errorFirmDetail.state) || (errorFirmDetail && errorFirmDetail.country) || ""
                    }
                    disabled={!canEdit}
                  />
                  <div className="column" style={{ marginTop: 0 }}>
                    <p className="multiExpLbl">Zip Code<span className='has-text-danger'>*</span></p>
                    <div className="field is-grouped-left">
                      <div className="control d-flex">
                        <ZipCodeNumberDisableInput
                          name="zip1"
                          tabIndex={tabIndex + 8}
                          size="5"
                          type="text"
                          format="#####"
                          placeholder="Zip Code 1"
                          label=""
                          disabled={!canEdit}
                          error={(errorFirmDetail && errorFirmDetail.zipCode && errorFirmDetail.zipCode.zip1) || ""}
                          value={(businessAddress && businessAddress.zipCode && businessAddress.zipCode.zip1) || ""}
                          onChange={event => {this.onAddressPartChange(event, "zip1")}}
                        />
                      </div>
                      <div className="control d-flex">
                        <ZipCodeNumberDisableInput
                          name="zip2"
                          tabIndex={tabIndex + 9}
                          size="4"
                          type="text"
                          format="####"
                          placeholder="Zip Code 2"
                          label=""
                          disabled={!this.props.canEdit}
                          error={(errorFirmDetail && errorFirmDetail.zipCode && errorFirmDetail.zipCode.zip2) || ""}
                          value={(businessAddress && businessAddress.zipCode && businessAddress.zipCode.zip2) || ""}
                          onChange={event => {this.onAddressPartChange(event, "zip2")}}
                        />
                      </div>
                    </div>
                  </div>
                </div>

                {!isUserAddress &&
                  <span>
                    <hr />
                    <div className="columns">
                      <TextInput
                        tabIndex={tabIndex + 10}
                        error={(errorFirmDetail && errorFirmDetail.website) || ""}
                        name="website"
                        type="text"
                        label="Website(s)"
                        value={businessAddress.website || ""}
                        onChange={event => {
                          this.onAddressPartChange(event, "website")
                        }}
                        disabled={!canEdit}
                      />
                    </div>
                    <div className="columns">
                      <div className="column is-third">
                        <p className="multiExpLbl">Office Phone</p>
                        {businessAddress.officePhone.map(
                          (item, idx) => {
                            const phoneError = (errorFirmDetail && errorFirmDetail.officePhone && errorFirmDetail.officePhone[idx]) || {}
                            const value = item && item.phoneNumber && item.phoneNumber.includes("+") || false
                            const Internationals = item.phoneNumber && formatPhoneNumber(value ? item.phoneNumber : `+1${item.phoneNumber}`, "International") || ""
                            console.log({Internationals}, "phone", item.phoneNumber)
                            return (
                              <div className="field is-grouped-left">
                                <div className="control">
                                  <PhoneInput
                                    tabIndex={tabIndex + 11}
                                    value={ Internationals || "" }
                                    disabled={!canEdit || (item && item.isActive === undefined ? false : !item.isActive)}
                                    onChange={event => {this.onPhoneNumberChange(event,"officePhone",idx, "phoneNumber")}}
                                    error={
                                      phoneError.phoneNumber === "Duplicate values are not allowed" ? "Duplicate phone number are not allowed" : phoneError.phoneNumber ? "Enter valid phone number" : ""
                                    }/>
                                </div>
                                <div className="control d-flex">
                                  <ZipCodeNumberDisableInput
                                    tabIndex={tabIndex + 12}
                                    format="##########"
                                    className="input is-small is-link"
                                    name="extension"
                                    placeholder="Ext"
                                    size="10"
                                    error={phoneError.extension ? "Enter valid phone extension" : ""}
                                    value={item.extension || ""}
                                    disabled={!canEdit || (item && item.isActive === undefined ? false : !item.isActive)}
                                    onChange={event => {
                                      this.onAddressPartChange(
                                        event,
                                        "officePhone",
                                        idx
                                      )
                                    }}
                                  />
                                </div>
                                {canEdit ?
                                  <span>
                                    {(!phonePrimary && ((item && item._id) && !(phoneError && phoneError.phoneNumber) && Internationals)) &&
                                      <label className="checkbox"> {/* eslint-disable-line */}
                                        <input
                                          type="checkbox"
                                          name="isActive"
                                          tabIndex={tabIndex + 13}
                                          checked={(businessAddress && businessAddress.officePhone && businessAddress.officePhone[idx])
                                          && (businessAddress.officePhone[idx].isActive === undefined ? true : businessAddress.officePhone[idx].isActive)}
                                          onChange={(e) => this.onCheck(e, "officePhone", idx)}
                                          onClick={(e) => this.onCheck(e, "officePhone", idx)}
                                        />
                                      </label>
                                    /* <span className="has-text-link"
                                      onClick={() => this.onReset("officePhone", idx)}>
                                      <i className="far fa-trash-alt"/>
                                    </span> */
                                    }
                                  </span> : null
                                }
                              </div>
                              /* <div
                                className="field is-grouped-left"
                                key={parseInt(idx * 20, 10)}
                              >
                                <div className="control d-flex">
                                  <ZipCodeNumberDisableInput
                                    format="+1 (###) ###-####"
                                    mask="_"
                                    className="input is-small is-link"
                                    name="phoneNumber"
                                    size="15"
                                    value={item.phoneNumber || ""}
                                    disabled={!this.props.canEdit}
                                    error={phoneError.phoneNumber === "Duplicate values are not allowed" ? "Duplicate phone number are not allowed" : phoneError.phoneNumber ? "Enter valid phone number" : ""}
                                    onChange={event => {
                                      this.onAddressPartChange(
                                        event,
                                        "officePhone",
                                        idx
                                      )
                                    }}
                                  />
                                </div>
                                <div>
                                  <div className="control d-flex">
                                    <ZipCodeNumberDisableInput
                                      format="##########"
                                      className="input is-small is-link"
                                      name="extension"
                                      size="10"
                                      error={phoneError.extension ? "Enter valid phone extension" : ""}
                                      value={item.extension || ""}
                                      disabled={!this.props.canEdit}
                                      onChange={event => {
                                        this.onAddressPartChange(
                                          event,
                                          "officePhone",
                                          idx
                                        )
                                      }}
                                    />
                                  </div>
                                </div>
                              </div> */
                            )
                          }
                        )}
                        {canEdit && (
                          <div className="control">
                            <button // eslint-disable-line
                              tabIndex={tabIndex + 14}
                              className="button is-link is-small"
                              onClick={event => {
                                this.addMore(event, "phone")
                              }}
                            >
                              Add More
                            </button>
                            <button // eslint-disable-line
                              tabIndex={tabIndex + 15}
                              className="button is-light is-small"
                              onClick={event => {
                                this.cancelAdd(event, "phone")
                              }}
                            >
                                  Cancel
                            </button>
                          </div>
                        )}
                      </div>
                      <div className="column">
                        <p className="multiExpLbl">Office Fax</p>
                        {businessAddress.officeFax.map((item, idx) => {
                          const faxError = (errorFirmDetail && errorFirmDetail.officeFax && errorFirmDetail.officeFax[idx]) || {}
                          const value = item && item.faxNumber && item.faxNumber.includes("+") || false
                          const Internationals = item.faxNumber && formatPhoneNumber(value ? item.faxNumber : `+1${item.faxNumber}`, "International") || ""
                          console.log({Internationals}, "fax", item.faxNumber)
                          return (
                            <div className="field is-grouped-left">
                              <div className="control">
                                <PhoneInput
                                  tabIndex={tabIndex + 16}
                                  value={ Internationals || "" }
                                  disabled={!canEdit || (item && item.isActive === undefined ? false : !item.isActive)}
                                  onChange={event => {this.onPhoneNumberChange(event,"officeFax",idx, "faxNumber")}}
                                  error={
                                    faxError.faxNumber === "Duplicate values are not allowed" ? "Duplicate Fax number are not allowed" : faxError.faxNumber ? "Enter valid fax number" : ""
                                  }/>
                              </div>
                              { canEdit ?
                                <span>
                                  {(!phonePrimary && ((item && item._id) && !(faxError && faxError.faxNumber) && Internationals)) &&
                                  <label className="checkbox"> {/* eslint-disable-line */}
                                    <input
                                      type="checkbox"
                                      name="isActive"
                                      tabIndex={tabIndex + 17}
                                      checked={(businessAddress && businessAddress.officeFax && businessAddress.officeFax[idx])
                                      && (businessAddress.officeFax[idx].isActive === undefined ? true : businessAddress.officeFax[idx].isActive)}
                                      onChange={(e) => this.onCheck(e, "officeFax", idx)}
                                      onClick={(e) => this.onCheck(e, "officeFax", idx)}
                                    />
                                  </label>
                                  /* <span className="has-text-link"
                                    onClick={() => this.onReset("officeFax", idx)}>
                                    <i className="far fa-trash-alt"/>
                                  </span> */
                                  }
                                </span> : null
                              }
                            </div>
                            /* <div className="field" key={parseInt(idx * 10, 10)}>
                              <div className="control d-flex">
                                <ZipCodeNumberDisableInput
                                  format="+1 (###) ###-####"
                                  mask="_"
                                  className="input is-small is-link"
                                  name="faxNumber"
                                  minWidth="90%"
                                  error={faxError.faxNumber === "Duplicate values are not allowed" ? "Duplicate Fax number are not allowed" : faxError.faxNumber ? "Enter valid fax number" : ""}
                                  value={item.faxNumber ? item.faxNumber : ""}
                                  onChange={event => {
                                    this.onAddressPartChange(
                                      event,
                                      "officeFax",
                                      idx
                                    )
                                  }}
                                  disabled={!this.props.canEdit}
                                />
                              </div>
                            </div> */
                          )
                        })}
                        {canEdit && (
                          <div className="control">
                            <button // eslint-disable-line
                              tabIndex={tabIndex + 18}
                              className="button is-link is-small"
                              onClick={event => {
                                this.addMore(event, "fax")
                              }}
                            >
                              Add More
                            </button>
                            <button // eslint-disable-line
                              className="button is-light is-small"
                              tabIndex={tabIndex + 19}
                              onClick={event => {
                                this.cancelAdd(event, "fax")
                              }}
                            >
                                  Cancel
                            </button>
                          </div>
                        )}
                      </div>

                      <div className="column">
                        <p className="multiExpLbl">Office Email</p>
                        {businessAddress.officeEmails.map(
                          (item, idx) => {
                            const emailError = (errorFirmDetail && errorFirmDetail.officeEmails && errorFirmDetail.officeEmails[idx]) || {}
                            return(
                              <div className="field" key={parseInt(idx * 5, 10)}>
                                <div className="control d-flex">
                                  <TextInput
                                    tabIndex={tabIndex + 20}
                                    error={emailError.emailId === "Duplicate values are not allowed" ? "Duplicate email are not allowed" : emailError.emailId ? "Enter valid email" : ""}
                                    disabled={!canEdit || (item && item.isActive === undefined ? false : !item.isActive)}
                                    name="emailId"
                                    type="email"
                                    placeholder="e.g. alexsmith@gmail.com"
                                    value={item.emailId ? item.emailId : ""}
                                    onChange={event => {
                                      this.onAddressPartChange(
                                        event,
                                        "officeEmails",
                                        idx
                                      )
                                    }}
                                  />
                                  {canEdit ?
                                    <span style={{marginLeft: "20px"}}>
                                      {(!phonePrimary && ((item && item._id) && !(emailError && emailError.emailId) && (item && item.emailId))) ?
                                        <label className="checkbox"> {/* eslint-disable-line */}
                                          <input
                                            type="checkbox"
                                            name="isActive"
                                            tabIndex={tabIndex + 21}
                                            checked={(businessAddress && businessAddress.officeEmails && businessAddress.officeEmails[idx])
                                            && (businessAddress.officeEmails[idx].isActive === undefined ? true : businessAddress.officeEmails[idx].isActive)}
                                            onChange={(e) => this.onCheck(e, "officeEmails", idx)}
                                            onClick={(e) => this.onCheck(e, "officeEmails", idx)}
                                          />
                                        </label> :
                                        <label className="checkbox" style={{visibility: "hidden"}}> {/* eslint-disable-line */}
                                          <input type="checkbox"/>
                                        </label>
                                      }
                                    </span> : null
                                  }
                                </div>
                              </div>

                            )
                          }
                        )}
                        {canEdit && (
                          <div className="control">
                            <button // eslint-disable-line
                              className="button is-link is-small"
                              tabIndex={tabIndex + 22}
                              onClick={event => {
                                this.addMore(event, "email")
                              }}
                            >
                              Add More
                            </button>
                            <button // eslint-disable-line
                              className="button is-light is-small"
                              tabIndex={tabIndex + 23}
                              onClick={event => {
                                this.cancelAdd(event, "email")
                              }}
                            >
                              Cancel
                            </button>
                          </div>
                        )}
                      </div>
                    </div>
                  </span>}
                <hr />
                {canEdit && (<span><div className="has-text-centered has-text-danger">
                  {primaryError}
                </div>
                <div className="has-text-centered has-text-danger">
                  {uniqueAddressName}
                </div>
                <div className="is-grouped-center">
                  <div className="control">
                    <button // eslint-disable-line
                      tabIndex={tabIndex + 24}
                      className="button is-link"
                      onClick={isCompliance ? () => this.onComplianceSave([businessAddress]) : () => this.onSaveAddress("savecontact")}
                      disabled={!(businessAddress && businessAddress.addressLine1) || false}
                    >
                      {isUserAddress ? "Save Address" : "Save Contact Info"}
                    </button>
                  </div>
                </div></span>)}
              </div>
            ) : null}
          </div>
        )}
      </article>
    )
  }
}
export default AdmTrnAddressListForm
