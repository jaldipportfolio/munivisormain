import React from "react"

const capitalizeWords = (str) => str.replace(/\w\S*/g, (txt) => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase())

const ContactType = (props) => (
  <article className={props.expanded.contacttype ? "accordion is-active" : "accordion"}>
    <div className="accordion-header toggle">
      <p
        onClick={event => {
          props.toggleButton(event, "contacttype")
        }}>Contact Type &amp; System Access Settings</p>
      {
        props.expanded.contacttype
          ? <i className="fas fa-chevron-up" />
          : <i className="fas fa-chevron-down" />
      }
    </div>
    {props.expanded.contacttype && <div className="accordion-body">
      <div className="accordion-content">

        <p className="title innerContactTitle">Select Contact Type</p>

        <div className="columns w-100" style={{ paddingLeft: "10px" }}>
          {props.loadingPickLists ? <div style={{ padding: 30 }} className="is-link"><span className="fas fa-sync fa-spin is-link" /></div> :

            props.userFlags.map((item, idx) => (
              <div className="column25" key={Math.random()}>
                <h1>
                  {!props.canEdit ? props.userDetails.userFlags.includes(item) && <p className="multiExpLbl " title={`${item}`}><small>{item}</small></p>
                    : <p className="multiExpLbl " title={`${item}`}>
                      {" "}
                      {item}
                      <input
                        type="checkbox"
                        name="userFlags"
                        value={item}
                        onChange={props.onChangeUserDetail}
                        checked={!!props.userDetails.userFlags.includes(item)}
                      />
                    </p>}
                </h1>
              </div>
            ))

          }
        </div>
        {props.errorUserDetail && props.errorUserDetail.userFlags && <small className="text-error">{props.errorUserDetail.userFlags}</small>}
        <hr />

        <p className="title innerContactTitle">Select Contact's System Access Level</p>
        <div className="columns">
          {props.loadingPickLists ? <div style={{ padding: 30 }} className="is-link"><span className="fas fa-sync fa-spin is-link" /></div> :
            props.userEntitlement.map((item, idx) => (
              <div className="column25" key={Math.random()} style={{paddingLeft: "10px"}}>
                <h1>
                  {!props.canEdit ? props.userDetails.userEntitlement === item && <p className="multiExpLbl " title={`${item}`}><small>{capitalizeWords((item.split("-")).join(" "))}</small></p>
                    : <p className="multiExpLbl " title={`Is ${capitalizeWords((item.split("-")).join(" "))}?`}>
                      {" "}
                      {capitalizeWords((item.split("-")).join(" "))}
                      <input
                        type="checkbox"
                        name="userEntitlement"
                        value={item}
                        checked={props.userDetails.userEntitlement === item}
                        onChange={props.onChangeUserDetail}
                      />
                    </p>}
                </h1>
              </div>
            ))}
          <br />
        </div>
        {props.errorUserDetail && props.errorUserDetail.userEntitlement && <small className="text-error">{props.errorUserDetail.userEntitlement}</small>}
      </div>
    </div>}

  </article>
)
export default ContactType
