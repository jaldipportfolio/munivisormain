import React from "react"
import { Link,Redirect } from "react-router-dom"
import Switch, { Case, Default } from "react-switch-case"
// import UpdateContact from "./containers/UpdateContactContainer"
import UsersNew from "../../Users/Users"

const ContactsView = props => {
  const { navComponent, id } = props
  return (
    <Switch condition={navComponent}>
      <Case value="cltprops">
        <UsersNew nav2={id} />
      </Case>
      <Case value="thirdparties">
        <UsersNew nav2={id} nav1="thirdparties"/>
      </Case>
      <Case value="migratedentities">
        <UsersNew nav2={id} nav3="migratedentities" />
      </Case>
      <Default>
        <Redirect to="/mast-allcontacts" />
      </Default>
    </Switch>
  )
}

export default ContactsView
