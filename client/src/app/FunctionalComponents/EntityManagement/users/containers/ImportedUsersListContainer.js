import React, { Component } from "react"
import {withRouter} from "react-router-dom"
import {connect} from "react-redux"
import Loader from "../../../../GlobalComponents/Loader"
import Accordion from "../../../../GlobalComponents/Accordion"
import EntityPageFilter from "../../CommonComponents/EntityPageFilter"
import {fetchIssuers} from "../../../../StateManagement/actions/CreateTransaction"

class ImportedUsersListContainer extends Component {

  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      allEntities: []
    }
  }

  componentWillMount() {
    const { loginDetails } = this.props.auth
    if(
      loginDetails &&
      loginDetails.userEntities &&
      loginDetails.userEntities[0] &&
      loginDetails.userEntities[0].entityId
    ){
      fetchIssuers(loginDetails.userEntities[0].entityId,
        res => {
          this.setState({
            allEntities: res.issuerList,
            loading: false
          })
        }
      )
    }
  }

  onChange = (e) => {
    const {name, value} = e.target
    this.setState({
      [name]: value
    })
  }

  render() {
    const {loading, allEntities} = this.state

    return (
      <div>
        {loading ? <Loader/> : null}
        <Accordion
          multiple activeItem={[0]} boxHidden render={() =>
            <div>
              <EntityPageFilter
                listType={this.props.listType || "importedUsers"}
                auth={this.props.auth}
                activeTab={this.props.activeTab}
                allEntities={allEntities}
                nav1={this.props.nav1 || ""}
                searchPref="mast-users-imported"
                title="Users"
                routeType="importedUsers"
              />
            </div>
          }
        />
      </div>
    )
  }
}

const mapStateToProps = state => {
  const { auth } = state
  return { auth }
}

const mapDispatchToProps = {}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ImportedUsersListContainer)
)
