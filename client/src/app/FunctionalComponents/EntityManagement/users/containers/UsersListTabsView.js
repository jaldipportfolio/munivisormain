import React, { Component } from "react"
import "react-table/react-table.css"
import { withRouter } from "react-router-dom"
import * as qs from "query-string"
import { connect } from "react-redux"
import { checkUserEntitlement } from "../../../../../globalutilities/helpers"
import "./../../scss/entity.scss"

const UsersListContainer = React.lazy(() => import("./UsersListContainer"))
const MigratedUsersListContainer = React.lazy(() => import( "./MigratedUsersListContainer"))
const ImportedUsersListContainer = React.lazy(() => import("./ImportedUsersListContainer"))

class UsersListTabsView extends Component {
  constructor(props) {
    super(props)
    this.state = {
      tabActiveIndex: 0,
      TABS: [
        { label: "Users" },
        { label: "Migrated Users" },
      /* { label: "Imported Users" }, */
      ]
    }
  }

  componentWillMount() {
    const queryString = qs.parse(location.search)
    let tabActiveIndex = parseInt(queryString.active, 10) || 0
    const code = queryString.code || ""
    if(code) tabActiveIndex = 2
    this.setState({
      tabActiveIndex
    })
  }

  identifyComponentToRender = (index) => {
    this.setState({
      tabActiveIndex:index
    },() => this.props.history.push(`mast-allcontacts?active=${index}`))
  }

  renderTabContents = () => {
    const {TABS,tabActiveIndex} = this.state
    const userRoleInTenant = checkUserEntitlement()
    let NEWTABS = []
    if (userRoleInTenant) {
      NEWTABS = [...TABS]
    } else {
      NEWTABS= [{ label: "Users" }]
    }

    const tabActive = TABS[tabActiveIndex].label
    return (<ul>
      {
        NEWTABS.map ( (t,index) =>
          <li key={t.label} className={tabActive === t.label ? "is-active" : ""} onClick={() => this.identifyComponentToRender(index)}>
            <a className="tabSecLevel"> {/* eslint-disable-line */}
              <span>{t.label}</span>
            </a>
          </li>
        )
      }
    </ul>)
  }

  renderSelectedView = (option) => {
    switch (option) {
    case 0 :
      return <UsersListContainer listType="users" activeTab="Users" nav1={this.props.nav1}/>
    case 1 :
      return <MigratedUsersListContainer listType="migratedusers" activeTab="Migrated Users" nav1={this.props.nav1} />
    case 2 :
      return <ImportedUsersListContainer listType="importedUsers" activeTab="Imported Users"  nav1={this.props.nav1} />
    default:
      return option
    }
  }
  render() {
    return (
      <div id="main">
        <div className="tabs">
          {this.renderTabContents()}
        </div>
        {this.renderSelectedView(this.state.tabActiveIndex)}
      </div>
    )
  }
}

const mapStateToProps = state => {
  const { auth } = state
  return { auth }
}

export default withRouter(connect(mapStateToProps,null)(UsersListTabsView))
