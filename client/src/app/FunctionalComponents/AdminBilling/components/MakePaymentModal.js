import React from "react"
import { Modal } from "../../../FunctionalComponents/TaskManagement/components/Modal"
import CheckoutFormContainer from "./MakePaymentForm"

export const MakePaymentModal  = (props) => {
  const { paymentModal, invoice } = props

  const toggleModal = () => {
    props.onModalChange({ paymentModal: !props.paymentModal })
  }


  return(
    <Modal
      closeModal={toggleModal}
      modalState={paymentModal}
      title="Make Payment"
      modalWidth={{ width: "60%" }}
    >

      <CheckoutFormContainer invoice={invoice} onMakePaymentSuccess={props.onMakePaymentSuccess}/>

    </Modal>
  )
}


export default MakePaymentModal



