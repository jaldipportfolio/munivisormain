import React from "react"
import ReactTable from "react-table"
import {TextLabelInput} from "../../../../GlobalComponents/TextViewBox"

const EditableReactTable = ({
  gaGeneralFirmInfo,
  isEditable,
  onSaveDoc,
  onEdit,
  onCancel,
  onRemoveDoc,
  isSaveDisabled,
  onItemChange,
  category,
  errors,
  onBlur,
  svControls,
  label,
  pageSizeOptions = [5, 10, 20, 50, 100],
}) => {

  const onChange = (event, item, index) => onItemChange({
    ...item,
    [event.target.name]: event.target.value,
  }, category, index)

  const onBlurInput = event => {
    if (event.target.title && event.target.value) {
      onBlur(`${event.target.title || "empty"} change to ${event.target.value || "empty"}`,category)
    }
  }

  const notes = []
  if(category === "gaGeneralFirmInfo"){
    notes.push(
      {
        Header: () => (
          <div>
            Notes<span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon"/></span>
          </div>
        ) ,
        id: "notes",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          const isEdit = (isEditable[category] === row.index)
          const error = (errors && errors[category] && errors[category][row.index.toString()]) || {}
          return (
            <div className="hpTablesTd wrap-cell-text">
              {
                isEdit ? item.isField ? <small>{item.notes || ""}</small> :
                  <TextLabelInput
                    title="Notes"
                    name="notes"
                    value={item.notes || ""}
                    error={error.notes || ""}
                    onChange={(e) => onChange(e, item, row.index)}
                    onBlur={onBlurInput}
                  />
                  : <small>{item.notes || ""}</small>
              }
            </div>
          )
        },
      }
    )
  }
  const columns = [
    {
      Header: () => (
        <div>
          Field<span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon"/></span>
        </div>
      ),
      id: "field",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        const isEdit = (isEditable[category] === row.index)
        const error = (errors && errors[category] && errors[category][row.index.toString()]) || {}
        return (
          <div className="hpTablesTd -striped">
            {
              isEdit ? item.isField ? <small>{item.field || ""}</small> :
                <TextLabelInput
                  title="Field"
                  name="field"
                  value={item.field || ""}
                  error={error.field || ""}
                  onChange={(e) => onChange(e, item, row.index)}
                  onBlur={onBlurInput}
                />
                : <small>{item.field || ""}</small>
            }
          </div>
        )
      },
    },
    ...notes,
    {
      Header: "Information",
      id: "information",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        const isEdit = (isEditable[category] === row.index)
        return (
          <div className="hpTablesTd wrap-cell-text">
            {
              isEdit ?
                <TextLabelInput
                  title="Information"
                  name="information"
                  value={item.information || ""}
                  onChange={(e) => onChange(e, item, row.index)}
                  onBlur={onBlurInput}
                />
                : <small>{item.information || ""}</small>
            }
          </div>
        )
      },
      // sortMethod: (a, b) => `${a.conductedBy}` - `${b.conductedBy}`
    },
    {
      Header: "Action",
      id: "action",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        const isEdit = (isEditable[category] === row.index)
        return (
          <div>
            {svControls ?
              <div className="hpTablesTd wrap-cell-text">
                <div className="field is-grouped" style={{marginBottom: 0}}>
                  <div className="control">
                    <a
                      onClick={isEdit ? () => onSaveDoc(category, item, row.index) : () => onEdit(category, row.index, label)}
                      className={`${isSaveDisabled ? "isDisabled" : ""}`}>
                      <span className="has-text-link">
                        {isEdit ? <i className="far fa-save" title="Save"/> : <i className="fas fa-pencil-alt" title="Edit"/>}
                      </span>
                    </a>
                  </div>
                  <div className="control">
                    <a
                      onClick={isEdit ? () => onCancel(category) : (category === "administrationList" && item && item.isField) ? null : () => onRemoveDoc(item._id, category, row.index)}
                      className={`${isSaveDisabled ? "isDisabled" : ""}`}>
                      <span className="has-text-link">
                        {isEdit ? <i className="fa fa-times" title="Cancel"/> : (category === "administrationList" && item && item.isField) ? null : <i className="far fa-trash-alt" title="Delete"/>}
                      </span>
                    </a>
                  </div>
                </div>
                {item.isNew && !isEdit ? <span className="text-error">New(Not Save)</span> : null}
              </div> : null
            }
          </div>

        )
      },
      sortMethod: (a, b) => `${a.attendees}` - `${b.attendees}`
    }]

  return (
    <div className="column">
      <ReactTable
        columns={columns}
        data={gaGeneralFirmInfo || []}
        showPaginationBottom
        defaultPageSize={10}
        pageSizeOptions={pageSizeOptions}
        className="-striped -highlight is-bordered"
        style={{ overflowX: "auto" }}
        showPageJump
        minRows={2}
      />
    </div>
  )
}

export default EditableReactTable

