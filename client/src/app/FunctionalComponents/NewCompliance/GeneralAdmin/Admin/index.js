import React, { Component } from "react"
import Loader from "Global/Loader"

import People from "./People"
import BusinessActivities from "./A-12BusinessActivities"
import RegistrationAndFees from "./RegistrationAndFees"
import DesignatedContacts from "./DesignatedContacts"

class Admin extends Component {

  constructor(props) {
    super(props)
    this.state = {
      loading:true,
      tabActiveIndex: this.props.activeIndex || 0,
      TABS: [
        { label: "A-12 (Firm Information)",row:1,Component:People},
        { label: "A-12 (Business Activities)",row:1,Component:BusinessActivities},
        { label: "Registrations & Fees",row:1,Component:RegistrationAndFees},
        { label: "Designated Contacts",row:1,Component:DesignatedContacts},
      ]
    }
  }

  componentWillMount() {
    const {TABS} = this.state
    const {activeIndex} = this.props
    const {Component} = TABS[activeIndex || 0]

    this.setState({
      loading: false,
      TABS,
      tabActiveIndex:activeIndex || 0,
      SelectedComponent:Component
    })
  }

  identifyComponentToRender = (tab, ind) => {
    this.setState({SelectedComponent:tab.Component,tabActiveIndex:ind})
  }

  renderTabContents() {
    const {TABS,tabActiveIndex} =this.state
    const tabActive = TABS[tabActiveIndex].label
    return (<ul>
      {
        TABS.map ( (t,i) =>
          <li key={t.label} className={tabActive === t.label ? "is-active" : ""} onClick={() => this.identifyComponentToRender(t,i)}>
            <a className="tabSecLevel">
              <span>{t.label}</span>
            </a>
          </li>
        )
      }
    </ul>)
  }

  renderSelectedTabComponent() {
    const {SelectedComponent} = this.state
    return <SelectedComponent {...this.props} />
  }

  render() {
    const loading = () => <Loader/>
    if (this.state.loading) {
      return loading()
    }

    return (
      <div className="column">
        <div className="tabs">
          {this.renderTabContents()}
        </div>
        {this.renderSelectedTabComponent()}
      </div>
    )
  }
}

export default Admin
