import React, { Component } from "react"
import Loader from "Global/Loader"
import * as qs from "query-string"
import { Link } from "react-router-dom"
import Disclaimer from "../../../../GlobalComponents/Disclaimer"

class Instructions extends Component {

  constructor(props) {
    super(props)
    this.state = {
      navId: "",
      loading:true,
    }
  }

  componentWillMount() {
    const queryString = qs.parse(this.props.history.location.search)
    const {disclosure} = queryString
    this.setState({
      navId: disclosure || "",
      loading: false,
    })
  }

  render() {
    const { navId } = this.state
    const { svControls } = this.props
    const nav3 =  svControls.supervisor ? "supervisor-view" : "user-view"
    const nav4 =  svControls.supervisor ? "sub-detail" : "detail"
    const loading = () => <Loader/>
    if (this.state.loading) {
      return loading()
    }

    return (
      <div>
        <div className="columns">
          <div className="column">
            <p>
              <small>Rule G-44 based recommended sections in compliance policy and supervisory procedure
                document
              </small>
              <a href="https://s3.amazonaws.com/munivisor-platform-static-documents/MSRB-G44Sample-Template-and-Checklist-for-Municipal-Advisor-WSPs.pdf"
                target="new">
                <i className="fas fa-info-circle" />
              </a>
            </p>
          </div>
        </div>

        <div className="columns">

          <div className="column">
            <section className="container">
              <div className="tile is-ancestor">
                <div className="tile is-parent">
                  <article className="tile is-child box has-text-centered notification is-warning" style={{backgroundColor: "#F29718"}}>
                    <p>You have <a
                      href="https://s3.amazonaws.com/munivisor-platform-static-documents/Rule+G-37+Political+Contributions+and+Prohibitions+on+Municipal+Securities+Business+and+Municipal+Advisory+Business.pdf" target="new">
                      read MSRB Rule G-37</a>. Refer <a href="https://s3.amazonaws.com/munivisor-platform-static-documents/NAMAG37CHART2.pdf" target="new">
                      NAMA published Rule G-37 Snapshot</a>. You are seeking supervisor pre-approval.
                    </p>
                    <hr/>
                    <Link to={`/compliance/cmp-sup-political/${nav3}/${nav4}?${navId ? `status=Pre-approval&disclosure=${navId}` : "status=Pre-approval"}`}>
                      <p className="title mgmtConTitle">Pre-approval</p>
                    </Link>
                  </article>
                </div>
                <div className="tile is-parent">
                  <article className="tile is-child box has-text-centered notification is-link" style={{backgroundColor: "#38A341"}}>
                    <p>You have <a
                      href="https://s3.amazonaws.com/munivisor-platform-static-documents/Rule+G-37+Political+Contributions+and+Prohibitions+on+Municipal+Securities+Business+and+Municipal+Advisory+Business.pdf" target="new">
                      read MSRB Rule G-37</a>. Refer <a href="https://s3.amazonaws.com/munivisor-platform-static-documents/NAMAG37CHART2.pdf" target="new">
                      NAMA published Rule G-37 Snapshot</a>. Nothing to record & affirming as such.
                    </p>
                    <hr/>
                    <Link to={`/compliance/cmp-sup-political/${nav3}/fastTrackDetail${navId ? `?disclosure=${navId}` : ""}`} >
                      <p className="title mgmtConTitle">Fast Track</p>
                    </Link>
                  </article>
                </div>
                <div className="tile is-parent">
                  <article className="tile is-child box has-text-centered notification is-link" style={{backgroundColor: "#3982CC"}}>
                    <p>You have <a
                      href="https://s3.amazonaws.com/munivisor-platform-static-documents/Rule+G-37+Political+Contributions+and+Prohibitions+on+Municipal+Securities+Business+and+Municipal+Advisory+Business.pdf" target="new">
                      read MSRB Rule G-37</a>. Refer <a href="https://s3.amazonaws.com/munivisor-platform-static-documents/NAMAG37CHART2.pdf" target="new">
                      NAMA published Rule G-37 Snapshot</a>. You want to record/disclose a political contribution.
                    </p>
                    <hr/>
                    <Link to={`/compliance/cmp-sup-political/${nav3}/${nav4}?${navId ? `status=Record&disclosure=${navId}` : "status=Record"}`} >
                      <p className="title mgmtConTitle">Make a record</p>
                    </Link>
                  </article>
                </div>
              </div>
            </section>
          </div>
        </div>
        <hr/>
        <Disclaimer/>
      </div>
    )
  }
}

export default Instructions
