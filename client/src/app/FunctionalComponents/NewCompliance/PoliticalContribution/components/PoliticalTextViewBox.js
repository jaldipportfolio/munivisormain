import React from "react"
import DatePicker from "react-datepicker"
import NumberFormat from "react-number-format"
import {Multiselect} from "react-widgets"

export const TextLabelInput = ({
  type,
  name,
  style,
  label,
  value,
  className,
  title,
  onChange,
  placeholder,
  error,
  min,
  max,
  onBlur,
  disabled,
  required
}) => {
  const props = {
    name,
    value,
    style,
    min,
    max,
    title: title || label || "",
    placeholder: type === "date" ? "MM-DD-YYYY" : placeholder || "",
    onChange,
    type: type || "text",
    disabled: disabled || false,
    onBlur: onBlur || (() => { })
  }

  /*
  if (min) {
    props.minDate = dateFormatted(min)
  }
  if (max) {
    props.maxDate = dateFormatted(max)
  }
  momentLocalizer()
  */


  if (type === "date") {
    return (
      <div className={className || `${label ? "column" : "w-100"}`} style={style || { minWidth: 100 }}>
        {label ? (
          <p className="multiExpLblBlk">
            {label}
            {required ? <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon"/></span> : null}
          </p>
        ) : null}
        <DatePicker
          selected={value || ""}
          {...props}
          onChange={(val) => {
            onChange({target: {name, value: val || null}})
          }}
          className="input is-small is-link"
        />
        {error && (
          <p className="is-small text-error has-text-danger " style={{ fontSize: 12 }}>
            {error}
          </p>
        )}
      </div>
    )
  } else {
    return (
      <div className={className || `${label ? "column" : "w-100"}`} style={style || { minWidth: 100 }}>
        {label && (
          <p className="multiExpLblBlk">
            {label}
            {required ? <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon"/></span> : null}
          </p>
        )}
        {disabled ? (
          <input className="input is-small is-link" {...props} />
        ) : (
          <input className="input is-small is-link" {...props} />
        )}
        {error && (
          <p className="is-small text-error has-text-danger " style={{ fontSize: 12 }}>
            {error}
          </p>
        )}
      </div>
    )
  }
}

export const SelectLabelInput = ({ list = [], name, placeholder, className, label, title, value, onChange, error, onBlur, disabled, inputStyle, required, disableValue }) => {
  const props = {
    name,
    value,
    title: title || label || "",
    onChange: e => onChange(e),
    disabled: disabled || false,
    onBlur: onBlur || (() => { })
  }
  disableValue = (disableValue && Array.isArray(disableValue) && disableValue) || []

  return (
    <div className={className || `${label ? "column" : ""}`}>
      {label && (
        <p className="multiExpLblBlk">
          {label}
          {required ? <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon"/></span> : null}
        </p>
      )}
      {disabled ? (
        <select style={inputStyle || { width: "100%", backgroundColor: "whitesmoke", borderColor: "#4288dd", fontSize: 12, padding: 4 }} {...props}>
          <option value="">{placeholder }</option>
          {
            list.map((name, i) => (
              <option key={i} disabled={disableValue.indexOf(name) !== -1 || !name.included}>
                {name && name.label}
              </option>
            ))
          }
        </select>
      ) : (
        <div
          className="select is-small  is-fullwidth is-link"
          style={inputStyle || { width: "100%" }}
        >
          <select style={inputStyle || { width: "100%" }} {...props}>
            <option value="">{placeholder || "Pick"}</option>
            {
              list.map((name, i) => (
                <option key={i} disabled={disableValue.indexOf(name && name.label) !== -1 || !name.included}>
                  {name && name.label}
                </option>
              ))
            }
          </select>
          {error && (
            <p className="is-small text-error has-text-danger" style={{ fontSize: 12 }}>
              {error}
            </p>
          )}
        </div>
      )}
    </div>
  )
}

export const NumberInput = ({
  type,
  name,
  label,
  prefix,
  suffix,
  value,
  title,
  format,
  className,
  thousandSeparator,
  onChange,
  placeholder,
  error,
  onBlur,
  disabled,
  decimalScale,
  required,
  style
}) => {
  const onChaneInput = e => {
    const event = {}
    if(e.value === "-"){
      e.value = 0
    }
    event.target = {
      name,
      title: title || label || "",
      placeholder: placeholder || "",
      type: type || "text",
      ...e,
    }
    onChange(event)
  }
  const props = {
    name,
    title: title || label || "",
    value: value || "",
    placeholder: placeholder || "",
    onValueChange: onChaneInput,
    type: type || "text",
    disabled: disabled || false,
    prefix: prefix || "",
    suffix: suffix || "",
    style,
    onBlur: onBlur || (() => { }),
    thousandSeparator: thousandSeparator === undefined ? true : thousandSeparator,
    decimalScale: parseInt(decimalScale, 10) || 2
  }

  if (format) {
    delete props.thousandSeparator
    props.format = format
  }
  return (
    <div className={className || `${label ? "column" : ""}`}>
      {label && (
        <p className="multiExpLblBlk">
          {label}
          {required ? <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon"/></span> : null}
        </p>
      )}
      {disabled ? (
        <NumberFormat
          className="input is-fullwidth is-small is-link"
          {...props}
        />
      ) : (
        <NumberFormat
          className="input is-fullwidth is-small is-link"
          {...props}
        />
      )}
      {error && (
        <p className="is-small text-error has-text-danger" style={{ fontSize: 12 }}>
          {error}
        </p>
      )}
    </div>
  )
}

export const MultiSelect = ({
  filter,
  data = [],
  label,
  value,
  groupBy,
  style,
  className,
  message,
  placeholder,
  disableValue,
  onChange,
  error,
  disabled,
  required,
  isFull,
  selectProps = {}
}) => {
  const props = {
    filter: filter || false,
    data: data || [],
    textField: "name",
    value: value || [],
    valueField: "id",
    onChange,
    message,
    groupBy: groupBy || "",
    placeholder: placeholder || "",
    disabled: disabled || false,
    ...selectProps
  }
  style = {
    ...style,
    fontSize: 12
  }
  return (
    <div
      className={`${label ? `column ${isFull && "is-full"}` : isFull ? "column is-full" : ""}`}
    >
      {label && <p className="multiExpLblBlk">
        {label}
        {required ? <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon"/></span> : null}
      </p>}
      {disabled ? (
        <span>
          {value
            ? value.map((mul, i) => (
              <small key={i}>
                {mul.name}
                {disableValue.length - 1 !== i ? "," : ""}
              </small>
            ))
            : ""}
        </span>
      ) : (
        <div className={`${className || ""}`} style={style || { minWidth: 100, fontSize: 12 }}>
          <Multiselect {...props} />
          {error && (
            <p className="is-small text-error has-text-danger" style={{ fontSize: 12 }}>
              {error}
            </p>
          )}
        </div>
      )}
    </div>
  )
}
