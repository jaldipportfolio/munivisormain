import React from "react"
import {Link} from "react-router-dom"
import ReactTable from "react-table"
import "react-table/react-table.css"

const highlightSearch = (string, searchQuery) => {
  const reg = new RegExp(
    searchQuery.replace(/[|\\{}()[\]^$+*?.]/g, "\\$&"),
    "i"
  )
  return string.replace(reg, str => `<mark>${str}</mark>`)
}

const EntityListReactTable = ({
  entityList,
  onSearchEntity,
  selectedEntity,
  searchText,
  onSelectEntity,
  length,
  onSelect
}) => {

  const columns = [
    {
      id: "select",
      Header: <div className="hpTablesTd wrap-cell-text" style={{justifyContent: "center", display: "flex"}}>
        <label className="checkbox-button" style={{paddingLeft: 5}}>
          <input
            type="checkbox"
            name="all"
            style={{position: "inherit"}}
            checked={ length === selectedEntity.length }
            onChange={() => onSelect(onSelectEntity.length)}
          /><span className="checkmark" style={{border: "1px solid black"}}/>
        </label>
      </div>,
      className: "multiExpTblVal",
      sortable: false,
      width: 100,
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text" style={{justifyContent: "center", display: "flex"}}>
            <label className="checkbox-button">
              {" "}
              <input
                type="checkbox"
                name="sendEmail"
                value="none"
                checked={item && item.checked || false}
                onClick={(e) => onSelectEntity(e, item._id)}
              />
              <span className="checkmark" style={{border: "1px solid"}}/>
            </label>
          </div>
        )
      },
      sortMethod: (a, b) => a.entityRelationshipType.localeCompare(b.entityRelationshipType)
    },
    {
      id: "type",
      Header: "Type",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            <div
              dangerouslySetInnerHTML={{
                __html: highlightSearch( item && item.entityRelationshipType || "--" , searchText)
              }}
            />
          </div>
        )
      },
      sortMethod: (a, b) => a.entityRelationshipType.localeCompare(b.entityRelationshipType)
    },
    {
      id: "entityName",
      Header: "Entity Name",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            <div
              dangerouslySetInnerHTML={{
                __html: highlightSearch( item && item.entityName || "--" , searchText)
              }}
            />
          </div>
        )
      },
      sortMethod: (a, b) => a.entityName - b.entityName
    },
    {
      id: "contactName",
      Header: "Primary Contact Name",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            <div
              dangerouslySetInnerHTML={{
                __html: highlightSearch( item && item.userFullName || "--" , searchText)
              }}
            />
          </div>
        )
      },
      sortMethod: (a, b) => a.userFullName.localeCompare(b.userFullName)
    },
    {
      id: "primaryEmail",
      Header: "Primary Email",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            <div
              dangerouslySetInnerHTML={{
                __html: highlightSearch( item && item.userPrimaryEmail || "--" , searchText)
              }}
            />
          </div>
        )
      },
      sortMethod: (a, b) => a.userPrimaryEmail.localeCompare(b.userPrimaryEmail)
    },
    {
      id: "primaryAddress",
      Header: "Primary Address",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            <div
              dangerouslySetInnerHTML={{
                __html: highlightSearch( item && item.entityPrimaryAddressGoogle || "--" , searchText)
              }}
            />
          </div>
        )
      },
      sortMethod: (a, b) => a.entityName.localeCompare(b.entityName)
    },
    {
      id: "update",
      Header: "Update Master List",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <Link
            to={`/clients-propects/${item.entityId}/entity`}
          >Detail</Link>
        )
      }
    }

  ]

  return (
    <div className="column">
      <div className="columns">
        <div className="column">
          <p className="control has-icons-left">
            <input className="input is-small is-link" type="text" name="searchText"  placeholder="search" onChange={(e) => onSearchEntity(e)} />
            <span className="icon is-left has-background-dark">
              <i className="fas fa-search" />
            </span>
          </p>
        </div>
      </div>
      { selectedEntity && selectedEntity.length ?
        <div >
          <div >
            <p style={{color: "#383838", fontSize: 15}}>Selected : {selectedEntity.length}</p>
          </div>
        </div> : null
      }
      <ReactTable
        columns={columns}
        data={entityList}
        showPaginationBottom
        defaultPageSize={5}
        className="-striped -highlight is-bordered"
        style={{ overflowX: "auto" }}
        showPageJump
        minRows={2}
      />
    </div>
  )
}

export default EntityListReactTable
