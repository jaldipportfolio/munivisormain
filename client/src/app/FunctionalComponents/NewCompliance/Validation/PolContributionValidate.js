import Joi from "joi-browser"

const entityByStateSchema = (none) => Joi.object().keys({
  nameOfMuniEntity: none ? Joi.string().allow("") : Joi.string().required(),
  nameOfMuniEntityID: Joi.string().allow("").optional(),
  titleOfMuniEntity: Joi.string().allow(""),
  state: none ? Joi.string().allow("") : Joi.string().required(),
  city: Joi.string().allow(""),
  county: Joi.string().allow(""),
  otherPoliticalSubdiv: Joi.string().allow(""),
  contributorCategory : Joi.string().allow(""),
  contributionAmount : none ? Joi.number().allow("", null).required().optional() : Joi.number().min(0).required(),
  contributionDate : none ? Joi.date().allow([null,""]).example(new Date("2005-01-01")).allow("", null).optional() : Joi.date().example(new Date("2005-01-01")).required(),
  exempted : Joi.boolean().required().optional(),
  checkNone : Joi.boolean().required().optional(),
  isNew : Joi.boolean().required().optional(),
  dateOfExemption : none ? Joi.date().allow([null,""]).example(new Date("2005-01-01")).allow("", null).optional() : Joi.date().allow([null,""]).example(new Date("2005-01-01")).allow("", null).optional(),
  createdDate : Joi.date().allow([null,""]).example(new Date("2005-01-01")).allow("", null).optional(),
  _id: none ? Joi.string().allow("") : Joi.string().required().optional(),
})

export const ContributeOfMunicipalEntityValidate = (inputTransDistribute, none) => Joi.validate(inputTransDistribute, entityByStateSchema(none), { abortEarly: false, stripUnknown:false })


const paymentSchema = (none) => Joi.object().keys({
  state: none ? Joi.string().allow("") : Joi.string().required(),
  city: Joi.string().allow(""),
  county: Joi.string().allow(""),
  politicalParty: none ? Joi.string().allow("") : Joi.string().required(),
  contributorCategory: Joi.string().allow(""),
  contributionAmount: none ? Joi.number().allow("", null).required().optional() : Joi.number().min(0).required(),
  contributionDate: none ? Joi.date().allow([null,""]).example(new Date("2005-01-01")).allow("", null).optional() : Joi.date().example(new Date("2005-01-01")).required(),
  checkNone : Joi.boolean().required().optional(),
  isNew : Joi.boolean().required().optional(),
  createdDate : Joi.date().allow([null,""]).example(new Date("2005-01-01")).allow("", null).optional(),
  _id: none ? Joi.string().allow("") : Joi.string().required().optional(),
})

export const PaymentPolPartiesValidate = (inputTransDistribute, none) => Joi.validate(inputTransDistribute, paymentSchema(none), { abortEarly: false, stripUnknown:false })


const ballotSchema = (none) => Joi.object().keys({
  nameOfBondBallot: none ? Joi.string().allow("") : Joi.string().required(),
  nameOfIssueMuniEntity: none ? Joi.string().allow("") : Joi.string().required(),
  nameOfIssueMuniEntityID: Joi.string().allow("").optional(),
  state: none ? Joi.string().allow("") : Joi.string().required(),
  city: Joi.string().allow(""),
  county: Joi.string().allow(""),
  otherPoliticalSubdiv: Joi.string().allow(""),
  contributorCategory: Joi.string().allow(""),
  contributionAmount: none ? Joi.number().allow("", null).required().optional() : Joi.number().min(0).required(),
  contributionDate: none ? Joi.date().allow([null,""]).example(new Date("2005-01-01")).allow("", null).optional() : Joi.date().example(new Date("2005-01-01")).required(),
  reimbursements: Joi.string().allow(""),
  reimbursementsAmount: none ? Joi.number().allow("", null).required().optional() : Joi.number().allow("", null).required().optional(),
  checkNone : Joi.boolean().required().optional(),
  isNew : Joi.boolean().required().optional(),
  createdDate : Joi.date().allow([null,""]).example(new Date("2005-01-01")).allow("", null).optional(),
  _id: none ? Joi.string().allow("") : Joi.string().required().optional(),
})

export const BallotContribValidate = (inputTransDistribute, none) => Joi.validate(inputTransDistribute, ballotSchema(none), { abortEarly: false, stripUnknown:false })
