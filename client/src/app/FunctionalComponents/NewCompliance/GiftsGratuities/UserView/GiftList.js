import React, {Component} from "react"
import Loader from "Global/Loader"
import { connect } from "react-redux"
import {Link} from "react-router-dom"
import moment from "moment"
import ReactTable from "react-table"
import "react-table/react-table.css"
import {getUserGiftsDetails} from "../../../../StateManagement/actions/Supervisor"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import Accordion from "../../../../GlobalComponents/Accordion"
import {checkSupervisorControls} from "../../../../StateManagement/actions/CreateTransaction"

class GiftList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading:true,
      allGifts: [],
      total: 0,
      pages: 0,
      page: 0,
      pageSize: 5,
      sorted: { "_id": -1 },
      loadingAction: true
    }
  }

  async componentWillMount() {
    const tabView = await checkSupervisorControls()
    this.onGridOperations()
    const superVisor = tabView && tabView.supervisor ? "supervisor-view" : "user-view"
    this.setState({
      superVisor,
      tabView,
      loading: false,
    })
  }

  onGridOperations = async () => {
    const {user, svControls} = this.props
    const {pageSize, page, sorted} = this.state
    const query = `?id=${user.userId || ""}&supervisor=${svControls && svControls.supervisor || ""}`

    const body = {
      page,
      pageSize,
      sortedFields: sorted
    }

    await getUserGiftsDetails(query, body,(res) => {
      if (res.data) {
        this.setState({
          allGifts: res.data || [],
          total: (res.metadata[0] && res.metadata[0].total) || 0,
          pages: (res.metadata[0] && res.metadata[0].pages) || 0,
          loadingAction: false,
        })
      }
    })
  }

  render() {
    const {allGifts, superVisor, tabView, pageSize, page, pages} = this.state
    const columns = [
      {
        id: "controlName",
        Header: "Control Name",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              <div>
                {
                  item && item.controlName || ""
                }
              </div>
              {item && item.controlEffectiveDate && <small>action date: {moment(item.controlEffectiveDate).format("MM-DD-YYYY") } </small>}
            </div>
          )
        },
        sortMethod: (a, b) => a.year - b.year
      },
      /* {
        id: "notes",
        Header: "Reference Notes",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          const latest = item.notes.length && item.notes[item.notes.length-1] && item.notes[item.notes.length-1].note
          return (
            <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html:
                ((item && item.giftsAffirmCompletionOfG20ForPeriod) ? (item && item.affirmationNotes || "-") : latest || "-")}} />
          )
        },
        sortMethod: (a, b) => {
          const t1 = a.notes[a.notes.length-1] && a.notes[a.notes.length-1].note.toLowerCase() || ""
          const t2 = b.notes[b.notes.length-1] && b.notes[b.notes.length-1].note.toLowerCase() || ""
          if (t1 < t2) { return -1 }
          if (t1 > t2) { return 1 }
          return 0
        }
      }, */
      {
        id: "year",
        Header: "Report Year",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (item.year || "-") }} />
          )
        },
        sortMethod: (a, b) => a.year - b.year
      },
      {
        id: "quarter",
        Header: "Report Quarter",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => { // eslint-disable-line
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (item.quarter || "-") }} />
          )
        },
        sortMethod: (a, b) => a.quarter - b.quarter
      },
      /* {
        id: "submitter",
        Header: "Submitter",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => { // eslint-disable-line
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (item.submitter || "-") }} />
          )
        },
        sortMethod: (a, b) => (a.submitter || "").localeCompare(b.submitter)
      }, */
      {
        id: "userFirstName",
        Header: "Disclosure For",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => { // eslint-disable-line
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (`${item.userFirstName} ${item.userLastName}` || "-") }} />
          )
        },
        sortMethod: (a, b) => (a.userFirstName || "").localeCompare(b.userFirstName)
      },
      {
        id: "preApprovalSought",
        Header: "Pre-approval Sought?",
        className: "multiExpTblVal",
        sortable: false,
        accessor: item => item,
        Cell: row => { // eslint-disable-line
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html: (item && item.discloseType === "Pre") ? "Yes" : "No" || "" }} />
          )
        },
        // sortMethod: (a, b) => (a.discloseType === "Pre" ? "Yes" : "No").localeCompare(b.discloseType === "Pre" ? "Yes" : "No")
      },
      {
        id: "actionBy",
        Header: "Action By",
        sortable: false,
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              {
                (item && item.actionBy) || "NA"
              }
            </div>
          )
        },
        // sortMethod: (a, b) => a.actionBy || "NA".localeCompare(b.actionBy || "NA")
      },
      {
        id: "status",
        Header: "Pre-approval Status?",
        className: "multiExpTblVal",
        sortable: false,
        accessor: item => item,
        Cell: row => { // eslint-disable-line
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html:
                (item && item.discloseType === "Pre") ? (item && item.status === "Complete") ? "Approved" : item.status === "Dropped" ? "NA" : item.status : "NA" ||  ""}}
            />
          )
        },
        sortMethod: (a, b) => (a.status || "").localeCompare((b.status || ""))
      },
      {
        id: "disclosureObligation",
        Header: "Disclosure Obligation",
        className: "multiExpTblVal",
        sortable: false,
        accessor: item => item,
        Cell: row => { // eslint-disable-line
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text" dangerouslySetInnerHTML={{ __html:
                (item && item.discloseType === "Pre") ? (item && (item.status === "Approved" || item.status === "Pre-approval" || item.status === "Pending")) ? "Pending" :
                  (item && item.status === "Complete") ? item.status : item.status === "Dropped" ? item.status : "NA" : item.status }}  />
          )
        },
        // sortMethod: (a, b) => (a.recordDate || "").localeCompare((b.recordDate || ""))
      },
      {
        id: "edit",
        Header: "Detail",
        className: "multiExpTblVal",
        sortable: false,
        accessor: item => item,
        Cell: row => {
          const item = row.value
          const link = (item && item.discloseType === "Fast") ? "fastTrackDetail?" : (item && item.discloseType === "Pre") ? "detail?status=Pre-approval&" :
            (item && item.discloseType === "Record") ? "detail?status=Record&" : ""
          const toFastTrack = `/compliance/cmp-sup-gifts/${superVisor}/fastTrackDetail?disclosure=${item._id}`
          const toCAC = `/compliance/cmp-sup-gifts/instructions?disclosure=${item._id}`
          const typeLink = `/compliance/cmp-sup-gifts/${superVisor}/${link}disclosure=${item._id}`
          return (
            <div className="hpTablesTd wrap-cell-text">
              {(item.status === "Dropped" && item && item.discloseType === undefined) ? <span>NA</span> :
                <Link to={item && item.cac ? item && item.discloseType ? `${typeLink}` : toCAC : item && item.G37Obligation ? toFastTrack : typeLink}>
                  <p className="">{(tabView && tabView.supervisor ? (item.status === "Pending" || item.status === "Pre-approval") : (item.status === "Pending"))
                    ? "Click Here" : "Read Only"}</p>
                </Link>
              }
            </div>
          )
        },
      }
    ]

    const loading = () => <Loader/>
    if (this.state.loading) {
      return loading()
    }

    return (
      <div>
        <Accordion
          multiple
          activeItem={[0]}
          boxHidden
          render={({ activeAccordions, onAccordion }) => (
            <div>
              <RatingSection
                onAccordion={() => onAccordion(0)}
                title="G-20 Gifts & Gratuities"
              >
                {activeAccordions.includes(0) && (
                  <ReactTable
                    columns={columns}
                    manual
                    data={allGifts}
                    showPaginationBottom
                    page={page}
                    pages={pages}
                    pageSize={pageSize}
                    defaultPageSize={5}
                    pageSizeOptions={[5, 10, 20, 50]}
                    className="-striped -highlight is-bordered"
                    style={{ overflowX: "auto" }}
                    showPageJump
                    minRows={2}
                    loading={this.state.loadingAction}
                    onPageChange={page => {
                      this.setState({
                        page: page,
                        loadingAction: true
                      },()=>{
                        this.onGridOperations()
                      })
                    }}
                    onPageSizeChange={pageSize => {
                      this.setState({
                        pageSize,
                        loadingAction: true,
                        page: 0
                      },()=>{
                        this.onGridOperations()
                      })
                    }}
                    onSortedChange={(newSort) => {
                      let sortedFields = {}
                      if (newSort && newSort[0] && newSort[0].id) {
                        sortedFields[newSort[0].id] = newSort[0].desc ? -1 : 1
                      }
                      this.setState({
                        sorted: sortedFields,
                        loadingAction: true
                      },()=>{
                        this.onGridOperations()
                      });
                    }}
                  />
                )}
              </RatingSection>
            </div>
          )}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

export default connect(mapStateToProps, null)(GiftList)
