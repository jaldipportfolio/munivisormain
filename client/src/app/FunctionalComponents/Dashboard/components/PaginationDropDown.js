import React from "react"

class PaginationDropDown extends React.Component {
  handleSelectChange = (e) => {
    const { target: { value }} = e
    this.props.handlePageChange(parseInt(value, 10))
  }
  render() {
    return (
      <div style={{ display: "flex" }}>
        <h3>Page Size</h3> &nbsp;
        <div className="select is-link is-small">
          <select value={this.props.defaultValue.toString()} onChange={this.handleSelectChange}>
            <option value="10">10</option>
            <option value="25">25</option>
            <option value="50">50</option>
            <option value="100">100</option>
          </select>
        </div>
      </div>
    )
  }
}

export default PaginationDropDown
