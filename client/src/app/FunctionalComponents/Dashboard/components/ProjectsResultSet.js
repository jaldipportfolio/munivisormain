/* eslint-disable jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events, no-underscore-dangle */

import React from "react"
import { connect } from "react-redux"

import DealsResultSet from "../../GlobalSearch/components/DealsResultSet"
import RfpResultSet from "../../GlobalSearch/components/RfpsResultSet"
import MarfpResultSet from "../../GlobalSearch/components/MarfpResultSet"

class SearchResultItems extends React.Component {
  constructor(props) {
    super(props)
    const res = props.data
    this.state = {
      isDealOpen: true,
      isRFPOpen: true,
      isMarfpOpen: true,
      res,
    }
  }

  componentWillReceiveProps(nextProps) {
    const res = nextProps.data
    this.updateState(res)
  }

  updateState = (res) => {
    this.setState({
      isDealOpen: true,
      isRFPOpen: true,
      isMarfpOpen: true,
      res,
    })
  }

  handleToggle = (group) => {
    this.setState({
      [group]: !this.state[group]
    })
  }

  render() {
    const { res } = this.state
    const { activeGroup } = this.props

    return (
      <div className="accordions">
        {activeGroup === "deals" && (<div className={`accordion ${this.state.isDealOpen && "is-active"}`}>
          <div className="accordion-header" onClick={() => { this.handleToggle("isDealOpen") }}>
            <span>Transactions</span>
            <div>
              {
                this.state.isDealOpen
                  ? <i className="fas fa-chevron-up" />
                  : <i className="fas fa-chevron-down" />
              }
            </div></div>
          {
            this.state.isDealOpen && (
              <div className="accordion-body">
                <div className="accordion-content">
                  <div >
                    <DealsResultSet data={this.props.data} allUrls={this.props.allUrls} />
                  </div>
                </div>
              </div>
            )
          }
        </div>
        )}
        {activeGroup === "rfps" && (<div className={`accordion ${this.state.isRFPOpen && "is-active"}`}>
          <div className="accordion-header" onClick={() => { this.handleToggle("isRFPOpen") }}>
            <span>Activity (Non-Transactions)</span>
            <div>
              {
                this.state.isRFPOpen
                  ? <i className="fas fa-chevron-up" />
                  : <i className="fas fa-chevron-down" />
              }
            </div></div>
          {
            this.state.isRFPOpen && (
              <div className="accordion-body">
                <div className="accordion-content">
                  <div >
                    <RfpResultSet data={this.props.data} allUrls={this.props.allUrls} />
                  </div>
                </div>
              </div>
            )
          }
        </div>
        )}
        { activeGroup === "marfps" && (<div className={`accordion ${this.state.isMarfpOpen && "is-active"}`}>
          <div className="accordion-header" onClick={() => { this.handleToggle("isMarfpOpen") }}>
            <span>MA-RFP</span>
            <div>
              {
                this.state.isRFPOpen
                  ? <i className="fas fa-chevron-up" />
                  : <i className="fas fa-chevron-down" />
              }
            </div></div>
          {
            this.state.isMarfpOpen && (
              <div className="accordion-body">
                <div className="accordion-content">
                  <div>
                    <MarfpResultSet data={this.props.data} allUrls={this.props.allUrls} />
                  </div>
                </div>
              </div>
            )
          }
        </div>)}
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  allUrls: state.urls.allurls
})

export default connect(mapStateToProps)(SearchResultItems)
