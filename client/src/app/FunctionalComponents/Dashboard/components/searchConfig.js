import _ from "lodash"

export const FACETS =  [
  {
    id: _.uniqueId("facet__"),
    isOpen: true,
    type: "MultiDataList",
    title: "Transaction Type",
    props: {
      componentId: "transactionTypeSensor",
      dataField: "tranType.raw",
      showSearch: false,
      data: [
        {
          label: "Deal Transaction",
          value: "Deal Transaction"
        },
        {
          label: "RFP Transaction",
          value: "RFP Transaction"
        }
      ],
      customQuery: (value) => {
        if (value.length) {
          return  {
            bool: {
              should: [
                { terms: { "tranType.raw": value } },
                { terms: { "rfpTranType.raw": value } }
              ]
            }
          }
        }

        return null
      }
    }
  },
  {
    id: _.uniqueId("facet__"),
    isOpen: true,
    type: "SingleDataList",
    title: "Transaction Status",
    props: {
      componentId: "transactionStatusSensor",
      dataField: "tranStatus",
      customQuery: (value) => {
        if (value === "All") {
          return ({
            bool: {
              should: [
                { exists: { field: "tranStatus" }},
                { exists: { field: "rfpTranStatus" }}
              ]
            }
          })
        }
        return ({
          bool: {
            should: [
              { match: { tranStatus: value } },
              { match: { rfpTranStatus: value } }
            ]
          }
        })
      },
      showSearch: false,
      defaultSelected: "All",
      selectAllLabel: "All",
      data: [
        {
          label: "New",
          value: "New"
        },
        {
          label: "In Progress",
          value: "In Progress"
        },
        {
          label: "Closed",
          value: "Closed"
        },
        {
          label: "On Hold",
          value: "On Hold"
        },
        {
          label: "Cancelled",
          value: "Cancelled"
        }
      ]
    }
  },
  {
    id: _.uniqueId("facet__"),
    isOpen: false,
    type: "DynamicRangeSlider",
    title: "Principle Amount",
    props: {
      componentId: "parAmountSlider",
      dataField: "parAmount",
      rangeLabels: (min, max) => (
        {
          "start": `${min} $`,
          "end": `${max} $`
        }
      ),
      interval: 2500,
      stepValue: 1,
      react: {
        and: ["transactionStatusSensor"]
      }
    }
  },
  {
    id: _.uniqueId("facet__"),
    isOpen: false,
    type: "DateRange",
    title: "Start Date",
    props: {
      componentId: "startDateSensor",
      dataField: "tranStartDate",
      numberOfMonths: 1,
      autoFocusEnd: false,
      customQuery: (value) => {
        if (value) {
          return {
            bool: {
              should: [
                { range: { tranStartDate: {gte: value.start, lte: value.end } } },
                { range: { rfpTranStartDate: {gte: value.start, lte: value.end } } }
              ]
            }
          }
        }
        return null

      }
    }
  },
  {
    id: _.uniqueId("facet__"),
    isOpen: false,
    type: "DateRange",
    title: "Expiry Date",
    props: {
      componentId: "expiryDateSensor",
      dataField: "expAwardDate",
      numberOfMonths: 1,
      customQuery: (value) => {
        if (value) {
          return {
            bool: {
              should: [
                { range: { tranExpectedEndDate: {gte: value.start, lte: value.end } } },
                { range: { rfpTranExpectedEndDate: {gte: value.start, lte: value.end } } }
              ]
            }
          }
        }
        return null

      }
    }
  }
]
