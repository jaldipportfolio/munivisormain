import React from "react"
import { connect } from "react-redux"
import _ from "lodash"
import Nav from "./Nav"
import EntityPageFilter from "../../EntityManagement/CommonComponents/EntityPageFilter"

const mapStateToProps = state => ({
  auth: state.auth
})

class ClientProspectDashboard extends React.Component {
  constructor() {
    super()
  }

  render() {
    return (
      <div>
        <Nav />
        <div id="main">
          <EntityPageFilter
            listType="client-prospect"
            auth={this.props.auth}
            entityId={this.props.nav2 || ""}
            searchPref="dash-cltprosp"
            title="Clients / Prospects"
          />
        </div>
      </div>
    )
  }
}

export default connect(mapStateToProps)(ClientProspectDashboard)
