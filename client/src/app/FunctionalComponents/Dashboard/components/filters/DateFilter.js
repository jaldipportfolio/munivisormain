import React from "react"
import moment from "moment"
import { connect } from "react-redux"

const mapStateToProps = state => {
  const { dashboardSearchPref } = state
  return {
    pref: dashboardSearchPref,
  }
}

class DateFilter extends React.Component {
  constructor() {
    super()

    this.handleSelectChange = this.handleSelectChange.bind(this)
  }

  componentDidMount() {
    const { pref, setQuery } = this.props

    if (pref.dateFilter && pref.dateFilter.value && pref.dateFilter.query) {
      setQuery(pref.dateFilter)
    }
  }

  handleSelectChange(e) {
    const { target: { value } } = e
    let query = {}
    switch (value) {
    case "today":
      query = {
        "range": {
          "taskDetails.taskEndDate": {
            "lte": moment().format("YYYY-MM-DD"),
            "gte": moment().format("YYYY-MM-DD")
          }
        }
      }
      break
    case "week":
      query = {
        "range": {
          "taskDetails.taskEndDate": {
            lte: moment().subtract(1, "week").endOf("week").format("YYYY-MM-DD"),
            gte: moment().subtract(1, "week").startOf("week").format("YYYY-MM-DD")
          }
        }
      }
      break
    case "month":
      query = {
        "range": {
          "taskDetails.taskEndDate": {
            "lte": moment().format("YYYY-MM-") + moment().daysInMonth(),
            "gte": moment().format("YYYY-MM-01")
          }
        }
      }
      break
    default:
      query = {}
      break
    }
    this.props.setQuery({
      query,
      value
    })

    this.props.handleFilterChange("dateFilter", {
      query,
      value
    })
  }

  render() {
    return (
      <div className="select is-fullwidth is-link is-small">
        <select defaultValue={(this.props.pref.dateFilter || {}).value || ""} onChange={this.handleSelectChange}>
          <option value="">All activities</option>
          <option value="today">Activity due today</option>
          <option value="week">Activity due this week</option>
          <option value="month">Activity due this month</option>
        </select>
      </div>
    )
  }
}

export default connect(mapStateToProps)(DateFilter)
