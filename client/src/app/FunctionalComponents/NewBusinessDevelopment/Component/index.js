import React from "react"
import { NavLink } from "react-router-dom"
import { connect } from "react-redux"
import { makeEligibleTabView } from "GlobalUtils/helpers"
import Loader from "../../../GlobalComponents/Loader"
import Details from "./Details"
import Documents from "./Documents"
import Audit from "../../../GlobalComponents/Audit"
import {activeStyle} from "../../../../globalutilities/consts"
import {
  bussinessActivityDetails,
} from "../../../StateManagement/actions/BussinessDevelopment"
import CONST from "../../../../globalutilities/consts"
import {fetchParticipantsAndOtherUsers} from "../../../StateManagement/actions/Transaction"


const TABS = [
  { path: "details", label: "Details" },
  { path: "documents", label: "Documents" },
  { path: "audit-trail", label: "Activity Log" }
]

class BusinessDevelopmentView extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      transaction: {},
      tranAction: {},
      loading: true,
    }
  }

  async componentWillMount() {
    const { user, nav, nav1, nav2, nav3 } = this.props
    let allEligibleNav = []
    if (nav && Object.keys(nav) && Object.keys(nav).length) {
      allEligibleNav =  nav[nav1] ? Object.keys(nav[nav1]).filter(key => nav[nav1][key]) : []
    }
    console.log("transaction eligible tab ", nav[nav1])

    if(nav2) {

      const transaction = await bussinessActivityDetails(nav2)
      if (transaction && transaction.actIssuerClient) {
        const isEligible = await makeEligibleTabView(
          user,
          nav2,
          TABS,
          allEligibleNav,
          transaction && transaction.actIssuerClient,
          transaction && transaction.actTranFirmId,
        )

        isEligible.tranAction.canEditTran = CONST.oppDecline.indexOf(transaction.actStatus) !== -1 ? false : isEligible.tranAction.canEditTran

        isEligible.tranAction.canTranStatusEditDoc = CONST.oppDecline.indexOf(transaction.actStatus) === -1 && isEligible.tranAction.canEditDocument

        if (isEligible && isEligible.tranAction && (!isEligible.tranAction.canEditTran && !isEligible.tranAction.canViewTran)) {
          this.props.history.push("/dashboard")
        } else if (isEligible.tranAction.view.indexOf(nav3) === -1) {
          const view = isEligible.tranAction.view.includes("details") ? "details" : isEligible.tranAction.view[0] || ""
          this.props.history.push(`/${nav1}/${nav2}/${view}`)
        } else {
          const participants = await fetchParticipantsAndOtherUsers(nav2)
          this.setState({
            transaction,
            participants,
            loading: false,
            tranAction: isEligible.tranAction,
            tabs: isEligible.tabs
          })
        }

      } else {
        this.props.history.push("/dashboard")
      }
    }

  }

  onParticipantsRefresh = async () => {
    this.setState({
      participants: await fetchParticipantsAndOtherUsers(this.props.nav2)
    })
  }

  renderViewSelection = (loanId) => (
    <nav className="tabs is-boxed">
      <ul>{this.renderTabs(this.state.tabs, loanId)}</ul>
    </nav>
  )

  renderTabs = (tabs, loanId) =>
    tabs.map(t => (
      <li key={t.path}>
        <NavLink to={`/bus-development/${loanId}/${t.path}`} activeStyle={activeStyle}>
          {t.label}
        </NavLink>
      </li>
    ))

  renderSelectedView = (loanId, option) => {
    const { tranAction, transaction, participants } = this.state
    switch (option) {
    case "details":
      return (
        <Details
          {...this.props}
          tranAction={tranAction}
          transaction={transaction}
          participants={participants}
          onParticipantsRefresh={this.onParticipantsRefresh}
        />
      )
    case "documents":
      return (
        <Documents
          {...this.props}
          tranAction={tranAction}
          transaction={transaction}
          participants={participants}
        />
      )
    case "audit-trail":
      return <Audit {...this.props} />
    default:
      return <p>{option}</p>
    }
  }


  render() {

    const { option } = this.props
    const { transaction } = this.state
    const bussDevId = transaction && transaction._id

    const loading = () => <Loader />

    if (this.state.loading) {
      return loading()
    }

    return (
      <div>
        <div className="hero is-link">
          <div className="hero-foot hero-footer-padding">
            <div className="container">
              {this.renderViewSelection(bussDevId, option)}
            </div>
          </div>
        </div>
        <section id="main" className="bankloan">
          {this.renderSelectedView(bussDevId, option)}
        </section>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
  nav: state.nav || {},
  loginEntity: (state.auth && state.auth.loginDetails && state.auth.loginDetails.userEntities
    && state.auth.loginDetails.userEntities.length && state.auth.loginDetails.userEntities[0]) || {}
})

export default connect(mapStateToProps, null)(BusinessDevelopmentView)
