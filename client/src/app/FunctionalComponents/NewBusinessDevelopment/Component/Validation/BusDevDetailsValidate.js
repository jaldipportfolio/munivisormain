import Joi from "joi-browser"

const busDevSchema = () =>
  Joi.object().keys({
    actIssuerClientEntityName: Joi.string().required().optional(),
    actIssuerClient: Joi.string().required().optional(),
    actIssuerClientMsrbType: Joi.string().required().optional(),
    actPrimarySector: Joi.string().allow("").optional(),
    actSecondarySector: Joi.string().allow("").optional(),
    actIssueName: Joi.string().allow("").optional(),
    actProjectName: Joi.string().required(),
  })

export const BusDevDetailsValidate = (inputTransDistribute, minDate) =>
  Joi.validate(inputTransDistribute, busDevSchema(minDate), {
    abortEarly: false,
    stripUnknown: false
  })

