import React from "react"
import { withRouter } from "react-router-dom"
import { connect } from "react-redux"
import cloneDeep from "lodash.clonedeep"
import { toast } from "react-toastify"
import Loader from "../../../../GlobalComponents/Loader"
import CONST, { ContextType } from "../../../../../globalutilities/consts"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import TranDocuments from "../../../../GlobalComponents/TranDocuments"
import {
  putTransactionDocStatus,
  pullTransactionDoc
} from "../../../../StateManagement/actions/Transaction"
import {
  putBusDevDocumentTransaction
} from "../../../../StateManagement/actions/BussinessDevelopment"
import { getDocumentList } from "../../../../../globalutilities/helpers"

class Documents extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      documentsList: [],
      attachedFolder: [],
      loading: true
    }
  }

  async componentWillMount() {
    const { transaction, user } = this.props
    const busDevDocuments =
      transaction &&
      transaction.busDevDocuments &&
      transaction.busDevDocuments.length
        ? cloneDeep(transaction.busDevDocuments)
        : []
    const filterDocuments = getDocumentList(
      cloneDeep(busDevDocuments),
      user
    )

    this.setState({
      documentsList:
        transaction.actTranFirmId === user.entityId
          ? busDevDocuments
          : filterDocuments || [],
      attachedFolder: transaction && transaction.attachedFolder || [],
      loading: false
    })
  }

  onDocSave = (docs, callback) => {
    const { transaction, user } = this.props
    console.log(docs)
    putBusDevDocumentTransaction(this.props.nav3, docs, res => {
      if (res && res.status === 200) {
        toast("Business Development Documents has been updated!", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.SUCCESS
        })
        this.props.submitAuditLogs(this.props.nav2)
        const filterDocuments = getDocumentList(
          cloneDeep(res.data && res.data.busDevDocuments),
          user
        )
        callback({
          status: true,
          documentsList:
            transaction.actTranFirmId === user.entityId
              ? res.data && res.data.busDevDocuments
              : filterDocuments || []
        })
      } else {
        toast("Something went wrong!", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.ERROR
        })
        callback({
          status: false
        })
      }
    })
  };

  onStatusChange = async (e, doc, callback) => {
    const { transaction, user } = this.props
    let document = {}
    let type = ""
    const { name, value } = (e && e.target) || {}
    if (name) {
      document = {
        [name]: value
      }
      if (value !== "Public") {
        document.settings = { firms: [], users: [], selectLevelType: "" }
      }
      type = "&type=status"
    } else {
      document = {
        ...doc
      }
    }
    const res = await putTransactionDocStatus(
      this.props.nav2,
      `ActBusDev${type}`,
      { _id: doc._id, ...document }
    )
    if (res && res.status === 200) {
      toast("Document has been updated!", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.SUCCESS
      })
      this.props.submitAuditLogs(this.props.nav2)
      const filterDocuments = getDocumentList(
        cloneDeep(res.data && res.data.document),
        user
      )
      if (name) {
        callback({
          status: true
        })
      } else {
        callback({
          status: true,
          documentsList:
            transaction.actTranFirmId === user.entityId
              ? res.data && res.data.document
              : filterDocuments || []
        })
      }
    } else {
      toast("Something went wrong!", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.ERROR
      })
      callback({
        status: false
      })
    }
  };

  onDeleteDoc = async (documentId, callback) => {
    const { transaction, user } = this.props
    const res = await pullTransactionDoc(
      this.props.nav2,
      `?tranType=ActBusDev&docId=${documentId}`
    )
    if (res && res.status === 200) {
      toast("Document removed successfully", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.SUCCESS
      })
      this.props.submitAuditLogs(this.props.nav2)
      const filterDocuments = getDocumentList(
        cloneDeep(res.data && res.data.busDevDocuments),
        user
      )
      callback({
        status: true,
        documentsList:
          transaction.actTranFirmId === user.entityId
            ? res.data && res.data.busDevDocuments
            : filterDocuments || []
      })
    } else {
      toast("Something went wrong!", {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.ERROR
      })
      callback({
        status: false
      })
    }
  };

  render() {
    const { documentsList, attachedFolder } = this.state
    const { transaction, tranAction } = this.props
    const tags = {
      clientId: transaction.actIssuerClient,
      clientName: `${transaction.actIssuerClientEntityName}`,
      tenantId: transaction.actTranFirmId,
      tenantName: transaction.actTranFirmName,
      contextName: transaction.actIssueName ? transaction.actIssueName : transaction.actProjectName
    }
    const loading = <Loader />

    if (this.state.loading) {
      return loading
    }
    return (
      <div className="business-development-documents">
        <TranDocuments
          {...this.props}
          onSave={this.onDocSave}
          nav1={this.props.nav1}
          tranId={this.props.nav2}
          transaction={transaction}
          title="Upload Business Development Documents"
          pickCategory="LKUPDOCCATEGORIES"
          pickSubCategory="LKUPCORRESPONDENCEDOCS"
          pickAction="LKUPDOCACTION"
          category="busDevDocuments"
          isDisabled={!tranAction.canTranStatusEditDoc}
          documents={documentsList}
          attachedFolder={attachedFolder}
          contextType={ContextType.busDev}
          tags={tags}
          onStatusChange={this.onStatusChange}
          onDeleteDoc={this.onDeleteDoc}
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {}
})

const WrappedComponent = withAuditLogs(Documents)
export default withRouter(
  connect(
    mapStateToProps,
    null
  )(WrappedComponent)
)
