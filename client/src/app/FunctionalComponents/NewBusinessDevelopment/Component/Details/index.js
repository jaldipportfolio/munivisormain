import React from "react"
import { connect } from "react-redux"
import {toast} from "react-toastify"
import { getPicklistByPicklistName, checkClientIsHistorical } from "GlobalUtils/helpers"
import Accordion from "../../../../GlobalComponents/Accordion"
import RatingSection from "../../../../GlobalComponents/RatingSection"
import Loader from "../../../../GlobalComponents/Loader"
import withAuditLogs from "../../../../GlobalComponents/withAuditLogs"
import TransactionNotes from "../../../../GlobalComponents/TransactionNotes"
import TranDetails from "./components/TranDetails"
import ReletedTranGlob from "../../../../GlobalComponents/ReletedTranGlob"
import CONST from "../../../../../globalutilities/consts"
import {sendEmailAlert} from "../../../../StateManagement/actions/Transaction"
import {
  putBusDevTransaction,
  postBusDevNotes,
  getTransactionRelatedTask
} from "../../../../StateManagement/actions/BussinessDevelopment"
import {pullRelatedTransactions} from "../../../../StateManagement/actions/CreateTransaction"
import { BusDevDetailsValidate } from "../Validation/BusDevDetailsValidate"
import SendEmailModal from "../../../../GlobalComponents/SendEmailModal"
import TaskListReatTable from "./components/TaskListReactTable"


class Details extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      bussDev: {
        actTranRelatedType: "",
        actRelTrans: []
      },
      activeItem: [0],
      tranNotes: [],
      tasks: [],
      relatedTypes: [],
      relatedTransactions: [],
      tempTransaction: {},
      errorMessages: {},
      transaction: {},
      dropDown: {},
      email: {
        category: "",
        message: "",
        subject: ""
      },
      modalState: false,
      isSaveDisabled: false,
      loading: true,
    }
  }

  async componentDidMount() {
    const { transaction, user } = this.props
    const tasks = await getTransactionRelatedTask(transaction && transaction._id, {userId: user && user.userId})
    const picResult = await getPicklistByPicklistName(["LKUPPRIMARYSECTOR", "LKUPRELATEDTYPE"])
    const result = (picResult.length && picResult[1]) || {}
    const primarySec =  picResult[2].LKUPPRIMARYSECTOR || {}
    const isClientHistorical = await checkClientIsHistorical(transaction && transaction.actIssuerClient)
    this.setState(prevState => ({
      relatedTypes: result.LKUPRELATEDTYPE || ["Bond Hedge", "Loan Hedge"],
      dropDown: {
        ...this.state.dropDown,
        primarySectors: result.LKUPPRIMARYSECTOR || [],
        secondarySectors: primarySec,
      },
      transaction,
      relatedTransactions: transaction.actRelTrans || [],
      tranNotes: transaction && transaction.tranNotes || [],
      tempTransaction: {
        actPrimarySector: transaction.actPrimarySector,
        actSecondarySector: transaction.actSecondarySector,
        actIssueName: transaction.actIssueName,
        actProjectName: transaction.actProjectName,
      },
      email: {
        ...prevState.email,
        subject: `Transaction - ${transaction.actIssueName ||
        transaction.actProjectName} - Notification`
      },
      tasks,
      isClientHistorical,
      loading: false
    }))
  }

  onChangeItem = (item, category) => {
    this.setState(prevState => ({
      [category]: {
        ...prevState[category],
        ...item
      }
    }))
  }

  onBlur = (category, change) => {
    const { user } = this.props
    const userName = `${user.userFirstName} ${user.userLastName}` || ""
    this.props.addAuditLog({userName, log: `${change} from details`, date: new Date(), key: category})
  }

  onRelatedTranSelect = item => {
    const { bussDev } = this.state
    const { user } = this.props
    this.props.addAuditLog({
      userName: `${user.userFirstName} ${user.userLastName}` || "",
      log: `In details related type ${
        bussDev.actTranRelatedType
      } and  business development ${item.name || ""}`,
      date: new Date(),
      key: "summary"
    })
    this.setState(prevState => ({
      bussDev: {
        ...prevState.bussDev,
        actRelTrans: item
      }
    }))
  }

  onChange = e => {
    const { bussDev } = this.state
    const { user } = this.props
    bussDev[e.target.name] = e.target.value
    this.props.addAuditLog({
      userName: `${user.userFirstName} ${user.userLastName}` || "",
      log: `In details select related type ${e.target.value}`,
      date: new Date(),
      key: "details"
    })
    this.setState({
      bussDev: {
        ...bussDev,
        actRelTrans: []
      }
    })
  }

  onModalSave = () => {
    this.setState({
      modalState: true,
      relatedSave: true
    })
  }

  onRelatedtranSave = () => {
    const { email, bussDev } = this.state
    const emailPayload = {
      tranId: this.props.nav2,
      type: this.props.nav1,
      sendEmailUserChoice: true,
      emailParams: {
        url: window.location.pathname.replace("/", ""),
        ...email
      }
    }

    const actRelTrans = {
      ...bussDev.actRelTrans,
      relType: bussDev.actTranRelatedType
    }

    if (!bussDev.actTranRelatedType || actRelTrans.length === 0)
      return false

    this.setState(
      {
        isSaveDisabled: true,
        modalState: false
      },
      () => {
        putBusDevTransaction(
          this.props.nav3,
          `${this.props.nav2}?subType=related`,
          actRelTrans,
          res => {
            if (res && res.status === 200) {
              toast("Add related transaction successfully", {
                autoClose: CONST.ToastTimeout,
                type: toast.TYPE.SUCCESS
              })
              this.props.submitAuditLogs(this.props.nav2)
              this.setState(
                {
                  transaction: res.data,
                  relatedTransactions: (res.data && res.data.actRelTrans) || [],
                  bussDev: {
                    actTranRelatedType: bussDev.actTranRelatedType,
                    actRelTrans: ""
                  },
                  isSaveDisabled: false,
                  relatedSave: false
                },
                async () => {
                  await sendEmailAlert(emailPayload)
                }
              )
            } else {
              toast("something went wrong", {
                autoClose: CONST.ToastTimeout,
                type: toast.TYPE.ERROR
              })
              this.setState({
                isSaveDisabled: false,
                relatedSave: false
              })
            }
          }
        )
      }
    )
  }

  onRelatedtranCancel = () => {
    this.setState({
      bussDev: {
        actTranRelatedType: "",
        actRelTrans: []
      }
    })
  }

  deleteRelatedTran = (relatedId, relTranId, relType, relatedType) => {
    const { activeItem } = this.state
    const { transaction } = this.props
    const query = `ActBusDev?tranId=${transaction._id}&relatedId=${relatedId}&relTranId=${relTranId}&relType=${relType}&relatedType=${relatedType}`
    this.setState({
      loading: true
    }, async () => {
      const related = await pullRelatedTransactions(query)
      const active = activeItem.includes(1) ? activeItem : activeItem.push(1)
      if(related && related.done){
        toast("related transaction removed successfully", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.SUCCESS
        })
        this.setState({
          relatedTransactions: related && related.relatedTransaction || [],
          loading: false,
          activeItem: active
        })
      } else {
        toast("Something went wrong", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.ERROR
        })
        this.setState({
          loading: false,
          activeItem: active
        })
      }
    })
  }

  onSave = () => {
    const { tempTransaction, isClientHistorical } = this.state
    const payload = { ...tempTransaction }
    let errors = {}
    if (!isClientHistorical) {
      errors = BusDevDetailsValidate(payload)
    }

    if (errors && errors.error) {
      const errorMessages = {}
      console.log(errors.error.details)
      errors.error.details.forEach(err => {
        errorMessages[err.context.key] = "Required" || err.message
      })
      console.log(errorMessages)
      this.setState({ errorMessages })
      this.setState(prevState => ({
        errorMessages: { ...prevState.errorMessages, details: errorMessages }
      }))
      return
    }

    this.setState({
      modalState: true
    })
  }

  onConfirmationSave = () => {
    const { email, tempTransaction } = this.state
    const payload = { ...tempTransaction }

    const tranId = this.props.nav2
    const type = this.props.nav1
    const emailParams = {
      tranId,
      type,
      sendEmailUserChoice: true,
      emailParams: {
        url: window.location.pathname.replace("/", ""),
        ...email
      }
    }

    this.setState(
      {
        isSaveDisabled: true,
        modalState: false,
        errorMessages: {}
      },
      () => {
        putBusDevTransaction("details", this.props.nav2, payload, res => {
          if (res && res.status === 200) {
            toast("Add Business Development Details successfully", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.SUCCESS
            })
            this.props.submitAuditLogs(this.props.nav2)
            this.setState({
              tempTransaction,
              isSaveDisabled: false
            }, async () => {
              await sendEmailAlert(emailParams)
            }
            )
          } else {
            toast("something went wrong", {
              autoClose: CONST.ToastTimeout,
              type: toast.TYPE.ERROR
            })
            this.setState({
              isSaveDisabled: false
            })
          }
        })
      }
    )
  }

  onCancel = () => {
    const { transaction } = this.props
    this.setState({
      tempTransaction: {
        actPrimarySector: transaction.actPrimarySector,
        actSecondarySector: transaction.actSecondarySector,
        actIssueName: transaction.actIssueName,
        actProjectName: transaction.actProjectName,
      },
    })
  }

  onNotesSave = (payload, callback) => {
    postBusDevNotes(this.props.nav2, payload, res => {
      if (res && res.status === 200) {
        toast("Add Business Development Notes successfully", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.SUCCESS
        })
        // this.props.submitAuditLogs(this.props.nav2)
        this.setState({
          tranNotes: res.data && res.data.tranNotes || []
        })
        callback({notesList: res.data && res.data.tranNotes || []})
      } else {
        toast("something went wrong", {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.ERROR
        })
      }
    })
  }

  onModalChange = (state, name) => {
    if (name === "message") {
      state = {
        email: {
          ...this.state.email,
          ...state
        }
      }
    }
    this.setState({
      ...state
    })
  }

  render() {
    const {
      activeItem,
      tranNotes,
      tempTransaction,
      errorMessages,
      dropDown,
      relatedTypes,
      transaction,
      isSaveDisabled,
      bussDev,
      relatedTransactions,
      busDevDetails,
      relatedSave,
      modalState,
      tasks,
      email
    } = this.state
    const { tranAction, nav1, nav2, participants, onParticipantsRefresh } = this.props
    const canEditTran = (tranAction && tranAction.canEditTran) || false

    const loading = () => <Loader />

    if (this.state.loading) {
      return loading()
    }

    return (
      <div>
        <SendEmailModal
          nav1={nav1}
          modalState={modalState}
          email={email}
          onModalChange={this.onModalChange}
          participants={participants}
          onParticipantsRefresh={onParticipantsRefresh}
          onSave={
            relatedSave === true
              ? this.onRelatedtranSave
              : this.onConfirmationSave
          }
        />
        <Accordion multiple isRequired={!!activeItem.length} activeItem={activeItem}
          boxHidden
          render={({activeAccordions, onAccordion}) =>
            <div>
              <RatingSection onAccordion={() => onAccordion(0)} title="Transaction Summary">
                {activeAccordions.includes(0) &&
                <TranDetails
                  transaction={tempTransaction || {}}
                  errorMessages={errorMessages}
                  item={busDevDetails}
                  dropDown={dropDown}
                  canEditTran={canEditTran}
                  onChangeItem={this.onChangeItem}
                  getBorrowerByFirm={this.getBorrowerByFirm}
                  onBlur={this.onBlur}
                  onSave={this.onSave}
                  onCancel={this.onCancel}
                  isSaveDisabled={isSaveDisabled}
                  category="busDevDetails"
                />
                }
              </RatingSection>

              <RatingSection
                onAccordion={() => onAccordion(1)}
                title="Related transactions"
                style={{ overflowY: "unset" }}
              >
                {activeAccordions.includes(1) && (
                  <ReletedTranGlob
                    relatedTypes={relatedTypes}
                    transaction={transaction}
                    errorMessages={errorMessages}
                    isSaveDisabled={isSaveDisabled}
                    item={bussDev}
                    tranId={nav2}
                    canEditTran={tranAction.canEditTran || false}
                    onRelatedTranSelect={this.onRelatedTranSelect}
                    onChange={this.onChange}
                    onRelatedtranSave={this.onModalSave}
                    onRelatedtranCancel={this.onRelatedtranCancel}
                    relatedTransactions={relatedTransactions}
                    deleteRelatedTran={this.deleteRelatedTran}
                  />
                )}
              </RatingSection>

              <RatingSection
                onAccordion={() => onAccordion(2)}
                title="Transaction Related Tasks"
                style={{ overflowY: "unset" }}
              >
                {activeAccordions.includes(2) && (
                  <TaskListReatTable
                    taskList={tasks || []}
                  />
                )}
              </RatingSection>

            </div>
          }
        />
        <TransactionNotes
          notesList={tranNotes || []}
          canEditTran={canEditTran || false}
          onSave={this.onNotesSave}
        />
      </div>
    )
  }
}


const mapStateToProps = (state) => ({
  userEmail: (state.auth && state.auth.userEmail) || "",
  user: (state.auth && state.auth.userEntities) || {},
})
const WrappedComponent = withAuditLogs(Details)

export default connect(mapStateToProps,null)(WrappedComponent)
