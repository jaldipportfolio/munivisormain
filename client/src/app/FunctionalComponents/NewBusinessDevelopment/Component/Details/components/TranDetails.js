import React from "react"
import {
  TextLabelInput,
} from "../../../../../GlobalComponents/TextViewBox"

const TranDetails = ({
  errorMessages,
  item = {},
  transaction,
  canEditTran,
  dropDown,
  onBlur,
  onChangeItem,
  category,
  onSave,
  onCancel,
  isSaveDisabled
}) => {

  const onChange = event => {
    if (
      event.target.name === "actPrimarySector" ||
      event.target.name === "actSecondarySector"
    ) {
      const tranSector = {
        ...transaction,
        [event.target.name]: event.target.value || ""
      }
      if (event.target.name === "actPrimarySector") {
        tranSector.actSecondarySector = ""
      }
      onChangeItem(tranSector, "tempTransaction")
    } else {
      onChangeItem(
        {
          ...transaction,
          [event.target.name]: event.target.value || ""
        },
        "tempTransaction"
      )
    }
  }

  const onBlurInput = event => {
    if (event.target.title && event.target.value) {
      onBlur(
        category,
        `${event.target.title || "empty"} change to ${
          event.target.type === "checkbox"
            ? event.target.checked
            : event.target.value || "empty"
        }`
      )
    }
  }

  return (
    <div>

      <div className="columns">
        <div className="column is-3">
          <div className="columns">
            <TextLabelInput
              label="Transaction Name"
              error={errorMessages.actIssueName || ""}
              name="actIssueName"
              value={transaction.actIssueName || ""}
              disabled={!canEditTran}
              onChange={onChange}
              onBlur={onBlurInput}
            />
          </div>
        </div>

        <div className="column is-3">
          <div className="columns">
            <TextLabelInput
              required
              label="Project Description (internal)"
              error={errorMessages.actProjectName || ""}
              name="actProjectName"
              value={transaction.actProjectName || ""}
              disabled={!canEditTran}
              onChange={onChange}
              onBlur={onBlurInput}
            />
          </div>
        </div>

        <div className="column">
          <p className="multiExpLbl">Primary Sector</p>
          <div className="control">
            <div className="select is-small is-link" style={{ width: "100%" }}>
              <select
                title="Primary Sector"
                value={transaction.actPrimarySector || ""}
                onChange={onChange}
                disabled={!canEditTran}
                name="actPrimarySector"
                onBlur={onBlurInput}
                style={{ width: "100%" }}
              >
                <option value="" disabled="">
                  Pick Primary Sector
                </option>
                {dropDown && dropDown.primarySectors && dropDown.primarySectors.map(t => (
                  <option key={t && t.label} value={t && t.label} disabled={!t.included}>
                    {t && t.label}
                  </option>
                ))}
              </select>
              {errorMessages && errorMessages.actPrimarySector && (
                <small className="text-error">
                  {errorMessages.actPrimarySector || ""}
                </small>
              )}
            </div>
          </div>
        </div>

        <div className="column">
          <p className="multiExpLbl">Secondary Sector</p>
          <div className="select is-small is-link" style={{ width: "100%" }}>
            <select
              title="Secondary Sector"
              name="actSecondarySector"
              value={transaction.actSecondarySector || ""}
              disabled={!canEditTran}
              onChange={onChange}
              onBlur={onBlurInput}
              style={{ width: "100%" }}
            >
              <option value="" disabled="">
                Pick Secondary Sector
              </option>
              {transaction.actPrimarySector &&
              dropDown.secondarySectors[transaction.actPrimarySector]
                ? dropDown.secondarySectors[transaction.actPrimarySector].map(
                  sector => (
                    <option key={sector && sector.label} value={sector && sector.label} disabled={!sector.included}>
                      {sector && sector.label}
                    </option>
                  )
                )
                : null}
            </select>
            {errorMessages && errorMessages.actSecondarySector && (
              <small className="text-error">
                {errorMessages.actSecondarySector || ""}
              </small>
            )}
          </div>
        </div>

      </div>

      {
        canEditTran ?
          <div className="column is-full" style={{marginTop:"25px"}}>
            <div className="field is-grouped-center">
              <div className="control">
                <button className="button is-link" onClick={onSave} disabled={isSaveDisabled || false}>Save</button>
              </div>
              <div className="control">
                <button className="button is-light" onClick={onCancel}>Cancel</button>
              </div>
            </div>
          </div>
          : null
      }

    </div>
  )
}

export default TranDetails
