import React from "react"
import moment from "moment"
import { NavLink } from "react-router-dom"
import ReactTable from "react-table"
import "react-table/react-table.css"

const TaskListReatTable = ({
  taskList,
  pageSizeOptions = [5, 10, 20, 50, 100],
  disable
}) => {
  const columns = [
    {
      id: "taskStartDate",
      Header: "Task Start Date",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            { item && item.taskDetails && item.taskDetails.taskStartDate && moment(item.taskDetails.taskStartDate).format("MM-DD-YYYY") }
          </div>
        )
      },
      sortMethod: (a, b) => a.taskDetails.taskStartDate - b.taskDetails.taskStartDate
    },
    {
      id: "taskEndDate",
      Header: "Task End Date",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            { item && item.taskDetails && item.taskDetails.taskEndDate && moment(item.taskDetails.taskEndDate).format("MM-DD-YYYY") }
          </div>
        )
      },
      sortMethod: (a, b) => a.taskDetails.taskEndDate - b.taskDetails.taskEndDate
    },
    {
      id: "taskType",
      Header: "Task Type",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            { item && item.taskDetails && item.taskDetails.taskType || "" }
          </div>
        )
      },
      sortMethod: (a, b) => a.taskDetails.taskType - b.taskDetails.taskType
    },
    {
      id: "taskDescription",
      Header: "To Do",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            <NavLink to={`/tasks/${item._id}`}>{item && item.taskDetails && item.taskDetails.taskDescription || "-"}</NavLink>
          </div>
        )
      },
      sortMethod: (a, b) => a.taskDetails.taskDescription - b.taskDetails.taskDescription
    },
    {
      id: "taskStatus",
      Header: "Activity Status",
      className: "multiExpTblVal",
      accessor: item => item,
      Cell: row => {
        const item = row.value
        return (
          <div className="hpTablesTd wrap-cell-text">
            { item && item.taskDetails && item.taskDetails.taskStatus === "Closed" ? item.taskDetails.taskStatus : "Open" }
          </div>
        )
      },
      sortMethod: (a, b) => a.taskDetails.taskStatus - b.taskDetails.taskStatus
    },
  ]


  return (
    <ReactTable
      columns={columns}
      data={taskList}
      showPaginationBottom
      defaultPageSize={5}
      pageSizeOptions={pageSizeOptions}
      className="-striped -highlight is-bordered"
      showPageJump
      minRows={2}
    />
  )
}

export default TaskListReatTable
