import React, { Component } from "react"
import Loader from "../../GlobalComponents/Loader"
import BusinessDevelopmentView from "./Component"

class NewBusinessDevelopment extends Component {
  render() {
    const { nav2, nav3 } = this.props
    if(nav2) {
      return (
        <div>
          <BusinessDevelopmentView {...this.props} option={nav3} />
        </div>
      )
    }
    return <Loader />
  }
}

export default NewBusinessDevelopment
