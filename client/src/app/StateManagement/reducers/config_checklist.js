import {
  GET_CONFIG_CHECKLIST,
  SAVE_CONFIG_CHECKLIST,
  ADD_NEW_CHECKLIST
} from "../actions/types"

const INITIAL_STATE = []

export default (state=INITIAL_STATE, action) => {
  switch (action.type) {
  case GET_CONFIG_CHECKLIST:
    console.log("in reducer GET_CONFIG_CHECKLIST : ", action)
    return [...action.payload]
  case ADD_NEW_CHECKLIST:
    console.log("in reducer ADD_NEW_CHECKLIST : ", action)
    return action.payload
  case SAVE_CONFIG_CHECKLIST:
    console.log("in reducer SAVE_CONFIG_CHECKLIST : ", action)
    return action.payload
  default:
    return state
  }
}
