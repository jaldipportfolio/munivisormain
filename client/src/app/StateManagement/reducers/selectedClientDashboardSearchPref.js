import {
  UPDATE_SELECTED_CLIENT_DASHBOARD_SEARCH_PREF
} from "../actions"

const INITIAL_STATE = "0"

export default (state=INITIAL_STATE, action) => {
  switch (action.type) {
  case UPDATE_SELECTED_CLIENT_DASHBOARD_SEARCH_PREF:
    return action.data
  default:
    return state
  }
}
