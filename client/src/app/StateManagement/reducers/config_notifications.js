import {
  ADD_NEW_NOTIFICATION,
  GET_CONFIG_NOTIFICATIONS,
  SAVE_CONFIG_NOTIFICATIONS
} from "../actions/types"

const INITIAL_STATE = []

export default (state=INITIAL_STATE, action) => {
  switch (action.type) {
  case GET_CONFIG_NOTIFICATIONS:
    console.log("in reducer GET_CONFIG_NOTIFICATIONS : ", action)
    return [...action.payload]
  case ADD_NEW_NOTIFICATION:
    console.log("in reducer ADD_NEW_NOTIFICATION : ", action)
    return action.payload
  case SAVE_CONFIG_NOTIFICATIONS:
    console.log("in reducer SAVE_CONFIG_NOTIFICATIONS : ", action)
    return action.payload
  default:
    return state
  }
}
