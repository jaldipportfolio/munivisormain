import {
  UPDATE_CLIENT_DASHBOARD_SEARCH_PREF,
  RENAME_SELECTED_CLIENT_DASHBOARD_PREF
} from "../actions"

const INITIAL_STATE = {}

export default (state=INITIAL_STATE, action) => {
  switch (action.type) {
  case UPDATE_CLIENT_DASHBOARD_SEARCH_PREF:
    return {
      ...state,
      [action.id]: action.payload
    }
  case RENAME_SELECTED_CLIENT_DASHBOARD_PREF:
    return action.pref
  default:
    return state
  }
}
