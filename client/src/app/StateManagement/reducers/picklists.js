import {
  GET_PICKLISTS
} from "../actions/types"

const INITIAL_STATE = []

export default (state=INITIAL_STATE, action) => {
  switch (action.type) {
  case GET_PICKLISTS:
    return [...action.payload]
  default:
    return state
  }
}
