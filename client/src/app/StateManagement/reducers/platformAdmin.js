import {
  SET_PLATFORMADMIN,
} from "../actions/types"

const INITIAL_STATE = {}

export default (state=INITIAL_STATE, action) => {
  switch (action.type) {
  case SET_PLATFORMADMIN:
    return {
      ...action.payload,
    }
  default:
    return state
  }
}
