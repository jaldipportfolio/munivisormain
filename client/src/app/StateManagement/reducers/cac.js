import {
  GET_CONTROLS,
  ADD_NEW_CONTROL,
  SAVE_CONTROLS
} from "../actions/types"

const INITIAL_STATE = []

export default (state=INITIAL_STATE, action) => {
  switch (action.type) {
  case GET_CONTROLS:
    console.log("in reducer GET_CONTROLS : ", action)
    return [...action.payload]
  case ADD_NEW_CONTROL:
    console.log("in reducer ADD_NEW_CONTROL : ", action)
    return action.payload
  case SAVE_CONTROLS:
    console.log("in reducer SAVE_CONTROLS : ", action)
    return action.payload
  default:
    return state
  }
}
