import * as types from "AppState/actions/AdminManagement/actionsType"

const initialState = {
  updated:false
}
export default function admUserReducer(state = initialState, action) {
  switch (action.type) {
  case types.USER_DETAIL_BY_ID:
    const payload = Object.assign({}, action.payload)
    return {
      ...state,
      ...payload,
      error: "",
      userAdded:false,
      updated: false
    }
  case types.UPDATED_USERS_DETAIL:
    const updatedUserDetails = Object.assign({}, action.payload)
    return {
      ...state,
      updatedUserDetails,
      error: "",
      userAdded:false,
      updated: true
    }
  case types.NEW_ADDED_USER_DETAIL:
    const newUserDetails = Object.assign({}, action.payload)
    return {
      ...state,
      newUserDetails,
      error: "",
      userAdded:true,
      updated: false
    }
  default:
    return state
  }
}