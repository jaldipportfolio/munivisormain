import {
  GET_NAV_OPTIONS
} from "../actions/types"

const INITIAL_STATE = {}

export default (state=INITIAL_STATE, action) => {
  switch (action.type) {
  case GET_NAV_OPTIONS:
    console.log("action in nav reducer :", action.payload)
    return {...action.payload}
  default:
    return state
  }
}
