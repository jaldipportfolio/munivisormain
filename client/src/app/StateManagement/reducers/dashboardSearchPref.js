import {
  UPDATE_DASHBOARD_PREF
} from "../actions"

const INITIAL_STATE = {}

export default (state=INITIAL_STATE, action) => {
  switch (action.type) {
  case UPDATE_DASHBOARD_PREF:
    return {...action.payload}
  default:
    return state
  }
}
