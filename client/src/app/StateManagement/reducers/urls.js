import {
  GET_URLS_FOR_FORNTEND,
} from "../actions/types"

const INITIAL_STATE = {}

export default (state=INITIAL_STATE, action) => {
  switch (action.type) {
  case GET_URLS_FOR_FORNTEND: {
    return {
      ...state,
      ...action.payload
    }
  }
  default:
    return state
  }
}
