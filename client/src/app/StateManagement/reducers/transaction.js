import {
  GET_ELIGIBLE_TRANSACTIONS,
} from "../actions/types"

const INITIAL_STATE = {
  eligibleTrans: []
}

export default (state=INITIAL_STATE, action) => {
  switch (action.type) {
  case GET_ELIGIBLE_TRANSACTIONS: {
    return {
      ...state,
      eligibleTrans: action.payload
    }
  }
  default:
    return state
  }
}
