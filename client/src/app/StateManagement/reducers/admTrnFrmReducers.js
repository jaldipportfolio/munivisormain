import * as types from "AppState/actions/AdminManagement/actionsType"

const initialState = {
  updated : false
}
export default function admFirmDetailsReducer(state = initialState, action) {
  switch (action.type) {
  case types.ADM_TRN_ADD_FRM:
    return [...state, Object.assign({}, action.payload)]
  case types.ADM_ALL_FIRM_LIST:
    const firmList = [{
      label: "Pick MSRB Firm Name",
      value: ""
    }]
    action.payload.map(item => {
      firmList.push({
        label: item.firmName,
        value: item._id
      })
    })
    return {
      ...state,
      firmList,
      updated: false,
      firmAdded:false
    }
  case types.FIRM_ERROR:
    const error = Object.assign({}, action.payload)
    return {
      ...state,
      error
    }
  case types.FIRM_DETAIL_BY_ID:
    const firmDetailById = Object.assign({}, action.payload)
    return {
      ...state,
      firmDetailById,
      error: "",
      updated: false,
      firmAdded:false
    }
  case types.UPDATED_FIRM_DETAIL:
    const updatedFirmDetails = Object.assign({}, action.payload)
    return {
      ...state,
      updatedFirmDetails,
      error: "",
      updated: true,
      firmAdded:false
    }
  case types.NEW_ADDED_FIRM_DETAIL:
    const newFirmDetails = Object.assign({}, action.payload)
    return {
      ...state,
      newFirmDetails,
      error: "",
      updated: false,
      firmAdded:true
    }
  case types.SAVE_FIRM_DATA:
  {
    const {payload} = action
    const oldFirmDetails = Object.assign({},payload)
    return{
      oldFirmDetails
    }
  }
  default:
    return state
  }
}
