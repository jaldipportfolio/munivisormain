import {
  GET_FRUITS,
  SAVE_FRUITS,
  REMOVE_FRUIT
} from "../actions/types"

const INITIAL_STATE = {}

export default (state=INITIAL_STATE, action) => {
  switch (action.type) {
  case GET_FRUITS:
    console.log("in reducer GET_FRUITS : ", action)
    return {...state, ...action.payload}
  case SAVE_FRUITS:
    console.log("in reducer SAVE_FRUITS : ", action)
    return {...action.payload}
  case REMOVE_FRUIT:
    console.log("in reducer REMOVE_FRUIT")
    return state
  default:
    return state
  }
}
