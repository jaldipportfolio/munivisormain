import axios from "axios"
import * as actions from "./actions"
import { startDeletingSearchPref, finsihDeletingSearchPref, startFetchingSearchPref, finsihFetchingSearchPref, startSavingSearchPref, finsihSavingSearchPref} from "../SearchPref"
import { muniApiBaseURL } from "../../../../globalutilities/consts"
import {getToken} from "GlobalUtils/helpers"

export const updateClientDashboardSearchPref = (filter, data) => (dispatch, getState) => {
  const { clientDashboardSearchPref, selectedClientDashboardPref } = getState()
  const currentData = clientDashboardSearchPref[selectedClientDashboardPref]
  const payload = { ...currentData, [filter]: data}

  dispatch({
    type: actions.UPDATE_CLIENT_DASHBOARD_SEARCH_PREF,
    id: selectedClientDashboardPref,
    payload
  })
}

export const setSelectedClientDashboardSearchPref = (data) => ({
  type: actions.UPDATE_SELECTED_CLIENT_DASHBOARD_SEARCH_PREF,
  data,
})

export const setClientDashboardPrefFilter = (pref) => ({
  type: actions.RENAME_SELECTED_CLIENT_DASHBOARD_PREF,
  pref
})

export const fetchClientDashboardSearchPrefs = () => (dispatch, getState) => {
  dispatch(startFetchingSearchPref())
  const { auth: { token }} = getState()
  axios({
    method: "GET",
    url: `${muniApiBaseURL}searchpref/context/dash-cltprosp`,
    headers: { Authorization: token },
  }).then(res => {
    dispatch(setClientDashboardPrefFilter(res.data.data.searchPreference))
    setTimeout(() => {
      dispatch(finsihFetchingSearchPref())
    },100)
  }).catch(() => {
    dispatch(finsihFetchingSearchPref())
  })
}

export const saveClientDashboardSearchFilter = (filterName) => (dispatch, getState) => {
  const {   clientDashboardSearchPref, selectedClientDashboardPref, auth: { token } } = getState()
  const pref = { ...clientDashboardSearchPref }
  const selectedPref = { ...pref[selectedClientDashboardPref] }
  delete pref[selectedClientDashboardPref]
  const key = filterName.replace(/[^a-zA-Z0-9]/g, "")
  pref[key] = selectedPref
  pref[key].filterName = filterName
  delete pref["0"]

  dispatch(startSavingSearchPref())
  return axios({
    method: "POST",
    url: `${muniApiBaseURL}searchpref/context/dash-cltprosp`,
    headers: { Authorization: token },
    data: pref
  }).then(() => {
    dispatch(setClientDashboardPrefFilter(pref))
    dispatch(setSelectedClientDashboardSearchPref(key))
    dispatch(finsihSavingSearchPref())
  }).catch(() => {
    dispatch(finsihSavingSearchPref())
  })
}

export const deleteClientDashboardSearchPref = () => (dispatch, getState) => {
  const {   clientDashboardSearchPref, selectedClientDashboardPref, auth: { token } } = getState()

  if (selectedClientDashboardPref !== "0") {
    const pref = { ...clientDashboardSearchPref }
    delete pref[selectedClientDashboardPref]
    delete pref["0"]

    dispatch(startDeletingSearchPref())
    return axios({
      method: "POST",
      url: `${muniApiBaseURL}searchpref/context/dash-cltprosp`,
      headers: { Authorization: token },
      data: pref
    }).then(() => {
      dispatch(setClientDashboardPrefFilter(pref))
      dispatch(setSelectedClientDashboardSearchPref("0"))
      dispatch(finsihDeletingSearchPref())
    }).catch(() => {
      dispatch(finsihDeletingSearchPref())
    })
  }

  return null
}

export const getcontracts = async (entityId) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}entity/contracts/${entityId}`, {headers:{"Authorization":getToken()}} )
    return response.data
  } catch (error) {
    console.log(error)
    return error
  }
}

export const getactivecontracts = async (entityId) => {
  try {
    const response = await axios.get(`${muniApiBaseURL}entity/activecontracts/${entityId}`, {headers:{"Authorization":getToken()}} )
    return response.data
  } catch (error) {
    console.log(error)
    return error
  }
}

export const putcontractstatus = async (entityId, query, body) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}entity/contracts/${entityId}/?status=${query}`, body, {headers:{"Authorization":getToken()}} )
    return response.data
  } catch (error) {
    console.log(error)
    return error
  }
}

export const putcontracts = async (entityId, body) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}entity/contracts/${entityId}`, body, {headers:{"Authorization":getToken()}} )
    return response.data
  } catch (error) {
    console.log(error)
    return error
  }
}

export const deleteContracts = async (entityId, contractId, callback) => {
  try {
    const response = await axios.delete(`${muniApiBaseURL}entity/contracts/${entityId}?contractId=${contractId}`,{headers:{"Authorization":getToken()}} )
    callback(response)
  } catch (error) {
    callback(response)
  }
}

export const pullContractDocuments = async (query) => {
  try {
    return await axios.delete(`${muniApiBaseURL}entity/contracts/document/${query}`,{headers:{"Authorization":getToken()}} )
  } catch (error) {
    console.log(error)
    return error
  }
}

export const putGenAuditLog = async(contractId, body, callback) => {
  try {
    const response = await axios.post(`${muniApiBaseURL}entity/auditlog/${contractId}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (error) {
    callback({error})
  }
}
