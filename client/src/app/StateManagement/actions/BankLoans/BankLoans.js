import axios from "axios"
import uuidv1 from "uuid/v1"
import { muniApiBaseURL } from "GlobalUtils/consts.js"
import { getToken } from "GlobalUtils/helpers"

export const fetchLoanTransaction = async (type, tranId, callback) => {
  try {
    const response = await axios.get(
      `${muniApiBaseURL}bankloans/transaction/${type}/${tranId}`,
      { headers: { Authorization: getToken() } }
    )
    callback(response.data)
  } catch (error) {
    callback({ error })
  }
}

export const putLoanStatusAndSec = async (tranId, body, callback) => {
  try {
    const response = await axios.put(
      `${muniApiBaseURL}bankloans/${tranId}`,
      body,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const fetchUserLoanTransactions = async (type, clientId, callback) => {
  try {
    let response = {}
    if (type === "debt") {
      response = await axios.get(
        `${muniApiBaseURL}bankloans/transactions/${clientId}`,
        { headers: { Authorization: getToken() } }
      )
    } else if (type === "derivative") {
      response = await axios.get(
        `${muniApiBaseURL}derivatives/transactions/${clientId}`,
        { headers: { Authorization: getToken() } }
      )
    } else if (type === "marfp") {
      response = await axios.get(
        `${muniApiBaseURL}marfp/transactions/${clientId}`,
        { headers: { Authorization: getToken() } }
      )
    }
    callback({
      transactions: response.data || []
    })
  } catch (error) {
    callback({ error })
  }
}

export const putLoanTransaction = async (type, tranId, body, callback) => {
  try {
    const response = await axios.put(
      `${muniApiBaseURL}bankloans/transaction/${type}/${tranId}`,
      body,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const postLoanNotes = async(tranId, body, callback) => {
  try {
    const response = await axios.post(`${muniApiBaseURL}bankloans/notes/${tranId}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (error) {
    callback({error})
  }
}

export const putCusIpsDetails = async (tranId, body, callback) => {
  try {
    const response = await axios.put(
      `${muniApiBaseURL}bankloans/cusIps/${tranId}`,
      body,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const pullLinkCucIps = async (tranId, cusIpId, callback) => {
  try {
    const response = await axios.delete(
      `${muniApiBaseURL}bankloans/cusIps/${tranId}?cusIpId=${cusIpId}`,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const createTrans = async (type, body, callback) => {
  try {
    body.opportunity = {
      status: body.opportunity || false,
      opportunityType: body.opportunity || false
    }
    if (type === "debt") {
      if (body.actTranSubType === "Bond Issue") {
        const generator = `TXDEAL${uuidv1()
          .replace(/-/g, "")
          .toUpperCase()}`
        body.actTranUniqIdentifier = generator.substring(0, 17)
        const participant = {
          dealPartType: body.participant.partType || "",
          dealPartFirmId: body.participant.partFirmId || "",
          dealPartFirmName: body.participant.partFirmName || "",
          dealPartContactId: body.participant.partContactId || "",
          dealPartContactName: body.participant.partContactName || "",
          dealPartContactEmail: body.participant.partContactEmail || "",
          dealPartContactPhone: body.participant.partContactPhone || "",
          dealPartContactFax: body.participant.partContactFax || "",
          createdDate: body.participant.createdDate || new Date(),
          dealPartContactAddToDL: false
        }
        if(body.participant && body.participant.partUserAddress){
          participant.dealPartUserAddress = body.participant.partUserAddress
          participant.dealPartContactAddrLine1 = body.participant.partContactAddrLine1
          participant.dealPartContactAddrLine2 = body.participant.partContactAddrLine2
          participant.dealParticipantState = body.participant.participantState
          participant.dealPartAddressGoogle = body.participant.dealPartAddressGoogle || ""
        }
        const payload = {
          createdByUser: body.createdByUser,
          dealIssueTranName: generator.substring(0, 22),
          dealIssueTranType: "Debt",
          dealIssueTranSubType: body.actTranSubType,
          dealIssueTranClientId: body.actTranFirmId, // this is the financial advisor who is the client of munivisor
          dealIssueTranClientFirmName: body.actTranFirmName,
          dealIssueTranPurposeOfRequest: body.rfpTranPurposeOfRequest,
          dealIssueTranAssignedTo: [body.actTranFirmLeadAdvisorId],
          dealIssueTranIssuerId: body.actTranClientId, // this is the issuer client
          dealIssueTranIssuerFirmName: body.actTranClientName,
          dealIssueTranIsConduitBorrowerFlag: body.actTranIsConduitBorrowerFlag,
          dealIssueTranIssueName: body.actTranIssueName,
          dealIssueTranRelatedTo: body.actTranRelatedTo,
          dealIssueTranState: body.actTranState || "",
          dealIssueTranCounty: body.actTranCounty || "",
          dealIssueTranPrimarySector: body.actTranPrimarySector,
          dealIssueTranSecondarySector: body.actTranSecondarySector,
          dealIssueTranProjectDescription: body.actTranProjectDescription,
          dealIssueTranStatus: body.actTranStatus,
          dealIssueParticipants: [participant],
          contracts: body.contracts,
          clientActive: body.clientActive,
          opportunity: body.opportunity || {}
        }
        if(body && body.tranNotes && Object.keys(body.tranNotes).length){
          payload.tranNotes = body.tranNotes || {}
        }
        const response = await axios.post(`${muniApiBaseURL}deals`, payload, {
          headers: { Authorization: getToken() }
        })
        callback(response)
      } else {
        body.actTranType = "Debt"
        body.bankLoanParticipants = [body.participant]
        body.bankLoanSummary = {
          actTranState: body.actTranState || "",
          actTranCounty: body.actTranCounty || "",
        }
        delete body.actTranCounty
        delete body.actTranState
        delete body.participant
        const response = await axios.post(`${muniApiBaseURL}bankloans`, body, {
          headers: { Authorization: getToken() }
        })
        callback(response)
      }
    } else if (type === "derivative") {
      body.actTranType = "Derivative"
      body.derivativeSummary = {tranCounterpartyClient: body.actTranClientName || ""}
      body.derivativeParticipants = [body.participant]
      delete body.participant
      delete body.actTranCounty
      delete body.actTranState
      const response = await axios.post(
        `${muniApiBaseURL}derivatives/create`,
        body,
        { headers: { Authorization: getToken() } }
      )
      callback(response)
    } else if (type === "marfp") {
      body.actType = "MA-RFP"
      body.actSubType = "RFP"
      body.maRfpParticipants = [body.participant]
      delete body.participant
      const response = await axios.post(`${muniApiBaseURL}marfps`, body, {
        headers: { Authorization: getToken() }
      })
      callback(response)
    }
    if (type === "rfp") {
      body.actTranType = "RFP"
      const participant = {
        rfpSelEvalContactId: body.participant.partContactId || "",
        rfpSelEvalRealFirstName: body.participant.partContactName || "",
        rfpSelEvalRealLastName: body.participant.partContactLastName || "",
        rfpSelEvalRealEmailId: body.participant.partContactEmail || "",
        rfpSelEvalRole: "Municipal Advisor",
        rfpSelEvalFirmId: body.participant.partFirmId,
        rfpSelEvalContactName: body.participant.partContactName || "",
        rfpSelEvalEmail: body.participant.partContactEmail || "",
        rfpSelEvalPhone: body.participant.partContactPhone || "",
        rfpSelEvalAddToDL: false
      }
      const payload = {
        createdByUser: body.createdByUser,
        rfpTranName: body.actTranUniqIdentifier,
        rfpTranType: body.actTranType,
        rfpTranSubType: body.actTranSubType,
        rfpTranOtherSubtype: body.actTranOtherSubtype,
        rfpTranClientId: body.actTranFirmId,
        rfpTranClientFirmName: body.actTranFirmName,
        rfpTranIssuerId: body.actTranClientId,
        rfpTranIssuerFirmName: body.actTranClientName,
        rfpTranIsConduitBorrowerFlag: body.actTranIsConduitBorrowerFlag,
        rfpTranIssueName: body.actTranIssueName,
        rfpTranRelatedTo: body.actTranRelatedTo,
        rfpTranPrimarySector: body.actTranPrimarySector,
        rfpTranSecondarySector: body.actTranSecondarySector,
        rfpTranProjectDescription: body.actTranProjectDescription,
        rfpTranAssignedTo: [body.actTranFirmLeadAdvisorId],
        rfpTranPurposeOfRequest:
          body.actTranSubType === "Client RFP for Underwriter"
            ? "Underwriting Services"
            : body.actTranSubType,
        rfpTranStatus: body.actTranStatus,
        clientActive: body.clientActive,
        rfpEvaluationTeam: [participant],
        contracts: body.contracts,
        opportunity: body.opportunity || {}
      }
      if(body && body.tranNotes && Object.keys(body.tranNotes).length){
        payload.tranNotes = body.tranNotes || {}
      }
      const response = await axios.post(`${muniApiBaseURL}rfp`, payload, {
        headers: { Authorization: getToken() }
      })
      callback(response)
    }
    if (type === "others") {
      body.participants = [body.participant]
      body.actTranType = "Others"
      delete body.participant
      delete body.actTranRelatedType
      const response = await axios.post(`${muniApiBaseURL}others`, body, {
        headers: { Authorization: getToken() }
      })
      callback(response)
    }
    if (type === "businessDevelopment") {
      body.actType = "BusinessDevelopment"
      body.participants = [body.participant]
      delete body.participant
      delete body.actTranCounty
      delete body.actTranState
      const response = await axios.post(`${muniApiBaseURL}busdev`, body, {
        headers: { Authorization: getToken() }
      })
      callback(response)
    }
  } catch (error) {
    callback({ error })
  }
}

export const putLoanParticipantsDetails = async (tranId, body, callback) => {
  try {
    const response = await axios.put(
      `${muniApiBaseURL}bankloans/participants/${tranId}`,
      body,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const pullLoanParticipantsDetails = async (tranId, id, callback) => {
  try {
    const response = await axios.delete(
      `${muniApiBaseURL}bankloans/participants/${tranId}?id=${id}`,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const putLoansTransaction = async (type, body, callback) => {
  try {
    const response = await axios.put(
      `${muniApiBaseURL}bankloans/transaction/${type}/${body._id}`,
      body,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback(error)
  }
}

export const pullAmortizationDetails = async (tranId, id, callback) => {
  try {
    const response = await axios.delete(
      `${muniApiBaseURL}bankloans/amortization/${tranId}?id=${id}`,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback({ error })
  }
}

export const putAmortizationDetails = async (tranId, body, callback) => {
  try {
    const response = await axios.put(
      `${muniApiBaseURL}bankloans/amortization/${tranId}`,
      body,
      { headers: { Authorization: getToken() } }
    )
    callback(response)
  } catch (error) {
    callback(error)
  }
}

export const fetchBorrowerObligorByFirm = async (firmId, callback) => {
  try {
    const response = await axios.get(
      `${muniApiBaseURL}entity/borrowerObligor/${firmId}`,
      { headers: { Authorization: getToken() } }
    )
    let borrower = []
    if (response.data && Array.isArray(response.data.entityBorObl)) {
      borrower = response.data.entityBorObl.map(ent => {
        const newObject = {
          ...ent,
          name: ent.borOblFirmName,
          id: ent._id, // eslint-disable-line
          type:
            ent.borOblRel === "conduit"
              ? "Obligor"
              : ent.borOblRel === "borrower" && "Borrower"
        }
        return newObject
      })
    }
    callback(borrower)
  } catch (error) {
    callback({ error })
  }
}
