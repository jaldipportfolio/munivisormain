import axios from "axios"
import {getToken} from "GlobalUtils/helpers"
import { muniApiBaseURL } from "GlobalUtils/consts.js"

export const oAuthSignIn = async () => {
  try {
    const response = await axios.get(`${muniApiBaseURL}msauth/authurl`, {headers: { Authorization: getToken() }})
    console.log("response oAuth token....", response)
    return response.data || {}
  } catch (error) {
    console.log(error)
    return error.response.data
  }
}

export const getOutLookContacts = async (code, msCredential) => {
  try {
    if(!code && !msCredential) return null
    const response = await axios.post(`${muniApiBaseURL}msauth/contacts`, {code, msCredential: JSON.parse(msCredential)}, {headers: { Authorization: getToken() }})
    return response.data || {}
  } catch (error) {
    console.log(error)
    return error.response.data
  }
}

export const getOutLookContactsFromFolder = async (folderId, code, msCredential) => {
  try {
    const response = await axios.post(`${muniApiBaseURL}msauth/dircontacts`, {folderId, code, msCredential}, {headers: { Authorization: getToken() }})
    return response.data || {}
  } catch (error) {
    console.log(error)
    return error.response.data
  }
}

export const importContacts = async (importData, type) => {
  try {
    const response = await axios.post(`${muniApiBaseURL}syncdata/import?type=${type}`, {importData}, {headers: { Authorization: getToken() }})
    return response.data || {}
  } catch (error) {
    console.log(error)
    return error.response.data
  }
}

export const getImportedContacts = async filteredVal => {
  try {
    const filteredValue = filteredVal === undefined ? "" : filteredVal
    return await axios.post(`${muniApiBaseURL}syncdata/importedlist`, filteredValue, {headers: { Authorization: getToken() }})
  } catch (error) {
    console.log(error)
    return error.response.data
  }
}

export const addToEntityContact = async (selectedUsers, addUserInEnt, resolvedObj) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}syncdata/import`, {selectedUsers, addUserInEnt, resolvedObj}, {headers: { Authorization: getToken() }})
    return response.data || {}
  } catch (error) {
    console.log(error)
    return error.response.data
  }
}
