import axios from "axios"
import { muniApiBaseURL } from "GlobalUtils/consts.js"
import {
  GET_MESSAGES
} from "../types"

export const getMessages = token => async dispatch => {
  token = token || localStorage.getItem("token")
  const res = await axios.get(`${muniApiBaseURL}messages`, {headers:{"Authorization":token}})
  if(res && res.data) {
    dispatch ({
      type: GET_MESSAGES,
      payload: res.data
    })
  } else {
    console.log("no messages")
    dispatch ({
      type: GET_MESSAGES,
      payload: []
    })
  }
}
