/* List of actions for all projects */
export const TM_PROJECTS_GET_ALL_LIST = "TM_PROJECTS_GET_ALL_LIST"
export const TM_PROJECTS_GET_BASIC_DETAILS = "TM_PROJECTS_GET_BASIC_DETAILS"
export const TM_PROJECT_GET_ALL_BOARDS = "TM_PRORJECT_GET_ALL_BOARDS"

export const taskGetProjectList= () => ({ type:TM_PROJECTS_GET_ALL_LIST })

export const taskGetProjectBasicDetails= (projectId) => ({ type:TM_PROJECTS_GET_BASIC_DETAILS, projectId })

export const taskGetProjectBoards= (projectId) => ({ type:TM_PROJECT_GET_ALL_BOARDS, projectId })

/* These actions can be used to add modify and delete items */
export const TM_PROJECT_ADD = "TM_PROJECT_ADD"
export const TM_PROJECT_MODIFY = "TM_PROJECT_MODIFY"
export const TM_PROJECT_DELETE = "TM_PROJECT_DELETE"
export const TM_PROJECT_GET_ACTIVITY_DETAILS = "TM_PROJECT_GET_ACTIVITY_DETAILS"

/* Track all the activity holistically */
export const TM_PROJECT_ADD_ACTIVITY = "TM_PROJECT_ADD_ACTIVITY"
export const TM_PROJECT_GET_ACTIVITY = "TM_PROJECT_GET_ACTIVITY"

export const TM_PROJECT_BOARD_ADD_ACTIVITY = "TM_PROJECT_BOARD_ADD_ACTIVITY"
export const TM_PROJECT_BOARD_GET_ACTIVITY = "TM_PROJECT_BOARD_GET_ACTIVITY"