import axios from "axios"
import { muniApiBaseURL } from "GlobalUtils/consts.js"
import * as types from "./actionsType"

export const getAllThirdPartyFirmList = () => (dispatch) => {
  axios.get(`${muniApiBaseURL}third-parties`).then((result) => {
    console.log("Firm List==>>", result.data)
    dispatch({ type: types.ALL_FIRM_LIST, payload: result.data })
  })
}
