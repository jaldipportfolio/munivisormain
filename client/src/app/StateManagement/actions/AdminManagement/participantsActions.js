import axios from "axios"
import { muniApiBaseURL } from "GlobalUtils/consts.js"
import * as types from "./actionsParticipants"

export const addDealParticipants = (participants) => {
  console.log("test action", participants)
  return function (dispatch) {
    axios.put(`${muniApiBaseURL}participantsteam`,
      {details:participants.details}
    ).then((result) => {
      console.log("Result===>>>")
      dispatch({ type: types.Part_UT_FRM, payload: result.data})
    })
  }
}
