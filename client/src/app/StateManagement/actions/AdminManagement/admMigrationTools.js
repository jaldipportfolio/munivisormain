import * as types from "./actionsType"

export const addGridData = gridData => (dispatch) => {
  dispatch({ type: types.ADD_GRID_DATA, payload: gridData })
}

export const setStepIndex = stepIndex => (dispatch) => {
  dispatch({ type: types.SET_STEP_INDEX, payload: stepIndex })
}

export const setFile = file => (dispatch) => {
  dispatch({ type: types.SET_FILE, payload: file })
}

export const setTypeOfFileUpload = type => (dispatch) => {
  dispatch({ type: types.SET_TYPE_OF_FILE, payload: type })
}

export const setInterimStorageKey = interimStorageKey => (dispatch) => {
  dispatch({ type: types.SET_INTERIM_STORAGE_KEY, payload: interimStorageKey })
}

export const setValidatedData = validedData => (dispatch) => {
  dispatch({ type: types.SET_VALIDED_DATA, payload: validedData })
}
