import axios from "axios"
import { muniApiBaseURL } from "GlobalUtils/consts.js"
import {getToken} from "GlobalUtils/helpers"

export const putOtherParticipantsDetails = async(dealId, body, callback) => {
  try {
    const response = await axios.put(`${muniApiBaseURL}others/participants/${dealId}`, body, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (error) {
    callback({error})
  }
}

export const pullOtherParticipantsDetails = async(dealId, id, callback) => {
  try {
    const response = await axios.delete(`${muniApiBaseURL}others/participants/${dealId}?id=${id}`, {headers:{"Authorization":getToken()}})
    callback(response)
  } catch (error) {
    callback({error})
  }
}
