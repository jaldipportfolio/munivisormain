import React, { Component } from "react"
import { connect } from "react-redux"

import {
  startSignUpProcess
} from "AppState/actions"

class SignUp extends Component {
  constructor(props) {
    super(props)
    this.state = { email:"", password:"", passwordConfirm:"" }
    this.handleLogin = this.handleLogin.bind(this)
    this.changeEmail = this.changeEmail.bind(this)
    this.changePassword = this.changePassword.bind(this)
    this.changePasswordConfirm = this.changePasswordConfirm.bind(this)
  }

  // Need to validate the email ID
  // Need to ensure the passowds match
  // Add some Minimum requirements for passsword in terms of rules

  handleLogin(event){
    event.preventDefault()
    const {email, password} = this.state
    console.log("the component state is", this.state)
    this.props.startSignUpProcess({email, password})
  }

  changeEmail(e) {
    const emailVal = e.target.value
    this.setState((prevState) => ({...prevState,...{email:emailVal} }))
  }

  changePassword(e) {
    const chgPassword = e.target.value
    this.setState((prevState) => ({...prevState,...{password:chgPassword} }))
  }

  changePasswordConfirm(e) {
    const passValidate = e.target.value
    this.setState((prevState) => ({...prevState,...{passwordConfirm:passValidate} }))
  }

  render() {
    
    const { authenticated, error } = this.props.auth
    return (
      <section className="hero is-info is-fullheight">
        <div className="hero-body">
          <div className="container has-text-centered">
            <div className="column is-6 is-offset-3">
              <h3 className="title has-text-white">MuniVisorSign up Form</h3>
              <p className="subtitle has-text-white">Please Signup</p>
              <div className="box">
                <form onSubmit={this.handleLogin}>
                  <div className="field">
                    <div className="control">
                      <input className="input is-large" type="email" value={this.state.email}  placeholder="Enter Email" onChange={this.changeEmail}/>
                    </div>
                  </div>
                  <div className="field">
                    <div className="control">
                      <input className="input is-large" type="password" value={this.state.password} placeholder="password" onChange={this.changePassword}/>
                    </div>
                  </div>
                  <div className="field">
                    <div className="control">
                      <input className="input is-large" type="password" value={this.state.passwordConfirm} placeholder="Re-enter password" onChange={this.changePasswordConfirm}/>
                    </div>
                  </div>                  
                  <button className="button is-block is-info is-large is-fullwidth">Login</button>
                  {authenticated ? <span> The User has successfully signed up on the application </span> : null }
                  {error ? <span style={{color:"red"}}> {`Error: ${error}`} </span> : null }

                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
    )
  }
}

// Connect the component to the redux store and also wire up the onsave action creator

export default connect((({auth}) => ({auth})),{ startSignUpProcess })(SignUp)
