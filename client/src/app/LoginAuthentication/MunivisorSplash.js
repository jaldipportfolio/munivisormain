import React, { Component } from "react"
import {  Link } from "react-router-dom"


class MunivisorSplash extends Component {
  constructor(props) {
    super(props)
    this.state = {  menuExpanded: false }
    this.expandMenu = this.expandMenu.bind(this)
  }

  componentDidMount() {
    this.unmount = false
    console.log("splash in mount")
    const token = localStorage.getItem("token")
    if(token) {
      this.props.history.push("/signin")
    }
  }

  componentWillUnmount() {
    this.unmount = true
  }


  expandMenu() {
    if (!this.unmount) {
      this.setState(prevState => ({ menuExpanded: !prevState.menuExpanded }))
    }
  }

  render() {
    const { menuExpanded} = this.state
    return (
      <div id="appnew">

        <nav id="navbar" className="navbar is-fixed-top">
          <div id="specialShadow" className="bd-special-shadow" />
          <div className="container">
            <div className="navbar-brand">
              {/*<a className="navbar-item" href="#">*/}
                {/*<img src="images/munivisor.png" alt="MuniVisor"/>*/}
              {/*</a>*/}

              <div id="navbarBurger" 
                className={
                  menuExpanded
                    ? "navbar-burger burger is-active"
                    : "navbar-burger burger"
                }
                onClick={this.expandMenu}
                data-target="navMenuDocumentation">
                <span />
                <span />
                <span />
              </div>

            </div>
            <div
              id="navMenuDocumentation"
              className={menuExpanded ? "navbar-menu is-active" : "navbar-menu"}>
            <div className="navbar-end" style={{display: this.state.menuExpanded ? 'block' : 'none' }}>
                  <Link to="/signin" className="navbar-item">
                    <span className="icon innerPgTitle">
                      <i className="fas fa-sign-in-alt" />Login
                    </span>
                  </Link>
                </div>
            </div>

            <div id="navMenuDocumentation" className="navbar-menu">

              <div className="navbar-start" />

              <div className="navbar-end">
                <Link to="/signin" className="navbar-item">
                  <span className="icon">
                    <i className="fas fa-sign-in-alt" />
                  </span>
                  <p className="title innerPgTitle">Login</p>
                </Link>
              </div>
            </div>

          </div>
        </nav>

        <hr />

        <div id="main">
          <section className="container">
            <div className="tile is-ancestor">
              <div className="tile is-parent">
                <article className="tile is-child box has-text-centered">
                  <p className="title">Let's get started</p>
                  <p className="subtitle">What kind of market participant are you?</p>
                  <div className="content" />
                </article>
              </div>
            </div>

            <div className="tile is-ancestor">

              <div className="tile is-parent">
                <article className="tile is-child box has-text-centered">
                  <Link to="/signup?type=fa">
                    <i className="far fa-7x fa-handshake" />
                    <hr/>
                    <p className="title signUpConTitle">Financial Advisor</p>
                    <br/>
                    <p className="subtitle">You work on municipal issuances as a financial or municipal advisor.</p>
                  </Link>
                </article>
              </div>

              <div className="tile is-parent">
                <article className="tile is-child box has-text-centered">
                  <Link to="/signup?type=comp">
                    <i className="far fa-7x fa-calendar-check" />
                    <hr/>
                    <p className="title signUpConTitle">Compliance Advisor</p>
                    <br/>
                    <p className="subtitle">You are a municipal market regulatory compliance advisor.</p>
                  </Link>
                </article>
              </div>

              <div className="tile is-parent">
                <article className="tile is-child box has-text-centered">
                  <Link to="/signup?type=oth">
                    <i className="far fa-7x fa-address-card" />
                    <hr/>
                    <p className="title signUpConTitle">Other</p>
                    <br/>
                    <p className="subtitle">You work on municipal issuances as a banker, counsel, or other.</p>
                  </Link>
                </article>
              </div>

              <div className="tile is-parent">
                <article className="tile is-child box has-text-centered">
                  <Link to="/signup?type=iss">
                    <i className="fas fa-7x fa-university" />
                    <hr/>
                    <p className="title signUpConTitle">Issuer</p>
                    <br/>
                    <p className="subtitle">You represent an Issuer or Borrower.</p>
                  </Link>
                </article>
              </div>
            </div>
          </section>
        </div>

      </div>
    )
  }
}

export default MunivisorSplash
