import React from "react"
import { Modal } from "../FunctionalComponents/TaskManagement/components/Modal"

export const SelectFirmModal = props => {

  const { modalState, selectedFirm, firms, modalMessage, buttonName } = props.modalData

  const toggleModal = () => {
    props.onModalChange({
      modalData: {
        ...props.modalData,
        modalState: !modalState,
        selectedFirm: ""
      }
    })
  }

  const onFirmChange = (value) => {
    props.onModalChange({
      modalData: {
        ...props.modalData,
        selectedFirm: value.entityId || ""
      }
    })
  }

  return (
    <Modal
      closeModal={toggleModal}
      modalState={modalState}
      title="Select Firm"
      saveModal={props.onSave}
      buttonName={buttonName || "Save"}
      disabled={!selectedFirm}
    >

      <div>
        <p style={{fontSize: 18}}>{modalMessage}</p>
        <br/>

        {firms.map((firm, index) =>
          <div key={index.toString()}>
            <label className="radio-button" style={{fontSize: 18}}>
              {firm.userFirmName || ""}
              <input
                type="radio"
                className="checkmark"
                checked={selectedFirm === firm.entityId}
                onChange={() => onFirmChange(firm)}
              />
              <span className="checkmark" />
            </label>
          </div>
        )}
      </div>

    </Modal>
  )
}

export default SelectFirmModal
