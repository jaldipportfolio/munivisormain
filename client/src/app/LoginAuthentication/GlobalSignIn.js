import React, { Component } from "react"
import { connect } from "react-redux"
import { Link } from "react-router-dom"
import { validateEmail } from "GlobalUtils/functionalutilities"
import { getUsersByEmailId } from "GlobalUtils/helpers"

import { startSignInProcess, checkAuth, signOut } from "AppState/actions"
import Loader from "../GlobalComponents/Loader"
import SelectFirmModal from "./SelectFirmModal"


class GlobalSignIn extends Component {
  constructor(props) {
    super(props)
    this.state = {
      formValues: { email: "", password: "" },
      modalData: {
        selectedFirm: "",
        buttonName: "",
        modalMessage: "",
        firms: [],
        modalState: false,
      },
      waiting: false,
      errors: {},
      blur: {},
      pageLoading: true
    }
    this.passwordInput = React.createRef()
    this.emailInput = React.createRef()
    this.loginButtonRef = React.createRef()
  }

  componentDidMount() {
    const { formValues } = this.state
    const keys = Object.keys(formValues)
    const blurInitiate = keys.reduce(
      (obj, key) => ({ ...obj, ...{ [key]: false } }),
      {}
    )
    const errorInitiate = keys.reduce(
      (obj, key) => ({ ...obj, ...{ [key]: "" } }),
      {}
    )

    this.setState(
      {
        waiting: true,
        errors: errorInitiate,
        blur: blurInitiate
      },
      this.checkExistingAuth
    )
  }

  onBlur = e => {
    const fieldName = e.target.name
    const blurValues = { ...this.state.blur }
    const errors = { ...this.state.errors }
    const newBlurState = { ...blurValues, ...{ [e.target.name]: true } }
    const numFieldsBlur = Object.values(newBlurState).reduce(
      (numBlur, fld) => (fld ? numBlur + 1 : numBlur),
      0
    )
    errors.invalidLogin = ""
    if (fieldName === "email") {
      this.setState({ waiting: false })
      const [validEmail] = validateEmail(this.state.formValues.email)
      if (!validEmail) {
        errors.email = "Enter a valid Email"
      } else {
        errors.email = ""
      }
      this.setState(
        {
          errors: { ...errors }
        },
        () => {
          this.setNextFocus(fieldName)
        }
      )
    } else if (fieldName === "password") {
      if (e.target.value === "") {
        errors.password = "Please enter a password"
        this.setState({
          waiting: false,
          errors: { ...errors }
        })
      } else {
        errors.password = ""
        this.setState(
          {
            waiting: false,
            errors: { ...errors }
          },
          () => this.loginButtonRef.current.focus()
        )
      }
    }
  }

  setNextFocus = fieldName => {
    if (this.state.errors.email === "" && fieldName === "email") {
      this.passwordInput.current.focus()
    } else {
      this.emailInput.current.focus()
      return
    }

    if (this.state.errors.password === "" && fieldName === "password") {
      this.loginButtonRef.current.focus()
    } else {
      this.passwordInput.current.focus()
    }
  }

  checkExistingAuth = async () => {
    const token = localStorage.getItem("token")
    if (token) {
      await this.props.checkAuth(token)
    } else {
      this.setState({ waiting: false, pageLoading: false })
    }
  }

  handleChange = e => {
    const formValues = { ...this.state.formValues }
    const errors = { ...this.state.errors }
    errors.invalidLogin = ""
    formValues[e.target.name] = e.target.value
    this.setState({
      formValues: { ...formValues },
      errors: { ...errors }
    })
  }

  onModalChange = (state) => {
    this.setState({
      ...state
    })
  }

  handleSignIn = async event => {
    const { formValues, errors } = this.state
    this.setState({
      waiting:true
    })

    event.preventDefault()

    if(errors && (!errors.email && !errors.password)){

      const firmsList = await getUsersByEmailId({email: formValues.email})

      if(firmsList && firmsList.users && firmsList.users.length > 1){
        return  this.setState({
          waiting:false,
          modalData: {
            ...this.state.modalData,
            modalMessage: <p><b>"{formValues.email}"</b>  This Email Id Found In Multiple Firm Please Select Your Firm And Continue To Sign In!</p>,
            buttonName: "Continue To Sign In",
            modalState: true,
            firms: firmsList.users,
            email: formValues.email
          }
        })
      }

    }

    const errorMessage = await this.props.startSignInProcess({
      email: formValues.email,
      password: formValues.password
    })
    this.setState({
      waiting:false
    })
    errors.invalidLogin = ""
    if (errorMessage === "") {
      this.props.history.push("/dashboard")
    } else {
      errors.invalidLogin = errorMessage
      this.setState(prevState => ({
        ...prevState,
        errors
      }))
    }
  }

  onModalLogin = async () => {
    const { modalData, formValues, errors } = this.state
    const { selectedFirm } = modalData
    console.log("==================>", {selectedFirm})
    this.setState({
      waiting:true
    })

    const errorMessage = await this.props.startSignInProcess({
      email: `${formValues.email} ${selectedFirm || ""}`,
      password: formValues.password
    })
    this.setState({
      waiting:false,
      modalData: {
        selectedFirm: "",
        firms: [],
        modalState: false,
      },
    })
    errors.invalidLogin = ""
    if (errorMessage === "") {
      this.props.history.push("/dashboard")
    } else {
      errors.invalidLogin = errorMessage
      this.setState(prevState => ({
        ...prevState,
        errors,
        modalData: {
          selectedFirm: "",
          firms: [],
          modalState: false,
        },
      }))
    }
  }

  render() {
    const { formValues, numFields, numFieldsBlur, errors, modalData } = this.state
    const hasErrors = Object.values(errors).some(val => val !== "")
    const formPristine = numFieldsBlur !== numFields
    const disabled = hasErrors || formPristine
    let messageToDisplay
    if (errors.password ) {
      if(errors.password.length > 0) {
        messageToDisplay =errors.password
      }
    } else if ( errors.invalidLogin) {
      if(errors.invalidLogin.length > 0) {
        messageToDisplay =errors.invalidLogin
      }
    }

    if (this.state.pageLoading) return <Loader />
    return (
      <div id="main">
        <SelectFirmModal modalData={modalData} onModalChange={this.onModalChange} onSave={this.onModalLogin}/>
        <section className="container">
          {this.state.waiting ? <Loader /> : null}
          <form onSubmit={this.handleSignIn}>
            <div className="tile is-ancestor">
              <div className="tile is-parent">
                <article className="tile is-child" />

                <article className="tile is-child box is-5">
                  <div className="columns">
                    <div className="column">&nbsp;</div>
                    <div className="column is-8">
                      <img src="/images/munivisor.png" />
                    </div>
                    <div className="column">&nbsp;</div>
                  </div>
                  <hr />
                  <br />
                  <p className="subtitle">Sign in to your account here</p>
                  <div className="field">
                    <div className="control has-icons-left">
                      <input
                        ref={this.emailInput}
                        className={errors.email ? "input is-danger" : "input"}
                        type="email"
                        name="email"
                        placeholder="Email"
                        onBlur={this.onBlur}
                        value={formValues.email}
                        onChange={this.handleChange}
                      />
                      <span className="icon is-small is-left">
                        <i className="fas fa-envelope" />
                      </span>
                    </div>
                    {errors.email ? (
                      <p className="help is-danger">{errors.email}</p>
                    ) : null}
                  </div>

                  <div className="field">
                    <div className="control has-icons-left">
                      <input
                        ref={this.passwordInput}
                        className={
                          errors.password ? "input is-danger" : "input"
                        }
                        type="password"
                        name="password"
                        placeholder="Password"
                        value={formValues.password}
                        onBlur={this.onBlur}
                        onChange={this.handleChange}
                      />
                      <span className="icon is-small is-left">
                        <i className="fas fa-lock" />
                      </span>
                    </div>
                    {messageToDisplay ? (
                      <p className="help is-danger">{messageToDisplay}</p>
                    ) : null}
                  </div>
                  <div style={{ textAlign: "-webkit-center" }} className="is-fullwidth">
                    <button
                      ref={this.loginButtonRef}
                      disabled={formValues && !(formValues.password && formValues.email) || false}
                      className="button is-block is-info is-medium" style={{ width: "50%" }}
                    >
                      Sign In
                    </button>
                  </div>
                  <br/>
                  <div className="columns">
                    <div className="column is-narrow">
                      <div className="control">
                        <Link to="/forgotpass">
                          <p className="is-link is-size-7">Forgot Password?</p>
                        </Link>
                      </div>
                    </div>
                    <div className="column is-narrow">
                      <div className="control">
                        <Link to="/splash">
                          <p className="is-link is-size-7">Create new account</p>
                        </Link>
                      </div>
                    </div>
                  </div>
                </article>
                <article className="tile is-child" />
              </div>
            </div>
          </form>
        </section >
      </div >
    )
  }
}

export default connect(
  ({ auth }) => ({ auth }),
  { startSignInProcess, checkAuth, signOut }
)(GlobalSignIn)
