import React from "react"
import { DropdownList } from "react-widgets"
import "react-widgets/dist/css/react-widgets.css"

const DropDownListClear = (props) => (
  <div className="custom-dropdown-list">
    <DropdownList
      {...props}
      filter="contains"
    />
    {
      props.isHideButton ?
        <i className="fa fa-times fa-1 custom-dropdown-close" title="Clear" onClick={props.onClear}/> : null
    }
  </div>
)

export default DropDownListClear
