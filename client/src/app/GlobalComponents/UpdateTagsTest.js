import React from "react"

import { updateS3DocTags } from "GlobalUtils/helpers"

const UpdateTagsTest = props => {

  const updateTags = async (docId, tags) => {
    await updateS3DocTags(docId, tags)
    console.log("updated tags")
  }

  const { docId, tags } = props
  return (
    <button onClick={() => updateTags(docId, tags)}>Update Tags</button>
  )
}

export default UpdateTagsTest
