import React from "react"
import { getTranEntityUrl, getTranUserUrl } from "GlobalUtils/helpers"
import PhoneInput, { formatPhoneNumber } from "react-phone-number-input"
import DropDownListClear from "./DropDownListClear"
import ParticipantsSearchAddress from "./ParticipantsSearchAddress"


const Participants = ({ participant = {}, index, partType, isSaveDisabled, firms, userList, userEmails, userAddresses, isEditable, canEditTran, onItemChange,
  onSave, onCancel, onEdit, onRemove, errors = {}, onBlur, category, tableTitle, onParticipantsRefresh, onCheckAddTODL, searchText, tempNewAddress, tabIndex }) => {

  const onChange = (event, name) => {
    if (name && name === "partType") {
      // onBlur(category, `${name || "empty"} change to ${event || "empty"}`)
      return onItemChange({
        ...participant,
        [name]: event && (event.id === "Municipal Advisor" || event.id === "Issuer") ? event.id : event,
        partFirmId: event.id === "Municipal Advisor" || event.id === "Issuer" ? event.firmId : "",
        partFirmName: event.id === "Municipal Advisor" || event.id === "Issuer" ? event.firmName : "",
        partContactId: "",
        partContactName: "",
        partContactEmail: "",
        partContactPhone: "",
        partUserAddress: "",
        partContactAddrLine1: "",
        partContactAddrLine2: "",
        participantState: "",
        partUserActive: ""
      }, category, index)
    }
    onItemChange({
      ...participant,
      [event.target.name]: event.target.type === "checkbox" ? event.target.checked : event.target.value,
    }, category, index)
  }

  const onPartChange = (firm) => {
    onItemChange({
      ...participant,
      partFirmId: firm.id,
      partFirmName: firm.name,
      partContactId: "",
      partContactName: "",
      partContactEmail: "",
      partContactPhone: "",
      partUserAddress: "",
      partContactAddrLine1: "",
      partContactAddrLine2: "",
      participantState: "",
      partUserActive: ""
    }, category, index)
  }

  const onUserChange = (user) => {
    let email = ""
    let phone = ""
    let fax = ""
    if (user && Array.isArray(user.userEmails) && user.userEmails.length) {
      email = user.userEmails.filter(e => e.emailPrimary)
      email = email.length ? email[0].emailId : user.userEmails[0].emailId
    }
    if (user && Array.isArray(user.userPhone) && user.userPhone.length) {
      phone = user.userPhone.find(e => e.phonePrimary)
      phone = phone ? phone.phoneNumber : user.userPhone[0].phoneNumber
    }
    if (user && Array.isArray(user.userFax) && user.userFax.length) {
      fax = user.userFax.find(e => e.faxPrimary)
      fax = fax ? fax.faxNumber : user.userFax[0].faxNumber
    }
    onItemChange({
      ...participant,
      partContactId: Object.keys(user).length === 0 ? "" : user.id,
      partContactName: Object.keys(user).length === 0 ? "" : `${user.userFirstName} ${user.userLastName}`,
      partContactEmail: email,
      partContactPhone: phone,
      partContactFax: fax,
      partUserAddress: "",
      partContactAddrLine1: "",
      partContactAddrLine2: "",
      participantState: "",
      partUserActive: user && user.userStatus === "inactive" ? "inactive" : "active"
    }, category, index)
  }

  const onAddressChange = (address) => {
    onItemChange({
      ...participant,
      partUserAddress: address.id,
      partContactAddrLine1: (!participant.isNew && address.name === "Enter new Address") ? tempNewAddress :
        address.addressLine1 ? `${address.addressLine1} ${address.country ? "," : ""} ${address.city ? `${address.city},` : ""} ${address.state ? `${address.state},` : ""} ${(address.zipCode && address.zipCode.zip1) ? `${address.zipCode.zip1},` : ""} ${address.country}` : "",
      partContactAddrLine2: address.addressLine2 || "",
      participantState: address.state || "",
      city: address.city || "",
      country: address.country || "",
      addressName: address.addressName || address.name,
      zipCode: address.zipCode ? (address.zipCode && address.zipCode.zip1) || "" : address.zipcode || "",
      addressLine1: address.addressLine1 || "",
      participantGoogleAddress: address.addressLine1 ? `${address.addressLine1 || ""} ${address.city || ""} ${address.state || ""} ${address.zipCode ? `${address.zipCode.zip1}` : ""}` : ""
    }, category, index)
  }

  const getAddressDetails = (address) => {
    ((address && Object.keys(address).length === 0) || typeof address === "undefined") ?
      onItemChange({
        ...participant,
        partContactAddrLine1: "",
        partContactAddrLine2: "",
        participantState: "",
        participantGoogleAddress: "",
        city: "",
        country: "",
        zipCode: "",
        addressLine1: ""
      }, category, index) :
      onItemChange({
        ...participant,
        partContactAddrLine1: `${address.formatted_address}` || "",
        partContactAddrLine2: address.addressLine2 || "",
        participantState: address.state || participant.participantState || "",
        city: address.city || participant.city || "",
        country: address.country || participant.country || "",
        zipCode: address.zipcode || participant.zipCode || "",
        addressLine1: address.addressLine1 || participant.addressLine1 || "",
        participantGoogleAddress: address.addressLine1 ? `${address.addressLine1 || ""} ${address.city || ""} ${address.state || ""} ${address.zipcode || ""}` : ""
      }, category, index)
  }

  const onBlurInput = (event, name, title) => {
    if (!name && event && event.target && event.target.title && event.target.value) {
      onBlur(category, `${event.target.title || "empty"} change to ${event.target.type === "checkbox" ? event.target.checked : event.target.value || "empty"}`)
    }
    if (name && title) {
      onBlur(category, `user ${title} change to ${participant[name]}`)
    }
  }

  const highlightSearch = (string, searchQuery) => {
    const reg = new RegExp(
      searchQuery.replace(/[|\\{}()[\]^$+*?.]/g, "\\$&"),
      "i"
    )
    return string.replace(reg, str => `<mark>${str}</mark>`)
  }

  isEditable = (isEditable === index)
  const userAddress = participant.partUserAddress ? userAddresses.find(addr => addr.id === participant.partUserAddress) : ""

  const required = <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon"/></span>

  const ListItem = ({ item }) => (
    <span style = {{color: item.userStatus === "inactive" ? "red" : ""}}>
      <strong>{item.name}</strong>
    </span>
  )

  const onSaveBtnClick = (e, category, participant, index) =>{
    if(e.keyCode === 13){
      onSave(category, participant, index)
    }
  }

  const onCancelBtnClick = (e, category) =>{
    if(e.keyCode === 13){
      onCancel(category)
    }
  }

  const valuePhone = participant && participant.partContactPhone && participant.partContactPhone.includes("+") || false
  const participantPhone = participant.partContactPhone && formatPhoneNumber(valuePhone ? participant.partContactPhone : `+1${participant.partContactPhone}`, "International") || "International"

  const valueFax = participant && participant.partContactFax && participant.partContactFax.includes("+") || false
  const participantFax = participant.partContactFax && formatPhoneNumber(valueFax ? participant.partContactFax : `+1${participant.partContactFax}`, "International") || "International"

  return (
    <div>
      <div className="columns">
        <div className="column is-11">
          <div className="columns">
            <div className="column is-2">
              <div className="control">
                <div className="is-small">
                  <p className="multiExpLbl">Participant Type {isEditable && required}</p>
                  {isEditable ?
                    <DropDownListClear
                      filter
                      autoFocus={!participant.partType}
                      tabIndex={tabIndex}
                      data={partType}
                      value={participant.partType}
                      textField="name"
                      valueField="id"
                      disabled={!isEditable || !canEditTran}
                      isHideButton={participant.partType && isEditable}
                      onChange={(e) => onChange(e, "partType")}
                      onBlur={(e) => onBlurInput(e, "partType", "Participant Type")}
                      onClear={() => {onChange("", "partType")}}
                    />
                    : <small dangerouslySetInnerHTML={{__html: highlightSearch(participant.partType || "-",searchText)}}/>
                  }
                  {errors.partType && <p className="text-error">{errors.partType}</p>}
                </div>
              </div>
            </div>
            <div className="column">
              <div className="control">
                <div className="is-small">
                  <p className="multiExpLbl">Firm Name {isEditable && required}</p>
                  {isEditable ?
                    <DropDownListClear
                      filter
                      tabIndex={tabIndex + 1}
                      data={firms}
                      value={participant.partFirmName}
                      textField="name"
                      valueField="id"
                      disabled={!isEditable || !canEditTran}
                      isHideButton={participant.partFirmName && isEditable}
                      onChange={onPartChange}
                      onBlur={(e) => onBlurInput(e, "partFirmName", "Firm Name")}
                      onClear={() => {onPartChange({})}}
                    />
                    : <div>{getTranEntityUrl(participant.partType, participant.partFirmName, participant.partFirmId, searchText)}</div>
                  }
                  {errors.partFirmName && <p className="text-error">{errors.partFirmName}</p>}
                  {participant.partType && !participant.partFirmName && isEditable ?
                    <div>
                      {/* <a className="has-text-link" style={{ fontSize: 12, padding: "0 0 8px 11px", marginTop: -10 }} target="_blank" href="/addnew-client" >Add New Client?</a> */}
                      <a className="has-text-link" style={{ fontSize: 12, padding: "0 0 8px 11px", marginTop: -10 }} target="_blank" href="/addnew-thirdparty" >Add New Third Party?</a>
                      <i className="fa fa-refresh" style={{ fontSize: 15, marginTop: -7, cursor: "pointer", padding: "0 0 0 10px" }} onClick={onParticipantsRefresh} />
                    </div> : null}
                </div>
              </div>
            </div>
            <div className="column">
              <div className="control">
                <div className="is-small">
                  <p className="multiExpLbl">Contact Name {isEditable && required}</p>
                  {isEditable ?
                    <DropDownListClear
                      filter
                      tabIndex={tabIndex + 2}
                      data={userList}
                      value={participant.partContactName}
                      textField="name"
                      valueField="id"
                      itemComponent={ListItem}
                      disabled={!isEditable || !canEditTran}
                      isHideButton={participant.partContactName && isEditable}
                      onChange={onUserChange}
                      onBlur={(e) => onBlurInput(e, "partContactName", "Contact Name")}
                      onClear={() => {onUserChange({})}}
                    />
                    : <div>{getTranUserUrl(participant.partType, participant.partContactName, participant.partContactId, searchText, participant.partUserActive)}</div>
                  }
                  {errors.partContactName && <p className="text-error">{errors.partContactName}</p>}
                </div>
                {participant.partFirmName && !participant.partContactName && isEditable ?
                  <div>
                    <a className="has-text-link" style={{ fontSize: 12, padding: "0 0 8px 11px", marginTop: -10 }} target="_blank" href="/addnew-contact" >Cannot find contact?</a>
                    <i className="fa fa-refresh" style={{ fontSize: 15, marginTop: -7, cursor: "pointer", padding: "0 0 0 10px" }} onClick={onParticipantsRefresh} />
                  </div> : null}
              </div>
            </div>
            <div className="column">
              <div className="is-small">
                <p className="multiExpLbl">Phone</p>
                <PhoneInput
                  value={ participantPhone || "" }
                  disabled
                />
                {/* <NumberFormat
                  title="Phone"
                  format="+1 (###) ###-####"
                  style={{width: 300, background: "transparent", border: "none"}}
                  // mask="_"
                  className="input is-small is-link"
                  name="partContactPhone"
                  value={participant.partContactPhone || ""}
                  disabled
                /> */}
              </div>
            </div>
            {/* <SelectLabelInput label="Phone" error={errors.partContactPhone && "Required/Valid"} list={userPhones || []} value={participant.partContactPhone} name="partContactPhone"
              onChange={onChange} onBlur={onBlurInput} disabled inputStyle={{ maxWidth: "100%", width: "100%" }} /> */}
            {/* <SelectLabelInput label="Email" error={errors.partContactEmail && "Required/Valid"} list={userEmails || []} value={participant.partContactEmail} name="partContactEmail"
              onChange={onChange} onBlur={onBlurInput} disabled inputStyle={{ maxWidth: "100%", width: "100%" }} notPicklist={true}/> */}
            <div className="column">
              <p className="multiExpLbl">Email</p>
              <div className="control">
                <div className="select is-small is-link" style={{ width: "100%" }}>
                  <select
                    title="Email"
                    value={participant.partContactEmail || ""}
                    onChange={onChange}
                    onBlur={onBlurInput}
                    name="partContactEmail"
                    disabled
                    style={{ width: "100%" }}
                  >
                    <option value="" disabled="">Pick Email</option>
                    {(userEmails && userEmails.length) ? userEmails.map(t => (
                      <option key={t.toString()} value={t}>{t}</option>
                    )) : null}
                  </select>
                  {errors.partContactEmail && ( <small className="text-error">Required/Valid</small> )}
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="column">
          <div className="field is-grouped">
            <div className="control">
              <p className="multiExpLbl">Add to DL?</p>
              <label className="checkbox"> {/* eslint-disable-line */}
                <input type="checkbox" name="addToDL" checked={participant.addToDL || false} onChange={() => { }} disabled={!participant._id || !canEditTran} onClick={(e) => onCheckAddTODL(e, participant, "participant")} />
              </label>
            </div>
          </div>
        </div>
      </div>

      <div className="columns">
        <div className="column is-11">
          <div className="columns">
            <div className="column is-2">
              <div className="control">
                <div >
                  <p className="multiExpLbl">Addresses</p>
                  {isEditable ?
                    <DropDownListClear
                      filter
                      tabIndex={tabIndex + 3}
                      data={userAddresses}
                      value={userAddress ? userAddress.name : ""}
                      textField="name"
                      valueField="id"
                      defaultValue="Enter new Address"
                      disabled={!isEditable || !canEditTran}
                      isHideButton={userAddress && isEditable}
                      onChange={onAddressChange}
                      onClear={() => {onAddressChange({})}}
                    />
                    : <small>{userAddress ? (userAddress && userAddress.name) :
                      (participant && participant.partContactAddrLine1 ? "New Address" : "")}</small>
                  }
                  {errors.partUserAddress && <p className="text-error">{errors.partUserAddress}</p>}
                </div>
              </div>
            </div>

            <div className="column is-three-fifths">
              {
                isEditable ?
                  <div className="control">
                    <div className="is-small">
                      <ParticipantsSearchAddress
                        idx={0}
                        tabIndex={tabIndex + 4}
                        getAddressDetails={getAddressDetails}
                        value={`${participant.partContactAddrLine1 || ""}`}
                        isDisabled={!!participant.partUserAddress}
                      />
                    </div>
                  </div> :
                  <div className="control">
                    <p className="multiExpLbl">Address</p>
                    <a href={`https://maps.google.com/?q=${participant.participantGoogleAddress ? participant.participantGoogleAddress : participant.partContactAddrLine1}`} target="_blank">
                      <small dangerouslySetInnerHTML={{__html: highlightSearch(participant.partContactAddrLine1 ? `${participant.partContactAddrLine1 || ""}` : "",searchText)}}/>
                      {/* <small dangerouslySetInnerHTML={{__html: highlightSearch(participant.partContactAddrLine2 ? `${participant.partContactAddrLine2}${participant.participantState ? "," : ""}` : "",searchText)}}/>
                      <small dangerouslySetInnerHTML={{__html: highlightSearch(participant.participantState || "",searchText)}}/> */}
                    </a>
                  </div>
              }
            </div>

            <div className="column">
              <div className="control">
                <p className="multiExpLbl">Fax</p>
                <PhoneInput
                  value={ participantFax || "" }
                  disabled
                />
                {/* <NumberFormat
                  title="Fax"
                  format="+1 (###) ###-####"
                  style={{width: 300, background: "transparent", border: "none"}}
                  // mask="_"
                  className="input is-small is-link"
                  name="dealPartContactPhone"
                  value={participant.partContactFax || ""}
                  disabled
                /> */}
              </div>
            </div>
            {/* {isEditable ?
              <TextLabelInput label="Address Line 1" error={errors.partContactAddrLine1 || ""} name="partContactAddrLine1" value={participant.partContactAddrLine1} placeholder="Address Line 1" onChange={onChange} onBlur={onBlurInput} disabled={!isEditable || !canEditTran} />
              : null
            }
            {isEditable ?
              <TextLabelInput label="Address Line 2" error={errors.partContactAddrLine2 || ""} name="partContactAddrLine2" value={participant.partContactAddrLine2} placeholder="Address Line 2" onChange={onChange} onBlur={onBlurInput} disabled={!isEditable || !canEditTran} />
              : null
            }
            {isEditable ?
              <TextLabelInput label="State" error={errors.participantState || ""} name="participantState" value={participant.participantState} placeholder="State" onChange={onChange} onBlur={onBlurInput} disabled={!isEditable || !canEditTran} />
              : null
            }
            {!isEditable ?
              <div className="column">
                <p className="multiExpLblBlk">Address</p>
                <a href={`https://maps.google.com/?q=${participant.partContactAddrLine1} ${participant.partContactAddrLine2 || ""} ${participant.participantState || ""}`} target="_blank">
                  <small dangerouslySetInnerHTML={{__html: highlightSearch(participant.partContactAddrLine1 ? `${participant.partContactAddrLine1}${participant.partContactAddrLine2 || participant.participantState ? "," : ""}` : "",searchText)}}/>
                  <small dangerouslySetInnerHTML={{__html: highlightSearch(participant.partContactAddrLine2 ? `${participant.partContactAddrLine2}${participant.participantState ? "," : ""}` : "",searchText)}}/>
                  <small dangerouslySetInnerHTML={{__html: highlightSearch(participant.participantState || "",searchText)}}/>
                </a>
              </div> : null
            }
            <div className="column" />
            {!isEditable ? <div className="column" /> : null}
            {!isEditable ? <div className="column" /> : null} */}
          </div>
        </div>
        {canEditTran ?
          <div className="column" style={{ marginTop: 18 }}>
            <div className="field is-grouped">
              <div className="control">
                <a
                  onClick={isEditable ? () => onSave(category, participant, index) : () => onEdit(category, index, participant)}
                  className={`${isSaveDisabled ? "isDisabled" : ""}`}> {/* eslint-disable-line */}
                  <span className="has-text-link">
                    {isEditable ?
                      <i onKeyDown={(e) => onSaveBtnClick(e, category, participant, index)} tabIndex={tabIndex + 5}
                        className="far fa-save" title="Save"/> : <i className="fas fa-pencil-alt" title="Edit"/>}
                  </span>
                </a>
              </div>
              <div className="control">
                <a onClick={isEditable ? () => onCancel(category) : () => onRemove(participant._id, category, index, participant.partContactName, tableTitle, participant.partType)} className={`${isSaveDisabled ? "isDisabled" : ""}`}> {/* eslint-disable-line */}
                  <span className="has-text-link">
                    {isEditable ?
                      <i onKeyDown={(e) => onCancelBtnClick(e, category)} className="fa fa-times" title="Cancel"
                        tabIndex={tabIndex + 6}/> : <i className="far fa-trash-alt" title="Delete"/>}
                  </span>
                </a>
              </div>
            </div>
            {participant.isNew && !isEditable ? <p className="text-error">New(Not Save)</p> : null}
          </div> : null}
      </div>
    </div>
  )
}


export default Participants
