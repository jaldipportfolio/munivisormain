// import React from "react"
// import moment from "moment/moment"
// import { Modal } from "../../../FunctionalComponents/TaskManagement/components/Modal"
//
// export const InvoicePreviewModal  = (props) => {
//   const { invoice, invoiceNumber, previewModal } = props
//   const {addresses} = invoice
//   let primaryAddress = {}
//   let email = ""
//   let phone = ""
//   if (addresses && Array.isArray(addresses) && addresses.length) {
//     primaryAddress = addresses.find(e => e.isPrimary)
//     primaryAddress = Object.keys(primaryAddress || {}).length ? primaryAddress : addresses[0]
//     email = primaryAddress.officeEmails && primaryAddress.officeEmails.length ? primaryAddress.officeEmails[0].emailId : ""
//     phone = primaryAddress.officePhone && primaryAddress.officePhone.length ? primaryAddress.officePhone[0].phoneNumber : ""
//   }
//
//   const toggleModal = () => {
//     props.onModalChange({ previewModal: !props.previewModal })
//   }
//
//   const renderTableHeadOrData =(tag, value) => {
//     if(tag === "head") return <th className="right">{value}</th>
//     if(tag === "data") return <td className="right">{value}</td>
//     return null
//   }
//
//   const onPrint = () => {
//     const printContents = document.getElementById("print").innerHTML
//     const originalContents = document.body.innerHTML
//     document.body.innerHTML = printContents
//     window.print()
//     document.body.innerHTML = originalContents
//   }
//
//   return(
//     <Modal
//       closeModal={toggleModal}
//       modalState={previewModal}
//       title="Invoice Preview"
//       modalWidth={{ width: "80%", height: "100%" }}
//     >
//
//       <div className="card">
//         <header className="card-header" style={{backgroundColor: "#f0f3f5", borderBottom: "1px solid #c8ced3"}}>
//           <p className="card-header-title">
//             Invoice <strong>#{invoiceNumber || ""}</strong>
//           </p>
//           <div className="card-header-icon" aria-label="more options">
//             <button className="button is-gray is-small" onClick={onPrint}>Print</button>
//           </div>
//
//         </header>
//         <div className="card-content" id="print">
//           <div className="columns">
//             <div className="column">
//               <p className="multiExpLblBlk">From:</p>
//               <div><strong>Munivisor By Otaras</strong></div>
//               <div><small>Email: munivisor@otaras.com</small></div>
//               <div><small>Phone: +48 123 456 789</small></div>
//             </div>
//             <div className="column">
//               <p className="multiExpLblBlk">To:</p>
//               <div><strong>{invoice && invoice.name || ""}</strong></div>
//               <div><small>{primaryAddress.addressLine1 || ""}</small></div>
//               <div><small>{primaryAddress.addressLine2 || ""}</small></div>
//               <div><small>{primaryAddress.city || ""} {primaryAddress.state || ""} {primaryAddress.country || ""}</small></div>
//               <div><small>{ email ? `Email: ${email}` : null}</small></div>
//               <div><small>{ email ? `Phone: ${phone}` : null}</small></div>
//             </div>
//             <div className="column">
//               <p className="multiExpLblBlk">Details:</p>
//               <div>Invoice <strong>#{invoiceNumber || "29MAY-20192377"}</strong></div>
//               <div><small>{moment(new Date()).format("MMM DD, YYYY")}</small></div>
//               <div><small>Account Name: {invoice.name || ""}</small></div>
//               <div><small>Type Of Billing: {invoice.typeOfBilling === "user" ? "User Specific" : "Enterprise"}</small></div>
//             </div>
//           </div>
//           <hr/>
//
//           <table className="table table-striped" style={{width: "100%"}}>
//             <thead>
//             <tr>
//               <th>Type of user</th>
//               <th className="center">Number of user</th>
//               {invoice && invoice.typeOfBilling === "user" ? renderTableHeadOrData("head", "Unit Cost") : null}
//               {invoice && invoice.typeOfBilling === "user" ? renderTableHeadOrData("head", "Total") : null}
//             </tr>
//             </thead>
//             <tbody>
//             <tr>
//               <td className="center">Series 50</td>
//               <td className="center">{invoice.series50 || 0}</td>
//               {invoice && invoice.typeOfBilling === "user" ? renderTableHeadOrData("data", invoice.chargPerseries50 ? `$${invoice.chargPerseries50}` : "-" ) : null}
//               {invoice && invoice.typeOfBilling === "user" ? renderTableHeadOrData("data", invoice.chargPerseries50 ? `$${invoice.chargPerseries50*invoice.series50}` : "-") : null}
//             </tr>
//             <tr>
//               <td className="center">Non Series 50</td>
//               <td className="center">{invoice.nonSeries50 || 0}</td>
//               {invoice && invoice.typeOfBilling === "user" ? renderTableHeadOrData("data", invoice.chargPerNonSeries50 ? `$${invoice.chargPerNonSeries50}` : "-" ) : null}
//               {invoice && invoice.typeOfBilling === "user" ? renderTableHeadOrData("data", invoice.chargPerseries50 ? `$${invoice.chargPerseries50*invoice.nonSeries50}` : "-") : null}
//             </tr>
//             </tbody>
//           </table>
//
//           <div className="columns is-mobile">
//             <div
//               className="column is-one-quarter">
//               <small> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</small>
//             </div>
//             <div className="column"/>
//             <div className="column"/>
//             <div className="column">
//               <div><strong>Subtotal</strong></div>
//               <div><strong>Discount {invoice.discountPercentage ? `(${invoice.discountPercentage || 0})` : ""}</strong></div>
//               <div><strong>Total</strong></div>
//             </div>
//             <div className="column">
//               <div><small>{`$${Number(invoice.totalCost || 0).toLocaleString()}` || "-"}</small></div>
//               <div><small>{`$${Number(invoice.totalCost-invoice.payableAmount || 0).toLocaleString()}` || "-"}</small></div>
//               <div><small>{`$${Number(invoice.payableAmount || 0).toLocaleString()}` || "-"}</small></div>
//             </div>
//           </div>
//         </div>
//
//       </div>
//
//     </Modal>
//   )
// }
//
//
// export default InvoicePreviewModal


import React from "react"
import { PaymentSuccessModal } from "./PaymentSuccessModal"
import image from "../../../../public/images/paymentSuccessful.jpg"

export const InvoicePreviewModal  = (props) => {
  const { previewModal } = props

  const toggleModal = () => {
    props.onModalChange({ previewModal: !props.previewModal })
  }

  return(
    <PaymentSuccessModal
      closeModal={toggleModal}
      modalState={previewModal}
      payment

    >

      <div className="columns">
        <div className="column is-3">
          <img src={image}/>
        </div>
        <div className="column is-9">
          <p style={{textAlign: "center", fontSize: 30, color: "mediumpurple"}}>Your payment was successful</p>
          <h1 style={{textAlign: "center" }}>We just sent your receipt to your email address.</h1>
        </div>
      </div>

    </PaymentSuccessModal>
  )
}


export default InvoicePreviewModal
