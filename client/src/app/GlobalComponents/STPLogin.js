import React, { Component } from "react"
import qs from "qs"

import Loader from "./Loader"

class STPLogin extends Component  {
  componentDidMount() {
    // console.log("props : ", this.props)
    const query = qs.parse(this.props.location.search, { ignoreQueryPrefix: true }) || {}
    // console.log("query : ", query)
    const { token, email, redirect } = query
    // console.log("token : ", token)
    // console.log("redirect : ", redirect)
    localStorage.setItem("token",token)
    localStorage.setItem("email",email)
    this.props.history.push(`/${redirect}`)
  }
  render() {
    return <Loader />
  }
}

export default STPLogin
