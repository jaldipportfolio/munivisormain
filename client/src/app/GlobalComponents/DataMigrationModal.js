import React from "react"
import { Modal } from "../FunctionalComponents/TaskManagement/components/Modal"

const DataMigrationModal  = (props) => (
  <Modal
    closeModal={props.toggleModal}
    modalState={props.modalState}
    title="Data Migration Errors"
  >
    <div className="columns">
      <div className="column">
        <table className="table is-bordered is-striped is-hoverable is-fullwidth">
          <thead>
            <tr>
              <th>Column Name</th>
              <th>Number of errors</th>
            </tr>
          </thead>
          <tbody>
            {
              Object.keys(props.data).map((key, index) => {
                if (props.data[key]) {
                  return (
                    <tr key={index.toString()}>
                      <td>
                        <span>
                          {key}
                        </span>
                      </td>
                      <td>
                        <span>
                          {props.data[key] || ""}
                        </span>
                      </td>
                    </tr>
                  )
                }
              })
            }
          </tbody>
        </table>
      </div>
    </div>
  </Modal>
)
export default DataMigrationModal
