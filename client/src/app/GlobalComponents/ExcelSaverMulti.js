import React, { Component } from "react"
import {postJSONData} from "../../../src/app/StateManagement/actions/Common"


class ExcelSaverMulti extends Component {

  componentWillReceiveProps(nextProps) {
    if(nextProps.startDownload && !this.props.startDownload) {
      this.getSample(nextProps)
    }
  }

  getSample = async (props) => {
    props = props || this.props
    const { jsonSheets, label, afterDownload } = props
    console.log("label : ", label)
    console.log("jsonSheets : ", jsonSheets)
    if(jsonSheets && jsonSheets.length){
      const payload = {}
      payload.dataArray = jsonSheets
      await postJSONData(payload, label)
      if(afterDownload) {
        afterDownload()
      }
    } else {
      console.log("Pleas pass the jsonSheets")
    }
  }

  render() {
    return (
      <a className="is-size-7" onClick={this.getSample}>
        {this.props.label || "Excel File"}</a>
    )
  }

}

export default ExcelSaverMulti
