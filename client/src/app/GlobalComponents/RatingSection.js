import React from "react"
import "../FunctionalComponents/RFP/Manage/scss/ratings.scss"

const props = {
  paddingTop: 0,
  paddingBottom: 0
}

const RatingSection = ({
  children,
  actionButtons,
  title,
  headerStyle,
  onAccordion,
  style
}) => (
  <article className="accordion is-active">
    <div className="accordion-header">
      <p style={headerStyle || {}} onClick={onAccordion || null}>{title}</p>
      <div>
        {actionButtons || null}
      </div>
      <i className={`${children ? "fas fa-chevron-down":"fas fa-chevron-up"}`} onClick={onAccordion || null}/>
    </div>
    <div className="accordion-body" style={style}>
      <div
        className="accordion-content"
        style={!children ? props : { overflow: "initial" }}
      >
        {children}
      </div>
    </div>
  </article>
)

export default RatingSection
