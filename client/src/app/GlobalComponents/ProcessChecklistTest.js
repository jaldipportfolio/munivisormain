import React, { Component } from "react"

import ProcessChecklist from "./ProcessChecklist"

class ProcessChecklistTest extends Component {

  constructor(props) {
    super(props)
    this.state = { checklists: [] }
    this.saveChecklist = this.saveChecklist.bind(this)
  }

  saveChecklist(checklists) {
    console.log("saving checklist : ", checklists)
    // this.setState(prevState => {
    // const checklists = [ ...prevState.checklists ]
    // const idx = checklists.findIndex(e => e.id === checklist.id)
    // if(idx >= 0) {
    //   checklists[idx] = checklist
    // } else {
    //   checklists.push(checklist)
    // }
    //   return { checklists }
    // })
    this.setState({ checklists })
  }

  render() {
    return (
      <ProcessChecklist checklists={this.state.checklists}
        totalThresholds={[0.20, 15000]}
        participants={[
          {_id: 1, name: "Ron", type:"RFP Contacts"},
          {_id: 2, name: "Max",type:"Evaluation Team"},
          {_id: 3, name: "John",type:"RFP Assignees"},
          {_id: 4, name: "Dave", type:"Bond Counsel"},
          {_id: 5, name: "Ruth",type:"Financial Advisor"}
        ]}
        onSaveChecklist={this.saveChecklist}
      />
    )
  }
}

export default ProcessChecklistTest
