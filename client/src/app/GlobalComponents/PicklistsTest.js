import React, { Component } from "react"

import { Combobox } from "react-widgets"

import { getPicklistValues } from "GlobalUtils/helpers"

class PicklistsTest extends Component {
  constructor(props) {
    super(props)
    this.state = { selectedValue: "", listValues: [] }
    this.changeSelectedValue = this.changeSelectedValue.bind(this)
  }

  async componentDidMount() {
    const result = await getPicklistValues(["LKUPRATINGTERM", "LKUPHIGHMEDLOW"])
    console.log("result : ", result)
    const listValues = result[1]
    this.setState({ listValues })
  }

  changeSelectedValue(selectedValue) {
    this.setState( { selectedValue })
  }

  render() {
    return (
      <div className="container">
        <Combobox
          style={{ "maxWidth": "350px" }}
          data={this.state.listValues}
          caseSensitive={false}
          minLength={1}
          value={this.state.selectedValue}
          onChange={this.changeSelectedValue}
          filter='contains'
        />
      </div>
    )
  }
}

export default PicklistsTest
