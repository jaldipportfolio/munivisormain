import React from "react"
import swal from "sweetalert"
import Accordion from "./Accordion"
import RatingSection from "./RatingSection"

export const View = (value, key) => {
  swal(`${key}: ${value}`, {
    button: false
  })
}

const HistoricalData = ({data, childData, syncData, title}) => {
  // if(!data || (data && !Object.keys(data).length)){
  //   return null
  // }
  const style = {
    noWrap: {
      whiteSpace: "nowrap",
      overflow: "hidden",
      textOverflow: "ellipsis",
      width: "8em",
      color: "blue",
      fontWeight: "bold"
    },
    flexWrap: {
      marginTop: 0,
      display: "flex",
      flexWrap: "wrap"
    },
    color: {
      fontWeight: "normal",
      color: "gray",
      cursor: "pointer"
    },
  }
  return (
    <Accordion multiple activeItem={[]} boxHidden render={({activeAccordions, onAccordion}) =>
      <RatingSection onAccordion={() => onAccordion(0)} title={title || "Historical Data"}>
        {activeAccordions.includes(0) &&
          <div>
            {
              syncData ? <pre> {JSON.stringify(data, null, 2)}</pre> :
                <div>
                  { childData && childData.length ? childData.map((child, index) => {
                    return(
                      <div>
                        <div className="migtools" style={style.flexWrap}>
                          {
                            Object.keys(child).map((key, index) => {
                              if (child[key]) {
                                return (
                                  <div className="mapping-input" key={index.toString()} style={{padding: 10}}>
                                    <div className="multiExpLblBlk">{key}</div>
                                    <div className="is-small  is-fullwidth is-link" style={style.noWrap}>
                                      <span style={style.color} title={child[key]} onClick={() => View(child[key], key)}>{child[key] || ""}</span>
                                    </div>
                                  </div>
                                )
                              }
                              return null
                            })
                          }
                        </div>
                        { childData.length - 1 === index ? null : <hr style={{border: "1px dotted"}}/> }
                      </div>
                    )
                  }) : null }
                  <div className="migtools" style={style.flexWrap}>
                    {
                      data && Object.keys(data).length ? Object.keys(data).map((key, index) => {
                        if (data[key]) {
                          return (
                            <div className="mapping-input" key={index.toString()} style={{padding: 10}}>
                              <div className="multiExpLblBlk">{key}</div>
                              <div className="is-small  is-fullwidth is-link" style={style.noWrap}>
                                <span style={style.color} title={data[key]} onClick={() => View(data[key], key)}>{data[key] || ""}</span>
                              </div>
                            </div>
                          )
                        }
                        return null
                      }) : null
                    }
                  </div>
                </div>
            }
          </div>
        }
      </RatingSection>
    }/>
  )
}

export default HistoricalData
