import React from "react"
import moment from "moment"
import get from "lodash.get"
import BorrowerLookup from "Global/BorrowerLookup"
import DocUpload from "../FunctionalComponents/docs/DocUpload"
import {TextLabelInput, SelectLabelInput, MultiSelect, NumberInput} from "./TextViewBox"
import DocLink from "../FunctionalComponents/docs/DocLink"
import DocModal from "../FunctionalComponents/docs/DocModal"
import DropDownListClear from "./DropDownListClear"
import PhoneInput, { formatPhoneNumber, isValidPhoneNumber } from "react-phone-number-input"

const TableBody = ({ tableIndex, item, body, index, users, error, category, onItemChange, onBlur, isEditable={}, onSave, onCancel, onEdit, onRemove, dropDown={}, onParentChange, onPhoneError,
  isSaveDisabled, bucketName, getBidBucket, contextType, contextId, tenantId, canEdit, tableTitle, deleteDoc, titles, minDate, maxDate, getUsers, duplicateData, tabIndex}) => {

  const onChange = (event) => onItemChange({
    ...item,
    [event.target.name]: event.target.value,
  }, category, index)

  const onBlurInput = (event) => {
    if(event && event.target && event.target.title && (event.target.checked || event.target.value)) {
      onBlur(category, `In ${tableTitle} ${event.target.title || "empty"} change to ${event.target.type === "checkbox" ? event.target.checked : event.target.value || "empty"}`, tableTitle)
    }
  }

  const onSaveBtnClick = (e, category, item, index, tableTitle) =>{
    if(e.keyCode === 13){
      onSave(category, item, index, tableTitle)
    }
  }

  const onCancelBtnClick = (e, category) =>{
    if(e.keyCode === 13){
      onCancel(category)
    }
  }

  isEditable = (isEditable[category] === index)

  return (
    <tbody>
      <tr>
        {
          body && body.length ? body.map((td,tdIndex) => {
            const valDate = td.type === "date" ? new Date(get(item, td.name, "")) : ""
            let val = td.type === "date" ? get(item, td.name, "") ? isEditable ? new Date(valDate) :
              new Date(valDate) : ""  : (td.type === "text" || td.type === "select") ? get(item, td.name, "") || "" : ""
            const html = titles.length ? (titles[tdIndex] && titles[tdIndex].name) || "" : ""
            const div = document.createElement("div")
            div.innerHTML = html
            const inputTitle = div.textContent || div.innerText || ""
            if(typeof item[td.keyValue] === "object") {
              let objVal = []
              const remain = td.remainVal ? td.remainVal : []
              Object.keys(item[td.keyValue]).forEach(key => {
                if(remain.indexOf(key) === -1) {
                  objVal.push(item[td.keyValue][key])
                }
              })
              objVal = objVal.filter(v => v !== "" )
              val = objVal.toString()
            }

            const onPhoneChange = (e) => {
              const Internationals = e && formatPhoneNumber(e, "International") || ""
              onItemChange({
                ...item,
                [td.name]: Internationals,
              }, category, index)
              const error = Internationals ? (isValidPhoneNumber(Internationals) ? "" : "Invalid phone number") : ""
              onPhoneError(error, category, index, td.name)
            }

            if(canEdit && td.name === "action"){
              return (
                <td key={tdIndex.toString()}>
                  <div className="field is-grouped" style={{marginBottom: 0}}>
                    <div className="control">
                      <a onClick={isEditable ? () => onSave(category, item, index, tableTitle) : () => onEdit(category, index, tableTitle, item)} className={`${isSaveDisabled ? "isDisabled" : ""}`}>
                        <span className="has-text-link">
                          {isEditable ?
                            <i className="far fa-save"
                              title="Save"
                              tabIndex={td.tabIndex}
                              onKeyDown={(e) => onSaveBtnClick(e, category, item, index, tableTitle)}/> :
                            <i className="fas fa-pencil-alt" title="Edit"/>}
                        </span>
                      </a>
                    </div>
                    <div className="control">
                      <a onClick={isEditable ? () => onCancel(category) :() => onRemove(item._id, category, index, tableTitle)} className={`${isSaveDisabled ? "isDisabled" : ""}`}>
                        <span className="has-text-link">
                          {isEditable ?
                            <i className="fa fa-times"
                              title="Cancel"
                              tabIndex={td.tabIndex + 1}
                              onKeyDown={(e) => onCancelBtnClick(e, category)}
                            /> :
                            <i className="far fa-trash-alt" title="Delete"/>}
                        </span>
                      </a>
                    </div>
                  </div>
                  {(item.isNew && !isEditable || duplicateData ) ?
                    <small className="text-error">New(Not Save)</small> : null}
                </td>
              )
            }

            if(td.type === "dropdown") {
              const clearObjectKey = ["name", "recipientEntityName", "seriesName"]
              let clear
              if (clearObjectKey.includes(td.name)) {
                clear = {}
              } else { clear = "" }
              return (
                <td key={tdIndex.toString()} style={td.style || {}}>
                  {/* <DropDownInput filter data={td.dropdownKey === "users" ? _.orderBy(users, ['name']) : _.orderBy(dropDown[td.dropdownKey], ['name']) || []}
                    error={error[td.name] || null} groupBy={td.groupBy || ""}
                    placeholder={td.displayName}
                    disableValue={typeof item[td.name] === "object" ? item[td.name].name || item[td.name][td.labelName] || "" : item[td.labelName] || `${item[td.labelName1] || "" } ${item[td.labelName2] || ""}` || ""}
                    value={typeof item[td.name] === "object" ? item[td.name].name ||  item[td.name][td.labelName] || `${item[td.name][td.labelName1] || ""} ${item[td.name][td.labelName2] || ""}` || "" : item[td.name]}
                    onChange={(select) => onParentChange(td.name, category, select, index, td, inputTitle)} disabled={!isEditable || !canEdit}/> */}

                  <DropDownListClear
                    filter
                    autoFocus={tdIndex === tableIndex && !item._id}
                    tabIndex={td.tabIndex}
                    data={td.dropdownKey === "users" ? _.orderBy(users, ["name"]) : _.orderBy(dropDown[td.dropdownKey], ["name"]) || []}
                    groupBy={td.groupBy || ""}
                    placeholder={td.displayName}
                    textField="name"
                    valueField="name"
                    disableValue={typeof item[td.name] === "object" ? item[td.name].name || item[td.name][td.labelName] || "" : item[td.labelName] || `${item[td.labelName1] || "" } ${item[td.labelName2] || ""}` || ""}
                    value={typeof item[td.name] === "object" ? item[td.name].name || item[td.name][td.labelName] || `${item[td.name][td.labelName1] || ""} ${item[td.name][td.labelName2] || ""}` || "" : item[td.name]}
                    onChange={(select) => onParentChange(td.name, category, select, index, td, inputTitle)} disabled={!isEditable || !canEdit || dropDown[td.disabledDropdownKey]}
                    isHideButton={item[td.name] && isEditable}
                    onClear={() => onParentChange(td.name, category, clear, index, td, inputTitle)}
                  />
                  {error[td.name] ? <p className="text-error">{error[td.name]}</p> : null}
                  {
                    (td.name === "name" && category === "responsibleSupervision") && !item[td.name] && (isEditable || canEdit) ?
                      <span className="is-link" style={{ fontSize: 12 }}>
                        <a className="has-text-link" target="_blank" href="/addnew-contact" >Cannot find contact?</a>
                        <i className="fa fa-refresh" style={{cursor: "pointer", padding: "0 0 0 10px"}} onClick={getUsers} />
                      </span> : null
                  }
                </td>
              )
            }

            if(td.type === "multiselect") {

              return (
                <td key={tdIndex.toString()}>
                  <MultiSelect tabIndex={td.tabIndex} filter data={dropDown[td.dropdownKey] || []} value={item[td.name] || []} error={error[td.name] || null}
                    disableValue={item[td.name] || []} onChange={(select) => onParentChange(td.name, category, select, index, td, inputTitle)} disabled={!isEditable || !canEdit}/>
                  {
                    td.name === "trgAttendees" && !item[td.name] ?  <a className="has-text-link" style={{fontSize: 12}} target="_blank" href="/addnew-contact" >Cannot find contact?</a> : null
                  }
                </td>
              )
            }

            if(td.type === "select") {
              return (
                <td key={tdIndex.toString()}>
                  {
                    isEditable ?
                      <SelectLabelInput tabIndex={td.tabIndex} title={inputTitle} error={error[td.name] || ""} list={dropDown[td.name] || (td.selectionKey ? dropDown[td.selectionKey] : [])} name={td.name} value={item[td.name] || ""} placeholder={td.placeholder} onChange={onChange} onBlur={onBlurInput}
                        disabled={!isEditable || !canEdit}/>
                      : <small>{item[td.name] || ""}</small>
                  }
                </td>
              )
            }

            if(td.type === "dependedSelect") {
              return (
                <td key={tdIndex.toString()}>
                  {
                    isEditable ?
                      <SelectLabelInput title={inputTitle} error={error[td.name] || ""}
                        list={(dropDown[td.selectionKey] && dropDown[td.selectionKey][item[td.childKey]]) || []}
                        name={td.name} value={item[td.name] || ""} placeholder={td.placeholder}
                        onChange={onChange} onBlur={onBlurInput}
                        disabled={!isEditable || !canEdit} tabIndex={td.tabIndex} />
                      : <small>{item[td.name] || ""}</small>
                  }
                </td>
              )
            }

            if(td.type === "lookup") {
              const entityName = {
                _id: "",
                firmName: "",
                participantType: "Governmental Entity / Issuer",
                isExisting: true
              }
              return (
                <td key={tdIndex.toString()}>
                  {
                    isEditable ?
                      <BorrowerLookup
                        tabIndex={td.tabIndex}
                        entityName={item[td.name] || entityName}
                        // onChange={(e) => onChangeEntityName(td.name, e)}
                        error={error[td.name] || ""}
                        onChange={(e) => onChange({ target: { name: td.name, value: e.firmName } })}
                        type="other"
                        style={{ width: 355, fontSize: 12 }}
                        notEditable={!canEdit}
                        isHide={item[td.name] && canEdit}
                      />
                      : <small>{item[td.name] || ""}</small>
                  }
                </td>
              )
            }

            if(td.type === "number") {
              return (
                <td key={tdIndex.toString()}>
                  {
                    isEditable || canEdit ?
                      <NumberInput tabIndex={td.tabIndex} title={inputTitle} prefix={td.prefix || ""} suffix={td.suffix || ""} error={error[td.name] ? td.error : error[td.name] || ""} name={td.name} value={item[td.name] || ""} placeholder={td.placeholder} onChange={onChange} onBlur={onBlurInput}
                        disabled={!isEditable || !canEdit}/>
                      : <small>{item[td.name] ? `${td.prefix || ""} ${item[td.name] || ""} ${td.suffix || ""}` : ""}</small>
                  }
                </td>
              )
            }

            if(td.type === "phone") {
              return(
                <td key={tdIndex.toString()}>
                  <div className="control">
                    {isEditable ?
                      /* <NumberFormat
                        tabIndex={td.tabIndex}
                        title={inputTitle}
                        format="+1 (###) ###-####"
                        mask="_"
                        className="input is-small is-link"
                        name={td.name}
                        placeholder="+1 (###) ###-####"
                        value={item[td.name] || ""}
                        onBlur={(event) => onBlurInput(event)}
                        onChange={(event) => onChange(event)}
                        disabled={!canEdit}
                      /> */
                      <PhoneInput
                        tabIndex={td.tabIndex}
                        title={inputTitle}
                        placeholder={td.placeholder}
                        name={td.name}
                        value={item[td.name] && formatPhoneNumber(item[td.name], "International") || "" }
                        onChange={event => onPhoneChange(event)}
                        disabled={!canEdit}
                      /> :
                      <PhoneInput
                        title={inputTitle}
                        value={item[td.name]}
                        disabled
                      />
                    }
                    {(isEditable && error[td.name]) ? <p className="text-error">Required/Valid</p> : null}
                  </div>
                </td>
              )
            }

            if(td.type === "document") {
              return(
                <td key={tdIndex.toString()}>
                  <DocUpload tabIndex={td.tabIndex} bucketName={bucketName} onUploadSuccess={getBidBucket} showFeedback bidIndex={index} contextId={contextId} contextType={contextType} tenantId={tenantId} disabled={!isEditable}/>
                  {error[td.labelName] ? <p className="text-error">Required</p> : null}
                </td>
              )
            }

            if(td.type === "file"){
              return(
                <td key={tdIndex.toString()}>
                  {item[td.name] ? <DocLink docId={item[td.name]} /> : null}
                  {item[td.name] ? <DocModal onDeleteAll={deleteDoc} documentId={item[td.documentIdLabel]} selectedDocId={item[td.name]} /> : null}
                </td>
              )
            }

            if(td.type === "label") {
              return (
                <td key={tdIndex.toString()}>
                  <small>{item[td.name] || ""}</small>
                  {/* <small>{Array.isArray(item[td.name]) ? ( item[td.name].map(item => item.length ? `${item},`: `${item}`)) : item[td.name] }</small> */}
                </td>
              )
            }

            if(td.type === "date") {
              return (
                <td key={tdIndex.toString()}>
                  {
                    isEditable ?
                      <TextLabelInput tabIndex={td.tabIndex} title={inputTitle} type={td.type} name={td.name} value={(item[td.name] === "" || !item[td.name]) ? null : new Date(item[td.name]) } placeholder={td.placeholder}
                        onChange = {td.parentEvent ? (e) => onParentChange(e, category, e, index, td, tableTitle) : onChange }
                        onBlur={onBlurInput} disabled={!isEditable || !canEdit} min={minDate} max={maxDate}/>
                      : <small>{val ? moment(val).format("MM-DD-YYYY") : "" || ""}</small>
                  }
                  {error[td.name] && td.type === "date" ? <p className="text-error">{td.error || "Required/valid"} </p> : null}
                </td>
              )
            }

            return (
              <td key={tdIndex.toString()}>
                {
                  isEditable ?
                    <TextLabelInput
                      title={inputTitle}
                      tabIndex={td.tabIndex}
                      error={(error[td.name] && td.error) ? td.error : error[td.name] || ""}
                      type={td.type || "text"}
                      name={td.name}
                      value={val || ""}
                      placeholder={td.placeholder}
                      /* onChange={onChange} */
                      onChange = {td.parentEvent ? (e) => onParentChange(e, category, e, index, td, tableTitle) : onChange }
                      onBlur={onBlurInput}
                      disabled={!isEditable || !canEdit}/>
                    : <small>{td.name === "userAddress" ? <a href={`https://maps.google.com/?q=${val}`} target="_blank">{val}</a> : val || ""}</small>
                }
              </td>
            )
          }) : null
        }
      </tr>
    </tbody>

  )}

export default TableBody
