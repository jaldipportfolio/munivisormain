import React from "react"
import {Link} from "react-router-dom"

const Tabs = ({activeTab, onTab, tabs, transactionId, type}) => (
  <div className="tabs">
    <ul>
      {
        tabs.map(tab=>(
          <li key={tab.value} className={`${tab.value === activeTab && "is-active"}`}>
            {
              transactionId ? <Link to={`/${type}/${transactionId}/${tab.path}`} role="button" className="tabSecLevel" >{tab.title}</Link> :
                <a role="button" className="tabSecLevel" onClick={() => onTab(tab.value)}>
                  {tab.title}
                </a>
            }
          </li>
        ))
      }
    </ul>
  </div>
)

export default Tabs
