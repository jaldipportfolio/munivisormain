import Joi from "joi-browser"

const loanPartDistSchema = Joi.object().keys({
  isNew: Joi.boolean().optional(),
  partFirmId: Joi.string().required().optional(),
  partType: Joi.string().required(),
  partFirmName: Joi.string().required().optional(),
  partContactId: Joi.string().required().optional(),
  partContactName: Joi.string().required(),
  partContactTitle: Joi.string().allow("").optional(),
  partContactEmail:  Joi.string().email().required(),
  partUserAddress: Joi.string().allow("").optional(),
  partContactAddrLine1: Joi.string().allow("").optional(),
  partContactAddrLine2: Joi.string().allow("").optional(),
  participantGoogleAddress: Joi.string().allow("").optional(),
  participantState: Joi.string().allow("").optional(),
  partContactPhone: Joi.string().allow("").optional(),
  partContactFax: Joi.string().allow("").optional(),
  addToDL: Joi.boolean().optional(),
  createdDate: Joi.date().example(new Date("2016-01-01")).optional(),
  _id: Joi.string().required().optional(),
})

export const ParticipantsValidate = (inputTransDistribute) => Joi.validate(inputTransDistribute, loanPartDistSchema, { abortEarly: false, stripUnknown:false })
