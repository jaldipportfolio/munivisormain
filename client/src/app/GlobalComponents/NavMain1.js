import React, { Component } from "react"
import { Link } from "react-router-dom"
import { connect } from "react-redux"

import { navLabels } from "GlobalUtils/consts"
import { getNavOptions, fakeLogout } from "../StateManagement/actions"
import "../../public/images/munivisor.png"

class NavMain extends Component {
  constructor(props) {
    super(props)
    this.state = { navOptions: {}, refreshNav: false }
    this.protectRoutes = this.protectRoutes.bind(this)
    this.logout = this.logout.bind(this)
  }

  componentDidMount() {
    this.getNavOptionsForUser(this.props)
    // this.setNavOptions(this.props.nav)
  }

  componentWillReceiveProps(nextProps) {
    if((!this.props.fakeAuthUser && nextProps.fakeAuthUser)
    || (this.props.fakeAuthUser && nextProps.fakeAuthUser
    && this.props.fakeAuthUser.userId !== nextProps.fakeAuthUser.userId)) {
      console.log("setting nav options")
      this.getNavOptionsForUser(nextProps)
    }
    this.setNavOptions(nextProps.nav)
  }

  getNavOptionsForUser(props) {
    let userId
    if(props.fakeAuthUser) {
      userId = props.fakeAuthUser.userId
    }
    console.log("userId : ", userId)
    if(userId) {
      props.getNavOptions(userId)
    } else {
      this.setNavOptions({})
    }
  }

  setNavOptions(navOptions) {
    const { refreshNav } = this.state
    this.setState({ navOptions, refreshNav: !refreshNav }, this.protectRoutes)
  }

  protectRoutes() {
    const { nav1, nav2 } = this.props
    const { navOptions } = this.state
    if(!navOptions[nav1] && (nav2 && navOptions[nav1] && !navOptions[nav1][nav2])) {
      console.log("Not Authorized - TODO!!!")
    }
  }

  logout() {
    this.props.fakeLogout()
  }

  renderNavs(options) {
    // console.log("options : ", options)
    let { navOption } = this.props
    navOption = navOption || "dashboard"
    const navItems = []
    Object.keys(options).forEach((e, i) => {
      if(options[e]) {
        // console.log("e : ", e)
        if(Object.getPrototypeOf(options[e]).constructor.name === "Object") {
          const subItems = []
          Object.keys(options[e]).forEach((k, j) => {
            if(options[e][k]) {
              // console.log("k : ", k)
              const itemClass = navOption === k ? "navbar-item is-active" : "navbar-item"
              subItems.push(
                <Link key={e+k+i+j} className={itemClass}
                  to={`/${k}`}>{navLabels[k] || k}</Link>
              )
            }
          })
          navItems.push(
            <div key={`${e}div${i}`} className="navbar-item has-dropdown is-hoverable">
              <a className="navbar-link" disabled selected>
                {navLabels[e] || e}
              </a>
              <div className="navbar-dropdown ">
                {subItems}
              </div>
            </div>
          )
        } else {
          const itemClass = navOption === e ? "navbar-item is-active" : "navbar-item"
          console.log("e : ", e)
          navItems.push(
            <Link key={e+i} to={`/${e}`}
              className={itemClass}>{navLabels[e] || e}</Link>
          )
        }
      }
    })
    console.log("navItems : ", navItems)
    return navItems
  }

  render() {
    const { navOptions } = this.state
    return (
      <nav id="navbar" className="navbar is-fixed-top">
        <div id="specialShadow" className="bd-special-shadow" />

        <div className="container">

          <div className="navbar-brand">

            <a className="navbar-item" href="/">
              <img src="images/munivisor.png" alt="MuniVisor"/>
            </a>

            <div id="navbarBurger" className="navbar-burger burger" data-target="navMenuDocumentation">
              <span />
              <span />
              <span />
            </div>

          </div>

          <div id="navMenuDocumentation" className="navbar-menu">

            <div className="navbar-start">
              {this.renderNavs(navOptions)}
            </div>

            <div className="navbar-end is-hidden-touch">

              <a href="" className="navbar-item">
                <span className="icon is-small">
                  <i className="fas fa-exclamation is-solid" />
                </span>
              </a>
              <a href="" className="navbar-item">
                <span className="icon is-small">
                  <i className="fa fa-bell is-solid" />
                </span>
              </a>
              <a href="" className="navbar-item">
                <span className="icon is-small">
                  <i className="fa fa-envelope" />
                </span>
              </a>
              <a href="" className="navbar-item">
                <span className="icon is-small">
                  <i className="fa fa-sliders-h" />
                </span>
              </a>
              <div className="navbar-item has-dropdown is-hoverable">
                <a href="" className="navbar-item">
                  <span className="icon">
                    <i className="fa fa-user" />
                  </span>
                </a>
                <div className="navbar-dropdown is-right">
                  <a className="navbar-item">
                    <span className="icon">
                      <i className="fas fa-cog" />
                    </span>
                                Profile
                  </a>
                  <hr className="navbar-divider" />
                  <a className="navbar-item">
                    <span className="icon" onClick={this.logout}>
                      <i className="fa fa-power-off" />
                    </span>
                                Logout
                  </a>
                </div>
              </div>

            </div>

          </div>

        </div>
      </nav>
    )
  }
}

const mapStateToProps = ({ nav, fakeAuthUser }) => ({ nav, fakeAuthUser })

export default connect(mapStateToProps, { getNavOptions, fakeLogout })(NavMain)
