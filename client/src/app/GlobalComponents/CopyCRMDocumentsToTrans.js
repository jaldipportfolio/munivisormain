import React, { Component } from "react"
import {toast} from "react-toastify"
import { postCopyDocument } from "GlobalUtils/helpers"
import { fetchDocumntsList } from "AppState/actions/AdminManagement/admTrnActions"
import Loader from "./Loader"
import { fetchDocDetails } from "../StateManagement/actions/Transaction"
import CONST from "../../globalutilities/consts"

class CopyCRMDocumentsToTrans extends Component {
  constructor(props) {
    super(props)
    this.state = {
      fileNameList: [],
      documentsList: [],
      selectedFileName: "",
      loading: true
    }
  }

  componentWillMount() {
    const { transaction } = this.props
    fetchDocumntsList(transaction && (transaction.dealIssueTranIssuerId || transaction.actTranClientId || transaction.actIssuerClient || transaction.rfpTranIssuerId), (res)=> {

      if (res && res.status === 200) {
        this.setState({
          loading:false,
          documentsList: res && res.data && res.data.documentsList,
          fileNameList: res && res.data && res.data.fileNameList
        })
      } else {
        this.setState({
          loading:false,
          documentsList: [],
        })
      }
    })
  }

  onSelectDoc = (e) => {
    const { fileNameList } = this.state
    const { name, value } = e.target
    const select = fileNameList && fileNameList.find(f => f._id === value) || {}
    this.setState({
      [name]: value || "",
      selectedFileName: select && select.originalName || ""
    })
  }

  onCopyDocuments = async () => {
    const { selectedDocument, documentsList, selectedFileName } = this.state
    const { user, copyDetails, authEvaluateId, nav1, nav3 } = this.props
    const doc = documentsList && documentsList.find(d => d.docAWSFileLocation === selectedDocument)
    this.setState({
      loading: true
    }, () => {
      fetchDocDetails(doc && doc.docAWSFileLocation).then(async(res) => {

        if(res && res.meta && res.meta.versions && res.meta.versions.length > 1){
          const version = res.meta.versions.find(v => v.originalName === selectedFileName)
          res = {
            ...res,
            meta: {
              ...res.meta,
              versions: [version]
            }
          }
        }

        const tranType = nav1 === "deals"
          ? "Deals"
          : nav1 === "marfp"
            ? "ActMaRFP"
            : nav1 === "derivative"
              ? "Derivatives"
              : nav1 === "loan"
                ? "BankLoans"
                : nav1 === "rfp"
                  ? "RFP"
                  : nav1 === "others"
                    ? "Others"
                    : nav1 === "bus-development"
                      ? "ActBusDev"
                      : ""

        const response = await postCopyDocument({
          oldDoc: res,
          copyDetails,
          user: {
            entityId: user && user.entityId,
            userName: user && `${user.userFirstName} ${user.userLastName}`,
            userId: user && user.userId
          },
          docs: doc,
          tranType,
          authEvaluateId: nav3 === "manageevaluate" && tranType === "RFP" ? authEvaluateId : ""
        })

        if(response && response.done){
          toast("Document Copied successfully", { autoClose: CONST.ToastTimeout, type: toast.TYPE.SUCCESS, })
          this.props.onCopyDocuments(response && response.documentsList || [])
        } else {
          toast(response && response.message || "something went wrong", { autoClose: CONST.ToastTimeout, type: toast.TYPE.ERROR, })
        }

        this.setState({
          loading: false,
          selectedDocument: "",
          selectedFileName: ""
        })

      })
    })

  }



  render(){
    const { loading, fileNameList, selectedDocument } = this.state
    return(
      <div>
        { loading ? <Loader/> : null }

        <div className="columns">
          <div className="column is-3">
            <p className="multiExpLbl">Select Document<span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon" /></span></p>
            <div className="select is-small is-fullwidth is-link">
              <select name="selectedDocument" value={selectedDocument || ""} onChange={this.onSelectDoc} className="is-link">
                <option value="">Pick</option>
                {fileNameList && fileNameList.map((value, index) => <option key={index.toString()} value={value._id}>{value.originalName}</option>)}
              </select>
            </div>
          </div>
        </div>

        <div className="field is-grouped-center">

          <button
            className="button is-link"
            disabled={!selectedDocument}
            onClick={this.onCopyDocuments}
          >
              Copy Document
          </button>

        </div>
      </div>
    )
  }
}

export default CopyCRMDocumentsToTrans
