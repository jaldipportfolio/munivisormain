import React, { Component } from "react"
import { connect } from "react-redux"

import { getMessages } from "../../app/StateManagement/actions"

class Envelope extends Component {
  constructor(props) {
    super(props)
    this.state = { newMessages: false, numMessages: 0 }
    this.pollMessages = this.pollMessages.bind(this)
  }

  componentDidMount() {
    if(!this.interval) {
      this.interval = window.setInterval(this.pollMessages, 60000)
    }
  }

  componentWillReceiveProps(nextProps) {
    const { numMessages } = nextProps
    if(numMessages !== this.props.numMessages) {
      this.setState({ newMessages: true, numMessages })
    } else {
      this.setState({ newMessages: false, numMessages: 0 })
    }
  }

  componentWillUnmount() {
    console.log("clearing interval on unmount of nav")
    window.clearInterval(this.interval)
    this.interval = null
  }

  pollMessages() {
    console.log("in pollMessages : ", !!this.props.token)
    const { token } = this.props
    if(token) {
      this.props.getMessages(token)
    }
  }

  render() {
    // const { newMessages, numMessages } = this.state
    return (
      // <span className={newMessages ? "icon is-small has-text-danger" : "icon is-small"}>
      <span className="icon is-small">
        <i className="fa fa-envelope" />
        {/* {numMessages} */}
      </span>
    )
  }

}

const mapStateToProps = ({ messages }) => ({ numMessages: (messages && messages.length) || 0 })

export default connect(mapStateToProps, { getMessages })(Envelope)
