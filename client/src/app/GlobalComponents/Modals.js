import React from "react"
import {Multiselect} from "react-widgets"
import { emailRegex } from "Validation/common"
import {Modal} from "../FunctionalComponents/TaskManagement/components/Modal"
import DropDownListClear from "./DropDownListClear"
import {TextLabelInput, DocSelectLabelInput} from "./TextViewBox"

export const ConvertToEntityModal = ({ closeModal, state, onChange, changeIssuerFlags, changeLegalRole, changeMarketRole, onSave }) => (
  <Modal
    closeModal={closeModal}
    modalState={state.entityModalState}
    title="Convert Migrated Data"
    saveModal={onSave}
    styleBody={{ minHeight: 300 }}
  >
    {
      state.selectedEnt && state.selectedEnt.length ?
        <div>
          <div className="columns">
            <div className="column">
              <div className="control field">
                <label className="radio">
                  <input
                    className="radio"
                    name="clientOrProspect"
                    type="radio"
                    value="Client"
                    checked={state.clientOrProspect === "Client"}
                    onChange={onChange}/>
                  &nbsp;Client
                </label>
                <label className="radio">
                  <input
                    className="radio"
                    name="clientOrProspect"
                    type="radio"
                    value="Prospect"
                    checked={state.clientOrProspect === "Prospect"}
                    onChange={onChange}/>
                  &nbsp;Prospect
                </label>
                <label className="radio">
                  <input
                    className="radio"
                    name="clientOrProspect"
                    type="radio"
                    value="Third Party"
                    checked={state.clientOrProspect === "Third Party"}
                    onChange={onChange}/>
                  &nbsp;Third Party
                </label>
                {state.error && state.error.clientOrProspect ?
                  <small className="has-text-danger">
                    &nbsp; Please select entity relationship type
                  </small>
                  : undefined
                }
              </div>
            </div>
            <div className="column">
              Selected Migrated Entities : {state.selectedEnt.length}
            </div>
          </div>
          <div className="columns">
            {
              state.clientOrProspect && state.clientOrProspect !== "Third Party" ?
                <div className="column is-one-third">
                  <p className="multiExpLblBlk">
                    Entity Type
                    <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon"/></span>
                  </p>
                  <div className="select is-small is-link">
                    <select value={state.entityType} name="entityType" onChange={onChange}>
                      <option value="">Select Entity Type</option>
                      {state.entityTypes.map(t => (
                        <option key={t.label} value={t.label} disabled={!t.included}>
                          {t.label}
                        </option>
                      ))}
                    </select>
                    {state.error && state.error.entityType
                      ? <p className="has-text-danger">
                        Entity type should not be empty
                      </p>
                      : undefined
                    }
                  </div>
                </div>
                : null
            }
            {state.clientOrProspect !== "Third Party" && ["Governmental Entity / Issuer", "501c3 - Obligor"].includes(state.entityType)
              ? <div className="column">
                <p className="multiExpLblBlk">Issuer Type</p>
                <Multiselect
                  data={state.issuerFlagsList}
                  disabled={state.disabledIssuerFlags || []}
                  placeholder=" Select issuer types"
                  caseSensitive={false}
                  minLength={1}
                  value={state.issuerFlags}
                  filter="contains"
                  onChange={changeIssuerFlags}/>
                {state.error && state.error.issuerFlags ?
                  <p className="has-text-danger">
                    {state.error.issuerFlags}
                  </p> : undefined}
              </div>
              : undefined
            }
          </div>
          {state.clientOrProspect === "Third Party" ?
            <div>
              <div className="columns">
                <div className="column">
                  <p className="multiExpLblBlk">Market Role</p>
                  <Multiselect
                    data={state.marketRolesList}
                    disabled={state.disabledMarketRole || []}
                    placeholder=" Select market role"
                    caseSensitive={false}
                    minLength={1}
                    value={state.marketRole}
                    filter="contains"
                    onChange={changeMarketRole}
                  />
                  {state.error && state.error.marketRole ?
                    <p className="has-text-danger">
                      {state.error.marketRole}
                    </p>
                    : undefined
                  }
                </div>
              </div>
              <div className="columns">
                {
                  state.marketRole.includes("Legal") ?
                    <div className="column">
                      <p className="multiExpLblBlk">Legal Type</p>
                      <Multiselect
                        data={state.legalTypes}
                        disabled={state.disabledLegalTypes || []}
                        placeholder=" Select legal types"
                        caseSensitive={false}
                        minLength={1}
                        value={state.legalRole}
                        filter="contains"
                        onChange={changeLegalRole}
                      />
                      {state.error && state.error.legalRole ?
                        <p className="has-text-danger">
                          {state.error.legalRole}
                        </p>
                        : undefined
                      }
                    </div>
                    : undefined
                }
              </div>
            </div>
            : null
          }
        </div>
        : <div className="subtitle is-grouped-center"> Please Select at list one migrated entity  </div>
    }
  </Modal>
)

export const ConfirmationModal = ({ message, closeModal, modalState, onConfirmed }) => {
  if (!modalState) {
    return null
  }
  return (
    <div className="modal is-active">
      <div
        className="modal-background"
        onClick={closeModal}
        role="presentation"
        onKeyPress={() => { }}
      />
      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-title">Confirmation!</p>
        </header>
        <section className="modal-card-body">
          <div className="content">{message}</div>
        </section>
        <footer className="modal-card-foot">
          <div className="field is-grouped-center">
            <button className="button is-link" onClick={onConfirmed}>
              Yes
            </button>
            <button className="button is-light" onClick={closeModal}>
              No
            </button>
          </div>
        </footer>
      </div>
    </div>
  )
}

export const UserAddToEntityModal = ({ closeModal, state, list, errors, errorResolved, onChangeItem, onSave }) => {
  const onChange = (selectItem) => {
    onChangeItem({ addUserInEnt: selectItem })
  }

  const handleInputChange = (e) => {
    const {name, value, id} = e.target
    const obj = state[id] || {}
    obj[name] = value
    onChangeItem({
      [id]: obj
    })
  }

  const onSaveValidation = () => {
    const {entityList, selectedUsers, allUserEmails} = state
    let errorResolved = {}
    let resolvedObj = {}

    const users = entityList.filter(u => selectedUsers.indexOf(u._id) !== -1)
    let userEmails = []

    users.forEach(u => {
      const uEmails = u.emailAddresses.map(email => email.address)
      userEmails = [...uEmails, ...userEmails]
    })
    state.errors.forEach(err => {
      const keys = Object.keys(err).filter(ek => ek.includes("Email") === true)
      keys.forEach(key => {
        const regexString = new RegExp(emailRegex, "g")
        if(state[err.id] && state[err.id][key] && regexString.test(state[err.id][key] || "")){
          userEmails.push(state[err.id][key])
        }
      })
    })

    state.errors.forEach(err => {
      const keys = Object.keys(err).filter(ek => ["User Name", "id"].indexOf(ek) === -1)
      const textObj = state[err.id] || keys.map(key => ({ [key]: `${key} should not be empty` }))
      const error = {}
      keys.forEach(key => {
        if(err.hasOwnProperty(key)){ // eslint-disable-line
          if(!textObj[key]) {
            error[key] = `${key} should not be empty`
          }else if(key.includes("Email")){
            const regexString = new RegExp(emailRegex, "g")
            const emailDup = userEmails.filter(e => e.toLowerCase() === textObj[key].toLowerCase())

            if(!regexString.test(textObj[key] || "")) {
              error[key] = "Enter valid email"
            }else if(emailDup.length > 1){
              error[key] = "Email must be unique"
            }else if(allUserEmails.indexOf(textObj[key]) !== -1){
              error[key] = "Email already exists in system"
            }
          }else {
            delete error[key]
          }
        }
      })
      if(Object.keys(error).length){
        errorResolved = {
          ...errorResolved,
          [err.id]: error
        }
      }else {
        resolvedObj = {
          ...resolvedObj,
          [err.id]: state[err.id]
        }
      }
    })

    console.log("Error Resolved : ", errorResolved)

    if(Object.keys(errorResolved).length){
      onChangeItem({errorResolved})
    }else {
      onChangeItem({errorResolved})
      console.log("Resolved Obj", resolvedObj)
      onSave(resolvedObj)
    }
  }

  return (
    <Modal
      closeModal={closeModal}
      modalState={state.userAddToEntityModal}
      title="Contact add to entity"
      saveModal={onSaveValidation}
      disabled={!state.addUserInEnt}
      styleBody={{ minHeight: "calc(100vh - 30vh)" }}
      modalWidth={{ width: "100%", overflow: "auto" }}
    >
      <div className="columns">
        <div className="column">
          <div>
            <p className="multiExpLbl">Entity Name
              <span className='icon has-text-danger'>
                <i className='fas fa-asterisk extra-small-icon'/>
              </span>
            </p>
            <div className="control">
              <div style={{ width: 300, fontSize: 12 }}>
                <DropDownListClear
                  filter="contains"
                  value={state.addUserInEnt || ""}
                  data={list || []}
                  message="select issuer"
                  textField="name"
                  valueField="id"
                  key="name"
                  groupBy={({ relType }) => relType}
                  onChange={onChange}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      {errors.length ?
        <div>
          <span className="has-text-danger has-text-weight-bold text-left">Errors</span>
        </div>
        : <div>
          <span className="has-text-success has-text-centered">All data are valid</span>
        </div> }
      <div className="columns">
        {
          errors.map((err,i) => (
            <div key={i.toString()} className="column is-3">
              <ul style={{border: "2px solid rgb(208, 208, 208)", padding: 10, marginTop: 5, borderRadius: 5, boxShadow: "rgba(191, 191, 191, 0.52) 2px 1px 3px 0px"}}>
                {Object.keys(err).map(errKey => {
                  if(errKey === "id"){
                    return null
                  }
                  return (
                    <li key={errKey} style={{ marginBottom: 20 }}>
                      {errKey === "User Name" ? <small className="multiExpLblBlk">{errKey}: {err[errKey]}</small> : <p className="has-text-info is-size-7">{err[errKey]}</p>}
                      <div className="columns">
                        {
                          errKey.includes("Email") ?
                            <TextLabelInput
                              label="Email"
                              title="Email"
                              name={errKey}
                              id={err.id}
                              value={(state[err.id] && state[err.id][errKey]) || ""}
                              error={(errorResolved && errorResolved[err.id] && errorResolved[err.id][errKey]) || ""}
                              placeholder="johnsmith@example.com"
                              onChange={handleInputChange}
                              required
                            />
                            : null
                        }
                        {
                          errKey === "First Name" ?
                            <TextLabelInput
                              label="First Name"
                              title="First Name"
                              placeholder="First Name"
                              name={errKey}
                              id={err.id}
                              value={(state[err.id] && state[err.id][errKey]) || ""}
                              error={(errorResolved && errorResolved[err.id] && errorResolved[err.id][errKey]) || ""}
                              required
                              onChange={handleInputChange}
                            />
                            : null
                        }
                        {
                          errKey === "Last Name" ?
                            <TextLabelInput
                              label="Last Name"
                              title="Last Name"
                              placeholder="Last Name"
                              name={errKey}
                              id={err.id}
                              value={(state[err.id] && state[err.id][errKey]) || ""}
                              error={(errorResolved && errorResolved[err.id] && errorResolved[err.id][errKey]) || ""}
                              required
                              onChange={handleInputChange}
                            />
                            : null
                        }
                      </div>
                    </li>
                  )
                })
                }
              </ul>
            </div>
          ))
        }
      </div>
    </Modal>
  )
}

export const ChangeUserFirmModal = ({ closeModal, entityModalState, relatedEntity, changeRelatedEntity, relatedEntityList, onSave }) => (
  <Modal
    closeModal={closeModal}
    modalState={entityModalState}
    title="Change Associated Entity"
    saveModal={onSave}
    styleBody={{ minHeight: 300 }}
    disabled={relatedEntity && !relatedEntity.length || false}
    buttonName="Change Entity Name"
  >
    <div className="column">
      <p className="multiExpLbl">Select Entity Name</p>
      <Multiselect
        data={relatedEntityList || []}
        placeholder="Search & select entities"
        textField="name"
        caseSensitive={false}
        minLength={1}
        value={relatedEntity}
        filter="contains"
        groupBy="group"
        onChange={changeRelatedEntity}
      />
    </div>
  </Modal>
)

export const MergeEntitiesModal = ({
  closeModal,
  mergeEntitiesModal,
  onSave,
  selectedEnt,
  value,
  state,
  onChange,
  onChangeParentChange,
  changeIssuerFlags,
  changeLegalRole,
  changeMarketRole,
  entityRelationshipType,
  listType
}) => (
  <Modal
    closeModal={closeModal}
    modalState={mergeEntitiesModal}
    title="Merge Entities"
    saveModal={onSave}
    styleBody={{ minHeight: 300 }}
    disabled={selectedEnt.length < 2}
  >

    {
      selectedEnt && selectedEnt.length > 1 ?
        <div>
          <div className="columns">
            <div className="column">
              <p className="multiExpLblBlk">
                Which is the entity that will be the parent ?
                <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon"/></span>
              </p>

              <DocSelectLabelInput
                list={selectedEnt && selectedEnt.map(s => s.entityName.trim().replace(/\s+/g, " ")) || []}
                // name="rfpTranSecurityType"
                value={value && value.trim().replace(/\s+/g, " ") || ""}
                onChange={onChangeParentChange}
                error={state && state.error && state.error.parentEntityId}
              />
            </div>
          </div>

          { listType === "migratedentities" ?
            <div className="columns">
              <div className="column">
                <div className="control field">
                  <label className="radio">
                    <input
                      className="radio"
                      name="clientOrProspect"
                      type="radio"
                      value="Client"
                      checked={state.clientOrProspect === "Client"}
                      onChange={onChange}/>
                    &nbsp;Client
                  </label>
                  <label className="radio">
                    <input
                      className="radio"
                      name="clientOrProspect"
                      type="radio"
                      value="Prospect"
                      checked={state.clientOrProspect === "Prospect"}
                      onChange={onChange}/>
                    &nbsp;Prospect
                  </label>
                  <label className="radio">
                    <input
                      className="radio"
                      name="clientOrProspect"
                      type="radio"
                      value="Third Party"
                      checked={state.clientOrProspect === "Third Party"}
                      onChange={onChange}/>
                    &nbsp;Third Party
                  </label>
                  {state.error && state.error.clientOrProspect ?
                    <p className="is-small text-error has-text-danger">
                      &nbsp; Please select entity relationship type
                    </p>
                    : undefined
                  }
                </div>
              </div>
            </div> : null
          }
          <div className="columns">
            {
              state.clientOrProspect && state.clientOrProspect !== "Third Party" ?
                <div className="column is-one-third">
                  <p className="multiExpLblBlk">
                    Entity Type
                    <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon"/></span>
                  </p>
                  <div className="select is-small is-link">
                    <select value={state.entityType} name="entityType" onChange={onChange}>
                      <option value="">Select Entity Type</option>
                      {state.entityTypes.map(t => (
                        <option key={t.label} value={t.label} disabled={!t.included}>
                          {t.label}
                        </option>
                      ))}
                    </select>
                    {state.error && state.error.entityType
                      ? <p className="is-small text-error has-text-danger">
                        Entity type should not be empty
                      </p>
                      : undefined
                    }
                  </div>
                </div>
                : null
            }
            {state.clientOrProspect !== "Third Party" && ["Governmental Entity / Issuer", "501c3 - Obligor"].includes(state.entityType)
              ? <div className="column">
                <p className="multiExpLblBlk">Issuer Type</p>
                <Multiselect
                  data={state.issuerFlagsList}
                  disabled={state.disabledIssuerFlags || []}
                  placeholder=" Select issuer types"
                  caseSensitive={false}
                  minLength={1}
                  value={state.issuerFlags}
                  filter="contains"
                  onChange={changeIssuerFlags}/>
                {state.error && state.error.issuerFlags ?
                  <p className="is-small text-error has-text-danger">
                    {state.error.issuerFlags}
                  </p> : undefined}
              </div>
              : undefined
            }
          </div>
          {state.clientOrProspect === "Third Party" ?
            <div>
              <div className="columns">
                <div className="column">
                  <p className="multiExpLblBlk">Market Role</p>
                  <Multiselect
                    data={state.marketRolesList}
                    disabled={state.disabledMarketRole || []}
                    placeholder=" Select market role"
                    caseSensitive={false}
                    minLength={1}
                    value={state.marketRole}
                    filter="contains"
                    onChange={changeMarketRole}
                  />
                  {state.error && state.error.marketRole ?
                    <p className="is-small text-error has-text-danger">
                      {state.error.marketRole}
                    </p>
                    : undefined
                  }
                </div>
              </div>
              <div className="columns">
                {
                  state.marketRole.includes("Legal") ?
                    <div className="column">
                      <p className="multiExpLblBlk">Legal Type</p>
                      <Multiselect
                        data={state.legalTypes}
                        disabled={state.disabledLegalTypes || []}
                        placeholder=" Select legal types"
                        caseSensitive={false}
                        minLength={1}
                        value={state.legalRole}
                        filter="contains"
                        onChange={changeLegalRole}
                      />
                      {state.error && state.error.legalRole ?
                        <p className="is-small text-error has-text-danger">
                          {state.error.legalRole}
                        </p>
                        : undefined
                      }
                    </div>
                    : undefined
                }
              </div>
            </div>
            : null
          }
        </div>
        : <div className="subtitle is-grouped-center"> Please Select at list two {listType === "migratedentities" ? "Migrated" : listType} entities  </div>
    }
  </Modal>
)

export const AttachDocsWithTransactions = ({ closeModal, modalState, dropDown, onCategoryChanges, value, type, selectType, selectedFiles, path, onChangeSelected, onSave }) => (
  <Modal
    closeModal={closeModal}
    modalState={modalState}
    title={`Attach ${selectType === "folder" ? "Folder" : "Documents"} With ${type}`}
    saveModal={onSave}
    styleBody={{ minHeight: 300 }}
    modalWidth={{ width: "50%", height: "70%" }}
    disabled={!(dropDown && dropDown.selected && dropDown.selected.length)}
  >
    <div>
      <div className="columns">
        <div className="column">
          <span>
            <p className="multiExpLbl">Selected {selectType === "file" ? "Documents" : "Folder"}:</p> {
              selectType === "file" ? selectedFiles.map((s, i) => `${s.split("/").pop()}${selectedFiles.length === i + 1 ? "" : ",   " }`) : path.split("/").pop()
            }
          </span>
        </div>
      </div>
      <div className="columns">
        <div className="column">
          <p className="multiExpLbl">Select {type} <span className="icon has-text-danger"><i className="fas fa-asterisk extra-small-icon"/></span></p>
          { type === "Transactions" ?
            <Multiselect
              data={dropDown && dropDown.transactionsName || []}
              placeholder="Search & Select Transactions"
              textField="name"
              caseSensitive={false}
              minLength={1}
              value={dropDown && dropDown.selected || []}
              filter="contains"
              groupBy="type"
              onChange={onChangeSelected}
            /> :
            <Multiselect
              data={dropDown && dropDown.relatedEntityList || []}
              placeholder="Search & Select Entities"
              textField="name"
              caseSensitive={false}
              minLength={1}
              value={dropDown && dropDown.selected || []}
              filter="contains"
              groupBy="type"
              onChange={onChangeSelected}
            />
          }
        </div>
      </div>
      { selectType !== "folder" ?
        <div className="columns">
          <div className="column">
            <p className="multiExpLbl">Category</p>
            <DropDownListClear
              filter
              textField="label"
              valueField={a => a}
              data={dropDown.category || []}
              value={value.docCategory || ""}
              isHideButton={value.docCategory}
              onChange={value => onCategoryChanges("docCategory", value)}
              onClear={() => {
                onCategoryChanges("docCategory", "")
              }}
            />
          </div>
          <div className="column">
            <p className="multiExpLbl">Sub-category</p>
            <DropDownListClear
              filter
              textField="label"
              valueField={a => a}
              data={(value && value.docCategory && dropDown.subCategory[value.docCategory]) || []}
              disabled={(value && value.docCategory && dropDown.subCategoryDisabled[value.docCategory]) || []}
              value={value.docSubCategory || ""}
              isHideButton={value.docSubCategory}
              onChange={value => onCategoryChanges("docSubCategory", value)}
              onClear={() => {
                onCategoryChanges("docSubCategory", "")
              }}
            />
          </div>
          <div className="column">
            <p className="multiExpLbl">Type</p>
            <DropDownListClear
              filter
              textField="label"
              valueField={a => a}
              data={dropDown.docType || []}
              disabled={dropDown.disabledDocType || []}
              value={value.docType || ""}
              isHideButton={value.docType}
              onChange={value => onCategoryChanges("docType", value)}
              onClear={() => {
                onCategoryChanges("docType", "")
              }}
            />
          </div>
        </div> : null
      }
    </div>
  </Modal>
)
