import React from "react"

const NotAvailable = () => (
  <div id="main">
    <h4>The page you are trying to access is not available.</h4>
  </div>
)

export default NotAvailable
