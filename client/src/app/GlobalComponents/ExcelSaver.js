import React, { Component } from "react"
import {postJSONData} from "../../../src/app/StateManagement/actions/Common"

class ExcelSaver extends Component {

  getSample = async () => {
    const { jsonSheet, label } = this.props
    if(jsonSheet && jsonSheet.length) {
      const headers = Object.keys(jsonSheet[0])
      const payload = {
        dataArray: [{
          name: label || "Sample excel",
          data: jsonSheet,
          headers,
        }]
      }
      await postJSONData(payload, label)
    }

  }

  render() {
    return (
      <a className="is-size-7" onClick={this.getSample}>{this.props.label || "Sample excel"}</a>
    )
  }

}

export default ExcelSaver
