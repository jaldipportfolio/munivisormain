import React from "react"
import ReactTable from "react-table"
import moment from "moment"

export const ContactList = ({rowData}) => (
  <ReactTable
    minRows={2}
    defaultPageSize={10}
    className="-striped -highlight is-bordered"
    data={rowData}
    columns={[
      {
        id: "displayName",
        Header: "Name",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              {(item && item.displayName )|| ""}
            </div>
          )
        }
      },
      {
        id: "emailAddresses",
        Header: "Email Addresses",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              <ul className="list-unstyled">
                {
                  ((item && item.emailAddresses) || []).map((email,i) => (
                    <li key={i.toString()} className="border p-1">
                      {email.address}
                    </li>
                  ))
                }
              </ul>
            </div>
          )
        }
      },
      {
        id: "createdDateTime",
        Header: "Created Date Time",
        className: "multiExpTblVal",
        accessor: item => item,
        Cell: row => {
          const item = row.value
          return (
            <div className="hpTablesTd wrap-cell-text">
              {(item && item.createdDateTime ? moment(item.createdDateTime).format("MM.DD.YYYY hh:mm a") : "" )}
            </div>
          )
        }
      }
    ]}
  />
)
