import React from "react"
import DropDownListClear from "./DropDownListClear"


const DocumentsView = ({
  doc = {},
  staticField,
  index,
  onChangeItem,
  errors = {},
  onRemove,
  chooseMultiple,
  onChangeError,
  clickUpload,
  onClickUpload,
  onChangeUpload,
  newDropDown,
  tabIndex,
  onSelectRows,
  selectedRows,
}) => {
  const onChangeSelect = (name, value) => {
    const names =
      name === "docCategory" ? "Category" : name === "docSubCategory"
        ? "Sub-category" : name === "docType"
          ? "Type" : name
    if (name === "docCategory") {
      doc.docSubCategory = ""
    }
    onChangeItem(
      {
        ...doc,
        [name]: value !== "" ? value : ""
      },
      `user ${names} set to ${value !== "" ? value : ""}`,
      name
    )
    if (name && value) {
      onChangeError(name, "documents", index)
    }
  }

  const onFileBtnClick = (e, index) => {
    if(e.keyCode === 13 || e.keyCode === 32){
      clickUpload(index)
    }
  }

  const onDeleteBtnClick = (e) =>{
    if(e.keyCode === 13){
      onRemove(index)
    }
  }

  const onSelect = (e) => {
    const { value } = e.target
    const findIndex = selectedRows.indexOf(parseInt(value,10))
    if(findIndex !== -1){
      selectedRows.splice(findIndex, 1)
    }else {
      selectedRows.push(parseInt(value,10))
    }
    onSelectRows(selectedRows)
  }

  return (
    <tbody className="is-marginless is-paddingless doc-view">
      <tr>
        <td>
          <div
            className="field is-grouped"
            style={{
              justifyContent: "start"
            }}>
            <div className="control">
              <div className="hpTablesTd wrap-cell-text" style={{justifyContent: "center", display: "flex"}}>
                <label className="checkbox-button" style={{paddingLeft: 5, marginTop: 5}}>
                  <input
                    type="checkbox"
                    name="all"
                    value={index}
                    style={{position: "inherit"}}
                    checked={selectedRows.indexOf(index) !== -1}
                    disabled={index === 0}
                    onChange={onSelect}
                  />
                  <span className="checkmark" style={{border: "1px solid black"}}/>
                </label>
              </div>
            </div>
          </div>
        </td>
        <td>
          {staticField && staticField.docCategory ? (
            <small>{doc.docCategory || ""}</small>
          ) : (
            <div className="complain-details">
              <DropDownListClear
                filter
                tabIndex={tabIndex + 1}
                textField="label"
                valueField={a => a}
                data={newDropDown.category || []}
                disabled={newDropDown.disabledCategory || []}
                value={doc.docCategory || ""}
                isHideButton={doc.docCategory}
                onChange={value => onChangeSelect("docCategory", value)}
                onClear={() => {onChangeSelect("docCategory", "")}}
              />
              {errors && errors.docCategory && (
                <small className="text-error">{errors.docCategory}</small>
              )}
            </div>
          )}
        </td>

        <td>
          {staticField && staticField.docSubCategory ? (
            <small>{doc.docSubCategory || ""}</small>
          ) : (
            <div className="complain-details">
              <DropDownListClear
                filter
                tabIndex={tabIndex + 2}
                textField="label"
                valueField={a => a}
                data={(doc.docCategory && newDropDown.subCategory[doc.docCategory]) || []}
                disabled={(doc.docCategory && newDropDown.subCategoryDisabled[doc.docCategory]) || []}
                value={doc.docSubCategory || ""}
                isHideButton={doc.docSubCategory}
                onChange={value => onChangeSelect("docSubCategory", value)}
                onClear={() => {onChangeSelect("docSubCategory", "")}}
              />
              {errors && errors.docSubCategory && (
                <small className="text-error">{errors.docSubCategory}</small>
              )}
            </div>
          )}
        </td>

        <td>
          {staticField && staticField.docType ? (
            <small>{doc.docType || ""}</small>
          ) : (
            <div className="complain-details">
              <DropDownListClear
                filter
                tabIndex={tabIndex + 3}
                textField="label"
                valueField={a => a}
                data={newDropDown.docType || []}
                disabled={newDropDown.disabledDocType || []}
                value={doc.docType || ""}
                isHideButton={doc.docType}
                onChange={value => onChangeSelect("docType", value)}
                onClear={() => {onChangeSelect("docType", "")}}
              />
              {errors.docType ? (
                <p className="text-error">{errors.docType}</p>
              ) : null}
            </div>
          )}
        </td>
        <td>
          <div style={{display: "table-cell"}}>
            <input
              type="file"
              style={{ display: "none" }}
              onClick={onClickUpload}
              id={index}
              onChange={(event) => onChangeUpload(event, index)}
              disabled={chooseMultiple}
            />
            <div
              tabIndex={tabIndex + 4}
              className={`file is-small ${chooseMultiple ? "disabled" : ""}`}
              style={{ cursor: chooseMultiple ? "not-allowed" : "pointer" }}
              onClick={() => clickUpload(index)}
              onKeyDown={(e)=> onFileBtnClick(e, index)}
            >
              <span className="file-cta">
                <span className="file-icon">
                  <i className="fas fa-upload" />
                </span>
                <span className="file-label">
                 Choose a file…
                </span>
              </span>
            </div>
          </div>
          {errors.docFileName && <p className="text-error">{errors.docFileName}</p>}
        </td>
        <td>
          <div className="control">
            <div className="multiExpTblVal">
              {doc.docFileName}
            </div>
          </div>
        </td>
        <td>
          <div className="field is-grouped">
            <div className="control">
              <a tabIndex={tabIndex + 5} onKeyDown={onDeleteBtnClick} onClick={() => onRemove(index)}> {/* eslint-disable-line */}
                <span className="has-text-link">
                  <i className="far fa-trash-alt" title="Delete"/>
                </span>
              </a>
            </div>
          </div>
        </td>
      </tr>
    </tbody>
  )
}

export default DocumentsView
