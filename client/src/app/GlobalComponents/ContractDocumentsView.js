import React from "react"
import {DocDropDownSelect} from "./TextViewBox"
import SingleFileDocUpload from "./SingleFileDocUpload"

const ContractDocumentsView = ({ doc = {}, staticField, tranId, user, contextType, contextId, index, dropDown, bucketName, getBidBucket, onChangeItem, errors = {}, tabIndex, onFileSetInState, onUploadSuccess, docFile }) => {

  const onChangeSelect = (name, value) => {
    if (name === "docCategory") {
      doc.docSubCategory = ""
    }
    onChangeItem({
      ...doc,
      [name]: value !== null ? value.value : null,
    }, `user doc type set to ${value !== null ? value.value : null}`)
  }

  return (
    <tbody className="is-marginless is-paddingless">
      <tr>
        <td>
          {staticField && staticField.docCategory ? <small>{doc.docCategory || ""}</small> :
            <div className="complain-details">
              <DocDropDownSelect name="category" tabIndex={tabIndex} data={dropDown.category || []} value={doc.docCategory || ""}
                onChange={(value) => onChangeSelect("docCategory", value)} />
              {errors && errors.docCategory && <small className="text-error">{errors.docCategory}</small>}
            </div>
          }
        </td>
        <td>
          {staticField && staticField.docSubCategory ? <small>{doc.docSubCategory || ""}</small> :
            <div className="complain-details">
              <DocDropDownSelect name="subcategory" tabIndex={tabIndex + 1} data={(doc.docCategory && dropDown.multiSubCategory[doc.docCategory]) || []} value={doc.docSubCategory || ""}
                onChange={(value) => onChangeSelect("docSubCategory", value)} defaultValue={doc.docSubCategory || ""} />
              {errors && errors.docSubCategory && <small className="text-error">{errors.docSubCategory}</small>}
            </div>
          }
        </td>
        <td>
          {
            staticField && staticField.docType ? <small>{doc.docType || ""}</small> :
              <div className="complain-details">
                <DocDropDownSelect name="doctype" tabIndex={tabIndex + 2} data={dropDown.docType || []} value={doc.docType || ""}
                  textField="name" valueField="id" key="name" onChange={(value) => onChangeSelect("docType", value)} />
                {errors.docType ? <p className="text-error">{errors.docType}</p> : null}
              </div>
          }
        </td>
        <td>
          {/* <DocUpload bucketName={bucketName} docMeta={{ category: doc.docCategory, subCategory: doc.docSubCategory, type: doc.docType }}
            versionMeta={{ uploadedBy: doc.createdUserName }} tabIndex={tabIndex + 3}
            onUploadSuccess={getBidBucket} showFeedback
            bidIndex={index} contextId={tranId || contextId} contextType={contextType || ""}
            tenantId={user.entityId} disabled={!(doc.docCategory || doc.docType)} />
          {errors.docAWSFileLocation && <p className="text-error">{errors.docAWSFileLocation}</p>} */}
          <SingleFileDocUpload
            bucketName={bucketName}
            docFile={docFile}
            onFileSetInState={onFileSetInState}
            onUploadSuccess={onUploadSuccess}
            versionMeta={{ uploadedBy: `${user.userFirstName} ${user.userLastName}` }}
            contextType={contextType || ""}
            tenantId={user.entityId}
            contextId={tranId || contextId}
            user={user}
            disabled={!(doc.docCategory || doc.docType)}
          />
          {errors.docFileName && <p className="text-error">{errors.docFileName}</p>}
        </td>
        <td>
          { doc.docFileName || "" }
        </td>
      </tr>
      <tr>
        <td colSpan={5}>
          <small>Note: Please select Category or Type for Upload Document</small>

        </td>
      </tr>
    </tbody>
  )
}

export default ContractDocumentsView
