import Joi from "joi-browser"
import dateFormat from "dateformat"

const taskAssignees = Joi.object().keys({
  _id:Joi.string().required().label("Assignee Id").optional(),
  entityId : Joi.string().required().label("EntityId").optional(),
  userEntityName : Joi.string().required().label("UserEntityName").optional(),
  userId : Joi.string().required().label("UserId Id").optional(),
  userFirstName : Joi.string().required().label("UserFirstName").optional(),
  userLastName : Joi.string().required().label("UserLastName").optional(),
  
})


const taskRelatedEntities = Joi.object().keys({
  firmName:Joi.string().required().label("FirmName").optional(),
  msrbFirmName:Joi.string().required().label("FsrbFirmName").optional(),
  participantEntityId:Joi.string().required().label("ParticipantEntityId").optional(),
  tranId:Joi.string().required().label("TranId").optional(),
  activityId : Joi.string().required().label("TranId").optional(),
  activityName : Joi.string().required().label("TranId").optional(),
  activityType : Joi.string().required().label("TranId").optional(),
  entityId : Joi.string().required().label("TranId").optional()
})

const taskRelatedActivities = Joi.object().keys({
  activityDescription:Joi.string().required().label("ActivityDescription").optional(),
  activityId: Joi.string().required().label("ActivityId").optional(),
  activityStatus: Joi.string().required().label("ActivityStatus").optional(),
  activitySubType: Joi.string().required().label("ActivitySubType").optional(),
  activityTranClientId: Joi.string().required().label("ActivityTranClientId").optional(),
  activityTranClientName: Joi.string().required().label("ActivityTranClientName").optional(),
  activityTranFirmId: Joi.string().required().label("ActivityTranFirmId").optional(),
  activityTranFirmName: Joi.string().required().label("ActivityTranFirmName").optional(),
  activityType : Joi.string().required().label("ActivityType").optional(),
  canEditTran : Joi.boolean().required().optional(),
  canViewTran : Joi.boolean().required().optional(),
  faEntityId : Joi.string().required().label("FaEntityId").optional(),
  loggedInUserEntityId : Joi.string().required().label("LoggedInUserEntityId").optional(),
  loggedInUserId: Joi.string().required().label("LoggedInUserId").optional(),
  tranId: Joi.string().required().label("TranId").optional()
})
const taskMgtSchema = Joi.object().keys({
  _id:Joi.string().required().label("Task Id").optional(),
  taskName:Joi.string().required().label("Task Name").optional(),
  taskNotes:Joi.string().allow("").label("Task Notes").optional(),
  userTaskFollowing:Joi.boolean().required().label("Following").optional(),
  taskType:Joi.string().required().label("Task Type").optional(),
  taskPriority:Joi.string().required().label("Task Priority").optional(),
  taskStartDate: Joi.date().example(new Date("2005-01-01")).required().label("Start Date").optional(),
  taskDueDate: Joi.date().example(new Date("2016-01-01")).min(Joi.ref("taskStartDate")).required().label("Due Date").optional(), // switched order of min and example
  taskAssignees:Joi.array().items(taskAssignees).min(1).unique().required().label("Assignee").optional(),
  taskRelatedActivities:Joi.any().when("taskType", { is: "general", then: Joi.optional(), otherwise: Joi.array().items(taskRelatedActivities).min(1).unique().required().label("Related Activity") }).optional(),
  taskRelatedEntities: Joi.any().when("taskType", { is: "general", then: Joi.optional(), otherwise: Joi.array().items(taskRelatedEntities).min(1).unique().required().label("Related Entity")}).optional(),
})

export const validateTaskMgt = (inputTaskMgt) => {
  const result = Joi.validate(inputTaskMgt, taskMgtSchema, { convert: true, abortEarly: false, stripUnknown: false })
  return result
}
