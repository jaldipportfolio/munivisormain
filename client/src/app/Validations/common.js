import Joi from "joi-browser"

export const emailRegex = /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/i

export const crmSchema = {
  emailId: {
    emailId: Joi.string().email().regex(emailRegex)
  }
}

export const validateValue = (schema={}, type, value) => Joi.validate(
  { [type]: value }, schema[type]
)
