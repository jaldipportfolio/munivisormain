import moment from "moment"

export default (date) => {
  if (!date || date==="" || date==="-" || date==="+275760-09-13T00:00:00.000Z") {
    return "-"
  }
  return moment(date).format("MM-DD-YYYY")
}
