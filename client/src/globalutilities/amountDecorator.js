/* eslint-disable indent, prefer-template */
export default item =>
  Math.ceil(
    item.dealIssueParAmount ||
      (item.bankLoanTerms || {}).parAmount ||
      (item.derivativeSummary || {}).tranNotionalAmt
  ) || 0

export const unitConvertor = labelValue =>
  // Nine Zeroes for Billions
  Math.abs(Number(labelValue)) >= 1.0e9
    ? (Math.abs(Number(labelValue)) / 1.0e9).toFixed(2) + "B"
    : // Six Zeroes for Millions
    Math.abs(Number(labelValue)) >= 1.0e6
    ? (Math.abs(Number(labelValue)) / 1.0e6).toFixed(2) + "M"
    : // Three Zeroes for Thousands
    Math.abs(Number(labelValue)) >= 1.0e3
    ? (Math.abs(Number(labelValue)) / 1.0e3).toFixed(2) + "K"
    : Math.abs(Number(labelValue)).toFixed(2)
