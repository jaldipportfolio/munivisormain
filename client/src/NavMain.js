import "react-widgets/dist/css/react-widgets.css"
import "react-select/dist/react-select.css"
import "react-table/react-table.css"
import "react-phone-number-input/style.css"

import React, { Component, Suspense } from "react"
import { Link, withRouter } from "react-router-dom"
import { connect } from "react-redux"
import { toast } from "react-toastify"

import CONST, { navLabels, PUBLIC_ROUTES } from "GlobalUtils/consts"


import GlobalSignUp from "Auth/GlobalSignUp"
import GlobalSignIn from "Auth/GlobalSignIn"
import {
  checkNavAccessEntitlrment
} from "GlobalUtils/helpers"
import STPLogin from "Global/STPLogin"
import NavSearch from "./app/FunctionalComponents/Search/NavigationSearch"
import {
  getNavOptions,
  checkAuth,
  signOut
} from "./app/StateManagement/actions"
import "./public/images/munivisor.png"
import { DevMainContainer } from "./DevMainContainer"
import RequestToken from "./app/LoginAuthentication/RequestToken"
import Loader from "./app/GlobalComponents/Loader"

const AttachChecklist = React.lazy(() => import("Global/AttachChecklist"))
const ProcessChecklistTest = React.lazy(() => import("Global/ProcessChecklistTest"))

const AuditLog = React.lazy(() => import("Global/AuditLog"))
const Messages = React.lazy(() => import("Global/Messages"))
const UserProfile = React.lazy(() => import("Global/UserProfile"))
const BulmaModalCard = React.lazy(() => import("Global/BulmaModal"))
const PicklistsTest = React.lazy(() => import("Global/PicklistsTest"))
const UpdateTagsTest = React.lazy(() => import("Global/UpdateTagsTest"))
const NotAvailable = React.lazy(() => import("Global/NotAvailable"))
const EventsCalendar = React.lazy(() => import("Task/components/EventsCalendar"))
const TaskMain = React.lazy(() => import("Task/TaskMain"))

const Firms = React.lazy(() => import("Admin/Firms/FirmsMain"))
const Users = React.lazy(() => import("Admin/Users/UsersMain"))
const AdminUsersListTabsView = React.lazy(() => import("Admin/Users/containers/AdminUsersListTabsView"))
const AdminClientFirmTabsView = React.lazy(() => import("Admin/Clients/containers/AdminClientFirmTabsView"))
const AdminThirdPartyTabsView = React.lazy(() => import("Admin/Third-Party/containers/AdminThirdPartyTabsView"))



const ClientsProspects = React.lazy(() => import("./app/FunctionalComponents/ClientsProspects/ClientsProspects"))
const ThirdPartyNew = React.lazy(() => import("./app/FunctionalComponents/ThirdParty/ThirdParty"))
const UsersNew = React.lazy(() => import("./app/FunctionalComponents/Users/Users"))

const ChooseActivity = React.lazy(() => import("./app/FunctionalComponents/BusinessProcesses/components/ChooseActivity"))
const ChooseEntity = React.lazy(() => import("./app/FunctionalComponents/BusinessProcesses/components/ChooseEntity"))
const ChooseAdmin = React.lazy(() => import("./app/FunctionalComponents/BusinessProcesses/components/ChooseAdmin"))

const DocsMain = React.lazy(() => import("./app/FunctionalComponents/docs/DocsMain"))
const DocUpload = React.lazy(() => import("./app/FunctionalComponents/docs/DocUpload"))


const ProjectDashboard = React.lazy(() => import("./app/FunctionalComponents/DashAlt/ProjectDashboard"))
const ClientProspectDashboard = React.lazy(() => import("./app/FunctionalComponents/Dashboard/components/ClientsProspectsDashboard"))

const RFP = React.lazy(() => import("./app/FunctionalComponents/RFP"))
const DealsMain = React.lazy(() => import("./app/FunctionalComponents/TransactionDeals/DealsMain"))
const BankLoan = React.lazy(() => import("./app/FunctionalComponents/BankLoan"))
const Others = React.lazy(() => import("./app/FunctionalComponents/Others"))
const Derivative = React.lazy(() => import("./app/FunctionalComponents/Derivative"))
const Marfp = React.lazy(() => import("./app/FunctionalComponents/Marfp"))


const DealRatingDetails = React.lazy(() => import("./app/FunctionalComponents/TransactionDeals/Component/DealSeriesRatings"))
const ConfigMain = React.lazy(() => import("./app/FunctionalComponents/PlatformManagement/ConfigMain"))

const MunivisorSplash = React.lazy(() => import("./app/LoginAuthentication/MunivisorSplash"))
const ForgotPassword = React.lazy(() => import("./app/LoginAuthentication/ForgotPassword"))

const SeriesDealRatings = React.lazy(() => import("./app/FunctionalComponents/TransactionDeals/Component/Ratings/DealRatings"))

const ClientFirmTabsView = React.lazy(() => import("./app/FunctionalComponents/EntityManagement/clients/containers/ClientFirmTabsView"))
const ThirdPartyFirmTabsView = React.lazy(() => import("./app/FunctionalComponents/EntityManagement/third-party/containers/ThirdPartyFirmTabsView"))
const UsersListTabsView = React.lazy(() => import("./app/FunctionalComponents/EntityManagement/users/containers/UsersListTabsView"))

const ClientsProspect = React.lazy(() => import("./app/FunctionalComponents/EntityManagement/clients/ClientsMain"))
const MigratedEntitiesMain = React.lazy(() => import("./app/FunctionalComponents/EntityManagement/clients/MigratedEntitiesMain"))
const ThirdParties = React.lazy(() => import("./app/FunctionalComponents/EntityManagement/third-party/ClientsMain"))
const BusinessDev = React.lazy(() => import("./app/FunctionalComponents/BusinessDevelopment/index"))
const NewBusinessDev = React.lazy(() => import("./app/FunctionalComponents/NewBusinessDevelopment"))

const Document = React.lazy(() => import("./app/FunctionalComponents/Document/components/ManageDocuments/index.js"))


const ContentDocs = React.lazy(() => import("./app/FunctionalComponents/ContentDocs"))
const MainContacts = React.lazy(() => import("./app/FunctionalComponents/EntityManagement/contacts/ContactsMain"))
const ChangeForgotPassword = React.lazy(() => import("./app/LoginAuthentication/ChangeForgotPassword"))
const FormsLinks = React.lazy(() => import("./app/FunctionalComponents/Compliance1/Component/FormsLinks"))

const CACMain = React.lazy(() => import("./app/FunctionalComponents/CAC/CACMain"))
const createTran = React.lazy(() => import("./app/FunctionalComponents/CreateTran"))
const BillingMain = React.lazy(() => import("./app/FunctionalComponents/Billing"))
const TaskDashboard = React.lazy(() => import("./app/FunctionalComponents/DashAlt/TaskDashboard"))
const BigCalendarComponent = React.lazy(() => import("./app/FunctionalComponents/TaskManagement/components/BigCalendarComponent"))
const PoliciesProcedures = React.lazy(() => import("./app/FunctionalComponents/Compliance1/Component/PoliciesProcedures"))
const NewCompliance = React.lazy(() => import("./app/FunctionalComponents/NewCompliance"))
const RevenuePipeline = React.lazy(() => import("./app/FunctionalComponents/RevenuePipeline"))
const GlobalSearch = React.lazy(() => import("./app/FunctionalComponents/Search/GlobalSearch"))
const MigrationTools1 = React.lazy(() => import("Admin/MigrationTools"))
const DocSearch = React.lazy(() => import("./app/FunctionalComponents/Search/DocSearch"))
const AdminUsage = React.lazy(() => import("./app/FunctionalComponents/AdminUsage"))
const AdminBilling = React.lazy(() => import("./app/FunctionalComponents/AdminBilling"))
const HistoricalSearch = React.lazy(() => import("./app/FunctionalComponents/Search/HistoricalSearch"))
const FileManager = React.lazy(() => import("./app/FunctionalComponents/FileManager/Search"))

const routesMap = {
  "dev": DevMainContainer,
  "dealRatings": DealRatingDetails,
  "seriesratings": SeriesDealRatings,
  "dashboard": GlobalSearch,
  // "dashboard": TaskDashboard,
  "forgotpass": ForgotPassword,
  "requesttoken": RequestToken,
  "changeforgotpass": ChangeForgotPassword,
  "deals": DealsMain,
  "signup": GlobalSignUp,
  "signin": GlobalSignIn,
  "splash": MunivisorSplash,
  "tasks": TaskMain,
  "auditlog": AuditLog,
  "modal": BulmaModalCard,
  "calendar": EventsCalendar,
  "docs": DocsMain,
  "upload": DocUpload,
  "rfp": RFP,
  "configuration": ConfigMain,
  "picklisttest": PicklistsTest,
  "firms": Firms,
  "third-parties": AdminThirdPartyTabsView,
  "users": Users,
  "clients": AdminClientFirmTabsView,
  "createTran": createTran,
  "update-tags": UpdateTagsTest,
  "attach-checklist": AttachChecklist,
  "process-checklist-test": ProcessChecklistTest,

  "addnew": TaskDashboard,
  "addnew-entity": ChooseEntity,
  "addnew-activity": ChooseActivity,
  "addnew-task": TaskMain,

  "dash": TaskDashboard,
  "dash-alt": TaskDashboard,
  "dash-task": TaskDashboard,
  "dash-projects": ProjectDashboard,
  "dash-projs": BusinessDev,
  // "bus-development": BusinessDev,
  "bus-development": NewBusinessDev,
  "dash-cltprosp": ClientProspectDashboard,
  "dash-cal": EventsCalendar,
  "dash-cal1": BigCalendarComponent,
  "dash-busdev": BusinessDev,

  "mast": TaskDashboard,
  "mast-cltprosp": ClientFirmTabsView,
  "mast-thirdparty": ThirdPartyFirmTabsView,
  "mast-allcontacts": UsersListTabsView,

  "comp": TaskDashboard,
  "compliance": NewCompliance,
  "new-compliance": NewCompliance,
  "comp-msrbsecfil": TaskDashboard,
  "comp-polandproc": PoliciesProcedures,
  "comp-formLinks": FormsLinks,
  "comp-stbusentfilandlicenses": TaskDashboard,
  "comp-orgdocuments": TaskDashboard,
  "comp-engletandforms": TaskDashboard,
  "comp-custcomp": TaskDashboard,

  "tools": TaskDashboard,
  "tools-analytics": TaskDashboard,
  "tools-revpipe": RevenuePipeline,
  "tools-billing": BillingMain,
  "tools-chat": TaskDashboard,
  "tools-docs": Document,
  "tools-docsearch": DocSearch,
  "historical-search": HistoricalSearch,
  "file-browser": FileManager,
  "contentDocs": ContentDocs,
  "tools-emma": TaskDashboard,
  "tools-globalsearch": TaskDashboard,
  // "tools-globalsearch": GlobalSearch,

  "contacts": MainContacts,
  "admin": ChooseAdmin,
  "admin-config": ConfigMain,
  "admin-migtools": MigrationTools1,
  "admin-usage": AdminUsage,
  "admin-billing": AdminBilling,

  "loan": BankLoan,
  "derivative": Derivative,
  "marfp": Marfp,
  "others": Others,
  "clients-propects": ClientsProspect,
  "migratedentities": MigratedEntitiesMain,

  "addnew-client": ClientsProspects,
  "addnew-prospect": ClientsProspects,
  "addnew-thirdparty": ThirdPartyNew,
  "addnew-contact": UsersNew,
  "admin-cltprosp": AdminClientFirmTabsView,
  "admin-thirdparty": AdminThirdPartyTabsView,
  "admin-firms": Firms,
  "admin-firmusers": Users,
  "admin-users": AdminUsersListTabsView,
  "thirdparties": ThirdParties,
  "cac": CACMain,
  "messages": Messages,
  "dash-projects-alt": ProjectDashboard,
  "not-available": NotAvailable,
  "userprofile": UserProfile,
  "stp-redirect": STPLogin
}

const componentProps = {
  docs: {
    rowCells: [
      "name",
      "uploadDate",
      "download",
      "docuSign",
      "share",
      "details"
    ],
    showUploadButton: true
  },
  upload: {
    tenantId: "tenant1",
    contextType: "rfp",
    contextId: "rfp123",
    tags: {
      docType: "type1",
      docCategory: "cat1"
    }
  },
  dealRatings: {
    multiple: false
  },
  "update-tags": {
    docId: "5b20b103483cc2f19c0b0f5a",
    tags: {
      docType: "type2",
      docCategory: "cat1"
    }
  },
  "attach-checklist": {
    checklistId: "CL3"
  }
}

class NavMain extends Component {
  constructor(props) {
    super(props)
    this.state = {
      navOptions: {},  // eslint-disable-line
      menuExpanded: false,
      allowedIds: "",
      waiting: false
    }
    this.protectRoutes = this.protectRoutes.bind(this)
    this.signOut = this.signOut.bind(this)
    this.expandMenu = this.expandMenu.bind(this)
    this.handleNotAllowed = this.handleNotAllowed.bind(this)
    this.setExpiryTimeout = this.setExpiryTimeout.bind(this)
    this.expireAuth = this.expireAuth.bind(this)
  }

  async componentDidMount() {
    this.unmount = false
    const { nav1, authenticated, exp } = this.props
    if (exp) {
      this.setExpiryTimeout(exp)
    }
    if (!nav1 && !authenticated) {
      this.props.history.push("/splash")
    } else if (authenticated) {
      await this.checkAllowedNavs(this.props)
    }
  }

  async componentWillReceiveProps(nextProps) {
    if (nextProps.exp && this.props.exp !== nextProps.exp) {
      this.setExpiryTimeout(nextProps.exp)
    }
    if (
      (this.props.authenticated !== nextProps.authenticated ||
        this.props.token !== nextProps.token) &&
      nextProps.nav1 !== "stp-redirect"
    ) {
      await this.props.checkAuth(nextProps.token)
      const nextUserEntities =
        (nextProps.loginDetails && nextProps.loginDetails.userEntities) || []
      if (nextUserEntities[0]) {
        await this.props.getNavOptions(nextProps.token)
      }
    } else if (nextProps.authenticated) {
      if (!Object.keys(nextProps.nav).length) {
        this.setNavOptions(nextProps.nav)
      } else {
        await this.protectRoutes(nextProps)
      }
    }
  }

  componentWillUnmount() {
    this.unmount = true
  }

  setNavOptions(navOptions) {
    navOptions = navOptions || {}
    if (!this.unmount) {
      this.setState({ navOptions })  // eslint-disable-line
    }
  }

  setExpiryTimeout(exp) {
    const tokenExpiry = new Date(exp * 1000) - new Date()
    console.log("token expires at : ", new Date(exp * 1000), tokenExpiry, exp)

    const oneDayInterval = 1 * 24 * 60 * 60 * 1000
    const fiveMinutesInterval = 5 * 60 * 1000
    if (
      !window.mvAuthWarningTimeout5 &&
      tokenExpiry < oneDayInterval &&
      tokenExpiry > fiveMinutesInterval
    ) {
      console.log("setting fiveMinutesInterval")
      window.mvAuthWarningTimeout5 = window.setTimeout(
        this.showAuthExpiryWarning.bind(this, 5),
        tokenExpiry - fiveMinutesInterval
      )
    }

    const oneMinuteInterval = 1 * 60 * 1000
    if (
      !window.mvAuthWarningTimeout1 &&
      tokenExpiry < oneDayInterval &&
      tokenExpiry > oneMinuteInterval
    ) {
      console.log("setting oneMinuteInterval")
      window.mvAuthWarningTimeout1 = window.setTimeout(
        this.showAuthExpiryWarning.bind(this, 1),
        tokenExpiry - oneMinuteInterval
      )
    }

    if (!window.mvAuthTimeout && tokenExpiry < oneDayInterval) {
      window.mvAuthTimeout = window.setTimeout(this.expireAuth, tokenExpiry)
    }
  }

  showAuthExpiryWarning(minutes) {
    console.log(
      "authWarningTimeout : ",
      minutes,
      window[`mvAuthWarningTimeout${minutes}`],
      new Date()
    )
    if (this[`mvAuthWarningTimeout${minutes}`]) {
      window.clearTimeout(this[`mvAuthWarningTimeout${minutes}`])
    }
    window[`mvAuthWarningTimeout${minutes}`] = null
    toast(
      `Your session will expire in ${minutes} minutes. You will be logged out automatically for security reasons. You will need to login again. Please save any unsaved work.`,
      {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.WARNING
      }
    )
  }

  expireAuth() {
    console.log("in expireAuth")
    if (window.mvAuthTimeout) {
      window.clearTimeout(window.mvAuthTimeout)
    }
    window.mvAuthTimeout = null
    toast(
      "Your session has expired. Please sign in again for security reasons.",
      {
        autoClose: CONST.ToastTimeout,
        type: toast.TYPE.WARNING
      }
    )
    this.props.signOut()
  }

  expandMenu() {
    if (!this.unmount) {
      this.setState(prevState => ({ menuExpanded: !prevState.menuExpanded }))
    }
  }

  handleNotAllowed() {
    toast("Selected functionality is not available to you. Redirecting to dashboard.", {
      autoClose: CONST.ToastTimeout,
      type: toast.TYPE.WARNING
    })
    this.timeout = window.setTimeout(() => {
      this.props.history.push("/dashboard")
    }, CONST.ToastTimeout)
  }

  async checkAllowedNavs(props) {
    const { nav1, nav, token } = props
    if (token !== localStorage.getItem("token")) {
      toast(
        "You are being logged out for security reasons. Please login again.",
        {
          autoClose: CONST.ToastTimeout,
          type: toast.TYPE.WARNING
        }
      )
      this.props.signOut()
    }
    if (token) {
      const isPlatformAdmin = false
      if (isPlatformAdmin) {
        if (!this.unmount) {
          this.setState({ allowedIds: {} })
        }
        return true
      }
    }
    const navKeys = Object.keys(props)
      .filter(k => /^nav(?=[1-9])/.test(k))
      .sort((a, b) => a.split("nav")[1] - b.split("nav")[1])
    if (
      token &&
      nav1 &&
      nav1 !== "signin" &&
      !PUBLIC_ROUTES.includes(nav1) &&
      nav
    ) {
      let ids = {}
      const regExp = /[^a-zA-Z0-9]/
      const { nav2 } = props
      if (nav2 && nav2.length === 24 && !regExp.test(nav2)) {
        ids = await checkNavAccessEntitlrment(nav2)
      }
      if (!this.unmount) {
        this.setState({ allowedIds: ids })
      }
      let val = ""
      let allowed = true
      const allNavL1Keys = Object.keys(nav)
      navKeys.some((k, i) => {  // eslint-disable-line
        if (i === 0) {
          if (allNavL1Keys.includes(props[k])) {
            val = nav[props[k]]
          } else {
            let foundL2 = false
            allNavL1Keys.some(k1 => {  // eslint-disable-line
              if (nav[k1] && nav[k1][props[k]]) {
                foundL2 = true
                val = nav[k1][props[k]]
                return true
              }
            })
            if (!foundL2) {
              allowed = false
              return true
            }
          }
          // console.log(" k, i val1 : ", k, i, val)
          if (val === true) {
            // return true
          } else if (!val) {
            allowed = false
            return true
          }
        } else if (
          val &&
          Object.getPrototypeOf(val).constructor.name === "Object"
        ) {
          if (val[props[k]]) {
            val = val[props[k]]
          } else if (!ids.view) {
            allowed = false
            return true
          } else if (!val[props[k]] && k === "nav3") {
            allowed = false
            return true
          }
        } else if (val === true) {
          // console.log(" k, i val3 : ", k, i, val)
          // return true
        } else if (!val && !ids.view) {
          allowed = false
          return true
        }
      })
      console.log("allowed : ", allowed)
      if (!allowed && nav1 !== "dashboard") {
        this.setState({ waiting: true }, this.handleNotAllowed)
      }
    }
  }

  async protectRoutes(props) {
    const { nav1, loginDetails, exp } = props
    if (exp) {
      this.setExpiryTimeout(exp)
    }
    if (
      loginDetails &&
      loginDetails.userEntities &&
      loginDetails.userEntities[0] &&
      loginDetails.userEntities[0].userId
    ) {
      if (!nav1 || nav1 === "signin") {
        this.props.history.push("/dashboard")
      } else {
        await this.checkAllowedNavs(props)
      }
    }
  }

  signOut() {
    this.props.signOut()
  }

  renderNavs(options) {
    let { navOption } = this.props
    navOption = navOption || "dashboard"
    const navItems = []
    const sortedOptions = Object.keys(options).sort(
      (a, b) =>
        (navLabels[a] ? navLabels[a].order : 0) -
        (navLabels[b] ? navLabels[b].order : 0)
    )
    sortedOptions.forEach((e, i) => {
      if (options[e]) {
        if (Object.getPrototypeOf(options[e]).constructor.name === "Object") {
          const subItems = []
          const nestedRoutes = { ...options[e] }
          const sortedNestedKeys = Object.keys(nestedRoutes).sort(
            (a, b) =>
              (navLabels[a] ? navLabels[a].order : 0) -
              (navLabels[b] ? navLabels[b].order : 0)
          )
          sortedNestedKeys.forEach((k, j) => {
            if (options[e][k]) {
              // console.log("k : ", k)
              const itemClass =
                navOption === k ? "navbar-item is-active" : "navbar-item"
              if (navLabels[k]) {
                subItems.push(
                  <Link
                    key={e + k + i + j}  // eslint-disable-line
                    className={itemClass}
                    to={`/${navLabels[k].route || k}`}
                  >
                    {navLabels[k].label}
                  </Link>
                )
              }
            }
          })
          if (navLabels[e]) {
            navItems.push(
              <div
                key={`${e}div${i}`}  // eslint-disable-line
                className="navbar-item has-dropdown is-hoverable"
              >
                <a className="navbar-link"> {/* eslint-disable-line */}
                  {navLabels[e].label}
                </a>
                <div className="navbar-dropdown ">{subItems}</div>
              </div>
            )
          }
        } else {
          const itemClass =
            navOption === e ? "navbar-item is-active" : "navbar-item"
          // console.log("e : ", e)
          if (navLabels[e]) {
            navItems.push(
              <Link
                key={e + i} // eslint-disable-line
                to={`/${navLabels[e].route || e}`}
                className={itemClass}
              >
                {navLabels[e].label}
              </Link>
            )
          }
        }
      }
    })
    navItems.push(
      <div key="logout-touch-div" className="navbar-item is-hoverable is-touch">
        {" "}
        <a key="privacy-policy- touch" target="_blank" rel="noopener noreferrer" href="https://www.munivisor.com/privacy.php" className="navbar-item">
           Privacy Policy
        </a>
        <a key="logout - touch" onClick={this.signOut} className="navbar-item">  {/* eslint-disable-line */}
          Logout
        </a>
      </div>
    )
    return navItems
  }

  renderNavBar(navOptions, menuExpanded) {
    return (
      <nav id="navbar" className="navbar is-gray is-fixed-top">
        <div id="specialShadow" className="bd-special-shadow" />
        <div className="navbar-brand">

          <div
            id="navbarBurger"
            className={
              menuExpanded
                ? "navbar-burger burger is-active"
                : "navbar-burger burger"
            }
            onClick={this.expandMenu}
            data-target="navMenuDocumentation"
          >
            <span />
            <span />
            <span />
          </div>
        </div>
        <div
          id="navMenuDocumentation"
          className={menuExpanded ? "navbar-menu is-active" : "navbar-menu"}
        >
          <div className="navbar-start">{this.renderNavs(navOptions)}</div>

          <div className="navbar-end is-hidden-touch ipad-nav">

            <div className="d-inline-block">
              <NavSearch
                loginDetails={this.props.loginDetails}
                allowedIds={this.state.allowedIds}
              />
            </div>
            {this.props.nav && Object.keys(this.props.nav).includes("admin") ? (
              <Link to="/admin" className="navbar-item d-inline-block">
                <span className="icon is-small">
                  <i className="fab fa-adn" />
                </span>
              </Link>
            ) : (
              undefined
            )}

            <div className="navbar-item navbar-item-config is-hoverable has-dropdown is-hidden-touch ">
              <a className="navbar-link"> {/* eslint-disable-line */}
                <span className="icon is-small">
                  <i className="fas fa-cog" />
                </span>
              </a>
              <div className="navbar-dropdown is-right">
                <Link to="/userprofile" className="navbar-item is-hidden-touch">
                  <span className="icon is-small">
                    <i className="fa fa-user" />
                  </span>&nbsp;
                   User Profile
                </Link>
                <hr className="navbar-divider" />
                <a className="navbar-item is-hidden-touch" target="_blank" rel="noopener noreferrer" href="https://www.munivisor.com/privacy.php">
                  <span className="icon">
                    <i className="fa fa-lock" />
                  </span>
                  Privacy Policy
                </a>
                <hr className="navbar-divider" />
                <a  // eslint-disable-line
                  className="navbar-item is-hidden-touch"
                  onClick={this.signOut}
                >
                  <span className="icon">
                    <i className="fa fa-power-off" />
                  </span>
                  Logout
                </a>
              </div>
            </div>
          </div>
        </div>
      </nav>
    )
  }

  renderPrivateComponent(nav1) {
    if (nav1 && routesMap.hasOwnProperty(nav1)) {   // eslint-disable-line
      const { allowedIds } = this.state
      const { loginDetails } = this.props
      if (
        !allowedIds ||
        !(
          loginDetails &&
          loginDetails.userEntities &&
          loginDetails.userEntities[0] &&
          loginDetails.userEntities[0].userId
        )
      ) {
        return <Loader />
      }
      const RouteComponent = withRouter(routesMap[nav1])
      return (
        <Suspense fallback={<Loader />}><RouteComponent
          {...this.props}
          {...componentProps[nav1]}
          allowedIds={allowedIds}
        /></Suspense>
      )
    }
    return <h4>404 - Page not found</h4>
  }

  renderPublicComponent(nav1) {
    if (routesMap.hasOwnProperty(nav1)) {  // eslint-disable-line
      const PublicComponent = withRouter(routesMap[nav1])
      return  <Suspense fallback={<Loader />}><PublicComponent /></Suspense>
    }
    return <h4>404 - Page not found</h4>
  }

  render() {
    const navOptions = this.props.nav
    const { nav1 } = this.props
    const { menuExpanded, waiting } = this.state

    if (waiting) {
      return <Loader />
    }

    // console.log(PUBLIC_ROUTES)
    if (nav1 && PUBLIC_ROUTES.includes(nav1)) {
      return this.renderPublicComponent(nav1)
    }

    if (this.props.authenticated) {
      if (Object.keys(navOptions).length) {
        return (
          <div>
            {this.renderNavBar(navOptions, menuExpanded)}
            <div style={{ marginTop: 50 }}>
              {this.renderPrivateComponent(nav1 || "dashboard")}
            </div>
            <div className="label-container">
              <div className="label-text">Dashboard</div>
              <i className="fa fa-play label-arrow" />
            </div>
          </div>
        )
      }
      return <Loader />
    }
    const SignIn = withRouter(GlobalSignIn)
    // console.log("render signin")
    return <SignIn />
  }
}

const mapStateToProps = ({
  nav,
  auth: { token, authenticated, loginDetails, exp },
  urls
}) => ({
  nav,
  token,
  authenticated,
  loginDetails,
  exp,
  allurls: urls ? urls.allurls : {}
})

export default connect(
  mapStateToProps,
  { getNavOptions, checkAuth, signOut }
)(NavMain)
