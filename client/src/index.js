import ReactDOM from "react-dom"
import React from "react"
import AppGlobal from "./AppGlobal"
// import * as serviceWorker from "./serviceWorker"

const renderApp = () => {
  ReactDOM.render(<AppGlobal />, document.getElementById("root"))
}

renderApp()

// if(module.hot) {
//   module.hot.accept("./AppGlobal.js",() => {
//     renderApp()
//   })
// }
// serviceWorker.register()
