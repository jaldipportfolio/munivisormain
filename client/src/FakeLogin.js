import React, { Component } from "react"

import { connect } from "react-redux"

import { getAuthUser } from "./app/StateManagement/actions"

class FakeLogin extends Component {
  constructor(props) {
    super(props)
    this.changeAuthUser = this.changeAuthUser.bind(this)
  }

  changeAuthUser(event) {
    const userId = event.target.value
    this.props.getAuthUser(userId)
  }

  render() {
    if(this.props.fakeAuthUser && this.props.fakeAuthUser.userId) {
      return null
    }
    return (
      <div className="container">
        <label>Login as : </label>
        <div className="select">
          <select onChange={this.changeAuthUser}>
            <option />
            <option>5b0bc30de129104ae2e19632</option>
          </select>
        </div>
      </div>
    )
  }
}

const mapStateToProps = ({ fakeAuthUser }) => {
  console.log("fakeAuthUser in state : ", fakeAuthUser)
  return { fakeAuthUser }
}

export default connect(mapStateToProps, { getAuthUser })(FakeLogin)
