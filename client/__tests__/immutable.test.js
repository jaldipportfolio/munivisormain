import Immutable from 'immutable'
import _ from 'lodash'

const mutateValue= (iterable, pos, value) => {
    iterable[pos] = value
}

const updateState= (immutable, pos, value) => {
    return immutable.set(pos.value)
}

const createObjToDos= (numToDos) => {

    let retObj = {}
    _.forEach(_.range(numToDos),(index) => {
        const seqId = String(index + 1)
        retObj[`todo ${seqId}`] = {
            title: `To Do - ${seqId}`,
            value: `Do something with this todo - ${seqId}`

        }
    })

    return retObj
}

describe("Manage app state using immutable JS",() => {
    it('Normal Arrays and Iterables are Mutable Structures',() => {
        const state = ["t1","t2"]
        const mutatedState = state

        mutateValue(mutatedState, 0, "new value")
        expect(state[0]).toBe("new value")

    })

    it('Objects Created from Immutable are not mutable',() => {
        const immutableState = Immutable.List(['t1','t2']);
        const immutableState2 = immutableState

        updateState(immutableState2,0,"new value");
        expect(immutableState.get(0)).toBe("t1")

    })

})

describe("There are many different ways of generating an immutable map",() => {
    it('Instantiate the Immutable Map with an Object that has keys and values',() => {
        const data = {
            "Naveen":{
                firstName:"Battery",
                lastName:"Balawat",
                age:40
            },
            "Soujanya":{
                firstName:"Soujanya",
                lastName:"Parameswara",
                age:39
            }
        }
    
        let immMap = Immutable.Map(data);
        expect(immMap.get("Naveen").firstName).toBe("Battery")
    
    })

    it('Ability to create using array tuples',() => {
        const data = [["t1",{title:"naveen",toDo:"this is a new todo"}]]
    
        let immMap = Immutable.Map(data);
        expect(immMap.get("t1").title).toBe("naveen")
    
    })    

    it('The Maps have size and not lengths',() => {
   
        const numToDos = 20
        const immMap = Immutable.Map(createObjToDos(numToDos));
        expect(immMap.size).toEqual(numToDos)
    
    })  
})