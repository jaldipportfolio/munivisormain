const templateDirectory = "./templates/development/"

export const devTemplates = [
  {
    Name: "DEV_LANDING_NEW_USER",
    filepath: `${templateDirectory}index-mv-welcome-user.html`,
    SubjectPart: "Welcome to MuniVisor - Invitation from {{userFirmName}}",
    isUpdatable: true,
  }
]