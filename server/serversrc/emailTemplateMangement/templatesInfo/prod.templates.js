const templateDirectory = "./templates/production/"

export const prodTemplates = [
  {
    Name: "LANDING_NEW_USER",
    filepath: `${templateDirectory}index-mv-welcome-user.html`,
    SubjectPart: "Welcome to MuniVisor - Invitation from {{userFirmName}}",
    isUpdatable: true,
  }
]