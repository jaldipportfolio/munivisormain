import dotenv from "dotenv"

import { prodTemplates } from "./templatesInfo/prod.templates"
import { devTemplates } from "./templatesInfo/dev.templates"


dotenv.config()
const isDevOnly = process.env.NODE_ENV === "development"

export const getTemplatesInfo = () => isDevOnly ? devTemplates : prodTemplates