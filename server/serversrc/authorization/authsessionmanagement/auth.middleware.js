// import express from "express"

const passportService = require("./passport")
const passport = require("passport")

// Create the interceptor and ensure that the cookie is turned off
export const requireAuth = passport.authenticate("jwt",{session:false})
export const requireSignIn = passport.authenticate("local",{session:false})

export const firmUsersCanEdit = (req, res, next) => {
  if ( !req.user) {
    return res.status(401).send({error:" Login in necessary"})
  }
  const { relationshipType } = req.user
  if ( relationshipType === "Self" ) {
    next()
  } else {
    return res.status(500).send({error:"The Logged In User is not entitled to create or update records for the tenant"})
  }
}

export const authMiddleware = async (req, res, next) => {
  try {
    const token = req.headers.authorization
    if (token !== process.env.PLATFORM_UTILITIES_TOKEN) {
      return res.status(500).send({
        error: "Unauthorized"
      })
    }
    next()
  } catch (err) {
    return res.status(403).send({ error: "Unauthorized" })
  }
}
