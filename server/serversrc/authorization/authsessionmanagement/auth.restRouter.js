import express from "express"
import {
  signin, signupnew, getUsersForSignUp, checkAuth,
  getEntitlementsForLoggedInUser, checkEntitlement, checkUserExistance,
  signupConfirm, forgotPassword, passwordResetConfirm, stpAuth, changePassword, getSignupUserFromResetId
} from "./auth.controller"
import { requireAuth, requireSignIn } from "./auth.middleware"
import { authLimiter } from "./../../middlewares"

// Create the interceptor and ensure that the cookie is turned off

export const authRouter = express.Router()

authRouter.get("/", requireAuth, (req, res) => {
  res.send({ message: "You are accessing a protected route" })
})
authRouter.get("/checkemailexists", checkUserExistance)
// authRouter.get("/entitlements",requireAuth, getEntitlementsForLoggedInUser)
authRouter.get("/entitlements", checkEntitlement)
authRouter.post("/signin", requireSignIn, signin)
authRouter.post("/signup", signupnew)
authRouter.get("/signupusers", getUsersForSignUp)
authRouter.get("/signupconfirm", signupConfirm)
authRouter.post("/forgotpassword", forgotPassword)
authRouter.post("/changepassword", changePassword)
authRouter.get("/resetpassconfirm", passwordResetConfirm)
authRouter.get("/userbyresetid", getSignupUserFromResetId)
authRouter.get("/checkauth", requireAuth, checkAuth)
authRouter.get("/stp", stpAuth)
