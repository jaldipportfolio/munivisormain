import {requestSecurityParser} from "./../helpers"

export const checkInputDataSecurity = () => ( req,res, next) => {
  let reqBodyChecker = false
  let reqParamsChecker = false
  let reqQueryChecker = false

  if ( req && req.body ) {
    reqBodyChecker = requestSecurityParser(req.body)
  } else {
    reqBodyChecker = true
  }

  if ( req && req.params ) {
    reqParamsChecker = requestSecurityParser(req.params)
  } else {
    reqParamsChecker = true
  }

  if ( req && req.query ) {
    reqQueryChecker = requestSecurityParser(req.query)
  } else {
    reqQueryChecker = true
  }

  if ( reqBodyChecker && reqParamsChecker && reqQueryChecker) {
    next()
  } else {
    res.status(403).send({done:"false",message:"there is a problem with either body or params or query parameters"})
  }
}