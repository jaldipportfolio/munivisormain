// eslint-disable-next-line
const docAuth = (req, res, next) => {
  const { authorization } = req.headers
  let valid = false

  if (authorization) {
    const decryptedcode = Buffer.from(
      authorization.split(" ")[1] || "",
      "base64"
    ).toString()
    if (decryptedcode === "3vpcdquDm1:SWFtlOBoSycz09BeDlUwySpEbRZ27YoT") {
      valid = true
    }
  }

  if (!valid) {
    return res.status(403).json({ error: "No credentials sent!" })
  }
  next()
}

export default docAuth
