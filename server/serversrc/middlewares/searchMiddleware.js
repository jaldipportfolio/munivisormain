import jwt from "jsonwebtoken"
import configkeys from "../config"

// eslint-disable-next-line
const middleware = async (req, res, next) => {
  try {
    const token = req.headers.authorization
    console.log(token)
    const searchingIndex = req.path.split("/")[1]
    // make sure searching index is present
    if (!searchingIndex) {
      return res.status(400).send({ error: "Please use valid searching index" })
    }
    const tenantId = searchingIndex.split("_").pop()
    const decodedToken = await jwt.verify(token, configkeys.secrets.JWT_SECRET)
    // make sure tenant is searching in his own index

    if (decodedToken.entityDetails.entityId !== tenantId) {
      return res.status(500).send({
        error: "You are violating policy of searching across different tenant"
      })
    }

    delete req.headers.authorization
  } catch (err) {
    return res.status(403).send({ error: "Unauthorized" })
  }

  next()
}

export default middleware
