import {Deals, Tasks, Entity, EntityUser, RFP, ActBusDev,ActMaRFP, Others, BankLoans, Derivatives} from "../appresources/models"

import {elasticSearchUpdateFunction} from "./esHelper"

const esUpdate = {
  DEALS: (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: Deals, _id: id, esType: "tranagencydeals"}),
  RFPS: (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: RFP, _id: id, esType: "tranagencyrfps"}),
  BANKLOANS: (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: BankLoans, _id: id, esType: "tranbankloans"}),
  MARFPS: (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: ActMaRFP, _id: id, esType: "actmarfps"}),
  DERIVATIVES: (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: Derivatives, _id: id, esType: "tranderivatives"}),
  OTHERS: (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: Others, _id: id, esType: "tranagencyothers"}),
  BUSDEV: (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: ActBusDev, _id: id, esType: "actbusdevs"}),
  TASKS: (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: Tasks, _id: id, esType: "mvtasks"}),
  ENTITYDOCS: (tenantId, id) => elasticSearchUpdateFunction({tenantId, Model: Entity, _id: id, esType: "entities"})
}

export const returnFromES  = (type, tenantId, id) => esUpdate[type](tenantId, id)
