import mongoose from "mongoose"
import {ObjectID} from "mongodb"
import isEmpty from "lodash/isEmpty"

import { TENANT_DOC_ORGANIZATION } from "./constants"
import  {
  Entity,EntityRel,Docs
}  from "./../appresources/models"

import {
  tenantUrlGenerator
} from "./../appresources/commonservices/urlGeneratorForTenant"

require("dotenv").config()

mongoose.Promise = global.Promise
mongoose.connect(process.env.DB_URL, { useNewUrlParser: true })

// const helperGetIds = (arr,prefix,keyProp) => arr.map((a) => ({[`${prefix}_${a[keyProp]}`]:a}) )
const helperGetIds = (arr,prefix,keyProp) => arr.map((a) => ({tranAWSKey:`${prefix}_${a[keyProp]}`,...a}) )


const getEntitiesForTenantAndRelationshipType =  async (tenantId, relationshipType,prefix) => {
  const relatedEntities= await EntityRel.aggregate([
    {$match:{entityParty1:ObjectID(tenantId),relationshipType}},
    {$project:{_id:"$entityParty2"}}
  ])
  return helperGetIds(relatedEntities,prefix)
}

const getUserIdsForTenantAndRelationshipType =  async (tenantId, relationshipType,prefix) => {
  const users= await EntityRel.aggregate([
    {$match:{entityParty1:ObjectID(tenantId),relationshipType}},
    {
      $lookup:
              {
                from: "entityusers",
                localField: "entityParty2",
                foreignField: "entityId",
                as: "users"
              }
    },
    { $unwind : "$users"},
    {$project:{_id:"$users._id"}}
  ])
  return helperGetIds(users,prefix)
}

export const getAllDetailsForTenant = async(tenantId) => {

  const commonPipelineStagesForAllTransactions = [
    {
      $addFields:{
        allIds:{$setUnion:[
          "$participantEntityIds",
          "$participantUserIds",
          ["$tranId"]
        ]}
      },
    },
    {
      $match:{docDetails: { $exists: true, $ne: [] }}
    },
    // {
    //   $unwind:"$docDetails"
    // },
    // // {
    // //   $addFields:{
    // //     docAWSReference:{ $toObjectId: "$docDetails.docAWSReference" }
    // //   }
    // // }
    // {
    //   $lookup:{
    //     from: "docs",
    //     localField: "docDetails.docAWSReference",
    //     foreignField: "_id",
    //     as: "awsreference"
    //   }
    // },
    // // {
    //   $unwind:"$awsrefrence"
    // },
    // {
    //   $group:{
    //     _id: {
    //       tranId:"$tranId",
    //       tenantId:"$tenantId",
    //       tenantName:"$tenantName",
    //       issuerName:"$issuerName",
    //       issuerId:"$issuerId",
    //       tranType:"$tranType",
    //       tranSubType:"$tranSubType",
    //       tranCreateDate:"$tranCreateDate",
    //       activityDescription :"$activityDescription",
    //       participantDetails:"$participantDetails",
    //       participantEntityIds:"$participantEntityIds",
    //       primarySector: "$primarySector",
    //       secondarySector: "$secondarySector",
    //       participantUserIds:"$participantUserIds",
    //       allIds:"$allIds",
    //     },
    //     dealDetailsWithAWS:{
    //       $push: {
    //         docId:"$docDetails.docId",
    //         fileName:"$docDetails.fileName",
    //         docCategory:"$docDetails.docCategory",
    //         docSubCategory:"$docDetails.docSubCategory",
    //         docType:"$docDetails.docType",
    //         docAWSReference:"$docDetails.docAWSReference",
    //         uploadedBy:"$docDetails.uploadedBy",
    //         docUploadUserId:"$docDetails.docUploadUserId",
    //         uploadDate:"$docDetails.uploadDate",
    //         fileNameAWS:"$awsrefrence.name"
    //       }
    //     }
    //   }
    // }
  ]

  const data = await EntityRel.aggregate([
    {
      $match:{entityParty1:ObjectID(tenantId), relationshipType:"Self"}
    },
    {
      $project:{tenantId:"$entityParty1"}
    },
    {
      $facet:{
        "DEALS":[
          {
            $lookup:{
              from: "tranagencydeals",
              localField: "tenantId",
              foreignField: "dealIssueTranClientId",
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              _id:0,
              tranId:"$trans._id",
              tenantId:1,
              tenantName:"$trans.dealIssueTranClientFirmName",
              issuerName:"$trans.dealIssueTranIssuerFirmName",
              issuerId:"$trans.dealIssueTranIssuerId",
              tranType:"$trans.dealIssueTranType",
              tranSubType:"$trans.dealIssueTranSubType",
              tranCreateDate:"$trans.created_at",
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.dealIssueTranIssueName",""]},{"$eq":["$trans.dealIssueTranIssueName",null]}]},
                  then:"$trans.dealIssueTranProjectDescription",
                  else:"$trans.dealIssueTranIssueName"},
              },
              participantDetails:{
                $setUnion:[
                  {$map: {
                    input: "$trans.dealIssueParticipants",
                    as: "participants",
                    in: {
                      partUserId: "$$participants.dealPartContactId" ,
                      partUserName: "$$participants.dealPartContactName",
                      partEntityId:"$$participants.dealPartFirmId",
                      partUserRole : "$$participants.dealPartType",
                      partEntityName:"$$participants.dealPartFirmName",
                    }
                  },
                  }]
              },
              docDetails:{
                $setUnion:[
                  {$map: {
                    input: "$trans.dealIssueDocuments",
                    as: "documents",
                    in: {
                      docId:"$$documents._id",
                      fileName:"$$documents.docFileName",
                      docCategory:"$$documents.docCategory",
                      docSubCategory:"$$documents.docSubCategory",
                      docType:"$$documents.docType",
                      docAWSReference:"$$documents.docAWSFileLocation",
                      uploadedBy:"$$documents.createdUserName",
                      docUploadUserId:"$$documents.createdBy",
                      uploadDate:"$$documents.LastUpdatedDate"
                    }
                  },
                  }]
              },


              participantEntityIds:{
                $setUnion:[
                  "$trans.dealIssueParticipants.dealPartFirmId",
                  "$trans.dealIssueUnderwriters.dealPartFirmId",
                  ["$trans.dealIssueTranIssuerId", "$trans.dealIssueTranClientId" ]
                ]
              },
              primarySector: "$trans.dealIssueTranPrimarySector",
              secondarySector: "$trans.dealIssueTranSecondarySector",
              participantUserIds:{
                $setUnion:[
                  "$trans.dealIssueParticipants.dealPartContactId",
                  "$trans.dealIssueTranAssignedTo",
                  "$trans.dealIssueDocuments.createdBy",
                  ["$trans.createdByUser" ]
                ]
              }
            }
          },
          ...commonPipelineStagesForAllTransactions
        ],
        "BANKLOANS":[
          {
            $lookup:{
              from: "tranbankloans",
              localField: "tenantId",
              foreignField: "actTranFirmId",
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              _id:0,
              tranId:"$trans._id",
              tenantId:1,
              tenantName:"$trans.actTranFirmName",
              issuerName:"$trans.actTranClientName",
              issuerId:"$trans.actTranClientId",
              tranType:"$trans.actTranType",
              tranSubType:"$trans.actTranSubType",
              documents:"$trans.bankLoanDocuments",
              tranCreateDate:"$trans.createdAt",
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.actTranIssueName",""]},{"$eq":["$trans.actTranIssueName",null]}]},
                  then:"$trans.actTranProjectDescription",
                  else:"$trans.actTranIssueName"},
              },
              participantDetails:{
                $setUnion:[
                  {$map: {
                    input: "$trans.bankLoanParticipants",
                    as: "participants",
                    in: {
                      partUserId: "$$participants.partContactId" ,
                      partUserName: "$$participants.partContactName",
                      partEntityId:"$$participants.partFirmId",
                      partUserRole : "$$participants.partType",
                      partEntityName:"$$participants.partFirmName",
                    }
                  },
                  }]
              },

              docDetails:{
                $setUnion:[
                  {$map: {
                    input: "$trans.bankLoanDocuments",
                    as: "documents",
                    in: {
                      docId:"$$documents._id",
                      fileName:"$$documents.docFileName",
                      docCategory:"$$documents.docCategory",
                      docSubCategory:"$$documents.docSubCategory",
                      docType:"$$documents.docType",
                      docAWSReference:"$$documents.docAWSFileLocation",
                      uploadedBy:"$$documents.createdUserName",
                      docUploadUserId:"$$documents.createdBy",
                      uploadDate:"$$documents.LastUpdatedDate"
                    }
                  },
                  }]
              },
              participantEntityIds:{
                $setUnion:[
                  "$trans.bankLoanParticipants.partFirmId",
                  ["$trans.actTranClientId", "$trans.actTranFirmId" ]
                ]
              },
              primarySector: "$trans.actTranPrimarySector",
              secondarySector: "$trans.actTranSecondarySector",
              participantUserIds:{
                $setUnion:[
                  "$trans.bankLoanParticipants.partContactId",
                  ["$trans.actTranFirmLeadAdvisorId"],
                  "$trans.bankLoanDocuments.createdBy",
                  ["$trans.createdByUser" ]
                ]
              }
            }
          },
          ...commonPipelineStagesForAllTransactions
        ],
        "DERIVATIVES":[
          {
            $lookup:{
              from: "tranderivatives",
              localField: "tenantId",
              foreignField: "actTranFirmId",
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              _id:0,
              tranId:"$trans._id",
              tenantId:1,
              tenantName:"$trans.actTranFirmName",
              issuerName:"$trans.actTranClientName",
              issuerId:"$trans.actTranClientId",
              tranType:"$trans.actTranType",
              tranSubType:"$trans.actTranSubType",
              tranCreateDate:"$trans.createdAt",
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.actTranIssueName",""]},{"$eq":["$trans.actTranIssueName",null]}]},
                  then:"$trans.actTranProjectDescription",
                  else:"$trans.actTranIssueName"},
              },
              participantDetails:{
                $setUnion:[
                  {$map: {
                    input: "$trans.derivativeParticipants",
                    as: "participants",
                    in: {
                      partUserId: "$$participants.partContactId" ,
                      partUserName: "$$participants.partContactName",
                      partEntityId:"$$participants.partFirmId",
                      partUserRole : "$$participants.partType",
                      partEntityName:"$$participants.partFirmName",
                    }
                  },
                  }]
              },
              docDetails:{
                $setUnion:[
                  {$map: {
                    input: "$trans.derivativeDocuments",
                    as: "documents",
                    in: {
                      docId:"$$documents._id",
                      fileName:"$$documents.docFileName",
                      docCategory:"$$documents.docCategory",
                      docSubCategory:"$$documents.docSubCategory",
                      docType:"$$documents.docType",
                      docAWSReference:"$$documents.docAWSFileLocation",
                      uploadedBy:"$$documents.createdUserName",
                      docUploadUserId:"$$documents.createdBy",
                      uploadDate:"$$documents.LastUpdatedDate"
                    }
                  },
                  }]
              },


              participantEntityIds:{
                $setUnion:[
                  "$trans.derivativeParticipants.partFirmId",
                  ["$trans.actTranClientId", "$trans.actTranFirmId" ]
                ]
              },
              primarySector: "$trans.actTranPrimarySector",
              secondarySector: "$trans.actTranSecondarySector",
              participantUserIds:{
                $setUnion:[
                  "$trans.derivativeParticipants.partContactId",
                  ["$trans.actTranFirmLeadAdvisorId"],
                  "$trans.derivativeDocuments.createdBy",
                  ["$trans.createdByUser" ]
                ]
              }
            }
          },
          ...commonPipelineStagesForAllTransactions
        ],
        "MARFPS":[
          {
            $lookup:{
              from: "actmarfps",
              localField: "tenantId",
              foreignField: "actTranFirmId",
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              _id:0,
              tranId:"$trans._id",
              tenantId:1,
              tenantName:"$trans.actTranFirmName",
              issuerName:"$trans.actIssuerClientEntityName",
              issuerId:"$trans.actIssuerClient",
              tranType:"$trans.actType",
              tranSubType:"$trans.actSubType",
              tranCreateDate:"$trans.createdAt",
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.actIssueName",""]},{"$eq":["$trans.actIssueName",null]}]},
                  then:"$trans.actTranProjectDescription",
                  else:"$trans.actIssueName"},
              },
              participantDetails:{
                $setUnion:[
                  {$map: {
                    input: "$trans.maRfpParticipants",
                    as: "participants",
                    in: {
                      partUserId: "$$participants.partContactId" ,
                      partUserName: "$$participants.partContactName",
                      partEntityId:"$$participants.partFirmId",
                      partUserRole : "$$participants.partType",
                      partEntityName:"$$participants.partFirmName",
                    }
                  },
                  }]
              },

              docDetails:{
                $setUnion:[
                  {$map: {
                    input: "$trans.maRfpDocuments",
                    as: "documents",
                    in: {
                      docId:"$$documents._id",
                      fileName:"$$documents.docFileName",
                      docCategory:"$$documents.docCategory",
                      docSubCategory:"$$documents.docSubCategory",
                      docType:"$$documents.docType",
                      docAWSReference:"$$documents.docAWSFileLocation",
                      uploadedBy:"$$documents.createdUserName",
                      docUploadUserId:"$$documents.createdBy",
                      uploadDate:"$$documents.LastUpdatedDate"
                    }
                  },
                  }]
              },

              participantEntityIds:{
                $setUnion:[
                  "$trans.maRfpParticipants.partFirmId",
                  ["$trans.actIssuerClient", "$trans.actTranFirmId" ]
                ]
              },
              primarySector: "$trans.actPrimarySector",
              secondarySector: "$trans.actSecondarySector",
              participantUserIds:{
                $setUnion:[
                  "$trans.maRfpParticipants.partContactId",
                  ["$trans.actLeadFinAdvClientEntityId"],
                  "$trans.maRfpDocuments.createdBy",
                  ["$trans.createdByUser" ]
                ]
              }
            }
          },
          ...commonPipelineStagesForAllTransactions
        ],
        "OTHERS":[
          {
            $lookup:{
              from: "tranagencyothers",
              localField: "tenantId",
              foreignField: "actTranFirmId",
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              _id:0,
              tranId:"$trans._id",
              tenantId:1,
              tenantName:"$trans.actTranFirmName",
              issuerName:"$trans.actTranClientName",
              issuerId:"$trans.actTranClientId",
              tranType:"$trans.actTranType",
              tranSubType:"$trans.actTranSubType",
              tranCreateDate:"$trans.createdAt",
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.actTranIssueName",""]},{"$eq":["$trans.actTranIssueName",null]}]},
                  then:"$trans.actTranProjectDescription",
                  else:"$trans.actTranIssueName"},
              },
              participantEntityIds:{
                $setUnion:[
                  "$trans.participants.partFirmId",
                  ["$trans.actTranClientId", "$trans.actTranFirmId" ]
                ]
              },
              docDetails:{
                $setUnion:[
                  {$map: {
                    input: "$trans.actTranDocuments",
                    as: "documents",
                    in: {
                      docId:"$$documents._id",
                      fileName:"$$documents.docFileName",
                      docCategory:"$$documents.docCategory",
                      docSubCategory:"$$documents.docSubCategory",
                      docType:"$$documents.docType",
                      docAWSReference:"$$documents.docAWSFileLocation",
                      uploadedBy:"$$documents.createdUserName",
                      docUploadUserId:"$$documents.createdBy",
                      uploadDate:"$$documents.LastUpdatedDate"
                    }
                  },
                  }]
              },

              participantDetails:{
                $setUnion:[
                  {$map: {
                    input: "$trans.participants",
                    as: "participants",
                    in: {
                      partUserId: "$$participants.partContactId" ,
                      partUserName: "$$participants.partContactName",
                      partEntityId:"$$participants.partFirmId",
                      partUserRole : "$$participants.partType",
                      partEntityName:"$$participants.partFirmName",
                    }
                  },
                  }]
              },

              primarySector: "$trans.actTranPrimarySector",
              secondarySector: "$trans.actTranSecondarySector",
              participantUserIds:{
                $setUnion:[
                  "$trans.participants.partContactId",
                  "$trans.actTranAssignedTo",
                  "$trans.actTranDocuments.createdBy",
                  ["$trans.createdByUser" ]
                ]
              }
            }
          },
          ...commonPipelineStagesForAllTransactions
        ],
        "RFPS":[
          {
            $lookup:{
              from: "tranagencyrfps",
              localField: "tenantId",
              foreignField: "rfpTranClientId",
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          {
            $project:{
              _id:0,
              tranId:"$trans._id",
              tenantId:1,
              tenantName:"$trans.actTranFirmName",
              issuerName:"$trans.rfpTranIssuerFirmName",
              issuerId:"$trans.rfpTranIssuerId",
              tranType:"$trans.rfpTranType",
              tranSubType:"$trans.rfpTranSubType",
              tranCreateDate:"$trans.createdAt",
              activityDescription : {
                $cond:{if:{$or:[{"$eq":["$trans.rfpTranIssueName",""]},{"$eq":["$trans.rfpTranIssueName",null]}]},
                  then:"$trans.rfpTranProjectDescription",
                  else:"$trans.rfpTranIssueName"},
              },
              participantEntityIds:{
                $setUnion:[
                  "$trans.rfpEvaluationTeam.rfpSelEvalFirmId",
                  "$trans.rfpProcessContacts.rfpProcessFirmId",
                  "$trans.rfpParticipants.rfpParticipantFirmId",
                  ["$trans.actTranClientId", "$trans.actTranFirmId" ]
                ]
              },
              participantDetails:{
                $setUnion:[
                  {$map: {
                    input: "$trans.rfpEvaluationTeam",
                    as: "participants",
                    in: {
                      partUserId: "$$participants.rfpSelEvalContactId" ,
                      partUserName: "$$participants.rfpSelEvalContactName",
                      partEntityId:"$$participants.rfpSelEvalFirmId",
                      partUserRole : "$$participants.rfpSelEvalRole",
                      partEntityName:"",
                    }
                  },
                  },
                  {$map: {
                    input: "$trans.rfpProcessContacts",
                    as: "participants",
                    in: {
                      partUserId: "$$participants.rfpProcessContactId" ,
                      partUserName: "$$participants.rfpContactName",
                      partEntityId:"$$participants.rfpProcessFirmId",
                      partUserRole : "$$participants.rfpContactFor",
                      partEntityName:"",
                    }
                  },
                  },
                  {$map: {
                    input: "$trans.rfpParticipants",
                    as: "participants",
                    in: {
                      partUserId: "$$participants.rfpParticipantContactId" ,
                      partUserName: "$$participants.rfpParticipantContactName",
                      partEntityId:"$$participants.rfpParticipantFirmId",
                      partUserRole : "$$participants.rfpParticipantRealMSRBType",
                      partEntityName:"$$participants.rfpParticipantFirmName",
                    }
                  },
                  }
                ]
              },

              docDetails:{
                $setUnion:[
                  {$map: {
                    input: "$trans.rfpBidDocuments",
                    as: "documents",
                    in: {
                      docId:"$$documents._id",
                      fileName:"$$documents.docFileName",
                      docCategory:"$$documents.docCategory",
                      docSubCategory:"$$documents.docSubCategory",
                      docType:"$$documents.docType",
                      docAWSReference:"$$documents.docAWSFileLocation",
                      uploadedBy:"$$documents.createdUserName",
                      docUploadUserId:"$$documents.createdBy",
                      uploadDate:"$$documents.LastUpdatedDate"
                    }
                  },
                  }]
              },


              primarySector: "$trans.rfpTranPrimarySector",
              secondarySector: "$trans.rfpTranSecondarySector",
              participantUserIds:{
                $setUnion:[
                  "$trans.rfpEvaluationTeam.rfpSelEvalContactId",
                  "$trans.rfpProcessContacts.rfpProcessContactId",
                  "$trans.rfpParticipants.rfpParticipantContactId",
                  "$trans.actTranAssignedTo",
                  "$trans.rfpBidDocuments.createdBy",
                  ["$trans.createdByUser" ]
                ]
              }
            }
          },
          ...commonPipelineStagesForAllTransactions
        ],

        "ENTITYDOCS":[
          {
            $lookup:{
              from: "entityrels",
              localField: "tenantId",
              foreignField: "entityParty1",
              as: "alltenantentities"
            }
          },
          {
            $unwind:"$alltenantentities"
          },
          {
            $project:{
              tenantId:1,
              entityId:"$alltenantentities.entityParty2",
              relationshipType:"$alltenantentities.relationshipType"
            }
          },
          {
            $lookup:{
              from: "entities",
              localField: "entityId",
              foreignField: "_id",
              as: "allentitydetails"
            }
          },
          {
            $unwind:"$allentitydetails"
          },
          {
            $project: {
              entityId:1,
              tenantId:1,
              relationshipType:1,
              entityName:"$allentitydetails.firmName",
              primarySector:"$allentitydetails.primarySectors",
              secondarySectors:"$allentitydetails.secondarySectors",
              docDetails:{
                $setUnion:[
                  {$map: {
                    input: "$allentitydetails.entityDocuments",
                    as: "documents",
                    in: {
                      docId:"$$documents._id",
                      fileName:"$$documents.docFileName",
                      docCategory:"$$documents.docCategory",
                      docSubCategory:"$$documents.docSubCategory",
                      docType:"$$documents.docType",
                      docAWSReference:"$$documents.docAWSFileLocation",
                      uploadedBy:"$$documents.createdUserName",
                      docUploadUserId:"$$documents.createdBy",
                      uploadDate:"$$documents.LastUpdatedDate"
                    }
                  },
                  }]
              },
            }
          },
          {
            $match:{docDetails:{$ne:null}}
          }

        ]
      }
    }
  ])

  if(isEmpty(data)) {
    return {}
  }
  return data[0]

}

export const getAllKeysForDocumentIndexService = async() => {

  // Get all Munivisor Clients
  // For each of them get the List of Deals, RFPs, Bankloans etc etc.
  // For each of them get the clients associated with the entities
  // For each of them get the prospects associated with the tenant
  // For each of them get the third parties associated with the tenant
  // Get the supervisory and other details
  // Get the Billing and other details

  const tenants= await Entity.find({isMuniVisorClient:true}).select("_id")
  const allTenantDetails = await tenants.reduce ( async (acc, {_id:tenantId}) =>  {
    const newacc = await acc
    const tenantInfo = await getAllDetailsForTenant(tenantId)

    const flattenedDocIds = Object.keys(tenantInfo).reduce ( (acc, k) => {
      const arrData = tenantInfo[k].reduce( (allDocIds, tr) => {
        const allDocIdsToStrings = tr.docDetails.map( aa => aa.docAWSReference.toString())
        return [ ...new Set([...allDocIds, ...allDocIdsToStrings])]
      },[])
      return [ ...new Set([...acc, ...arrData])]
    },[])

    const flattenedConvertedToObjectIDs = flattenedDocIds.map( dd => ObjectID(dd))

    const awsDocdetails = await Docs.find({_id:{"$in":flattenedConvertedToObjectIDs}}).select({_id:1,name:1})
    const awsDocDetailsObject = awsDocdetails.reduce ( (acc, a) => {
      return {...acc,...{[a._id.toString()]:a.name}}
    },{})

    const revisedObjectsWithAWSFileName = Object.keys(tenantInfo).reduce ( (acc, k) => {
      const allConvertedTransactions = tenantInfo[k].reduce( (allTransactions, tr) => {
        const newDocDetails = tr.docDetails.map( aa => ({...aa,...{awsFileName:awsDocDetailsObject[aa.docAWSReference]}}))
        return [...allTransactions,{...tr,docDetails:newDocDetails}]
      },[])
      return {...acc, [k]:allConvertedTransactions}
    },{})


    const allUrlDetailsForTenant = await tenantUrlGenerator(tenantId)

    const revisedTenantInfo = {
      [TENANT_DOC_ORGANIZATION.DEALS]:helperGetIds(revisedObjectsWithAWSFileName.DEALS,TENANT_DOC_ORGANIZATION.DEALS,"tranId"),
      [TENANT_DOC_ORGANIZATION.RFPS]:helperGetIds(revisedObjectsWithAWSFileName.RFPS,TENANT_DOC_ORGANIZATION.RFPS,"tranId"),
      [TENANT_DOC_ORGANIZATION.BANKLOANS]:helperGetIds(revisedObjectsWithAWSFileName.BANKLOANS,TENANT_DOC_ORGANIZATION.BANKLOANS,"tranId"),
      [TENANT_DOC_ORGANIZATION.DERIVATIVES]:helperGetIds(revisedObjectsWithAWSFileName.DERIVATIVES,TENANT_DOC_ORGANIZATION.DERIVATIVES,"tranId"),
      [TENANT_DOC_ORGANIZATION.MARFPS]:helperGetIds(revisedObjectsWithAWSFileName.MARFPS,TENANT_DOC_ORGANIZATION.MARFPS, "tranId"),
      [TENANT_DOC_ORGANIZATION.OTHERS]:helperGetIds(revisedObjectsWithAWSFileName.MARFPS,TENANT_DOC_ORGANIZATION.OTHERS, "tranId"),
      [TENANT_DOC_ORGANIZATION.ENTITYDOCS]:helperGetIds(revisedObjectsWithAWSFileName.ENTITYDOCS,TENANT_DOC_ORGANIZATION.ENTITYDOCS,"entityId"),
      tenantUrls:{...allUrlDetailsForTenant}
    }

    return Promise.resolve({...newacc, ...{[tenantId]:revisedTenantInfo}})
  },Promise.resolve({}))
  return allTenantDetails
}
