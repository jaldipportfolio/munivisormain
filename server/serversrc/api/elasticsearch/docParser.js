/* eslint-disable */
import fs from "fs-extra"
import axios from "axios"
import AWS from "aws-sdk"
import path from "path"
import _ from "lodash"
import moment from "moment"
import { getAllKeysForDocumentIndexService } from "./getAllTenantInfoForDocSearch"
import { TENANT_DOC_ORGANIZATION, AWS_ACCESS_KEY, AWS_SECRET, ELASTIC_SEARCH_DOC_INDEX, ELASTIC_SEARCH_DOC_URL, ALLOWED_FILE_TYPES } from "./constants"
import { getHeaders, putData } from "./esHelper"

const PDFParser = require("pdf2json");
const pdfParser = new PDFParser(this,1);

const MAX_FILE_SIZE = 4097152
const rootPath = path.join()
const logPath = `${rootPath}/logs`
const docFileLogs = `${logPath}/doc_logs.txt`
const errorFileLogs = `${logPath}/error_doc_logs.txt`
const tmpPath = `${rootPath}/tmp`
const tmpPathFiles = `${tmpPath}/files`
const tmpCurlFiles = `${tmpPath}/curl`
const bucketName = process.env.S3BUCKET
const s3 = new AWS.S3({
  accessKeyId: process.env.S3ACCESSKEYID,
  secretAccessKey: process.env.S3SECRETKEY,
  maxRetries: 30,
  httpOptions: {
    connectTimeout: 50000000,
    timeout: 50000000
  }
})

// function to encode file data to base64 encoded string
const base64Encode = (file) => {
  const buffer = fs.readFileSync(file)
  return Buffer.from(buffer).toString("base64")
}

// function to index doc file on elasticsearch
const indexDocs = (fileList) => {
  const indexedFiles = []
  const promises = fileList.map((f, i) => (
    new Promise((resolve) => {
      const file = f.filename
      const id = f.indexingTags.docDetails.docId
      const url = `${ELASTIC_SEARCH_DOC_URL}${ELASTIC_SEARCH_DOC_INDEX}/doc_no_base64/${id}`
      console.log('Preparing request', id, file)

      if (file.indexOf('pdf') > -1) {
        pdfParser.on("pdfParser_dataError", errData => {
          fs.appendFileSync(errorFileLogs, `Unable to read => ${f.Key}\n${errData}\n\n`)
        });
        pdfParser.on("pdfParser_dataReady", pdfData => {
          if (pdfData) {
            const data = {
              attachment_data: Buffer.from(pdfParser.getRawTextContent()).toString('base64'),
              link: f.link,
              tranId: f.tranId,
              ...f.indexingTags,
            }

            axios.put(`${url}?pipeline=docs_attachment`, data, {
              headers: getHeaders(),
              timeout: 50000000,
            }).then((iData) => {
              indexedFiles.push(f)
              console.log(iData.data, Date())
              resolve(iData)
            }).catch(err => {
              fs.appendFileSync(errorFileLogs, `Attachment Pilpeline Failed => ${f.Key}\n${err}\n\n`)
              resolve()
            })
          } else {
            fs.appendFileSync(errorFileLogs, `Unable to read data from pdf => ${f.Key}\n\n`);
          }
        });

        pdfParser.loadPDF(file);
      } else {
        const fname = f.Key.replace(/\//g, "_")
        const data = {
          attachment_data: base64Encode(file),
          link: f.link,
          tranId: f.tranId,
          ...f.indexingTags,
        }

        axios.put(`${url}?pipeline=docs_attachment`, data, {
          headers: getHeaders(),
          timeout: 50000000,
        }).then((iData) => {
          indexedFiles.push(f)
          resolve(iData)
        }).catch(err => {
          fs.appendFileSync(errorFileLogs, `Attachment Pilpeline Failed => ${f.Key}\n${err}\n\n`)
          resolve()
        })
      }
    })
  ))

  return Promise.all(promises).then(() => indexedFiles)
}

// function to download files on local
const fileDownloader = (fileList) => {
  const files = []
  const promises = fileList.map((f) => {
    const url = f.Key
    const filename = `${tmpPathFiles}/${url}`
    fs.ensureFileSync(filename)
    return new Promise((resolve) => {
      s3.getObject({
        Bucket: bucketName,
        Key: url
      }).createReadStream().on("error", (err) => {
        fs.appendFileSync(errorFileLogs, `S3 Failed => ${url}\n${JSON.stringify(err)}\n`)
        resolve(err)
      }).pipe(fs.createWriteStream(filename)).on("close", () => {
        fs.appendFileSync(docFileLogs, `\nDownloaded => ${url}`)
        files.push({
          ...f,
          filename,
        })
        resolve()
      }).on("error", (ferr) => {
        fs.appendFileSync(errorFileLogs, `${new Date()} Failed to write file=> ${url}\n${JSON.stringify(ferr)}\n`)
        fs.appendFileSync(docFileLogs, url)
        resolve()
      })
    })
  })

  return Promise.all(promises).then(() => files)
}

// function to list s3 objects v2
const listS3V2 = (client) => {
  const docs = []
  const MAX_S3_CHUNK = 1000

  const listObjectParams = {
    Delimiter: "/",
    Bucket: bucketName,
    EncodingType: "url",
    MaxKeys: MAX_S3_CHUNK
  }
  const listPrmoises = Object.keys(TENANT_DOC_ORGANIZATION).forEach(type => {
    const tranPromises = client[TENANT_DOC_ORGANIZATION[type]].forEach((obj) => {
      const { docDetails, ...rest } = obj
      docDetails.forEach(d => {
        docs.push({
          Key: `${client.tenantId}/${type}/${obj.tranAWSKey}/${d.awsFileName}`,
          docDetails: d,
          ...rest,
        })
      })
      // console.log(JSON.stringify(tranId))
      // return new Promise((resolve) => {
      //   s3.listObjectsV2({...listObjectParams, Prefix: `${client.tenantId}/${type}/${obj.tranAWSKey}/`}, (err, data) => {
      //     if(err) {
      //       fs.appendFileSync(errorFileLogs, `${new Date()} Failed to List file=> ${ `${client.tenantId}/${type}/${tranId}/`}\n${JSON.stringify(err)}\n`)
      //       resolve()
      //     } else {
      //       data.Contents.forEach((file) => {
      //         const extension = file.Key.split(".").pop()
      //         if (_.includes(ALLOWED_FILE_TYPES, extension)) {
      //           docs.push({
      //             Key: file.Key,
      //             tranId: tranId.split("_")[1]
      //           })
      //         }
      //       })
      //       resolve()
      //     }
      //   })
      // })
    })

    // return Promise.all(tranPromises).then(() => docs)
  })
  // return Promise.all(listPrmoises).then(() => docs)
  return docs
}

const checkFileSize = (docs) => {
  const finaldocs = []
  const docsPromise = docs.map(doc => (
    new Promise(resolve => {
      s3.headObject({
        Bucket: bucketName,
        Key: doc.Key
      }, (err, data) => {
        if (data && data.ContentLength <= MAX_FILE_SIZE) {
          finaldocs.push(doc)
        }
        resolve()
      })
    })
  ))

  return Promise.all(docsPromise).then(() => finaldocs)
}

// function filter doc based on tags
const getUnparsedDocs = (docs) => {
  const unparsedDocs = []
  const tagsPromise = docs.map(doc => (
    new Promise(resolve => {
      s3.getObjectTagging({
        Bucket: bucketName,
        Key: doc.Key
      }, (tagErr, tagData) => {
        const tagSet = (tagData || {}).TagSet || []
        const tagslist = tagSet.map(tag => tag.Key)
        const indexingTags = doc
        if (tagslist.indexOf("parsed_at") === -1) {
          unparsedDocs.push({
            Key: doc.Key,
            TagSet: tagSet,
            indexingTags,
            tranId: doc.tranId,
            link: `https://s3.amazonaws.com/${bucketName}/${doc.Key}`
          })
        }
        resolve()
      })
    })
  ))

  return Promise.all(tagsPromise).then(() => unparsedDocs)
}

// function to put tags
const putParsedTag = (docs) => {
  const taggedFiles = []
  const promises = docs.map(doc => (
    new Promise((resolve) => {
      s3.putObjectTagging({
        Bucket: bucketName,
        Key: doc.Key,
        Tagging: {
          TagSet: [...doc.TagSet, { Key: "parsed_at", Value: moment().format() }]
        }
      }, () => {
        console.log(`File parsed successfully: ${doc.Key}`)
        taggedFiles.push(doc)
        resolve()
      })
    })
  ))

  return Promise.all(promises).then(() => taggedFiles)
}

/**
 *
 * @param {*} docList
 * function to parse docs basd on given chunk
 */
export const docParser = async (docList) => {
  fs.ensureDirSync(logPath)
  fs.ensureFileSync(docFileLogs)
  fs.ensureFileSync(errorFileLogs)
  fs.ensureDirSync(tmpPath)
  fs.ensureDirSync(tmpPathFiles)
  fs.ensureDirSync(tmpCurlFiles)
  // const CHUNK_SIZE = 20

  try {
    const unparsedDocs = await getUnparsedDocs(docList)
    const eligibleDocs = await checkFileSize(unparsedDocs)
    const downLoadedFiles = await fileDownloader(eligibleDocs)
    const indexedFiles = await indexDocs(downLoadedFiles)
    const taggedFiles = await putParsedTag(indexedFiles)

    for (let i = 0; i < taggedFiles.length; i++) {
      fs.removeSync(`${tmpPathFiles}/${taggedFiles[i].Key}`)
    }

    console.log('Chunk completed')

    return taggedFiles
  } catch(err) {
    throw err
  }
}

/**
 *
 * @param {*}
 * @description main doc parsing function
 */
export const docParserRequest = async () => {
  const clientData = await getAllKeysForDocumentIndexService()
  const docList = []
  const PARALLE_CHUNK_SIZE = 4
  const pipeline = await axios.put(`${ELASTIC_SEARCH_DOC_URL}_ingest/pipeline/docs_attachment?pretty`, {
    "description" : "Extract attachment information",
    "processors" : [
      {
        "attachment" : {
          "field" : "attachment_data",
          "indexed_chars": -1
        }
      }
    ]
  }, {
    headers: getHeaders(),
    timeout: 50000000
  })
  console.log(pipeline.data)
  Object.keys(clientData).forEach(client => {
    const clientInfo = {
      tenantId: client,
      ...clientData[client]
    }
    docList.push(...listS3V2(clientInfo))
  })

  const chunks = _.chunk(docList, PARALLE_CHUNK_SIZE)
  console.log("******** PROCESS STARTED AT *************", Date())
  console.log("******** NUMBER OF DOCS ****************", docList.length)
  console.log("******** PARALLEL PROCESSING DOCS ****************", PARALLE_CHUNK_SIZE)
  console.log("******** CHUNKS ****************", chunks.length)

  for(let chunk of chunks) {
    try {
      const data = await docParser(chunk)
    } catch(err) {
      fs.appendFileSync(errorFileLogs, `Final Error => \n${err}\n`)
    }
  }
}
