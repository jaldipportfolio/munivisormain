import axios from "axios"
import { getHeaders } from "./esHelper"
import { metaDataConfig } from "./config"

require("dotenv").config()

const clusterUrl = process.env.ES_HOST
const metaIndex = "metadata"
const metaUrl = `${clusterUrl}${metaIndex}`
const ALLOWED_STATES = ["READY", "COMPLETED", "PROCESSING", "FAILED"]
const DATE_FIELDS = ["createdAt", "updatedAt"]
/**
 *
 * @param {*} tenantIds // array of tenantIds
 * @param {*} states array of states; READY | COMPLETED | PROCESSING | FAILED
 * @param {*} dateRange { startDate: '2019-03-15', endDate: '2019-03-16'}
 * @param {*} dateField createdAt | updatedAt
 * @param {*} documentIds list of docId as per mongodb
 * @param {*} documentVersionIds list of ids which is combination of docIds~versionIds
 */
const queryBuilder = ({
  tenantIds,
  states,
  dateRange,
  dateField = "updatedAt",
  documentIds,
  documentVersionIds
}) => {
  const query = {
    bool: {
      must: []
    }
  }

  if (tenantIds && tenantIds.length) {
    query.bool.must.push({
      terms: {
        "tenantId.keyword": tenantIds
      }
    })
  }

  if (states && states.length) {
    const filteredStates = states.filter(
      item => ALLOWED_STATES.indexOf(item) > -1
    )
    query.bool.must.push({
      term: {
        "state.keyword": filteredStates
      }
    })
  }

  if (
    dateRange &&
    dateRange.startDate &&
    dateRange.endDate &&
    dateField &&
    DATE_FIELDS.indexOf(dateField) > -1
  ) {
    query.bool.must.push({
      range: {
        [dateField]: {
          gte: dateRange.startDate,
          lte: dateRange.endDate
        }
      }
    })
  }

  if (documentIds && documentIds.length) {
    query.bool.must.push({
      terms: {
        "mid.keyword": documentIds
      }
    })
  }

  if (documentVersionIds && documentVersionIds.length) {
    query.bool.must.push({
      ids: {
        values: documentVersionIds
      }
    })
  }

  if (query.bool.must.length === 0) {
    query.bool.must.push({
      match_all: {}
    })
  }

  console.log("meta index query", JSON.stringify(query))

  return query
}

export const dropMetaIndex = async () => {
  try {
    const { data } = await axios.delete(metaUrl, {
      headers: getHeaders()
    })
    return data
  } catch (err) {
    throw err
  }
}

export const getDocs = async reqData => {
  try {
    const { data: countData } = await axios.post(
      `${metaUrl}/_count`,
      {
        query: { ...queryBuilder(reqData) }
      },
      {
        headers: getHeaders()
      }
    )

    const { data } = await axios.post(
      `${metaUrl}/_search`,
      {
        size: countData.count,
        query: { ...queryBuilder(reqData) }
      },
      {
        headers: getHeaders()
      }
    )

    return data
  } catch (err) {
    throw err
  }
}

export const deleteDocs = async reqData => {
  try {
    const { data } = await axios.post(
      `${metaUrl}/_delete_by_query`,
      { query: { ...queryBuilder(reqData) } },
      {
        headers: getHeaders()
      }
    )

    return data
  } catch (err) {
    throw err
  }
}

export const updateState = async reqData => {
  try {
    const { data } = await axios.post(
      `${metaUrl}/_update_by_query?conflicts=proceed`,
      {
        query: { ...queryBuilder(reqData) },
        script: {
          inline: `ctx._source.state="${reqData.newState}";`
        }
      },
      {
        headers: getHeaders()
      }
    )

    return data
  } catch (err) {
    throw err
  }
}

export const checkMetaIndex = async () => {
  try {
    await axios.head(metaUrl)
    return { success: true }
  } catch (error) {
    return { success: false }
  }
}

export const initMetaIndex = async () => {
  try {
    const { success } = await checkMetaIndex()
    if (!success) {
      const apiRes = await axios.put(`${clusterUrl}metadata`, metaDataConfig, {
        headers: getHeaders()
      })
      return apiRes.data
    }
    return {
      acknowledged: true
    }
  } catch (err) {
    throw err
  }
}
