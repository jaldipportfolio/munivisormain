import { generateControllers } from "../../modules/query"
import  { EntityUser,Deals,RFP,ActMaRFP,Derivatives,BankLoans,Others }  from "./../models"
import {canUserPerformAction} from "../../commonDbFunctions"
import {manageTasksForProcessChecklists} from "../taskmanagement/taskProcessInteraction"
import { checkTranEligiblityForLoggedInUser } from "../../commonDbFunctions/getEligibleTransactionsForLoggedInUser"
import { putData } from "../../elasticsearch/esHelper"


const {ObjectID} = require("mongodb")

const postDealTransaction = () => async (req, res) => {

  const resRequested = [
    {resource:"Deals",access:2},
  ]

  try {
    const entitled = await canUserPerformAction(req.user,resRequested)
    if(entitled) {
      const newDeal = new Deals({
        ...req.body
      })
      const dealTransaction = await newDeal.save()
      const data = await putData("tranagencydeals", dealTransaction._id, dealTransaction._doc)
      console.log("The return data from elastic search", data)
      res.json(dealTransaction)
    }
    else {
      res.status(500).send({done:false,error:"User not entitled to create transaction"})
    }
  } catch (error) {
    res.status(500).send({done: false,error: "Error saving information on Deals"})
  }

}


const getDealTransactionDetails = () => async (req, res) => {
  const { type, dealId } = req.params
  const resRequested = [
    {resource:"Deals",access:1},
  ]
  try {
    const entitled = await canUserPerformAction(req.user,resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, dealId)

    console.log("------CHECKING THE ENTITLEMENTS THAT ARE BEING RRETURNED FROM SERVICE", {entitled,isEligible})

    if( !entitled || (isEligible && !isEligible.canViewTran)) {
      return res.status(500).send({done:false,error:"User is not entitled to access the requested services",success:"",requestedServices:resRequested})
    }

    let select = ""
    if(type === "summary") {
      select = "dealIssueTranClientId dealIssueTranAssignedTo dealIssueTranClientId dealIssueTranIssuerId dealIssueTranType dealIssueTranName dealIssueTranIssuerFirmName rfpTranIsConduitBorrowerFlag " +
        "dealIssueTranIssueName dealIssueTranProjectDescription dealIssueTranSubType dealIssueGuarantorName dealIssueParticipants dealIssueParAmount dealIssueTransNotes dealIssueTranStatus dealIssueBorrowerName dealIssueSecurityType dealIssueTranRelatedTo"
    }
    if(type === "details") {
      select = "dealIssueTranClientId dealIssueTranAssignedTo dealIssueTranIssuerFirmName dealIssueTranClientId dealIssueTranIssuerId dealIssueTranType dealIssueTranSubType " +
        "dealIssueParticipants dealIssueTranName dealIssueTranPurposeOfRequest dealIssueTranOtherTransactionType dealIssueTranState dealIssueTranCounty dealIssueTranPrimarySector dealIssueTranRelatedTo " +
        "dealIssueTranSecondarySector dealIssueTranDateHired dealIssueTranStartDate dealIssueTranExpectedEndDate dealIssueTransNotes dealIssueCostOfIssuanceNotes created_at " +
        "dealIssueBorrowerName dealIssueGuarantorName dealIssueTranStatus dealIssueofferingType dealIssueSecurityType dealIssueBankQualified dealIssueCorpType dealIssueParAmount " +
        "dealIssuePricingDate dealIssueExpAwardDate dealIssueActAwardDate dealIssueSdlcCreditPerc dealIssueEstimatedRev dealIssueEstimatedRev dealIssueUseOfProceeds dealIssuePlaceholder dealIssueTranIssueName dealIssueTranProjectDescription"
    }
    if(type === "participants") {
      select = "dealIssueTranClientId  dealIssueTranIssuerId dealIssueTranAssignedTo dealIssueTranIssuerFirmName dealIssueTranClientFirmName dealIssueUnderwriters dealIssueTranType dealIssueTranSubType dealIssueParticipants dealIssueTranSubType dealIssueTranIssueName dealIssueTranProjectDescription"
    }
    if(type === "documents") {
      select = "dealIssueTranClientId  dealIssueTranIssuerId dealIssueTranClientFirmName dealIssueTranType dealIssueTranSubType dealIssueParticipants " +
        "dealIssueDocuments dealIssueTranClientId dealIssueTranSubType dealIssueTranIssuerFirmName dealIssueTranProjectDescription"
    }
    if(type === "rating") {
      select = "dealIssueTranClientId  dealIssueTranIssuerId dealIssueTranType dealIssueTranSubType dealIssueSeriesDetails.seriesRatings dealIssueSeriesDetails.cepRatings dealIssueParticipants dealIssueTranIssueName dealIssueTranProjectDescription"
    }
    if(type === "contract") {
      select = "dealIssueTranClientId dealIssueTranIssuerId dealIssueTradingInformation dealIssueTranSubType dealIssueParticipants dealIssueContractInformation dealIssueReceiveLeg dealIssuePayLeg dealIssueCompliance dealIssueTranIssueName dealIssueTranProjectDescription"
    }
    if(type === "pricing") {
      select = "dealIssueTranClientId dealIssueTranIssuerId dealIssueTranSubType dealIssueSeriesDetails._id dealIssueSeriesDetails.seriesName dealIssueSeriesDetails.description " +
        "dealIssueSeriesDetails.tag dealIssueParticipants dealIssueTranProjectDescription dealIssueTranIssueName"
    }
    if(type === "series" || type === "ratings") {
      select = "dealIssueTranClientId dealIssueTranSubType dealIssueSeriesDetails.seriesRatings dealIssueSeriesDetails.cepRatings dealIssueSeriesDetails._id dealIssueSeriesDetails.seriesName dealIssueSeriesDetails.description " +
        "dealIssueSeriesDetails.tag dealIssueParticipants dealIssueTranProjectDescription dealIssueTranIssueName"
    }
    if(type === "check-track") {
      select = "dealIssueTranClientId dealIssueTranIssuerId dealIssueCostOfIssuance dealIssueTranSubType dealIssueScheduleOfEvents dealIssueCostOfIssuanceNotes dealIssueParticipants dealIssueTranProjectDescription dealIssueTranIssueName dealChecklists"
    }
    if(type === "audit-trail") {
      select = "dealIssueTranClientId dealIssueTranIssuerId"
    }
    const deals = await Deals.findById({_id: dealId}).select(select).populate({
      path: "dealIssueTranAssignedTo",
      model: EntityUser,
      select: "userFirstName userLastName userFirmName entityId userPhone userEmails userAddresses"
    })
    res.json(select ? deals : {})
  } catch(error) {
    res.status(422).send({done:false,error:"There is a failure in retrieving information for entitlements",success:"",requestedServices:resRequested})
  }
}

const updateDealTransaction = () => async (req, res) => {
  const { type, dealId } = req.params
  const resRequested = [
    {resource:"Deals",access:2},
  ]
  try {
    const entitled = await canUserPerformAction(req.user,resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, dealId)

    if( !entitled || (isEligible && !isEligible.canEditTran)) {
      return res.status(422).send({done:false,error:"User is not entitled to access the requested services",success:"",requestedServices:resRequested})
    }

    let select = ""
    if(type === "details" || type === "summary") {
      select = "dealIssueTranClientId dealIssueTranAssignedTo dealIssueTranIssuerId dealIssueTranType dealIssueTranName dealIssueTranPurposeOfRequest " +
        "dealIssueTranOtherTransactionType dealIssueTranState dealIssueTranCounty dealIssueTranPrimarySector dealIssueTranSecondarySector dealIssueTranDateHired " +
        "dealIssueTranStartDate dealIssueTranExpectedEndDate dealIssueTransNotes dealIssueCostOfIssuanceNotes created_at dealIssueBorrowerName dealIssueGuarantorName " +
        "dealIssueTranStatus dealIssueofferingType dealIssueSecurityType dealIssueBankQualified dealIssueCorpType dealIssueParAmount dealIssuePricingDate dealIssueExpAwardDate " +
        "dealIssueActAwardDate dealIssueSdlcCreditPerc dealIssueEstimatedRev dealIssueEstimatedRev dealIssueUseOfProceeds dealIssuePlaceholder"
      await Deals.findByIdAndUpdate({_id: dealId}, req.body)
    }
    if(type === "documents") {
      select = "dealIssueTranClientId  dealIssueTranIssuerId dealIssueTranType dealIssueDocuments"
      await Deals.update({ _id: dealId}, {$addToSet: {dealIssueDocuments: req.body.dealIssueDocuments} })
    }
    if(type === "contract") {
      select = "dealIssueTranClientId dealIssueTranIssuerId dealIssueTradingInformation dealIssueContractInformation dealIssueReceiveLeg dealIssuePayLeg dealIssueCompliance"
      await Deals.update({ _id: dealId}, req.body)
    }
    if(type === "series") {
      select = "dealIssueTranClientId dealIssueTranIssuerId dealIssueSeriesDetails.seriesName dealIssueSeriesDetails.seriesPricingDetails dealIssueSeriesDetails.seriesPricingData dealIssueSeriesDetails.description dealIssueSeriesDetails.tag"
      await Deals.update({ _id: dealId}, {$addToSet: {dealIssueSeriesDetails: req.body.dealIssueSeriesDetails} })
    }
    if(type === "check-track") {
      select = "dealIssueTranClientId dealIssueTranIssuerId dealChecklists"
      await Deals.update({ _id: dealId}, req.body)
    }
    const deals = await Deals.findOne({_id: dealId}).select(select)
    await manageTasksForProcessChecklists(dealId, req.user)
    res.json(select ? deals : {})
  } catch(error) {
    console.log("*********Deal********updateDealTransaction**************",error)
    res.status(422).send({done:false,error:"There is a failure in retrieving information for entitlements",success:"",requestedServices:resRequested})
  }
}

const removeParticipantByPartId = () => async (req, res) => {
  const { type, dealId } = req.params
  const { removeFrom } = req.query
  const resRequested = [
    {resource:"Deals",access:2},
  ]
  try {
    const entitled = await canUserPerformAction(req.user,resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, type)

    if( !entitled || (isEligible && !isEligible.canEditTran)) {
      return res.status(422).send({done:false,error:"User is not entitled to access the requested services",success:"",requestedServices:resRequested})
    }
    if(removeFrom === "dealIssueParticipants") {
      await Deals.update({ _id: type}, { $pull: { "dealIssueParticipants": { _id: dealId } }})
    }
    if(removeFrom === "dealIssueUnderwriters") {
      await Deals.update({ _id: type}, { $pull: { "dealIssueUnderwriters": { _id: dealId } }})
    }
    await manageTasksForProcessChecklists(type, req.user)
    const deal = await Deals.findOne({_id: type}).select(`${removeFrom}`)
    res.json(removeFrom ? deal :  "Not found")
  } catch(error) {
    res.status(422).send({done:false,error:"There is a failure in retrieving information for entitlements",success:"",requestedServices:resRequested})
  }
}

const getDealRatings = () => async (req, res) => {
  const { contactId, dealId } = req.params
  const resRequested = [
    {resource:"Deals",access:1},
  ]
  try {
    const entitled = await canUserPerformAction(req.user,resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, dealId)

    if( !entitled || (isEligible && !isEligible.canViewTran)) {
      return res.status(422).send({done:false,error:"User is not entitled to access the requested services",success:"",requestedServices:resRequested})
    }

    let deals = await Deals.aggregate([
      { $match: {_id: ObjectID(dealId)}},
      { $unwind: "$dealIssueSeriesDetails"},
      { $match: {"dealIssueSeriesDetails.contactId": ObjectID(contactId)}},
      { $project: {
        dealIssueTranClientId: 1,
        "dealIssueSeriesDetails.contactId": 1,
        "dealIssueSeriesDetails.seriesRatings" : 1,
        "dealIssueSeriesDetails.cepRatings" : 1
      }
      }])
    deals = Array.isArray(deals) && deals.length ? deals[0] : deals
    res.json(deals)
  } catch(error) {
    res.status(422).send({done:false,error:"There is a failure in retrieving information for entitlements",success:"",requestedServices:resRequested})
  }
}

const removeDealRatingByRatingId = () => async (req, res) => {
  const { contactId, dealId } = req.params
  const { ratingId } = req.query
  const resRequested = [
    {resource:"Deals",access:2},
  ]
  const entitled = await canUserPerformAction(req.user,resRequested)
  const isEligible = await checkTranEligiblityForLoggedInUser(req.user, dealId)

  if( !entitled || (isEligible && !isEligible.canEditTran)) {
    return res.status(422).send({done:false,error:"User is not entitled to access the requested services",success:"",requestedServices:resRequested})
  }
  await Deals.findOne({ _id: dealId}).then(async (dealRating) => {
    const findIndex = dealRating.dealIssueSeriesDetails.findIndex(p => p.contactId.toString() === contactId)
    if(findIndex !== -1) {
      dealRating.dealIssueSeriesDetails[findIndex].seriesRatings = dealRating.dealIssueSeriesDetails[findIndex].seriesRatings.filter(x => x._id.toString() !== ratingId)
      dealRating.dealIssueSeriesDetails[findIndex].cepRatings = dealRating.dealIssueSeriesDetails[findIndex].cepRatings.filter(x => x._id.toString() !== ratingId)
      await dealRating.save()
      await manageTasksForProcessChecklists(dealId, req.user)
      res.json(dealRating)
    }else {
      res.json("contactId not found")
    }
  })
    .catch(error => {
      res.status(422).send({done:false,error:"There is a failure in retrieving information for entitlements",success:"",requestedServices:resRequested})
    })
}

const getDealSeriesBySeriesId = () => async (req, res) => {
  const { dealId, seriesId } = req.params
  const resRequested = [
    {resource:"Deals",access:1},
  ]
  try {
    const entitled = await canUserPerformAction(req.user,resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, dealId)

    if( !entitled || (isEligible && !isEligible.canViewTran)) {
      return res.status(422).send({done:false,error:"User is not entitled to access the requested services",success:"",requestedServices:resRequested})
    }
    let deals = await Deals.aggregate([
      { $match: {_id: ObjectID(dealId)}},
      { $unwind: "$dealIssueSeriesDetails"},
      { $match: {"dealIssueSeriesDetails._id": ObjectID(seriesId)}},
      { $project: {
        "dealIssueSeriesDetails._id" : 1,
        "dealIssueSeriesDetails.seriesName": 1,
        "dealIssueSeriesDetails.seriesPricingDetails": 1,
        "dealIssueSeriesDetails.seriesPricingData": 1,
        "dealIssueSeriesDetails.description": 1,
        "dealIssueSeriesDetails.tag": 1,
      }
      }])
    deals = Array.isArray(deals) && deals.length ? deals[0] : deals
    res.json(deals)
  } catch(error) {
    res.status(422).send({done:false,error:"There is a failure in retrieving information for entitlements",success:"",requestedServices:resRequested})
  }
}

const saveDealsPricingBySeriesId = () => async (req, res) => {
  const { seriesId, dealId } = req.params
  const { seriesPricingData, seriesPricingDetails } = req.body
  const resRequested = [
    {resource:"Deals",access:2},
  ]
  const entitled = await canUserPerformAction(req.user,resRequested)
  const isEligible = await checkTranEligiblityForLoggedInUser(req.user, dealId)

  if( !entitled || (isEligible && !isEligible.canEditTran)) {
    return res.status(422).send({done:false,error:"User is not entitled to access the requested services",success:"",requestedServices:resRequested})
  }
  return Deals.findOne({_id: dealId}).then(async (dealRating) => {
    const findIndex = dealRating.dealIssueSeriesDetails.findIndex(p => p._id.toString() === seriesId)
    if(findIndex !== -1) {
      dealRating.dealIssueSeriesDetails[findIndex].seriesPricingDetails = seriesPricingDetails
      dealRating.dealIssueSeriesDetails[findIndex].seriesPricingData = seriesPricingData
      await dealRating.save()
      await manageTasksForProcessChecklists(dealId, req.user)
      res.json(dealRating.dealIssueSeriesDetails[findIndex])
    } else {
      res.status(400).json("series not found")
    }
  })
    .catch(error => {
      res.status(422).send({done:false,error:"There is a failure in retrieving information for entitlements",success:"",requestedServices:resRequested})
    })
}

const delSeriesBySeriesId = () => async (req, res) => {
  const { seriesId, dealId } = req.params
  const resRequested = [
    {resource:"Deals",access:1},
  ]
  const entitled = await canUserPerformAction(req.user,resRequested)
  const isEligible = await checkTranEligiblityForLoggedInUser(req.user, dealId)

  if( !entitled || (isEligible && !isEligible.canEditTran)) {
    return res.status(422).send({done:false,error:"User is not entitled to access the requested services",success:"",requestedServices:resRequested})
  }
  await Deals.findOne({ _id: dealId}).then(async (dealSeries) => {
    dealSeries.dealIssueSeriesDetails = dealSeries.dealIssueSeriesDetails.filter(p => p._id.toString() !== seriesId)
    await dealSeries.save()
    await manageTasksForProcessChecklists(dealId, req.user)
    res.json(dealSeries.dealIssueSeriesDetails)
  })
    .catch(error => {
      res.status(422).send({done:false,error:"There is a failure in retrieving information for entitlements",success:"",requestedServices:resRequested})
    })
}

const saveDealSeriesBySeriesId = () => async (req, res) => {
  const { seriesId, dealId } = req.params
  const { seriesName, tag, description } = req.body
  const resRequested = [
    {resource:"Deals",access:2},
  ]
  const entitled = await canUserPerformAction(req.user,resRequested)
  const isEligible = await checkTranEligiblityForLoggedInUser(req.user, dealId)

  if( !entitled || (isEligible && !isEligible.canEditTran)) {
    return res.status(422).send({done:false,error:"User is not entitled to access the requested services",success:"",requestedServices:resRequested})
  }
  return Deals.findOne({_id: dealId}).then(async (dealSeries) => {
    const findIndex = dealSeries.dealIssueSeriesDetails.findIndex(p => p._id.toString() === seriesId)
    if(findIndex !== -1) {
      dealSeries.dealIssueSeriesDetails[findIndex].seriesName = seriesName
      dealSeries.dealIssueSeriesDetails[findIndex].tag = tag
      dealSeries.dealIssueSeriesDetails[findIndex].description = description
      await dealSeries.save()
      await manageTasksForProcessChecklists(dealId, req.user)
      res.json(dealSeries.dealIssueSeriesDetails[findIndex])
    } else {
      res.status(400).json("series not found")
    }
  })
    .catch(error =>{
      res.status(422).send({done:false,error:"There is a failure in retrieving information for entitlements",success:"",requestedServices:resRequested})
    })
}

const saveDealParticipantsbyPuId = () => async (req, res) => {
  const { type, dealId } = req.params
  const { _id } = req.body
  const resRequested = [
    {resource:"Deals",access:2},
  ]
  try {
    const entitled = await canUserPerformAction(req.user,resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, dealId)

    if( !entitled || (isEligible && !isEligible.canEditTran)) {
      return res.status(422).send({done:false,error:"User is not entitled to access the requested services",success:"",requestedServices:resRequested})
    }
    if(type === "underwriters") {
      if(_id) {
        await Deals.updateOne(
          { _id: dealId, "dealIssueUnderwriters._id": ObjectID(_id)},
          { $set: { "dealIssueUnderwriters.$" : req.body } }
        )
      }else {
        await Deals.update(
          { _id: dealId},
          {$addToSet: {dealIssueUnderwriters: req.body} }
        )
      }
      const dealsUnderWriter = await Deals.findById({ _id: dealId}).select("dealIssueUnderwriters")
      await manageTasksForProcessChecklists(dealId, req.user)
      return res.json(dealsUnderWriter)
    }
    if(type === "participants") {
      const isExists =  await Deals.findOne(
        { _id: ObjectID(dealId)},
        { dealIssueParticipants:
            { $elemMatch: {
              dealPartContactId: ObjectID(req.body.dealPartContactId),
              dealPartFirmId: ObjectID(req.body.dealPartFirmId),
              dealPartType: req.body.dealPartType,
            }
            }
        }
      )
      if(isExists && isExists.dealIssueParticipants && isExists.dealIssueParticipants.length) {
        if(isExists.dealIssueParticipants[0]._id.toString() !== (_id &&_id.toString())) {
          return res.status(422).json({error: "This user was already exists"})
        }
      }
      if(_id) {
        await Deals.updateOne(
          { _id: dealId, "dealIssueParticipants._id": ObjectID(_id)},
          { $set: { "dealIssueParticipants.$" : req.body } }
        )
      }else {
        await Deals.update(
          { _id: dealId},
          {$addToSet: {dealIssueParticipants: req.body} }
        )
      }
      const dealsPart = await Deals.findById({ _id: dealId}).select("dealIssueParticipants")
      await manageTasksForProcessChecklists(dealId, req.user)
      return res.json(dealsPart)
    }

    res.status(404).json("not found")
  } catch(error) {
    console.log("============", error)
    res.status(422).send({done:false,error:"There is a failure in retrieving information for entitlements",success:"",requestedServices:resRequested})
  }
}

const saveDealsRatingsByDealId = () => async (req, res) => {
  const { type, dealId } = req.params
  const { _id, seriesId } = req.body
  const resRequested = [
    {resource:"Deals",access:2},
  ]
  try {
    const entitled = await canUserPerformAction(req.user,resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, dealId)

    if( !entitled || (isEligible && !isEligible.canEditTran)) {
      return res.status(422).send({done:false,error:"User is not entitled to access the requested services",success:"",requestedServices:resRequested})
    }

    let srIndex, ratingIndex
    const rating = await Deals.findOne(
      {
        _id: dealId,
        /* "dealIssueSeriesDetails.seriesName": seriesName,
           "dealIssueSeriesDetails.seriesRatings._id": _id */
      }
    ).select("dealIssueSeriesDetails")
    if(_id) {
      if(type === "dealAgencyRatings") {
        srIndex = rating.dealIssueSeriesDetails.findIndex(x =>  x._id.toString() === seriesId)
        ratingIndex = rating.dealIssueSeriesDetails[srIndex].seriesRatings.findIndex(x => x._id.toString() === _id)
        rating.dealIssueSeriesDetails[srIndex].seriesRatings[ratingIndex] = Object.assign({}, rating.dealIssueSeriesDetails[srIndex].seriesRatings[ratingIndex], req.body)
      }else {
        srIndex = rating.dealIssueSeriesDetails.findIndex(x =>  x._id.toString() === seriesId)
        ratingIndex = rating.dealIssueSeriesDetails[srIndex].cepRatings.findIndex(x => x._id.toString() === _id)
        rating.dealIssueSeriesDetails[srIndex].cepRatings[ratingIndex] = Object.assign({}, rating.dealIssueSeriesDetails[srIndex].cepRatings[ratingIndex], req.body)
      }
      await rating.save()
      await manageTasksForProcessChecklists(dealId, req.user)
      const saveRating = await Deals.findById({_id: dealId}).select("dealIssueSeriesDetails")
      res.json(saveRating.dealIssueSeriesDetails)
    }else {
      if(type === "dealAgencyRatings") {
        srIndex = rating.dealIssueSeriesDetails.findIndex(x =>  x._id.toString() === seriesId)
        rating.dealIssueSeriesDetails[srIndex].seriesRatings.push(req.body)
      }else {
        srIndex = rating.dealIssueSeriesDetails.findIndex(x =>  x._id.toString() === seriesId)
        rating.dealIssueSeriesDetails[srIndex].cepRatings.push(req.body)
      }
      await rating.save()
      await manageTasksForProcessChecklists(dealId, req.user)
      const saveRating = await Deals.findById({_id: dealId}).select("dealIssueSeriesDetails")
      res.json(saveRating.dealIssueSeriesDetails)
    }
  }
  catch(error) {
    console.log("================", error.message)
    res.status(422).send({done:false,error:"There is a failure in retrieving information for entitlements",success:"",requestedServices:resRequested})
  }
}


const delDealsRatingByRatingId = () => async (req, res) => {
  const { type, dealId } = req.params
  const { seriesId, ratingId } = req.query
  const resRequested = [
    {resource:"Deals",access:2},
  ]
  try {
    const entitled = await canUserPerformAction(req.user,resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, dealId)

    if( !entitled || (isEligible && !isEligible.canEditTran)) {
      return res.status(422).send({done:false,error:"User is not entitled to access the requested services",success:"",requestedServices:resRequested})
    }
    if(type === "dealAgencyRatings") {
      await Deals.update(
        {_id: ObjectID(dealId), "dealIssueSeriesDetails._id" : ObjectID(seriesId)},
        {$pull: {"dealIssueSeriesDetails.$.seriesRatings": {_id: ObjectID(ratingId)}}
        })
      await manageTasksForProcessChecklists(dealId, req.user)
    }else {
      await Deals.update(
        {_id: ObjectID(dealId), "dealIssueSeriesDetails._id" : ObjectID(seriesId)},
        {$pull: {"dealIssueSeriesDetails.$.cepRatings": {_id: ObjectID(ratingId)}}
        })
      await manageTasksForProcessChecklists(dealId, req.user)
    }
    const rating = await Deals.findById({_id: dealId}).select("dealIssueSeriesDetails")
    res.json(rating.dealIssueSeriesDetails)
  } catch(error) {
    res.status(422).send({done:false,error:"There is a failure in retrieving information for entitlements",success:"",requestedServices:resRequested})
  }
}

const delDealsCheckInTrack  = () => async (req, res) => {
  const { dealId } = req.params
  const { type, trackId, category } = req.query
  const resRequested = [
    {resource:"Deals",access:1},
  ]
  try {
    const entitled = await canUserPerformAction(req.user,resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, dealId)

    if( !entitled || (isEligible && !isEligible.canEditTran)) {
      return res.status(422).send({done:false,error:"User is not entitled to access the requested services",success:"",requestedServices:resRequested})
    }
    let deals = {}
    if(category === "scheduleOfEvents") {
      deals = await Deals.update(
        {_id: dealId, "dealIssueScheduleOfEvents.soeCategoryName" : type},
        {$pull: {"dealIssueScheduleOfEvents.$.soeItems": { _id: ObjectID(trackId)}}},
      )
    }else {
      deals = await Deals.update(
        {_id: dealId, "dealIssueCostOfIssuance.costOfIssCategory" : type},
        {$pull: {"dealIssueCostOfIssuance.$.costItemDetails": { _id: ObjectID(trackId)}}},
      )
    }
    await manageTasksForProcessChecklists(dealId, req.user)
    res.status(200).json(deals)
  } catch(error) {
    res.status(422).send({done:false,error:"There is a failure in retrieving information for entitlements",success:"",requestedServices:resRequested})
  }
}


const putRelatedTran = () => async (req, res) => {
  const resRequested = [
    {resource:"Deals",access:2},
  ]
  const { dealId } = req.params
  try {
    const entitled = await canUserPerformAction(req.user,resRequested)
    const isEligible = await checkTranEligiblityForLoggedInUser(req.user, dealId)
    const select = "dealIssueTranClientId dealIssueTranAssignedTo dealIssueTranClientId dealIssueTranIssuerId dealIssueTranType dealIssueTranName dealIssueTranIssuerFirmName rfpTranIsConduitBorrowerFlag " +
      "dealIssueTranIssueName dealIssueTranProjectDescription dealIssueTranSubType dealIssueParticipants dealIssueTranStatus dealIssueBorrowerName dealIssueSecurityType dealIssueTranRelatedTo"

    if( !entitled || (isEligible && !isEligible.canEditTran)) {
      return res.status(422).send({done:false,error:"User is not entitled to access the requested services",success:"",requestedServices:resRequested})
    }
    await Deals.update({ _id: dealId}, {$addToSet: {dealIssueTranRelatedTo: req.body} })
    const rfp = await Deals.findById({_id: dealId}).select(select)
    await manageTasksForProcessChecklists(dealId, req.user)
    res.json(rfp)
  } catch(error) {
    res.status(422).send({done:false,error:"There is a failure in retrieving information for entitlements",success:"",requestedServices:resRequested})
  }
}

export const putTransactionDocStatus = () => async (req, res) => {
  const { tranId } = req.params
  const { tranType } = req.query
  const {_id, docStatus} = req.body
  try {

    const tranTypes = ["Deals","RFP","ActMaRFP","Derivatives","BankLoans","Others"]


    if(tranTypes.indexOf(tranType) !== -1 && tranId && _id) {

      const key = tranType === "Deals" ? "dealIssueDocuments" : tranType === "ActMaRFP" ? "maRfpDocuments" : tranType === "Derivatives" ? "derivativeDocuments"
        : tranType === "BankLoans" ? "bankLoanDocuments" :  tranType === "RFP" ? "rfpBidDocuments" : tranType === "Others" ? "actTranDocuments" : ""

      const query1 = `${key}._id`

      const query2 = `${key}.$.docStatus`

      const modal = tranType === "Deals" ? Deals : tranType === "ActMaRFP" ? ActMaRFP : tranType === "Derivatives" ? Derivatives
        : tranType === "BankLoans" ? BankLoans :  tranType === "RFP" ? RFP : tranType === "Others" ? Others : ""

      const document = await modal.updateOne(
        { _id: ObjectID(tranId), [query1]: ObjectID(_id)},
        { $set: { [query2] : docStatus } }
      )

      res.status(200).json({done: true, document})
    }else {

      res.status(422).send({done:false,error:"This type type of transaction doesn't exists."})

    }
  } catch(error) {
    console.log("===========================", error.message)
    res.status(422).send({done:false, error: error.message})

  }
}

export const pullTransactionDoc = () => async (req, res) => {
  const { tranId } = req.params
  const { tranType, docId } = req.query
  try {

    const tranTypes = ["Deals","RFP","ActMaRFP","Derivatives","BankLoans","Others"]


    if(tranTypes.indexOf(tranType) !== -1 && tranId && docId) {

      const key = tranType === "Deals" ? "dealIssueDocuments" : tranType === "ActMaRFP" ? "maRfpDocuments" : tranType === "Derivatives" ? "derivativeDocuments"
        : tranType === "BankLoans" ? "bankLoanDocuments" :  tranType === "RFP" ? "rfpBidDocuments" : tranType === "Others" ? "actTranDocuments" : ""

      const modal = tranType === "Deals" ? Deals : tranType === "ActMaRFP" ? ActMaRFP : tranType === "Derivatives" ? Derivatives
        : tranType === "BankLoans" ? BankLoans :  tranType === "RFP" ? RFP : tranType === "Others" ? Others : ""

      await modal.update({ _id: tranId}, { $pull: { [key]: { _id: docId } }})

      const document = await modal.findById(tranId).select(key)

      res.status(200).json(document)
    }else {

      res.status(422).send({done:false,error:"This Document doesn't exists."})

    }
  } catch(error) {
    console.log("===========================", error.message)
    res.status(422).send({done:false, error: error.message})

  }
}

export default generateControllers(Deals, {
  getDealTransactionDetails: getDealTransactionDetails(),
  updateDealTransaction: updateDealTransaction(),
  removeParticipantByPartId: removeParticipantByPartId(),
  getDealRatings: getDealRatings(),
  saveDealsRatingsByDealId: saveDealsRatingsByDealId(),
  removeDealRatingByRatingId: removeDealRatingByRatingId(),
  getDealSeriesBySeriesId: getDealSeriesBySeriesId(),
  saveDealsPricingBySeriesId: saveDealsPricingBySeriesId(),
  delSeriesBySeriesId: delSeriesBySeriesId(),
  saveDealSeriesBySeriesId: saveDealSeriesBySeriesId(),
  saveDealParticipantsbyPuId: saveDealParticipantsbyPuId(),
  delDealsRatingByRatingId: delDealsRatingByRatingId(),
  delDealsCheckInTrack: delDealsCheckInTrack(),
  putRelatedTran: putRelatedTran(),
  putTransactionDocStatus: putTransactionDocStatus(),
  pullTransactionDoc: pullTransactionDoc(),
  postDealTransaction:postDealTransaction()
})
