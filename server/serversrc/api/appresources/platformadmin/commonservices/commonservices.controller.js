import axios from "axios"
import AWS from "aws-sdk"
import fileSize from "file-size"
import {
  Entity,
  EntityRel,
} from "./../../models"
import { Config } from "../../config/config.model"
import {getHeaders} from "../../../elasticsearch/esHelper"

const { ObjectID } = require("mongodb")


export const getPlatFormAdminPickLists = async (req, res, next) => {
  const { user } = req
  let { entityId, userEntitlement } = user || {}

  const { require, searchTerm} = req.query
  console.log("The query Parameter is ", searchTerm)
  const regexSearchTerm = new RegExp(searchTerm,"i")

  const entities = await EntityRel.aggregate([
    {
      $match:{
        relationshipType: "Self"
      }
    },
    {
      $lookup: {
        from: "entities",
        localField: "entityParty1",
        foreignField: "_id",
        as: "entity"
      }
    },
    {
      $unwind: "$entity"
    },
    {
      $project: {
        entityId: "$entity._id",
        firmName: "$entity.firmName",
      }
    },
    {
      $unwind: "$entityId"
    }
  ])
  const entityID = entities && entities.length ? entities && entities[0] && entities[0].entityId : ""
  entityId = entityID

  let searchQueryPart = []
  if(searchTerm && searchTerm.length > 0) {
    searchQueryPart = [{
      $match:{ "$or": [
        { "picklistTitle": {"$regex":regexSearchTerm} },
        { "picklistItems.label": {"$regex":regexSearchTerm }}
      ]}
    }]
  }

  console.log("The search Query Party", JSON.stringify(searchQueryPart,null,2))

  const pickListPipeline = [
    {
      $match: {entityId:ObjectID(entityId)}
    },
    {
      $unwind:"$picklists"
    },
    {
      $project:{
        entityId:"$entityId",
        picklistId:"$picklists._id",
        picklistTitle:"$picklists.title",
        picklistItems:"$picklists.items",
        picklistMeta:"$picklists.meta"
      }
    },
    {
      $unwind:"$picklistItems"
    },
    ...searchQueryPart,
    {
      $group:{
        _id:{entityId:"$entityId",picklistId:"$picklistId",title:"$picklistTitle",meta:"$picklistMeta"},
        items:{$addToSet:"$picklistItems"}
      }
    },
    {
      $project:{
        _id:0,
        entityId:"$_id.entityId",
        picklistId:"$_id.picklistId",
        title:"$_id.title",
        meta:"$_id.meta",
        // items:1
      }
    }
  ]

  console.log("The overall pipeline is", JSON.stringify(pickListPipeline,null,2))

  // const filter = req.query || {}
  const filter = { entityId }
  let docs
  try {
    switch(require) {
    case "entitlement":
      docs = await Config.find(filter).select("accessPolicy")
      res.json(docs[0].accessPolicy.filter(d => d.role === userEntitlement))
      break
    case "picklistsMeta":
      docs = await Config.findOne(filter).select({"picklists.meta": 1, "picklists.title": 1})
      res.json(docs.picklists)
      break
    case "picklists": {
      const start = new Date()
      const { names } = req.query
      let docs = []
      console.log("names : ", names)
      if(names && names.length) {
        const systemNames = names.split(",")
        console.log("systemNames : ", systemNames)
        docs = await Config.aggregate([
          {
            $match: filter
          },
          {
            $limit: 1
          },
          {
            $project: { picklists: 1 }
          },
          {
            $project: {
              picklists: {
                $filter: {
                  input: "$picklists",
                  as: "picklist",
                  cond: { $in: [ "$$picklist.meta.systemName", systemNames ] }
                }
              }
            }
          }
        ])
      } else {
        // docs = await Config.find(filter).select("picklists.meta picklists.title")
        docs = await Config.aggregate(pickListPipeline)
        docs = [{picklists:docs}]
        console.log("The picklist items to be showcased are", JSON.stringify(docs, null, 2))
      }
      console.log("time : ", new Date() - start)
      res.json((docs.length && docs[0] && docs[0].picklists) || [] )
      break
    }
    case "checklists":
      docs = await Config.find(filter).select("checklists")
      res.json(docs[0].checklists)
      break
    case "notifications":
      docs = await Config.find(filter).select("notifications")
      res.json(docs[0].notifications)
      break
    default:
      docs = await Config.find(filter)
      res.json(docs)
    }
  } catch (error) {
    next(error)
  }

}

export const s3 = new AWS.S3({
  region : process.env.SESREGION,
  bucket: process.env.S3BUCKET,
  accessKeyId: process.env.S3ACCESSKEYID,
  secretAccessKey: process.env.S3SECRETKEY
})

let allKeys = []

export const listAllKeys = async (params, token, callback) => {

  if(token) params.ContinuationToken = token

  const data = await s3.listObjectsV2(params).promise()
  allKeys = allKeys.concat(data.Contents)

  if(data.IsTruncated) {
    await listAllKeys(params, data.NextContinuationToken, callback)
  }

  return allKeys
}

export const getS3FolderSize = async (tenantId) => {

  const params = {
    Bucket: process.env.S3BUCKET,
    Prefix: tenantId
  }
  let totalSize = 0

  return new Promise(async (res) => {
    const documents = await listAllKeys(params)
    if(documents && documents.length){
      documents.forEach(d => {
        totalSize += d.Size
      })
    }
    allKeys = []
    res(fileSize(totalSize).human("jedec"))
  })
}

export const getDbStorageSize = (obj) => {
  let bytes = 0

  function sizeOf(obj) {
    if(obj !== null && obj !== undefined) {
      switch(typeof obj) {
      case "number":
        bytes += 8
        break
      case "string":
        bytes += obj.length * 2
        break
      case "boolean":
        bytes += 4
        break
      case "object":
        const objClass = Object.prototype.toString.call(obj).slice(8, -1)
        if(objClass === "Object" || objClass === "Array") {
          for(const key in obj) {
            if(!obj.hasOwnProperty(key)) continue
            sizeOf(obj[key])
          }
        } else bytes += obj.toString().length * 2
        break
      }
    }
    return bytes
  };

  function formatByteSize(bytes) {
    if(bytes < 1024) return `${bytes  } Bytes`
    else if(bytes < 1048576) return`${(bytes / 1024).toFixed(3)  } KB`
    else if(bytes < 1073741824) return`${(bytes / 1048576).toFixed(3)  } MB`
    return`${(bytes / 1073741824).toFixed(3)  } GB`
  };

  return formatByteSize(sizeOf(obj))

}

export const getElasticStorageDetails = async (type, tenantId) => {

  try {
    const indexExists = await axios.get(`${process.env.ES_HOST}tenant_${type}_${tenantId}/_stats/indexing,store,docs,search?pretty`,{ headers: getHeaders() })
    const elasticData = await indexExists.data

    const { docs, indexing, search, store } = elasticData && elasticData.indices && elasticData.indices[`tenant_${type}_${tenantId}`] && elasticData.indices[`tenant_${type}_${tenantId}`].primaries || {}

    return {
      docsCount: docs && docs.count || 0,
      docsDeleted: docs && docs.deleted || 0,
      indexingIndexTotal: indexing && indexing.index_total || 0,
      searchQueryTotal: search && search.query_total || 0,
      searchFetchTotal: search && search.fetch_total || 0,
      size: store && store.size_in_bytes && fileSize(store.size_in_bytes).human("jedec") || fileSize(0).human("jedec"),
    }
  } catch (e) {
    return {
      docsCount: 0,
      docsDeleted: 0,
      indexingIndexTotal: 0,
      searchQueryTotal: 0,
      searchFetchTotal: 0,
      size: fileSize(0).human("jedec"),
    }
  }
}

export const getPlatFormAdminUserAndTransaction = async (req, res) => {
  const { id } = req.params
  try {

    const date = new Date()

    const snapData = await EntityRel.aggregate([
      {
        $match: {
          relationshipType: "Self"
        }
      },
      {
        $lookup: {
          from: "entityusers",
          localField: "entityParty1",
          foreignField: "entityId",
          as: "user"
        }
      },
      {
        $lookup: {
          from: "entityusers",
          localField: "entityParty1",
          foreignField: "entityId",
          as: "userArray"
        }
      },
      {
        $lookup: {
          from: "tranagencydeals",
          localField: "entityParty1",
          foreignField: "dealIssueTranClientId",
          as: "deal"
        }
      },
      {
        $lookup: {
          from: "tranagencydeals",
          localField: "entityParty1",
          foreignField: "dealIssueTranClientId",
          as: "dealArray"
        }
      },
      {
        $lookup: {
          from: "tranbankloans",
          localField: "entityParty1",
          foreignField: "actTranFirmId",
          as: "loan"
        }
      },
      {
        $lookup: {
          from: "tranbankloans",
          localField: "entityParty1",
          foreignField: "actTranFirmId",
          as: "loanArray"
        }
      },
      {
        $lookup: {
          from: "tranderivatives",
          localField: "entityParty1",
          foreignField: "actTranFirmId",
          as: "derivative"
        }
      },
      {
        $lookup: {
          from: "tranderivatives",
          localField: "entityParty1",
          foreignField: "actTranFirmId",
          as: "derivativeArray"
        }
      },
      {
        $lookup: {
          from: "tranagencyothers",
          localField: "entityParty1",
          foreignField: "actTranFirmId",
          as: "others"
        }
      },
      {
        $lookup: {
          from: "tranagencyothers",
          localField: "entityParty1",
          foreignField: "actTranFirmId",
          as: "othersArray"
        }
      },
      {
        $lookup: {
          from: "tranagencyrfps",
          localField: "entityParty1",
          foreignField: "rfpTranClientId",
          as: "rfp"
        }
      },
      {
        $lookup: {
          from: "tranagencyrfps",
          localField: "entityParty1",
          foreignField: "rfpTranClientId",
          as: "rfpArray"
        }
      },
      {
        $lookup: {
          from: "actmarfps",
          localField: "entityParty1",
          foreignField: "actTranFirmId",
          as: "marfp"
        }
      },
      {
        $lookup: {
          from: "actmarfps",
          localField: "entityParty1",
          foreignField: "actTranFirmId",
          as: "marfpArray"
        }
      },
      {
        $lookup: {
          from: "actbusdevs",
          localField: "entityParty1",
          foreignField: "actTranFirmId",
          as: "bussdev"
        }
      },
      {
        $lookup: {
          from: "actbusdevs",
          localField: "entityParty1",
          foreignField: "actTranFirmId",
          as: "bussdevArray"
        }
      },
      {
        $lookup: {
          from: "entities",
          localField: "entityParty1",
          foreignField: "_id",
          as: "entity"
        }
      },
      {
        $lookup: {
          from: "entities",
          localField: "entityParty1",
          foreignField: "_id",
          as: "entityArray"
        }
      },
      {
        $unwind: "$entity"
      },
      {
        $project : {
          tenantId: "$entity._id",
          firmName: "$entity.firmName",
          userFlags: "$user.userFlags",
          Deal: {
            $size: "$deal"
          },
          BankLoan: {
            $size: "$loan"
          },
          Derivative: {
            $size: "$derivative"
          },
          Others: {
            $size: "$others"
          },
          RFP: {
            $size: "$rfp"
          },
          MaRfp: {
            $size: "$marfp"
          },
          BusinessDevelopment: {
            $size: "$bussdev"
          },
          array: ["$userArray", "$dealArray", "$loanArray", "$derivativeArray", "$othersArray", "$rfpArray", "$marfpArray", "$bussdevArray", "$entityArray"]
        }
      }
    ])

    const thirdPartyLength = await EntityRel.aggregate([
      {
        $match: {
          relationshipType: "Third Party"
        }
      },
      {
        $group: {
          _id: "$entityParty1",
          count:{$sum:1}
        }
      },
    ])

    const clientLength = await EntityRel.aggregate([
      {
        $match: {
          relationshipType: "Client"
        }
      },
      {
        $group: {
          _id: "$entityParty1",
          count:{
            $sum:1
          }
        }
      },
    ])

    const prospectLength = await EntityRel.aggregate([
      {
        $match: {
          relationshipType: "Prospect"
        }
      },
      {
        $group: {
          _id: "$entityParty1",
          count: {
            $sum:1
          }
        }
      },
    ])

    const snapShots = []

    for (const snap of snapData) {

      const { Deal, BankLoan, Derivative, Others, RFP, MaRfp, BusinessDevelopment, userFlags, firmName, tenantId, array } = snap

      let series50 = 0
      let nonSeries50 = 0

      if(userFlags && userFlags.length){
        userFlags.forEach(u => {
          if(u.includes("Series 50")){
            series50 += 1
          } else {
            nonSeries50 += 1
          }
        })
      }

      const client = clientLength.find(c => c._id.toString() === tenantId.toString())
      const prospect = prospectLength.find(p => p._id.toString() === tenantId.toString())
      const thirdParty = thirdPartyLength.find(t => t._id.toString() === tenantId.toString())

      const clientArray = await EntityRel.find({entityParty1: ObjectID(tenantId), relationshipType: "Client"})
      const prospectArray = await EntityRel.find({entityParty1: ObjectID(tenantId), relationshipType: "Prospect"})
      const thirdPartyArray = await EntityRel.find({entityParty1: ObjectID(tenantId), relationshipType: "Third Party"})

      if(clientArray && clientArray.length){
        array.push(clientArray)
      }
      if(prospectArray && prospectArray.length){
        array.push(prospectArray)
      }
      if(thirdPartyArray && thirdPartyArray.length){
        array.push(thirdPartyArray)
      }

      const storageSize = await getS3FolderSize(tenantId.toString())

      const dbStorage = await getDbStorageSize(JSON.stringify(array))

      const elasticDocs = await getElasticStorageDetails("docs", tenantId)
      const elasticData = await getElasticStorageDetails("data", tenantId)

      snapShots.push({
        appId: id,
        firmName,
        tenantId,
        users: {
          series50,
          nonSeries50
        },
        transactions: Deal + BankLoan + Derivative + Others + RFP + MaRfp + BusinessDevelopment,
        client: client && client.count || 0,
        prospect: prospect && prospect.count || 0,
        thirdParty: thirdParty && thirdParty.count || 0,
        storageSize,
        dbStorage,
        elasticStorage: {
          docs: elasticDocs || {},
          data: elasticData || {}
        },
        transactionWise: {
          Deal,
          BankLoan,
          Derivative,
          Others,
          RFP,
          MaRfp,
          BusinessDevelopment
        },
        date
      })

    }

    res.json({snapShots})

  } catch (error) {
    console.log("Error:", error)
  }

}

export const fetchBillingInvoices = async ( req, res) => {
  const { entityId } = req.params
  try {
    const invoiceList = await axios.post(`${process.env.PLATFORM_API_URL}/service/invoice`, {entityId} ,{
      headers: {Authorization : process.env.PLATFORM_CALL_TO_PLATFORM_MANAGEMENT_TOKEN }
    })
    res.status(200).send(invoiceList.data)
  } catch(e) {
    console.log(e)
    res.status(500).send({done: false,  error:e})
  }

}

export const postMakePayment = async ( req, res) => {

  try {

    const payment = await axios.post(`${process.env.PLATFORM_API_URL}/service/payment`, req.body,{
      headers: {Authorization : process.env.PLATFORM_CALL_TO_PLATFORM_MANAGEMENT_TOKEN }
    })

    res.send(payment && payment.data)

  } catch(err) {
    res.send({done: false,  error: err, message: err.message || ""})
  }

}

export const updateFirmInfo = async(req, res) => {
  const {entityId} = req.params
  const {settings, auditLogs} = req.body
  try {
    if (!entityId) {
      res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: ""})
    }

    const entity = await Entity.update({ _id: entityId }, {
        $set: { "settings.auditFlag": settings.auditFlag},
        $push : { auditLogs : auditLogs }
    })

    if(entity.ok){
      res.status(200).send({done: true})
    }
  } catch (e) {
    res
      .status(422)
      .send({done: false, error: e, success: "", requestedServices: "asas"})
  }
}
export const fetchPlatFormDocs = async ( req, res) => {

  const { docId } = req.params
  try {
    const doc = await axios.get(`${process.env.PLATFORM_API_URL}/service/platformdocs/${docId}`, {
      headers: {Authorization : process.env.PLATFORM_CALL_TO_PLATFORM_MANAGEMENT_TOKEN }
    })

    res.status(200).send(doc && doc.data && doc.data.doc)
  } catch(e) {
    console.log(e)
    res.status(500).send({done:true, userDetails:[], error:e})
  }

}

export const fetchTenantDetails = async ( req, res) => {
  const { id } = req.params
  try {
    const response = await axios.get(`${process.env.PLATFORM_API_URL}/service/tenantconfig/${id}`, {
      headers: {Authorization : process.env.PLATFORM_CALL_TO_PLATFORM_MANAGEMENT_TOKEN }
    })
    res.status(200).send(response && response.data)
  } catch(e) {
    console.log(e)
    res.status(500).send({done:true, userDetails:[], error:e})
  }

}

export const getAllDisablePicklists = async ( req, res ) => {

  try {
    const { entityId, searchTerm, names } = req.query
    const filter = { entityId: ObjectID(entityId) }
    let docs = []
    const regexSearchTerm = new RegExp(searchTerm,"i")
    let searchQueryPart = []

    if(searchTerm && searchTerm.length > 0) {
      searchQueryPart = [{
        $match:{ "$or": [
          { "picklistTitle": {"$regex":regexSearchTerm} },
          { "picklistItems.label": {"$regex":regexSearchTerm }},
          { "picklistMeta.systemName": {"$regex":regexSearchTerm }}
        ]}
      }]
      console.log("The search Query Party", JSON.stringify(searchQueryPart,null,2))
    }

    if(names && names.length) {
      const systemNames = names.split(",")
      console.log("systemNames : ", systemNames)
      docs = await Config.aggregate([
        {
          $match: filter
        },
        {
          $limit: 1
        },
        {
          $project: { picklists: 1 }
        },
        {
          $project: {
            picklists: {
              $filter: {
                input: "$picklists",
                as: "picklist",
                cond: { $in: [ "$$picklist.meta.systemName", systemNames ] }
              }
            }
          }
        }
      ])
      console.log("Picklists ", docs)
    } else {
      const pickListPipeline = [
        {
          $match: {entityId:ObjectID(entityId)}
        },
        {
          $unwind:"$picklists"
        },
        {
          $project:{
            entityId:"$entityId",
            picklistId:"$picklists._id",
            picklistTitle:"$picklists.title",
            picklistItems:"$picklists.items",
            picklistMeta:"$picklists.meta"
          }
        },
        {
          $unwind:"$picklistItems"
        },
        ...searchQueryPart,
        {
          $group:{
            _id:{entityId:"$entityId",picklistId:"$picklistId",title:"$picklistTitle",meta:"$picklistMeta"},
            items:{$addToSet:"$picklistItems"}
          }
        },
        {
          $project:{
            _id:0,
            entityId:"$_id.entityId",
            picklistId:"$_id.picklistId",
            title:"$_id.title",
            meta:"$_id.meta",
          }
        }
      ]

      console.log("The overall pipeline is", JSON.stringify(pickListPipeline,null,2))
      docs = await Config.aggregate(pickListPipeline)
      docs = [{ picklists:docs }]
      console.log("The picklist items to be showcased are", JSON.stringify(docs, null, 2))
    }
    res.json((docs.length && docs[0] && docs[0].picklists) || [] )
  } catch(e) {
    console.log(e)
    res.status(500).send({done:true, userDetails:[], error:e})
  }
}

export const updateSpecificPicklist = async (req, res, next) => {

  try {
    const { tenantId, pickList  } = req.body
    let pickLists = []
    const id = pickList._id
    const items = pickList && pickList.items
    const title = pickList && pickList.title
    const meta = pickList && pickList.meta
    if(tenantId) {
      if(id){
        console.log({ id, tenantId } )
        await Config.updateOne(
          { entityId: ObjectID(tenantId), "picklists._id": id },
          { $set: { "picklists.$.items": items, "picklists.$.meta": meta, "picklists.$.title": title } }
        )
      }else {
        pickLists = await Config.update(
          { entityId: ObjectID(tenantId) },
          { $addToSet: { picklists: [pickList] } }
        )
      }
    }
    const names = meta.systemName
    if(names && names.length) {
      const systemNames = names.split(",")
      console.log("systemNames : ", systemNames)
      pickLists = await Config.aggregate([
        {
          $match: { entityId: ObjectID(tenantId) }
        },
        {
          $limit: 1
        },
        {
          $project: { picklists: 1 }
        },
        {
          $project: {
            picklists: {
              $filter: {
                input: "$picklists",
                as: "picklist",
                cond: { $in: [ "$$picklist.meta.systemName", systemNames ] }
              }
            }
          }
        }
      ])
      pickLists = pickLists.length ? pickLists[0].picklists[0] : {}
    }
    res.json({done:true, pickLists})
  } catch (error) {
    console.log(error)
    res.status(500).send({done:true, pickLists:[], error})
  }
}

export const deleteSpecificPicklist = async (req, res) => {
  const { tenantId, picklistId } = req.query
  try {
    if(tenantId) {
      await Config.update({
        entityId: ObjectID(tenantId)
      }, {
        $pull: { picklists: { _id: picklistId } }
      })
    }
    const resData = await Config.find({ entityId: ObjectID(tenantId) }).select("picklists")
    console.log("resData",resData)
    res.json({done:true, pickLists: resData.picklists})
  } catch (error) {
    console.log(error)
    res.status(500).send({done:true, pickLists:[], error})
  }
}

export const getAllChecklists = async ( req, res) => {
  try {
    const { tenantId } = req.query
    const filter = { entityId: tenantId }
    const details = await Config.find(filter).select("checklists")
    res.json(details[0].checklists)
  } catch(err) {
    res.send({done: false,  error: err, message: "Error in getting checklists"})
  }
}

export const updateChecklists = async(req, res) => {
  try {
    const {checklists, tenantId} = req.body

    if (!checklists || !tenantId) {
      return res
        .status(422)
        .send({error: "No checklists data provided"})
    }

    const config = await Config
      .findOne({entityId: tenantId})
      .select({_id: 1})

    if (!config) {
      return res
        .status(422)
        .send({error: "No config for the user found"})
    }
    const {_id} = config
    const updateRes = await Config.update({
      _id
    }, {$set: {
      checklists
    }})

    res.json({updateRes})
  } catch(err) {
    res.send({done: false,  error: err, message: "Error in updating checklists"})
  }
}

export const savePicklistSpecific = async (req, res) =>{
  try {
    const {firmName, pickListItems} = req.body
    const names = (pickListItems && pickListItems.meta && pickListItems.meta.systemName) || ""
    const entityIds = (firmName || []).map(p => ObjectID(p.value)) || []
    const items = (pickListItems && pickListItems.items) || []
    const title = (pickListItems && pickListItems.title) || ""
    const meta = (pickListItems && pickListItems.meta) || {}
    let docs = []
    if(names) {
      const systemNames = names.split(",")
      docs = await Config.aggregate([
        {
          $match: {
            "entityId" :
              {
                $in : entityIds
              }
          }
        },
        {
          $project: { picklists: 1 }
        },
        {
          $project: {
            picklists: {
              $filter: {
                input: "$picklists",
                as: "picklist",
                cond: { $in: [ "$$picklist.meta.systemName", systemNames ] }
              }
            }
          }
        }
      ])
    }

    if(docs.length){
      const addNewIds = []
      const updateIds = []
      const updateIdsPicklist = []
      const firstLevelUILabels = items.map(name => name.label)

      docs.forEach(itemDetails => {
        if (itemDetails.picklists && itemDetails.picklists.length) {
          itemDetails.picklists.forEach(o => {
            if (o.items && o.items.length) {
              firstLevelUILabels.forEach((t, index) => {
                const itemIndex = (o.items || []).map(k => k.label).indexOf(t)
                if (itemIndex === -1) {
                  o.items.push(items[index])
                } else {
                  const secondLevelUILabels = (items[index].items || []).map(j => j.label)
                  const secondLevelLabels = (o.items[itemIndex].items || []).map(j => j.label)
                  secondLevelUILabels.forEach((sl, secondInd) => {
                    const secondIndex = secondLevelLabels.indexOf(sl)
                    if (secondIndex === -1) {
                      o.items[itemIndex].items.push(items[index].items[secondInd])
                    } else {
                      const thirdLevelUILabels = (items[index].items[secondInd].items || []).map(k => k.label)
                      const thirdLevelLabels = (o.items[itemIndex].items[secondIndex].items || []).map(k => k.label)
                      thirdLevelUILabels.forEach((tl, thirdInd) => {
                        const thirdIndex = thirdLevelLabels.indexOf(tl)
                        if (thirdIndex === -1) {
                          o.items[itemIndex].items[secondIndex].items.push(items[index].items[secondInd].items[thirdInd])
                        }
                      })
                    }
                  })
                }
              })
            }
          })
          const id = ObjectID(itemDetails._id)
          const pickListId = itemDetails.picklists && itemDetails.picklists[0] && itemDetails.picklists[0]._id
          if(pickListId && id){
            updateIds.push(id)
            updateIdsPicklist.push(pickListId)
          }
        } else {
          const id = ObjectID(itemDetails._id)
          addNewIds.push(id)
        }
      })

      if(addNewIds.length){
        for (const i in addNewIds) {
          await Config.update(
            { _id: { $in: addNewIds[i] } },
            { $addToSet: { picklists: pickListItems } }
          )
        }
      }

      if(updateIds.length && updateIdsPicklist.length){
        for (const i in updateIds) {
          const finalData = (docs || []).find(p => p._id === updateIds[i]) || {}
          const innerData = (finalData && finalData.picklists || []).find(l => l._id === updateIdsPicklist[i])
          await Config.updateOne(
            { _id : updateIds[i], "picklists._id": updateIdsPicklist[i] },
            { $set: { "picklists.$.items": (innerData && innerData.items), "picklists.$.meta": meta, "picklists.$.title": title } }
          )
        }
      }
      res.json({ done: true, message: "Changes Saved", status: "success" })
    }
  }catch (e) {
    console.log(e)
    res.send({done: false,  error: e, message: "Error in save picklist"})
  }
}
