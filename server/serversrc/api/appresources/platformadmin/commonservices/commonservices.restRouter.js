import express from "express"
import {
  getPlatFormAdminPickLists,
  getPlatFormAdminUserAndTransaction,
  fetchBillingInvoices,
  postMakePayment,
  fetchPlatFormDocs,
  getAllDisablePicklists,
  updateSpecificPicklist,
  deleteSpecificPicklist,
  fetchTenantDetails,
  getAllChecklists,
  updateChecklists,
  savePicklistSpecific,
  updateFirmInfo,
} from "./commonservices.controller"
import {platFormAuthMiddleware} from "../auth"
import { requireAuth } from "../../../../authorization/authsessionmanagement/auth.middleware"

export const commonservicesRouter = express.Router()

commonservicesRouter
  .route("/snapshots/:id")
  .get( getPlatFormAdminUserAndTransaction)

commonservicesRouter
  .route("/picklists")
  .get(platFormAuthMiddleware, getPlatFormAdminPickLists)

commonservicesRouter
  .route("/checklists")
  .get(platFormAuthMiddleware, getAllChecklists)
  .post(platFormAuthMiddleware, updateChecklists)

commonservicesRouter
  .route("/disablepicklists")
  .get(platFormAuthMiddleware, getAllDisablePicklists)
  .put(platFormAuthMiddleware, updateSpecificPicklist)
  .delete(platFormAuthMiddleware, deleteSpecificPicklist)

commonservicesRouter
  .route("/billing/:entityId")
  .get(requireAuth, fetchBillingInvoices)

commonservicesRouter
  .route("/payment")
  .post(requireAuth, postMakePayment)

commonservicesRouter
  .route("/platformdocs/:docId")
  .get(requireAuth, fetchPlatFormDocs)

commonservicesRouter
  .route("/tenantconfig/:id")
  .get(requireAuth, fetchTenantDetails)

commonservicesRouter
  .route("/configs/update-specific")
  .post(savePicklistSpecific)

commonservicesRouter
  .route("/entity/:entityId")
  .post(updateFirmInfo)
