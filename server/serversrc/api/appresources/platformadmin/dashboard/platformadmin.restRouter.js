import express from "express"
import {
  fetchDataForPlatFormAdminLists,
  fetchTenantUsersList,
  fetchFirmDetails,
  countTenantUsersForPlatform
} from "./platformadmin.controller"
import {platFormAuthMiddleware} from "../auth"

export const platFormAdminRouter = express.Router()

platFormAdminRouter
  .route("/tenants")
  .post(platFormAuthMiddleware, fetchDataForPlatFormAdminLists)

platFormAdminRouter
  .route("/tenantusers")
  .post(platFormAuthMiddleware, fetchTenantUsersList)

platFormAdminRouter
  .route("/entities/:entityId")
  .get(platFormAuthMiddleware, fetchFirmDetails)

platFormAdminRouter
  .route("/tenantusercount")
  .get(platFormAuthMiddleware, countTenantUsersForPlatform)
