import {onboardTenant} from "../../seeddata/revisedseedata/clientonboarding/onboardingDriver"
// import {firm} from "../../seeddata/revisedseedata/clientonboarding/firm"
// import {users} from "../../seeddata/revisedseedata/clientonboarding/users"


export const generateOnBoardingTenant = async (req, res) => {
  try {
    const { firms } = req.body
    let allStatusMessages = {}
    for (const i in firms) {
      const firm = firms[i]
      console.log(`*********${firm.firmName}************`, JSON.stringify(firm.users))
      const tenantStatus = await onboardTenant(firm, firm.users)
      allStatusMessages = {...allStatusMessages,[firm.firmName]:tenantStatus }
    }
    res.json({ done: true, message: "Successfully generate on boarding firm data", status:allStatusMessages })
  }
  catch(err) {
    console.log("Generate on boarding firm data error", err)
    res
      .status(500)
      .send({ done: false, error: "Error saving information on Firm" })
  }
}



