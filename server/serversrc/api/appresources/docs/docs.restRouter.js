import express from "express"
import docsController from "./docs.controller"
import { Docs } from "./docs.model"
import {requireAuth} from "../../../authorization/authsessionmanagement/auth.middleware"
import {returnFromES} from "../../elasticsearch/returnEsFrom"

export const docsRouter = express.Router()


docsRouter.post("/update-versions", requireAuth,async (req, res) => {
  const { _id, versions, name, originalName, option } = req.body
  const newVersionDetails = (versions || []).map( v => ({
    uploadedByUserId:req.user._id,
    uploadedByUserTenantId:req.user.tenantId,
    uploadedByUserEntityId:req.user.entityId,
    ...v,
  }))

  if(!_id || !option || !newVersionDetails || !newVersionDetails.length) {
    return res.status(422).send({ error: "invalid input" })
  }

  try {
    if(option === "add") {
      if(!name || !originalName) {
        return res.status(422).send({ error: "invalid input" })
      }
      const result = await Docs.update({ _id }, {
        $set: { name, originalName },
        $push: { "meta.versions": { $each: newVersionDetails } }
      })
      res.json(result)
    } else if(option === "remove") {
      console.log("Removed versions: ", versions)
      /* await Docs.update({ _id }, {
        $pull: { "meta.versions": {
          versionId: { $in: versions }
        } }
      }) */
      const docDetails = await Docs.findOne({ _id })
      for (const i in versions) {
        await Docs.update({ _id, "meta.versions.versionId": versions[i]},
          { $set: { "meta.versions.$.isInActive": true } }
        )
      }
      console.log(docDetails.contextType, req.user.tenantId, docDetails.contextId)
      await returnFromES(docDetails.contextType, req.user.tenantId, docDetails.contextId)
      const result = await Docs.findOne({ _id })
      res.json(result)
    } else {
      return res.status(422).send({ error: "Wrong update option provided" })
    }
  } catch (err) {
    return res.status(422).send({ error: err })
  }
})

docsRouter.param("id", docsController.findByParam)

docsRouter.route("/")
  .get(requireAuth,docsController.getAll)
  .put(requireAuth,docsController.updateFoldersDocs)
  .post(requireAuth,docsController.createOne)

docsRouter.route("/:id")
  .get(requireAuth,docsController.getOne)
  .put(requireAuth,docsController.updateOne)
  .delete(requireAuth,docsController.deleteOne)

docsRouter.route("/meta/:docId")
  .put(requireAuth, docsController.updateMeta)
