import mongoose from "mongoose"
import timestamps from "mongoose-timestamp"
import {EntityUser} from "../models"

const { Schema } = mongoose

const documentsSchema=Schema({
  docAWSFileLocation:String,
  docFileName:String,
  createdBy: {type: Schema.Types.ObjectId, ref: EntityUser},
  createdUserName: String,
  LastUpdatedDate: { type: Date, required: true, default: Date.now }
})

const pocketExpensesSchema =Schema({
  expenseType:String,
  expenseNumMiles:Number,
  expenseRatePerMile:Number,
  expenseAmount:Number,
  expenseDate:Date,
  expenseContextType:String,
  expenseDetail:String,
  documents:[documentsSchema],
})

const timeTrackerSchema =Schema({
  fromDate:Date,
  toDate:Date,
  hours:Number,
})

const billingManageExpenseSchema = new Schema({
  activityId:Schema.Types.ObjectId,
  activityTranType:String,
  activityTranSubType:String,
  activityDescription:String,
  activityTranFirmId:Schema.Types.ObjectId,
  activityTranFirmName:String,
  activityTranClientId:Schema.Types.ObjectId,
  activityClientName:String,
  userId:Schema.Types.ObjectId,
  userFirstName:String,
  userLastName:String,
  roleType:String,
  outOfPocketExpensesData: [pocketExpensesSchema],
  timeTracker: [timeTrackerSchema],
  lastUpdateDate:{type:Date,required: true,default: Date.now},
  confirmation:Boolean,
}).plugin(timestamps)

billingManageExpenseSchema.index({activityId:1})
billingManageExpenseSchema.index({activityTranClientId:1})


export const BillingManageExpense = mongoose.model("billingmanageexpense", billingManageExpenseSchema)


