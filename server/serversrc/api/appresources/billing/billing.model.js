import mongoose from "mongoose"
import timestamps from "mongoose-timestamp"
import {EntityUser} from "../entityUser/entityUser.model"

const { Schema } = mongoose

const billReceiptsSchema =Schema({
  docCategory:String,
  docSubCategory:String,
  docType:String,
  docAWSFileLocation:String,
  docFileName:String,
  docStatus:String, // WIP, send for review and other flags
  docActions:[{
    actionType:String,
    actionDate:Date
  }],
  lastUpdatedDate: { type: Date, required: true, default: Date.now }
})

const billingReceiptsUserSchema = Schema({
  firmUserId:Schema.Types.ObjectId,
  userFirstName:String,
  userLastName:String,
  receiptUploads:[billReceiptsSchema]
})

const sofSchema = new Schema({
  securityType: String,
  sofDealType: String,
  feesType: String,
  sofMin: Number,
  sofMax: Number,
  fees: [{
    sofDesc: String,
    sofAmt: Number,
    sofCharge: Number
  }]
})

const nonTranFeesSchema = new Schema({
  role: String ,
  stdHourlyRate: Number ,
  quoatedRate:Number
})
const retAndEscSchema = new Schema({
  type: String,
  hoursCovered: Number ,
  effStartDate: Date ,
  effEndDate: Date
})

const outOfPocketExpensesSchema = new Schema({
  expenseType:String,
  expenseNumMiles:Number,
  expenseRatePerMile:Number,
  expenseAmount:Number,
  expenseDate:Date,
  expenseContextType:String,
  expenseDetail:String,
  expenseUploadTime:{type:Date,default:Date.now()}
})

const outOfPockUserExpensesSchema = new Schema({
  tenantUserId:Schema.Types.ObjectId,
  userFirstName:String,
  userLastName:String,
  outOfPocketExpenses:[outOfPocketExpensesSchema],
  billingReceipts:[billReceiptsSchema]
})

const contractReferenceSchema = new Schema({
  contractId:String,
  contractName:String,
  contractType:String,
  startDate: Date,
  endDate: Date,
})

const bankDetailsSchema = new Schema({
  bankName: String,
  addressLine1: String,
  addressLine2: String,
  ABA: String,
  accountOf: String,
  accountNo: Number,
  attention: String,
  telephone: String
})

export const contractDocumentsSchema=Schema({
  docCategory:String,
  docSubCategory:String,
  docType:String,
  docAWSFileLocation:String,
  docFileName:String,
  createdBy: {type: Schema.Types.ObjectId, ref: EntityUser},
  createdUserName: String,
  uploadedDate: Date
})

const billServiceFeeSchema = new Schema({
  activityId:Schema.Types.ObjectId,
  activityTranType:String,
  activityTranSubType:String,
  activityDescription:String,
  activityTranFirmId:Schema.Types.ObjectId,
  activityTranFirmName:String,
  activityTranClientId:Schema.Types.ObjectId,
  activityClientName:String,
  activityParAmount:Number,
  activityClientAddress:Schema.Types.Mixed,
  invoiceNumber:String,
  outOfPocketExpenses:Schema.Types.Mixed,
  consultingCharge:Schema.Types.Mixed,
  activityLeadAdvisorId:Schema.Types.ObjectId,
  activityLeadAdvisorName:String,
  contractRef:contractReferenceSchema,
  documents:[contractDocumentsSchema],
  bankDetails: bankDetailsSchema,
  digitize: Boolean,
  sof: [sofSchema],
  nonTranFees: [nonTranFeesSchema],
  retAndEsc: [retAndEscSchema],
  outOfPocketUserExpenses:[outOfPockUserExpensesSchema],
  totalFinancialAdvisoryFees:Number,
  totalConsultingEngFees:Number,
  totalOutOfPocketExpenses:Number,
  totalOverallExpenses:Number,
  notes: String,
  invoiceNotes: String,
  userReceipts:[billingReceiptsUserSchema],
  invoiceStatus:String, // This information is available on the dashboard.
  lastUpdateDate:{type:Date,required: true,default: Date.now}
}).plugin(timestamps)

billServiceFeeSchema.index({activityId:1})
billServiceFeeSchema.index({activityTranClientId:1})
billServiceFeeSchema.index({activityId:1, activityTranClientId:1})

export const TenantBilling = mongoose.model("tenantservicebilling", billServiceFeeSchema)


