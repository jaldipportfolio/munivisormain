import express from "express"
import personController from "./person.controller"
import {personAddressRouter} from "./person.subdocsRouter"

export const personRouter = express.Router()
personRouter.use("/:id1/address", personAddressRouter)

personRouter.param("id", personController.findByParam)

personRouter
  .route("/")
  .get(personController.getAll)
  .post(personController.createOne)

personRouter
  .route("/:id")
  .get(personController.getOne)
  .put(personController.updateOne)
  .delete(personController.createOne)
