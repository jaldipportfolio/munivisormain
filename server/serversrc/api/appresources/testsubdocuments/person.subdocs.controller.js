import {generateSubDocControllers} from "../../modules/query"
import { Person } from "./person.model"

export const personAddressController = generateSubDocControllers(Person,"addresses")
