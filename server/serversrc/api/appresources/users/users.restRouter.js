import express from "express"
import usersController from "./users.controller"

export const usersRouter = express.Router()

usersRouter.param("id", usersController.findByParam)

usersRouter.route("/")
  .get(usersController.getAll)
  .post(usersController.createOne)

usersRouter.route("/:id")
  .get(usersController.getOne)
  .put(usersController.updateOne)
  .delete(usersController.createOne)
