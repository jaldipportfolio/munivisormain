import express from "express"

import logger from "./../../modules/logger"


export const securityRouter = express.Router()

securityRouter.route("/cspviolationreport/")
  .post( (req, res) => {
    logger.warn(JSON.stringify(req.body["csp-report"], null, 2))
    res.status(204).end()
  })
