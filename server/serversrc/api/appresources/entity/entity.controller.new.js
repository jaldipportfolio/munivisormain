import ip from "ip"
import AWS from "aws-sdk"
import uniqBy from "lodash.uniqby"
import {
  Entity,
  EntityRel,
  GlobRefEntity,
  EntityUser,
  AuditLog,
  Deals,
  BankLoans,
  Derivatives,
  Others,
  ActBusDev,
  RFP,
  ActMaRFP,
  Docs,
  Tasks,
  GiftsAndGratuities
} from "../models"
import {canUserPerformAction} from "../../commonDbFunctions"
import {updateEligibleIdsForLoggedInUserFirm} from "./../../entitlementhelpers"
import {deleteDocsInBulkForTenant, deleteDataInBulkForTenant, elasticSearchUpdateFunction} from "../../elasticsearch/esHelper"
import {generateConfigForEntities} from "../config/generateConfigForDifferentEntityTypes"
import {getTransactionInformationForLoggedInUser} from "./getTransactionFromEntityIds"

const {ObjectID} = require("mongodb")



const returnFromES = (tenantId,id) => elasticSearchUpdateFunction({tenantId,Model: Entity, _id: id, esType: "entities"})

export const getEntities = async(req, res) => {}

export const getEntityById = async(req, res) => {
  const {entityId} = req.params

  if (!entityId) {
    res
      .status(422)
      .send("No entity id provided")
  }

  try {
    const entity = await Entity.aggregate([
      {
        $match: {
          _id: ObjectID(entityId)
        }
      }, {
        $limit: 1
      }, {
        $lookup: {
          from: "entityrels",
          localField: "_id",
          foreignField: "entityParty2",
          as: "rel"
        }
      }, {
        $lookup: {
          from: "entityusers",
          localField: "primaryContactId",
          foreignField: "_id",
          as: "primaryContact"
        }
      }, {
        $replaceRoot: {
          newRoot: {
            $mergeObjects: [
              {
                relationshipType: {
                  $arrayElemAt: ["$rel.relationshipType", 0]
                }
              }, {
                primaryContactFirstName: {
                  $arrayElemAt: ["$primaryContact.userFirstName", 0]
                }
              }, {
                primaryContactLastName: {
                  $arrayElemAt: ["$primaryContact.userLastName", 0]
                }
              }, {
                primaryContactEmail: {
                  $arrayElemAt: ["$primaryContact.userLoginCredentials.userEmailId", 0]
                }
              },
              "$$ROOT"
            ]
          }
        }
      }, {
        $project: {
          rel: 0,
          primaryContact: 0
        }
      }
    ])
    res.json(entity)
  } catch (err) {
    res
      .status(422)
      .send({err})
  }
}

export const saveEntity = async(req, res) => {
  const {user} = req
  const {entityDetails, relationshipType} = req.body

  const resRequested = [
    {
      resource: "Entity",
      access: 2
    }, {
      resource: "EntityRel",
      access: 2
    }
  ]

  try {
    const start1 = new Date()
    const entitled = await canUserPerformAction(user, resRequested)
    console.log("WHAT WAS RETURNED FROM THE SERVICE", entitled)
    const time1 = new Date() - start1
    if (!entitled) {
      res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    const start2 = new Date()
    const entity = await Entity.create(entityDetails)
    const time2 = new Date() - start2
    // await updateEligibleIdsForLoggedInUserFirm(req.user)
    let entityParty1
    let entityParty2

    console.log("entity id : ", entity._id)
    if (["Client", "Prospect", "Third Party"].includes(relationshipType)) {
      entityParty1 = ObjectID(req.user.entityId)
      entityParty2 = ObjectID(entity._id)
    }

    const start3 = new Date()
    const rel = await EntityRel.create({entityParty1, entityParty2, relationshipType})
    const time3 = new Date() - start3
    const start4 = new Date()
    console.log("rel id : ", rel._id)
    await Promise.all([
      returnFromES(req.user.tenantId,entity._id),
      updateEligibleIdsForLoggedInUserFirm(req.user)
    ])
    const time4 = new Date() - start4
    // console.log("time1, 2, 3, 4", time1, time2, time3, time4)
    res.json([entity, time1, time2, time3, time4])
  } catch (e) {
    res
      .status(422)
      .send({done: false, error: e, success: "", requestedServices: resRequested})
  }
}

export const updateEntity = async(req, res) => {
  const {user} = req
  const {entityId} = req.params
  const {type} = req.query
  const {entityDetails, relationshipType} = req.body

  const resRequested = [
    {
      resource: "Entity",
      access: 2
    }, {
      resource: "EntityRel",
      access: 2
    }
  ]

  try {
    const start1 = new Date()
    const entitled = await canUserPerformAction(user, resRequested)
    const time1 = new Date() - start1
    console.log("WHAT WAS RETURNED FROM THE SERVICE", entitled)
    if (!entitled) {
      res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    const start2 = new Date()

    if (relationshipType === "Self") {
      const regexSearchTerm = new RegExp(entityDetails.firmName,"i")
      let selfEntities = await Entity.find({"firmName": regexSearchTerm}).select("firmName")
      selfEntities = (selfEntities && selfEntities.length) ? selfEntities.filter(e => e._id.toString() !== req.user.tenantId.toString()) : []

      if (selfEntities && selfEntities.length) {
        return res.status(203).send({error:true, message:"Firm name is already existing choose another firm name!"})
      }
    }

    const entity = await Entity.findOneAndUpdate({
      _id: entityId
    }, {
      $set: {
        ...entityDetails
      }
    })

    const entityUsers = await EntityUser.find({entityId, userFlags: "Primary Contact"}).select("userFlags")

    if(entityUsers && entityUsers.length){
      const entityUsersIds = entityUsers.map(e => e._id.toString() )
      await EntityUser.updateMany({_id: { $in: entityUsersIds } },   {$pull: {userFlags: "Primary Contact"}})
    }
    const isPrimaryContact = entityDetails && entityDetails.primaryContactId
    if(isPrimaryContact){
      await EntityUser.updateOne({ _id: entityDetails.primaryContactId} , {
        $addToSet: { userFlags: "Primary Contact" }
      })
    }

    const time2 = new Date() - start2
    // await updateEligibleIdsForLoggedInUserFirm(req.user)
    console.log("entity id : ", entity._id)
    const start3 = new Date()
    if (["Client", "Prospect", "Third Party"].includes(relationshipType)) {
      const rel = await EntityRel.findOneAndUpdate({
        entityParty1: ObjectID(req.user.entityId),
        entityParty2: ObjectID(entityId)
      }, {$set: {
        relationshipType
      }})
    }

    console.log("=======type==========", type)
    if(type === "migrated"){
      console.log("=======entityId==========", entityId)
      await generateConfigForEntities([entityId])
    }
    const time3 = new Date() - start3
    const returnValueEs = await returnFromES(req.user.tenantId, entityId)
    console.log("After inserting into ES ------", returnValueEs)

    const start4 = new Date()
    await updateEligibleIdsForLoggedInUserFirm(req.user)
    const time4 = new Date() - start4
    res.json([entity, time1, time2, time3, time4])
  } catch (e) {
    res
      .status(422)
      .send({done: false, error: e, success: "", requestedServices: resRequested})
  }
}

export const lookupBorrowerObligor = async(req, res) => {
  const resRequested = [
    {
      resource: "Entity",
      access: 1
    }
  ]
  const {type, searchString} = req.body
  try {
    const typeMatch = type
      ? {
        "$eq": ["$$entity.entityType", type]
      }
      : {
        "$in": [
          "$$entity.entityType",
          ["Governmental Entity / Issuer", "501c3 - Obligor"]
        ]
      }
    const invalid = /[°"§%()\[\]{}=\\?´`'#<>|,;.:+_-]+/g
    const newSearchString = searchString.replace(invalid, "")
    const regexValue = new RegExp(newSearchString, "i")
    const searchStringLower = newSearchString.toLowerCase()
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    const relatedEntities = await EntityRel.aggregate([
      {
        $match: {
          entityParty2: req.user.entityId
        }
      }, {
        $lookup: {
          "from": "entityrels",
          "localField": "entityParty1",
          "foreignField": "entityParty1",
          "as": "rels"
        }
      }, {
        $project: {
          rels: {
            $filter: {
              "input": "$rels",
              "as": "rel",
              "cond": {
                "$ne": ["$$rel.relationshipType", "Self"]
              }
            }
          }
        }
      }, {
        $project: {
          entityParty2: "$rels.entityParty2"
        }
      }, {
        $unwind: "$entityParty2"
      }, {
        $lookup: {
          "from": "entities",
          "localField": "entityParty2",
          "foreignField": "_id",
          "as": "entities"
        }
      }, {
        $project: {
          entities: {
            $filter: {
              "input": "$entities",
              "as": "entity",
              "cond": {
                $and: [
                  typeMatch, {
                    $gt: [
                      {
                        $indexOfCP: [
                          {
                            $toLower: "$$entity.firmName"
                          },
                          searchStringLower
                        ]
                      },
                      -1
                    ]
                  }
                ]
              }
            }
          }
        }
      }, {
        $unwind: "$entities"
      }, {
        $project: {
          _id: "$entities._id",
          firmName: "$entities.firmName",
          entityType: "$entities.entityType"
        }
      }
    ])
    const entityList = relatedEntities.map(r => r.firmName)
    const globIssuer = await GlobRefEntity
      .find({
        participantType: "Governmental Entity / Issuer",
        firmName: {
          $nin: entityList,
          $regex: regexValue,
          $options: "i"
        }
      })
      .select("firmName _id participantType")
      .limit(25)
    let entities = []
    relatedEntities
      .slice(0, 25)
      .forEach(item => {
        const firm = {
          _id: item._id,
          firmName: item.firmName,
          participantType: item.entityType,
          related: true
        }
        entities.push(firm)
      })
    entities = [
      ...entities,
      ...globIssuer
    ]
    res.json(entities)
  } catch (err) {
    res
      .status(422)
      .send({done: false, error: err, success: "", requestedServices: resRequested})
  }
}

export const lookupThirdParties = async(req, res) => {
  const resRequested = [
    {
      resource: "Entity",
      access: 1
    }
  ]
  const {searchString} = req.body
  const invalid = /[°"§%()\[\]{}=\\?´`'#<>|,;.:+_-]+/g
  const newSearchString = searchString.replace(invalid, "")
  const regexValue = new RegExp(newSearchString, "i")
  const searchStringLower = newSearchString.toLowerCase()
  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }
    const relatedEntities = await EntityRel.aggregate([
      {
        $match: {
          entityParty2: req.user.entityId
        }
      }, {
        $lookup: {
          "from": "entityrels",
          "localField": "entityParty1",
          "foreignField": "entityParty1",
          "as": "rels"
        }
      }, {
        $project: {
          rels: {
            $filter: {
              "input": "$rels",
              "as": "rel",
              "cond": {
                "$eq": ["$$rel.relationshipType", "Third Party"]
              }
            }
          }
        }
      }, {
        $project: {
          entityParty2: "$rels.entityParty2"
        }
      }, {
        $unwind: "$entityParty2"
      }, {
        $lookup: {
          "from": "entities",
          "localField": "entityParty2",
          "foreignField": "_id",
          "as": "entities"
        }
      }, {
        $project: {
          entities: {
            $filter: {
              "input": "$entities",
              "as": "entity",
              "cond": {
                $gt: [
                  {
                    $indexOfCP: [
                      {
                        $toLower: "$$entity.firmName"
                      },
                      searchStringLower
                    ]
                  },
                  -1
                ]
              }
            }
          }
        }
      }, {
        $unwind: "$entities"
      }, {
        $project: {
          _id: "$entities._id",
          firmName: "$entities.firmName",
          entityType: "$entities.entityType"
        }
      }
    ])
    const entityList = relatedEntities.map(r => r.firmName)
    const globIssuer = await GlobRefEntity
      .find({
        firmType: "Third Party",
        firmName: {
          $nin: entityList,
          $regex: regexValue,
          $options: "i"
        }
      })
      .select("firmName _id participantType")
      .limit(25)
    let entities = []
    relatedEntities
      .slice(0, 25)
      .forEach(item => {
        const firm = {
          _id: item._id,
          firmName: item.firmName,
          participantType: item.entityType,
          related: true
        }
        entities.push(firm)
      })
    entities = [
      ...entities,
      ...globIssuer
    ]
    res.json(entities)
  } catch (err) {
    res
      .status(422)
      .send({done: false, error: err, success: "", requestedServices: resRequested})
  }
}

export const s3 = new AWS.S3({
  region : process.env.SESREGION,
  bucket: process.env.S3BUCKET,
  accessKeyId: process.env.S3ACCESSKEYID,
  secretAccessKey: process.env.S3SECRETKEY
})

export const copyDocuments = async (tenant, newContextId, keys) => {
  const bucketName = process.env.S3BUCKET        // example bucket

  try {

    const docs = await Docs.find({_id: { $in: keys }})
    for (const doc of docs) {
      const { meta, tenantId, contextType, contextId, _id } = doc

      await Promise.all(
        meta && meta.versions.map(async (vrs, index) => {

          const fileName = vrs && vrs.name
          const fileKey = `${tenantId}/${contextType}/${contextType}_${contextId}/${fileName}`
          const destinationFolder = `${tenantId}/${contextType}/${contextType}_${newContextId}/${fileName}`

          const params = {
            Bucket: bucketName,
            CopySource: `${bucketName}/${fileKey}`,  // old file Key
            Key: `${fileKey.replace(fileKey, destinationFolder)}`, // new file Key
          }

          s3.copyObject(params, async (copyErr, copyData) => {
            if (copyErr) {
              console.log(copyErr)
            } else {
              await Docs.update({ _id }, { $set: { contextId: newContextId, [`meta.versions.${index}.versionId`]: copyData && copyData.VersionId } })
              await s3.deleteObject({
                Bucket: bucketName,
                Key: fileKey,
              }).promise()
            }
          })
        })
      )
    }

    let elasticDocIds = []
    docs.forEach(doc => {
      elasticDocIds = [ ...elasticDocIds , ...doc.meta.versions.map(version => `${doc._id}~${version.versionId}`)]
    })
    await deleteDocsInBulkForTenant({
      tenantId: tenant,
      ids: elasticDocIds
    })
  } catch (err) {
    console.error(err) // error handling
  }
}

export const postMergeEntities = async(req, res) => {
  const resRequested = [
    {
      resource: "Entity",
      access: 1
    }
  ]
  const { selectedIds, parentEntityId, basicDetails, relationshipType, listType } = req.body
  const { firmName } = basicDetails
  const { tenantId } = req.user

  try {
    const entitled = await canUserPerformAction(req.user, resRequested)
    if (!entitled) {
      return res
        .status(422)
        .send({done: false, error: "User is not entitled to access the requested services", success: "", requestedServices: resRequested})
    }

    const matchData = await getTransactionInformationForLoggedInUser(req.user, selectedIds)

    /** ******************************* Merged Entities Users and update ************************************************ * */

    const entityUsers = await EntityUser.find({entityId: { $in: selectedIds }})
    let entityUsersIds = []

    if(entityUsers && entityUsers.length){
      entityUsersIds = entityUsers.map(e => e._id.toString() )
      await EntityUser.updateMany({_id: { $in: entityUsersIds } }, { $pull: { userFlags: "Primary Contact" } })
      await EntityUser.updateMany({_id: { $in: entityUsersIds } }, { $set: { entityId: parentEntityId, userFirmName: firmName } })
    }

    /** ******************************* Merged Entities Details ************************************************ * */

    const entityDetails = await Entity.find({_id: { $in: [...selectedIds, parentEntityId] }}).select("addresses entityAliases entityLinkedCusips entityBorObl contracts entityDocuments notes firmName entityFlags childHistoricalData historicalData")
    const mergeEntity = {childHistoricalData: []}
    const firmNames = []
    let isAddress = true

    const parent = entityDetails.find(e => e._id.toString() === parentEntityId)
    const selected = entityDetails.filter(e => e._id.toString() !== parentEntityId)

    if(parent && parent.addresses && parent.addresses.length){
      isAddress = false
    }

    /** ******************************* Merged Entities Meta data Merge into Parent Entity ************************************************ * */

    const entityFlags = parent && parent.entityFlags || {}
    const type = listType === "third-party" ? "marketRole" : "issuerFlags"

    selected && selected.length && selected.forEach(snap => {
      const keyObj = ["addresses", "entityAliases", "entityLinkedCusips", "entityBorObl", "contracts", "entityDocuments", "notes", "childHistoricalData"]
      snap && snap.addresses && snap.addresses.forEach(obj => {
        if(isAddress && obj.isPrimary){
          obj.isPrimary = true
          isAddress = false
        } else {
          obj.isPrimary = false
        }
      })

      if(snap && snap.historicalData && Object.keys(snap.historicalData).length){
        mergeEntity.childHistoricalData.push(snap.historicalData)
      }

      keyObj.forEach(async key => {
        mergeEntity[key] = [ ...(mergeEntity[key] || []), ...(snap[key] || []) ]

        if(key === "entityAliases"){
          mergeEntity[key].push(snap.firmName)
        }

        /** ******************************* Copy AWS S3 Documents ************************************************ * */

        if((key === "contracts" || key === "entityDocuments") && snap && snap[key] && snap[key].length){
          const keys = []
          if(key === "contracts"){
            snap[key].map(m => m.documents.map(d => keys.push(d.docAWSFileLocation)))
          }
          await copyDocuments(tenantId, parentEntityId, key === "contracts" ? keys : snap[key].map(s => s.docAWSFileLocation))
        }

        /** ********************************************************************************************************************** * */

      })

      if((listType === "third-party" || listType === "client-prospect") && snap && snap.entityFlags && snap.entityFlags[type] && snap.entityFlags[type].length){
        snap.entityFlags[type].forEach(t => {
          entityFlags[type] = [...((entityFlags && entityFlags[type]) || ""), t]
        })
      }
      firmNames.push(snap && snap.firmName)
    })

    const log = {
      groupId: req.user.tenantId,
      type: parentEntityId,
      changeLog: [
        {
          userName: `${req.user.userFirstName} ${req.user.userLastName}`,
          log: `${firmNames} merged into ${firmName}`,
          date: new Date(),
          key: "entities",
          ip: ip.address(),
          userId: req.user._id
        }
      ]
    }

    /** ******************************* Update Transactions Client Names And Entities ************************************************ * */

    if(listType === "third-party" || listType === "client-prospect") {

      entityFlags[type] = entityFlags && entityFlags[type].filter((f, g) => entityFlags[type].indexOf(f) === g)

      await Deals.updateMany({ dealIssueTranIssuerId: { $in: selectedIds }}, { $set: { dealIssueTranIssuerId: parentEntityId, dealIssueTranIssuerFirmName: firmName } })
      await BankLoans.updateMany({ actTranClientId: { $in: selectedIds }}, { $set: { actTranClientId: parentEntityId, actTranClientName: firmName } })
      await Derivatives.updateMany({ actTranClientId: { $in: selectedIds }}, { $set: { actTranClientId: parentEntityId, actTranClientName: firmName } })
      await Others.updateMany({ actTranClientId: { $in: selectedIds }}, { $set: { actTranClientId: parentEntityId, actTranClientName: firmName } })
      await ActBusDev.updateMany({ actIssuerClient: { $in: selectedIds }}, { $set: { actIssuerClient: parentEntityId, actIssuerClientEntityName: firmName } })
      await RFP.updateMany({ rfpTranIssuerId: { $in: selectedIds }}, { $set: { rfpTranIssuerId: parentEntityId, rfpTranIssuerFirmName: firmName } })
      await ActMaRFP.updateMany({ actIssuerClient: { $in: selectedIds }}, { $set: { actIssuerClient: parentEntityId, actIssuerClientEntityName: firmName } })

      await Entity.updateOne({ _id: parentEntityId }, { $set: { entityFlags } })

    } else {
      await Entity.updateOne({ _id: parentEntityId }, { $set: { ...basicDetails } })
    }

    await Entity.updateOne({ _id: parentEntityId }, { $addToSet: mergeEntity })

    const newAuditLog = new AuditLog({
      ...log
    })

    await newAuditLog.save()

    /** ******************************* Update Relationship Type of Merged Entity ************************************************ * */

    if(listType === "migratedentities"){
      if (["Client", "Prospect", "Third Party"].includes(relationshipType)) {
        await EntityRel.findOneAndUpdate({
          entityParty1: ObjectID(req.user.entityId),
          entityParty2: ObjectID(parentEntityId)
        }, {$set: {
          relationshipType
        }})
      }
    }

    /** ******************************* Remove Merged Entities and Entities Relation ************************************************ * */

    await EntityRel.deleteMany({entityParty2: { $in: selectedIds }})
    await Entity.deleteMany({_id: { $in: selectedIds }})

    /** ******************************* Add Configuration on Migrated Entities ************************************************ * */

    if(req.query.type === "migrated"){
      await generateConfigForEntities([parentEntityId])
    }

    /** ******************************* Parent Entity Update on Elastic Search ************************************************************** * */

    const returnValueEs = await returnFromES(req.user.tenantId, parentEntityId)
    console.log("After inserting into ES ------", returnValueEs)

    await updateEligibleIdsForLoggedInUserFirm(req.user)

    /** ******************************* Remove Entities on Elastic Search ************************************************************** * */

    await deleteDataInBulkForTenant({
      tenantId: req.user.tenantId,
      ids: selectedIds
    })

    /** ******************************* Update Tasks On Elastic Search and mongoDB ************************************************************** * */

    await Tasks.updateMany({ "taskDetails.taskAssigneeUserId": { $in: selectedIds }}, { $set: { "taskDetails.taskAssigneeUserId": parentEntityId, "taskDetails.taskAssigneeName": firmName } })
    await Tasks.updateMany({ "relatedActivityDetails.activityIssuerClientId": { $in: selectedIds }}, { $set: { "relatedActivityDetails.activityIssuerClientId": parentEntityId } })
    await Tasks.updateMany({ "relatedEntityDetails.entityId": { $in: selectedIds }}, { $set: { "relatedEntityDetails.entityId": parentEntityId, "relatedEntityDetails.entityName": firmName } })

    const tasks = await Tasks.find({"relatedActivityDetails.activityIssuerClientId": parentEntityId}) || []
    const task = await Tasks.find({"relatedEntityDetails.entityId": parentEntityId}) || []
    const taskData = tasks.concat(task)
    if(taskData && taskData.length){
      taskData.map(async t => {
        await elasticSearchUpdateFunction({tenantId: tenantId.toString(), Model: Tasks, _id: t._id, esType: "mvtasks"})
      })
    }

    const transactionType = ["dealChecklists", "bankLoanChecklists", "derivativeChecklists", "actTranChecklists", "rfpBidCheckList"]

    transactionType.map(async tran => {
      const Model = tran === "dealChecklists" ? Deals :
        tran === "rfpBidCheckList" ? RFP :
          tran === "bankLoanChecklists" ? BankLoans :
            tran === "derivativeChecklists" ? Derivatives :
              tran === "ActMaRFP" ? ActMaRFP :
                tran === "actTranChecklists" ? Others :
                  tran === "ActBusDev" ? ActBusDev : ""


      const checkList = await Model.find({}).select(tran)

      if(checkList && checkList.length){
        for (const mainData of checkList){
          const { _id } = mainData
          const checkListData = mainData[tran]
          if(checkListData && checkListData.length){
            for (const [index, check] of checkListData.entries()){
              const { data } = check
              for (const [secondIndex, secondData] of data.entries()){
                const { items } = secondData
                for (const [thirdIndex, thirdData] of items.entries()){
                  const { assignedTo } = thirdData
                  if(assignedTo && assignedTo.length){
                    for (const [fourthIndex, fourthData] of assignedTo.entries()){

                      await Model.update(
                        { _id, [`${tran}.${index}.data.${secondIndex}.items.${thirdIndex}.assignedTo.${fourthIndex}._id`]: { $in: selectedIds.map(s => s.toString()) } },
                        { $set: {
                          [`${tran}.${index}.data.${secondIndex}.items.${thirdIndex}.assignedTo.${fourthIndex}._id`]: parentEntityId,
                          [`${tran}.${index}.data.${secondIndex}.items.${thirdIndex}.assignedTo.${fourthIndex}.name`]: firmName
                        } }
                      )

                    }
                  }
                }
              }
            }
          }
        }
      }
    })

    const giftList = await GiftsAndGratuities.find({ finAdvisorEntityId: tenantId }).select("giftsToRecipients")

    if(giftList && giftList.length){
      giftList.map(gift => {
        if(gift && gift.giftsToRecipients && gift.giftsToRecipients.length){
          gift.giftsToRecipients.map(async (g, index) => {
            await GiftsAndGratuities.update(
              { _id: gift._id, [`giftsToRecipients.${index}.recipientEntityId`]: { $in: selectedIds } },
              { $set: {
                [`giftsToRecipients.${index}.recipientEntityId`]: parentEntityId,
                [`giftsToRecipients.${index}.recipientEntityName`]: firmName
              } }
            )
          })
        }
      })
    }

    /** ******************************* Transaction Update On Elastic Search ************************************************************** * */

    if(matchData && matchData.data && matchData.data.length){

      await Promise.all(
        matchData.data.map(async tran => {
          const { type, tranId, bankLoanParticipants, derivativeParticipants, maRfpParticipants, rfpParticipants, participants, actTranUnderwriters, dealIssueParticipants, dealIssueUnderwriters } = tran

          const esType = type === "Deal" ? "tranagencydeals" :
            type === "RFP" ? "tranagencyrfps" :
              type === "BankLoans" ? "tranbankloans" :
                type === "Derivatives" ? "tranderivatives" :
                  type === "ActMaRFP" ? "actmarfps" :
                    type === "Others" ? "tranagencyothers" :
                      type === "ActBusDev" ? "actbusdevs" : ""

          const Model = type === "Deal" ? Deals :
            type === "RFP" ? RFP :
              type === "BankLoans" ? BankLoans :
                type === "Derivatives" ? Derivatives :
                  type === "ActMaRFP" ? ActMaRFP :
                    type === "Others" ? Others :
                      type === "ActBusDev" ? ActBusDev : ""

          const participantsKey = type === "BankLoans" ? bankLoanParticipants :
            type === "Derivatives" ? derivativeParticipants :
              type === "ActMaRFP" ? maRfpParticipants :
                type === "Others" ? participants : ""


          const key = type === "BankLoans" ? "bankLoanParticipants" :
            type === "Derivatives" ? "derivativeParticipants" :
              type === "ActMaRFP" ? "maRfpParticipants" :
                type === "Others" ? "participants" : ""

          if( type === "Deal" ){
            dealIssueParticipants && dealIssueParticipants.length && dealIssueParticipants.map(async f => {
              if(selectedIds.findIndex(s => s.toString() === f.dealPartFirmId.toString()) > -1){
                f.dealPartFirmId = parentEntityId
                f.dealPartFirmName = firmName
                await Deals.updateOne({_id: tranId, "dealIssueParticipants._id": ObjectID(f._id) }, { $set: { "dealIssueParticipants.$": f }})
              }
            })
            dealIssueUnderwriters && dealIssueUnderwriters.length && dealIssueUnderwriters.map(async f => {
              if(selectedIds.findIndex(s => s.toString() === f.dealPartFirmId.toString()) > -1){
                f.dealPartFirmId = parentEntityId
                f.dealPartFirmName = firmName
                await Deals.updateOne({_id: tranId, "dealIssueUnderwriters._id": ObjectID(f._id) }, { $set: { "dealIssueUnderwriters.$": f }})
              }
            })
          }

          if( type === "BankLoans" || type === "Derivatives" || type === "ActMaRFP" || type === "Others" ) {

            participantsKey && participantsKey.length && participantsKey.map(async f => {
              if(selectedIds.findIndex(s => s.toString() === f.partFirmId.toString()) > -1){
                f.partFirmId = parentEntityId
                f.partFirmName = firmName
                await Model.updateOne({_id: tranId, [`${key}._id`]: ObjectID(f._id) }, { $set: { [`${key}.$`]: f }})
              }
            })

            if( type === "Others" ){
              actTranUnderwriters && actTranUnderwriters.length && actTranUnderwriters.map(async f => {
                if(selectedIds.findIndex(s => s.toString() === f.partFirmId.toString()) > -1){
                  f.partFirmId = parentEntityId
                  f.partFirmName = firmName
                  await Model.updateOne({_id: tranId, "actTranUnderwriters._id": ObjectID(f._id) }, { $set: { "actTranUnderwriters.$": f }})
                }
              })
            }
          }

          if( type === "RFP" ){
            rfpParticipants && rfpParticipants.length && rfpParticipants.map(async f => {
              if(selectedIds.findIndex(s => s.toString() === f.rfpParticipantFirmId.toString()) > -1){
                f.rfpParticipantFirmId = parentEntityId
                f.rfpParticipantRealFirmName = firmName
                await RFP.updateOne({_id: tranId, "rfpParticipants._id": ObjectID(f._id) }, { $set: { "rfpParticipants.$": f }})
              }
            })
          }

          await elasticSearchUpdateFunction({tenantId: tenantId.toString(), Model, _id: tranId.toString(), esType})
        })
      )
    }

    res.status(200).send({done: true, entityUsersIds, log, entityFlags, selected})

  } catch (err) {
    console.log("====================604", err)
    res
      .status(422)
      .send({done: false, error: err, success: "", requestedServices: resRequested})
  }
}

export const getTransaction = async (req, res) => {

  try {
    const result = await getTransactionInformationForLoggedInUser(req.user, req.body.entityId)
    res.json(result)
  } catch (error) {
    res
      .status(422)
      .send({error})
  }
}

export const postEntitiesAttachWithTransactions = async (req, res) => {

  const { selectedIds } = req.body
  try {

    const duplicateData = []
    const matchData = await getTransactionInformationForLoggedInUser(req.user, selectedIds)
    const { data } = matchData

    data.map(d => {
      const { tranAttributes } = d
      const { status, clientId, clientName } = tranAttributes
      const types = ["Deal", "BankLoans", "Derivatives", "Others", "RFP", "ActMaRFP"]
      const client = clientId.toString()

      if(types.includes(d.type)){

        if(status === "Closed"){

          if(selectedIds.findIndex(s => s === client) !== -1){
            duplicateData.push({id: client, name: clientName})
          }

          const loop = ["bankLoanParticipants", "derivativeParticipants", "participants", "actTranUnderwriters", "maRfpParticipants", "dealIssueParticipants", "dealIssueUnderwriters", "rfpParticipants"]

          loop.map(key => {
            d[key] && d[key].length && d[key].map(f => {

              const firmId =
                (key === "bankLoanParticipants" || key === "derivativeParticipants" || key === "participants" || key === "actTranUnderwriters" || key === "maRfpParticipants") ? "partFirmId" :
                  (key === "dealIssueParticipants" || key === "dealIssueUnderwriters") ? "dealPartFirmId" : key === "rfpParticipants" ? "rfpParticipantFirmId" : ""

              const firmName =
                (key === "bankLoanParticipants" || key === "derivativeParticipants" || key === "participants" || key === "actTranUnderwriters" || key === "maRfpParticipants") ? "partFirmName" :
                  (key === "dealIssueParticipants" || key === "dealIssueUnderwriters") ? "dealPartFirmName" : key === "rfpParticipants" ? "rfpParticipantRealFirmName" : ""

              if(selectedIds.findIndex(s => s.toString() === f[firmId].toString()) > -1){
                duplicateData.push({id: f[firmId].toString(), name: f[firmName]})
              }

            })
          })

        }
      }

    })

    const notMerge = uniqBy(duplicateData, "id")
    res.status(200).send({done: true, status: "merged", notMerge, matchData})
  } catch (error) {
    res
      .status(422)
      .send({error})
  }
}
