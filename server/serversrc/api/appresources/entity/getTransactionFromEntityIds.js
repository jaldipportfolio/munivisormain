import { ObjectID} from "mongodb"

import { getAllAccessAndEntitlementsForLoggedInUser } from "./../../entitlementhelpers"
import  {
  RFP, Deals, BankLoans, Derivatives, ActMaRFP,Others,ActBusDev
}  from "./../models"

const transRelatedToEntitiesLookup = async ({trantype,tranIds,refEntity,tenantId,applyEntitlement}) => {

  let matchQuery = []

  const referenceQuery = {
    deals:[{"$match":{dealIssueTranClientId:ObjectID(tenantId)}}],
    rfps:[{"$match":{rfpTranClientId:ObjectID(tenantId)}}],
    bankloans:[{"$match":{actTranFirmId:ObjectID(tenantId)}}],
    derivatives:[{"$match":{actTranFirmId:ObjectID(tenantId)}}],
    marfps:[{"$match":{actTranFirmId:ObjectID(tenantId)}}],
    others:[{"$match":{actTranFirmId:ObjectID(tenantId)}}],
    busdevs:[{"$match":{actTranFirmId:ObjectID(tenantId)}}]
  }

  if(applyEntitlement) {
    matchQuery = [{ $match:{"_id":{"$in":[...tranIds || []]}}}]
    matchQuery = [...matchQuery, ...referenceQuery[trantype]]
  } else {
    matchQuery = referenceQuery[trantype]
  }

  let checkRefEntity = []

  if (refEntity) {
    checkRefEntity = [      {
      $match:{"allTranEntities": { $in: refEntity } }
    }]
  }

  if( trantype === "deals" ) {
    return Deals.aggregate([
      ...matchQuery,
      {
        $project: {
          _id:"$_id",
          dealIssueTranClientId:1,
          dealIssueTranIssuerId:1,
          dealIssueUnderwriters:1,
          dealIssueParticipants:1,
          dealParticipantFirms:{$setUnion:["$dealIssueUnderwriters.dealPartFirmId","$dealIssueParticipants.dealPartFirmId"]},
          dealParticipantUsers:{$setUnion:["$dealIssueParticipants.dealPartContactId","$dealIssueTranAssignedTo",["$createdByUser"]]},
          tranAttributes:{
            projectDescription:"$dealIssueTranProjectDescription",
            status:"$dealIssueTranStatus",
            clientId: "$dealIssueTranIssuerId",
            clientName: "$dealIssueTranIssuerFirmName"
          }
        },
      },
      {
        $lookup:
          {
            from: "entityusers",
            localField: "dealParticipantUsers",
            foreignField: "_id",
            as: "participantUserDetails"
          }
      },
      {$unwind:{"path":"$participantUserDetails", "preserveNullAndEmptyArrays": true}},
      {
        $project: {
          _id:1,
          dealIssueTranClientId:1,
          dealIssueTranIssuerId:1,
          dealParticipantFirms:1,
          dealParticipantUsers:1,
          tranAttributes:1,
          dealIssueUnderwriters:1,
          dealIssueParticipants:1,
          tranEntities:"$participantUserDetails.entityId"
        },
      },
      {
        $group:{
          _id:{tranId:"$_id",dealIssueTranClientId:"$dealIssueTranClientId",dealIssueTranIssuerId:"$dealIssueTranIssuerId",
            tranAttributes:"$tranAttributes",dealParticipantFirms:"$dealParticipantFirms", dealIssueUnderwriters: "$dealIssueUnderwriters", dealIssueParticipants: "$dealIssueParticipants"},
          dealIssueTranClientId:{$addToSet:"$dealIssueTranClientId"},
          dealIssueTranIssuerId:{$addToSet:"$dealIssueTranIssuerId"},
          tranEntities:{$push:"$tranEntities"}
        }
      },
      { $project:
          { _id:0, tranId:"$_id.tranId",tranAttributes:"$_id.tranAttributes", dealIssueUnderwriters:"$_id.dealIssueUnderwriters", dealIssueParticipants:"$_id.dealIssueParticipants",
            allTranEntities:{ $setUnion: [ "$_id.dealParticipantFirms","$dealIssueTranClientId","$dealIssueTranIssuerId","$tranEntities" ] }, type: "Deal"
          }
      },
      {
        $match:{"allTranEntities": { $in: refEntity } }
      }
    ])
  }

  if( trantype === "rfps" ) {
    return RFP.aggregate([
      ...matchQuery,
      {
        $project: {
          _id:"$_id",
          rfpTranClientId:1,
          rfpTranIssuerId:1,
          rfpEvaluationTeam:1,
          rfpProcessContacts:1,
          rfpParticipants:1,
          rfpParticipantUsers:{$setUnion:["$rfpEvaluationTeam.rfpSelEvalContactId","$rfpProcessContacts.rfpProcessContactId", "$rfpParticipants.rfpParticipantContactId", "$rfpTranAssignedTo", ["$createdByUser"]]},
          tranAttributes:{
            projectDescription:"$rfpTranProjectDescription",
            status:"$rfpTranStatus",
            clientId:"$rfpTranIssuerId",
            clientName:"$rfpTranIssuerFirmName",
          }
        },
      },
      {
        $lookup:
          {
            from: "entityusers",
            localField: "rfpParticipantUsers",
            foreignField: "_id",
            as: "participantUserDetails"
          }
      },
      {$unwind:{"path":"$participantUserDetails", "preserveNullAndEmptyArrays": true}},
      {
        $project: {
          _id:1,
          rfpTranClientId:1,
          rfpTranIssuerId:1,
          rfpParticipantUsers:1,
          tranAttributes:1,
          rfpEvaluationTeam:1,
          rfpProcessContacts:1,
          rfpParticipants:1,
          tranEntities:"$participantUserDetails.entityId"
        },
      },
      {
        $group:{
          _id:{tranId:"$_id",rfpTranClientId:"$rfpTranClientId",rfpTranIssuerId:"$rfpTranIssuerId", tranAttributes:"$tranAttributes", rfpEvaluationTeam: "$rfpEvaluationTeam",
            rfpProcessContacts: "$rfpProcessContacts", rfpParticipants: "$rfpParticipants"},
          rfpTranClientId:{$addToSet:"$rfpTranClientId"},
          rfpTranIssuerId:{$addToSet:"$rfpTranIssuerId"},
          tranEntities:{$push:"$tranEntities"}
        }
      },
      { $project: {
        _id:0, tranId:"$_id.tranId", rfpEvaluationTeam:"$_id.rfpEvaluationTeam", rfpProcessContacts:"$_id.rfpProcessContacts", rfpParticipants:"$_id.rfpParticipants",tranAttributes:"$_id.tranAttributes",
        allTranEntities:{ $setUnion: [ "$tranEntities","$rfpTranClientId","$rfpTranIssuerId" ] }, type: "RFP" } },
      {
        $match:{"allTranEntities": { $in: refEntity } }
      }
    ])
  }

  if( trantype === "bankloans" ) {
    return BankLoans.aggregate([
      ...matchQuery,
      {
        $project: {
          _id:"$_id",
          bankLoansFirmId:"$actTranFirmId",
          bankLoansClientId:"$actTranClientId",
          bankLoanParticipants:1,
          bankLoansParticipantUsers:{$setUnion:["$bankLoanParticipants.partContactId", ["$actTranFirmLeadAdvisorId"],["$createdByUser"]]},
          tranAttributes:{
            projectDescription:"$actTranProjectDescription",
            status:"$actTranStatus",
            clientId:"$actTranClientId",
            clientName:"$actTranClientName"
          }
        },
      },
      {$unwind:{"path":"$bankLoansParticipantUsers", "preserveNullAndEmptyArrays": true}},
      {
        $lookup:
          {
            from: "entityusers",
            localField: "bankLoansParticipantUsers",
            foreignField: "_id",
            as: "participantUserDetails"
          }
      },
      {$unwind:{"path":"$participantUserDetails", "preserveNullAndEmptyArrays": true}},
      {
        $project: {
          _id:1,
          bankLoansFirmId:1,
          bankLoansClientId:1,
          bankLoansParticipantUsers:1,
          tranAttributes:1,
          bankLoanParticipants:1,
          tranEntities:"$participantUserDetails.entityId"
        },
      },
      {
        $group:{
          _id:{tranId:"$_id",bankLoansFirmId:"$bankLoansFirmId",bankLoansClientId:"$bankLoansClientId", tranAttributes:"$tranAttributes", bankLoanParticipants: "$bankLoanParticipants"},
          bankLoansFirmId:{$addToSet:"$bankLoansFirmId"},
          bankLoansClientId:{$addToSet:"$bankLoansClientId"},
          tranEntities:{$push:"$tranEntities"}
        }
      },
      {
        $project:
          { _id:0, tranId:"$_id.tranId",
            tranAttributes:"$_id.tranAttributes",
            bankLoanParticipants:"$_id.bankLoanParticipants",
            allTranEntities:{ $setUnion: [ "$tranEntities","$bankLoansFirmId","$bankLoansClientId" ] },
            type: "BankLoans"
          }
      },
      {
        $match:{"allTranEntities": { $in: refEntity } }
      }
    ])
  }

  if( trantype === "derivatives" ) {
    return Derivatives.aggregate([
      ...matchQuery,
      {
        $project: {
          _id:"$_id",
          derivativesFirmId:"$actTranFirmId",
          derivativesClientId:"$actTranClientId",
          derivativeParticipants:1,
          derivativesParticipantUsers:{$setUnion:["$derivativeParticipants.partContactId", ["$actTranFirmLeadAdvisorId"],["$createdByUser"]]},
          tranAttributes:{
            projectDescription:"$actTranProjectDescription",
            status:"$actTranStatus",
            clientId:"$actTranClientId",
            clientName:"$actTranClientName"
          }
        },
      },
      {$unwind:{"path":"$derivativesParticipantUsers", "preserveNullAndEmptyArrays": true}},
      {
        $lookup:
          {
            from: "entityusers",
            localField: "derivativesParticipantUsers",
            foreignField: "_id",
            as: "participantUserDetails"
          }
      },
      {$unwind:{"path":"$participantUserDetails", "preserveNullAndEmptyArrays": true}},
      {
        $project: {
          _id:1,
          derivativesFirmId:1,
          derivativesClientId:1,
          derivativesParticipantUsers:1,
          tranAttributes:1,
          derivativeParticipants:1,
          tranEntities:"$participantUserDetails.entityId"
        },
      },
      {
        $group:{
          _id:{tranId:"$_id",derivativesFirmId:"$derivativesFirmId",derivativesClientId:"$derivativesClientId", tranAttributes:"$tranAttributes", derivativeParticipants:"$derivativeParticipants"},
          derivativesFirmId:{$addToSet:"$derivativesFirmId"},
          derivativesClientId:{$addToSet:"$derivativesClientId"},
          tranEntities:{$push:"$tranEntities"}
        }
      },
      {
        $project:
          { _id:0, tranId:"$_id.tranId",
            tranAttributes:"$_id.tranAttributes",
            derivativeParticipants:"$_id.derivativeParticipants",
            allTranEntities:{ $setUnion: [ "$tranEntities","$derivativesFirmId","$derivativesClientId" ] },
            type: "Derivatives"
          }
      },
      {
        $match:{"allTranEntities": { $in: refEntity } }
      }
    ])
  }

  if( trantype === "marfps" ) {
    return ActMaRFP.aggregate([
      ...matchQuery,
      {
        $project: {
          _id:"$_id",
          marfpFirmId:"$actTranFirmId",
          marfpClientId:"$actIssuerClient",
          maRfpParticipants:1,
          marfpParticipantUsers:{$setUnion:["$maRfpParticipants.partContactId", ["$actLeadFinAdvClientEntityId"],["$createdByUser"]]},
          tranAttributes:{
            projectDescription:"$actProjectName",
            status:"$actStatus",
            clientId:"$actIssuerClient",
            clientName:"$actIssuerClientEntityName"
          }
        },
      },
      {$unwind:{"path":"$marfpParticipantUsers", "preserveNullAndEmptyArrays": true}},
      {
        $lookup:
          {
            from: "entityusers",
            localField: "marfpParticipantUsers",
            foreignField: "_id",
            as: "participantUserDetails"
          }
      },
      {$unwind:{"path":"$participantUserDetails", "preserveNullAndEmptyArrays": true}},
      {
        $project: {
          _id:1,
          marfpFirmId:1,
          marfpClientId:1,
          marfpParticipantUsers:1,
          tranAttributes:1,
          maRfpParticipants:1,
          tranEntities:"$participantUserDetails.entityId"
        },
      },
      {
        $group:{
          _id:{tranId:"$_id",marfpFirmId:"$marfpFirmId",marfpClientId:"$marfpClientId", tranAttributes:"$tranAttributes", maRfpParticipants:"$maRfpParticipants"},
          marfpFirmId:{$addToSet:"$marfpFirmId"},
          marfpClientId:{$addToSet:"$marfpClientId"},
          tranEntities:{$push:"$tranEntities"}
        }
      },
      {
        $project:
          { _id:0, tranId:"$_id.tranId",
            tranAttributes:"$_id.tranAttributes",
            maRfpParticipants:"$_id.maRfpParticipants",
            allTranEntities:{ $setUnion: [ "$tranEntities","$marfpFirmId","$marfpClientId" ] },
            type: "ActMaRFP"
          }
      },
      ...checkRefEntity
    ])
  }

  if( trantype === "others" ) {
    return Others.aggregate([
      ...matchQuery,
      {
        $project: {
          _id:"$_id",
          actTranClientId:1,
          actTranIssuerId:1,
          participants:1,
          actTranUnderwriters:1,
          participantFirms:{$setUnion:["$actTranUnderwriters.partFirmId","$participants.partFirmId"]},
          participantUsers:{$setUnion:["$participants.partContactId","$actTranAssignedTo",["$createdByUser"]]},
          tranAttributes:{
            projectDescription:"$actTranProjectDescription",
            status:"$actTranStatus",
            parAmount:"$actTranParAmount",
            clientId:"$actTranClientId",
            clientName:"$actTranClientName"
          }
        },
      },
      {$unwind:{"path":"$participantUsers", "preserveNullAndEmptyArrays": true}},
      {
        $lookup:
          {
            from: "entityusers",
            localField: "participantUsers",
            foreignField: "_id",
            as: "participantUserDetails"
          }
      },
      {$unwind:{"path":"$participantUserDetails", "preserveNullAndEmptyArrays": true}},
      {
        $project: {
          _id:1,
          actTranClientId:1,
          actTranIssuerId:1,
          participantFirms:1,
          participantUsers:1,
          tranAttributes:1,
          participants:1,
          actTranUnderwriters:1,
          tranEntities:"$participantUserDetails.entityId"
        },
      },
      {
        $group:{
          _id:{
            tranId:"$_id",actTranClientId:"$actTranClientId",actTranIssuerId:"$actTranIssuerId", tranAttributes:"$tranAttributes",participantFirms:"$participantFirms",
            participants:"$participants", actTranUnderwriters:"$actTranUnderwriters",
          },
          actTranClientId:{$addToSet:"$actTranClientId"},
          actTranIssuerId:{$addToSet:"$actTranIssuerId"},
          tranEntities:{$push:"$tranEntities"},
          // tranUsers:{$push:"$participantUsers"}
        }
      },
      { $project:
          { _id:0, tranId:"$_id.tranId",tranAttributes:"$_id.tranAttributes", participants:"$_id.participants", actTranUnderwriters:"$_id.actTranUnderwriters",
            allTranEntities:{ $setUnion: [ "$_id.participantFirms","$actTranClientId","$actTranIssuerId","$tranEntities" ] },
            type: "Others"
          }
      },
      ...checkRefEntity
    ])
  }

  if( trantype === "busdevs" ) {
    return ActBusDev.aggregate([
      ...matchQuery,
      {
        $project: {
          _id:"$_id",
          actTranFirmId:1,
          actIssuerClient:1,
          participantFirms:{$setUnion:["$participants.partFirmId",["$actIssuerClient"]]},
          participantUsers:{$setUnion:["$participants.partContactId",["$createdByUser","$actLeadFinAdvClientEntityId"]]},
          tranAttributes:{
            projectDescription:"$actProjectName",
            status:"$actStatus",
            clientId:"$actIssuerClient",
            clientName:"$actIssuerClientEntityName"
          }
        },
      },
      {$unwind:{"path":"$participantUsers", "preserveNullAndEmptyArrays": true}},
      {
        $lookup:
          {
            from: "entityusers",
            localField: "participantUsers",
            foreignField: "_id",
            as: "participantUserDetails"
          }
      },
      {$unwind:{"path":"$participantUserDetails", "preserveNullAndEmptyArrays": true}},
      {
        $project: {
          _id:1,
          actTranClientId:1,
          actTranIssuerId:1,
          participantFirms:1,
          participantUsers:1,
          tranAttributes:1,
          tranEntities:"$participantUserDetails.entityId"
        },
      },
      {
        $group:{
          _id:{tranId:"$_id",actTranClientId:"$actTranClientId",actTranIssuerId:"$actTranIssuerId", tranAttributes:"$tranAttributes",participantFirms:"$participantFirms"},
          actTranClientId:{$addToSet:"$actTranClientId"},
          actTranIssuerId:{$addToSet:"$actTranIssuerId"},
          tranEntities:{$push:"$tranEntities"}
        }
      },
      { $project:
          { _id:0, tranId:"$_id.tranId",tranAttributes:"$_id.tranAttributes", // tranUsers:1,
            allTranEntities:{ $setUnion: [ "$_id.participantFirms","$actTranClientId","$actTranIssuerId","$tranEntities" ] },
            type: "ActBusDev"
          }
      },
      ...checkRefEntity
    ])
  }
  return []
}

export const getTransactionInformationForLoggedInUser = async (user,refEntity) => {

  try {
    const{checkEntitlement, tenantId} = user
    const tranTypes = ["deals","rfps","bankloans","derivatives","marfps","others","busdevs"]

    let mongoTranIds =[]

    if (checkEntitlement) {
      const allEntitledData = await getAllAccessAndEntitlementsForLoggedInUser(user, "consolidated")
      const tranIds = (allEntitledData && allEntitledData.tranEntitlements && Object.keys(allEntitledData.tranEntitlements)) || []
      mongoTranIds = (tranIds || []).map( tranId => ObjectID(tranId) )
    }

    const allDataToRun = await Promise.all((tranTypes || []).map( async (tranType) =>
      transRelatedToEntitiesLookup({
        trantype:tranType,
        tranIds:mongoTranIds || [],
        refEntity:refEntity && refEntity.length ? refEntity.map(r => ObjectID(r)) : [],
        tenantId,
        applyEntitlement:checkEntitlement
      })
    ))

    // const dataToReturn = ((tranTypes || []).reduce((acc, tranType, ind) => ({...acc, [tranType]:allDataToRun[ind]}),{}))
    const dataToReturn = (allDataToRun || []).reduce((acc,t) =>([...acc,...t]),[])
    // console.log("New Console Setup", JSON.stringify(dataToReturn,null,2))
    return {status:"success",message:"Successfully fetched transactions for Logged in User", data:dataToReturn, length: dataToReturn.length}
  } catch (e)
  {
    console.log( "Error while fetch eligible transactions for users", e)
    return {status:"fail",message:"there was an error fetching data for logged in user", data:undefined}
  }
}
