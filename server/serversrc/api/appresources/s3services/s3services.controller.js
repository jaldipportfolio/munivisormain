import AWS from "aws-sdk"
import fileSize from "file-size"
import {
  Deals,
  Docs,
  ActBusDev,
  ActMaRFP,
  BankLoans,
  Derivatives,
  Others,
  RFP
} from "../models"
import {elasticSearchUpdateFunction} from "../../elasticsearch/esHelper"

const async = require("async")
const fs = require("fs")
const {ObjectID} = require("mongodb")

const returnFromES = (tenantId, model, id, type) => elasticSearchUpdateFunction({tenantId, Model: model, _id: id, esType: type})

export const s3 = new AWS.S3({
  region : process.env.SESREGION,
  bucket: process.env.S3BUCKET,
  accessKeyId: process.env.S3ACCESSKEYID,
  secretAccessKey: process.env.S3SECRETKEY
})

export const getAWSS3Object = async (req, res) => {

  const { contextType } = req.body
  const id = req.body.id || req.params.id

  try {

    let totalSize = 0

    const params = {
      Bucket: process.env.S3BUCKET,
      Prefix: `${id}/${contextType || ""}`,
    }

    s3.listObjectVersions(params, (err, data) => {
      if (err) console.log(err, err.stack) // an error occurred
      else {
        console.log({data})
        data && data.Versions && data.Versions.forEach(d => {
          totalSize += d.Size
          d.fileName = d && d.Key && d.Key.split("/")[3]
        })
        res.send({...data, totalSize: fileSize(totalSize).human("jedec")})
      }
    })

  } catch (e) {
    console.log(e)
  }

}

export const getAWSS3ContextType = (req, res) => {

  const { id } = req.params

  try {

    const params = {
      Bucket: process.env.S3BUCKET,
      Prefix: `${id}/`,
      Delimiter: "/"
    }

    s3.listObjects(params, (err, data) => {
      if (err) {
        console.log(err, err.stack)
        res.send({done: false, err, message: err.stack})
      } else {
        console.log({data})
        const contextList = []
        data && data.CommonPrefixes && data.CommonPrefixes.length && data.CommonPrefixes.forEach(obj => {
          const key = obj.Prefix.split("/")[1]
          contextList.push(key)
        })
        res.send({done: true, contextList, })
      }
    })

  } catch (error) {
    console.log({error})
  }

}

export const getAWSS3Documents = async (req, res) => {

  try {

    const docs = await Docs.findOne(req.query)
    res.send(docs)

  } catch (error) {
    console.log({error})
  }
}

export const copyDocuments = async (req, res) => {

  const { copyDetails, user, oldDoc, docs, tranType, authEvaluateId } = req.body

  try {

    const tranTypes = [
      "Deals",
      "RFP",
      "ActMaRFP",
      "Derivatives",
      "BankLoans",
      "Others",
      "ActBusDev"
    ]

    if (tranTypes.indexOf(tranType) !== -1) {

      const key = tranType === "Deals"
        ? "dealIssueDocuments"
        : tranType === "ActMaRFP"
          ? "maRfpDocuments"
          : tranType === "Derivatives"
            ? "derivativeDocuments"
            : tranType === "BankLoans"
              ? "bankLoanDocuments"
              : tranType === "RFP"
                ? "rfpBidDocuments"
                : tranType === "Others"
                  ? "actTranDocuments"
                  : tranType === "ActBusDev"
                    ? "busDevDocuments"
                    : ""

      const model = tranType === "Deals"
        ? Deals
        : tranType === "ActMaRFP"
          ? ActMaRFP
          : tranType === "Derivatives"
            ? Derivatives
            : tranType === "BankLoans"
              ? BankLoans
              : tranType === "RFP"
                ? RFP
                : tranType === "Others"
                  ? Others
                  : tranType === "ActBusDev"
                    ? ActBusDev
                    : ""

      const esType = tranType === "Deals"
        ? "tranagencydeals"
        : tranType === "ActMaRFP"
          ? "actmarfps"
          : tranType === "Derivatives"
            ? "tranderivatives"
            : tranType === "BankLoans"
              ? "tranbankloans"
              : tranType === "RFP"
                ? "tranagencyrfps"
                : tranType === "Others"
                  ? "tranagencyothers"
                  : tranType === "ActBusDev"
                    ? "actbusdevs"
                    : ""

      const {tenantId, contextType, contextId, name} = oldDoc || {}

      let fileName = ""
      const extnIdx = oldDoc.originalName.lastIndexOf(".")
      if (extnIdx > -1) {
        fileName = `${oldDoc.originalName.substr(
          0,
          extnIdx
        )}_${new Date().getTime()}${oldDoc.originalName.substr(extnIdx)}`
      }
      console.log("fileName2 : ", fileName)

      const old = `${tenantId}/${contextType}/${contextType}_${contextId}/${name}`
      const newPre = `${copyDetails && copyDetails.tenantId}/${copyDetails && copyDetails.contextType}/${copyDetails && copyDetails.contextType}_${copyDetails && copyDetails.contextId}/${fileName}`

      const params = {
        Bucket: process.env.S3BUCKET,
        Prefix: old
      }

      await s3.listObjects(params, async (err, data) => {
        console.log("err", err)
        console.log("data", data)
        if (err) {
          return res
            .send({done: false, message: err && err.message})
        }
        if (data && data.Contents && data.Contents.length) {
          async.each(data.Contents, (file, cb) => {

            const params = {
              Bucket: process.env.S3BUCKET,
              CopySource: `${process.env.S3BUCKET}/${file.Key}`,
              Key: file.Key.replace(old, newPre)
            }

            s3.copyObject(params, async (copyErr, copyData) => {
              if (copyErr) {
                console.log(err)
                return res
                  .send({done: false, message: copyErr && copyErr.message})
              }
              delete oldDoc._id
              const doc = {
                ...oldDoc,
                name: fileName,
                contextType: copyDetails && copyDetails.contextType,
                contextId: copyDetails && copyDetails.contextId,
                tenantId: copyDetails && copyDetails.tenantId,
                createdBy: user && user.userName,
                createdDate: new Date(),
                updatedAt: new Date(),
                createdAt: new Date(),
                meta: {
                  ...oldDoc.meta,
                  versions: [
                    {
                      ...oldDoc.meta.versions[0],
                      name: fileName,
                      versionId: copyData && copyData.VersionId,
                      uploadDate: new Date(),
                      uploadedBy: user && user.userName,
                      uploadedByUserId: user && user.userId,
                      uploadedByUserTenantId: copyDetails && copyDetails.tenantId,
                      uploadedByUserEntityId: user && user.entityId
                    }
                  ]
                }
              }

              const documentSave = new Docs(doc)
              const newDocumentAfterInsert = await documentSave.save()
              console.log({doc})

              delete docs._id
              const newDocument = {
                ...docs,
                docFileName: oldDoc && oldDoc.originalName || "",
                LastUpdatedDate: new Date(),
                createdBy: user && user.userId,
                createdUserName: user && user.userName,
                docAWSFileLocation: newDocumentAfterInsert && newDocumentAfterInsert._id,
                markedPublic: {
                  publicFlag: false,
                  publicDate: new Date().toString()
                },
                sentToEmma: {
                  emmaSendFlag: false,
                  emmaSendDate: new Date().toString()
                },
              }

              let documentsList = []
              if(authEvaluateId && tranType === "RFP"){

                await RFP.update({
                  _id: copyDetails.contextId,
                  "rfpMemberEvaluations._id": authEvaluateId
                }, {
                  $push: {
                    "rfpMemberEvaluations.$.rfpEvaluationDocuments": {
                      $each: [newDocument]
                    }
                  }
                })
                const updatedData = await RFP.find({_id: ObjectID(copyDetails.contextId)}).select("rfpMemberEvaluations._id rfpMemberEvaluations.rfpEvaluationDocuments")

                documentsList = (updatedData && updatedData.length && updatedData[0].rfpMemberEvaluations && updatedData[0].rfpMemberEvaluations.length && updatedData[0].rfpMemberEvaluations[0] && updatedData[0].rfpMemberEvaluations[0].rfpEvaluationDocuments) || []

              } else {
                await model.update({
                  _id: copyDetails.contextId
                }, {
                  $addToSet: {
                    [key]: [newDocument]
                  }
                })
                const updatedData = await model.find({_id: ObjectID(copyDetails.contextId)}).select(key)
                documentsList = (updatedData && updatedData.length && updatedData[0] && updatedData[0][key]) || []

              }

              await returnFromES(req.user.tenantId, model, copyDetails.contextId, esType)

              res.json({done: true, documentsList})

              console.log("Copied: ", params.Key)
              cb()

            })
          })
        }
      })

    } else {
      res
        .send({done: false, message: "something went wrong"})
    }

  } catch (e) {
    console.log("===========================", e.message)
    res
      .send({done: false, message: e.message})
  }

}

export const getObject = async (Key) => {
  const params = {
    Bucket: process.env.S3BUCKET,
    Key
  }

  return new Promise(async (res) => {
    await s3.getObject(params, (err, data) => {
      if (err) {
        // console.log(err, err.stack)
        res({done: false, err, message: err.stack})
      } else {
        // console.log({data})
        res({done: true, data})
      }
    })
  })
}

export const getlistObjects = async (Prefix) => {
  const params = {
    Bucket: process.env.S3BUCKET,
    Prefix,
    Delimiter: "/"
  }

  return new Promise(async (res) => {
    await s3.listObjects(params, (err, data) => {
      if (err) {
        // console.log(err, err.stack)
        res({done: false, err, message: err.stack})
      } else {
        // console.log({data})
        res(data)
      }
    })
  })
}

export const getFileName = (key) => {
  const array = key.split("/")
  const index = array.length - 1
  return array[index]
}

export const getDocuments = async (req, res) => {

  const { id } = req.params

  try {

    const json = {
      children: [],
      name: "Folders",
      path: "Folders/",
      toggled: true
    }

    const arrayData = []
    const mainData = await getlistObjects("5cade5927f0e2e2fece0a8f3/ONEDRIVE/")

    if(mainData && mainData.Contents && mainData.Contents.length){
      for (const snap of mainData.Contents) {
        const array = await getObject(snap.Key)
        console.log("================>385", array && array.data && array.data.VersionId)
        const fileName = await getFileName(snap.Key)
        json.children = []
        json.name = "ONEDRIVE"
        json.path = "ONEDRIVE/"
        json.toggled = true
        json.children.push({
          children: [],
          ext: "json",
          id: array && array.data && array.data.VersionId,
          name: fileName,
          path: `ONEDRIVE/${fileName}`,
          size: snap.Size,
          type: "file"
        })
        arrayData.push({Key: snap.Key, Size: snap.Size, VersionId: array && array.data && array.data.VersionId, fileName })
      }
    }
    if(mainData && mainData.CommonPrefixes && mainData.CommonPrefixes.length){
      for (const snap of mainData.CommonPrefixes) {
        const index = snap.Prefix.split("/").length - 2
        json.children.push({
          children: [],
          name: `${snap.Key.split("/")[index]}`,
          path: `ONEDRIVE/${snap.Key.split("/")[index]}`,
          type: "folder"
        })
        const stepOne = await getlistObjects(snap.Prefix)
        if(stepOne && stepOne.Contents && stepOne.Contents.length){
          for (const snap of stepOne.Contents) {
            const array = await getObject(snap.Key)
            console.log("================>396", array && array.data && array.data.VersionId)
            const fileName = await getFileName(snap.Key)
            const length = snap.Key.split("/").length - 2
            const index = json.children.findIndex(s => s.name === (snap.Key.split("/")[length]))
            if(index > -1){
              json.children[index].children.push({
                children: [],
                ext: "json",
                id: array && array.data && array.data.VersionId,
                name: fileName,
                path: `Folders/${snap.Key.split("/")[length]}/${fileName}`,
                size: snap.Size,
                type: "file"
              })
            } else {
              json.children.push({
                children: [{
                  children: [],
                  ext: "json",
                  id: array && array.data && array.data.VersionId,
                  name: fileName,
                  path: `Folders/${snap.Key.split("/")[length]}/${fileName}`,
                  size: snap.Size,
                  type: "file"
                }],
                name: `${snap.Key.split("/")[length]}`,
                path: `Folders/${snap.Key.split("/")[length]}`,
                type: "folder"
              })
            }
            arrayData.push({Key: snap.Key, Size: snap.Size, VersionId: array && array.data && array.data.VersionId, fileName})
          }
        }
        if(stepOne && stepOne.CommonPrefixes && stepOne.CommonPrefixes.length){
          for (const snap of stepOne.CommonPrefixes) {
            const stepTwo = await getlistObjects(snap.Prefix)
            if(stepTwo && stepTwo.Contents && stepTwo.Contents.length){
              for (const snap of stepTwo.Contents) {
                const array = await getObject(snap.Key)
                console.log("================>407", array && array.data && array.data.VersionId)
                const fileName = await getFileName(snap.Key)
                const secondLength = snap.Key.split("/").length - 3
                const thirdLength = snap.Key.split("/").length - 2
                const second = json.children.findIndex(s => s.name === (snap.Key.split("/")[secondLength]))
                const third = second && second > -1 && json.children[secondLength].children.findIndex(s => s.name === (snap.Key.split("/")[thirdLength]))
                console.log("========================>445", snap.Key.split("/"))
                if(second > -1 && third > -1){
                  json.children[second].children[third].children.push({
                    children: [{
                      children: [],
                      ext: "json",
                      id: array && array.data && array.data.VersionId,
                      name: fileName,
                      path: `Folders/${snap.Key.split("/")[secondLength]}/${snap.Key.split("/")[thirdLength]}`,
                      size: snap.Size,
                      type: "file"
                    }],
                    ext: "json",
                    id: array && array.data && array.data.VersionId,
                    name: fileName,
                    path: `Folders/${snap.Key.split("/")[secondLength]}/${snap.Key.split("/")[thirdLength]}/${fileName}`,
                    size: snap.Size,
                    type: "file"
                  })
                } else {
                  json.children.push({
                    children: [{
                      children: [{
                        children: [],
                        ext: "json",
                        id: array && array.data && array.data.VersionId,
                        name: fileName,
                        path: `Folders/${snap.Key.split("/")[secondLength]}/${snap.Key.split("/")[thirdLength]}/${fileName}`,
                        size: snap.Size,
                        type: "file"
                      }],
                      ext: "json",
                      id: array && array.data && array.data.VersionId,
                      name: snap.Key.split("/")[thirdLength],
                      path: `Folders/${snap.Key.split("/")[secondLength]}/${snap.Key.split("/")[thirdLength]}`,
                      size: snap.Size,
                      type: "folder"
                    }],
                    name: `${snap.Key.split("/")[secondLength]}`,
                    path: `Folders/${snap.Key.split("/")[secondLength]}`,
                    type: "folder"
                  })
                }
                arrayData.push({Key: snap.Key, Size: snap.Size, VersionId: array && array.data && array.data.VersionId, fileName})
              }
            }
            if(stepTwo && stepTwo.CommonPrefixes && stepTwo.CommonPrefixes.length){
              for (const snap of stepTwo.CommonPrefixes) {
                const stepThree = await getlistObjects(snap.Prefix)
                if(stepThree && stepThree.Contents && stepThree.Contents.length){
                  for (const snap of stepThree.Contents) {
                    const array = await getObject(snap.Key)
                    console.log("================>418", array && array.data && array.data.VersionId)
                    const fileName = await getFileName(snap.Key)
                    arrayData.push({Key: snap.Key, Size: snap.Size, VersionId: array && array.data && array.data.VersionId, fileName})
                  }
                }
                if(stepThree && stepThree.CommonPrefixes && stepThree.CommonPrefixes.length){
                  for (const snap of stepThree.CommonPrefixes) {
                    const stepFour = await getlistObjects(snap.Prefix)
                    if(stepFour && stepFour.Contents && stepFour.Contents.length){
                      for (const snap of stepFour.Contents) {
                        const array = await getObject(snap.Key)
                        console.log("================>429", array && array.data && array.data.VersionId)
                        const fileName = await getFileName(snap.Key)
                        arrayData.push({Key: snap.Key, Size: snap.Size, VersionId: array && array.data && array.data.VersionId, fileName})
                      }
                    }
                    if(stepFour && stepFour.CommonPrefixes && stepFour.CommonPrefixes.length){
                      for (const snap of stepFour.CommonPrefixes) {
                        const stepFive = await getlistObjects(snap.Prefix)
                        if(stepFive && stepFive.Contents && stepFive.Contents.length){
                          for (const snap of stepFive.Contents) {
                            const array = await getObject(snap.Key)
                            console.log("================>440", array && array.data && array.data.VersionId)
                            const fileName = await getFileName(snap.Key)
                            arrayData.push({Key: snap.Key, Size: snap.Size, VersionId: array && array.data && array.data.VersionId, fileName})
                          }
                        }
                        if(stepFive && stepFive.CommonPrefixes && stepFive.CommonPrefixes.length){
                          for (const snap of stepFive.CommonPrefixes) {
                            const stepFive = await getlistObjects(snap.Prefix)
                            // console.log("================>", {stepOne})
                          }
                        }
                        // console.log("================>", {stepOne})
                      }
                    }
                    // console.log("================>", {stepOne})
                  }
                }
                // console.log("================>", {stepOne})
              }
            }
            // console.log("================>", {stepTwo})
          }
        }

      }
    }
    res.send({done: true, mainData, arrayData, json})

  } catch (error) {
    console.log({error})
  }

}

export const getDocFromPath = async (req, res) => {
  try {
    const {path} = req.query
    const modifiedDrive = []
    const parentDir = `${req.user.tenantId}/ONEDRIVE/`
    const mainData = await getlistObjects(`${parentDir}${path || ""}`)
    if(mainData && mainData.CommonPrefixes){
      mainData.CommonPrefixes.forEach(dir => {
        const pathDir = dir.Prefix.split("/")
        modifiedDrive.push({
          name: pathDir[pathDir.length - 2] || "",
          path: pathDir.splice(2, pathDir.length - 2).join("/"),
          rootPath: dir.Prefix,
          size: "",
          type: "folder"
        })
      })
    }
    if(mainData && mainData.Contents){
      mainData.Contents.forEach(file => {
        const pathDir = file.Key.split("/")
        if(pathDir[pathDir.length - 1]){
          modifiedDrive.push({
            name: pathDir[pathDir.length - 1] || "",
            path: pathDir.splice(2, pathDir.length - 2).join("/"),
            lastModified: file.LastModified,
            rootPath: file.Key,
            size: file.Size,
            type: "file"
          })
        }
      })
    }
    res.send({done: true, mainData, modifiedDrive})
  } catch (error) {
    console.log("===========================", error.message)
    res.send({done: false, message: error.message})
  }
}
