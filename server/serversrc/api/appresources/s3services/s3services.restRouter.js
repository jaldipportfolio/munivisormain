import express from "express"
import {requireAuth} from "../../../authorization/authsessionmanagement/auth.middleware"
import {
  getAWSS3Object,
  getAWSS3ContextType,
  getAWSS3Documents,
  copyDocuments,
  getDocuments,
  getDocFromPath
} from "./s3services.controller"

export const s3ServicesRouter = express.Router()


s3ServicesRouter.route("/aws_context/:id")
  .get(requireAuth, getAWSS3ContextType)
  .post(requireAuth, getAWSS3Object)

s3ServicesRouter.route("/aws_context_docs")
  .get(requireAuth, getAWSS3Documents)

s3ServicesRouter.route("/post_document")
  .post(requireAuth, copyDocuments)
  .get(requireAuth, getDocuments)

s3ServicesRouter.route("/get_docs")
  .get(requireAuth, getDocFromPath)
