import mongoose from "mongoose"
import timestamps from "mongoose-timestamp"

const { Schema } = mongoose

const dataMigrationMappingSchema = new Schema({
  tenantId:Schema.Types.ObjectId,
  userId:Schema.Types.ObjectId,
  userFirstName:String,
  userLastName:String,
  type: String,
  mapObject: Object
})

dataMigrationMappingSchema.plugin(timestamps)

export const DmMapping = mongoose.model("dmmapping", dataMigrationMappingSchema)
