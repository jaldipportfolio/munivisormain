import express from "express"
import dmMappingController from "./dmmapping.controller"
import { requireAuth } from "./../../../authorization/authsessionmanagement/auth.middleware"

export const dmMappingRouter = express.Router()

dmMappingRouter.route("/mappinginfo")
  .get(requireAuth, dmMappingController.getDmMappingObject)
  .post(requireAuth, dmMappingController.postDmMappingObject)


