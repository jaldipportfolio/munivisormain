import { generateControllers } from "../../modules/query"
import { DmMapping } from "./dmmapping.model"

const postDmMappingObject = async (req, res) => {
  try {
    const {user} = req
    const {mapObject} = req.body
    /* const {type} = req.params
     if(!type){
       res.status(422).send({ done: false, message: "please pass data migration information type" })
     } */
    let mappingObject = await DmMapping.findOne({ userId: user._id /* , type */ }).select("mapObject")
    if(mappingObject){
      await DmMapping.update({ userId: user._id }, req.body)
      mappingObject = await DmMapping.findOne({ userId: user._id }).select("mapObject")
    }else {
      const newDmMapping = new DmMapping({
        tenantId: user.entityId,
        userId: user._id,
        userFirstName: user.userFirstName,
        userLastName:user.userLastName,
        // type,
        mapObject: mapObject || {}
      })
      mappingObject = await newDmMapping.save()
    }
    res.json({done: true, message: "Successfully update mapping object", data: (mappingObject && mappingObject.mapObject) || {} })
  }catch (e) {
    console.log(e)
    res.status(422).send({ done: false, message: "Something went wrong" })
  }
}

const getDmMappingObject = async (req, res) => {
  try {
    const {user} = req
    /* const {type} = req.params
    if(!type){
      res.status(422).send({ done: false, message: "please pass data migration information type" })
    } */

    const mappingObject = await DmMapping.findOne({ userId: user._id /* , type */ }).select("mapObject")
    res.json((mappingObject && mappingObject.mapObject) || {})
  }catch (e) {
    console.log(e)
    res.status(422).send({ done: false, message: "Something went wrong" })
  }
}

export default generateControllers(DmMapping, {
  getDmMappingObject,
  postDmMappingObject,
})
