import pdfMake from "pdfmake/build/pdfmake.min"
import pdfFonts from "pdfmake/build/vfs_fonts"
import moment from "moment"

const PDFMakePrinter = require("pdfmake/src/printer")
const path = require("path")

pdfMake.vfs = pdfFonts.pdfMake.vfs

const fontDescriptors = {
  Roboto: {
    normal: path.join(__dirname, "/fonts/Roboto-Regular.ttf"),
    bold: path.join(__dirname,  "/fonts/Roboto-Medium.ttf"),
    italics: path.join(__dirname, "/fonts/Roboto-Italic.ttf"),
    bolditalics: path.join(__dirname, "/fonts/Roboto-MediumItalic.ttf")
  }
}

const invoiceStyles = {
  header: {
    fontSize: 14,
    bold: true,
    margin: [0, 10, 0, 20],
    alignment: "center",
    color: "blue"
  },
  subheader: {
    fontSize: 10,
    margin: [2, 2, 0, 0]
  },
  tableExample: {
    fontSize: 12,
    margin: [0, 10, 0, 5]
  },
  tableHeader: {
    bold: true,
    fontSize: 12
  },
}

const styles = {
  header: {
    fontSize: 18,
    bold: true,
    margin: [0, 15, 0, 20]
  },
  subheader: {
    fontSize: 12,
    bold: true,
    margin: [0, 50, 0, 10]
  },
  tableExample: {
    margin: [0, 10, 0, 15]
  },
  tableHeader: {
    bold: true,
    fontSize: 13,
    color: "black"
  },
  imageHeader: {
    bold: true,
    fontSize: 13,
    color: "black",
    alignment: "center",
  }
}

const docDefinition = {
  content: ["This will show up in the file created"]
}

const generatePdf = (docDefinition, callback) => {
  try {
    const printer = new PDFMakePrinter(fontDescriptors)
    const doc = printer.createPdfKitDocument(docDefinition)
    const chunks = []
    doc.on("data", (chunk) => {
      chunks.push(chunk)
    })
    doc.on("end", () => {
      callback(Buffer.concat(chunks))
    })
    doc.end()
  }
  catch(err) {
    throw(err)
  }
}

export const generatePDFDoc = (req, res) => {
  console.log("Entered the Service - New One")

  let fileName = "testfile"
  fileName = `${encodeURIComponent(fileName)}.pdf`
  console.log("I entered the PDF service", fileName)

  generatePdf(docDefinition, (response) => {
    res.setHeader("Content-disposition", `attachment; filename="${fileName}"`)
    res.setHeader("Content-Type", "application/pdf")
    console.log("Before the Response")
    res.send(response) // Buffer data
  })

}

export const generateInvoicePDF = ( req, res ) => {
  try {
    const {invoiceData, fileName, contentData, location, align, logo} = req.body
    const consultingCharges = []
    const pocketExpenses = []
    let totalOutOfPocketExpenses = 0
    let totalConsultingCharge = 0

    if(invoiceData.consultingCharge){
      Object.keys(invoiceData.consultingCharge).forEach(key => {
        consultingCharges.push([key, {text: `$${parseInt(invoiceData.consultingCharge[key] || 0, 10).toLocaleString()}`,  alignment: "right"}])
      })

      Object.keys(invoiceData.consultingCharge).forEach(key => {
        totalConsultingCharge += parseInt(invoiceData.consultingCharge[key] || 0, 10)
      })
    }

    if(invoiceData.outOfPocketExpenses){
      Object.keys(invoiceData.outOfPocketExpenses).forEach(key => {
        pocketExpenses.push([ key, {text: `$${parseInt(invoiceData.outOfPocketExpenses[key] || 0, 10).toLocaleString()}`,  alignment: "right"} ])
      })

      Object.keys(invoiceData.outOfPocketExpenses).forEach(key => {
        totalOutOfPocketExpenses += parseInt(invoiceData.outOfPocketExpenses[key] || 0, 10)
      })
    }

    if(consultingCharges.length ){consultingCharges.push([{text: "Total", bold: true}, {text: `$${totalConsultingCharge.toLocaleString()}`,  alignment: "right", bold: true}])}
    if(pocketExpenses.length){pocketExpenses.push([{text: "Total Out-of-pocket or Additional Expenses", bold: true}, {text: `$${totalOutOfPocketExpenses.toLocaleString()}`,  alignment: "right", bold: true}])}

    const consultingObj = consultingCharges.length ? [
      { text: "Consulting Charges", bold: true, color: "black", alignment: "left", margin: [0, 20, 0, 0] },
      {
        style: "tableExample",
        table: {
          widths: ["50%","50%"],
          body: consultingCharges
        }
      },
    ] : []
    const pocketObj = pocketExpenses.length ? [
      { text: "Out of Pocket Expenses", bold: true, color: "black", alignment: "left", margin: [0, 20, 0, 0] },
      {
        style: "tableExample",
        table: {
          widths: ["50%", "50%"],
          body: pocketExpenses
        }
      },
    ] : []

    const contentInfo = [
      contentData,
      { text: invoiceData.activityTranFirmName || "", style: "header" },
      { text: `Date: ${moment(new Date()).format("MM-DD-YYYY")}`, alignment: "right" },
      {
        columns: [
          {
            stack: [
              "Client Name",
              { text: invoiceData.activityClientName || "", style: "subheader", bold: false},
            ],
            style: "tableHeader"
          },
          {
            stack: [
              "Title or Client Company Name",
              { text: invoiceData.activityDescription || "", style: "subheader", bold: false},
            ],
            style: "tableHeader"
          },
          {
            stack: [
              "Address",
              { text: (invoiceData && invoiceData.activityClientAddress && invoiceData.activityClientAddress.formatted_address) || "", style: "subheader", bold: false},
            ],
            style: "tableHeader"
          },
        ]
      },
      {
        style: "tableExample",
        table: {
          widths: ["auto", "auto"],
          margin: [0, 20, 0, 20],
          body: [
            [{text: "Title", style: "tableHeader"}, {text: "Revised Expenses / Revenue", style: "tableHeader"}],
            [
              {
                text: [
                  "For Financial Advisory services provided to ",
                  {text: `${invoiceData.activityClientName || ""}, for their $${parseInt(invoiceData.activityParAmount || 0, 10).toLocaleString()} ${invoiceData.activityTranType || ""} / ${invoiceData.activityTranSubType || ""} ${invoiceData.activityDescription || ""}`, color: "red"},
                ]
              },
              {text: `$${parseInt(invoiceData.totalFinancialAdvisoryFees || 0, 10).toLocaleString()}`,  alignment: "right"}
            ],
            /* [
            {
              text: [
                "For services related to the ",
                {text: invoiceData.activityDescription || "", color: "red"},
              ]
            },
            {text: `$${parseInt(invoiceData.totalConsultingEngFees || 0, 10).toLocaleString()}`,  alignment: "right"}
          ],
          [{text: "Total", bold: true}, { text: `$${(parseInt(invoiceData.totalFinancialAdvisoryFees || 0, 10) + parseInt(invoiceData.totalConsultingEngFees || 0, 10)).toLocaleString()}`,
            alignment: "right", bold: true}] */
            [{text: "Total", bold: true}, { text: `$${(parseInt(invoiceData.totalFinancialAdvisoryFees || 0, 10)).toLocaleString()}`, alignment: "right", bold: true}]
          ]
        }
      },
      ...consultingObj,
      ...pocketObj,
      {
        columns: [
          {
            stack: [
              { text: "Grand Total", margin: [0, 20, 0, 2]},
            ],
            style: "tableHeader",
          },
          {
            stack: [
              {text: `$${parseInt((invoiceData && invoiceData.totalOverallExpenses) || 0, 10).toLocaleString()}`, margin: [0, 20, 0, 2], alignment: "right", bold: true}
            ]
          }
        ]
      },
      {
        columns: [
          {
            stack: [
              { text: "Comments / Notes", style: "subheader", margin: [0, 20, 0, 0],},
              { text: (invoiceData && invoiceData.invoiceNotes) || "- - -", style: "subheader", bold: false,  },
            ],
            style: "tableHeader"
          },
        ]
      },
      { text: "Bank Details", bold: true, color: "black", alignment: "center", margin: [0, 25, 0, 0] },
      {
        columns: [
          {
            stack: [
              { text: "Bank Name", style: "subheader", margin: [0, 20, 0, 0]},
              { text: (invoiceData && invoiceData.bankDetails && invoiceData.bankDetails.bankName) || "", style: "subheader", bold: false},
            ],
            style: "tableHeader"
          },
          {
            stack: [
              { text: "ABA#", style: "subheader", margin: [0, 20, 0, 0]},
              { text: (invoiceData && invoiceData.bankDetails && invoiceData.bankDetails.ABA) || "", style: "subheader", bold: false},
            ],
            style: "tableHeader"
          },
          {
            stack: [
              { text: "Address", style: "subheader", margin: [0, 20, 0, 0]},
              { text: `${(invoiceData && invoiceData.bankDetails && invoiceData.bankDetails.addressLine1) || ""},
            ${(invoiceData.bankDetails && invoiceData.bankDetails.addressLine2) || ""}`, style: "subheader", bold: false},
            ],
            style: "tableHeader"
          },
        ],
      },
      {
        columns: [
          {
            stack: [
              { text: "For the account of", style: "subheader", margin: [0, 20, 0, 0]},
              { text: (invoiceData && invoiceData.bankDetails && invoiceData.bankDetails.accountOf) || "", style: "subheader", bold: false},
            ],
            style: "tableHeader"
          },
          {
            stack: [
              { text: "Account No.", style: "subheader", margin: [0, 20, 0, 0]},
              { text: (invoiceData && invoiceData.bankDetails && invoiceData.bankDetails.accountNo) || "", style: "subheader", bold: false},
            ],
            style: "tableHeader"
          },
          {
            stack: [
              { text: "Bank Contact Name / Telephone", style: "subheader", margin: [0, 20, 0, 0]},
              { text: `${(invoiceData && invoiceData.bankDetails && invoiceData.bankDetails.attention) || ""} / ${(invoiceData && invoiceData.bankDetails && invoiceData.bankDetails.telephone) || ""}`, style: "subheader", bold: false},
            ],
            style: "tableHeader"
          },
        ],
      }
    ]

    let header = {}
    let footer = {}

    if(location === "header" && logo ) {
      header = {
        columns: [
          {
            image: logo,
            fit: [100, 40],
            alignment: align || "center",
            margin: [15, 7, 0, 20 ],
          },
        ]
      }
    }
    if(location === "footer" && logo) {
      footer = {
        columns: [
          {
            image: logo,
            fit: [100, 40],
            alignment: align || "center",
            margin: [10, 0, 0, 10 ],
          }
        ]
      }
    }

    const docDefinition = {pageMargins: [ 20, 45, 20, 40 ], header, content: contentInfo, footer, styles: invoiceStyles }
    generatePdf(docDefinition, (response) => {
      res.setHeader("Content-disposition", `attachment; filename="${fileName}"`)
      res.setHeader("Content-Type", "application/pdf")
      console.log("Before the Response")
      res.send(response)
    }, (error) => {
      res.status(500).send(`ERROR:${  error}`)
    })
  }
  catch(err) {
    throw(err)
  }
}

export const generateJSONToPDF = ( req, res ) => {
  try {
    const { contentData, location, align, data, fileName, logo} = req.body

    data.forEach((e) => {
      const content = []
      const { titles, description, headers, rows, url} = e

      const headersData = headers.map(h => ({ text: h, style: styles.tableHeader }))
      const widths = []
      if(titles && titles.length && titles[0] === "Summary"){
        const newRows = []
        rows.forEach(rd => {
          const rowData = {}
          rd.forEach((rdd, j) => {
            rowData[headers[j]] = rdd
          })
          newRows.push((rowData))
        })
        const data = []
        newRows.forEach(o => {
          Object.keys(o).forEach(key => { data.push([key,o[key]]) })
        })
        content.push({
          /* ul: data */
          style: styles.tableExample,
          table: {
            body: data
          }
        })
      }else if(headers.length > 5){
        headers.forEach(h => widths.push("10%"))
        const newRows = []
        rows.forEach(rd => {
          const rowData = {}
          rd.forEach((rdd, j) => {
            rowData[headers[j]] = rdd
          })
          newRows.push((rowData))
        })

        const data = []
        newRows.forEach(o => {
          let text =  "" // `${e.titles[0]} ${j+1}`
          Object.keys(o).forEach(key => { text += `${key}:  ${o[key]}\n` })
          data.push(text)
        })
        // console.log(data)
        if(data.length%2 === 1){
          data.push("")
        }
        const temp = data.slice()
        const coupleOfArr = []
        while (temp.length) {
          coupleOfArr.push(temp.splice(0,2))
        }

        // console.log("===================>", coupleOfArr)
        content.push({
          style: styles.tableExample,

          /* markerColor: "blue", */
          /* ol: data, */
          /* columns: [
            {
              ul: data.slice(0,parseInt(data.length/2,10))
            },
            {
              ul: data.slice(parseInt(data.length/2,10),data.length)
            }
          ] */
          table: {
            widths: ["50%","50%"],
            body: coupleOfArr
          }
        })
      }else {
        headers.forEach(h => widths.push("20%"))
        content.push({
          style: styles.tableExample,
          table: {
            widths,
            headerRows: 1,
            // dontBreakRows: true,
            // keepWithHeaderRows: 1,
            body: [
              headersData,
              ...rows
            ]
          }
        })
      }

      let titlesData = []
      let descData = []
      if(titles && titles.length) {
        titlesData = titles.map(t => ({ text: t, style: styles.subheader }))
      }
      if(description) {
        descData = [description]
      }

      contentData.push(...[ ...titlesData, ...descData, ...content, "\n\n" ])
    })

    let header = {}
    let footer = {}

    if(location === "header" && logo ) {
      header = {
        columns: [
          {
            image: logo,
            fit: [100, 40],
            alignment: align || "center",
            margin: [15, 7, 0, 20 ],
          },
        ]
      }
    }
    if(location === "footer" && logo) {
      footer = {
        columns: [
          {
            image: logo,
            fit: [100, 40],
            alignment: align || "center",
            margin: [10, 0, 0, 10 ],
          }
        ]
      }
    }
    const docDefinition = {
      pageMargins: [ 20, 45, 20, 40 ],
      header,
      content: contentData,
      footer
    }
    generatePdf(docDefinition, (response) => {
      res.setHeader("Content-disposition", `attachment; filename="${fileName}"`)
      res.setHeader("Content-Type", "application/pdf")
      console.log("Before the Response")
      res.send(response)
    }, (error) => {
      res.status(500).send(`ERROR:${  error}`)
    })
  } catch (err) {
    return res.status(422).send({ error: "Error in getting details", err })
  }
}

export const generateG10PDF = ( req, res ) => {
  try {
    const { contentData, location, align, entityName, logo} = req.body

    let header = {}
    let footer = {}

    if(location === "header") {
      header = {
        columns: [
          {
            fit: [100, 40],
            fontSize: 20,
            alignment: align || "center",
            margin: [15, 7, 0, 20 ],
          },
        ]
      }
      if(logo){
        header.columns[0].image = logo
      } else {
        header.columns[0].text = entityName
      }
    }
    if(location === "footer") {
      footer = {
        columns: [
          {
            fit: [100, 40],
            alignment: align || "center",
            margin: [10, 0, 0, 10 ],
          }
        ]
      }
      if(logo){
        footer.columns[0].image = logo
      } else {
        footer.columns[0].text = entityName
      }
    }
    const docDefinition = {
      styles: {
        marker: {
          background: "yellow"
        },
        "html-s": {
          decoration: "lineThrough",
        },
        "html-samp": {
          italic: true
        },
        "html-del": {
          decoration: "lineThrough",
        },
        "html-ins": {
          decoration: "underline",
        },
        "html-cite": {
          italic: true
        },
      },
      pageMargins: [ 20, 45, 20, 40 ],
      header,
      content: contentData,
      footer
    }
    generatePdf(docDefinition, (response) => {
      res.setHeader("Content-disposition", `attachment; filename="${"filename"}"`)
      res.setHeader("Content-Type", "application/pdf")
      console.log("Before the Response")
      res.send(response)
    }, (error) => {
      res.status(500).send(`ERROR:${  error}`)
    })
  } catch (err) {
    return res.status(422).send({ error: "Error in getting details", err })
  }
}

export const generateDistributionPDF = ( req, res ) => {
  try {
    const { logo, clientName, transactionType, data, fileName, firmName } = req.body

    let firmNameLogo = ""
    if(logo){
      firmNameLogo = {image: logo, fit: [140, 60], alignment: "right", margin: [30, 20, 0, 20]}
    } else {
      firmNameLogo = {text: firmName || "", alignment: "right", fontSize: 16, bold: true, margin: [30, 20, 30, 10]}
    }

    const top = 100
    const docDefinition = {
      watermark: { text: firmName, color: "gray", opacity: 0.2 },
      pageMargins: [40,top,40,40],
      pageSize: "A4",
      header:[
        {
          columns: [
            {
              stack: [
                { text: clientName, style: "subheader", bold: true,marginTop:30 ,marginLeft:35},
                { text: `${transactionType}, Series 2018`, bold: true, marginLeft:35},
                { text: "Distribution List", style: "subheader", bold: true, marginLeft:35},
                { text: moment(new Date()).format("MM-DD-YYYY") , style: "subheader", bold: true,marginLeft:35},
              ],
            },
            firmNameLogo
          ]
        },
        {
          canvas: [
            { type: "line",x1: 33, y1: 10, x2: 595-32, y2: 10, lineWidth: 1 }
          ]
        }
      ],

      content: [data],

      styles: {
        subheader: {
          fontSize: 12,
          bold: true,
        },
      }
    }
    generatePdf(docDefinition, (response) => {
      res.setHeader("Content-disposition", `attachment; filename="${fileName}"`)
      res.setHeader("Content-Type", "application/pdf")
      console.log("Before the Response")
      res.send(response)
    }, (error) => {
      res.status(500).send(`ERROR:${  error}`)
    })
  } catch (err) {
    return res.status(422).send({ error: "Error in getting details", err })
  }
}

export const politicalG37SummaryPDF = ( req, res ) => {
  try {
    const { name, year, quarter, firstTable, secondTable, thirdTable, firmDetails, fourthTable, fifthTable,  logo, contentData, location, align} = req.body
    console.log({name, year, quarter, firstTable, secondTable, thirdTable, firmDetails})
    let header = {}
    let footer = {}

    if(location === "header" && logo ) {
      header = {
        columns: [
          {
            image: logo,
            fit: [100, 40],
            alignment: align || "center",
            margin: [15, 7, 0, 20 ],
          },
        ]
      }
    }
    if(location === "footer" && logo) {
      footer = {
        columns: [
          {
            image: logo,
            fit: [100, 40],
            alignment: align || "center",
            margin: [10, 0, 0, 10 ],
          }
        ]
      }
    }
    const docDefinition = {

      pageMargins: [40,60,40,40],
      pageSize: "A4",
      header,
      footer,
      content: [ [
        contentData,
        {
          columns: [
            { text: "FORM G-37", "fontSize": 18, style: "subheader", bold: true },
            [
              { text: "MSRB", "fontSize": 18, style: "subheader", bold: true, alignment: "right" }
            ]
          ]
        },

        {
          columns: [
            {
              stack: [
                { text: `Name of dealer : ${firmDetails && firmDetails.firmName || ""}`, bold: true,  margin: [0, 20, 0, 0] },
                {
                  canvas: [
                    {
                      type: "line",
                      x1: 90,
                      y1: 2,
                      x2: 520,
                      y2: 2,
                      lineWidth: 1,
                    }
                  ]
                }
              ]
            },
          ]
        },
        {
          columns: [
            {
              stack: [
                { text: `Report period :    Year ${year} | Quarter ${quarter}`, bold: true,  margin: [0, 10, 0, 0] },
                {
                  canvas: [
                    {
                      type: "line",
                      x1: 90,
                      y1: 2,
                      x2: 520,
                      y2: 2,
                      lineWidth: 1,
                    }
                  ]
                }
              ]
            },
          ]
        },

        {
          text: "I. CONTRIBUTIONS made to issuer officials (list by state)",
          fontSize: 12,
          bold: true,
          marginTop: 25,
          marginBottom: 10,
        },
        {
          style: "tableExample",
          fontSize: 10,
          table: {
            widths: ["*", "*", "*"],
            headerRows: 1,
            body: firstTable
          }
        },
        {
          text: "II. PAYMENTS made to political parties of states or political subdivisions (list by state)",
          fontSize: 12,
          bold: true,
          marginTop: 25,
          marginBottom: 10,
        },
        {
          style: "tableExample",
          fontSize: 10,
          table: {
            widths: ["*", "*", "*"],
            headerRows: 1,
            body: secondTable
          }
        },
        {
          text: "III. CONTRIBUTIONS made to bond ballot campaigns (list by state)",
          fontSize: 12,
          bold: true,
          marginTop: 25,
          marginBottom: 10,
        },
        {
          text: "A. Contributions",
          fontSize: 10,
          bold: true,
          margin: [10, 0, 0, 10]
        },
        {
          style: "tableExample",
          margin: [10, 0],
          fontSize: 10,
          table: {
            widths: ["*", "*", "*"],
            headerRows: 1,
            body: thirdTable
          }
        },
        {
          text: "B. Reimbursement for Contributions",
          fontSize: 10,
          bold: true,
          margin: [10, 10, 0, 10]
        },
        {
          text: "List below any payments or reimbursements, related to any disclosed bond ballot contribution, received by each broker, dealer or municipal securities dealer, municipal finance professional or nonMFP executive officer from any third party, including the amount paid and the name of the third party making such payments or reimbursements.",
          fontSize: 10,
          margin: [10, 0, 0, 10]
        },
        {
          text: "IV. ISSUERS with which dealer has engaged in municipal securities business (list by state)",
          fontSize: 12,
          bold: true,
          marginTop: 25,
          marginBottom: 10,
        },
        {
          text: "A. Municipal Securities Business",
          fontSize: 10,
          bold: true,
          margin: [10, 0, 0, 10]
        },
        {
          style: "tableExample",
          margin: [10, 0],
          fontSize: 10,
          table: {
            widths: ["*", "*", "*"],
            headerRows: 1,
            body: fourthTable
          }
        },
        {
          text: "B. Ballot-Approved Offerings",
          fontSize: 10,
          bold: true,
          margin: [10, 10, 0, 10]
        },
        {
          text: "Full issuer name and full issue description of any primary offering resulting from the bond ballot campaign to which each contributor category (i.e., dealer, dealer controlled PAC, municipal finance professional controlled PAC, municipal finance professionals and non-MFP executive officers) has made a contribution and the reportable date of selection on which the broker, dealer or municipal securities dealer was selected to engage in such municipal securities business.",
          fontSize: 10,
          margin: [10, 10, 23, 10]
        },
        {
          style: "tableExample",
          margin: [10, 10, 30, 20],
          fontSize: 10,
          table: {
            widths: ["*", "*", "*"],
            headerRows: 1,
            body: fifthTable
          }
        },

        {
          columns: [
            {
              stack: [
                { text: "Signature :  ", bold: true },
                {
                  canvas: [
                    {
                      type: "line",
                      x1: 60,
                      y1: 2,
                      x2: 270,
                      y2: 2,
                      lineWidth: 1
                    }
                  ]
                }
              ]
            },
            {
              stack: [
                { text: `Date : ${moment(new Date()).format("MM-DD-YYYY")}`, bold: true },
                {
                  canvas: [
                    {
                      type: "line",
                      x1: 30,
                      y1: 2,
                      x2: 250,
                      y2: 2,
                      lineWidth: 1
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "text": "(must be officer of dealer)",
          "fontSize": 9,
          "bold": true,
          "margin": [90, 0, 0, 0],
        },

        {
          columns: [
            {
              stack: [
                { text: `Name :       ${firmDetails && firmDetails.supervisorUserName || ""}`, bold: true, "margin": [0, 10, 0, 0] },
                {
                  canvas: [
                    {
                      type: "line",
                      x1: 60,
                      y1: 2,
                      x2: 520,
                      y2: 2,
                      lineWidth: 1
                    }
                  ]
                }
              ]
            },
          ]
        },
        {
          columns: [
            {
              stack: [
                { text: `Address :   ${firmDetails && firmDetails.addressLine1 || ""}`, bold: true, "margin": [0, 10, 0, 0] },
                {
                  canvas: [
                    {
                      type: "line",
                      x1: 60,
                      y1: 2,
                      x2: 520,
                      y2: 2,
                      lineWidth: 1
                    }
                  ]
                }
              ]
            },
          ]
        },
        {
          columns: [
            {
              stack: [
                firmDetails && firmDetails.addressLine2 ?
                  { text: firmDetails && firmDetails.addressLine2 || "", margin: [60, 10, 0, 0], bold: true } :
                  { text: firmDetails && firmDetails.addressLine2 || "", margin: [60, 20, 0, 0], bold: true },
                {
                  canvas: [
                    {
                      type: "line",
                      x1: 60,
                      y1: 2,
                      x2: 520,
                      y2: 2,
                      lineWidth: 1
                    }
                  ]
                }
              ]
            },
          ]
        },
        {
          columns: [
            {
              stack: [
                { text: `Phone :      ${firmDetails && firmDetails.phone || ""}`, bold: true,  margin: [0, 10, 0, 0] },
                {
                  canvas: [
                    {
                      type: "line",
                      x1: 60,
                      y1: 2,
                      x2: 520,
                      y2: 2,
                      lineWidth: 1,
                    }
                  ]
                }
              ]
            },
          ]
        },

        {
          text: "Submit two completed forms quarterly by due date (specified by the MSRB) to:",
          fontSize: 10,
          bold: true,
          marginTop: 25,
          marginBottom: 10,
        },
        {
          text: "Municipal Securities Rulemaking Board",
          fontSize: 11,
          bold: true,
        },
        {
          text: "1300 I Street NW",
          fontSize: 11,
        },
        {
          text: "Suite 1000",
          fontSize: 11,
        },
        {
          text: "Washington, DC 20005-3314",
          fontSize: 11,
        },
      ] ],

      styles: {
        "html-div": {
          background: "#eeeeee",
          padding: [5, 10],
          border: "2px solid #cccccc",
        },
        "html-s":  {
          content: "openQuote"
        },
        marker: {
          background: "yellow"
        },
        subheader: {
          fontSize: 12,
          bold: true,
        },
        tableHeader: {
          bold: true,
          fontSize: 11,
        }
      }
    }

    generatePdf(docDefinition, (response) => {
      res.setHeader("Content-disposition", `attachment; filename="${year}/${quarter}"`)
      res.setHeader("Content-Type", "application/pdf")
      console.log("Before the Response")
      res.send(response)
    }, (error) => {
      res.status(500).send(`ERROR:${  error}`)
    })
  } catch (err) {
    return res.status(422).send({ error: "Error in getting details", err })
  }
}
