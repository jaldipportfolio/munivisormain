module.exports = {
  "properties": {
    "dealIssueTranClientId": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "dealIssueTranIssuerId": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "dealIssueTranClientMSRBType": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "dealIssueTranIssuerFirmName": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "dealIssueTranIssuerMSRBType": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "dealIssueBorrowerName": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "dealIssueGuarantorName": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "dealIssueTranName": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "dealIssueTranType": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "dealIssueTranPurposeOfRequest": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "dealIssueTranAssignedTo": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "dealIssueTranRelatedTo": {
      "properties": {
        "id": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "type": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        }
      }
    },
    "dealIssueTranState": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "dealIssueTranCounty": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "dealIssueTranPrimarySector": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "dealIssueTranSecondarySector": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "dealIssueTranDateHired": {
      "type": "date"
    },
    "dealIssueTranStartDate": {
      "type": "date"
    },
    "dealIssueTranExpectedEndDate": {
      "type": "date"
    },
    "dealIssueTranStatus": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "dealIssueTransNotes": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "dealIssueofferingType": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "dealIssueSecurityType": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "dealIssueBankQualified": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "dealIssueCorpType": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "dealIssueParAmount": {
      "type": "float"
    },
    "dealIssuePricingDate": {
      "type": "date"
    },
    "dealIssueExpAwardDate": {
      "type": "date"
    },
    "dealIssueActAwardDate": {
      "type": "date"
    },
    "dealIssueSdlcCreditPerc": {
      "type": "float"
    },
    "dealIssueEstimatedRev": {
      "type": "float"
    },
    "dealIssueUseOfProceeds": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "dealIssueUnderwriters": {
      "properties": {
        "dealPartFirmId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "dealPartType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "dealPartFirmName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "roleInSyndicate": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "liabilityPerc": {
          "type": "float"
        },
        "managementFeePerc": {
          "type": "float"
        },
        "dealPartContactAddToDL": {
          "type": "boolean"
        }
      }
    },
    "dealIssueParticipants": {
      "properties": {
        "dealPartFirmId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "dealPartType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "dealPartFirmName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "dealPartContactId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "dealPartContactName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "dealPartContactEmail": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "dealPartContactAddrLine1": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "dealPartContactAddrLine2": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "dealParticipantState": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "dealPartContactPhone": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "dealPartContactAddToDL": {
          "type": "boolean"
        }
      }
    },
    "dealIssueSeriesDetails": {
      "properties": {
        "contactId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "seriesName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "description": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "tag": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "seriesRatings": {
          "properties": {
            "seriesId": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "ratingAgencyName": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "longTermRating": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "longTermOutlook": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "shortTermOutlook": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "shortTermRating": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            }
          }
        },
        "cepRatings": {
          "properties": {
            "cepName": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "seriesId": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "cepType": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "longTermRating": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "longTermOutlook": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "shortTermOutlook": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "shortTermRating": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            }
          }
        },
        "seriesPricingDetails": {
          "properties": {
            "dealSeriesPrincipal": {
              "type": "float"
            },
            "dealSeriesSecurityType": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "dealSeriesSecurity": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "dealSeriesSecurityDetails": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "dealSeriesDatedDate": {
              "type": "date"
            },
            "dealSeriesSettlementDate": {
              "type": "date"
            },
            "dealSeriesFirstCoupon": {
              "type": "date"
            },
            "dealSeriesPutDate": {
              "type": "date"
            },
            "dealSeriesRecordDate": {
              "type": "date"
            },
            "dealSeriesCallDate": {
              "type": "date"
            },
            "dealSeriesFedTax": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "dealSeriesStateTax": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "dealSeriesPricingAMT": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "dealSeriesBankQualified": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "dealSeriesAccrueFrom": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "dealSeriesPricingForm": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "dealSeriesCouponFrequency": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "dealSeriesDayCount": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "dealSeriesRateType": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "dealSeriesCallFeature": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "dealSeriesCallPrice": {
              "type": "float"
            },
            "dealSeriesInsurance": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "dealSeriesUnderwtiersInventory": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "dealSeriesMinDenomination": {
              "type": "float"
            },
            "dealSeriesSyndicateStructure": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "dealSeriesGrossSpread": {
              "type": "float"
            },
            "dealSeriesEstimatedTakeDown": {
              "type": "float"
            },
            "dealSeriesInsuranceFee": {
              "type": "float"
            }
          }
        },
        "seriesPricingData": {
          "properties": {
            "term": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "maturityDate": {
              "type": "date"
            },
            "amount": {
              "type": "float"
            },
            "coupon": {
              "type": "float"
            },
            "yield": {
              "type": "float"
            },
            "price": {
              "type": "float"
            },
            "YTM": {
              "type": "float"
            },
            "cusip": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "callDate": {
              "type": "date"
            },
            "takeDown": {
              "type": "float"
            },
            "insured": {
              "type": "boolean"
            },
            "drop": {
              "type": "boolean"
            }
          }
        }
      }
    },
    "dealIssueDocuments": {
      "properties": {
        "docCategory": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "docSubCategory": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "docType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "docAWSFileLocation": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "docFileName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "markedPublic": {
          "properties": {
            "publicFlag": {
              "type": "boolean"
            },
            "publicDate": {
              "type": "date"
            }
          }
        },
        "sentToEmma": {
          "properties": {
            "emmaSendFlag": {
              "type": "boolean"
            },
            "emmaSendDate": {
              "type": "date"
            }
          }
        },
        "LastUpdatedDate": {
          "type": "date"
        }
      }
    },
    "dealIssueCostOfIssuanceNotes": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "dealIssuePlaceholder": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "dealIssueTradingInformation": {
      "properties": {
        "contractID": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "contractStatus": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "portfolio": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "portfolioGroup": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "counterParty": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "securityGroup": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "securityType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "brokerOrDealer": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "tradeDate": {
          "type": "date"
        },
        "settlementDays": {
          "type": "float"
        },
        "settlementDate": {
          "type": "date"
        }
      }
    },
    "dealIssueContractInformation": {
      "properties": {
        "standardContract": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "receive": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "currency": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "notional": {
          "type": "float"
        },
        "fixedRate": {
          "type": "float"
        },
        "referenceRate": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "spread": {
          "type": "float"
        },
        "initialRate": {
          "type": "float"
        },
        "effectiveDate": {
          "type": "date"
        },
        "tenor": {
          "type": "float"
        },
        "tenorType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        }
      }
    },
    "dealIssueReceiveLeg": {
      "properties": {
        "currency": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "notional": {
          "type": "float"
        },
        "interest": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "fixedRate": {
          "type": "float"
        },
        "referenceRate": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "spread": {
          "type": "float"
        },
        "frequency": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "initialRate": {
          "type": "float"
        },
        "effectiveDate": {
          "type": "date"
        },
        "maturityDate": {
          "type": "date"
        },
        "dayCount": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "businessConvention": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "bankHolidays": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "endOfMonth": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "securityGroup": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "securityType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        }
      }
    },
    "dealIssuePayLeg": {
      "properties": {
        "currency": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "notional": {
          "type": "float"
        },
        "interest": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "fixedRate": {
          "type": "float"
        },
        "referenceRate": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "spread": {
          "type": "float"
        },
        "frequency": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "initialRate": {
          "type": "float"
        },
        "effectiveDate": {
          "type": "date"
        },
        "maturityDate": {
          "type": "date"
        },
        "dayCount": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "businessConvention": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "bankHolidays": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "endOfMonth": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "securityGroup": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "securityType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        }
      }
    },
    "dealIssueCompliance": {
      "properties": {
        "status": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "canRelease": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        }
      }
    },
    "created_at": {
      "type": "date"
    },
    "updateDate": {
      "type": "date"
    },
    "updateUser": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    }
  }
}
