import AWS from "aws-sdk"
import axios from "axios"
import fileSize from "file-size"
import {
  getTokenFromCode,
  getAuthUrl,
  getContacts,
  refreshToken,
  getContactFolderList,
  getDirContactsFromFolderId,
  getCalendarEvents,
  getTasks,
  getDocs
} from "./auth"
import {Docs} from "../docs"

const request = require("request-promise")


export const getMicrosoftAuthUrl = async(req, res) => {
  try {
    const params = {}
    console.log("Test Params :", params)
    params.signInUrl = await getAuthUrl()
    res.status(200).send(params)
  } catch (error) {
    console.log(error)
    res
      .status(422)
      .send({done: false, error: "There is a failure in retrieving microsoft auth info"})
  }
}

export const getOutLookContacts = async (req, res) => { // eslint-disable-line
  try {
    console.log("Fly for token....")
    const  {code, msCredential} = req.body
    if(msCredential && msCredential.access_token){
      const accessCredential = await refreshToken(msCredential)
      if(!accessCredential) {
        return res.status(422).send({done: false, error: "Not able to create refresh access token."})
      }
      const contacts = await getContacts(accessCredential.access_token)
      const directories = await getContactFolderList(accessCredential.access_token)
      return res.status(200).send({accessCredential, ...contacts, directories})
    }

    const accessCredential = await getTokenFromCode(code || "")
    console.log(accessCredential)
    if(accessCredential && !accessCredential.done){
      return res.status(422).send(accessCredential)
    }
    const contacts = await getContacts(accessCredential.access_token)
    const directories = await getContactFolderList(accessCredential.access_token)
    res.status(200).send({accessCredential, ...contacts, directories})

  } catch (error) {
    console.log("Access Token Error", error)
    res
      .status(422)
      .send({done: false, error: error.message})
  }
}

export const getAllOutLookContacts = async (req, res) => { // eslint-disable-line
  try {
    console.log("Fly for token....")
    const  {code, msCredential} = req.body
    if(msCredential && msCredential.access_token){
      const accessCredential = await refreshToken(msCredential)
      if(!accessCredential) {
        return res.status(422).send({done: false, error: "Not able to create refresh access token."})
      }
      const contacts = await getContacts(accessCredential.access_token)
      const directories = await getContactFolderList(accessCredential.access_token)

      for(const i in directories){ // eslint-disable-line
        const contact = await getDirContactsFromFolderId(directories[i].id, accessCredential.access_token) // eslint-disable-line
        contacts.contacts = [ ...contacts.contacts, ...contact ]
        contacts.totalResults = contacts.contacts.length
      }
      return res.status(200).send({accessCredential, ...contacts})
    }

    const accessCredential = await getTokenFromCode(code || "")
    console.log(accessCredential)
    if(accessCredential && !accessCredential.done){
      return res.status(422).send(accessCredential)
    }
    const contacts = await getContacts(accessCredential.access_token)
    const directories = await getContactFolderList(accessCredential.access_token)
    for(const i in directories){  // eslint-disable-line
      const contact = await getDirContactsFromFolderId(directories[i].id, accessCredential.access_token) // eslint-disable-line
      contacts.contacts = [ ...contacts.contacts, ...contact ]
      contacts.totalResults = contacts.contacts.length
    }
    res.status(200).send({accessCredential, ...contacts})

  } catch (error) {
    console.log("Access Token Error", error)
    res
      .status(422)
      .send({done: false, error: error.message})
  }
}

export const getOutLookContactDirectory = async (req, res) => { // eslint-disable-line
  try {
    const  {code, msCredential, folderId} = req.body
    if(msCredential && msCredential.access_token){
      const accessCredential = await refreshToken(msCredential)
      if(!accessCredential) {
        return res.status(422).send({done: false, error: "Not able to create refresh access token."})
      }
      const dirContacts = await getDirContactsFromFolderId(folderId, accessCredential.access_token)
      return res.status(200).send({accessCredential, dirContacts})
    }

    const accessCredential = await getTokenFromCode(code || "")
    if(accessCredential && !accessCredential.done){
      return res.status(422).send(accessCredential)
    }
    const dirContacts = await getDirContactsFromFolderId(folderId, accessCredential.access_token)
    res.status(200).send({accessCredential, dirContacts})

  } catch (error) {
    console.log("Access Token Error", error)
    res
      .status(422)
      .send({done: false, error: error.message})
  }
}

export const getOutLookCalendarEvents = async (req, res) => { // eslint-disable-line
  try {
    const  {code, msCredential} = req.body
    if(msCredential && msCredential.access_token){
      const accessCredential = await refreshToken(msCredential)
      if(!accessCredential) {
        return res.status(422).send({done: false, error: "Not able to create refresh access token."})
      }
      const events = await getCalendarEvents(accessCredential.access_token)
      return res.status(200).send({accessCredential, events})
    }

    const accessCredential = await getTokenFromCode(code || "")
    if(accessCredential && !accessCredential.done){
      return res.status(422).send(accessCredential)
    }
    const events = await getCalendarEvents(accessCredential.access_token)
    res.status(200).send({accessCredential, events})

  } catch (error) {
    console.log("Access Token Error", error)
    res
      .status(422)
      .send({done: false, error: error.message})
  }
}

export const getPlannerTasks = async (req, res) => { // eslint-disable-line
  try {
    const  {code, msCredential} = req.body
    if(msCredential && msCredential.access_token){
      const accessCredential = await refreshToken(msCredential)
      if(!accessCredential) {
        return res.status(422).send({done: false, error: "Not able to create refresh access token."})
      }
      const events = await getTasks(accessCredential.access_token)
      return res.status(200).send({accessCredential, events})
    }

    const accessCredential = await getTokenFromCode(code || "")
    if(accessCredential && !accessCredential.done){
      return res.status(422).send(accessCredential)
    }
    const events = await getTasks(accessCredential.access_token)
    res.status(200).send({accessCredential, events})

  } catch (error) {
    console.log("Access Token Error", error)
    res
      .status(422)
      .send({done: false, error: error.message})
  }
}

export const getOneDriveDocs = async (req, res) => { // eslint-disable-line
  try {
    const  {code, msCredential} = req.body
    if(msCredential && msCredential.access_token){
      const accessCredential = await refreshToken(msCredential)
      if(!accessCredential) {
        return res.status(422).send({done: false, error: "Not able to create refresh access token."})
      }
      const driveAndDocs = await getDocs(accessCredential.access_token)
      return res.status(200).send({accessCredential, driveAndDocs})
    }

    const accessCredential = await getTokenFromCode(code || "")
    if(accessCredential && !accessCredential.done){
      return res.status(422).send(accessCredential)
    }
    const driveAndDocs = await getDocs(accessCredential.access_token)
    res.status(200).send({accessCredential, driveAndDocs})

  } catch (error) {
    console.log("Access Token Error", error)
    res
      .status(422)
      .send({done: false, error: error.message})
  }
}

export const getOneDriveFolderAndFilesDetails = async (req, res) => {
  try {
    const  {token, id, type} = req.body
    const msData = await axios.get(`https://graph.microsoft.com/v1.0/me/drive/items/${id}${type || ""}`, {headers: { Authorization: token || "" }})
    const data = type === "/children" ? msData && msData.data && msData.data.value : msData && msData.data
    res.status(200).send(data)

  } catch (error) {
    console.log({error})
    res
      .status(422)
      .send({done: false, error: error.message})
  }
}

export const s3 = new AWS.S3({
  region: process.env.SESREGION,
  bucket: process.env.S3BUCKET,
  accessKeyId: process.env.S3ACCESSKEYID,
  secretAccessKey: process.env.S3SECRETKEY
})

export const msDocumentCopyToAWS = (url, bucket, key, callback) => new Promise(async (response) => {
  request({
    url,
    encoding: null
  }, async (err, res, body) => {
    if (err)
      return callback(err, res)

    response(await s3.upload({
      Bucket: bucket,
      Key: key,
      ContentType: res.headers["content-type"],
      ContentLength: res.headers["content-length"],
      Body: body // buffer
    }, callback))
  })
})

export const postOneDriveDocsToMuni = async (req, res) => {
  const { keys, token, userDetails } = req.body
  try {

    const item = []
    const { tenantId, entityId, folderId, userId, userName } = userDetails

    for (const k of keys) {

      const msData = await axios.get(`${k && k[0]}/drives/${k && k[1]}/items/${k && k[2]}`, {headers: { Authorization: token || "" }})
      const originalName = msData && msData.data && msData.data.name || ""

      let name = ""
      const extnIdx = originalName.lastIndexOf(".")
      if (extnIdx > -1) {
        name = `${originalName.substr(
          0,
          extnIdx
        )}_${new Date().getTime()}${originalName.substr(extnIdx)}`
      }

      await msDocumentCopyToAWS(msData && msData.data && msData.data["@microsoft.graph.downloadUrl"], process.env.S3BUCKET, `${tenantId}/USERSDOCS/USERSDOCS_${userId}/${name}`, async (err, response) => {

        if (err)
          throw err

        console.log("Uploaded data successfully!")

        const docs = {
          tenantId: entityId,
          name,
          originalName,
          folderId,
          contextType: "USERSDOCS",
          contextId: userId,
          meta: {
            category: "Firm Documents",
            subCategory: "Firm User Documents",
            type: "Firm Documents",
            versions: [{
              name,
              originalName,
              size: fileSize(msData && msData.data && msData.data.size || 0).human("jedec"),
              uploadDate: new Date(),
              uploadedBy: userName,
              uploadedByUserEntityId: entityId,
              uploadedByUserId: userId,
              uploadedByUserTenantId: tenantId,
              versionId: response && response.VersionId
            }]
          },
          createdDate: new Date(),
          createdBy: userName
        }

        item.push(docs)

        if(keys.length === item.length){
          const data = await Docs.insertMany(item)
          await res.send({done: true, data})
        }
      })
    }

  } catch (error) {
    console.log({error})
    console.log({message: error.message})
    res
      .status(422)
      .send({done: false, error: error.message})
  }

}

export const getFileDetails = async (id, token, type) => {
  const msData = await axios.get(`https://graph.microsoft.com/v1.0/me/drive/items/${id}${type || ""}`, {headers: { Authorization: token || "" }})
  return type === "/children" ? msData && msData.data && msData.data.value : msData && msData.data

}

export const fileNameChanger = (fileName) => {
  const extnIdx = fileName.lastIndexOf(".")
  if (extnIdx > -1) {
    return `${fileName.substr(
      0,
      extnIdx
    )}_${new Date().getTime()}${fileName.substr(extnIdx)}`
  }
}

export const postOneDriveDocsToMuniSecond = async (req, res) => {
  const { msCredential, userDetails, copyDetailsIds } = req.body
  try {
    const { userName, entityId, tenantId, userId, contextType } = userDetails

    if(msCredential && msCredential.access_token){
      const accessCredential = await refreshToken(msCredential)
      if(!accessCredential) {
        return res.status(422).send({done: false, message: "Not able to create refresh access token."})
      }

      const driveAndDocs = copyDetailsIds

      const item = []
      let length = 0

      if(driveAndDocs && driveAndDocs.length){

        for (const getLastFileData of driveAndDocs) {
          if (getLastFileData && getLastFileData.hasOwnProperty("file")) {
            length += 1
          }
          if (getLastFileData && getLastFileData.hasOwnProperty("folder")) {
            if (getLastFileData && getLastFileData.folder && getLastFileData.folder.childCount > 0) {
              const stepOne = await getFileDetails(getLastFileData && getLastFileData.id, msCredential.access_token, "/children")
              if (stepOne && stepOne.length) {

                for (const stpOne of stepOne) {
                  if (stpOne && stpOne.folder && stpOne.folder.childCount > 0) {
                    const stepTwo = await getFileDetails(stpOne && stpOne.id, msCredential.access_token, "/children")
                    for (const stpTwo of stepTwo) {
                      if (stpTwo && stpTwo.folder && stpTwo.folder.childCount > 0) {
                        const stepThree = await getFileDetails(stpTwo && stpTwo.id, msCredential.access_token, "/children")
                        for (const stpThree of stepThree) {
                          if (stpThree && stpThree.folder && stpThree.folder.childCount > 0) {
                            const stepFour = await getFileDetails(stpThree && stpThree.id, msCredential.access_token, "/children")

                            for (const stpFour of stepFour) {
                              if (stpFour && stpFour.folder && stpFour.folder.childCount > 0) {
                                const stepFive = await getFileDetails(stpFour && stpFour.id, msCredential.access_token, "/children")

                                for (const stpFive of stepFive) {
                                  if (stpFive && stpFive.folder && stpFive.folder.childCount > 0) {
                                    const stepSix = await getFileDetails(stpFive && stpFive.id, msCredential.access_token, "/children")

                                    for (const stpSix of stepSix) {
                                      // if(stpSix && stpSix.folder && stpSix.folder.childCount > 0) {
                                      //
                                      // }
                                      if (stpSix && stpSix.file && stpSix.file.mimeType) {
                                        length += 1
                                      }
                                    }
                                  }
                                  if (stpFive && stpFive.file && stpFive.file.mimeType) {
                                    length += 1
                                  }
                                }
                              }
                              if (stpFour && stpFour.file && stpFour.file.mimeType) {
                                length += 1
                              }
                            }
                          }
                          if (stpThree && stpThree.file && stpThree.file.mimeType) {
                            length += 1
                          }
                        }
                      }
                      if (stpTwo && stpTwo.file && stpTwo.file.mimeType) {
                        length += 1
                      }
                    }
                  }
                  if (stpOne && stpOne.file && stpOne.file.mimeType) {
                    length += 1
                  }
                }
              }
            }
          }
        }

        for (const [index, drv] of driveAndDocs.entries()) {
          // console.log("==============336=========", drv)

          if(drv && drv.folder && drv.folder.childCount > 0){
            const stepOne = await getFileDetails(drv && drv.id, msCredential.access_token, "/children")

            if(stepOne && stepOne.length){

              for (const stpOne of stepOne) {

                if(stpOne && stpOne.folder && stpOne.folder.childCount > 0) {
                  const stepTwo = await getFileDetails(stpOne && stpOne.id, msCredential.access_token, "/children")

                  for (const stpTwo of stepTwo) {
                    if(stpTwo && stpTwo.folder && stpTwo.folder.childCount > 0) {
                      const stepThree = await getFileDetails(stpTwo && stpTwo.id, msCredential.access_token, "/children")

                      for (const stpThree of stepThree) {
                        if(stpThree && stpThree.folder && stpThree.folder.childCount > 0) {
                          const stepFour = await getFileDetails(stpThree && stpThree.id, msCredential.access_token, "/children")

                          for (const stpFour of stepFour) {
                            if(stpFour && stpFour.folder && stpFour.folder.childCount > 0) {
                              const stepFive = await getFileDetails(stpFour && stpFour.id, msCredential.access_token, "/children")

                              for (const stpFive of stepFive) {
                                if(stpFive && stpFive.folder && stpFive.folder.childCount > 0) {
                                  const stepSix = await getFileDetails(stpFive && stpFive.id, msCredential.access_token, "/children")

                                  for (const stpSix of stepSix) {
                                    // if(stpSix && stpSix.folder && stpSix.folder.childCount > 0) {
                                    //
                                    // }
                                    if(stpSix && stpSix.file && stpSix.file.mimeType){
                                      const stepSixFile = await getFileDetails(stpSix && stpSix.id, msCredential.access_token, "")
                                      const originalName = stepSixFile && stepSixFile.name
                                      const name = await fileNameChanger(originalName)
                                      const context = stepSixFile && stepSixFile.parentReference && stepSixFile.parentReference.path && stepSixFile.parentReference.path.split("root:/")
                                      const contextId = context && context[1] || ""
                                      await msDocumentCopyToAWS(stepSixFile && stepSixFile["@microsoft.graph.downloadUrl"],
                                        process.env.S3BUCKET, `${tenantId}/${contextType}${contextId}${context[1] ? name : `/${name}`}`, async (err, response) => {

                                          if (err)
                                            throw err

                                          if(response){
                                            console.log("Uploaded data successfully!")
                                            const docs = {
                                              tenantId: entityId,
                                              name,
                                              originalName,
                                              contextType,
                                              contextId,
                                              meta: {
                                                versions: [{
                                                  name,
                                                  originalName,
                                                  size: fileSize(stepSixFile && stepSixFile.size || 0).human("jedec"),
                                                  uploadDate: new Date(),
                                                  uploadedBy: userName,
                                                  uploadedByUserEntityId: entityId,
                                                  uploadedByUserId: userId,
                                                  uploadedByUserTenantId: tenantId,
                                                  versionId: response && response.VersionId
                                                }]
                                              },
                                              createdDate: new Date(),
                                              createdBy: userName
                                            }
                                            item.push(docs)
                                            if(item.length === length){
                                              const data = await Docs.insertMany(item)
                                              return res.status(200).send({done: true, data})
                                            }
                                          }
                                        }
                                      )
                                    }
                                  }
                                }
                                if(stpFive && stpFive.file && stpFive.file.mimeType){
                                  const stepFiveFile = await getFileDetails(stpFive && stpFive.id, msCredential.access_token, "")
                                  const originalName = stepFiveFile && stepFiveFile.name
                                  const name = await fileNameChanger(originalName)
                                  const context = stepFiveFile && stepFiveFile.parentReference && stepFiveFile.parentReference.path && stepFiveFile.parentReference.path.split("root:/")
                                  const contextId = context && context[1] || ""
                                  await msDocumentCopyToAWS(stepFiveFile && stepFiveFile["@microsoft.graph.downloadUrl"],
                                    process.env.S3BUCKET, `${tenantId}/${contextType}/${contextId}${contextId ? `/${name}` : name}`, async (err, response) => {

                                      if (err)
                                        throw err

                                      if(response){
                                        console.log("Uploaded data successfully!")
                                        const docs = {
                                          tenantId: entityId,
                                          name,
                                          originalName,
                                          contextType,
                                          contextId,
                                          meta: {
                                            versions: [{
                                              name,
                                              originalName,
                                              size: fileSize(stepFiveFile && stepFiveFile.size || 0).human("jedec"),
                                              uploadDate: new Date(),
                                              uploadedBy: userName,
                                              uploadedByUserEntityId: entityId,
                                              uploadedByUserId: userId,
                                              uploadedByUserTenantId: tenantId,
                                              versionId: response && response.VersionId
                                            }]
                                          },
                                          createdDate: new Date(),
                                          createdBy: userName
                                        }
                                        item.push(docs)

                                        if(item.length === length){
                                          const data = await Docs.insertMany(item)
                                          return res.status(200).send({done: true, data})
                                        }
                                      }
                                    }
                                  )
                                }
                              }
                            }
                            if(stpFour && stpFour.file && stpFour.file.mimeType){
                              const stepFourFile = await getFileDetails(stpFour && stpFour.id, msCredential.access_token, "")
                              const originalName = stepFourFile && stepFourFile.name
                              const name = await fileNameChanger(originalName)
                              const context = stepFourFile && stepFourFile.parentReference && stepFourFile.parentReference.path && stepFourFile.parentReference.path.split("root:/")
                              const contextId = context && context[1] || ""
                              await msDocumentCopyToAWS(stepFourFile && stepFourFile["@microsoft.graph.downloadUrl"],
                                process.env.S3BUCKET, `${tenantId}/${contextType}/${contextId}${contextId ? `/${name}` : name}`, async (err, response) => {

                                  if (err)
                                    throw err

                                  if(response){
                                    console.log("Uploaded data successfully!")
                                    const docs = {
                                      tenantId: entityId,
                                      name,
                                      originalName,
                                      contextType,
                                      contextId,
                                      meta: {
                                        versions: [{
                                          name,
                                          originalName,
                                          size: fileSize(stepFourFile && stepFourFile.size || 0).human("jedec"),
                                          uploadDate: new Date(),
                                          uploadedBy: userName,
                                          uploadedByUserEntityId: entityId,
                                          uploadedByUserId: userId,
                                          uploadedByUserTenantId: tenantId,
                                          versionId: response && response.VersionId
                                        }]
                                      },
                                      createdDate: new Date(),
                                      createdBy: userName
                                    }
                                    item.push(docs)

                                    if(item.length === length){
                                      const data = await Docs.insertMany(item)
                                      return res.status(200).send({done: true, data})
                                    }
                                  }
                                })
                            }
                          }
                        }
                        if(stpThree && stpThree.file && stpThree.file.mimeType){
                          const stepThreeFile = await getFileDetails(stpThree && stpThree.id, msCredential.access_token, "")
                          const originalName = stepThreeFile && stepThreeFile.name
                          const name = await fileNameChanger(originalName)
                          const context = stepThreeFile && stepThreeFile.parentReference && stepThreeFile.parentReference.path && stepThreeFile.parentReference.path.split("root:/")
                          const contextId = context && context[1] || ""
                          await msDocumentCopyToAWS(stepThreeFile && stepThreeFile["@microsoft.graph.downloadUrl"],
                            process.env.S3BUCKET, `${tenantId}/${contextType}/${contextId}${contextId ? `/${name}` : name}`, async (err, response) => {

                              if (err)
                                throw err

                              if(response){
                                console.log("Uploaded data successfully!")
                                const docs = {
                                  tenantId: entityId,
                                  name,
                                  originalName,
                                  contextType,
                                  contextId,
                                  meta: {
                                    versions: [{
                                      name,
                                      originalName,
                                      size: fileSize(stepThreeFile && stepThreeFile.size || 0).human("jedec"),
                                      uploadDate: new Date(),
                                      uploadedBy: userName,
                                      uploadedByUserEntityId: entityId,
                                      uploadedByUserId: userId,
                                      uploadedByUserTenantId: tenantId,
                                      versionId: response && response.VersionId
                                    }]
                                  },
                                  createdDate: new Date(),
                                  createdBy: userName
                                }
                                item.push(docs)

                                if(item.length === length){
                                  const data = await Docs.insertMany(item)
                                  return res.status(200).send({done: true, data})
                                }
                              }
                            })
                        }
                      }
                    }
                    if(stpTwo && stpTwo.file && stpTwo.file.mimeType){
                      const stepTwoFile = await getFileDetails(stpTwo && stpTwo.id, msCredential.access_token, "")
                      const originalName = stepTwoFile && stepTwoFile.name
                      const name = await fileNameChanger(originalName)
                      const context = stepTwoFile && stepTwoFile.parentReference && stepTwoFile.parentReference.path && stepTwoFile.parentReference.path.split("root:/")
                      const contextId = context && context[1] || ""
                      await msDocumentCopyToAWS(stepTwoFile && stepTwoFile["@microsoft.graph.downloadUrl"],
                        process.env.S3BUCKET, `${tenantId}/${contextType}/${contextId}${contextId ? `/${name}` : name}`, async (err, response) => {

                          if (err)
                            throw err

                          if(response){
                            console.log("Uploaded data successfully!")
                            const docs = {
                              tenantId: entityId,
                              name,
                              originalName,
                              contextType,
                              contextId,
                              meta: {
                                versions: [{
                                  name,
                                  originalName,
                                  size: fileSize(stepTwoFile && stepTwoFile.size || 0).human("jedec"),
                                  uploadDate: new Date(),
                                  uploadedBy: userName,
                                  uploadedByUserEntityId: entityId,
                                  uploadedByUserId: userId,
                                  uploadedByUserTenantId: tenantId,
                                  versionId: response && response.VersionId
                                }]
                              },
                              createdDate: new Date(),
                              createdBy: userName
                            }
                            item.push(docs)

                            if(item.length === length){
                              const data = await Docs.insertMany(item)
                              return res.status(200).send({done: true, data})
                            }
                          }

                        })
                    }
                  }
                }
                if(stpOne && stpOne.file && stpOne.file.mimeType){
                  const stepOneFile = await getFileDetails(stpOne && stpOne.id, msCredential.access_token, "")
                  const originalName = stepOneFile && stepOneFile.name
                  const name = await fileNameChanger(originalName)
                  const context = stepOneFile && stepOneFile.parentReference && stepOneFile.parentReference.path && stepOneFile.parentReference.path.split("root:/")
                  const contextId = context && context[1] || ""
                  await msDocumentCopyToAWS(stepOneFile && stepOneFile["@microsoft.graph.downloadUrl"],
                    process.env.S3BUCKET, `${tenantId}/${contextType}/${contextId}${contextId ? `/${name}` : name}`, async (err, response) => {

                      if (err)
                        throw err

                      if(response){
                        console.log("Uploaded data successfully!")
                        const docs = {
                          tenantId: entityId,
                          name,
                          originalName,
                          contextType,
                          contextId,
                          meta: {
                            versions: [{
                              name,
                              originalName,
                              size: fileSize(stepOneFile && stepOneFile.size || 0).human("jedec"),
                              uploadDate: new Date(),
                              uploadedBy: userName,
                              uploadedByUserEntityId: entityId,
                              uploadedByUserId: userId,
                              uploadedByUserTenantId: tenantId,
                              versionId: response && response.VersionId
                            }]
                          },
                          createdDate: new Date(),
                          createdBy: userName
                        }
                        item.push(docs)
                        if(item.length === length){
                          const data = await Docs.insertMany(item)
                          return res.status(200).send({done: true, data})
                        }
                      }
                    })
                }
              }
            }
          }

          if(drv && drv.file && drv.file.mimeType){
            const stepFile = await getFileDetails(drv && drv.id, msCredential.access_token, "")
            const originalName = stepFile && stepFile.name
            const name = await fileNameChanger(originalName)
            const context = stepFile && stepFile.parentReference && stepFile.parentReference.path && stepFile.parentReference.path.split("root:/")
            const contextId = context && context[1] || ""
            await msDocumentCopyToAWS(stepFile && stepFile["@microsoft.graph.downloadUrl"],
              process.env.S3BUCKET, `${tenantId}/${contextType}/${contextId}${contextId ? `/${name}` : name}`, async (err, response) => {

                if (err)
                  throw err

                if(response){
                  console.log("Uploaded data successfully!")
                  const docs = {
                    tenantId: entityId,
                    name,
                    originalName,
                    contextType,
                    contextId,
                    meta: {
                      versions: [{
                        name,
                        originalName,
                        size: fileSize(stepFile && stepFile.size || 0).human("jedec"),
                        uploadDate: new Date(),
                        uploadedBy: userName,
                        uploadedByUserEntityId: entityId,
                        uploadedByUserId: userId,
                        uploadedByUserTenantId: tenantId,
                        versionId: response && response.VersionId
                      }]
                    },
                    createdDate: new Date(),
                    createdBy: userName
                  }
                  item.push(docs)
                  if(item.length === length){
                    const data = await Docs.insertMany(item)
                    return res.status(200).send({done: true, data})
                  }
                }

              }
            )
          }

        }
      }

      if(item.length === length){
        const data = await Docs.insertMany(item)
        return res.status(200).send({done: true, data})
      }

    }

  } catch (error) {
    console.log({error})
    console.log({message: error.message})
    res
      .status(422)
      .send({done: false, error: error.message})
  }

}
