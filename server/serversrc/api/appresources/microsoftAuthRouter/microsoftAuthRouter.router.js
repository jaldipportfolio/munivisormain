import express from "express"
import {
  getMicrosoftAuthUrl,
  getAllOutLookContacts,
  getOutLookContactDirectory,
  getOutLookCalendarEvents,
  getPlannerTasks,
  getOneDriveDocs,
  postOneDriveDocsToMuni,
  postOneDriveDocsToMuniSecond,
  getOneDriveFolderAndFilesDetails
} from "./microsoftAuthRouter.controller"

import {requireAuth} from "./../../../authorization/authsessionmanagement/auth.middleware"

export const microsoftAuthRouter = express.Router()

microsoftAuthRouter.route("/authurl")
  .get(requireAuth, getMicrosoftAuthUrl)

microsoftAuthRouter.route("/contacts")
  .post(requireAuth, getAllOutLookContacts)

microsoftAuthRouter.route("/dircontacts")
  .post(requireAuth, getOutLookContactDirectory)

microsoftAuthRouter.route("/calendarevents")
  .post(requireAuth, getOutLookCalendarEvents)

microsoftAuthRouter.route("/plannertasks")
  .post(requireAuth, getPlannerTasks)

microsoftAuthRouter.route("/onedrivedocs")
  .post(requireAuth, getOneDriveDocs)

microsoftAuthRouter.route("/get_onedrive_folder")
  .post(requireAuth, getOneDriveFolderAndFilesDetails)

microsoftAuthRouter.route("/onedrive_to_muni")
  .post(requireAuth, postOneDriveDocsToMuni)

microsoftAuthRouter.route("/onedrive_to_muni_second")
  .post(requireAuth, postOneDriveDocsToMuniSecond)
