import { Client } from "@microsoft/microsoft-graph-client"
import moment from "moment"

const credentials = {
  client: {
    id: process.env.MS_APP_ID,
    secret: process.env.MS_APP_PASSWORD,
  },
  auth: {
    tokenHost: "https://login.microsoftonline.com",
    authorizePath: "common/oauth2/v2.0/authorize",
    tokenPath: "common/oauth2/v2.0/token"
  }
}
const oauth2 = require("simple-oauth2").create(credentials)

/*
{
  "assistantName": "string",
  "birthday": "String (timestamp)",
  "businessAddress": {"@odata.type": "microsoft.graph.physicalAddress"},
  "businessHomePage": "string",
  "businessPhones": ["string"],
  "categories": ["string"],
  "changeKey": "string",
  "children": ["string"],
  "companyName": "string",
  "createdDateTime": "String (timestamp)",
  "department": "string",
  "displayName": "string",
  "emailAddresses": [{"@odata.type": "microsoft.graph.emailAddress"}],
  "fileAs": "string",
  "generation": "string",
  "givenName": "string",
  "homeAddress": {"@odata.type": "microsoft.graph.physicalAddress"},
  "homePhones": ["string"],
  "id": "string (identifier)",
  "imAddresses": ["string"],
  "initials": "string",
  "jobTitle": "string",
  "lastModifiedDateTime": "String (timestamp)",
  "manager": "string",
  "middleName": "string",
  "mobilePhone": "string",
  "nickName": "string",
  "officeLocation": "string",
  "otherAddress": {"@odata.type": "microsoft.graph.physicalAddress"},
  "parentFolderId": "string",
  "personalNotes": "string",
  "profession": "string",
  "spouseName": "string",
  "surname": "string",
  "title": "string",
  "yomiCompanyName": "string",
  "yomiGivenName": "string",
  "yomiSurname": "string",
  "photo": { "@odata.type": "microsoft.graph.profilePhoto" }
}
*/

export const getAuthUrl = () => {
  console.log(
    {
      id: process.env.MS_APP_ID,
      secret: process.env.MS_APP_PASSWORD,
      redirect_uri: process.env.MS_REDIRECT_URI,
      scope: process.env.MS_APP_SCOPES
    }
  )
  const returnVal = oauth2.authorizationCode.authorizeURL({
    redirect_uri: process.env.MS_REDIRECT_URI,
    scope: process.env.MS_APP_SCOPES,
    response_type: "code"
  })
  console.log(`Generated auth url: ${returnVal}`)
  return returnVal
}

export const getTokenFromCode  = async (code) => {
  try {
    const result = await oauth2.authorizationCode.getToken({
      code,
      redirect_uri: process.env.MS_REDIRECT_URI,
      scope: process.env.MS_APP_SCOPES, // also can be an array of multiple scopes, ex. ['<scope1>, '<scope2>', '...']
      expires_in: process.env.MS_TOKEN_EXPIRED_IN
    })

    const token = oauth2.accessToken.create(result)
    console.log("Token created: ", token.token)
    return {done:true, ...token.token }
  } catch (error) {
    console.log("Access Token Error", error)
    return {done: false, error: "Not able to create access token."}
  }
}

export const getAuthTokenFromIdPwd= async () => {
  try {
    const tokenConfig = {
      username: process.env.MS_ACC_ID,
      password: process.env.MS_ACC_PWD,
      scope: process.env.MS_APP_SCOPES, // also can be an array of multiple scopes, ex. ['<scope1>, '<scope2>', '...']
    }
    const result = await oauth2.ownerPassword.getToken(tokenConfig)
    const accessToken = oauth2.accessToken.create(result)
    return {done:true, accessToken }
  } catch (error) {
    console.log("Access Token Error", error)
    return {done: false, error: error.message}
  }
}

export const getContacts= async (accessToken) => {
  try {
    const client = Client.init({
      authProvider: (done) => {
        done(null, accessToken)
      }
    })

    const totalResults = await client
      .api("/me/contacts")
      .count(true)
      .get()

    const result = await client
      .api("/me/contacts")
      .select("*")
      .top(totalResults["@odata.count"])
      .orderby("givenName ASC")
      .get()

    return {done:true, contacts: result.value, totalResults: totalResults["@odata.count"] }
  } catch (error) {
    console.log("Access Token Error", error)
    return {done: false, error: error.message}
  }
}

export const refreshToken = async (credential) => {
  const expiration = moment(credential.expires_at).unix()
  if (expiration > moment(new Date()).unix()) {
    return credential
  }
  if (credential.refresh_token) {
    const freshToken = await oauth2.accessToken.create({refresh_token: credential.refresh_token}).refresh()
    return {...freshToken.token }
  }
  return null
}

export const getContactFolderList = async (accessToken) => {
  const client = Client.init({
    authProvider: (done) => {
      done(null, accessToken)
    }
  })

  const result = await client
    .api("/me/contactFolders")
    .get()
  return result.value
}

export const getDirContactsFromFolderId = async (folderId, accessToken) => {
  const client = Client.init({
    authProvider: (done) => {
      done(null, accessToken)
    }
  })

  const result = await client
    .api(`/me/contactfolders/${folderId}/contacts`)
    .get()
  return result.value
}

export const getCalendarEvents = async (accessToken) => {
  const client = Client.init({
    authProvider: (done) => {
      done(null, accessToken)
    }
  })

  const result = await client
    .api("/me/calendar/events")
    .get()
  return result.value
}

export const getTasks = async (accessToken) => {
  const client = Client.init({
    authProvider: (done) => {
      done(null, accessToken)
    }
  })

  const result = await client
    .api("/me/planner/tasks")
    .get()
  return result.value
}

export const getDocs = async (accessToken) => {
  const client = Client.init({
    authProvider: (done) => {
      done(null, accessToken)
    }
  })

  const result = await client
    .api("/me/drive/root/children")
    .select("@microsoft.graph.downloadUrl,id,name,size,parentReference,file,folder")
    .get()
  return result.value
}
