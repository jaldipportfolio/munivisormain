import mongoose from "mongoose"
import timestamps from "mongoose-timestamp"
import { EntityUser } from "./../../models"
import {Docs} from "../../docs/docs.model"

const { Schema } = mongoose


const superVisorDocumentSchema =Schema({
  docCategory:String,
  docSubCategory:String,
  docType:String,
  docAWSFileLocation: {type: Schema.Types.ObjectId, ref: Docs},
  docFileName:String,
  docStatus:String, // WIP, send for review and other flags
  docActions:[{
    actionType:String,
    actionDate:Date
  }],
  createdBy: {type: Schema.Types.ObjectId, ref: EntityUser},
  createdUserName: String,
  lastUpdatedDate: { type: Date, required: true, default: Date.now }
})

const PoliContributionSummarySchema = new Schema({
  entityId: { type: Schema.Types.ObjectId },
  entityName: String,
  year: Number,
  quarter: Number,
  emmaSubmissionStatus: String,
  municipalSecuritiesBusiness: [{
    entityId: String,
    objectId: Number,
    selected: Boolean,
    securitiesBusiness: [{
      id: String,
      name: String,
      label: String
    }],
  }],
  municipalAdvisoryBusiness: [{
    entityId: String,
    objectId: Number,
    selected: Boolean,
    advisoryBusiness: [{
      id: String,
      name: String,
      label: String
    }],
    solicitedBusiness: [{
      id: String,
      name: String,
      label: String
    }],
    thirdPartyName: String
  }],
  ballotApprovedOfferings: [{
    entityId: String,
    objectId: Number,
    selected: Boolean,
    reportableDate: Date
  }],
  securityToggle: Boolean,
  advisoryToggle: Boolean,
  offeringsToggle: Boolean,
  poliContributionSummaryDocuments:[superVisorDocumentSchema],
}).plugin(timestamps)

export const PoliContributionSummary = mongoose.model("poliContributionSummary", PoliContributionSummarySchema)
