import mongoose from "mongoose"
import timestamps from "mongoose-timestamp"
import { EntityUser } from "./../../models"
import {Docs} from "../../docs/docs.model"

const { Schema } = mongoose

const auditLogs = Schema({
  superVisorModule: String, // General Admin Activities,
  superVisorSubSection: String, // Documents
  userId: { type: Schema.Types.ObjectId, ref: EntityUser },
  userName: String,
  log: String,
  ip: String,
  date: Date
})

const superVisorDocumentSchema =Schema({
  docCategory:String,
  docSubCategory:String,
  docType:String,
  docAWSFileLocation: {type: Schema.Types.ObjectId, ref: Docs},
  docFileName:String,
  docStatus:String, // WIP, send for review and other flags
  docNote:String,
  docActions:[{
    actionType:String,
    actionDate:Date
  }],
  createdBy: {type: Schema.Types.ObjectId, ref: EntityUser},
  createdUserName: String,
  lastUpdatedDate: { type: Date, required: true, default: Date.now }
})

const generalAdminActivities = new Schema({
  finAdvisorEntityId:Schema.Types.ObjectId,
  finAdvisorEntityName:String,
  gaDesignateFirmContacts:[{
    userId:Schema.Types.ObjectId,
    name:String,
    userFirstName:String,
    userLastName:String,
    userPrimaryEmailId:String,
    userPrimaryPhone:String,
    userAddress:String,
    userContactType:String
  }],
  gaQualifiedAssPersons:[{
    qualification:String,
    userId:Schema.Types.ObjectId,
    name:String,
    userFirstName:String,
    userLastName:String,
    userPrimaryPhone:String,
    userPrimaryEmailId:String,
    profFeePaidOn:Date,
    series50PassDate:Date,
    series50ValidityEndDate:Date,
    createdDate: { type: Date, required: true, default: Date.now },
  }],
  businessActivities: [{
    activity: String,
    subActivity: String,
    notes: String,
  }],
  gaFirmRegInformation: [{
    registeringBody: String,
    regFeePaidOn: Date,
    feeAmount: Number,
    regValidTill: Date,
  }],
  gaGeneralFirmInfo: [{
    field: String,
    notes: String,
    information: String,
    isField: Boolean,
  }],
  gaDocuments:[superVisorDocumentSchema],
  auditLogs:[auditLogs],
  lastUpdateDate:{type:Date,required: true,default: Date.now}
}).plugin(timestamps)

export const GeneralAdmin = mongoose.model("generaladmin", generalAdminActivities)
