import {generateControllers} from "./../../../modules/query"
import {GeneralAdmin} from "./generaladmin.model"
import {Entity} from "../../entity/entity.model"

const {ObjectID} = require("mongodb")

const getGeneralAdminDetails = () => async (req, res) => {
  const resRequested = [
    {resource: "GeneralAdmin", access: 1},
  ]
  const {type} = req.query
  try {
    let select = ""
    if(type === "documents"){
      select = "finAdvisorEntityId gaDocuments"
    }else {
      select = "finAdvisorEntityId gaDesignateFirmContacts gaQualifiedAssPersons auditLogs gaDocuments businessActivities gaFirmRegInformation gaGeneralFirmInfo"
    }
    const generalAdmin = await GeneralAdmin.findOne({finAdvisorEntityId: req.user.entityId}).select(select)
    res.json(generalAdmin)
  } catch (error) {
    res.status(422).send({done:false, error})
  }
}

const putGenAdminContactsOrPersons = () => async (req, res) => {
  const resRequested = [
    {resource:"GeneralAdmin",access:2},
    {resource:"Entity",access:2},
  ]
  const {type} = req.query
  try {
    let generalAdmin = await GeneralAdmin.findOne({finAdvisorEntityId: req.user.entityId})
    const details = []
    const entity = await Entity.findOne({_id: req.user.entityId}).select("firmName msrbFirmName msrbRegistrantType")
    const {_id } = req.body

    if(type === "gaDesignateFirmContacts" || type === "gaQualifiedAssPersons" || type === "businessActivities" || type === "gaFirmRegInformation" || type === "gaGeneralFirmInfo") {
      if (generalAdmin) {
        if (_id) {
          const dndString1 = `${type}._id`
          const dndString2 = `${type}.$`
          const query1 = {finAdvisorEntityId: ObjectID(req.user.entityId), [dndString1]: ObjectID(_id)}
          const query2 = {$set: {[dndString2]: req.body}}
          await GeneralAdmin.updateOne(query1, query2)
        } else {
          if(type === "gaGeneralFirmInfo"){
            await GeneralAdmin.updateOne(
              {finAdvisorEntityId: ObjectID(req.user.entityId)},
              {$set: {[type]: req.body}}
            )
          } else {
            await GeneralAdmin.updateOne(
              {finAdvisorEntityId: ObjectID(req.user.entityId)},
              {$addToSet: {[type]: req.body}}
            )
          }
        }
      } else {
        details.push(req.body)
        const newGenAdmin = new GeneralAdmin({
          finAdvisorEntityId: req.user.entityId,
          finAdvisorEntityName: (entity && entity.firmName) || "",
          [type]: type === "gaGeneralFirmInfo" ? req.body : details
        })
        await newGenAdmin.save()
      }

      generalAdmin = await GeneralAdmin.findOne({finAdvisorEntityId: req.user.entityId}).select(`finAdvisorEntityId ${type}`)
      res.json(generalAdmin)

    }else {
      res.status(404).send({done:false,error:"pass query params"})
    }

  } catch(error) {
    console.log("======================", error)
    res.status(422).send({done:false, error})
  }
}

const pullGenAdminContactsOrPersons = () => async (req, res) => {
  const { pullId } = req.params
  const {type} = req.query
  const resRequested = [
    {resource:"GeneralAdmin",access:2},
  ]
  try {
    if(type === "gaDesignateFirmContacts" || type === "gaQualifiedAssPersons" || type === "businessActivities" || type === "gaFirmRegInformation" || type === "gaGeneralFirmInfo") {
      await GeneralAdmin.update({ finAdvisorEntityId: req.user.entityId}, { $pull: { [type]: { _id: pullId } }})
      const generalAdmin = await GeneralAdmin.findOne({ finAdvisorEntityId: req.user.entityId}).select(`finAdvisorEntityId ${type}`)
      res.json(generalAdmin)
    }else {
      res.status(404).send({done:false,error:"invalid query params"})
    }
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const getGenAdminContactsOrPersons = () => async (req, res) => {
  const { pullId } = req.params
  const {type} = req.query
  const resRequested = [
    {resource:"GeneralAdmin",access:2},
  ]
  console.log("======100========>", pullId, type)
  try {
    if(type === "gaDesignateFirmContacts" || type === "gaQualifiedAssPersons" || type === "businessActivities" || type === "gaFirmRegInformation") {
      const generalAdmin = await GeneralAdmin.find({finAdvisorEntityId:  req.user.entityId} , {[type]: {$elemMatch: {_id: pullId  }}})
      res.json({generalAdmin})

    }else {
      res.status(404).send({done:false,error:"invalid query params"})
    }
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

const putGenAdminDocuments = () => async (req, res) => {
  const resRequested = [
    {resource:"GeneralAdmin",access:2},
  ]
  const {details} = req.query
  try {
    let generalAdmin = await GeneralAdmin.findOne({finAdvisorEntityId: req.user.entityId})
    const entity = await Entity.findOne({_id: req.user.entityId}).select("firmName msrbFirmName msrbRegistrantType")

    if(details === "docStatus"){
      const { _id, docStatus} = req.body
      await GeneralAdmin.updateOne(
        { finAdvisorEntityId: req.user.entityId, "gaDocuments._id": ObjectID(_id)},
        { $set: { "gaDocuments.$.docStatus" : docStatus } })
    }else if(details === "updateMeta"){
      const {_id } = req.body
      await GeneralAdmin.updateOne(
        { finAdvisorEntityId: req.user.entityId, "gaDocuments._id": ObjectID(_id)},
        { $set: { "gaDocuments.$" : req.body } })
    }else if (generalAdmin) {
      await GeneralAdmin.update({ finAdvisorEntityId: req.user.entityId}, {$addToSet: {gaDocuments: req.body.gaDocuments}})
    } else {
      const newGenAdmin = new GeneralAdmin({
        finAdvisorEntityId: req.user.entityId,
        finAdvisorEntityName: (entity && entity.firmName) || "",
        gaDocuments: req.body.gaDocuments
      })
      await newGenAdmin.save()
    }
    generalAdmin = await GeneralAdmin.findOne({ finAdvisorEntityId: req.user.entityId}).select("finAdvisorEntityId gaDocuments")
    res.json(generalAdmin)
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

export const pullGenAdminDocuments = () => async (req, res) => {
  const { docId } = req.query
  try {
    if(docId) {

      await GeneralAdmin.update({ finAdvisorEntityId: req.user.entityId}, { $pull: { gaDocuments: { _id: docId } }})

      const document = await GeneralAdmin.findOne({ finAdvisorEntityId: req.user.entityId}).select("gaDocuments")

      res.status(200).json(document)
    }else {

      res.status(422).send({done:false,error:"This Document doesn't exists."})

    }
  } catch(error) {
    console.log("===========================", error.message)
    res.status(422).send({done:false, error: error.message})

  }
}

const putAuditLogs = () => async (req, res) => {
  const resRequested = [
    {resource:"GeneralAdmin",access:2},
  ]
  try {
    await GeneralAdmin.update({ finAdvisorEntityId: req.user.entityId}, {$addToSet: {auditLogs: req.body} })
    const generalAdmin = await GeneralAdmin.findOne({ finAdvisorEntityId: req.user.entityId}).select("auditLogs")
    res.json(generalAdmin)
  } catch(error) {
    res.status(422).send({done:false, error})
  }
}

export default generateControllers(GeneralAdmin, {
  getGeneralAdminDetails: getGeneralAdminDetails(),
  putGenAdminContactsOrPersons: putGenAdminContactsOrPersons(),
  pullGenAdminContactsOrPersons: pullGenAdminContactsOrPersons(),
  getGenAdminContactsOrPersons: getGenAdminContactsOrPersons(),
  putGenAdminDocuments: putGenAdminDocuments(),
  pullGenAdminDocuments: pullGenAdminDocuments(),
  putAuditLogs: putAuditLogs(),
})
