import Joi from "joi-browser"

const giftExpensesSchema = Joi.object().keys({
  recipientEntityId: Joi.string().required(),
  recipientEntityName: Joi.string().required(),
  recipientUserId: Joi.string().required(),
  name: Joi.string().required(),
  recipientUserFirstName: Joi.string().required(),
  recipientUserLastName: Joi.string().required(),
  giftValue: Joi.number().min(0).required(),
  giftDate: Joi.date().allow([null,""]).example(new Date("2011-01-01")).allow("", null).optional(),
  createdUserId: Joi.string().allow("").optional(),
  createdUserName: Joi.string().allow("").optional(),
  createdDate: Joi.date().allow("").optional(),
  _id: Joi.string().optional(),
})

export const RecordGiftAndExpenses = (inputTransDistribute) => Joi.validate(inputTransDistribute, giftExpensesSchema, { abortEarly: false, stripUnknown:false })

export const errorHandling = (req, res, next) => {
  const { type } = req.query
  let errors
  if(type === "giftsToRecipients") {
    errors = RecordGiftAndExpenses(req.body)
  }
  if(errors && errors.error && Array.isArray(errors.error.details) && errors.error.details.length) {
    return res.status(422).json(errors.error.details)
  }
  return next()
}
