import mongoose from "mongoose"
import timestamps from "mongoose-timestamp"
import {EntityUser} from "../../models"
import {Docs} from "../../docs/docs.model"

const {Schema} = mongoose

const auditLogs = Schema({
  superVisorModule: String, // General Admin Activities,
  superVisorSubSection: String, // Documents
  userId: {
    type: Schema.Types.ObjectId,
    ref: EntityUser
  },
  userName: String,
  log: String,
  ip: String,
  date: Date
})

const giftDocumentShema = Schema({
  docCategory: String,
  docSubCategory: String,
  docType: String,
  docAWSFileLocation: {
    type: Schema.Types.ObjectId,
    ref: Docs
  },
  docFileName: String,
  docStatus: String, // WIP, send for review and other flags
  docActions: [
    {
      actionType: String,
      actionDate: Date
    }
  ],
  docNote:String,
  createdBy: {
    type: Schema.Types.ObjectId,
    ref: EntityUser
  },
  createdUserName: String,
  lastUpdatedDate: {
    type: Date,
    required: true,
    default: Date.now
  }
})

const giftsAndGrautitiesDisclosureSchema = new Schema({
  finAdvisorEntityId: Schema.Types.ObjectId,
  finAdvisorEntityName: String,
  year: Number, // Store this as an Integer
  quarter: String, // Store 1,2,,4
  entityOrUserFlag: Boolean, // Entity Id or User Id
  userOrEntityId: Schema.Types.ObjectId,
  userFirstName: String,
  userLastName: String,
  entityName: String,
  noDisclosure: Boolean,
  preApprovalSought: String,
  preApprovalDate: String,
  submitter: String,
  controlName: String,
  controlId: String,
  taskRefId: String,
  cac: {type:Boolean,default:false},
  // Flip this to false if the User wants to make a disclosure other this will be false
  controlEffectiveDate:Date,
  userOrEntityHasNoGiftsToDiscloseForquarter: {
    type: Boolean,
    default: false
  }, // Default to False

  affirmationNotes: String,
  notes: [{
    userName: String,
    note: String,
    supervisor: Boolean,
    createdDate: { type: Date, required: true, default: Date.now },
  }],
  status: String,
  discloseType: String,
  actionBy:String,
  G20Obligation: Boolean,
  giftsToRecipients:[
    {
      recipientEntityId:Schema.Types.ObjectId,
      recipientEntityName:String,
      name:String,
      recipientUserId:Schema.Types.ObjectId,
      recipientUserFirstName:String,
      recipientUserLastName:String,
      createdUserId:Schema.Types.ObjectId,
      createdUserName:String,
      giftValue:Number,
      giftDate:Date,
      createdDate: {
        type: Date,
        required: true,
        default: Date.now
      },
    }
  ],
  // Gifts and Gratuities Not Subject to General Limitation
  giftsNotSubjectToGenLimitation:{type:Boolean, default:false},

  // Prohibition of Use of Offering Proceeds Exclusion
  giftsOfferingProceedsExclusion:{type:Boolean, default:false},

  // Permitted non-cash compensation arrangements
  giftsPermittedCompensationArrangements:{type:Boolean, default:false},

  // m that I/we did not directly or indirectly accept or make payments or offers of payments of any non-cash compensation in connection with the sale and distribution of a primary offering of municipal securities
  giftsMuncipalSecurities:{type:Boolean, default:false},

  //  I/we confirm that I/we have read and understood Prohibition of Use of Offering Proceeds as defined in Rule G-20 section (e). At the time of making this disclosure, I/we are not aware of instances where I/we are requesting or obtaining reimbursement of costs and expenses related to the entertainment of any person, including, but not limited to, any official or other personnel of the municipal entity or personnel of the obligated person, from the proceeds of such offering of municipal securities as defined in Rule G-20 section (e).
  ruleG20Section:{type:Boolean, default:false},

  giftsAffirmCompletionOfG20ForPeriod:{type:Boolean, default:false},

  giftsPayments:{type:Boolean, default:false},

  giftsRuleG20:{type:Boolean, default:false},

  giftDisclosureDocuments: [giftDocumentShema],
  auditLogs: [auditLogs],
  lastUpdateDate: {
    type: Date,
    required: true,
    default: Date.now
  },

}).plugin(timestamps)

export const GiftsAndGratuities = mongoose.model("giftsAndGrautities", giftsAndGrautitiesDisclosureSchema)
