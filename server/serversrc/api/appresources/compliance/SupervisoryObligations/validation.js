import Joi from "joi-browser"

const responsibleSchema = Joi.object().keys({
  userId: Joi.string().required(),
  name: Joi.string().required(),
  userEntityId: Joi.string().allow("").optional(),
  userFirstName: Joi.string().allow("").optional(),
  userLastName: Joi.string().allow("").optional(),
  userPrimaryEmailId: Joi.string().allow("").optional(),
  userPrimaryPhone: Joi.string().allow("").optional(),
  userComplianceDesignation: Joi.string().required(),
  userRoleFirm: Joi.string().allow("").optional(),
  /* userActive: Joi.string().allow("").optional(), */
  startDate: Joi.date().example(new Date("2005-01-01")).allow("", null).optional(),
  endDate: Joi.date().example(new Date("2005-01-01")).min(Joi.ref("startDate")).allow("", null).optional(),
  _id: Joi.string().required().optional(),

})

export const ResponsibleSupervisoryValidate = (inputTransDistribute) => Joi.validate(inputTransDistribute, responsibleSchema, { abortEarly: false, stripUnknown:false })

const annualSchema = Joi.object().keys({
  certifiedBy: Joi.object({
    id: Joi.string().required().optional(),
    name: Joi.string().required().optional(),
    userId: Joi.string().required(),
    userFirstName: Joi.string().required(),
    userLastName: Joi.string().required(),
    userEntityId: Joi.string().required(),
    userPrimaryEmailId: Joi.string().required(),
  }).required(),
  certifiedOn: Joi.date().required(),
  certifierDesignation: Joi.string().required(),
  docFileName: Joi.string().required(),
  docNotes: Joi.string().allow("").optional(),
  docAWSFileLocation: Joi.string().required(),
  docCategory : Joi.string().required(),
  docSubCategory: Joi.string().required(),
  createdDate: Joi.date().required().optional(),
  _id: Joi.string().required().optional(),
})

export const AnnualCertificationValidate = (inputTransDistribute) => Joi.validate(inputTransDistribute, annualSchema, { abortEarly: false, stripUnknown:false })


export const errorHandling = (req, res, next) => {
  const { type } = req.query
  let errors
  if(type === "responsibleSupervision") {
    errors = ResponsibleSupervisoryValidate(req.body)
  }
  if(type === "annualCertification") {
    errors = AnnualCertificationValidate(req.body)
  }
  if(errors && errors.error && Array.isArray(errors.error.details) && errors.error.details.length) {
    return res.status(422).json(errors.error.details)
  }
  return next()
}
