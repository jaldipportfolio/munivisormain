import mongoose from "mongoose"
import timestamps from "mongoose-timestamp"
import { EntityUser } from "./../../models"
import {Docs} from "../../docs/docs.model"

const { Schema } = mongoose

const auditLogs = Schema({
  superVisorModule: String, // General Admin Activities,
  superVisorSubSection: String, // Documents
  userId: { type: Schema.Types.ObjectId, ref: EntityUser },
  userName: String,
  log: String,
  ip: String,
  date: Date
})

const superVisorDocumentSchema =Schema({
  docCategory:String,
  docSubCategory:String,
  docType:String,
  docAWSFileLocation: {type: Schema.Types.ObjectId, ref: Docs},
  docFileName:String,
  docStatus:String, // WIP, send for review and other flags
  docNote:String,
  docActions:[{
    actionType:String,
    actionDate:Date
  }],
  createdBy: {type: Schema.Types.ObjectId, ref: EntityUser},
  createdUserName: String,
  lastUpdatedDate: { type: Date, required: true, default: Date.now }
})

const supervisoryObligations = new Schema({
  finAdvisorEntityId:Schema.Types.ObjectId,
  finAdvisorEntityName:String,
  responsibleSupervision:[{
    userId:Schema.Types.ObjectId,
    name:String,
    userFirstName:String,
    userEntityId:Schema.Types.ObjectId,
    userLastName:String,
    userPrimaryEmailId:String,
    userPrimaryPhone:String,
    userRoleFirm:String,
    userComplianceDesignation:String,
    userActive:String,
    startDate: String,
    endDate: String
  }],
  annualCertification:[{
    certifiedOn:Date,
    certifiedBy:{
      userId:Schema.Types.ObjectId,
      userFirstName:String,
      userEntityId:Schema.Types.ObjectId,
      userLastName:String,
      userPrimaryEmailId:String,
    },
    certifierDesignation:String,
    docCategory:String, // Default to "Supervisory Obligations"
    docSubCategory:String, // Default to "Annual Certification"
    docAWSFileLocation:String,
    docFileName:String,
    docNotes: String,
    createdDate: { type: Date, required: true, default: Date.now },
  }],
  supervisoryObligationsDocuments:[superVisorDocumentSchema],
  auditLogs:[auditLogs],
  lastUpdateDate:{type:Date,required: true,default: Date.now}
}).plugin(timestamps)

export const SuperVisoryObligations = mongoose.model("supervisoryobligations", supervisoryObligations)
