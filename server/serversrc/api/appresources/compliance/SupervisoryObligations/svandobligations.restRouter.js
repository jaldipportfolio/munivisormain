import express from "express"
import superVisoryObligationsController from "./svandobligations.controller"
import { errorHandling } from "./validation"
import {requireAuth} from "./../../../../authorization/authsessionmanagement/auth.middleware"

export const superVisoryObligationsRouter = express.Router()

superVisoryObligationsRouter.param("id", superVisoryObligationsController.findByParam)

superVisoryObligationsRouter.route("/")
  .get(requireAuth,superVisoryObligationsController.getAll)
  .post(requireAuth, superVisoryObligationsController.createOne)

superVisoryObligationsRouter.route("/svorcertifications")
  .get(requireAuth, superVisoryObligationsController.getSuperVisoryObligationsDetails)
  .put(requireAuth, errorHandling, superVisoryObligationsController.putSvcoSupervisionCertifications)

superVisoryObligationsRouter.route("/svorcertifications/:pullId")
  .delete(requireAuth, superVisoryObligationsController.pullSvcoSupervisionCertifications)

superVisoryObligationsRouter.route("/documents")
  .put(requireAuth, superVisoryObligationsController.putSvcoDocuments)
  .delete(requireAuth, superVisoryObligationsController.pullSvcoDocuments)

superVisoryObligationsRouter.route("/auditlogs")
  .post(requireAuth, superVisoryObligationsController.putAuditLogs)
