import Joi from "joi-browser"

const entityByStateSchema = Joi.object().keys({
  nameOfMuniEntity: Joi.string().allow(""),
  nameOfMuniEntityID: Joi.string().allow("").optional(),
  titleOfMuniEntity: Joi.string().allow(""),
  state: Joi.string().allow(""),
  city: Joi.string().allow(""),
  county: Joi.string().allow(""),
  otherPoliticalSubdiv: Joi.string().allow(""),
  contributorCategory : Joi.string().allow(""),
  contributionAmount : Joi.number().allow("", null).required().optional(),
  contributionDate : Joi.date().allow([null,""]).example(new Date("2005-01-01")).allow("", null).optional(),
  exempted : Joi.boolean().required().optional(),
  checkNone : Joi.boolean().required().optional(),
  dateOfExemption : Joi.date().allow([null,""]).example(new Date("2005-01-01")).allow("", null).optional(),
  createdDate : Joi.date().allow([null,""]).example(new Date("2005-01-01")).allow("", null).optional(),
  _id: Joi.string().required().optional(),
})

export const EntityByStatePolContribution = (inputTransDistribute) => Joi.validate(inputTransDistribute, entityByStateSchema, { abortEarly: false, stripUnknown:false })

const paymentSchema = Joi.object().keys({
  state: Joi.string().allow(""),
  city: Joi.string().allow(""),
  county: Joi.string().allow(""),
  politicalParty: Joi.string().allow(""),
  contributorCategory: Joi.string().allow(""),
  contributionAmount: Joi.number().allow("", null).required().optional(),
  contributionDate: Joi.date().allow([null,""]).example(new Date("2005-01-01")).allow("", null).optional(),
  checkNone : Joi.boolean().required().optional(),
  createdDate: Joi.date().allow([null,""]).example(new Date("2005-01-01")).allow("", null).optional(),
  _id: Joi.string().required().optional(),
})

export const PaymentPolPartiesValidate = (inputTransDistribute) => Joi.validate(inputTransDistribute, paymentSchema, { abortEarly: false, stripUnknown:false })

const ballotSchema = Joi.object().keys({
  nameOfBondBallot: Joi.string().allow(""),
  nameOfIssueMuniEntityID: Joi.string().allow(""),
  nameOfIssueMuniEntity: Joi.string().allow(""),
  state: Joi.string().allow(""),
  city: Joi.string().allow(""),
  county: Joi.string().allow(""),
  otherPoliticalSubdiv: Joi.string().allow(""),
  contributorCategory: Joi.string().allow(""),
  contributionAmount: Joi.number().allow("", null).required().optional(),
  contributionDate: Joi.date().allow([null,""]).example(new Date("2005-01-01")).allow("", null).optional(),
  reimbursements: Joi.string().allow(""),
  reimbursementsAmount: Joi.number().allow("", null).required().optional(),
  checkNone : Joi.boolean().required().optional(),
  createdDate: Joi.date().allow([null,""]).example(new Date("2005-01-01")).allow("", null).optional(),
  _id: Joi.string().required().optional(),
})

export const BallotContribValidate = (inputTransDistribute) => Joi.validate(inputTransDistribute, ballotSchema, { abortEarly: false, stripUnknown:false })

const reimbursementsSchema = Joi.object().keys({
  /* userId: Joi.string().allow(""), */
  state: Joi.string().required(),
  ballotDetail: Joi.string().required(),
  /* thirdPartyDetails: Joi.object({
    userId: Joi.string().allow(""),
    userFirstName: Joi.string().required(),
    userLastName: Joi.string().required(),
    userAddressConsolidated: Joi.string().required(),
  }).required(), */
  userAddressConsolidated: Joi.string().required(),
  thirdPartyCategory: Joi.string().required(),
  reimbursementAmt: Joi.number().required(),
  reimbursementDate: Joi.date().required().optional(),
  createdDate: Joi.date().required().optional(),
  _id: Joi.string().required().optional(),
})

export const ReimbursementsValidate = (inputTransDistribute) => Joi.validate(inputTransDistribute, reimbursementsSchema, { abortEarly: false, stripUnknown:false })

const ballotApprovedSchema = Joi.object().keys({
  entityId: Joi.string().allow(""),
  municipalEntityName: Joi.string().required(),
  processDescription: Joi.string().required(),
  reportablesaleDate: Joi.date().required().optional(),
  createdDate: Joi.date().required().optional(),
  _id: Joi.string().required().optional(),
})

export const BallotApprovedContribValidate = (inputTransDistribute) => Joi.validate(inputTransDistribute, ballotApprovedSchema, { abortEarly: false, stripUnknown:false })

const contributorDisclosureSchema = Joi.object().keys({
  entityId: Joi.string().required(),
  entityName: Joi.string().required(),
  userId: Joi.string().allow("").required(),
  userFirstName: Joi.string().required(),
  userMiddleName: Joi.string().required(),
  userLastName: Joi.string().required(),
  userPrimaryEmailId: Joi.string().email().required(),
  userPrimaryPhone: Joi.string().required(),
  userFax: Joi.string().required(),
  userAddressConsolidated: Joi.object().keys({
    country: Joi.string().required(),
    state: Joi.string().required(),
    city: Joi.string().required(),
    zipCode : {
      zip1: Joi.string().required(),
      zip2: Joi.string().required()
    },
    addressName : Joi.string().required(),
    addressType : Joi.string().allow("").required(),
    addressLine1 : Joi.string().required(),
    addressLine2 : Joi.string().required(),
  }).required(),
  _id: Joi.string().required().optional(),
})

export const ContributorDisclosureValidate = (inputTransDistribute) => Joi.validate(inputTransDistribute, contributorDisclosureSchema, { abortEarly: false, stripUnknown:false })

export const contributorErrorHandling = (req, res, next) => {
  const errors = ContributorDisclosureValidate(req.body)
  if(errors && errors.error && Array.isArray(errors.error.details) && errors.error.details.length) {
    return res.status(422).json(errors.error.details)
  }
  return next()
}

export const errorHandling = (req, res, next) => {
  const { type } = req.query
  let errors
  if(type === "contribMuniEntityByState") {
    errors = EntityByStatePolContribution(req.body)
  }
  if(type === "paymentsToPartiesByState") {
    errors = PaymentPolPartiesValidate(req.body)
  }
  if(type === "ballotContribByState") {
    errors = BallotContribValidate(req.body)
  }
  if(type === "reimbursements") {
    errors = ReimbursementsValidate(req.body)
  }
  if(type === "ballotApprovedOfferings") {
    errors = BallotApprovedContribValidate(req.body)
  }
  if(errors && errors.error && Array.isArray(errors.error.details) && errors.error.details.length) {
    return res.status(422).json(errors.error.details)
  }
  return next()
}
