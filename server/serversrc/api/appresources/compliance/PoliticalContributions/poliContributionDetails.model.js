import mongoose from "mongoose"
import timestamps from "mongoose-timestamp"
import { EntityUser } from "./../../models"
import { PoliticalContributions } from "./policontributions.model"
import {Docs} from "../../docs/docs.model"

const { Schema } = mongoose

const auditLogs = Schema({
  superVisorModule: String, // General Admin Activities,
  superVisorSubSection: String, // Documents
  userId: { type: Schema.Types.ObjectId, ref: EntityUser },
  userName: String,
  log: String,
  ip: String,
  date: Date
})

const superVisorDocumentSchema =Schema({
  docCategory:String,
  docSubCategory:String,
  docType:String,
  docAWSFileLocation: {type: Schema.Types.ObjectId, ref: Docs},
  docFileName:String,
  docStatus:String, // WIP, send for review and other flags
  docActions:[{
    actionType:String,
    actionDate:Date
  }],
  createdBy: {type: Schema.Types.ObjectId, ref: EntityUser},
  createdUserName: String,
  lastUpdatedDate: { type: Date, required: true, default: Date.now }
})

const politicalContributionsByUsers = new Schema({
  contributeId: { type: Schema.Types.ObjectId, ref: PoliticalContributions },
  finAdvisorEntityId: { type: Schema.Types.ObjectId, ref: PoliticalContributions },
  finAdvisorEntityName: String,
  year: Number,
  quarter: Number,
  submitter: String,
  controlName: String,
  preApprovalSought: String,
  entityOrUserFlag: Boolean, // Entity Id or User Id
  userOrEntityId:Schema.Types.ObjectId,
  userFirstName:String,
  userLastName:String,
  entityName:String,
  userHasPoliticalContribForQuarter:{type:Boolean,default:false}, // Default to False
  controlId: String,
  controlEffectiveDate:Date,
  taskRefId: String,
  noDisclosure: Boolean,
  cac: {type:Boolean,default:false},
  affirmationNotes:String,
  actionBy:String,
  notes: [{
    userName: String,
    note: String,
    supervisor: Boolean,
    createdDate: { type: Date, required: true, default: Date.now },
  }],
  status: String,
  discloseType: String,
  G37Obligation:Boolean,
  contribMuniEntityByState:[{
    nameOfMuniEntityID: { type: Schema.Types.ObjectId },
    nameOfMuniEntity: String,
    titleOfMuniEntity: String,
    state: String,
    city: String,
    county: String,
    otherPoliticalSubdiv: String,
    contributorCategory : String,
    contributionAmount : Number,
    contributionDate : Date,
    exempted : Boolean,
    checkNone : Boolean,
    dateOfExemption : Date,
    createdDate: { type: Date, required: true, default: Date.now }
  }],
  paymentsToPartiesByState:[{
    state: String,
    city: String,
    county: String,
    politicalParty: String,
    contributorCategory: String,
    contributionAmount: Number,
    contributionDate: Date,
    checkNone : Boolean,
    createdDate: { type: Date, required: true, default: Date.now }
  }],
  ballotContribByState:[{
    nameOfBondBallot: String,
    nameOfIssueMuniEntityID: { type: Schema.Types.ObjectId },
    nameOfIssueMuniEntity: String,
    state: String,
    city: String,
    county: String,
    otherPoliticalSubdiv: String,
    contributorCategory: String,
    contributionAmount: Number,
    contributionDate: Date,
    reimbursements: String,
    reimbursementsAmount: Number,
    checkNone : Boolean,
    createdDate: { type: Date, required: true, default: Date.now }
  }],
  contributionsRelatedDocuments:[superVisorDocumentSchema],
  auditLogs:[auditLogs],
}).plugin(timestamps)

export const PoliContributionDetails = mongoose.model("poliContributionDetails", politicalContributionsByUsers)
