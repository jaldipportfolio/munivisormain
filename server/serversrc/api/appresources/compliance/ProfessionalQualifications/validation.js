import Joi from "joi-browser"

const pqQualifiedPersons = Joi.object().keys({
  qualification: Joi.string().required(),
  userId: Joi.string().required(),
  userFirstName: Joi.string().allow("").optional(),
  userLastName: Joi.string().allow("").optional(),
  userPrimaryEmailId: Joi.string().email().allow("").optional(),
  userPrimaryPhone: Joi.string().allow("").optional(),
  // userOrgRole: Joi.string().required(),
  profFeePaidOn: Joi.date().required(),
  userSeries50PassedOnDate: Joi.date().example(new Date("2016-01-01")).allow(""),
  userSeries50ValidEndDate: Joi.date().example(new Date("2016-01-01")).allow(""),
  createdDate: Joi.date().required().optional(),
  _id: Joi.string().required().optional(),
})

export const PqQualifiedPersonsValidate = (inputTransDistribute) => Joi.validate(inputTransDistribute, pqQualifiedPersons, { abortEarly: false, stripUnknown:false })

const pqTrainingPrograms = Joi.object().keys({
  trgProgramName: Joi.string().required(),
  trgOrganizationDate: Joi.date().allow(""),
  trgConductedBy: Joi.object({
    id: Joi.string().required().optional(),
    name: Joi.string().required().optional(),
    userId: Joi.string().required(),
    userFirstName: Joi.string().required(),
    userLastName: Joi.string().required(),

  }).required(),
  trgAttendees: Joi.array().min(1).required(),
  trgNotes: Joi.string().required(),
  createdDate: Joi.date().required().optional(),
  _id: Joi.string().required().optional(),
})

export const PqTrainingProgramsValidate = (inputTransDistribute) => Joi.validate(inputTransDistribute, pqTrainingPrograms, { abortEarly: false, stripUnknown:false })

const pqTrainingDetail = Joi.object().keys({
  topic: Joi.string().required().optional(),
  programName: Joi.string().required().optional(),
  trainingDate: Joi.date().required().optional(),
  conductedOn: Joi.date().required().optional(),
  conductedBy: Joi.string().allow("").optional(),
  AWSFileLocation: Joi.string().allow("").optional(),
  fileName: Joi.string().allow("").optional(),
  attendees: Joi.array().required().optional(),
  notes: Joi.string().allow("").optional(),
  _id: Joi.string().required().optional(),
})

export const PqTrainingDetailValidate = (inputTransDistribute) => Joi.validate(inputTransDistribute, pqTrainingDetail, { abortEarly: false, stripUnknown:false })


export const errorHandling = (req, res, next) => {
  const { type } = req.query
  let errors
  if(type === "pqQualifiedPersons") {
    errors = PqQualifiedPersonsValidate(req.body)
  }
  if(type === "pqTrainingPrograms") {
    errors = PqTrainingProgramsValidate(req.body)
  }
  if(type === "pqTrainingDetail") {
    errors = PqTrainingDetailValidate(req.body)
  }
  if(errors && errors.error && Array.isArray(errors.error.details) && errors.error.details.length) {
    return res.status(422).json(errors.error.details)
  }
  return next()
}
