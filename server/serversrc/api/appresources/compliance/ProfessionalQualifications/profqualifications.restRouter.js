import express from "express"
import profQualificationsController from "./profqualifications.controller"
import { errorHandling } from "./validation"
import {requireAuth} from "./../../../../authorization/authsessionmanagement/auth.middleware"

export const profQualificationsRouter = express.Router()

profQualificationsRouter.param("id", profQualificationsController.findByParam)

profQualificationsRouter.route("/")
  .get(requireAuth,profQualificationsController.getAll)
  .post(requireAuth, profQualificationsController.createOne)

profQualificationsRouter.route("/personsortraining")
  .get(requireAuth, profQualificationsController.getProfQualificationsDetails)
  .put(requireAuth, errorHandling, profQualificationsController.putProfQualificationsPersonsOrTraining)

profQualificationsRouter.route("/userseriesdate/:userId")
  .get(requireAuth, profQualificationsController.getSeriesPassedAndValid)

profQualificationsRouter.route("/personsortraining/:pullId")
  .delete(requireAuth, profQualificationsController.pullProfQualificationsPersonsOrTraining)

profQualificationsRouter.route("/documents")
  .put(requireAuth, profQualificationsController.putProfQualificationsDocuments)
  .delete(requireAuth, profQualificationsController.pullDocuments)

profQualificationsRouter.route("/auditlogs")
  .post(requireAuth, profQualificationsController.putAuditLogs)
