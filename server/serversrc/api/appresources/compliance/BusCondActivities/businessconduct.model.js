import mongoose from "mongoose"
import timestamps from "mongoose-timestamp"
import { EntityUser } from "./../../models"

const { Schema } = mongoose

const auditLogs = Schema({
  superVisorModule: String, // General Admin Activities,
  superVisorSubSection: String, // Documents
  userId: { type: Schema.Types.ObjectId, ref: EntityUser },
  userName: String,
  log: String,
  ip: String,
  date: Date
})

const busCondActivities = new Schema({
  finAdvisorEntityId:Schema.Types.ObjectId,
  finAdvisorEntityName:String,
  bcaContraDetails:[{
    /* userId: Schema.Types.ObjectId,
    userFirstName:String,
    userLastName:String,
    userPrimaryEmailId:String,
    userPrimaryPhone:String, */
    violationType:String,
    violataionDate:Date,
    violationNotes: String,
    recordDate:Date,
    closeDate:Date,
    assPersonName: String,
    docAWSFileLocation: String,
    docFileName: String,
    createdDate: { type: Date, required: true, default: Date.now },
  }],
  lastUpdateDate:{type:Date,default: Date.now},
  auditLogs:[auditLogs]
}).plugin(timestamps)

export const BusConduct = mongoose.model("buscondactivities", busCondActivities)
