import {generateControllers} from "./../../../modules/query"
import {BusConduct} from "./businessconduct.model"
import {Entity} from "../../entity/entity.model"

const {ObjectID} = require("mongodb")


const getBusinessConductDetails = () => async (req, res) => {
  const resRequested = [
    {resource: "BusConduct", access: 1},
  ]
  try {
    const businessConduct = await BusConduct.findOne({finAdvisorEntityId: req.user.entityId})
    res.json(businessConduct)
  } catch (error) {
    res.status(422).send({
      done: false,
      error: "There is a failure in retrieving information for entitlements",
      success: "",
      requestedServices: resRequested
    })
  }
}

const putBusinessConduct = () => async (req, res) => {
  const resRequested = [
    {resource:"BusConduct",access:2},
    {resource:"Entity",access:2},
  ]

  try {
    let businessConduct = await BusConduct.findOne({finAdvisorEntityId: req.user.entityId})
    const bcaContraDetails = []
    const entity = await Entity.findOne({_id: req.user.entityId}).select("firmName msrbFirmName msrbRegistrantType")
    const {_id } = req.body

    if(businessConduct) {
      if(_id) {
        await BusConduct.updateOne(
          {finAdvisorEntityId: ObjectID(req.user.entityId), "bcaContraDetails._id": ObjectID(_id)},
          {$set: {"bcaContraDetails.$": req.body}}
        )
      }else {
        await BusConduct.updateOne(
          {finAdvisorEntityId: ObjectID(req.user.entityId)},
          {$addToSet: {bcaContraDetails: req.body} }
        )
      }
      // await BusConduct.update({ finAdvisorEntityId: req.user.entityId}, {$addToSet: {bcaContraDetails: req.body} })
    }else {
      bcaContraDetails.push(req.body)
      const newBusConduct = new BusConduct({
        finAdvisorEntityId: req.user.entityId,
        finAdvisorEntityName: (entity && entity.firmName) || "",
        bcaContraDetails
      })
      await newBusConduct.save()
    }

    businessConduct = await BusConduct.findOne({finAdvisorEntityId: req.user.entityId}).select("finAdvisorEntityId bcaContraDetails")

    res.json(businessConduct)

  } catch(error) {

    console.log("======================", error)
    res.status(422).send({done:false,error:"There is a failure in retrieving information for entitlements",success:"",requestedServices:resRequested})

  }
}

const pullBusinessConduct = () => async (req, res) => {
  const { bcaId } = req.params
  const resRequested = [
    {resource:"BusConduct",access:2},
  ]
  try {
    await BusConduct.update({ finAdvisorEntityId: req.user.entityId}, { $pull: { "bcaContraDetails": { _id: bcaId } }})
    const businessConduct = await BusConduct.findOne({ finAdvisorEntityId: req.user.entityId}).select("finAdvisorEntityId bcaContraDetails")
    res.json(businessConduct)
  } catch(error) {
    res.status(422).send({done:false,error:"There is a failure in retrieving information for entitlements",success:"",requestedServices:resRequested})
  }
}

const putAuditLogs = () => async (req, res) => {
  const resRequested = [
    {resource:"BusConduct",access:2},
  ]
  try {
    await BusConduct.update({ finAdvisorEntityId: req.user.entityId}, {$addToSet: {auditLogs: req.body} })
    const businessConduct = await BusConduct.findOne({ finAdvisorEntityId: req.user.entityId}).select("auditLogs")
    res.json(businessConduct)
  } catch(error) {
    res.status(422).send({done:false,error:"There is a failure in retrieving information for entitlements",success:"",requestedServices:resRequested})
  }
}

export const pullBusinessDocuments = () => async (req, res) => {
  const { docId } = req.query
  try {
    if(docId) {
      await BusConduct.updateOne(
        {
          finAdvisorEntityId: req.user.entityId,
          "bcaContraDetails._id": docId
        },
        { $set: { "bcaContraDetails.$.docAWSFileLocation": "", "bcaContraDetails.$.docFileName": ""}}
      )
      const document = await BusConduct.findOne({ finAdvisorEntityId: req.user.entityId}).select("bcaContraDetails")
      res.status(200).json(document)
    }else {
      res.status(422).send({done:false,error:"This Document doesn't exists."})
    }
  } catch(error) {
    console.log("==============error=============>", error.message)
    res.status(422).send({done:false, error: error.message})
  }
}

export default generateControllers(BusConduct, {
  getBusinessConductDetails: getBusinessConductDetails(),
  putBusinessConduct: putBusinessConduct(),
  pullBusinessConduct: pullBusinessConduct(),
  putAuditLogs: putAuditLogs(),
  pullBusinessDocuments: pullBusinessDocuments()
})
