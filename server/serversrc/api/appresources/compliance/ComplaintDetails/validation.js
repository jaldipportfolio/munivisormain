import Joi from "joi-browser"

const keyDetailsSchema = Joi.object().keys({
  complaintsName: Joi.string().required(),
  organizationName: Joi.string().required(),
  assPersonName: Joi.string().required(),
  docAWSFileLocation: Joi.string().allow("").optional(),
  docFileName: Joi.string().allow("").optional(),
  productCode: Joi.string().required(),
  problemCode: Joi.string().required(),
  complaintStatus: Joi.string().required(),
  dateReceived: Joi.date().allow([null,""]).example(new Date("2005-01-01")).allow("", null).optional(),
  dateOfActivity: Joi.date().allow([null,""]).example(new Date("2005-01-01")).allow("", null).optional(),
  activityDetails: Joi.array().min(0).required().optional(),
  complaintDescription: Joi.string().allow("").optional(),
  actionAgainstComplaint: Joi.string().allow("").optional(),
  createdDate: Joi.date().required().optional(),
  _id: Joi.string().required().optional(),
})

export const KeyDetailsValidate = (inputTransDistribute) => Joi.validate(inputTransDistribute, keyDetailsSchema, { abortEarly: false, stripUnknown:false })

const complainantDetailsSchema = Joi.object().keys({
  entityId: Joi.string().required(),
  entityName: Joi.string().required(),
  userId: Joi.string().allow("").required(),
  userFirstName: Joi.string().required(),
  userMiddleName: Joi.string().allow(""),
  userLastName: Joi.string().required(),
  userPrimaryEmailId: Joi.string().email().required(),
  userPrimaryPhone: Joi.string().required(),
  userFax: Joi.string().required(),
  userAddressConsolidated: Joi.object().keys({
    country: Joi.string().required(),
    state: Joi.string().required(),
    city: Joi.string().required(),
    zipCode : {
      zip1: Joi.string().required(),
      zip2: Joi.string().required()
    },
    addressName : Joi.string().required(),
    addressType : Joi.string().allow("").required(),
    addressLine1 : Joi.string().required(),
    addressLine2 : Joi.string().allow("").optional(),
  }).required(),
  _id: Joi.string().required().optional(),
})

export const ComplainantDetailsValidate = (inputTransDistribute) => Joi.validate(inputTransDistribute, complainantDetailsSchema, { abortEarly: false, stripUnknown:false })

const assocPersonDetailsSchema = Joi.object().keys({
  userId: Joi.string().allow(""),
  userFirstName: Joi.string().required(),
  userMiddleName: Joi.string().required(),
  userLastName: Joi.string().required(),
  userPrimaryEmailId: Joi.string().email().required(),
  userPrimaryPhone: Joi.string().required(),
  userEntityId: Joi.string().allow("").optional(),
  userEntityName: Joi.string().required(),
  userPrimaryAddress: Joi.string().required(),
  _id: Joi.string().required().optional(),
})

export const AssocPersonDetailsValidate = (inputTransDistribute) => Joi.validate(inputTransDistribute, assocPersonDetailsSchema, { abortEarly: false, stripUnknown:false })

export const errorHandling = (req, res, next) => {
  const { type } = req.params
  let errors
  if(type === "keyDetails") {
    errors = KeyDetailsValidate(req.body)
  }
  if(type === "complainantDetails") {
    errors = ComplainantDetailsValidate(req.body)
  }
  if(type === "complainantAssocPersonDetails") {
    errors = AssocPersonDetailsValidate(req.body)
  }
  if(errors && errors.error && Array.isArray(errors.error.details) && errors.error.details.length) {
    return res.status(422).json(errors.error.details)
  }
  return next()
}
