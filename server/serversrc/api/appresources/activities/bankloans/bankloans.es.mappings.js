module.exports = {
  "properties": {
    "actTranType": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranSubType": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranUniqIdentifier": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranFirmId": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranFirmName": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranClientId": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranClientName": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranClientMsrbType": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranIsClientMsrbRegistered": {
      "type": "boolean"
    },
    "actTranIsConduitBorrowerFlag": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranPrimarySector": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranSecondarySector": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranFirmLeadAdvisorId": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranFirmLeadAdvisorName": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranIssueName": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranProjectDescription": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "actTranRelatedTo": {
      "properties": {
        "relTranId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "relTranIssueName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "relTranProjectName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "relTranClientId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "relTranClientName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "relType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        }
      }
    },
    "actTranNotes": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "bankLoanCompSolicitation": {
      "type": "boolean"
    },
    "bankLoanSummary": {
      "properties": {
        "actTranStatus": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        // "actTranSecondarySector": {
        //   "type": "text",
        //   "fields": {
        //     "raw": {
        //       "type": "keyword"
        //     },
        //     "searchable": {
        //       "type": "text",
        //       "analyzer": "searchableText"
        //     }
        //   }
        // },
        // "actTranPrimarySector": {
        //   "type": "text",
        //   "fields": {
        //     "raw": {
        //       "type": "keyword"
        //     },
        //     "searchable": {
        //       "type": "text",
        //       "analyzer": "searchableText"
        //     }
        //   }
        // },
        "actTenorMaturities": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "actTranBorrowerName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "actTranObligorName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "actTranState": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "actTranCounty": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "actTranClosingDate": {
          "type": "date"
        },
        "actTranUseOfProceeds": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "actTranType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        }
      }
    },
    "bankLoanTerms": {
      "properties": {
        "stateTax": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "bankQualified": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "parAmount": {
          "type": "float"
        },
        "fedTax": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "paymentType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "prinPaymentFreq": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "prinPaymentStartDate": {
          "type": "date"
        },
        "prinPaymentDay": {
          "type": "float"
        },
        "intPaymentFreq": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "intPaymentStartDate": {
          "type": "date"
        },
        "intPaymentDay": {
          "type": "float"
        },
        "fixedRate": {
          "type": "float"
        },
        "floatingRateRef": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "floatingRateRatio": {
          "type": "float"
        },
        "floatingRateSpread": {
          "type": "float"
        },
        "dayCount": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "busConvention": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "bankHolidays": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "prePaymentTerms": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        }
      }
    },
    "bankLoanLinkCusips": {
      "properties": {
        "cusip": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "description": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "tag": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        }
      }
    },
    "bankLoanParticipants": {
      "properties": {
        "partType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "partFirmId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "partFirmName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "partContactId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "partContactName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "partContactEmail": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "partUserAddress": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "partContactAddrLine1": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "partContactAddrLine2": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "participantState": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "partContactPhone": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "partContactTitle": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "partContactAddToDL": {
          "type": "boolean"
        }
      }
    },
    "bankLoanChecklists": {
      "properties": {
        "type": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "id": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "name": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "attributedTo": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "bestPractice": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "notes": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "lastUpdated": {
          "properties": {
            "date": {
              "type": "date"
            },
            "by": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            }
          }
        },
        "data": {
          "properties": {
            "title": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "headers": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            }
          }
        }
      }
    },
    "bankLoanAmort": {
      "properties": {
        "reductionDate": {
          "type": "date"
        },
        "prinAmountReduction": {
          "type": "float"
        },
        "reviedPrinAmount": {
          "type": "float"
        }
      }
    },
    "bankLoanDocuments": {
      "properties": {
        "docCategory": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "docSubCategory": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "docType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "docAWSFileLocation": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "docFileName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "markedPublic": {
          "properties": {
            "publicFlag": {
              "type": "boolean"
            },
            "publicDate": {
              "type": "date"
            }
          }
        },
        "sentToEmma": {
          "properties": {
            "emmaSendFlag": {
              "type": "boolean"
            },
            "emmaSendDate": {
              "type": "date"
            }
          }
        },
        "LastUpdatedDate": {
          "type": "date"
        }
      }
    },
    "banLoanRfpEvaluationTeam": {
      "properties": {
        "rfpSelEvalContactId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpSelEvalRealFirstName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpSelEvalRealLastName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpSelEvalRealEmailId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpSelEvalRole": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpSelEvalContactName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpSelEvalEmail": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpSelEvalPhone": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpSelEvalAddToDL": {
          "type": "boolean"
        }
      }
    },
    "banLoanRfpProcessContacts": {
      "properties": {
        "rfpProcessContactId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpProcessContactRealFirstName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpProcessContactRealLastName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpProcessContactRealEmailId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpContactFor": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpContactName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpContactEmail": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpContactPhone": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpContactAddToDL": {
          "type": "boolean"
        }
      }
    },
    "banLoanRfpParticipants": {
      "properties": {
        "rfpParticipantFirmId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpParticipantFirmName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpParticipantRealFirmName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpParticipantRealMSRBType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpParticipantContactId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpParticipantRealFirstName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpParticipantRealLastName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpParticipantRealEmailId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpParticipantContactName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpParticipantContactEmail": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpParticipantContactPhone": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpParticipantContactAddToDL": {
          "type": "boolean"
        }
      }
    },
    "banLoanRfpBidDocuments": {
      "properties": {
        "rfpBidDocName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpBidDocId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpBidDocType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpBidDocStatus": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpBidDocAction": {
          "properties": {
            "actionType": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "user": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "date": {
              "type": "date"
            },
            "userFirstName": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "userLastName": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "userEmailId": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            }
          }
        },
        "rfpBidDocUploadUser": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpBidDocUploadFirstName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpBidDocUploadLastName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpBidDocUploadEmailId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        }
      }
    },
    "banLoanRfpParticipantQuestions": {
      "properties": {
        "rfpPartUserFirstName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpPartUserLastName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpPartUserEmailId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpPartContactId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpPartQuestDocId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpPartQuestDetails": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpPartQuestPosts": {
          "properties": {
            "postDetails": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "postDate": {
              "type": "date"
            },
            "postVisibilityFilter": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "postUserFirstName": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "postUserLastName": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "postUserEmailId": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "postContactId": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "postDocument": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "postParentId": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "postVisibilityFlag": {
              "type": "boolean"
            }
          }
        }
      }
    },
    "banLoanRfpMemberEvaluations": {
      "properties": {
        "rfpParticipantFirmId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpEvaluationCommitteeMemberId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpParticipantFirmName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpParticipantMSRBType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpCommitteeMemberFirstName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpCommitteeMemberLastName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpCommitteeMemberEmailId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpEvaluationCategories": {
          "properties": {
            "categoryName": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "categoryItems": {
              "properties": {
                "evalItem": {
                  "type": "text",
                  "fields": {
                    "raw": {
                      "type": "keyword"
                    },
                    "searchable": {
                      "type": "text",
                      "analyzer": "searchableText"
                    }
                  }
                },
                "evalPriority": {
                  "type": "text",
                  "fields": {
                    "raw": {
                      "type": "keyword"
                    },
                    "searchable": {
                      "type": "text",
                      "analyzer": "searchableText"
                    }
                  }
                },
                "evalRating": {
                  "type": "float"
                }
              }
            }
          }
        },
        "evalNotes": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        }
      }
    },
    "banLoanRfpFinalEvaluations": {
      "properties": {
        "rfpParticipantFirmId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpEvaluationCategories": {
          "properties": {
            "categoryName": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "categoryAggregatedScore": {
              "type": "float"
            }
          }
        }
      }
    },
    "banLoanRfpFinalSelections": {
      "properties": {
        "rfpParticipantFirmId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpParticipantFirmName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpParticipantMSRBType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "rfpFinalContractDocuments": {
          "properties": {
            "rfpFinalContractName": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            },
            "rfpFinalContractLocation": {
              "type": "float"
            },
            "rfpFinalContractDocType": {
              "type": "text",
              "fields": {
                "raw": {
                  "type": "keyword"
                },
                "searchable": {
                  "type": "text",
                  "analyzer": "searchableText"
                }
              }
            }
          }
        },
        "evaluationComments": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "notesWinningBidder": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "documentId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "documentName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        }
      }
    },
    "banLoanRfpSupplierResponses": {
      "properties": {
        "PartContactId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "PartUserFirstName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "PartUserLastName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "PartUserEmailId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "DocId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "DatePosted": {
          "type": "date"
        },
        "CurrentAction": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "LastUpdatedDate": {
          "type": "date"
        },
        "Comment": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        }
      }
    }
  }
}
