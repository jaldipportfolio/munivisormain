import express from "express"

import othersController from "./others.controller"
import { requireAuth } from "../../../../authorization/authsessionmanagement/auth.middleware"

import {
  createErrorHandling,
  OthersErrorHandling,
  pricingErrorHandling,
} from "./validation"
import {checkClosedTransaction} from "../../commonservices/services.controller"

export const othersRouter = express.Router()


othersRouter.param("id", othersController.findByParam)

othersRouter.route("/")
  .get(requireAuth, othersController.getAll)
  .post(createErrorHandling, requireAuth, othersController.postOthersTransaction)

othersRouter.route("/:id")
  .get(requireAuth, othersController.getOne)
  .put(requireAuth, checkClosedTransaction, othersController.updateOne)
  .delete(requireAuth, checkClosedTransaction, othersController.deleteOne)

othersRouter.route("/related/:tranId")
  .put(requireAuth, checkClosedTransaction, othersController.putRelatedTran)

othersRouter.route("/transaction/:type/:tranId")
  .get(requireAuth, othersController.getOthersTransactionDetails)
  .put(OthersErrorHandling, requireAuth, checkClosedTransaction, othersController.updateTransaction)
  .delete(requireAuth, checkClosedTransaction, othersController.removeParticipantByPartId)

othersRouter.route("/rating/:type/:tranId")
  .put(requireAuth, checkClosedTransaction, othersController.saveRatingsByTranId)
  .delete(requireAuth, checkClosedTransaction, othersController.delRatingByRatingId)

othersRouter.route("/pricing/:seriesId/:tranId")
  .get(requireAuth, othersController.getSeriesBySeriesId)
  .put(pricingErrorHandling, requireAuth, checkClosedTransaction, othersController.savePricingBySeriesId)

othersRouter.route("/series/:seriesId/:tranId")
  .put(requireAuth, checkClosedTransaction, othersController.saveSeriesBySeriesId)
  .delete(requireAuth, checkClosedTransaction, othersController.delSeriesBySeriesIds)

othersRouter.route("/participants/:type/:tranId")
  .post(requireAuth, checkClosedTransaction, othersController.postUnderwriterCheckAddToDL)
  .put(OthersErrorHandling, requireAuth, checkClosedTransaction, othersController.saveParticipants)

othersRouter.route("/notes/:tranId")
  .post(requireAuth, checkClosedTransaction, othersController.postOthersNotes)
