import express from "express"
import derivativesController from "./derivatives.controller"
import {requireAuth} from "./../../../../authorization/authsessionmanagement/auth.middleware"
import {
  createErrorHandling
} from "../bankloans/validation"
import {
  errorHandling,
  errorParticipants
} from "./validation"
import {checkClosedTransaction} from "../../commonservices/services.controller"


export const derivativesRouter = express.Router()

derivativesRouter.param("id", derivativesController.findByParam)

derivativesRouter.route("/")
  .get(requireAuth,derivativesController.getAll)
  .post(requireAuth,derivativesController.postDerivativeTransaction)

derivativesRouter.route("/:id")
  .get(requireAuth,derivativesController.getOne)
  .put(requireAuth, checkClosedTransaction, derivativesController.updateOne)
  .delete(requireAuth, checkClosedTransaction, derivativesController.createOne)

derivativesRouter.route("/create")
  .post(createErrorHandling, requireAuth,derivativesController.postDerivativeTransaction)

derivativesRouter.route("/transactions/:tranClientId")
  .get(requireAuth, derivativesController.getUserTransactions)

derivativesRouter.route("/transaction/:type/:tranId")
  .get(requireAuth, derivativesController.getTransactionDetails)
  .put(errorHandling, requireAuth, checkClosedTransaction, derivativesController.putTransaction)

derivativesRouter.route("/participants/:type/:tranId")
  .put(errorParticipants, requireAuth,  checkClosedTransaction, derivativesController.putParticipantsDetails)
  .delete(requireAuth, checkClosedTransaction, derivativesController.pullParticipantsDetails)

derivativesRouter.route("/notes/:tranId")
  .post(requireAuth, checkClosedTransaction, derivativesController.postDerivativeNotes)
