import express from "express"
import busdevController from "./busdev.controller"
import {requireAuth} from "../../../../authorization/authsessionmanagement/auth.middleware"

export const busdevRouter = express.Router()

busdevRouter.param("id", busdevController.findByParam)

busdevRouter.route("/")
  .get(requireAuth, busdevController.getAll)
  .post(requireAuth,busdevController.postBusDevTransaction)

busdevRouter.route("/:id")
  .get(requireAuth,busdevController.getOne)
  .post(requireAuth, busdevController.getRelatedTask)

busdevRouter.route("/tasks/search")
  .get(requireAuth,busdevController.searchTaskByEntity)

busdevRouter.route("/transaction/:type/:tranId")
  .put(requireAuth, busdevController.putBusDevTransaction)

busdevRouter.route("/notes/:tranId")
  .post(requireAuth, busdevController.postBusDevNotes)
