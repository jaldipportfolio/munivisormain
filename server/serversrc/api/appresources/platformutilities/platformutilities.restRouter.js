import express from "express"
import { addTenantIdsOnEntityUsersAndEntity } from "./platformutilities.controller"
import {authMiddleware} from "../../../authorization/authsessionmanagement/auth.middleware"

export const platformUtilitiesRouter = express.Router()


platformUtilitiesRouter.route("/add-tenantid-on-entity-entityusers-task")
  .post(authMiddleware,addTenantIdsOnEntityUsersAndEntity)
