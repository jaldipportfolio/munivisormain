import { ObjectID } from "mongodb"
import {
  Controls,
  Entity,
  EntityUser,
  Tasks,
  EntityRel
} from "../models"

export const addTenantIdsOnEntityUsersAndEntity = async ( req, res ) => {

  try {

    const entityRelationList = await EntityRel.find({})
    const taskList = await Tasks.find({})
    const controlsList = await Controls.find({})

    entityRelationList.map(async ent => {

      const entity = await  EntityRel.aggregate([
        { $match: { entityParty2: ObjectID(ent.entityParty2) } },
        { $lookup: { from: "entities", localField: "entityParty2", foreignField: "_id", as: "entity" } },
        { $lookup: { from: "entityusers", localField: "entityParty2", foreignField: "entityId", as: "entityUser" } },

        {
          $project : { tenantId: "$entityParty1" , entity: "$entity._id", users: "$entityUser._id" }
        }
      ])

      const entityDetails = entity[0]

      await Entity.update({_id: { $in: entityDetails.entity[0] } }, {$set: {tenantId: entityDetails.tenantId}})
      await EntityUser.updateMany({_id: { $in: entityDetails.users } }, {$set: {tenantId: entityDetails.tenantId}})

    })


    taskList.map(async task => {

      let tenantId = ""
      const matchUser = await EntityUser.findOne({_id: task.taskDetails.taskAssigneeUserId})
      if(matchUser && matchUser.tenantId){
        tenantId = matchUser && matchUser.tenantId
      } else {
        const matchEntity = await Entity.findOne({_id: task.taskDetails.taskAssigneeUserId})
        tenantId = matchEntity && matchEntity.tenantId
      }
      await Tasks.update({_id: task._id}, {$set: {tenantId}})

    })

    controlsList.map(async control => {

      const matchControl = await Entity.findOne({_id: control.entityId})
      if(matchControl && matchControl.tenantId){
        await Controls.update({_id: control._id}, {$set: {tenantId: matchControl.tenantId}})
      }

    })

    res.send({done: true, message: "tenantId Updated Successfully"})

  } catch (err) {
    console.log(err)
    res.status(422).send({ done: false, error: "Error in update tenantId", err })
  }
}
