import { generateControllers } from "../../modules/query"
import { AuditLog } from "./auditlog.model"
import {ObjectID} from "mongodb"

const getAuditLogByTranId = () => async (req, res, next) => {
  try {
    const { type } = req.params
    const { user } = req
    let changeLog = []
    if(type === "Data Migration"){
      console.log("DM AUDIT QUERY...", JSON.stringify({type, groupId: user.entityId.toString() }))
      changeLog = await AuditLog.aggregate(
        [
          { "$match": {type, groupId: user.entityId.toString() }},
          { $group : { _id : "$type",  changeLog: { $push: "$changeLog" } } },
        ])
    }else {
      changeLog = await AuditLog.aggregate(
        [
          { "$match": {type, "changeLog.userId": ObjectID(user._id) }},
          { $group : { _id : "$type",  changeLog: { $push: "$changeLog" } } },
        ])
    }

    const mergeChangelog = []
    if(changeLog && changeLog.length){
      changeLog[0].changeLog.forEach(log => {
        log.forEach(l => {
          mergeChangelog.push(l)
        })
      })
      changeLog[0].changeLog = mergeChangelog
      res.json(changeLog[0])
    }else {
      res.json([])
    }
  } catch(error) {
    console.log("Get Audit Log Error...", error)
    next(error)
  }
}

export const overrideCreateOne = async (req, res) => {
  const { user } = req
  const { entityId } = user || {}

  if(!user || !entityId) {
    return res.status(422).send({ error: "No user or entityId found" })
  }
  try {
    AuditLog.create({ ...req.body, groupId: entityId })
    res.json({ message: "OK" })
  } catch (error) {
    return res.status(422).send({ error })
  }
}

export const overrideGet = async (req, res) => {
  const { user } = req
  const { entityId } = user || {}

  if(!user || !entityId) {
    return res.status(422).send({ error: "No user or entityId found" })
  }

  const { type } = req.query

  try {
    const result = await AuditLog.find({ groupId: entityId, type })
    res.json(result)
  } catch (error) {
    return res.status(422).send({ error })
  }
}

export default generateControllers(AuditLog, {
  getAll: overrideGet,
  getOne: overrideGet,
  createOne: overrideCreateOne,
  getAuditLogByTranId: getAuditLogByTranId()
})
