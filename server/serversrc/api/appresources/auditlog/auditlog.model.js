import mongoose from "mongoose"
// import { RFP } from "../rfp/rfp.model"
import { EntityUser } from "../entityUser/entityUser.model"

const { Schema } = mongoose

const changeLogSchema = new Schema({
  userId: { type: Schema.Types.ObjectId, ref: EntityUser },
  userName: String,
  log: String,
  ip: String,
  date: Date
}, { strict: false })

const auditLogSchema = new Schema({
  groupId: String,
  type: String,
  changeLog: [changeLogSchema]
}, { strict: false })

auditLogSchema.index({groupId:1, type:1})

export const AuditLog = mongoose.model("auditlog", auditLogSchema)

