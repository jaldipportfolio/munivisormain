import express from "express"
import auditLogController from "./auditlog.controller"
import {requireAuth} from "../../../authorization/authsessionmanagement/auth.middleware"

export const auditLogRouter = express.Router()

auditLogRouter.param("id", auditLogController.findByParam)

auditLogRouter.route("/")
  .get(requireAuth, auditLogController.getAll)
  .post(requireAuth, auditLogController.createOne)

auditLogRouter.route("/:id")
  .get(requireAuth, auditLogController.getOne)
  .put(requireAuth, auditLogController.updateOne)
  .delete(requireAuth, auditLogController.createOne)

auditLogRouter.route("/transaction/:type")
  .get(requireAuth, auditLogController.getAuditLogByTranId)
