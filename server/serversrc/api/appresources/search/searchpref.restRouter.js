import express from "express"
import searchPrefController from "./searchpref.controller"
import {requireAuth} from "../../../authorization/authsessionmanagement/auth.middleware"

export const searchPrefRouter = express.Router()

searchPrefRouter.param("id", searchPrefController.findByParam)

searchPrefRouter.route("/")
  .get(requireAuth,searchPrefController.getAllUserPreferences)
  .post(requireAuth,searchPrefController.upsertAllUserPrefs)


searchPrefRouter.route("/context/:contextname")
  .get(requireAuth,searchPrefController.getAllUserPrefForContext)
  .post(requireAuth,searchPrefController.savePrefrences)

searchPrefRouter.route("/:id")
  .get(requireAuth, searchPrefController.getOne)
  .put(requireAuth, searchPrefController.updateOne)
  .delete(requireAuth, searchPrefController.deleteOne)
