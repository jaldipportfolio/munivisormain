module.exports = {
  "properties": {
    "taskIdentifier": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "taskContentHash": {
      "type": "text",
      "fields": {
        "raw": {
          "type": "keyword"
        },
        "searchable": {
          "type": "text",
          "analyzer": "searchableText"
        }
      }
    },
    "taskDetails": {
      "properties": {
        "taskAssigneeUserId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "taskAssigneeName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "taskAssigneeType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "taskDescription": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "taskStartDate": {
          "type": "date"
        },
        "taskEndDate": {
          "type": "date"
        },
        "taskPriority": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "taskStatus": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "taskFollowing": {
          "type": "boolean"
        }
      }
    },
    "relatedActivityDetails": {
      "properties": {
        "activityId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "activityType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "activitySubType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "activityProjectName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "activityIssuerClientId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "activityIssuerClientName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "activityContext": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "activityContextSection": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "activityContextTaskId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        }
      }
    },
    "taskAssigneeUserDetails": {
      "properties": {
        "userId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "userFirstName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "userLastName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "userEntityId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "userEntityName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        }
      }
    },
    "relatedEntityDetails": {
      "properties": {
        "entityId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "entityName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        }
      }
    },
    "relatedUserDetails": {
      "properties": {
        "relatedUserId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "relatedUserName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "relatedUserEntityId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "relatedUserEntityName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        }
      }
    },
    "taskRelatedDocuments": {
      "properties": {
        "docId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "taskDocumentName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "taskContextId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "taskContextType": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "taskDocumentUploadDate": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "taskDocumentUploadUser": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        }
      }
    },
    "taskCreatedBy": {
      "properties": {
        "userId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "userFirstName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "userLastName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "userEntityId": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        },
        "userEntityName": {
          "type": "text",
          "fields": {
            "raw": {
              "type": "keyword"
            },
            "searchable": {
              "type": "text",
              "analyzer": "searchableText"
            }
          }
        }
      }
    }
  }
}
