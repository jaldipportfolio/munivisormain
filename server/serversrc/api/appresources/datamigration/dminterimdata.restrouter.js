import express from "express"
import {
  saveInterimData,
  getInterimData,
  getInterimDataByInformationType,
  postInterimDataByInformationType,
  deleteInterimDataByInformationType,
  uploadMigrationData,
  saveDealsMigrationData
} from "./dmintermindata.controller"
import { requireAuth } from "./../../../authorization/authsessionmanagement/auth.middleware"

export const dataMigrationRouter = express.Router()

dataMigrationRouter.route("/")
  .post(requireAuth, saveInterimData)

dataMigrationRouter.route("/:id")
  .get(requireAuth, getInterimData)
  .post(requireAuth, saveInterimData)

dataMigrationRouter.route("/interiminformation/:infotype")
  .get(requireAuth, getInterimDataByInformationType)
  .post(requireAuth, postInterimDataByInformationType)
  .delete(requireAuth, deleteInterimDataByInformationType)

dataMigrationRouter.route("/saveData/deals")
  .post(requireAuth, saveDealsMigrationData)

dataMigrationRouter.route("/saveData/:infotype")
  .post(requireAuth, uploadMigrationData)


