import mongoose from "mongoose"

const { Schema } = mongoose

export const messagesSchema = new Schema({
  userId: String,
  from: String,
  sentDate: Date,
  description: String,
  sourceType: String,
  sourceId: String,
  docIds: [],
  readDate: Date
}, { strict: false })

messagesSchema.index({userId:1})

export const Messages = mongoose.model("messages", messagesSchema)
