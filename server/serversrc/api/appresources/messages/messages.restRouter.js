import express from "express"
import messagesController from "./messages.controller"

import {requireAuth} from "../../../authorization/authsessionmanagement/auth.middleware"
import { getAllTransactionDetailsForLoggedInUser } from
  "../../commonDbFunctions/getAllDetailedTransactionsForLoggedInUser"
import { getAllUsersForFirm } from
  "../../commonDbFunctions/getAllUsersForFirm"

export const messagesRouter = express.Router()

messagesRouter.get("/related-info", requireAuth, async (req, res) => {
  const { user } = req
  if(!user) {
    return res.status(422).send({ error: "No user found" })
  }

  try {
    const result = await getAllTransactionDetailsForLoggedInUser(user)
    return res.json(result)
  } catch (err) {
    return res.status(422).send({ error: "Error in getting related info" })
  }
})

messagesRouter.get("/users", requireAuth, async (req, res) => {
  const { user } = req
  if(!user) {
    return res.status(422).send({ error: "No user found" })
  }

  try {
    const result = await getAllUsersForFirm(user.entityId)
    return res.json(result.map(e => ({
      id: e._id,
      name: `${e.userFirstName} ${e.userMiddleName} ${e.userLastName}`,
      emails: e.userEmails
    })))
  } catch (err) {
    return res.status(422).send({ error: "Error in getting users info" })
  }
})

messagesRouter.param("id", messagesController.findByParam)

messagesRouter.route("/")
  .get(requireAuth, messagesController.getAll)
  .post(messagesController.createOne)

messagesRouter.route("/:id")
  .get(messagesController.getOne)
  .put(messagesController.updateOne)
  .delete(messagesController.createOne)
