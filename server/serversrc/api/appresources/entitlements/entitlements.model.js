import mongoose from "mongoose"
import timestamps from "mongoose-timestamp"

const { Schema } = mongoose

const entManagementSchema = new Schema({
  tenantId:Schema.Types.ObjectId,
  entityId:Schema.Types.ObjectId,
  userId:Schema.Types.ObjectId,
  accessInfo:{}
})

entManagementSchema.plugin(timestamps)
entManagementSchema.index({entityId:1, userId:1})


export const UserEntitlements = mongoose.model("tenantuserentitlements", entManagementSchema)
