import {ObjectID} from "mongodb"
import { EntityRel } from "./../../appresources/models"
import isEmpty from "lodash/isEmpty"

import { URLMAPPER,ALLURLS} from "./constants"

export const tenantUrlGenerator = async (tenantId) => {

  const comoonQueryComponent = (type) => {
    const retValue = [
      {
        $project:{
          _id:"$trans._id",
          // url:{"$concat":[...urlString]}
        }
      }

    ]
    return retValue
  }

  const commonURLMapper = (type, id) => {
    const concatStrings = URLMAPPER[type]
    let urlString = ""
    if(concatStrings[1]) {
      urlString = `/${concatStrings[0]}/${id}/${concatStrings[1]}`
    } else {
      urlString = `/${concatStrings[0]}/${id}`
    }

    return urlString
  }
  const allURLMapper = (type, id) => {
    const {l1, otherlevels} = ALLURLS[type] || {}

    let allURLDetails
    if(!isEmpty(otherlevels)) {
      allURLDetails = (otherlevels || []).reduce( (acc, k) => {
        const urlString = `/${l1}/${id}/${k}`
        return {...acc,[k]:urlString }
      },{})
    } else {
      return {"default":`/${l1}/${id}`}
    }
    return allURLDetails
  }


  // const allURLMapper = (type, id) => {
  //   const {l1, otherlevels} = ALLURLS[type] || {}

  //   const allURLDetails = (otherlevels || []).reduce( (acc, k) => {
  //     const urlString = `/${l1}/${id}/${k}`
  //     return {...acc,[k]:urlString }
  //   },{})

  //   return allURLDetails
  // }

  const commonUserQueryComponent = (type) => [
    {
      $lookup:{
        from: "entityrels",
        localField: "tenantId",
        foreignField: "entityParty1", 
        as: "alltenantentities"
      }
    },
    {
      $unwind:"$alltenantentities"
    },
    {
      $project:{
        entityId:"$alltenantentities.entityParty2",
        relationshipType:"$alltenantentities.relationshipType"
      }
    },
    {
      $lookup:{
        from: "entityusers",
        localField: "entityId",
        foreignField: "entityId", 
        as: "allusers"
      }
    },
    {
      $unwind:"$allusers"
    },
    {
      $match:{relationshipType:type}
    },
    {
      $project: {
        "_id":"$allusers._id"
      }
    }
  ]

  const commonEntityQueryComponent = (type) => [
    {
      $lookup:{
        from: "entityrels",
        localField: "tenantId",
        foreignField: "entityParty1", 
        as: "alltenantentities"
      }
    },
    {
      $unwind:"$alltenantentities"
    },
    {
      $project:{
        entityId:"$alltenantentities.entityParty2",
        relationshipType:"$alltenantentities.relationshipType"
      }
    },
    {
      $lookup:{
        from: "entities",
        localField: "entityId",
        foreignField: "_id", 
        as: "allentities"
      }
    },
    {
      $unwind:"$allentities"
    },
    {
      $match:{relationshipType:type}
    },
    {
      $project: {
        "_id":"$allentities._id"
      }
    }
  ]


  const alldetails = await EntityRel.aggregate([
    {
      $match:{entityParty1:ObjectID(tenantId), relationshipType:"Self"}
    },
    {
      $project:{tenantId:"$entityParty1"}
    },
    {
      $facet:{
        "deals":[
          {
            $lookup:{
              from: "tranagencydeals",
              localField: "tenantId",
              foreignField: "dealIssueTranClientId", 
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          ...comoonQueryComponent("deals")
        ],
        "rfps":[
          {
            $lookup:{
              from: "tranagencyrfps",
              localField: "tenantId",
              foreignField: "rfpTranClientId", 
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          ...comoonQueryComponent("rfps")
        ],
        "marfps":[
          {
            $lookup:{
              from: "actmarfps",
              localField: "tenantId",
              foreignField: "actTranFirmId", 
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          ...comoonQueryComponent("marfps")
        ],
        "bankloans":[
          {
            $lookup:{
              from: "tranbankloans",
              localField: "tenantId",
              foreignField: "actTranFirmId", 
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          ...comoonQueryComponent("bankloans")
        ],
        "others":[
          {
            $lookup:{
              from: "tranagencyothers",
              localField: "tenantId",
              foreignField: "actTranFirmId", 
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          ...comoonQueryComponent("others")
        ],

        "derivatives":[
          {
            $lookup:{
              from: "tranderivatives",
              localField: "tenantId",
              foreignField: "actTranFirmId", 
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          ...comoonQueryComponent("derivatives")
        ],
        "busdevs":[
          {
            $lookup:{
              from: "actbusdevs",
              localField: "tenantId",
              foreignField: "actTranFirmId", 
              as: "trans"
            }
          },
          {
            $unwind:"$trans"
          },
          ...comoonQueryComponent("busdevs")
        ],

        "tenantusers":[ ...commonUserQueryComponent("Self")  ],
        "clientusers":[ ...commonUserQueryComponent("Client")  ],
        "prospectusers":[ ...commonUserQueryComponent("Prospect")  ],
        "migratedusers":[ ...commonUserQueryComponent("Undefined")  ],
        "thirdpartyusers":[ ...commonUserQueryComponent("Third Party")  ],
        "firms":[ ...commonEntityQueryComponent("Self")  ],
        "clients":[ ...commonEntityQueryComponent("Client")  ],
        "prospects":[ ...commonEntityQueryComponent("Prospect")  ],
        "thirdparties":[ ...commonEntityQueryComponent("Third Party")  ],
        "migratedentities":[ ...commonEntityQueryComponent("Undefined")  ]
      },
    }
  ])

  const retData = alldetails[0]

  const defaultUrls = Object.keys(retData).reduce ( (a, k) => {
    retData[k].forEach( val => {
      a[val._id] = commonURLMapper(k, val._id)
    })
    return a
  },{})

  const allUrlData = Object.keys(retData).reduce ( (a, k) => {
    retData[k].forEach( val => {
      a[val._id] = allURLMapper(k, val._id)
    })
    return a
  },{})
  return {defaultUrls,allurls:allUrlData}
}
