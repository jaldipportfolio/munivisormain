import mongoose from "mongoose"
import { Entity } from "../entity/entity.model"
import timestamps from "mongoose-timestamp"

const {Schema} = mongoose


const entityRelationshipsSchema = Schema({
  // _id: { type: Schema.ObjectId, required: true},
  entityParty1:{ type: Schema.Types.ObjectId, ref: Entity },
  entityParty2:{ type: Schema.Types.ObjectId, ref: Entity },
  relationshipType:String,
})
entityRelationshipsSchema.plugin(timestamps)
entityRelationshipsSchema.index({entityParty1:1, entityParty2:1})
entityRelationshipsSchema.index({entityParty1:1, relationshipType:1})
entityRelationshipsSchema.index({entityParty2:1, relationshipType:1})

export const EntityRel =  mongoose.model("entityrel", entityRelationshipsSchema)
