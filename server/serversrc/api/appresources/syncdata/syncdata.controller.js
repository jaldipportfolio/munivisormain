import isEmpty from "lodash/isEmpty"
import { ImportedSyncData } from "./syncdata.model"
import { EntityUser } from "../models"

const {ObjectID} = require("mongodb")

export const importSyncData = async (req,res) => {
  try {
    let {importData} = req.body
    const {type} = req.query
    const { _id, userFirstName, userLastName, entityId, tenantId } = req.user
    const importIds = importData.map(con => con.id)
    let alreadyExists = []
    if(importIds.length){
      alreadyExists = await ImportedSyncData.find({syncId: { $in: importIds } })
      alreadyExists = alreadyExists.map(exist => exist.syncId)
    }
    const alreadyImportData = importData.filter(con => alreadyExists.indexOf(con.id) !== -1)
    importData = importData.filter(con => alreadyExists.indexOf(con.id) === -1)
    importData = (importData || []).map(contact => ({
      syncContact: contact,
      userId: ObjectID(_id),
      userName: `${userFirstName} ${userLastName}`,
      entityId: ObjectID(entityId),
      tenantId: ObjectID(tenantId),
      syncId: contact.id,
      outLookSync: type === "outlook",
      googleSync: type === "google"
    }))
    if(importData.length){
      await ImportedSyncData.insertMany(importData)
    }
    res.send({ done: true, message: `Data imported successfully and ${alreadyImportData.length} contacts duplicate`})
  }
  catch (e) {
    console.log(e)
    res.status(422).send({ done:false, message:"There is a failure in importing data" })
  }
}

export const fetchSyncData = async (req,res) => {
  try {
    const { user } = req
    const { filters, pagination } = req.body
    const { freeTextSearchTerm } = filters
    const { serverPerformPagination, currentPage, size, sortFields } = pagination
    const matchQueryNew = []

    if (!isEmpty(freeTextSearchTerm)) {
      matchQueryNew.push({
        $match: {
          $or: [{ "keySearchDetails": { $regex: freeTextSearchTerm, $options: "i" } } ]
        }
      })
    }
    console.log("The Match Query is", JSON.stringify(matchQueryNew, null, 2) )

    // const paginationParameter = 5
    const recordstoskip = size * currentPage

    if(sortFields &&  Object.keys(sortFields).length){
      Object.keys(sortFields).forEach(key => {
        sortFields[`syncContact.${key}`] = sortFields[key]
      })
    }

    const queryPipeline = [
      { $match: { userId: ObjectID(user._id) /* , mergedIntoFirmCRM: false */ } },
      {
        $addFields: {
          keySearchDetails: {
            $concat: [
              { $ifNull: ["$syncContact.displayName", ""] }
            ]
          },
        }
      },
      ...matchQueryNew,
      {
        $facet: {
          metadata: [{ $count: "total" }, { $addFields: { pages: { $ceil: { $divide: ["$total", size] } } } }],
          data: [{ $sort: sortFields }, { $skip: recordstoskip }, { $limit: size }] // add projection here wish you re-shape the docs
        }
      }
    ]


    const pipeLineQueryResults = await ImportedSyncData.aggregate([
      ...queryPipeline
    ])
    pipeLineQueryResults[0].data = (pipeLineQueryResults[0].data || []).map(contact => ({ ...contact.syncContact, _id: contact._id, mergedIntoFirmCRM: contact.mergedIntoFirmCRM || false }))
    res.send({pipeLineQueryResults})
  }
  catch (e) {
    console.log(e)
    res.status(422).send({ done:false, message:"There is a failure in fetching imported data" })
  }
}

/*
{
  "assistantName": "string",
  "birthday": "String (timestamp)",
  "businessAddress": {"@odata.type": "microsoft.graph.physicalAddress"},
  "businessHomePage": "string",
  "businessPhones": ["string"],
  "categories": ["string"],
  "changeKey": "string",
  "children": ["string"],
  "companyName": "string",
  "createdDateTime": "String (timestamp)",
  "department": "string",
  "displayName": "string",
  "emailAddresses": [{"@odata.type": "microsoft.graph.emailAddress"}],
  "fileAs": "string",
  "generation": "string",
  "givenName": "string",
  "homeAddress": {"@odata.type": "microsoft.graph.physicalAddress"},
  "homePhones": ["string"],
  "id": "string (identifier)",
  "imAddresses": ["string"],
  "initials": "string",
  "jobTitle": "string",
  "lastModifiedDateTime": "String (timestamp)",
  "manager": "string",
  "middleName": "string",
  "mobilePhone": "string",
  "nickName": "string",
  "officeLocation": "string",
  "otherAddress": {"@odata.type": "microsoft.graph.physicalAddress"},
  "parentFolderId": "string",
  "personalNotes": "string",
  "profession": "string",
  "spouseName": "string",
  "surname": "string",
  "title": "string",
  "yomiCompanyName": "string",
  "yomiGivenName": "string",
  "yomiSurname": "string",
  "photo": { "@odata.type": "microsoft.graph.profilePhoto" }
}

{
  userId:String, // Should we make this the same as the primary email
  entityId: { type: Schema.Types.ObjectId, ref: Entity },
  userStatus: String,
  timeZone: {},
  userFirmName:String,
  userFlags: [String], // [series50,msrbcontact, emmacontact, supprinsipal, compofficer, mfp, primarycontact, procurementcont, fincontact, distmember, electedofficial, invstcontact, attorney, cpa ]
  userRole:String, // [ Admin, ReadOnly, Backup ]
  userEntitlement:String, // [ Global, Transaction ]
  userFirstName:String,
  userMiddleName:String,
  userLastName:String,
  userEmails:[{
    emailId:String,
    emailPrimary:Boolean,
  }],
  userPhone: [{
    phoneNumber: "",
    extension: "",
    phonePrimary: false
  }],
  userFax: [{
    faxNumber: "",
    faxPrimary: false
  }],
  userEmployeeID:String,
  userJobTitle:String,
  userManagerEmail:String,
  userJoiningDate:Date,
  userEmployeeType:String,
  userDepartment:String,
  userExitDate:Date,
  userCostCenter:String,
  userAddresses:[userAddressSchema],
  userAddOns:[String],
  userLoginCredentials:userCredentialsSchema,
  userAddDate:Date,
  userUpdateDate:Date,
  userDocuments:[documentsSchema],
  notes:[notesSchema],
  OnBoardingDataMigrationHistorical: Boolean,
  historicalData: Object
}
*/

const contactAddress = (user) => {
  const userAddresses = []
  if(user && user.homeAddress){
    const hAddress = user.homeAddress || {}
    userAddresses.push({
      addressName: "Home Address",
      addressType: "residence",
      isPrimary: true,
      isActive: true,
      addressLine1: hAddress.street || "",
      addressLine2: "",
      country: hAddress.countryOrRegion || "",
      state: hAddress.state || "",
      city: hAddress.city || "",
      zipCode: {
        zip1: hAddress.postalCode || "",
        zip2:""
      },
    })
  }
  if(user && user.businessAddress){
    const bAddress = user.businessAddress || {}
    userAddresses.push({
      addressName: "Business Address",
      addressType: "Office address",
      isPrimary: !userAddresses.length,
      isActive: true,
      addressLine1: bAddress.street || "",
      addressLine2: "",
      country: bAddress.countryOrRegion || "",
      state: bAddress.state || "",
      city: bAddress.city || "",
      zipCode: {
        zip1: bAddress.postalCode || "",
        zip2:""
      },
    })
  }
  if(user && user.otherAddress){
    const oAddress = user.otherAddress || {}
    userAddresses.push({
      addressName: "Other Address",
      addressType: "residence",
      isPrimary: !userAddresses.length,
      isActive: true,
      addressLine1: oAddress.street || "",
      addressLine2: "",
      country: oAddress.countryOrRegion || "",
      state: oAddress.state || "",
      city: oAddress.city || "",
      zipCode: {
        zip1: oAddress.postalCode || "",
        zip2:""
      },
    })
  }
  return userAddresses
}
export const contactAddToEntity = async (req,res) => {
  try {
    const { selectedUsers, addUserInEnt, resolvedObj } = req.body
    const modifiedUsers = []
    const users = await  ImportedSyncData.find({_id: { $in : selectedUsers }, mergedIntoFirmCRM: false }).select("syncContact")

    users.forEach(contact => {
      const user = contact.syncContact || {}
      const userEmails = []
      const userPhone = []
      const userAddresses = contactAddress(user)
      const reObj = (resolvedObj && resolvedObj[user.id]) || {}

      user.emailAddresses.forEach((email,i) => {
        userEmails.push({
          emailId: reObj && reObj[`Email${i+1}`] ? reObj[`Email${i+1}`] : email.address,
          emailPrimary: i === 0
        })
      })
      if(user.mobilePhone){
        userPhone.push({
          phoneNumber: user.mobilePhone,
          extension: "",
          phonePrimary: true
        })
      }
      user.homePhones.forEach(phone => {
        userPhone.push({
          phoneNumber: phone,
          extension: "",
          phonePrimary: false
        })
      })

      if(!userEmails.length){
        userEmails.push({
          emailId: reObj.Email,
          emailPrimary: true
        })
      }
      modifiedUsers.push({
        userId: ObjectID(),
        entityId: ObjectID(addUserInEnt.id),
        userStatus: "active",
        userFirmName: addUserInEnt.name,
        userFlags: [],
        userRole: "tran-edit",
        userEntitlement: "tran-edit",
        userFirstName: reObj && reObj["First Name"] ? reObj["First Name"] : user.givenName || "",
        userMiddleName: user.middleName || "",
        userLastName: reObj && reObj["Last Name"] ? reObj["Last Name"] : user.surname || "",
        userEmails,
        userPhone,
        isMuniVisorClient: false,
        userLoginCredentials: {
          userEmailId: userEmails.length ? userEmails[0].emailId : "",
          password: "",
          onboardingStatus: "created",
          userEmailConfirmString: "",
          userEmailConfirmExpiry: "",
          passwordConfirmString: "",
          passwordConfirmExpiry: "",
          isUserSTPEligible: false,
          authSTPToken: "",
          authSTPPassword: "",
          passwordResetIteration: 0,
          passwordResetStatus: 0,
          passwordResetDate: ""
        },
        userFax: [],
        userEmployeeID: "",
        userJobTitle: "",
        userManagerEmail: "",
        userJoiningDate: new Date(),
        userEmployeeType: "",
        userDepartment: "",
        userAddresses,
        userAddDate: new Date(),
        userUpdateDate: new Date(),
        notes:[],
        OnBoardingDataMigrationHistorical: false,
        syncData: true,
        syncDataObj: user,
        updatedAt: new Date(),
        createdAt: new Date()
      })
    })

    // console.log(modifiedUsers)
    await ImportedSyncData.updateMany(
      { _id: { $in : selectedUsers } },
      { $set: { mergedIntoFirmCRM: true } }
    )
    await EntityUser.insertMany(modifiedUsers)
    res.send({done: true, message: "Users added to entity successfully", modifiedUsers})
  }
  catch (e) {
    console.log(e)
    res.status(422).send({ done:false, message:"There is a failure in fetching imported data" })
  }
}
