import mongoose from "mongoose"
import timestamps from "mongoose-timestamp"

const {Schema} = mongoose


const importedSyncDataSchema = Schema({
  tenantId: Schema.Types.ObjectId,
  userId: Schema.Types.ObjectId,
  entityId: Schema.Types.ObjectId,
  userName: String,
  mergedIntoFirmCRM: {
    type: Boolean,
    default: false
  },
  syncId: String,
  syncContact: Object,
  outLookSync: {
    type: Boolean,
    default: false
  },
  googleSync: {
    type: Boolean,
    default: false
  }
})
importedSyncDataSchema.plugin(timestamps)

export const ImportedSyncData =  mongoose.model("importedsyncdata", importedSyncDataSchema)
