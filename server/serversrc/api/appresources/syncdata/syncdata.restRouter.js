import express from "express"
import {importSyncData, fetchSyncData, contactAddToEntity} from "./syncdata.controller"
import {requireAuth} from "../../../authorization/authsessionmanagement/auth.middleware"

export const syncDataRouter = express.Router()

syncDataRouter.route("/import")
  .get(requireAuth, fetchSyncData)
  .post(requireAuth, importSyncData)
  .put(requireAuth, contactAddToEntity)

syncDataRouter.route("/importedlist")
  .post(requireAuth, fetchSyncData)
