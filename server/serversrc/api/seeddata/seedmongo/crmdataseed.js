const assert = require('assert');
const keys = require('./../../configs/keys');
const MongoClient = require('mongodb').MongoClient;
const elasticsearch = require('elasticsearch');

const url = keys.mongoUri;
const mclient = new MongoClient(url);
const dbName = 'munivisor_dev';

const DROPDOWN = require('./../testdata/globaldropdown');
const fs = require('fs');
const ObjectID = require('mongodb').ObjectID;
const faker = require('faker');
const moment = require("moment");

let numRelationships = 5;
let numEntities = 10;
let numContacts = 20;

const start = new Date();
// const uidData = fs.readFileSync("uids.json", "utf8");

let groupIds = [];

const randomInt = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

for (let i = 0; i < numRelationships; i++) {
  groupIds.push(new ObjectID());
}

let numGroupIds = groupIds.length;

let uids = [];

for (let i = 0; i < numRelationships * 4; i++) {
  uids.push({
    _id: new ObjectID(),
    groupId: groupIds[randomInt(0, numGroupIds)]
  });
}

let numUids = uids.length;

let iter;

const generateRelationships = (numRelationships) => {
  let customerRelationships = [];
  for (let i = 1; i <= numRelationships; i++) {
    customerRelationships.push(generateRelationship(i));
  }
  return customerRelationships;
}

const getRandomUid = groupId => {
  let users = uids.filter(u => u.groupId === groupId);
  let user = users[randomInt(0, users.length - 1)];
  return user
    ? user._id
    : new ObjectID();
}

const generateRelationship = (relCounter) => {
  let relationship = {};
  relationship._id = new ObjectID();
  relationship.groupId = groupIds[randomInt(0, numGroupIds)];
  relationship.uid = getRandomUid(relationship.groupId);
  let now = moment().valueOf();
  relationship.createdAt = now;
  relationship.updatedAt = now;
  relationship.updatedBy = relationship.uid;

  relationship.entities = generateEntities(relCounter, randomInt(0, numEntities));
  //let entityIds = relationship.entities.map(e => e.entityId);

  return relationship;
};

const getRandomLookup = (lookupKey) => {
  const len = DROPDOWN[lookupKey].length;
  return DROPDOWN[lookupKey][randomInt(0, len - 1)].value;
}

let generateEntities = (relCounter, numEntities) => {
  let entities = [];
  for (let i = 1; i <= numEntities; i++) {
    entities.push(generateEntity(relCounter, i));
  }
  return entities;
}

let generateEntity = (relCounter, entityCounter) => {
  let entity = {};
  entity.type = getRandomLookup("LKUPPARTICIPANTTYPE");
  entity.entityId = `Entity-${relCounter}-${entityCounter}`;
  entity.active = getRandomLookup("LKUPYESNO");
  entity.name = faker
    .company
    .companyName();
  entity.aliases = faker
    .random
    .words()
    .split(" ");
  entity.keyParams = {};
  entity.keyParams.sector = getRandomLookup("LKUPSECTORS");
  entity.keyParams.state = getRandomLookup("LKUPSTATE");
  entity.keyParams.taxID = faker
    .finance
    .account();
  entity.keyParams.contractStatus = getRandomLookup("LKUPSTATE");
  entity.keyParams.engagedSince = faker
    .date
    .past(2);
  entity.keyParams.numberOfEmployees = getRandomLookup("LKUPNUMOFPEOPLE");
  entity.keyParams.annualRevenue = getRandomLookup("LKUPDOLLARRANGE");
  entity.keyParams.businessStructure = getRandomLookup("LKUPBUSSTRUCT");
  entity.keyParams.ticker = faker
    .lorem
    .word();
  entity.keyParams.websiteURL = faker
    .internet
    .url();
  entity.keyParams.notes = faker
    .lorem
    .sentence();

  entity.linkedBorrowersOrObligors = [];
  iter = randomInt(1, 4);
  for (let i = 0; i < iter; i++) {
    let item = {};
    item.relationship = getRandomLookup("LKUPCRMBORROWER");
    item.entity = getRandomLookup("LKUPBORROWER");
    item.engagedSince = faker
      .date
      .past(2);
    item.engagedTill = faker
      .date
      .past(1);
    entity
      .linkedBorrowersOrObligors
      .push(item);
  }

  entity.linkedCusips = [];
  iter = randomInt(1, 4);
  for (let i = 0; i < iter; i++) {
    let item = {};
    item.debtType = getRandomLookup("LKUPDEBTTYPE");
    item.associatedCUSIP6 = randomInt(100000, 999999);
    entity
      .linkedCusips
      .push(item);
  }

  entity.addresses = [];
  iter = randomInt(1, 4);
  for (let i = 0; i < iter; i++) {
    let item = {};
    item.active = getRandomLookup("LKUPYESNO");
    item.default = getRandomLookup("LKUPYESNO");
    item.primaryOffice = getRandomLookup("LKUPYESNO");
    item.addressType = getRandomLookup("LKUPADDRESSTYPE");
    item.addressLine = faker
      .address
      .streetAddress();
    item.city = faker
      .address
      .city();
    item.state = getRandomLookup("LKUPSTATE");
    item.zip = faker
      .address
      .zipCode();
    item.phoneType = getRandomLookup("LKUPPHONETYPE");
    item.phoneNumber = faker
      .phone
      .phoneNumber();
    item.fax = faker
      .phone
      .phoneNumber();
    entity
      .addresses
      .push(item);
  }

  entity.contacts = generateContacts(randomInt(0, numContacts));

  entity.contracts = generateContracts(randomInt(0, numEntities));

  entity.feeDetails = generateFeeDetails();

  entity.activities = generateActivities(randomInt(0, numContacts));

  return entity;
};

let generateContacts = (numContacts) => {
  let contacts = [];
  for (let i = 1; i <= numContacts; i++) {
    contacts.push(generateContact(i));
  }
  return contacts;
}

let generateContact = (contactCounter) => {
  let contact = {};
  contact.contactType = getRandomLookup("LKUPCONTACTTYPE");
  contact.isActive = getRandomLookup("LKUPYESNO");
  contact.isDefault = getRandomLookup("LKUPYESNO");
  contact.prefix = getRandomLookup("LKUPNAMEPREFIX");
  contact.firstName = faker
    .name
    .firstName();
  contact.middle = faker
    .lorem
    .word();
  contact.lastName = faker
    .name
    .lastName();
  contact.suffix = getRandomLookup("LKUPNAMESUFFIX");
  contact.titleDesignation = getRandomLookup("LKUPROLEWITHENTITY");
  contact.addressType = getRandomLookup("LKUPADDRESSTYPE");
  contact.addressLine = faker
    .address
    .streetAddress();
  contact.city = faker
    .address
    .city();
  contact.state = getRandomLookup("LKUPSTATE");
  contact.zip = faker
    .address
    .zipCode();
  contact.phoneType = getRandomLookup("LKUPPHONETYPE");
  contact.phoneNumber = faker
    .phone
    .phoneNumber();
  contact.fax = faker
    .phone
    .phoneNumber();
  contact.email = faker
    .internet
    .email();

  return contact;
};

let generateContracts = (numContracts) => {
  let contracts = [];
  for (let i = 1; i <= numContracts; i++) {
    contracts.push(generateContract(i));
  }
  return contracts;
}

let generateContract = (contractCounter) => {
  let contract = {};

  contract.disclosures = [];
  iter = randomInt(1, 4);
  for (let i = 0; i < iter; i++) {
    let item = {};
    item.appraisalDistricts = faker
      .random
      .word();
    item.marketingRestrictions = getRandomLookup("LKUPYESNO");
    item.filingResponsibility = getRandomLookup("LKUPCDCFILINGRESPONSIBILITY");
    item.negativeFilingConsent = getRandomLookup("LKUPYESNO");
    item.notes = faker
      .lorem
      .sentence();
    contract
      .disclosures
      .push(item);
  }

  contract.exemptions = [];
  iter = randomInt(1, 4);
  for (let i = 0; i < iter; i++) {
    let item = {};
    item.exemptionType = faker
      .random
      .word();
    item.startDate = faker
      .date
      .past(2);
    item.endDate = faker
      .date
      .past(1);
    item.notes = faker
      .lorem
      .sentence();
    contract
      .exemptions
      .push(item);
  }

  return contract;
};

let generateFeeDetails = () => {
  let feeDetails = {};

  feeDetails.feeSchedules = [];
  iter = randomInt(1, 4);
  for (let i = 0; i < iter; i++) {
    let item = {};
    item.scheduleName = faker
      .random
      .word();
    item.scheduleStart = faker
      .date
      .past(2);
    item.scheduleEnd = faker
      .date
      .past(1);
    item.baseFee = randomInt(1000, 99999);
    item.contractStatus = getRandomLookup("LKUPCONTRACTSTATUS");
    item.contractRenewalType = getRandomLookup("LKUPCONTRACTRENEWAL");
    feeDetails
      .feeSchedules
      .push(item);
  }

  feeDetails.rateBasis = [];
  iter = randomInt(1, 4);
  for (let i = 0; i < iter; i++) {
    let item = {};
    item.fAFeeContractType = getRandomLookup("LKUPFAFEECONTRACTTYPE");
    item.value = randomInt(1000, 99999);
    item.tierAmount = randomInt(1000, 99999);
    item.notes = faker
      .lorem
      .sentence();
    feeDetails
      .rateBasis
      .push(item);
  }

  feeDetails.hourlyFees = [];
  iter = randomInt(1, 4);
  for (let i = 0; i < iter; i++) {
    let item = {};
    item.roleWithClient = getRandomLookup("LKUPROLEWITHENTITY");
    item.hourlyRate = randomInt(100, 999);
    item.notes = faker
      .lorem
      .sentence();
    feeDetails
      .hourlyFees
      .push(item);
  }

  feeDetails.addendums = [];
  iter = randomInt(1, 4);
  for (let i = 0; i < iter; i++) {
    let item = {};
    item.feeName = faker
      .random
      .words();
    item.feeAmount = randomInt(1000, 99999);
    item.isItReimburseable = getRandomLookup("LKUPYESNO");
    feeDetails
      .addendums
      .push(item);
  }

  return feeDetails;
};

let generateActivities = (numActivities) => {
  let activities = [];
  for (let i = 1; i <= numActivities; i++) {
    activities.push(generateActivity(i));
  }
  return activities;
}

let generateActivity = (activityCounter) => {
  let activity = {};
  activity.dealId = `Deal ${randomInt(1, 10)}`;
  activity.transactionName = faker
    .random
    .words();
  activity.transactionID = `Txn ${randomInt(1, 10)}`;
  activity.activityType = getRandomLookup("LKUPFAACTIVITYTYPE");
  activity.name = faker
    .name
    .firstName();
  activity.roleWithClient = getRandomLookup("LKUPROLEWITHENTITY");
  activity.taskStart = faker
    .date
    .past(2);
  activity.taskEnd = faker
    .date
    .past(1);
  activity.hours = randomInt(1, 100);
  activity.billable = getRandomLookup("LKUPYESNO");
  activity.notes = faker
    .lorem
    .sentence();

  return activity;
};

//console.log(JSON.stringify(generateRelationships(2), null, 4));

/* let data = JSON.stringify(generateRelationships(numRelationships), null, 2);


fs.writeFile('crm.json', data, 'utf8', (err, res) => {
  if(err) {
    console.log("err : ", err);
  } else {
    console.log("crm details are written ok");
    const end = new Date();
    console.log("time taken in ms : ", end - start);
  }
}); */

const crmdatasets = generateRelationships(20);

/* Construct the String Query for Elastic Search */

const initialString = '{"index":{}}';
const finalElasticSearchLoadingObject = crmdatasets.map(elem => {
  return JSON.stringify(elem);
}).join(`\n${initialString}\n`);

const elasticLoadFinal = `${initialString}\n${finalElasticSearchLoadingObject}`;
/* Insert Data into Mongo DB */

MongoClient.connect(url, (err, mongoclient) => {
  assert.equal(null, err);
  console.log('Connected correctly to server');
  const db = mongoclient.db(dbName);
  db
    .collection('crmdata_dev')
    .deleteMany({}, function (err, r) {
      assert.equal(null, err);
      console.log('deleted all the documents from Mongo');
      mongoclient.close();

      MongoClient.connect(url, (err, mongoclient) => {
        assert.equal(null, err);
        console.log('Connected correctly to server');
        const db = mongoclient.db(dbName);
        db
          .collection('crmdata_dev')
          .insertMany(crmdatasets, function (err, r) {
            assert.equal(null, err);
            console.log('inserted all the documents into Mongo');
            mongoclient.close();
          });
      });
    });
});

const elasticclient = new elasticsearch.Client({
  //host: 'localhost:9200' // Note that fa is the index and deals is the document
  host: 'https://search-munivisor-6sxxstd3jxvu4e4udr6lghn5ue.us-east-1.es.amazonaws.com'
});

const elasticclient_bulk = new elasticsearch.Client({
  // host: 'localhost:9200/fa/deals' // Note that fa is the index and deals is the
  // document
  host: 'https://search-munivisor-6sxxstd3jxvu4e4udr6lghn5ue.us-east-1.es.amazonaws.com/f' +
      'a/crmdata'
});

/* Delete Information from Elastic Search */
elasticclient.deleteByQuery({
  index: 'fa',
  type: 'crmdata',
  body: {
    query: {
      match_all: {}
    }
  }
}, function (error, response) {
  assert.equal(null, error);
  console.log('All data was deleted from the Elastic Index');
  elasticclient.close();

  elasticclient_bulk.bulk({
    body: [elasticLoadFinal]
  }, function (err, resp) {
    assert.equal(null, err);
    console.log('Bulk Inserted all data into Elastic Index');
    elasticclient_bulk.close();
  });
});
