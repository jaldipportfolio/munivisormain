const mongoose = require("mongoose")
const _ = require("lodash")

const Schema = mongoose.Schema
const TaskModels = require("./../testschema/Task")
const ObjectID = require("mongodb").ObjectID

mongoose.Promise = global.Promise
mongoose.connect("MONGO DB URL")

/* Destructure the objects from the exported variables */

const { Task, UserRevised, Tenant, Deal } = TaskModels
const user = ObjectID("5a8f595b1eca615e64c360b7")
const getTasksForUsers = async () => {
  tasks = await Task.find({ user })
  return tasks
}

// getTasksForUsers().then(tasks => console.log(tasks));

const getTasksForUsersNew = () => {
  Promise(async (resolve, reject) => {
    const user = ObjectID("5a8f595b1eca615e64c360b7")
    try {
      console.log("Getting the tasks for a user")
      const tasks = await Task.find({ user })
      resolve({ tasks })
    } catch (error) {
      reject(error)
    }
  })
}

const getTasksBasedOnSearch = async () => {
  const user = "5a8f595b1eca615e64c360b7"
  const term = "dolo"
  const regExpTerm = new RegExp(term, "i")
  console.log(regExpTerm)
  const regExpSearch = [
    { title: regExpTerm  },
    { description: regExpTerm }
  ]
  console.log(regExpSearch)
  tasks = await Task.find({
    user ,
    $or: regExpSearch
  })
  return tasks
}

getTasksBasedOnSearch().then(tasks => console.log(tasks)).catch(err => console.log(err))

// getTasksForUsersNew()
