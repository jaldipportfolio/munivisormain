export const rfpBidCheckList = [{
  checkListCategoryId: "Name of Issuer",
  checkListItems: [
    "Name and Brief Description",
    "Type of service being requested"
  ]
},
{
  checkListCategoryId: "Description and Issuer Summary",
  checkListItems: [
    "Info pertaining to financing plans",
    "Legal authority of issue debt",
    "Capital plans",
    "Planned issuances / refunding",
    "Ratings",
    "Outstanding Debt"
  ]
},
{
  checkListCategoryId: "Scope of Services",
  checkListItems: [
    "Specific services desired",
    "Duration of contract and options for renewal"
  ]
},
{
  checkListCategoryId: "Selection Criteria",
  checkListItems: [
    "Specific services desired",
    "Weighting criteria"
  ]
},
{
  checkListCategoryId: "Time Table to award ",
  checkListItems: [
    "Specific services desired",
    "Weighting criteria"
  ]
},
{
  checkListCategoryId: "Instructions for submitting proposals",
  checkListItems: [
    "Date and time due",
    "Weighting criteria"
  ]
},
{
  checkListCategoryId: "Miscellaneous",
  checkListItems: [
    "Terms and conditions",
    "Documents to conform to"
  ]
}
]

export const rfpEvaluationChecklist = [{
  categoryName: "Cost of Service",
  categoryItems: ["Cost of Service"]
},
{
  categoryName: "Qualifications of firm and key personnel",
  categoryItems: ["General Financing Approach", "Market for the Bonds"]
},
{
  categoryName: "Financing Approach",
  categoryItems: ["No law and regulations violation", "No conflicts of interest"]
}
]