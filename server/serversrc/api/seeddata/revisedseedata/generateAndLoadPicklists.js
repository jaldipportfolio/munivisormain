import  {
  Entity, Config
}  from "./models"

const csv = require("fast-csv")
const path = require("path")
const {ObjectID} = require("mongodb")

const getPickListFormattedValues = (dropdownvals) => {
  const formattedVals = dropdownvals.reduce( (finalInsert, {colName,value}) => {
    const insertObject =  {
      "label" : value,
      "included" : true,
      "visible" : true,
      "items" : []
    }
    if(finalInsert[colName]) {
      finalInsert[colName].items.push(insertObject)
    } else {
      finalInsert[colName] = {
        "title" : colName,
        "meta" : {
          "key":(new ObjectID()).toHexString(),
          "systemName" : colName,
          "subListLevel2" : false,
          "subListLevel3" : false,
          "systemConfig" : true,
          "restrictedList" : false,
          "externalList" : false,
          "bestPractice" : true
        },
        "items" : [ insertObject ]
      }
    }
    return finalInsert
  },{})
  return Object.values(formattedVals)
}

export const loadPickValuesForMuniVisorClients = async () =>{
  const dropdownValues=[]
  await csv
    .fromPath(path.resolve(path.join(__dirname, "picklists", "dropdownvalues1.csv")),{headers: true})
    .on("data", (data) => {
      dropdownValues.push(data)
    })
    .on("end", async () => {
      try {
        const formattedPicklists = await getPickListFormattedValues(dropdownValues)
        // get all entities that are munivisor clients
        const entities = await Entity.find({isMuniVisorClient:true}).select({_id:1,msrbFirmName:1 })

        const updatedObjects = await Promise.all (entities.map(async(ent) => {
          const upObj = await Config.findOneAndUpdate({entityId:ent._id}, {picklists:formattedPicklists},       {
            "new":true,
            "upsert": true,
            "runValidators": true
          })
          console.log("The config was updated", upObj.entityId)
          return upObj.entityId
        }))
        console.log("Successfully Loaded Picklists For all Financial Advisors", updatedObjects)

        // For each MuniVisor Client Insert the picklists values
      } catch (error) {
        console.log("There is an error",error)
      }
    })
}
// mongoose.connect("mongodb://127.0.0.1:27017/mvtrainingdev")
// loadPickValuesForMuniVisorClients().then(()=> console.log("Loaded Picklists")).catch(e => console.log(e))
