import * as models from "./models"

const { AuditLog, Entity, EntityUser, EntityRel, Config, GlobRefEntity,UserEntitlements,Deals, RFP,Tasks,ActBusDev,Others, BankLoans, Derivatives, ActMaRFP,Controls,ControlsActions,TenantBilling, BillingManageExpense, SearchPref }  = models
const { Docs,SuperVisoryObligations, ComplSupervisor,BusConduct,ProfQualifications,ComplaintDetails,GeneralAdmin,GiftsAndGratuities,PoliticalContributions,DmMapping,DocFolder,Messages }  = models

// Clean Up the Database
export const cleanAllCollections = async () => {
  try {
    await Promise.all([
      Entity.remove({}),
      EntityUser.remove({}),
      EntityRel.remove({}),
      Config.remove({}),
      GlobRefEntity.remove({}),
      UserEntitlements.remove({}),
      Deals.remove({}),
      RFP.remove({}),
      Tasks.remove({}),
      BankLoans.remove({}),
      Derivatives.remove({}),
      Others.remove({}),
      DmMapping.remove({}),
      DocFolder.remove({}),
      Messages.remove({}),
      BillingManageExpense.remove({}),
      ActMaRFP.remove({}),
      ActBusDev.remove({}),
      Controls.remove({}),
      ControlsActions.remove({}),
      TenantBilling.remove({}),
      AuditLog.remove({}),
      SuperVisoryObligations.remove({}),
      ComplSupervisor.remove({}),
      ProfQualifications.remove({}),
      PoliticalContributions.remove({}),
      GiftsAndGratuities.remove({}),
      GeneralAdmin.remove({}),
      ComplaintDetails.remove({}),
      BusConduct.remove({}),
      Docs.remove({}),
      SearchPref.remove({}),
    ])

    return "All collections have been removed{}"
  } catch (err) {
    console.log("There is an error in cleaning up DB", err)
    throw Error(err)
  }
}

/*
export {Deals} from "./deals"
export {Entity,GlobRefEntity} from "./entity"
export {EntityUser} from "./entityUser"
export {EntityRel} from "./entityRel"
export {RFP} from "./rfp"
export {Tasks} from "./taskmanagement"
export {Config} from "./config"
export {BankLoans,Derivatives,ActMaRFP,Others,ActBusDev} from "./activities"
export {SearchPref} from "./search"
export {TenantBilling, BillingManageExpense} from "./billing"
export {AuditLog} from "./auditlog"
export {Docs} from "./docs"
export {DocFolder} from "./docfolder"
export {DataOnboarding} from "./datamigration"
export {UserEntitlements} from "./entitlements"
export {Controls} from "./cac"
export {Person} from "./testsubdocuments"
*/
