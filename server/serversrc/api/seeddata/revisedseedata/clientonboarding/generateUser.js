import mongoose from "mongoose"
import {
  EntityUser
} from "../models"

mongoose.Promise = global.Promise

const defaultParams = {
  userAddOns:[],
  userDocuments: []
}

export const generateEntityUserData = (data) => {
  const { entityId, userFirmName, userFirstName, userMiddleName, userLastName,
    userEntitlement, address, phones, faxes, emails,userFlags } = data
  const userData = {
    _id: mongoose.Types.ObjectId(),
    entityId,
    userFirmName,
    userFirstName,
    userMiddleName,
    userFlags,
    userLastName,
    userRole: userEntitlement,
    userEntitlement,
    userAddresses: [{...address, addressName: "Office", isPrimary: true, isActive: true}],
    userPhone: [phones],
    userFax: [faxes],
    userEmails: [{ emailId: emails[0], emailPrimary: true }],
    userLoginCredentials: {
      userId:emails[0], // Should come from the user Collection
      userEmailId:emails[0], // We can get rid of this if we are able to just use the email ID.
      password: null,  // This is the salted password.
      onboardingStatus:"created",
      userEmailConfirmString:"",
      userEmailConfirmExpiry:null,
      passwordConfirmString:"",
      passwordConfirmExpiry:null,
      passwordResetIteration:0,
      passwordResetStatus:"",
      passwordResetDate: null,
      isUserSTPEligible:false,
      authSTPToken:null,
      authSTPPassword:null,
    }
  }
  return new EntityUser({ ...userData,
    ...defaultParams
  })
}
