import { generateConfigData } from "../generateConfigData"
import { loadPickValuesForOneFirm } from "../generateAndLoadPicklists1"
import { loadChecklistForOneFirm } from "../generateAndLoadChecklists1"
import { loadUIAccessInformationForOne } from "../generateAndLoadUIAccess"

const ENTITY_TYPES = ["Self", "Client", "Third Party", "Prospect", "PlatformAdmin"]

const addConfig = (entity) => generateConfigData(entity).save()

export const addAllConfigForOne = async (entity, type) => {
  console.log("starting config load for : ", entity._id, entity.firmName, type)
  await addConfig(entity)
  console.log("accessPolicy added")
  if(type === "Self" || type === "PlatformAdmin") {
    await loadPickValuesForOneFirm(entity)
    console.log("picklists added")
    await loadChecklistForOneFirm(entity)
    console.log("checklists added")
  } else {
    console.log("skipping picklists and checklists")
  }
  await loadUIAccessInformationForOne(entity._id)
  console.log("UI Access added")
}
