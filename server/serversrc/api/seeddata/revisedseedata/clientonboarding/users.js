export const users = [
  {
    "userEntitlement" : "global-edit",
    "userFirstName" : "Will",
    "userMiddleName" : "",
    "userLastName" : "Reed",
    emails: [
      "wreed@fordassocinc.com"
    ]
  },
  {
    "userEntitlement" : "global-edit",
    "userFirstName" : "Jon",
    "userMiddleName" : "",
    "userLastName" : "Ford",
    emails: [
      "jonford@fordassocinc.com"
    ]
  },
  {
    "userEntitlement" : "global-edit",
    "userFirstName" : "Jerry",
    "userMiddleName" : "",
    "userLastName" : "Ford",
    emails: [
      "jwford@fordassoc.com"
    ]
  },
  {
    "userEntitlement" : "global-edit",
    "userFirstName" : "Herminia",
    "userMiddleName" : "",
    "userLastName" : "Hohmann",
    emails: [
      "admin@fordassoc.com"
    ]
  },
  {
    "userEntitlement" : "global-edit",
    "userFirstName" : "Otaras",
    "userMiddleName" : "",
    "userLastName" : "Admin",
    emails: [
      "forduatadmin@otaras.com"
    ]
  }
]
