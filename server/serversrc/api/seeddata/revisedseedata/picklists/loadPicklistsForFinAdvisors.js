import { loadPickValuesForMuniVisorClientsLatest} from "./../generateAndLoadPicklists1"

const loadPicklists = async () => {
  try {
    const returnedValue = await loadPickValuesForMuniVisorClientsLatest()
    console.log("After Loading the Picklists", returnedValue)
  }
  catch (e) {
    console.log("Unable to load picklists", e)
  }
  finally {
    console.log("end")
  }
}

loadPicklists().then(a => process.exit(0))
// loadPickValuesForMuniVisorClientsLatest().then( a=> console.log(a)).then( () => console.log("done"))

