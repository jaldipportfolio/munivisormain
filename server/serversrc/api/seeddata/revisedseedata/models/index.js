import mongoose from "mongoose"

require("dotenv").config()


mongoose.Promise = global.Promise
// mongoose.connect("mongodb://admin:admin123@ds133746.mlab.com:33746/munivisordevsearch")
// mongoose.connect("mongodb://admin:admin123@ds115931.mlab.com:15931/munivisorqa1")
// mongoose.connect("mongodb://127.0.0.1:27017/testMunivisor")
// url: "mongodb://127.0.0.1:27017/testMunivisor"
// mongoose.connect("mongodb://admin:admin123@ds139929.mlab.com:39929/mvtrainingdev")
// mongoose.connect("mongodb://127.0.0.1:27017/mvtrainingdev")
// mongoose.connect("mongodb://52.91.222.27:27017/munivisor_dev")
// mongoose.connect("mongodb://52.91.222.27:27017/munivisor_qa_2")
// mongoose.connect("mongodb://52.91.102.91:27017/munivisor-dev")
// mongoose.connect("mongodb://admin:admin123@ds141421-a0.mlab.com:41421")
// mongoose.connect("mongodb://admin:admin123@ds115931.mlab.com:15931/munivisorqa1")
// mongoose.connect("mongodb://admin:admin123@ds141421-a0.mlab.com:41421,ds141421-a1.mlab.com:41421/munivisormaindev?replicaSet=rs-ds141421")
// mongoose.connect("mongodb://52.91.222.27:27017/munivisor_qa")
mongoose.set("useCreateIndex", true)
mongoose.connect(process.env.DB_URL,{ useNewUrlParser: true })

// export const mongooseDisconnect = mongoose.disconnect
export const dbCon = mongoose.connection

export {
  Entity, EntityRel, EntityUser, RFP, Deals, Tasks, Config, BankLoans,Derivatives,ActMaRFP,TenantBilling,AuditLog,ActBusDev,
  Others,
  BillingManageExpense,
  DmMapping,
  Messages,
  DocFolder,
  Notifications,
  GlobRefEntity,
  SuperVisoryObligations,
  ComplSupervisor,
  ProfQualifications,
  PoliticalContributions,
  GiftsAndGratuities,
  GeneralAdmin,
  ComplaintDetails,
  BusConduct,
  Controls,
  ControlsActions,
  UserEntitlements,
  PoliContributionDetails,
  PoliContributionSummary,
  Docs,
  SearchPref
}
  from "./../../../appresources/models"


