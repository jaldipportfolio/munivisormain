module.exports.LKUPACCRUEFROM = [
  {
    label: "Blank",
    value: "Blank"
  }, {
    label: "Dated Date",
    value: "Dated Date"
  }, {
    label: "Settlement Date",
    value: "Settlement Date"
  }
]
module.exports.LKUPADDRESSTYPE = [
  {
    label: "Default",
    value: "Default"
  }, {
    label: "Mailing",
    value: "Mailing"
  }, {
    label: "Other 1",
    value: "Other 1"
  }, {
    label: "Other 2",
    value: "Other 2"
  }
]
module.exports.LKUPAMT = [
  {
    label: "Yes",
    value: "Yes"
  }, {
    label: "No",
    value: "No"
  }, {
    label: "Other",
    value: "Other"
  }
]
module.exports.LKUPBANKQUALIFIED = [
  {
    label: "Yes",
    value: "Yes"
  }, {
    label: "No",
    value: "No"
  }, {
    label: "Other",
    value: "Other"
  }
]
module.exports.LKUPUSERROLE = [
  {
    label: "global-edit",
    value: "global-edit"
  }, {
    label: "global-readonly",
    value: "global-readonly"
  }, {
    label: "tran-edit",
    value: "tran-edit"
  }, {
    label: "tran-readonly",
    value: "tran-readonly"
  }, {
    label: "global-readonly-tran-edit",
    value: "global-readonly-tran-edit"
  }
]
module.exports.LKUPUSERFLAG = [
  {
    label: "Seris 50",
    value: "Seris 50"
  }, {
    label: "MSRB Contact",
    value: "MSRB Contact"
  }, {
    label: "EMMA Contact",
    value: "EMMA Contact"
  }, {
    label: "Supervisory Principal",
    value: "Supervisory Principal"
  }, {
    label: "Compliance Officer",
    value: "global-readonly-tran-edit"
  }, {
    label: "mfp",
    value: "mfp"
  }, {
    label: "Primary Contact",
    value: "Primary Contact"
  }, {
    label: "Procuremnt Contact",
    value: "Procuremnt Contact"
  }, {
    label: "Finance Contact",
    value: "Finance Contact"
  }, {
    label: "Dist Member",
    value: "Dist Member"
  }
]
module.exports.LKUPUSERENTITLEMENT = [
  {
    label: "global-edit",
    value: "global-edit"
  }, {
    label: "global-readonly",
    value: "global-readonly"
  }, {
    label: "tran-edit",
    value: "tran-edit"
  }, {
    label: "tran-readonly",
    value: "tran-readonly"
  }, {
    label: "global-readonly-tran-edit",
    value: "global-readonly-tran-edit"
  }
]
module.exports.LKUPPRIMARYSECTORS = [
  {
    label: "Primary Sector 1",
    value: "Primary Sector 1"
  }, {
    label: "Primary Sector 2",
    value: "Primary Sector 2"
  }
]
module.exports.LKUPSECONDARYSECTORS = [
  {
    label: "Secondary Sector 1",
    value: "Secondary Sector 1"
  }, {
    label: "Secondary Sector 2",
    value: "Secondary Sector 2"
  }
]
module.exports.LKUPISSUERFLAG = [
  {
    label: "State",
    value: "State"
  }, {
    label: "County",
    value: "County"
  }, {
    label: "Conduit",
    value: "Borrower"
  }, {
    label: "Schools",
    value: "Schools"
  }, {
    label: "Borrower",
    value: "Borrower"
  }
]
module.exports.SERVICETYPE = [
  {
    label: "Office 365",
    value: "Office 365"
  }, {
    label: "Docusign",
    value: "Docusign"
  }, {
    label: "EMMA",
    value: "EMMA"
  }, {
    label: "Analytics",
    value: "Analytics"
  }
]
module.exports.LKUPUANNUALREVENUE = [
  {
    label: "Less than 5 M",
    value: "Less than 5 M"
  }, {
    label: "5M to 20M",
    value: "5M to 20M"
  }, {
    label: "20M +",
    value: "20M +"
  }
]
module.exports.LKUPMARKETROLE = [
  {
    label: "Financial Advisors",
    value: "Financial Advisors"
  }, {
    label: "Reg Invst Advisors",
    value: "Reg Invst Advisors"
  }, {
    label: "Underwriting Services",
    value: "Underwriting Services"
  }, {
    label: "Rebate Services",
    value: "Rebate Services"
  }, {
    label: "Escrow Agents",
    value: "Escrow Agents"
  }, {
    label: "Printing & Binding",
    value: "Printing & Binding"
  }, {
    label: "Underwriter Counsel",
    value: "Underwriter Counsel"
  }, {
    label: "Bond Counsel",
    value: "Bond Counsel"
  }
]

module.exports.LKUPBORROWER = [
  {
    label: "Borrower-1",
    value: "Borrower-1"
  }, {
    label: "Borrower-2",
    value: "Borrower-2"
  }, {
    label: "Borrower-3",
    value: "Borrower-3"
  }, {
    label: "Borrower-4",
    value: "Borrower-4"
  }, {
    label: "Borrower-5",
    value: "Borrower-5"
  }, {
    label: "Borrower-6",
    value: "Borrower-6"
  }, {
    label: "Borrower-7",
    value: "Borrower-7"
  }, {
    label: "Borrower-8",
    value: "Borrower-8"
  }, {
    label: "Borrower-9",
    value: "Borrower-9"
  }, {
    label: "Borrower-10",
    value: "Borrower-10"
  }, {
    label: "Obligor-1",
    value: "Obligor-1"
  }, {
    label: "Obligor-2",
    value: "Obligor-2"
  }, {
    label: "Obligor-3",
    value: "Obligor-3"
  }, {
    label: "Obligor-4",
    value: "Obligor-4"
  }, {
    label: "Obligor-5",
    value: "Obligor-5"
  }, {
    label: "Obligor-6",
    value: "Obligor-6"
  }, {
    label: "Obligor-7",
    value: "Obligor-7"
  }, {
    label: "Obligor-8",
    value: "Obligor-8"
  }, {
    label: "Obligor-9",
    value: "Obligor-9"
  }, {
    label: "Obligor-10",
    value: "Obligor-10"
  }
]
module.exports.LKUPBUSSTRUCT = [
  {
    label: "Sole Proprietorship",
    value: "Sole Proprietorship"
  }, {
    label: "General Partnership",
    value: "General Partnership"
  }, {
    label: "Limited Partnership",
    value: "Limited Partnership"
  }, {
    label: "LLC",
    value: "LLC"
  }, {
    label: "LLP",
    value: "LLP"
  }, {
    label: "LLLP",
    value: "LLLP"
  }, {
    label: "Corporation Private",
    value: "Corporation Private"
  }, {
    label: "Corporation Public",
    value: "Corporation Public"
  }, {
    label: "Corporation Non Profit",
    value: "Corporation Non Profit"
  }, {
    label: "Trust",
    value: "Trust"
  }, {
    label: "Joint Venture",
    value: "Joint Venture"
  }, {
    label: "Tenants in Common",
    value: "Tenants in Common"
  }, {
    label: "Municipality",
    value: "Municipality"
  }, {
    label: "Governmental or Quasi",
    value: "Governmental or Quasi"
  }, {
    label: "NA",
    value: "NA"
  }
]
module.exports.LKUPCALLFEATURE = [
  {
    label: "Blank",
    value: "Blank"
  }, {
    label: "Callable",
    value: "Callable"
  }, {
    label: "Non Callable",
    value: "Non Callable"
  }
]
module.exports.LKUPCDCEVENTNOTICE = [
  {
    label: "Principal and interest payment delinquencies",
    value: "Principal and interest payment delinquencies"
  }, {
    label: "Non-payment related defaults",
    value: "Non-payment related defaults"
  }, {
    label: "Unscheduled draws on debt service reserves reflecting financial difficulties",
    value: "Unscheduled draws on debt service reserves reflecting financial difficulties"
  }, {
    label: "Unscheduled draws on credit enhancements reflecting financial difficulties",
    value: "Unscheduled draws on credit enhancements reflecting financial difficulties"
  }, {
    label: "Substitution of credit or liquidity providers, or their failure to perform",
    value: "Substitution of credit or liquidity providers, or their failure to perform"
  }, {
    label: "Adverse tax opinions or events affecting the tax-exempt status of the security",
    value: "Adverse tax opinions or events affecting the tax-exempt status of the security"
  }, {
    label: "Modifications to rights of security holders",
    value: "Modifications to rights of security holders"
  }, {
    label: "Bond calls and tender offers",
    value: "Bond calls and tender offers"
  }, {
    label: "Defeasances",
    value: "Defeasances"
  }, {
    label: "Release, substitution or sale of property securing repayment of the securities",
    value: "Release, substitution or sale of property securing repayment of the securities"
  }, {
    label: "Rating changes",
    value: "Rating changes"
  }, {
    label: "Bankruptcy, insolvency or receivership",
    value: "Bankruptcy, insolvency or receivership"
  }, {
    label: "Merger, acquisition or sale of all issuer assets",
    value: "Merger, acquisition or sale of all issuer assets"
  }, {
    label: "Appointment of successor trustee",
    value: "Appointment of successor trustee"
  }
]
module.exports.LKUPCDCFILINGRESPONSIBILITY = [
  {
    label: "NA",
    value: "NA"
  }, {
    label: "Self",
    value: "Self"
  }, {
    label: "Vendor",
    value: "Vendor"
  }, {
    label: "Self with Vendor Assist",
    value: "Self with Vendor Assist"
  }, {
    label: "Other",
    value: "Other"
  }
]
module.exports.LKUPCDCVOLADDTIONAL = [
  {
    label: "Amendment to continuing disclosure undertaking",
    value: "Amendment to continuing disclosure undertaking"
  }, {
    label: "Change in obligated person",
    value: "Change in obligated person"
  }, {
    label: "Notice to investors pursuant to bond documents",
    value: "Notice to investors pursuant to bond documents"
  }, {
    label: "Certain communications from the IRS (other than those included under SEC Rule 15" +
        "c2-12)",
    value: "Certain communications from the IRS (other than those included under SEC Rule 15" +
        "c2-12)"
  }, {
    label: "Bid for auction rate or other securities",
    value: "Bid for auction rate or other securities"
  }, {
    label: "Capital or other financing plan",
    value: "Capital or other financing plan"
  }, {
    label: "Litigationenforcement action",
    value: "Litigationenforcement action"
  }, {
    label: "Change of tender agent, remarketing agent, or other on-going party",
    value: "Change of tender agent, remarketing agent, or other on-going party"
  }, {
    label: "Derivative or other similar transaction",
    value: "Derivative or other similar transaction"
  }, {
    label: "Other event-based disclosures",
    value: "Other event-based disclosures"
  }
]
module.exports.LKUPCDCVOLFINANCIALINFO = [
  {
    label: "Quarterlymonthly financial information",
    value: "Quarterlymonthly financial information"
  }, {
    label: "Timing of annual disclosure (120150 days)",
    value: "Timing of annual disclosure (120150 days)"
  }, {
    label: "Change in fiscal yeartiming of annual disclosure",
    value: "Change in fiscal yeartiming of annual disclosure"
  }, {
    label: "Accounting standard (GAAP-GASBFASB)",
    value: "Accounting standard (GAAP-GASBFASB)"
  }, {
    label: "Change in accounting standard",
    value: "Change in accounting standard"
  }, {
    label: "Interimadditional financial informationoperating data",
    value: "Interimadditional financial informationoperating data"
  }, {
    label: "Budget",
    value: "Budget"
  }, {
    label: "Investmentdebtfinancial policy",
    value: "Investmentdebtfinancial policy"
  }, {
    label: "Material provided to rating agency or creditliquidity provider",
    value: "Material provided to rating agency or creditliquidity provider"
  }, {
    label: "Consultant reports",
    value: "Consultant reports"
  }, {
    label: "Other financialoperating data",
    value: "Other financialoperating data"
  }, {
    label: "Bank loan and alternative financing disclosures",
    value: "Bank loan and alternative financing disclosures"
  }
]
module.exports.LKUPCDCVOLOTHERINFO = [
  {
    label: "Timing of and accounting standard used for annual financial disclosure",
    value: "Timing of and accounting standard used for annual financial disclosure"
  }, {
    label: "Pre-sale documents, including preliminary official statements",
    value: "Pre-sale documents, including preliminary official statements"
  }, {
    label: "Investor and rating agency presentations",
    value: "Investor and rating agency presentations"
  }, {
    label: "Links to investor websites",
    value: "Links to investor websites"
  }
]
module.exports.LKUPCEP = [
  {
    label: "ACA Financial Guaranty Corporation",
    value: "ACA Financial Guaranty Corporation"
  }, {
    label: "Ambac Assurance Corp.",
    value: "Ambac Assurance Corp."
  }, {
    label: "Assured Guaranty Corp (AGC)",
    value: "Assured Guaranty Corp (AGC)"
  }, {
    label: "Assured Guaranty Municipal Corp (AGM)",
    value: "Assured Guaranty Municipal Corp (AGM)"
  }, {
    label: "Berkshire Hathaway Assurance Corp",
    value: "Berkshire Hathaway Assurance Corp"
  }, {
    label: "Build America Mutual (BAM)",
    value: "Build America Mutual (BAM)"
  }, {
    label: "CIFG Assurance North America Inc.",
    value: "CIFG Assurance North America Inc."
  }, {
    label: "Financial Guaranty Insurance Corp (FGIC)",
    value: "Financial Guaranty Insurance Corp (FGIC)"
  }, {
    label: "Financial Security Assurance (FSA)",
    value: "Financial Security Assurance (FSA)"
  }, {
    label: "Indiana State Aid Intercept Program",
    value: "Indiana State Aid Intercept Program"
  }, {
    label: "MBIA Insurance Corporation",
    value: "MBIA Insurance Corporation"
  }, {
    label: "Michigan School District Credit Enhancement Program",
    value: "Michigan School District Credit Enhancement Program"
  }, {
    label: "Minnesota School Credit Enhancement Program",
    value: "Minnesota School Credit Enhancement Program"
  }, {
    label: "Municipal Assurance Corp (MAC)",
    value: "Municipal Assurance Corp (MAC)"
  }, {
    label: "National Public Finance Guarantee Corp",
    value: "National Public Finance Guarantee Corp"
  }, {
    label: "New Jersey School Bond Reserve Act",
    value: "New Jersey School Bond Reserve Act"
  }, {
    label: "New Mexico School District Credit Enhancement Program",
    value: "New Mexico School District Credit Enhancement Program"
  }, {
    label: "Assured Guaranty Municipal Corp",
    value: "Assured Guaranty Municipal Corp"
  }, {
    label: "Build America Mutual Assurance Company",
    value: "Build America Mutual Assurance Company"
  }, {
    label: "PNC Bank, N.A.",
    value: "PNC Bank, N.A."
  }, {
    label: "MAC",
    value: "MAC"
  }, {
    label: "AGM",
    value: "AGM"
  }
]
module.exports.LKUPCLIENTCONTRACTTYPE = [
  {
    label: "FA",
    value: "FA"
  }, {
    label: "Underwriting",
    value: "Underwriting"
  }, {
    label: "SWAP",
    value: "SWAP"
  }, {
    label: "Placement",
    value: "Placement"
  }, {
    label: "Operating",
    value: "Operating"
  }, {
    label: "OPEB",
    value: "OPEB"
  }, {
    label: "OPEB Verbal",
    value: "OPEB Verbal"
  }, {
    label: "Engagement Letter",
    value: "Engagement Letter"
  }, {
    label: "Disclosure",
    value: "Disclosure"
  }, {
    label: "Arbitrage Rebate Compliance",
    value: "Arbitrage Rebate Compliance"
  }, {
    label: "Other",
    value: "Other"
  }
]
module.exports.LKUPCONFLICTOFINTEREST = [
  {
    label: "Yes",
    value: "Yes"
  }, {
    label: "No",
    value: "No"
  }, {
    label: "Other",
    value: "Other"
  }
]
module.exports.LKUPCONTACTTYPE = [
  {
    label: "Primary",
    value: "Primary"
  }, {
    label: "Secondary",
    value: "Secondary"
  }, {
    label: "G17",
    value: "G17"
  }, {
    label: "Disclosure Compliance",
    value: "Disclosure Compliance"
  }, {
    label: "Arbitrage Rebate Compliance",
    value: "Arbitrage Rebate Compliance"
  }, {
    label: "Other",
    value: "Other"
  }, {
    label: "NA",
    value: "NA"
  }
]
module.exports.LKUPCONTRACTRENEWAL = [
  {
    label: "Auto",
    value: "Auto"
  }, {
    label: "Perpetual",
    value: "Perpetual"
  }, {
    label: "Other",
    value: "Other"
  }
]
module.exports.LKUPCONTRACTSTATUS = [
  {
    label: "Active",
    value: "Active"
  }, {
    label: "Pending",
    value: "Pending"
  }, {
    label: "Expired",
    value: "Expired"
  }, {
    label: "To be obtained",
    value: "To be obtained"
  }, {
    label: "Terminated",
    value: "Terminated"
  }, {
    label: "Other",
    value: "Other"
  }, {
    label: "NA",
    value: "NA"
  }
]
module.exports.LKUPCORPTYPE = [
  {
    label: "Municipal",
    value: "Municipal"
  }, {
    label: "Corporate",
    value: "Corporate"
  }, {
    label: "Others",
    value: "Others"
  }, {
    label: "Blank",
    value: "Blank"
  }
]
module.exports.LKUPCORRESPONDENCEDOCS = [
  {
    label: "Hard Copy Correspondence (Letters etc.)",
    value: "Hard Copy Correspondence (Letters etc.)"
  }, {
    label: "Other",
    value: "Other"
  }
]
module.exports.LKUPCOSTSOFISSUANCE = [
  {
    label: "Underwriters Discount",
    value: "Underwriters Discount"
  }, {
    label: "Bond Counsel Fees and Expenses",
    value: "Bond Counsel Fees and Expenses"
  }, {
    label: "Financial AdvisorConsultant Fees and Expenses",
    value: "Financial AdvisorConsultant Fees and Expenses"
  }, {
    label: "Rating Agency",
    value: "Rating Agency"
  }, {
    label: "Bond Insurance",
    value: "Bond Insurance"
  }, {
    label: "Disclosure Counsel Fees and Expenses",
    value: "Disclosure Counsel Fees and Expenses"
  }, {
    label: "Underwriters Counsel Fees and Expenses",
    value: "Underwriters Counsel Fees and Expenses"
  }, {
    label: "Trustee, COI Agent, Paying Agent andor Escrow Agent Fee",
    value: "Trustee, COI Agent, Paying Agent andor Escrow Agent Fee"
  }, {
    label: "Printing",
    value: "Printing"
  }, {
    label: "Verification Agent",
    value: "Verification Agent"
  }, {
    label: "CUSIP Fees (if separate)",
    value: "CUSIP Fees (if separate)"
  }, {
    label: "Contingency",
    value: "Contingency"
  }
]
module.exports.LKUPCOUNTY = [
  {
    label: "County-1",
    value: "County-1"
  }, {
    label: "County-2",
    value: "County-2"
  }, {
    label: "County-3",
    value: "County-3"
  }, {
    label: "County-4",
    value: "County-4"
  }, {
    label: "County-5",
    value: "County-5"
  }, {
    label: "County-6",
    value: "County-6"
  }, {
    label: "County-7",
    value: "County-7"
  }, {
    label: "County-8",
    value: "County-8"
  }, {
    label: "County-9",
    value: "County-9"
  }, {
    label: "County-10",
    value: "County-10"
  }
]
module.exports.LKUPCOUPONFREQUENCY = [
  {
    label: "Blank",
    value: "Blank"
  }, {
    label: "Annual",
    value: "Annual"
  }, {
    label: "Semi Annual",
    value: "Semi Annual"
  }, {
    label: "Quarterly ",
    value: "Quarterly "
  }, {
    label: "Monthly",
    value: "Monthly"
  }, {
    label: "Weekly",
    value: "Weekly"
  }, {
    label: "Daily",
    value: "Daily"
  }
]
module.exports.LKUPCREDITENHANCEMENTTYPE = [
  {
    label: "Credit Enhancement Agreement",
    value: "Credit Enhancement Agreement"
  }, {
    label: "Letter of Credit",
    value: "Letter of Credit"
  }, {
    label: "Letter of Backing",
    value: "Letter of Backing"
  }, {
    label: "Others",
    value: "Others"
  }, {
    label: "Blank",
    value: "Blank"
  }
]
module.exports.LKUPCRMBORROWER = [
  {
    label: "Conduit Borrower",
    value: "Conduit Borrower"
  }, {
    label: "Obligor",
    value: "Obligor"
  }, {
    label: "Obligated Person",
    value: "Obligated Person"
  }, {
    label: "Other",
    value: "Other"
  }
]
module.exports.LKUPDAYCOUNT = [
  {
    label: "Blank",
    value: "Blank"
  }, {
    label: "30-360",
    value: "30-360"
  }, {
    label: "Actual-360",
    value: "Actual-360"
  }, {
    label: "Actual-365",
    value: "Actual-365"
  }, {
    label: "Actual-Actual",
    value: "Actual-Actual"
  }
]
module.exports.LKUPDEALFILECHECKLIST = [
  {
    label: "ENGAGEMENT REVIEW DOCUMENTS",
    value: "ENGAGEMENT REVIEW DOCUMENTS"
  }, {
    label: "MARKETING MATERIALS ",
    value: "MARKETING MATERIALS "
  }, {
    label: "G-17 DISCLOSURES ",
    value: "G-17 DISCLOSURES "
  }, {
    label: "DL",
    value: "DL"
  }, {
    label: "UNDERWRITERS COUNSEL DOCUMENTS",
    value: "UNDERWRITERS COUNSEL DOCUMENTS"
  }, {
    label: "DUE DILIGENCE DOCUMENTS",
    value: "DUE DILIGENCE DOCUMENTS"
  }, {
    label: "LEGAL OPINIONS",
    value: "LEGAL OPINIONS"
  }, {
    label: "RATING AGENCYINSURER DOCUMENTS",
    value: "RATING AGENCYINSURER DOCUMENTS"
  }, {
    label: "LEGAL DOCUMENTS",
    value: "LEGAL DOCUMENTS"
  }, {
    label: "RETAIL ORDER PERIOD RELATED",
    value: "RETAIL ORDER PERIOD RELATED"
  }, {
    label: "DISCLOSURE DOCUMENTS",
    value: "DISCLOSURE DOCUMENTS"
  }, {
    label: "UNDERWRITING COMMITMENT",
    value: "UNDERWRITING COMMITMENT"
  }, {
    label: "SALES MEMOPRICING ",
    value: "SALES MEMOPRICING "
  }, {
    label: "DELIVERY DOCUMENTS",
    value: "DELIVERY DOCUMENTS"
  }, {
    label: "CORRESPONDENCE ",
    value: "CORRESPONDENCE "
  }, {
    label: "FIRM REVENUE ",
    value: "FIRM REVENUE "
  }
]
module.exports.LKUPDEALOFFERING = [
  {
    label: "Negotiated",
    value: "Negotiated"
  }, {
    label: "Competitive",
    value: "Competitive"
  }, {
    label: "Private Placement-Bank Loan",
    value: "Private Placement-Bank Loan"
  }, {
    label: "Private Placement-Securities Offering",
    value: "Private Placement-Securities Offering"
  }, {
    label: "Other",
    value: "Other"
  }
]
module.exports.LKUPDELIVERYDOCS = [
  {
    label: "CUSIP Application",
    value: "CUSIP Application"
  }, {
    label: "Closing Memo",
    value: "Closing Memo"
  }, {
    label: "Closing Certificate",
    value: "Closing Certificate"
  }, {
    label: "Printers Certificate",
    value: "Printers Certificate"
  }
]
module.exports.LKUPDERIVATES = [
  {
    label: "Yes",
    value: "Yes"
  }, {
    label: "No",
    value: "No"
  }, {
    label: "Other",
    value: "Other"
  }
]
module.exports.LKUPDISCLOSERDOCUMENTS = [
  {
    label: "POS",
    value: "POS"
  }, {
    label: "OS",
    value: "OS"
  }, {
    label: "Completed G-32 Form",
    value: "Completed G-32 Form"
  }, {
    label: "Remarketing AgreementBroker Dealer Agreement",
    value: "Remarketing AgreementBroker Dealer Agreement"
  }
]
module.exports.LKUPDOCACTION = [
  {
    label: "Submit for review internally to",
    value: "Submit for review internally to"
  }, {
    label: "Submit for review externally to",
    value: "Submit for review externally to"
  }, {
    label: "Update document access entitlement",
    value: "Update document access entitlement"
  }, {
    label: "Share document (audited)",
    value: "Share document (audited)"
  }
]
module.exports.LKUPDOCSHARESTATUS = [
  {
    label: "Public",
    value: "Public"
  }, {
    label: "Private",
    value: "Private"
  }, {
    label: "Confidential",
    value: "Confidential"
  }
]
module.exports.LKUPDOCSTATUS = [
  {
    label: "Work in progress",
    value: "Work in progress"
  }, {
    label: "Draft",
    value: "Draft"
  }, {
    label: "Baselined",
    value: "Baselined"
  }, {
    label: "Submitted to Internal Review",
    value: "Submitted to Internal Review"
  }, {
    label: "Submitted to External Review",
    value: "Submitted to External Review"
  }, {
    label: "Reworking on Review Feedback",
    value: "Reworking on Review Feedback"
  }, {
    label: "Ready for submit to EMMA",
    value: "Ready for submit to EMMA"
  }, {
    label: "Ready for submit to IRS",
    value: "Ready for submit to IRS"
  }, {
    label: "Ready for submit to SEC",
    value: "Ready for submit to SEC"
  }, {
    label: "Ready for submit to Market Participants",
    value: "Ready for submit to Market Participants"
  }
]
module.exports.LKUPDOCTYPE = [
  {
    label: "Bond Purchase Agreement-Remarketing Agreement",
    value: "Bond Purchase Agreement-Remarketing Agreement"
  }, {
    label: "Continuing Disclosure Agreement or undertaking",
    value: "Continuing Disclosure Agreement or undertaking"
  }, {
    label: "Final Official Statement",
    value: "Final Official Statement"
  }, {
    label: "Issue Price Certificate",
    value: "Issue Price Certificate"
  }, {
    label: "Transcript",
    value: "Transcript"
  }, {
    label: "Underwriters Counsel Opinion (10b-5)",
    value: "Underwriters Counsel Opinion (10b-5)"
  }, {
    label: "Deal File Checklist",
    value: "Deal File Checklist"
  }, {
    label: "G-17 letters and acknowledgements",
    value: "G-17 letters and acknowledgements"
  }, {
    label: "Written Proof of Transaction Award",
    value: "Written Proof of Transaction Award"
  }, {
    label: "Issuer Counsel Opinion (10b-5)",
    value: "Issuer Counsel Opinion (10b-5)"
  }, {
    label: "Due Diligence Call Calendar Invite",
    value: "Due Diligence Call Calendar Invite"
  }, {
    label: "Accountants comfort letters",
    value: "Accountants comfort letters"
  }, {
    label: "Agreement Among Underwriters and Selling Group Agreement",
    value: "Agreement Among Underwriters and Selling Group Agreement"
  }, {
    label: "Any documents, notices or written disclosures provided to customers",
    value: "Any documents, notices or written disclosures provided to customers"
  }, {
    label: "Bond Insurance",
    value: "Bond Insurance"
  }, {
    label: "Case Study",
    value: "Case Study"
  }, {
    label: "Closing certificates and exhibits including board resolutions",
    value: "Closing certificates and exhibits including board resolutions"
  }, {
    label: "Closing Memorandum",
    value: "Closing Memorandum"
  }, {
    label: "Committee memos",
    value: "Committee memos"
  }, {
    label: "Continuing Disclosure Review",
    value: "Continuing Disclosure Review"
  }, {
    label: "Copy of Bids",
    value: "Copy of Bids"
  }, {
    label: "Cross receipt",
    value: "Cross receipt"
  }, {
    label: "CUSIPs",
    value: "CUSIPs"
  }, {
    label: "EMMA submission confirmation",
    value: "EMMA submission confirmation"
  }, {
    label: "Engagement Notification",
    value: "Engagement Notification"
  }, {
    label: "Final distribution analysis",
    value: "Final distribution analysis"
  }, {
    label: "Final Labels for printer",
    value: "Final Labels for printer"
  }, {
    label: "Final Operative Financing Documents",
    value: "Final Operative Financing Documents"
  }, {
    label: "Final Pricing Book",
    value: "Final Pricing Book"
  }, {
    label: "Form G-32 submissions",
    value: "Form G-32 submissions"
  }, {
    label: "Form of Disclaimers",
    value: "Form of Disclaimers"
  }, {
    label: "Form of G-17 Letters",
    value: "Form of G-17 Letters"
  }, {
    label: "Form of MUCC submissions",
    value: "Form of MUCC submissions"
  }, {
    label: "Form of the Bonds-Notes",
    value: "Form of the Bonds-Notes"
  }, {
    label: "Investor Road Show Materials",
    value: "Investor Road Show Materials"
  }, {
    label: "Ipreo Deal File",
    value: "Ipreo Deal File"
  }, {
    label: "Ipreo Wires",
    value: "Ipreo Wires"
  }, {
    label: "IRS Determination Letters",
    value: "IRS Determination Letters"
  }, {
    label: "Legal Opinions",
    value: "Legal Opinions"
  }, {
    label: "Legal-Tax research memoranda",
    value: "Legal-Tax research memoranda"
  }, {
    label: "Letter of Credit -SBPA-Line of Credit Documents",
    value: "Letter of Credit -SBPA-Line of Credit Documents"
  }, {
    label: "NIIDS submissions",
    value: "NIIDS submissions"
  }, {
    label: "Notice of Sale",
    value: "Notice of Sale"
  }, {
    label: "Orders and Allotments",
    value: "Orders and Allotments"
  }, {
    label: "PnL Form",
    value: "PnL Form"
  }, {
    label: "PnL Statements",
    value: "PnL Statements"
  }, {
    label: "Preliminary Official Statement",
    value: "Preliminary Official Statement"
  }, {
    label: "Press reports and Advertisements",
    value: "Press reports and Advertisements"
  }, {
    label: "Proof of check-wire",
    value: "Proof of check-wire"
  }, {
    label: "Rating agency letters",
    value: "Rating agency letters"
  }, {
    label: "Rating Agency presentation",
    value: "Rating Agency presentation"
  }, {
    label: "Receipt of Bonds",
    value: "Receipt of Bonds"
  }, {
    label: "Records re: anomalous priority-allotment decisions",
    value: "Records re: anomalous priority-allotment decisions"
  }, {
    label: "Regulatory Requirements Checklist",
    value: "Regulatory Requirements Checklist"
  }, {
    label: "RFP-RFQ and Responses to RFP-RFQ",
    value: "RFP-RFQ and Responses to RFP-RFQ"
  }, {
    label: "Selling-Sales Point memorandum",
    value: "Selling-Sales Point memorandum"
  }, {
    label: "Tax Documents",
    value: "Tax Documents"
  }, {
    label: "Working Group List",
    value: "Working Group List"
  }, {
    label: "Presale-Blank",
    value: "Presale-Blank"
  }, {
    label: "Presale-Presentations",
    value: "Presale-Presentations"
  }, {
    label: "Presale-RFP Responses",
    value: "Presale-RFP Responses"
  }, {
    label: "Presale-Refunding Letters",
    value: "Presale-Refunding Letters"
  }, {
    label: "Presale-Proposals",
    value: "Presale-Proposals"
  }, {
    label: "Presale-RFQ Responses",
    value: "Presale-RFQ Responses"
  }, {
    label: "Engagement Review-Engagement Review Submission Form",
    value: "Engagement Review-Engagement Review Submission Form"
  }, {
    label: "Engagement Review-Meeeting notes listing engagement",
    value: "Engagement Review-Meeeting notes listing engagement"
  }, {
    label: "Engagement Review-Documentation evidencing applicable Municipal Advisor exemptio" +
        "n-IRMA, Underwriter",
    value: "Engagement Review-Documentation evidencing applicable Municipal Advisor exemptio" +
        "n-IRMA, Underwriter"
  }, {
    label: "Engagement Review-Distribution List",
    value: "Engagement Review-Distribution List"
  }, {
    label: "Underwritring Counsel-Documentation associated with the selection of underwriter" +
        "s counsel, if applicable.",
    value: "Underwritring Counsel-Documentation associated with the selection of underwriter" +
        "s counsel, if applicable."
  }, {
    label: "Underwritring Counsel-Documentation that underwriters counsel was vetted through" +
        " bank legal department.",
    value: "Underwritring Counsel-Documentation that underwriters counsel was vetted through" +
        " bank legal department."
  }, {
    label: "Underwritring Counsel-Documentation of disclosure in OS, that Underwriters Couns" +
        "el was retained optional",
    value: "Underwritring Counsel-Documentation of disclosure in OS, that Underwriters Couns" +
        "el was retained optional"
  }, {
    label: "Due Diligence-Due Diligence Worksheet (Including any  all accompanying materials" +
        ")",
    value: "Due Diligence-Due Diligence Worksheet (Including any  all accompanying materials" +
        ")"
  }, {
    label: "Due Diligence-Due Diligence review",
    value: "Due Diligence-Due Diligence review"
  }, {
    label: "Due Diligence-Continuing Disclosure Agreement",
    value: "Due Diligence-Continuing Disclosure Agreement"
  }, {
    label: "Due Diligence-Issuer continuing disclosure procedures",
    value: "Due Diligence-Issuer continuing disclosure procedures"
  }, {
    label: "Due Diligence-Sources and Uses Docs (Verification report, Construction Budgets, " +
        "Insurance verification, COI detail, SLUG application)",
    value: "Due Diligence-Sources and Uses Docs (Verification report, Construction Budgets, " +
        "Insurance verification, COI detail, SLUG application)"
  }, {
    label: "Rating Agency / Insurer-Credit Report",
    value: "Rating Agency / Insurer-Credit Report"
  }, {
    label: "Rating Agency / Insurer-Insurance Letter",
    value: "Rating Agency / Insurer-Insurance Letter"
  }, {
    label: "Rating Agency / Insurer-LOC documents",
    value: "Rating Agency / Insurer-LOC documents"
  }, {
    label: "Legal-Underwriters certificate",
    value: "Legal-Underwriters certificate"
  }, {
    label: "Legal-Document CD (in folder)",
    value: "Legal-Document CD (in folder)"
  }, {
    label: "Disclosure-POS",
    value: "Disclosure-POS"
  }, {
    label: "Disclosure-OS",
    value: "Disclosure-OS"
  }, {
    label: "Disclosure-Completed G-32 Form",
    value: "Disclosure-Completed G-32 Form"
  }, {
    label: "Disclosure-Remarketing AgreementBroker Dealer Agreement",
    value: "Disclosure-Remarketing AgreementBroker Dealer Agreement"
  }, {
    label: "Underwriter Commitment-Bond Purchase Contract",
    value: "Underwriter Commitment-Bond Purchase Contract"
  }, {
    label: "Underwriter Commitment-Agreement Among Underwriters -NA if Sole",
    value: "Underwriter Commitment-Agreement Among Underwriters -NA if Sole"
  }, {
    label: "Underwriter Commitment-Underwriters Counsel Fee Paid",
    value: "Underwriter Commitment-Underwriters Counsel Fee Paid"
  }, {
    label: "Sale Pricing-Tan Sheet",
    value: "Sale Pricing-Tan Sheet"
  }, {
    label: "Sale Pricing-Committee Approval (minutes it was approved)",
    value: "Sale Pricing-Committee Approval (minutes it was approved)"
  }, {
    label: "Sale Pricing-Desk Wires",
    value: "Sale Pricing-Desk Wires"
  }, {
    label: "Sale Pricing-Pricing book andor final #s",
    value: "Sale Pricing-Pricing book andor final #s"
  }, {
    label: "Sale Pricing-Blue Sky Memo",
    value: "Sale Pricing-Blue Sky Memo"
  }, {
    label: "Sale Pricing-Deal information sheet  (DIS)",
    value: "Sale Pricing-Deal information sheet  (DIS)"
  }, {
    label: "Delivery-CUSIP Application",
    value: "Delivery-CUSIP Application"
  }, {
    label: "Delivery-Closing Memo",
    value: "Delivery-Closing Memo"
  }, {
    label: "Delivery-Closing Certificate",
    value: "Delivery-Closing Certificate"
  }, {
    label: "Delivery-Printers Certificate",
    value: "Delivery-Printers Certificate"
  }, {
    label: "Underwriting Revenue-Profit Statement",
    value: "Underwriting Revenue-Profit Statement"
  }, {
    label: "Underwriting Revenue-Allocation letter",
    value: "Underwriting Revenue-Allocation letter"
  }, {
    label: "Underwriting Revenue-Designation letter",
    value: "Underwriting Revenue-Designation letter"
  }
]
module.exports.LKUPDOLLARRANGE = [
  {
    label: "less than USD 1M",
    value: "less than USD 1M"
  }, {
    label: "less than USD 5M",
    value: "less than USD 5M"
  }, {
    label: "less than USD 10M",
    value: "less than USD 10M"
  }, {
    label: "less than USD 50M",
    value: "less than USD 50M"
  }, {
    label: "less than USD 100M",
    value: "less than USD 100M"
  }, {
    label: "greater than USD 100M",
    value: "greater than USD 100M"
  }, {
    label: "NA",
    value: "NA"
  }
]
module.exports.LKUPDUEDILIGENCEDOCUMENTS = [
  {
    label: "Due Diligence Worksheet (Including any  all accompanying materials)",
    value: "Due Diligence Worksheet (Including any  all accompanying materials)"
  }, {
    label: "Due Diligence review",
    value: "Due Diligence review"
  }, {
    label: "Continuing Disclosure Agreement",
    value: "Continuing Disclosure Agreement"
  }, {
    label: "Issuer continuing disclosure procedures",
    value: "Issuer continuing disclosure procedures"
  }, {
    label: "Sources and Uses Docs (Verification report, Construction Budgets, Insurance veri" +
        "fication, COI detail, SLUG application)",
    value: "Sources and Uses Docs (Verification report, Construction Budgets, Insurance veri" +
        "fication, COI detail, SLUG application)"
  }
]
module.exports.LKUPENGAGEMENTREVIEWDOCUMENTS = [
  {
    label: "Engagement Review Submission Form",
    value: "Engagement Review Submission Form"
  }, {
    label: "Meeeting notes listing engagement",
    value: "Meeeting notes listing engagement"
  }, {
    label: "Documentation evidencing applicable Municipal Advisor exemption-IRMA, Underwrite" +
        "r",
    value: "Documentation evidencing applicable Municipal Advisor exemption-IRMA, Underwrite" +
        "r"
  }, {
    label: "Distribution List",
    value: "Distribution List"
  }
]
module.exports.LKUPFAACTIVITYTYPE = [
  {
    label: "Analysis",
    value: "Analysis"
  }, {
    label: "Conference Call",
    value: "Conference Call"
  }, {
    label: "Financial Planning",
    value: "Financial Planning"
  }, {
    label: "Legal",
    value: "Legal"
  }, {
    label: "Market Update",
    value: "Market Update"
  }, {
    label: "Meeting",
    value: "Meeting"
  }, {
    label: "Meeting Attendance",
    value: "Meeting Attendance"
  }, {
    label: "Other",
    value: "Other"
  }, {
    label: "Pricing ",
    value: "Pricing "
  }, {
    label: "Public Private Partnership",
    value: "Public Private Partnership"
  }, {
    label: "Rating Agency Related",
    value: "Rating Agency Related"
  }, {
    label: "Research",
    value: "Research"
  }, {
    label: "Review",
    value: "Review"
  }, {
    label: "RFP Related",
    value: "RFP Related"
  }, {
    label: "Special Project",
    value: "Special Project"
  }, {
    label: "Structured Finance",
    value: "Structured Finance"
  }, {
    label: "Travel",
    value: "Travel"
  }
]
module.exports.LKUPFAENGAGEDINDEAL = [
  {
    label: "Yes",
    value: "Yes"
  }, {
    label: "No",
    value: "No"
  }, {
    label: "Other",
    value: "Other"
  }
]
module.exports.LKUPFAFEECONTRACTTYPE = [
  {
    label: "Rate - In Percentage per USD 1000 Par",
    value: "Rate - In Percentage per USD 1000 Par"
  }, {
    label: "Rate - In USD per USD 1000 Par",
    value: "Rate - In USD per USD 1000 Par"
  }, {
    label: "Hourly",
    value: "Hourly"
  }
]
module.exports.LKUPFEDTAX = [
  {
    label: "Taxable",
    value: "Taxable"
  }, {
    label: "Tax-Exempt",
    value: "Tax-Exempt"
  }, {
    label: "Other",
    value: "Other"
  }
]
module.exports.LKUPFIRMNAME = [
  {
    label: "K0104-A. M. Miller & Co., Inc",
    value: "K0104-A. M. Miller & Co., Inc"
  }, {
    label: "K0205-A. M. Peche & Associates LLC",
    value: "K0205-A. M. Peche & Associates LLC"
  }, {
    label: "A4892-A.Bridge-Realvest Securities Corp.",
    value: "A4892-A.Bridge-Realvest Securities Corp."
  }, {
    label: "K0116-Acacia Financial Group, Inc.",
    value: "K0116-Acacia Financial Group, Inc."
  }, {
    label: "K0118-Acacia Investment Advisory Group, Inc.",
    value: "K0118-Acacia Investment Advisory Group, Inc."
  }, {
    label: "K1155-Advanced Municipal Planners LLC",
    value: "K1155-Advanced Municipal Planners LLC"
  }, {
    label: "K1086-AE2S Nexus, LLC",
    value: "K1086-AE2S Nexus, LLC"
  }, {
    label: "K1075-AEM Financial Solutions,LLC",
    value: "K1075-AEM Financial Solutions,LLC"
  }, {
    label: "K0479-AKF Consulting LLC",
    value: "K0479-AKF Consulting LLC"
  }, {
    label: "A3686-Alamo Capital",
    value: "A3686-Alamo Capital"
  }, {
    label: "K1164-All American Municipal Advisors, LLC",
    value: "K1164-All American Municipal Advisors, LLC"
  }, {
    label: "K1202-Alpine Valley Advisors LLC",
    value: "K1202-Alpine Valley Advisors LLC"
  }, {
    label: "K1181-Alton Health Advisors LLC",
    value: "K1181-Alton Health Advisors LLC"
  }, {
    label: "K1203-AMD Capital, LLC",
    value: "K1203-AMD Capital, LLC"
  }, {
    label: "K0353-American Deposit Management LLC",
    value: "K0353-American Deposit Management LLC"
  }, {
    label: "A2110-Ameritas Investment Corp.",
    value: "A2110-Ameritas Investment Corp."
  }, {
    label: "K0852-AMKO Advisors, LLC",
    value: "K0852-AMKO Advisors, LLC"
  }, {
    label: "K0315-Anne Marie Donahoe",
    value: "K0315-Anne Marie Donahoe"
  }, {
    label: "K1010-Applied Best Practices, LLC",
    value: "K1010-Applied Best Practices, LLC"
  }, {
    label: "K1145-Argent Trust Co",
    value: "K1145-Argent Trust Co"
  }, {
    label: "A7413-ARK Global LLC",
    value: "A7413-ARK Global LLC"
  }, {
    label: "K0448-Armadale Capital Inc.",
    value: "K0448-Armadale Capital Inc."
  }, {
    label: "K0177-Arrow Partners",
    value: "K0177-Arrow Partners"
  }, {
    label: "K0299-Arup Advisory, Inc. ",
    value: "K0299-Arup Advisory, Inc. "
  }, {
    label: "K0580-Ascension Capital Enterprises, LLC",
    value: "K0580-Ascension Capital Enterprises, LLC"
  }, {
    label: "K0481-Austin Meade Financial Ltd",
    value: "K0481-Austin Meade Financial Ltd"
  }, {
    label: "K0563-Avant Energy, Inc.",
    value: "K0563-Avant Energy, Inc."
  }, {
    label: "A6194-AVM, L.P.",
    value: "A6194-AVM, L.P."
  }, {
    label: "A6298-Backstrom McCarley Berry & Co., LLC",
    value: "A6298-Backstrom McCarley Berry & Co., LLC"
  }, {
    label: "A0081-Baker Group LP",
    value: "A0081-Baker Group LP"
  }, {
    label: "K1027-Baker Tilly Municipal Advisors, LLC",
    value: "K1027-Baker Tilly Municipal Advisors, LLC"
  }, {
    label: "K0904-Barbara A. Lloyd",
    value: "K0904-Barbara A. Lloyd"
  }, {
    label: "A6566-Barclays Capital Inc.",
    value: "A6566-Barclays Capital Inc."
  }, {
    label: "K0414-Bartle Wells Associates",
    value: "K0414-Bartle Wells Associates"
  }, {
    label: "A6823-BB&T Securities, LLC",
    value: "A6823-BB&T Securities, LLC"
  }, {
    label: "K1033-Beacon Lights  Research & Advisory, LLC",
    value: "K1033-Beacon Lights  Research & Advisory, LLC"
  }, {
    label: "K0762-Becker Capital & Finance LLC",
    value: "K0762-Becker Capital & Finance LLC"
  }, {
    label: "A4477-Beekman Securities, Inc.",
    value: "A4477-Beekman Securities, Inc."
  }, {
    label: "K0220-Bendzinski & Co.",
    value: "K0220-Bendzinski & Co."
  }, {
    label: "A6963-Benjamin F. Edwards & Company",
    value: "A6963-Benjamin F. Edwards & Company"
  }, {
    label: "K0211-Bernard P. Donegan, Inc.",
    value: "K0211-Bernard P. Donegan, Inc."
  }, {
    label: "K0950-BJH Advisors LLC",
    value: "K0950-BJH Advisors LLC"
  }, {
    label: "K1013-BlackRock Institutional Trust Company, N.A. ",
    value: "K1013-BlackRock Institutional Trust Company, N.A. "
  }, {
    label: "K0186-Blitch Associates, Inc.",
    value: "K0186-Blitch Associates, Inc."
  }, {
    label: "K0611-Blue Rose Capital Advisors, LLC",
    value: "K0611-Blue Rose Capital Advisors, LLC"
  }, {
    label: "K0143-BLX Group LLC",
    value: "K0143-BLX Group LLC"
  }, {
    label: "A4685-BNY Mellon Capital Markets, LLC",
    value: "A4685-BNY Mellon Capital Markets, LLC"
  }, {
    label: "A0156-Boenning & Scattergood, Inc.",
    value: "A0156-Boenning & Scattergood, Inc."
  }, {
    label: "A4700-BOK Financial Securities, Inc.",
    value: "A4700-BOK Financial Securities, Inc."
  }, {
    label: "K0105-Bradley Payne LLC",
    value: "K0105-Bradley Payne LLC"
  }, {
    label: "K0653-Bretwood Capital Partners",
    value: "K0653-Bretwood Capital Partners"
  }, {
    label: "K1152-BRG Fund Management Services, LLC",
    value: "K1152-BRG Fund Management Services, LLC"
  }, {
    label: "K0984-Bridge Strategic Partners LLC",
    value: "K0984-Bridge Strategic Partners LLC"
  }, {
    label: "K0583-Bridgeport Partners, LLC ",
    value: "K0583-Bridgeport Partners, LLC "
  }, {
    label: "K1208-Bright Bay Advisors, LLC",
    value: "K1208-Bright Bay Advisors, LLC"
  }, {
    label: "K1056-Brookhurst Development  Corporation",
    value: "K1056-Brookhurst Development  Corporation"
  }, {
    label: "K0746-Brown Advisory LLC",
    value: "K0746-Brown Advisory LLC"
  }, {
    label: "K0541-Buck Financial Advisors LLC",
    value: "K0541-Buck Financial Advisors LLC"
  }, {
    label: "K1190-Busey Trust CO",
    value: "K1190-Busey Trust CO"
  }, {
    label: "K0549-C Financial Investment Inc.",
    value: "K0549-C Financial Investment Inc."
  }, {
    label: "K0128-C.M.de Crinis & Co., Inc.",
    value: "K0128-C.M.de Crinis & Co., Inc."
  }, {
    label: "A4470-Cabrera Capital Markets, LLC",
    value: "A4470-Cabrera Capital Markets, LLC"
  }, {
    label: "K1037-Cadence Bank, N.A.",
    value: "K1037-Cadence Bank, N.A."
  }, {
    label: "A1793-Cain Brothers & Co.",
    value: "A1793-Cain Brothers & Co."
  }, {
    label: "K0184-Caine Mitter & Associates Incorporated",
    value: "K0184-Caine Mitter & Associates Incorporated"
  }, {
    label: "K0214-Caldwell Flores Winters, Inc",
    value: "K0214-Caldwell Flores Winters, Inc"
  }, {
    label: "K0148-Calhoun, Baker Inc.",
    value: "K0148-Calhoun, Baker Inc."
  }, {
    label: "K0489-Capital Markets Advisors LLC",
    value: "K0489-Capital Markets Advisors LLC"
  }, {
    label: "K0526-Capital Markets Management, L.L.C.",
    value: "K0526-Capital Markets Management, L.L.C."
  }, {
    label: "K0554-Capitol Public Finance Group, LLC",
    value: "K0554-Capitol Public Finance Group, LLC"
  }, {
    label: "K0362-CapM Funding",
    value: "K0362-CapM Funding"
  }, {
    label: "K0421-Cardea Partners East, LLC",
    value: "K0421-Cardea Partners East, LLC"
  }, {
    label: "K0862-Carter Dillon Umbaugh LLC",
    value: "K0862-Carter Dillon Umbaugh LLC"
  }, {
    label: "K0255-CaseCon Capital Inc",
    value: "K0255-CaseCon Capital Inc"
  }, {
    label: "K0985-Castle Advisory Co LLC",
    value: "K0985-Castle Advisory Co LLC"
  }, {
    label: "K0193-Causey Demgen & Moore P.C.",
    value: "K0193-Causey Demgen & Moore P.C."
  }, {
    label: "K0447-Cedar Partners, ltd.",
    value: "K0447-Cedar Partners, ltd."
  }, {
    label: "K0401-Cender & Company, L.L.C.",
    value: "K0401-Cender & Company, L.L.C."
  }, {
    label: "K1169-CFW Advisory Services, LLC",
    value: "K1169-CFW Advisory Services, LLC"
  }, {
    label: "K0138-cfX Inc",
    value: "K0138-cfX Inc"
  }, {
    label: "K1011-Charles Edward Hampel",
    value: "K1011-Charles Edward Hampel"
  }, {
    label: "K1076-Charter School Services Corp",
    value: "K1076-Charter School Services Corp"
  }, {
    label: "K1036-Chatham Hedging Advisors, LLC",
    value: "K1036-Chatham Hedging Advisors, LLC"
  }, {
    label: "K0515-CIM Investment Management, Inc",
    value: "K0515-CIM Investment Management, Inc"
  }, {
    label: "K0356-Citycode Financial LLC",
    value: "K0356-Citycode Financial LLC"
  }, {
    label: "K0635-Clary Consulting Co.",
    value: "K0635-Clary Consulting Co."
  }, {
    label: "K0953-CLB Porter, LLC",
    value: "K0953-CLB Porter, LLC"
  }, {
    label: "A7104-Clean Energy Capital Securities LLC",
    value: "A7104-Clean Energy Capital Securities LLC"
  }, {
    label: "K1174-Clearwater Financial, LLC",
    value: "K1174-Clearwater Financial, LLC"
  }, {
    label: "K1053-CliftonLarsonAllen Municipal Advisors LLC",
    value: "K1053-CliftonLarsonAllen Municipal Advisors LLC"
  }, {
    label: "K0183-Columbia Capital Management, LLC",
    value: "K0183-Columbia Capital Management, LLC"
  }, {
    label: "K0252-Comer Capital Group, LLC",
    value: "K0252-Comer Capital Group, LLC"
  }, {
    label: "K0832-Commonwealth Economics Partners, LLC",
    value: "K0832-Commonwealth Economics Partners, LLC"
  }, {
    label: "K0287-Community Capital",
    value: "K0287-Community Capital"
  }, {
    label: "K1215-Community Concepts Group, Inc.",
    value: "K1215-Community Concepts Group, Inc."
  }, {
    label: "K0270-Community Development Associates",
    value: "K0270-Community Development Associates"
  }, {
    label: "K1063-Compass Municipal Advisors, LLC",
    value: "K1063-Compass Municipal Advisors, LLC"
  }, {
    label: "K0285-Concord Public Financial Advisors, Inc.",
    value: "K0285-Concord Public Financial Advisors, Inc."
  }, {
    label: "A3434-Cooper Malone McClain, Inc.",
    value: "A3434-Cooper Malone McClain, Inc."
  }, {
    label: "K0630-Cooperative Strategies, LLC",
    value: "K0630-Cooperative Strategies, LLC"
  }, {
    label: "K1179-Corcoran Consulting",
    value: "K1179-Corcoran Consulting"
  }, {
    label: "A0323-Coughlin & Company Inc.",
    value: "A0323-Coughlin & Company Inc."
  }, {
    label: "A0333-Crews & Associates, Inc.",
    value: "A0333-Crews & Associates, Inc."
  }, {
    label: "K0827-CRF Financial Group, Inc.",
    value: "K0827-CRF Financial Group, Inc."
  }, {
    label: "K0842-Crito Capital LLC",
    value: "K0842-Crito Capital LLC"
  }, {
    label: "K0222-Crowe Horwath LLP",
    value: "K0222-Crowe Horwath LLP"
  }, {
    label: "K0779-Crystal Financial Consultants, Inc.",
    value: "K0779-Crystal Financial Consultants, Inc."
  }, {
    label: "K0547-CSG Advisors Incorporated",
    value: "K0547-CSG Advisors Incorporated"
  }, {
    label: "K1173-CTBH Partners LLC",
    value: "K1173-CTBH Partners LLC"
  }, {
    label: "K0551-Cumberland Advisors",
    value: "K0551-Cumberland Advisors"
  }, {
    label: "K0221-Cumberland Partners LLC",
    value: "K0221-Cumberland Partners LLC"
  }, {
    label: "K0811-Cumberland Securities Company, Inc.",
    value: "K0811-Cumberland Securities Company, Inc."
  }, {
    label: "K0568-D & G Consulting Group, LLC",
    value: "K0568-D & G Consulting Group, LLC"
  }, {
    label: "A0365-DA Davidson & Company",
    value: "A0365-DA Davidson & Company"
  }, {
    label: "K0944-DA Group, Inc.",
    value: "K0944-DA Group, Inc."
  }, {
    label: "K0586-Dale Scott & Company Inc.",
    value: "K0586-Dale Scott & Company Inc."
  }, {
    label: "A0364-Davenport & Company LLC",
    value: "A0364-Davenport & Company LLC"
  }, {
    label: "K0319-David Drown Associates, Inc.",
    value: "K0319-David Drown Associates, Inc."
  }, {
    label: "K0961-David Taussig and Associates, Inc",
    value: "K0961-David Taussig and Associates, Inc"
  }, {
    label: "K0185-DEC Associates Inc",
    value: "K0185-DEC Associates Inc"
  }, {
    label: "K0108-Del Rio Advisors, LLC",
    value: "K0108-Del Rio Advisors, LLC"
  }, {
    label: "K1101-Derivative Advisors, LLC",
    value: "K1101-Derivative Advisors, LLC"
  }, {
    label: "K1105-Derivative Logic Inc.",
    value: "K1105-Derivative Logic Inc."
  }, {
    label: "K1122-Derivatives Consulting LLC",
    value: "K1122-Derivatives Consulting LLC"
  }, {
    label: "K0413-Development & Public Finance, LLC",
    value: "K0413-Development & Public Finance, LLC"
  }, {
    label: "K0958-Development Planning and Financing Group, Inc",
    value: "K0958-Development Planning and Financing Group, Inc"
  }, {
    label: "K0615-DiPerna & Company, LLC.",
    value: "K0615-DiPerna & Company, LLC."
  }, {
    label: "K1069-Disclosure Advisors LLC",
    value: "K1069-Disclosure Advisors LLC"
  }, {
    label: "K0102-DIXWORKS LLC",
    value: "K0102-DIXWORKS LLC"
  }, {
    label: "A1329-Dougherty & Company LLC",
    value: "A1329-Dougherty & Company LLC"
  }, {
    label: "A7115-Drexel Hamilton, LLC",
    value: "A7115-Drexel Hamilton, LLC"
  }, {
    label: "K0227-Dunlap & Associates, Inc.",
    value: "K0227-Dunlap & Associates, Inc."
  }, {
    label: "B0446-Eastern Bank",
    value: "B0446-Eastern Bank"
  }, {
    label: "K0786-Eastshore Consulting LLC",
    value: "K0786-Eastshore Consulting LLC"
  }, {
    label: "K0224-Echo Financial Products LLC",
    value: "K0224-Echo Financial Products LLC"
  }, {
    label: "K1117-Economic Development Group, Ltd.",
    value: "K1117-Economic Development Group, Ltd."
  }, {
    label: "K0146-Efficient Capital Corp.",
    value: "K0146-Efficient Capital Corp."
  }, {
    label: "K1206-EFG Consulting LLC",
    value: "K1206-EFG Consulting LLC"
  }, {
    label: "K0165-Ehlers & Associates, Inc",
    value: "K0165-Ehlers & Associates, Inc"
  }, {
    label: "K1204-Elzey Consulting Group, LLC",
    value: "K1204-Elzey Consulting Group, LLC"
  }, {
    label: "K0978-Environmental Attribute Advisors LLC",
    value: "K0978-Environmental Attribute Advisors LLC"
  }, {
    label: "K0387-Environmental Capital LLC",
    value: "K0387-Environmental Capital LLC"
  }, {
    label: "K0308-Eppinger & Associates, LLC",
    value: "K0308-Eppinger & Associates, LLC"
  }, {
    label: "K0175-Ernst & Young Infrastructure Advisors, LLC",
    value: "K0175-Ernst & Young Infrastructure Advisors, LLC"
  }, {
    label: "A6423-Essex Financial Services, Inc.",
    value: "A6423-Essex Financial Services, Inc."
  }, {
    label: "A3631-Estrada Hinojosa & Co., Inc.",
    value: "A3631-Estrada Hinojosa & Co., Inc."
  }, {
    label: "K0952-Excelsior Capital Advisory Services LLC",
    value: "K0952-Excelsior Capital Advisory Services LLC"
  }, {
    label: "K1031-F&J Housing Group, L.L.C.",
    value: "K1031-F&J Housing Group, L.L.C."
  }, {
    label: "K0354-Fairmount Capital Advisors, Inc.",
    value: "K0354-Fairmount Capital Advisors, Inc."
  }, {
    label: "K0859-Farr Miller & Washington, LLCDC",
    value: "K0859-Farr Miller & Washington, LLCDC"
  }, {
    label: "K1126-FBT Project Finance Advisors LLC",
    value: "K1126-FBT Project Finance Advisors LLC"
  }, {
    label: "K0276-Fieldman, Rolapp & Associates, Inc.",
    value: "K0276-Fieldman, Rolapp & Associates, Inc."
  }, {
    label: "A1070-Fifth Third Securities, Inc.",
    value: "A1070-Fifth Third Securities, Inc."
  }, {
    label: "K1167-Financial Advisory Investment Management Group, LLC",
    value: "K1167-Financial Advisory Investment Management Group, LLC"
  }, {
    label: "K0501-Financial S&Lutions LLC",
    value: "K0501-Financial S&Lutions LLC"
  }, {
    label: "K0365-Financial Solutions Group, Inc.",
    value: "K0365-Financial Solutions Group, Inc."
  }, {
    label: "K1021-Financing Ideas Inc.",
    value: "K1021-Financing Ideas Inc."
  }, {
    label: "A4599-First Allied Securities, Inc.",
    value: "A4599-First Allied Securities, Inc."
  }, {
    label: "K0906-First American Financial Advisors, Inc.",
    value: "K0906-First American Financial Advisors, Inc."
  }, {
    label: "A2469-First Empire Securities, Inc.",
    value: "A2469-First Empire Securities, Inc."
  }, {
    label: "A5890-First Financial Equity Corporation",
    value: "A5890-First Financial Equity Corporation"
  }, {
    label: "K0937-First Hawaiian Bank",
    value: "K0937-First Hawaiian Bank"
  }, {
    label: "K0129-First Infrastructure LLC",
    value: "K0129-First Infrastructure LLC"
  }, {
    label: "A0487-First Kentucky Securities Corporation.",
    value: "A0487-First Kentucky Securities Corporation."
  }, {
    label: "A0495-First Midstate Inc.",
    value: "A0495-First Midstate Inc."
  }, {
    label: "A5914-First National Capital Markets, Inc.",
    value: "A5914-First National Capital Markets, Inc."
  }, {
    label: "K0346-First River Advisory L.L.C.",
    value: "K0346-First River Advisory L.L.C."
  }, {
    label: "A7093-First Southern Securities, LLC",
    value: "A7093-First Southern Securities, LLC"
  }, {
    label: "A4993-First Tryon Securities, LLC",
    value: "A4993-First Tryon Securities, LLC"
  }, {
    label: "K0191-Fiscal Advisors & Marketing, Inc.",
    value: "K0191-Fiscal Advisors & Marketing, Inc."
  }, {
    label: "K0710-Fiscal Strategies Group, Inc.",
    value: "K0710-Fiscal Strategies Group, Inc."
  }, {
    label: "K1055-Fishkind & Associates, Inc.",
    value: "K1055-Fishkind & Associates, Inc."
  }, {
    label: "K0525-Ford & Associates, Inc.",
    value: "K0525-Ford & Associates, Inc."
  }, {
    label: "K0160-Forsyth Street Advisors LLC",
    value: "K0160-Forsyth Street Advisors LLC"
  }, {
    label: "A7170-Fortress Group, Inc.",
    value: "A7170-Fortress Group, Inc."
  }, {
    label: "K0942-Forum Capital Securities LLC",
    value: "K0942-Forum Capital Securities LLC"
  }, {
    label: "K0874-FRANCIS FINANCIAL GROUP, LLC",
    value: "K0874-FRANCIS FINANCIAL GROUP, LLC"
  }, {
    label: "K0164-Frasca & Associates, LLC",
    value: "K0164-Frasca & Associates, LLC"
  }, {
    label: "A2720-Friedman Luzzatto & Co.",
    value: "A2720-Friedman Luzzatto & Co."
  }, {
    label: "K1134-Froggatte & Co.",
    value: "K1134-Froggatte & Co."
  }, {
    label: "K0373-Frontier Partners, Inc.",
    value: "K0373-Frontier Partners, Inc."
  }, {
    label: "B0358-Frost Bank Capital Markets",
    value: "B0358-Frost Bank Capital Markets"
  }, {
    label: "K0582-FTI Capital Advisors, LLC",
    value: "K0582-FTI Capital Advisors, LLC"
  }, {
    label: "K1189-FTN Financial Municipal Advisors",
    value: "K1189-FTN Financial Municipal Advisors"
  }, {
    label: "K0743-G-Entry Principle, P.C.",
    value: "K0743-G-Entry Principle, P.C."
  }, {
    label: "K0275-G.L. Hicks Financial, LLC",
    value: "K0275-G.L. Hicks Financial, LLC"
  }, {
    label: "K1125-Gannett Fleming Valuation and Rate Consultants, LLC",
    value: "K1125-Gannett Fleming Valuation and Rate Consultants, LLC"
  }, {
    label: "A4020-Gates Capital Corporation",
    value: "A4020-Gates Capital Corporation"
  }, {
    label: "K0922-GB ASSOCIATES LLC",
    value: "K0922-GB ASSOCIATES LLC"
  }, {
    label: "K0328-Genesis Marketing Group, Inc.",
    value: "K0328-Genesis Marketing Group, Inc."
  }, {
    label: "A0104-George K Baum & Company",
    value: "A0104-George K Baum & Company"
  }, {
    label: "A0576-Goldman Sachs & Co. LLC ",
    value: "A0576-Goldman Sachs & Co. LLC "
  }, {
    label: "K0237-Gollahon Financial Services, Inc.",
    value: "K0237-Gollahon Financial Services, Inc."
  }, {
    label: "K0495-Government Capital Management, LLC",
    value: "K0495-Government Capital Management, LLC"
  }, {
    label: "A5412-Government Capital Securities Corporation",
    value: "A5412-Government Capital Securities Corporation"
  }, {
    label: "K0339-Government Consultants, Inc.",
    value: "K0339-Government Consultants, Inc."
  }, {
    label: "K0127-Government Financial Strategies inc.",
    value: "K0127-Government Financial Strategies inc."
  }, {
    label: "K1193-GovRates, Inc.",
    value: "K1193-GovRates, Inc."
  }, {
    label: "K0980-GPM Municipal Advisors, LLC",
    value: "K0980-GPM Municipal Advisors, LLC"
  }, {
    label: "K0442-Grant & Associates LLC.",
    value: "K0442-Grant & Associates LLC."
  }, {
    label: "K1073-Great Disclosure LLC",
    value: "K1073-Great Disclosure LLC"
  }, {
    label: "A1706-Grigsby & Associates, Inc.",
    value: "A1706-Grigsby & Associates, Inc."
  }, {
    label: "K0645-Growth Capital Associates, Inc.",
    value: "K0645-Growth Capital Associates, Inc."
  }, {
    label: "K0109-Guardian Advisors, LLC",
    value: "K0109-Guardian Advisors, LLC"
  }, {
    label: "K0171-H.J. Umbaugh & Associates, Certified Public Accountants, LLP",
    value: "K0171-H.J. Umbaugh & Associates, Certified Public Accountants, LLP"
  }, {
    label: "A7304-H2C Securities Inc.",
    value: "A7304-H2C Securities Inc."
  }, {
    label: "K0771-Hamlin Capital Advisors LLC",
    value: "K0771-Hamlin Capital Advisors LLC"
  }, {
    label: "A3627-Harbor Financial Services, L.L.C.",
    value: "A3627-Harbor Financial Services, L.L.C."
  }, {
    label: "K0336-Harrell & Company Advisors, LLC",
    value: "K0336-Harrell & Company Advisors, LLC"
  }, {
    label: "K0557-Harris Housing Advisors, LLC",
    value: "K0557-Harris Housing Advisors, LLC"
  }, {
    label: "K0176-Health Care Network Associates, LLC",
    value: "K0176-Health Care Network Associates, LLC"
  }, {
    label: "K1114-Hedge Point Financial, LLC",
    value: "K1114-Hedge Point Financial, LLC"
  }, {
    label: "K0638-Hendrickson Mark Allan",
    value: "K0638-Hendrickson Mark Allan"
  }, {
    label: "A1481-Herbert J Sims & Co., Inc.",
    value: "A1481-Herbert J Sims & Co., Inc."
  }, {
    label: "K0239-HFA Partners, LLC",
    value: "K0239-HFA Partners, LLC"
  }, {
    label: "K0360-HG Wilson Municipal Finance Inc.",
    value: "K0360-HG Wilson Municipal Finance Inc."
  }, {
    label: "A1290-Hilltop Securities Inc.",
    value: "A1290-Hilltop Securities Inc."
  }, {
    label: "K0286-Hobbs, Ong & Associates, Inc.",
    value: "K0286-Hobbs, Ong & Associates, Inc."
  }, {
    label: "K0320-Hooks, LLC",
    value: "K0320-Hooks, LLC"
  }, {
    label: "A0693-Hutchinson,Shockey,Erley & Co.",
    value: "A0693-Hutchinson,Shockey,Erley & Co."
  }, {
    label: "K0991-IBERIA Wealth Advisors, a division of IBERIABANK",
    value: "K0991-IBERIA Wealth Advisors, a division of IBERIABANK"
  }, {
    label: "K0599-IMG Rebel Advisory, Inc.",
    value: "K0599-IMG Rebel Advisory, Inc."
  }, {
    label: "K0332-Independent Bond & Investment Consultants LLC",
    value: "K0332-Independent Bond & Investment Consultants LLC"
  }, {
    label: "K0810-Independent Public Advisors, LLC",
    value: "K0810-Independent Public Advisors, LLC"
  }, {
    label: "K0369-Ineo Capital LLC",
    value: "K0369-Ineo Capital LLC"
  }, {
    label: "K0426-InnoVative Capital LLc",
    value: "K0426-InnoVative Capital LLc"
  }, {
    label: "A7419-Institutional Bond Network LLC.",
    value: "A7419-Institutional Bond Network LLC."
  }, {
    label: "K1135-Institutional Capital Management Inc.",
    value: "K1135-Institutional Capital Management Inc."
  }, {
    label: "K0923-Ironwood Advisors LLC",
    value: "K0923-Ironwood Advisors LLC"
  }, {
    label: "K1047-IRR Corporate & Public Finance, LLC",
    value: "K1047-IRR Corporate & Public Finance, LLC"
  }, {
    label: "K1200-Isosceles & Company",
    value: "K1200-Isosceles & Company"
  }, {
    label: "K0891-J. Donald Porter & Co., Inc.",
    value: "K0891-J. Donald Porter & Co., Inc."
  }, {
    label: "K1146-JAMES JOSEPH DARBY",
    value: "K1146-JAMES JOSEPH DARBY"
  }, {
    label: "A0785-Janney Montgomery Scott LLC",
    value: "A0785-Janney Montgomery Scott LLC"
  }, {
    label: "K0388-JCRA Financial LLC",
    value: "K0388-JCRA Financial LLC"
  }, {
    label: "A5232-Jefferies LLC",
    value: "A5232-Jefferies LLC"
  }, {
    label: "K0363-JFBP, LLC",
    value: "K0363-JFBP, LLC"
  }, {
    label: "A0668-JJB Hilliard, WL Lyons, LLC",
    value: "A0668-JJB Hilliard, WL Lyons, LLC"
  }, {
    label: "K0761-JLT Capital Partners LLC",
    value: "K0761-JLT Capital Partners LLC"
  }, {
    label: "K0273-JNA Consulting Group, LLC",
    value: "K0273-JNA Consulting Group, LLC"
  }, {
    label: "A0797-Joe Jolly & Co., Inc.",
    value: "A0797-Joe Jolly & Co., Inc."
  }, {
    label: "K1046-Johnson Research Group, Inc.",
    value: "K1046-Johnson Research Group, Inc."
  }, {
    label: "A7377-Jones Lang LaSalle Securities, LLC",
    value: "A7377-Jones Lang LaSalle Securities, LLC"
  }, {
    label: "K1210-K-12 Capital Advisors, LLC",
    value: "K1210-K-12 Capital Advisors, LLC"
  }, {
    label: "K0120-Kaiser Wealth Management",
    value: "K0120-Kaiser Wealth Management"
  }, {
    label: "A3525-Kane, McKenna Capital, Inc",
    value: "A3525-Kane, McKenna Capital, Inc"
  }, {
    label: "K0216-Kaufman, Hall & Associates, LLC",
    value: "K0216-Kaufman, Hall & Associates, LLC"
  }, {
    label: "K0153-Kensington Capital Advisors, LLC",
    value: "K0153-Kensington Capital Advisors, LLC"
  }, {
    label: "K0644-Kentucky Association of Counties",
    value: "K0644-Kentucky Association of Counties"
  }, {
    label: "K0988-KeyBank Municipal Advisor Department",
    value: "K0988-KeyBank Municipal Advisor Department"
  }, {
    label: "K0254-KEYGENT LLC",
    value: "K0254-KEYGENT LLC"
  }, {
    label: "K0169-Kidwell & Company Inc.",
    value: "K0169-Kidwell & Company Inc."
  }, {
    label: "K0251-Kings Financial Consulting Incorporated",
    value: "K0251-Kings Financial Consulting Incorporated"
  }, {
    label: "A6881-Kipling Jones & Co., Ltd.",
    value: "A6881-Kipling Jones & Co., Ltd."
  }, {
    label: "K0121-Kitahata & Company",
    value: "K0121-Kitahata & Company"
  }, {
    label: "K1151-KNN Public Finance, LLC",
    value: "K1151-KNN Public Finance, LLC"
  }, {
    label: "K0269-Kosan Associates",
    value: "K0269-Kosan Associates"
  }, {
    label: "K0505-Kosmont Realty Corp",
    value: "K0505-Kosmont Realty Corp"
  }, {
    label: "K0836-KPM Financial, LLC",
    value: "K0836-KPM Financial, LLC"
  }, {
    label: "K0295-Krause John Lawrence Jr ",
    value: "K0295-Krause John Lawrence Jr "
  }, {
    label: "A4635-Kuehl Capital Corporation",
    value: "A4635-Kuehl Capital Corporation"
  }, {
    label: "A4071-L.J. Hart & Company",
    value: "A4071-L.J. Hart & Company"
  }, {
    label: "K0110-Lamont Financial Services Corporation",
    value: "K0110-Lamont Financial Services Corporation"
  }, {
    label: "A3381-Lancaster Pollard & Co., LLC",
    value: "A3381-Lancaster Pollard & Co., LLC"
  }, {
    label: "K0689-Larsen Wurzel & Associates, Inc.",
    value: "K0689-Larsen Wurzel & Associates, Inc."
  }, {
    label: "K0712-Larson Consulting Services, LLC",
    value: "K0712-Larson Consulting Services, LLC"
  }, {
    label: "K0122-Lawrence Financial Consulting LLC",
    value: "K0122-Lawrence Financial Consulting LLC"
  }, {
    label: "K0707-Lawrenson Services Inc.",
    value: "K0707-Lawrenson Services Inc."
  }, {
    label: "A1967-Lawson Financial Corporation",
    value: "A1967-Lawson Financial Corporation"
  }, {
    label: "K0411-Leora Consulting LLC",
    value: "K0411-Leora Consulting LLC"
  }, {
    label: "A4774-Lewis Young Robertson & Burningham, Inc.",
    value: "A4774-Lewis Young Robertson & Burningham, Inc."
  }, {
    label: "K0155-Liberty Capital Services, LLC",
    value: "K0155-Liberty Capital Services, LLC"
  }, {
    label: "K0757-Liscarnan Solutions, LLC",
    value: "K0757-Liscarnan Solutions, LLC"
  }, {
    label: "K1130-Live Oak Public Finance, LLC",
    value: "K1130-Live Oak Public Finance, LLC"
  }, {
    label: "K0379-London Witte Group LLC",
    value: "K0379-London Witte Group LLC"
  }, {
    label: "K0616-Long-Economic Development Advisors, LLC",
    value: "K0616-Long-Economic Development Advisors, LLC"
  }, {
    label: "K0546-Longhouse Capital Advisors, LLC",
    value: "K0546-Longhouse Capital Advisors, LLC"
  }, {
    label: "K1042-Los Alamos National Bank",
    value: "K1042-Los Alamos National Bank"
  }, {
    label: "K0981-Macquarie Capital (USA) Inc.",
    value: "K0981-Macquarie Capital (USA) Inc."
  }, {
    label: "K0902-Majors Group",
    value: "K0902-Majors Group"
  }, {
    label: "K0454-Malachi Financial Products Inc.",
    value: "K0454-Malachi Financial Products Inc."
  }, {
    label: "K0790-MANN MANN JENSEN PARTNERS LP",
    value: "K0790-MANN MANN JENSEN PARTNERS LP"
  }, {
    label: "K0948-Martin J. Luby",
    value: "K0948-Martin J. Luby"
  }, {
    label: "A1037-Martin Nelson & Co., Inc.",
    value: "A1037-Martin Nelson & Co., Inc."
  }, {
    label: "K1177-MAS Financial Advisory Services LLC",
    value: "K1177-MAS Financial Advisory Services LLC"
  }, {
    label: "K0215-McGuire Sponsel",
    value: "K0215-McGuire Sponsel"
  }, {
    label: "A0962-McLiney and Company",
    value: "A0962-McLiney and Company"
  }, {
    label: "A0028-ME Allison & Co., Inc.",
    value: "A0028-ME Allison & Co., Inc."
  }, {
    label: "K0915-Meierhenry Sargent LLP",
    value: "K0915-Meierhenry Sargent LLP"
  }, {
    label: "K0416-Melio & Company, LLC",
    value: "K0416-Melio & Company, LLC"
  }, {
    label: "K0870-Meno Accounting and Financial Services",
    value: "K0870-Meno Accounting and Financial Services"
  }, {
    label: "K0147-Mercator Advisors LLC",
    value: "K0147-Mercator Advisors LLC"
  }, {
    label: "A0980-Mesirow Financial Inc",
    value: "A0980-Mesirow Financial Inc"
  }, {
    label: "K0520-MGIC Corp.",
    value: "K0520-MGIC Corp."
  }, {
    label: "K0946-Millco Advisors, LP",
    value: "K0946-Millco Advisors, LP"
  }, {
    label: "K0678-Miller Buckfire&Co., LLC",
    value: "K0678-Miller Buckfire&Co., LLC"
  }, {
    label: "K0889-Mission Trail Advisors, LLC",
    value: "K0889-Mission Trail Advisors, LLC"
  }, {
    label: "K0119-Mohanty Gargiulo LLC",
    value: "K0119-Mohanty Gargiulo LLC"
  }, {
    label: "K0361-Montague DeRose and Associates, LLC",
    value: "K0361-Montague DeRose and Associates, LLC"
  }, {
    label: "K0446-Moody Reid Financial Advisors",
    value: "K0446-Moody Reid Financial Advisors"
  }, {
    label: "A1007-Moors & Cabot, Inc. ",
    value: "A1007-Moors & Cabot, Inc. "
  }, {
    label: "K0977-Morse Associates Consulting, LLC",
    value: "K0977-Morse Associates Consulting, LLC"
  }, {
    label: "K0846-MSA Professional Services, Inc.",
    value: "K0846-MSA Professional Services, Inc."
  }, {
    label: "A3678-Multi-Bank Securities, Inc.",
    value: "A3678-Multi-Bank Securities, Inc."
  }, {
    label: "K0641-MuniCap, Inc.",
    value: "K0641-MuniCap, Inc."
  }, {
    label: "K0681-Municipal Advisor Solutions, LLC",
    value: "K0681-Municipal Advisor Solutions, LLC"
  }, {
    label: "K0423-Municipal Advisors Group of Boston, Inc.",
    value: "K0423-Municipal Advisors Group of Boston, Inc."
  }, {
    label: "K1205-Municipal Advisors of Mississippi Inc.",
    value: "K1205-Municipal Advisors of Mississippi Inc."
  }, {
    label: "K1147-Municipal Capital Advisors LLC",
    value: "K1147-Municipal Capital Advisors LLC"
  }, {
    label: "A5738-Municipal Capital Markets Grp., Inc.",
    value: "A5738-Municipal Capital Markets Grp., Inc."
  }, {
    label: "K0236-Municipal Finance Services, Inc.",
    value: "K0236-Municipal Finance Services, Inc."
  }, {
    label: "K0393-Municipal Financial Consultants Incorporated",
    value: "K0393-Municipal Financial Consultants Incorporated"
  }, {
    label: "K0173-Municipal Solutions, Inc.",
    value: "K0173-Municipal Solutions, Inc."
  }, {
    label: "K0911-MuniGroup, LLC",
    value: "K0911-MuniGroup, LLC"
  }, {
    label: "K0114-Munistat Services, Inc.",
    value: "K0114-Munistat Services, Inc."
  }, {
    label: "K1195-MW Financial Advisory Services LLC",
    value: "K1195-MW Financial Advisory Services LLC"
  }, {
    label: "K1068-National Capital Resources, LLC",
    value: "K1068-National Capital Resources, LLC"
  }, {
    label: "K0737-National Healthcare Capital LLC",
    value: "K0737-National Healthcare Capital LLC"
  }, {
    label: "K0228-National Minority Consultants, Inc.",
    value: "K0228-National Minority Consultants, Inc."
  }, {
    label: "K0639-NBS Government Finance Group",
    value: "K0639-NBS Government Finance Group"
  }, {
    label: "K1048-Neuberger Berman Trust Co National Association",
    value: "K1048-Neuberger Berman Trust Co National Association"
  }, {
    label: "K0753-New Albany Capital Partners, LLC",
    value: "K0753-New Albany Capital Partners, LLC"
  }, {
    label: "K0772-NHA Advisors, LLC",
    value: "K0772-NHA Advisors, LLC"
  }, {
    label: "K0201-North Slope Capital Advisors",
    value: "K0201-North Slope Capital Advisors"
  }, {
    label: "K1140-Northeast Municipal Advisors LLC",
    value: "K1140-Northeast Municipal Advisors LLC"
  }, {
    label: "A4861-Northland Securities, Inc.",
    value: "A4861-Northland Securities, Inc."
  }, {
    label: "K0243-Northwest Municipal Advisors, Inc.",
    value: "K0243-Northwest Municipal Advisors, Inc."
  }, {
    label: "K0511-Not For Profit Capital Strategies LLC",
    value: "K0511-Not For Profit Capital Strategies LLC"
  }, {
    label: "K0438-NW Financial Group, LLC",
    value: "K0438-NW Financial Group, LLC"
  }, {
    label: "K0378-O W Krohn & Associates, LLP",
    value: "K0378-O W Krohn & Associates, LLP"
  }, {
    label: "A6930-OConnor & Company Securities, Inc.",
    value: "A6930-OConnor & Company Securities, Inc."
  }, {
    label: "K1144-Olsen Fredrick Harley",
    value: "K1144-Olsen Fredrick Harley"
  }, {
    label: "K0142-Omnicap Group LLC",
    value: "K0142-Omnicap Group LLC"
  }, {
    label: "A0455-Oppenheimer & Co. Inc.",
    value: "A0455-Oppenheimer & Co. Inc."
  }, {
    label: "K0714-Optimal Capital Group, llc",
    value: "K0714-Optimal Capital Group, llc"
  }, {
    label: "K0775-Oxford Advisors LLC",
    value: "K0775-Oxford Advisors LLC"
  }, {
    label: "K0364-Oyster River Capital LP",
    value: "K0364-Oyster River Capital LP"
  }, {
    label: "K1057-p3point",
    value: "K1057-p3point"
  }, {
    label: "K1091-Parenteau & Associates, Inc.",
    value: "K1091-Parenteau & Associates, Inc."
  }, {
    label: "K1003-Pension Consulting Alliance, LLC",
    value: "K1003-Pension Consulting Alliance, LLC"
  }, {
    label: "K0894-Percy Consulting LLC",
    value: "K0894-Percy Consulting LLC"
  }, {
    label: "K0849-Perkins Fund Marketing, LLC",
    value: "K0849-Perkins Fund Marketing, LLC"
  }, {
    label: "K1187-Perseverance Capital Advisors LLC",
    value: "K1187-Perseverance Capital Advisors LLC"
  }, {
    label: "K0303-Peters Municipal Consultants, LTD",
    value: "K0303-Peters Municipal Consultants, LTD"
  }, {
    label: "K1162-PFM Financial Advisors LLC",
    value: "K1162-PFM Financial Advisors LLC"
  }, {
    label: "K0949-PFM Swap Advisors LLC",
    value: "K0949-PFM Swap Advisors LLC"
  }, {
    label: "K0117-Phoenix Advisors, LLC",
    value: "K0117-Phoenix Advisors, LLC"
  }, {
    label: "K0491-Phoenix Capital Partners, LLP",
    value: "K0491-Phoenix Capital Partners, LLP"
  }, {
    label: "K1137-Pickwick Capital Partners, LLC",
    value: "K1137-Pickwick Capital Partners, LLC"
  }, {
    label: "A7265-Piedmont Securities LLC",
    value: "A7265-Piedmont Securities LLC"
  }, {
    label: "K0283-Pinnacle Financial Group, LLC",
    value: "K0283-Pinnacle Financial Group, LLC"
  }, {
    label: "A1126-Piper Jaffray & Co.",
    value: "A1126-Piper Jaffray & Co."
  }, {
    label: "A4912-PMA Securities, Inc.",
    value: "A4912-PMA Securities, Inc."
  }, {
    label: "K1038-PolyChronic PMC, LLC",
    value: "K1038-PolyChronic PMC, LLC"
  }, {
    label: "K0309-Ponder & Co.",
    value: "K0309-Ponder & Co."
  }, {
    label: "K0124-Pop-Lazic & Co.",
    value: "K0124-Pop-Lazic & Co."
  }, {
    label: "A0742-Porter, White & Company, Inc.",
    value: "A0742-Porter, White & Company, Inc."
  }, {
    label: "A3877-Powell Capital Markets, Inc.",
    value: "A3877-Powell Capital Markets, Inc."
  }, {
    label: "A2940-Prager & Co., LLC",
    value: "A2940-Prager & Co., LLC"
  }, {
    label: "A2137-ProEquities,Inc",
    value: "A2137-ProEquities,Inc"
  }, {
    label: "K0834-Project Finance Advisory Limited",
    value: "K0834-Project Finance Advisory Limited"
  }, {
    label: "A0137-Prospera Financial Services, Inc.",
    value: "A0137-Prospera Financial Services, Inc."
  }, {
    label: "K0539-prospero capital llc",
    value: "K0539-prospero capital llc"
  }, {
    label: "K0995-Provident Municipal Advisor, LLC",
    value: "K0995-Provident Municipal Advisor, LLC"
  }, {
    label: "K0264-Public Advisory Consultants, Inc.",
    value: "K0264-Public Advisory Consultants, Inc."
  }, {
    label: "K1054-Public Alternative Advisors, LLC",
    value: "K1054-Public Alternative Advisors, LLC"
  }, {
    label: "K1005-Public Economics, Inc.",
    value: "K1005-Public Economics, Inc."
  }, {
    label: "K0702-Public Finance & Energy Advisors, LLC",
    value: "K0702-Public Finance & Energy Advisors, LLC"
  }, {
    label: "K0959-Public Finance Group LLC",
    value: "K0959-Public Finance Group LLC"
  }, {
    label: "K0204-Public Financial Management, Inc.",
    value: "K0204-Public Financial Management, Inc."
  }, {
    label: "K1166-Public Investment Advisors, Inc.",
    value: "K1166-Public Investment Advisors, Inc."
  }, {
    label: "K0133-Public Resources Advisory Group, Inc.",
    value: "K0133-Public Resources Advisory Group, Inc."
  }, {
    label: "K1149-Purvis & Jeffcoat Ltd Co",
    value: "K1149-Purvis & Jeffcoat Ltd Co"
  }, {
    label: "K1183-R. G. Timbs, Inc",
    value: "K1183-R. G. Timbs, Inc"
  }, {
    label: "K0666-Raftelis Financial Consultants, Inc.",
    value: "K0666-Raftelis Financial Consultants, Inc."
  }, {
    label: "K0327-Ranson Financial Corp",
    value: "K0327-Ranson Financial Corp"
  }, {
    label: "K1196-Ranson Financial Group, LLC",
    value: "K1196-Ranson Financial Group, LLC"
  }, {
    label: "K0161-Rathmann & Associates, L.P.",
    value: "K0161-Rathmann & Associates, L.P."
  }, {
    label: "K0619-Ravi Chitkara",
    value: "K0619-Ravi Chitkara"
  }, {
    label: "A1172-Raymond James & Assoc., Inc.",
    value: "A1172-Raymond James & Assoc., Inc."
  }, {
    label: "A4258-RBC Capital Markets LLC",
    value: "A4258-RBC Capital Markets LLC"
  }, {
    label: "K0499-Reedy Financial Group P.C.",
    value: "K0499-Reedy Financial Group P.C."
  }, {
    label: "K1136-Renasant Bank",
    value: "K1136-Renasant Bank"
  }, {
    label: "K0826-Rice Advisory LLC",
    value: "K0826-Rice Advisory LLC"
  }, {
    label: "A2966-Rice Securities, LLC ",
    value: "A2966-Rice Securities, LLC "
  }, {
    label: "K0997-Ring McAfee & Co. Inc.",
    value: "K0997-Ring McAfee & Co. Inc."
  }, {
    label: "K0983-RK Holman & Company, LLC",
    value: "K0983-RK Holman & Company, LLC"
  }, {
    label: "K1097-Robert Kuo Consulting, LLC",
    value: "K1097-Robert Kuo Consulting, LLC"
  }, {
    label: "A0079-Robert W Baird & Co., Inc.",
    value: "A0079-Robert W Baird & Co., Inc."
  }, {
    label: "K0450-Robert W.E. Fisher",
    value: "K0450-Robert W.E. Fisher"
  }, {
    label: "K0280-Roberts Consulting, LLC",
    value: "K0280-Roberts Consulting, LLC"
  }, {
    label: "A6991-Rockfleet Financial Services, Inc.",
    value: "A6991-Rockfleet Financial Services, Inc."
  }, {
    label: "K0863-Rockmill Financial Consulting, LLC",
    value: "K0863-Rockmill Financial Consulting, LLC"
  }, {
    label: "A1218-Roosevelt & Cross Incorporated",
    value: "A1218-Roosevelt & Cross Incorporated"
  }, {
    label: "K0253-Ross Financial",
    value: "K0253-Ross Financial"
  }, {
    label: "A3543-Ross, Sinclaire & Associates, LLC",
    value: "A3543-Ross, Sinclaire & Associates, LLC"
  }, {
    label: "K1188-ROTHSCHILD INC.",
    value: "K1188-ROTHSCHILD INC."
  }, {
    label: "K0764-Rothstein Group, LLC.",
    value: "K0764-Rothstein Group, LLC."
  }, {
    label: "K1034-RSI Group LLC",
    value: "K1034-RSI Group LLC"
  }, {
    label: "K0808-RTM Asset Management LLC",
    value: "K0808-RTM Asset Management LLC"
  }, {
    label: "K0951-Rubicon Infrastructure Advisors",
    value: "K0951-Rubicon Infrastructure Advisors"
  }, {
    label: "K1123-Rydle Project Funding",
    value: "K1123-Rydle Project Funding"
  }, {
    label: "K0290-S L Capital Strategies LLC",
    value: "K0290-S L Capital Strategies LLC"
  }, {
    label: "K0625-S. B. Friedman & Company",
    value: "K0625-S. B. Friedman & Company"
  }, {
    label: "K0194-S.B. Clark, Inc.",
    value: "K0194-S.B. Clark, Inc."
  }, {
    label: "K0422-S.P. Yount Financial, LLC",
    value: "K0422-S.P. Yount Financial, LLC"
  }, {
    label: "A2689-SA Stone Wealth Management, Inc",
    value: "A2689-SA Stone Wealth Management, Inc"
  }, {
    label: "A6695-SAMCO CAPITAL MARKETS, INC.",
    value: "A6695-SAMCO CAPITAL MARKETS, INC."
  }, {
    label: "K1115-SAMUEL A MACRINA",
    value: "K1115-SAMUEL A MACRINA"
  }, {
    label: "A1165-SAMUEL A. RAMIREZ & COMPANY INC.",
    value: "A1165-SAMUEL A. RAMIREZ & COMPANY INC."
  }, {
    label: "A3533-Scully Capital Securities Corp.",
    value: "A3533-Scully Capital Securities Corp."
  }, {
    label: "K0837-Sentry Financial Services ",
    value: "K0837-Sentry Financial Services "
  }, {
    label: "K0716-Sentry Management, Inc.",
    value: "K0716-Sentry Management, Inc."
  }, {
    label: "K0674-SG Americas Securities, LLC",
    value: "K0674-SG Americas Securities, LLC"
  }, {
    label: "K1148-Shea Advisory Services, LLC",
    value: "K1148-Shea Advisory Services, LLC"
  }, {
    label: "A4749-Shearson Financial Services LLC",
    value: "A4749-Shearson Financial Services LLC"
  }, {
    label: "A5074-Siebert Cisneros Shank & Co., L.L.C.",
    value: "A5074-Siebert Cisneros Shank & Co., L.L.C."
  }, {
    label: "K0272-Sierra Management Group",
    value: "K0272-Sierra Management Group"
  }, {
    label: "A3587-Sisung Securities Corporation",
    value: "A3587-Sisung Securities Corporation"
  }, {
    label: "K0259-SJ Advisors, LLC",
    value: "K0259-SJ Advisors, LLC"
  }, {
    label: "K0841-SOA Financial",
    value: "K0841-SOA Financial"
  }, {
    label: "K0190-South Avenue Investment Partners LLC",
    value: "K0190-South Avenue Investment Partners LLC"
  }, {
    label: "K0436-Southeastern Investment Securities, LLC",
    value: "K0436-Southeastern Investment Securities, LLC"
  }, {
    label: "K0207-Southern Municipal Advisors, Inc.",
    value: "K0207-Southern Municipal Advisors, Inc."
  }, {
    label: "K1182-Southwest Stanford Group LLC",
    value: "K1182-Southwest Stanford Group LLC"
  }, {
    label: "K1113-Special Districts Association of Oregon Advisory Services LLC",
    value: "K1113-Special Districts Association of Oregon Advisory Services LLC"
  }, {
    label: "K0134-Specialized Public Finance Inc.",
    value: "K0134-Specialized Public Finance Inc."
  }, {
    label: "K0107-Spectrum Municipal Services, Inc.",
    value: "K0107-Spectrum Municipal Services, Inc."
  }, {
    label: "K0162-Speer Financial, Inc.",
    value: "K0162-Speer Financial, Inc."
  }, {
    label: "K0514-Sperry Capital Inc.",
    value: "K0514-Sperry Capital Inc."
  }, {
    label: "K0457-Springsted Incorporated",
    value: "K0457-Springsted Incorporated"
  }, {
    label: "K0459-Springsted Investment Advisors Incorporated",
    value: "K0459-Springsted Investment Advisors Incorporated"
  }, {
    label: "K0330-SRJ Government Consultants, LLC",
    value: "K0330-SRJ Government Consultants, LLC"
  }, {
    label: "K0293-Stanley P. Stone & Associates, Inc.",
    value: "K0293-Stanley P. Stone & Associates, Inc."
  }, {
    label: "A3857-Starshak Winzenburg & Co",
    value: "A3857-Starshak Winzenburg & Co"
  }, {
    label: "K0103-Stephen H. McDonald & Associates, Inc.",
    value: "K0103-Stephen H. McDonald & Associates, Inc."
  }, {
    label: "K0371-Stephen L. Smith Corp.",
    value: "K0371-Stephen L. Smith Corp."
  }, {
    label: "A1303-Stephens Inc.",
    value: "A1303-Stephens Inc."
  }, {
    label: "A2178-Stern Brothers & Co.",
    value: "A2178-Stern Brothers & Co."
  }, {
    label: "K0261-Steven Brock",
    value: "K0261-Steven Brock"
  }, {
    label: "K0871-Steven Gortler",
    value: "K0871-Steven Gortler"
  }, {
    label: "K1186-Stewart Carr LLC",
    value: "K1186-Stewart Carr LLC"
  }, {
    label: "A1313-Stifel, Nicolaus & Company,Inc",
    value: "A1313-Stifel, Nicolaus & Company,Inc"
  }, {
    label: "A2359-Sturges Company (The)",
    value: "A2359-Sturges Company (The)"
  }, {
    label: "K0435-Sudsina & Associates, LLC",
    value: "K0435-Sudsina & Associates, LLC"
  }, {
    label: "K0271-Susquehanna Group Advisors, Inc.",
    value: "K0271-Susquehanna Group Advisors, Inc."
  }, {
    label: "K1103-Sustainable Capital Advisors, LLC",
    value: "K1103-Sustainable Capital Advisors, LLC"
  }, {
    label: "A4192-Sutter Securities, Inc.",
    value: "A4192-Sutter Securities, Inc."
  }, {
    label: "K0219-Swap Financial Group, LLC",
    value: "K0219-Swap Financial Group, LLC"
  }, {
    label: "K0289-Sycamore Advisors, LLC",
    value: "K0289-Sycamore Advisors, LLC"
  }, {
    label: "A1715-Synovus Securities Inc.",
    value: "A1715-Synovus Securities Inc."
  }, {
    label: "K1026-Systima Capital Management LLC",
    value: "K1026-Systima Capital Management LLC"
  }, {
    label: "K1120-T. Kiser & Associates, Inc.",
    value: "K1120-T. Kiser & Associates, Inc."
  }, {
    label: "K0571-Tennessee Utility Assistance, LLC",
    value: "K0571-Tennessee Utility Assistance, LLC"
  }, {
    label: "K1024-Terminus Municipal Advisors, LLC",
    value: "K1024-Terminus Municipal Advisors, LLC"
  }, {
    label: "K0359-TESSERA CAPITAL PARTNERS, LLC",
    value: "K0359-TESSERA CAPITAL PARTNERS, LLC"
  }, {
    label: "K0420-THB Advisory LLC",
    value: "K0420-THB Advisory LLC"
  }, {
    label: "A1006-The GMS Group, LLC",
    value: "A1006-The GMS Group, LLC"
  }, {
    label: "A6322-The Williams Capital Group, L.P.",
    value: "A6322-The Williams Capital Group, L.P."
  }, {
    label: "K0469-Therber, Brock & Associates, LLC",
    value: "K0469-Therber, Brock & Associates, LLC"
  }, {
    label: "K0693-Thomas E. Laird",
    value: "K0693-Thomas E. Laird"
  }, {
    label: "A1357-Thornton Farish Inc.",
    value: "A1357-Thornton Farish Inc."
  }, {
    label: "K1071-TIAA-CREF Tuition Financing, Inc.",
    value: "K1071-TIAA-CREF Tuition Financing, Inc."
  }, {
    label: "K0503-TKG & Associates LLC",
    value: "K0503-TKG & Associates LLC"
  }, {
    label: "K0248-TLHocking & Associates LLC",
    value: "K0248-TLHocking & Associates LLC"
  }, {
    label: "K1185-Toni Hackett Antrum",
    value: "K1185-Toni Hackett Antrum"
  }, {
    label: "K0896-Top Capital Advisors, Inc.",
    value: "K0896-Top Capital Advisors, Inc."
  }, {
    label: "K1198-Torain Group",
    value: "K1198-Torain Group"
  }, {
    label: "K1199-Treasury Solutions, LLC",
    value: "K1199-Treasury Solutions, LLC"
  }, {
    label: "K0367-Triago Americas, Inc.",
    value: "K0367-Triago Americas, Inc."
  }, {
    label: "K1006-Trilogy Consulting, LLC",
    value: "K1006-Trilogy Consulting, LLC"
  }, {
    label: "K0820-Trinity Capital Resources, LLC",
    value: "K0820-Trinity Capital Resources, LLC"
  }, {
    label: "K0250-Tschudy Will B.",
    value: "K0250-Tschudy Will B."
  }, {
    label: "K0174-UniBank Fiscal Advisory Services, Inc.",
    value: "K0174-UniBank Fiscal Advisory Services, Inc."
  }, {
    label: "K0322-UNION BANK & TRUST CO NE",
    value: "K0322-UNION BANK & TRUST CO NE"
  }, {
    label: "K0284-Universal Structured Finance Group, Inc.",
    value: "K0284-Universal Structured Finance Group, Inc."
  }, {
    label: "K0256-Urban Futures Incorporated",
    value: "K0256-Urban Futures Incorporated"
  }, {
    label: "K1050-US BANK NATIONAL ASSOCIATION",
    value: "K1050-US BANK NATIONAL ASSOCIATION"
  }, {
    label: "K0872-USCA Municipal Advisors LLC",
    value: "K0872-USCA Municipal Advisors LLC"
  }, {
    label: "K0990-Vanguard Advisers, Inc.",
    value: "K0990-Vanguard Advisers, Inc."
  }, {
    label: "K0516-VeraPath Global Investing, LLC",
    value: "K0516-VeraPath Global Investing, LLC"
  }, {
    label: "K0612-Virginia Local Government Finance Corporation",
    value: "K0612-Virginia Local Government Finance Corporation"
  }, {
    label: "K0376-W J Fawell LLC",
    value: "K0376-W J Fawell LLC"
  }, {
    label: "K0282-Waters and Company, LLC",
    value: "K0282-Waters and Company, LLC"
  }, {
    label: "K0994-Wayne Joseph Neveu",
    value: "K0994-Wayne Joseph Neveu"
  }, {
    label: "A1436-Wedbush Securities Inc.",
    value: "A1436-Wedbush Securities Inc."
  }, {
    label: "K0972-Weist Law Firm",
    value: "K0972-Weist Law Firm"
  }, {
    label: "A5646-Wells Nelson & Associates, LLC",
    value: "A5646-Wells Nelson & Associates, LLC"
  }, {
    label: "K0345-Western Financial Group, LLC",
    value: "K0345-Western Financial Group, LLC"
  }, {
    label: "A3527-Westhoff, Cone & Holmstedt",
    value: "A3527-Westhoff, Cone & Holmstedt"
  }, {
    label: "K0135-WhiteStone Capital Partners, Inc.",
    value: "K0135-WhiteStone Capital Partners, Inc."
  }, {
    label: "K0976-Whitney Bank Trust & Asset Management Municipal Advisors Group - a SID of " +
        "Whitney Bank ",
    value: "K0976-Whitney Bank Trust & Asset Management Municipal Advisors Group - a SID of " +
        "Whitney Bank "
  }, {
    label: "A0747-Wiley Bros-Aintree Capital LLC",
    value: "A0747-Wiley Bros-Aintree Capital LLC"
  }, {
    label: "K0263-William Euphrat Municipal Finance, Inc.",
    value: "K0263-William Euphrat Municipal Finance, Inc."
  }, {
    label: "K0158-Winters & Co. Advisors, LLC",
    value: "K0158-Winters & Co. Advisors, LLC"
  }, {
    label: "K0232-Wisconsin Public Finance Professionals, LLC",
    value: "K0232-Wisconsin Public Finance Professionals, LLC"
  }, {
    label: "K0152-WM Financial Strategies",
    value: "K0152-WM Financial Strategies"
  }, {
    label: "K0212-Wolf & Company Inc",
    value: "K0212-Wolf & Company Inc"
  }, {
    label: "K1062-Wrobel Miriam Sophie",
    value: "K1062-Wrobel Miriam Sophie"
  }, {
    label: "A0763-Wulff, Hansen & Company",
    value: "A0763-Wulff, Hansen & Company"
  }, {
    label: "K0278-Wye River Capital, Inc.",
    value: "K0278-Wye River Capital, Inc."
  }, {
    label: "K0467-XT Capital Partners, LLC",
    value: "K0467-XT Capital Partners, LLC"
  }, {
    label: "K1064-YaCari Consultants, LLC",
    value: "K1064-YaCari Consultants, LLC"
  }, {
    label: "K1096-YMR & Associates, LLC",
    value: "K1096-YMR & Associates, LLC"
  }, {
    label: "K0986-Young America Capital, LLC.",
    value: "K0986-Young America Capital, LLC."
  }, {
    label: "K0151-Yuba Group LLC",
    value: "K0151-Yuba Group LLC"
  }, {
    label: "B0300-ZB, N.AMSD",
    value: "B0300-ZB, N.AMSD"
  }, {
    label: "K1080-Zions Public Finance, Inc.",
    value: "K1080-Zions Public Finance, Inc."
  }, {
    label: "K0500-Zomermaand Financial Advisory Services, LLC",
    value: "K0500-Zomermaand Financial Advisory Services, LLC"
  }
]
module.exports.LKUPFORM = [
  {
    label: "Blank",
    value: "Blank"
  }, {
    label: "Book Entry",
    value: "Book Entry"
  }, {
    label: "Registered",
    value: "Registered"
  }, {
    label: "Bearer",
    value: "Bearer"
  }
]
module.exports.LKUPG17DISCLOSURES = [
  {
    label: "G-17 Disclosure Checklist (fully completed & signed by banker)",
    value: "G-17 Disclosure Checklist (fully completed & signed by banker)"
  }, {
    label: "Conflicts of interest, complex transaction, G-37  Check for conflicts to Complia" +
        "nce",
    value: "Conflicts of interest, complex transaction, G-37  Check for conflicts to Complia" +
        "nce"
  }, {
    label: "Acknowledgment of G-17 Disclosures (if none, document reason in specificity)",
    value: "Acknowledgment of G-17 Disclosures (if none, document reason in specificity)"
  }
]
module.exports.LKUPG17TYPE = [
  {
    label: "Other",
    value: "Other"
  }, {
    label: "Role Disclosure Letter",
    value: "Role Disclosure Letter"
  }, {
    label: "Conflict Letter",
    value: "Conflict Letter"
  }, {
    label: "Role Letter, Disclosure Letter",
    value: "Role Letter, Disclosure Letter"
  }
]
module.exports.LKUPGUARANTOR = [
  {
    label: "Guarantor-1",
    value: "Guarantor-1"
  }, {
    label: "Guarantor-2",
    value: "Guarantor-2"
  }, {
    label: "Guarantor-3",
    value: "Guarantor-3"
  }, {
    label: "Guarantor-4",
    value: "Guarantor-4"
  }, {
    label: "Guarantor-5",
    value: "Guarantor-5"
  }, {
    label: "Guarantor-6",
    value: "Guarantor-6"
  }, {
    label: "Guarantor-7",
    value: "Guarantor-7"
  }, {
    label: "Guarantor-8",
    value: "Guarantor-8"
  }, {
    label: "Guarantor-9",
    value: "Guarantor-9"
  }, {
    label: "Guarantor-10",
    value: "Guarantor-10"
  }
]
module.exports.LKUPINSURANCE = [
  {
    label: "None",
    value: "None"
  }, {
    label: "Partial",
    value: "Partial"
  }, {
    label: "Full",
    value: "Full"
  }
]
module.exports.LKUPISSUER = [
  {
    label: "Abington Heights School District",
    value: "Abington Heights School District"
  }, {
    label: "Allegheny County Sanitary Authority (Alcosan)",
    value: "Allegheny County Sanitary Authority (Alcosan)"
  }, {
    label: "Allegheny Intermediate Unit No. 3",
    value: "Allegheny Intermediate Unit No. 3"
  }, {
    label: "Armstrong County",
    value: "Armstrong County"
  }, {
    label: "Avonworth School District",
    value: "Avonworth School District"
  }, {
    label: "Baldwin Borough",
    value: "Baldwin Borough"
  }, {
    label: "Bedford Area School District",
    value: "Bedford Area School District"
  }, {
    label: "Berks County",
    value: "Berks County"
  }, {
    label: "Bethlehem Authority",
    value: "Bethlehem Authority"
  }, {
    label: "Bloomfield Township",
    value: "Bloomfield Township"
  }, {
    label: "Blue Mountain Area School District",
    value: "Blue Mountain Area School District"
  }, {
    label: "Blue Mountain School District",
    value: "Blue Mountain School District"
  }, {
    label: "Board Of Education Of The City Of Chicago",
    value: "Board Of Education Of The City Of Chicago"
  }, {
    label: "Board Of Trustees Of Grand Valley State University",
    value: "Board Of Trustees Of Grand Valley State University"
  }, {
    label: "Borough Of Bell Acres",
    value: "Borough Of Bell Acres"
  }, {
    label: "Borough Of Green Tree",
    value: "Borough Of Green Tree"
  }, {
    label: "Borough Of Mercer",
    value: "Borough Of Mercer"
  }, {
    label: "Borough Of New Wilmington",
    value: "Borough Of New Wilmington"
  }, {
    label: "Bradford City Water Authority",
    value: "Bradford City Water Authority"
  }, {
    label: "Bridgewater-Raritan Regional School District",
    value: "Bridgewater-Raritan Regional School District"
  }, {
    label: "Bucks County Community College Authority",
    value: "Bucks County Community College Authority"
  }, {
    label: "Burlington County Bridge Commission",
    value: "Burlington County Bridge Commission"
  }, {
    label: "Butler County General Authority",
    value: "Butler County General Authority"
  }, {
    label: "Butler Township",
    value: "Butler Township"
  }, {
    label: "Canon-Mcmillan School District",
    value: "Canon-Mcmillan School District"
  }, {
    label: "Catawba County",
    value: "Catawba County"
  }, {
    label: "Cheltenham Township ",
    value: "Cheltenham Township "
  }, {
    label: "Chester County",
    value: "Chester County"
  }, {
    label: "Chicago Park District",
    value: "Chicago Park District"
  }, {
    label: "Chicago Transit Authority",
    value: "Chicago Transit Authority"
  }, {
    label: "City Of Akron",
    value: "City Of Akron"
  }, {
    label: "City Of Asheville",
    value: "City Of Asheville"
  }, {
    label: "City Of Atlanta, Georgia",
    value: "City Of Atlanta, Georgia"
  }, {
    label: "City Of Butler",
    value: "City Of Butler"
  }, {
    label: "City Of Charlotte",
    value: "City Of Charlotte"
  }, {
    label: "City Of Chesterfield",
    value: "City Of Chesterfield"
  }, {
    label: "City Of Chicago",
    value: "City Of Chicago"
  }, {
    label: "City Of Chicago - Chicago Midway Airport ",
    value: "City Of Chicago - Chicago Midway Airport "
  }, {
    label: "City Of Cincinnati",
    value: "City Of Cincinnati"
  }, {
    label: "City Of Cleveland, Ohio",
    value: "City Of Cleveland, Ohio"
  }, {
    label: "City Of Columbus, Ohio",
    value: "City Of Columbus, Ohio"
  }, {
    label: "City Of Durham",
    value: "City Of Durham"
  }, {
    label: "City Of Erie Higher Education Building Authority",
    value: "City Of Erie Higher Education Building Authority"
  }, {
    label: "City Of Kannapolis, Nc",
    value: "City Of Kannapolis, Nc"
  }, {
    label: "City Of New York",
    value: "City Of New York"
  }, {
    label: "City Of Pataskala",
    value: "City Of Pataskala"
  }, {
    label: "City Of Pembroke Pines",
    value: "City Of Pembroke Pines"
  }, {
    label: "City Of Pittsburgh",
    value: "City Of Pittsburgh"
  }, {
    label: "City Of Pompano Beach, Florida",
    value: "City Of Pompano Beach, Florida"
  }, {
    label: "City Of Reading",
    value: "City Of Reading"
  }, {
    label: "City Of Ventnor",
    value: "City Of Ventnor"
  }, {
    label: "City Of Westerville",
    value: "City Of Westerville"
  }, {
    label: "City Of Wilkes Barre",
    value: "City Of Wilkes Barre"
  }, {
    label: "City Of Wilkes-Barre Finance Authority",
    value: "City Of Wilkes-Barre Finance Authority"
  }, {
    label: "City Of Wilmington",
    value: "City Of Wilmington"
  }, {
    label: "Cleveland Municipal School District",
    value: "Cleveland Municipal School District"
  }, {
    label: "Cleveland-Cuyahoga County Port Authority",
    value: "Cleveland-Cuyahoga County Port Authority"
  }, {
    label: "Clinton County",
    value: "Clinton County"
  }, {
    label: "Coatesville Area School District",
    value: "Coatesville Area School District"
  }, {
    label: "Collier Township",
    value: "Collier Township"
  }, {
    label: "Columbus City School District",
    value: "Columbus City School District"
  }, {
    label: "Commonwealth Financing Authority",
    value: "Commonwealth Financing Authority"
  }, {
    label: "Community Health Network, Inc.",
    value: "Community Health Network, Inc."
  }, {
    label: "Conneaut School District",
    value: "Conneaut School District"
  }, {
    label: "Corry Area School District",
    value: "Corry Area School District"
  }, {
    label: "County Of Allegheny",
    value: "County Of Allegheny"
  }, {
    label: "County Of Brunswick",
    value: "County Of Brunswick"
  }, {
    label: "County Of Chatham, Nc",
    value: "County Of Chatham, Nc"
  }, {
    label: "County Of Fairfield, Ohio",
    value: "County Of Fairfield, Ohio"
  }, {
    label: "County Of Franklin, Ohio",
    value: "County Of Franklin, Ohio"
  }, {
    label: "County Of Monroe",
    value: "County Of Monroe"
  }, {
    label: "County Of Osceola, Fl ",
    value: "County Of Osceola, Fl "
  }, {
    label: "County Of Pitt",
    value: "County Of Pitt"
  }, {
    label: "County Of Somerset",
    value: "County Of Somerset"
  }, {
    label: "County Of York",
    value: "County Of York"
  }, {
    label: "Cranberry Township",
    value: "Cranberry Township"
  }, {
    label: "Cumberland County Municipal Authority",
    value: "Cumberland County Municipal Authority"
  }, {
    label: "Cumberland Valley School District",
    value: "Cumberland Valley School District"
  }, {
    label: "Dallas Area Municipal Authority",
    value: "Dallas Area Municipal Authority"
  }, {
    label: "Dallas School District",
    value: "Dallas School District"
  }, {
    label: "Dayton City School District",
    value: "Dayton City School District"
  }, {
    label: "Delaware Valley School District",
    value: "Delaware Valley School District"
  }, {
    label: "Dormatory Authority Of The State Of Ny",
    value: "Dormatory Authority Of The State Of Ny"
  }, {
    label: "Forest Hills Local School District",
    value: "Forest Hills Local School District"
  }, {
    label: "Girard School District",
    value: "Girard School District"
  }, {
    label: "Grand Valley State University",
    value: "Grand Valley State University"
  }, {
    label: "Greater Cleveland Regional Transit Authority",
    value: "Greater Cleveland Regional Transit Authority"
  }, {
    label: "Greater Nanticoke Area School District",
    value: "Greater Nanticoke Area School District"
  }, {
    label: "Hamilton Township Municipal Authority",
    value: "Hamilton Township Municipal Authority"
  }, {
    label: "Hanover Borough",
    value: "Hanover Borough"
  }, {
    label: "Health And Educational Facilities Authority Of Missouri",
    value: "Health And Educational Facilities Authority Of Missouri"
  }, {
    label: "Hermitage Municipal Authority",
    value: "Hermitage Municipal Authority"
  }, {
    label: "Hermitage School District",
    value: "Hermitage School District"
  }, {
    label: "Housing Authority Of The City Of Milwaukee",
    value: "Housing Authority Of The City Of Milwaukee"
  }, {
    label: "Huntingdon Area School District",
    value: "Huntingdon Area School District"
  }, {
    label: "Huron Valley School District",
    value: "Huron Valley School District"
  }, {
    label: "Illinois Finance Authority",
    value: "Illinois Finance Authority"
  }, {
    label: "Illinois Housing Development Authority",
    value: "Illinois Housing Development Authority"
  }, {
    label: "Illinois Municipal Electric Agency",
    value: "Illinois Municipal Electric Agency"
  }, {
    label: "Illinois State Toll Highway Authority",
    value: "Illinois State Toll Highway Authority"
  }, {
    label: "Indiana County Hospital Authority",
    value: "Indiana County Hospital Authority"
  }, {
    label: "Indiana Finance Authority",
    value: "Indiana Finance Authority"
  }, {
    label: "Indiana Municipal Power Agency",
    value: "Indiana Municipal Power Agency"
  }, {
    label: "Indiana State University Board Of Trustees",
    value: "Indiana State University Board Of Trustees"
  }, {
    label: "Indianapolis Local Public Improvement Bond Bank",
    value: "Indianapolis Local Public Improvement Bond Bank"
  }, {
    label: "Kalamazoo Public Schools",
    value: "Kalamazoo Public Schools"
  }, {
    label: "Keep Memory Alive",
    value: "Keep Memory Alive"
  }, {
    label: "Kent State University",
    value: "Kent State University"
  }, {
    label: "Kentucky AssetLiability Commission",
    value: "Kentucky AssetLiability Commission"
  }, {
    label: "Kentucky Housing Corporation",
    value: "Kentucky Housing Corporation"
  }, {
    label: "Kentucky Public Transportation Infrastructure Authority",
    value: "Kentucky Public Transportation Infrastructure Authority"
  }, {
    label: "Metropolitan Transportation Authority",
    value: "Metropolitan Transportation Authority"
  }, {
    label: "Miami-Dade County Industrial Development Authority",
    value: "Miami-Dade County Industrial Development Authority"
  }, {
    label: "Michigan Finance Authority",
    value: "Michigan Finance Authority"
  }, {
    label: "Michigan Strategic Fund",
    value: "Michigan Strategic Fund"
  }, {
    label: "Monmouth County Improvement Authority",
    value: "Monmouth County Improvement Authority"
  }, {
    label: "Monroe Twp Boe (Gloucester County)",
    value: "Monroe Twp Boe (Gloucester County)"
  }, {
    label: "Montgomery Co. Higher Ed & Health Auth",
    value: "Montgomery Co. Higher Ed & Health Auth"
  }, {
    label: "Montrose Area School District",
    value: "Montrose Area School District"
  }, {
    label: "Mt. Lebanon, Pennsylvania",
    value: "Mt. Lebanon, Pennsylvania"
  }, {
    label: "Multi-Purpose Stadium Authority Of Lackawanna County",
    value: "Multi-Purpose Stadium Authority Of Lackawanna County"
  }, {
    label: "Municipal Authority Of Washington Township",
    value: "Municipal Authority Of Washington Township"
  }, {
    label: "Municipality Of Kingston",
    value: "Municipality Of Kingston"
  }, {
    label: "New Hanover County, Nc",
    value: "New Hanover County, Nc"
  }, {
    label: "New Jersey Building Authority",
    value: "New Jersey Building Authority"
  }, {
    label: "New Jersey Health Care Facilities Financing Authority",
    value: "New Jersey Health Care Facilities Financing Authority"
  }, {
    label: "New Jersey Turnpike Authority",
    value: "New Jersey Turnpike Authority"
  }, {
    label: "New York City Transitional Finance Authority",
    value: "New York City Transitional Finance Authority"
  }, {
    label: "New York Cty G.O.",
    value: "New York Cty G.O."
  }, {
    label: "North Allegheny School District",
    value: "North Allegheny School District"
  }, {
    label: "Northeast Ohio Medical University",
    value: "Northeast Ohio Medical University"
  }, {
    label: "Northwest Area School District",
    value: "Northwest Area School District"
  }, {
    label: "Ny Metropolitan Transportation Authority",
    value: "Ny Metropolitan Transportation Authority"
  }, {
    label: "Oakmont Municipal Authority",
    value: "Oakmont Municipal Authority"
  }, {
    label: "Ohio Higher Edcuational Facility Comission",
    value: "Ohio Higher Edcuational Facility Comission"
  }, {
    label: "Ohio Higher Educational Facility Commission",
    value: "Ohio Higher Educational Facility Commission"
  }, {
    label: "Ohio University",
    value: "Ohio University"
  }, {
    label: "Onslow County Public Facilities Company",
    value: "Onslow County Public Facilities Company"
  }, {
    label: "Palmerton Borough",
    value: "Palmerton Borough"
  }, {
    label: "Penn Hills School District",
    value: "Penn Hills School District"
  }, {
    label: "Pennsylvania Economic Development Financing Authority",
    value: "Pennsylvania Economic Development Financing Authority"
  }, {
    label: "Pennsylvania Higher Educational Facilities Authority",
    value: "Pennsylvania Higher Educational Facilities Authority"
  }, {
    label: "Pennsylvania Turnpike Commission",
    value: "Pennsylvania Turnpike Commission"
  }, {
    label: "Penn-Trafford School District",
    value: "Penn-Trafford School District"
  }, {
    label: "Philadelphia Authority For Industrial Development",
    value: "Philadelphia Authority For Industrial Development"
  }, {
    label: "Pine-Richland School District",
    value: "Pine-Richland School District"
  }, {
    label: "Pittsburgh Water & Sewer Authority",
    value: "Pittsburgh Water & Sewer Authority"
  }, {
    label: "Rocky River City School District",
    value: "Rocky River City School District"
  }, {
    label: "Ross Township",
    value: "Ross Township"
  }, {
    label: "Sales Tax Asset Receivable Corporation",
    value: "Sales Tax Asset Receivable Corporation"
  }, {
    label: "School District Of Hillsborough County",
    value: "School District Of Hillsborough County"
  }, {
    label: "School District Of Pasco County, Fl",
    value: "School District Of Pasco County, Fl"
  }, {
    label: "School District Of Pittsburgh",
    value: "School District Of Pittsburgh"
  }, {
    label: "Somerset County",
    value: "Somerset County"
  }, {
    label: "South Carolina Jobs-Economic Development Authority",
    value: "South Carolina Jobs-Economic Development Authority"
  }, {
    label: "Sports & Exhibition Authority Of Pittsburgh And Allegheny County",
    value: "Sports & Exhibition Authority Of Pittsburgh And Allegheny County"
  }, {
    label: "Springboro Community City School District",
    value: "Springboro Community City School District"
  }, {
    label: "State Of Michigan",
    value: "State Of Michigan"
  }, {
    label: "State Of Ohio",
    value: "State Of Ohio"
  }, {
    label: "State Of Ohio - Treasurer Of State",
    value: "State Of Ohio - Treasurer Of State"
  }, {
    label: "State Of Ohio  Ohio Higher Educational Facility Commission",
    value: "State Of Ohio  Ohio Higher Educational Facility Commission"
  }, {
    label: "State Of Wisconsin",
    value: "State Of Wisconsin"
  }, {
    label: "State Public School Building Authority",
    value: "State Public School Building Authority"
  }, {
    label: "Sumter County (Florida) Industrial Development Authority",
    value: "Sumter County (Florida) Industrial Development Authority"
  }, {
    label: "Sumter County Industrial Authority",
    value: "Sumter County Industrial Authority"
  }, {
    label: "Swatara Township",
    value: "Swatara Township"
  }, {
    label: "The Board Of Education Of The Township Of Monroe",
    value: "The Board Of Education Of The Township Of Monroe"
  }, {
    label: "The Board Of Education Of The Township Of Sparta",
    value: "The Board Of Education Of The Township Of Sparta"
  }, {
    label: "The Board Of Education Of The Upper Freehold Rsd",
    value: "The Board Of Education Of The Upper Freehold Rsd"
  }, {
    label: "The Boe Of The Borough Of Haddonfield",
    value: "The Boe Of The Borough Of Haddonfield"
  }, {
    label: "The Boe Of The Township Of Clinton",
    value: "The Boe Of The Township Of Clinton"
  }, {
    label: "The Boe Of The Township Of Hamilton",
    value: "The Boe Of The Township Of Hamilton"
  }, {
    label: "The Boe Of The Township Of Moorestown",
    value: "The Boe Of The Township Of Moorestown"
  }, {
    label: "The Boe Of The Township Of Randolph",
    value: "The Boe Of The Township Of Randolph"
  }, {
    label: "The Boe Of The Township Of Robbinsville",
    value: "The Boe Of The Township Of Robbinsville"
  }, {
    label: "The Burlington County Bridge Commission",
    value: "The Burlington County Bridge Commission"
  }, {
    label: "The Camden County Improvement Authority",
    value: "The Camden County Improvement Authority"
  }, {
    label: "The City Of New York",
    value: "The City Of New York"
  }, {
    label: "The City Of Philadelphia",
    value: "The City Of Philadelphia"
  }, {
    label: "The City Of Philadelphia ",
    value: "The City Of Philadelphia "
  }, {
    label: "The County Of Cook, Illinois",
    value: "The County Of Cook, Illinois"
  }, {
    label: "The Gloucester County Improvement Authority",
    value: "The Gloucester County Improvement Authority"
  }, {
    label: "The Indianapolis Local Public Improvement Bond Bank",
    value: "The Indianapolis Local Public Improvement Bond Bank"
  }, {
    label: "The Monmouth County Improvement Authority",
    value: "The Monmouth County Improvement Authority"
  }, {
    label: "The School Board Of Broward County",
    value: "The School Board Of Broward County"
  }, {
    label: "The School Board Of Pasco County",
    value: "The School Board Of Pasco County"
  }, {
    label: "The School District Of Philadelphia",
    value: "The School District Of Philadelphia"
  }, {
    label: "The School District Of The City Of Harrisburg",
    value: "The School District Of The City Of Harrisburg"
  }, {
    label: "The Township Of Mount Laurel",
    value: "The Township Of Mount Laurel"
  }, {
    label: "The University Of Akron",
    value: "The University Of Akron"
  }, {
    label: "Titusville Asd",
    value: "Titusville Asd"
  }, {
    label: "Town Of Davie, Florida",
    value: "Town Of Davie, Florida"
  }, {
    label: "Town Of Oak Island",
    value: "Town Of Oak Island"
  }, {
    label: "Township Of Heidelberg",
    value: "Township Of Heidelberg"
  }, {
    label: "Township Of Indiana",
    value: "Township Of Indiana"
  }, {
    label: "Township Of Ohara",
    value: "Township Of Ohara"
  }, {
    label: "Triborough Bridge And Tunnel Authority",
    value: "Triborough Bridge And Tunnel Authority"
  }, {
    label: "Tulpehocken Area School District",
    value: "Tulpehocken Area School District"
  }, {
    label: "Union Co Hospital Authority",
    value: "Union Co Hospital Authority"
  }, {
    label: "Union County",
    value: "Union County"
  }, {
    label: "University Area Joint Authority",
    value: "University Area Joint Authority"
  }, {
    label: "University Of Toledo",
    value: "University Of Toledo"
  }, {
    label: "Upper Arlington",
    value: "Upper Arlington"
  }, {
    label: "Village Of Hawthorn Woods",
    value: "Village Of Hawthorn Woods"
  }, {
    label: "Virginia Small Business Financing Authority",
    value: "Virginia Small Business Financing Authority"
  }, {
    label: "Wayne County Airport Authority",
    value: "Wayne County Airport Authority"
  }, {
    label: "West Allegheny School District",
    value: "West Allegheny School District"
  }, {
    label: "Westmoreland County Airport Authority",
    value: "Westmoreland County Airport Authority"
  }, {
    label: "Wilkes-Barre Area School District",
    value: "Wilkes-Barre Area School District"
  }, {
    label: "Winston Salem State University ",
    value: "Winston Salem State University "
  }, {
    label: "Wright State University",
    value: "Wright State University"
  }, {
    label: "Wyoming County",
    value: "Wyoming County"
  }, {
    label: "York County Industrial Development Authority",
    value: "York County Industrial Development Authority"
  }, {
    label: "Abington Heights School District",
    value: "Abington Heights School District"
  }, {
    label: "Allegheny County Sanitary Authority (Alcosan)",
    value: "Allegheny County Sanitary Authority (Alcosan)"
  }, {
    label: "Allegheny Intermediate Unit No. 3",
    value: "Allegheny Intermediate Unit No. 3"
  }, {
    label: "Armstrong County",
    value: "Armstrong County"
  }, {
    label: "Avonworth School District",
    value: "Avonworth School District"
  }, {
    label: "Baldwin Borough",
    value: "Baldwin Borough"
  }, {
    label: "Bedford Area School District",
    value: "Bedford Area School District"
  }, {
    label: "Berks County",
    value: "Berks County"
  }, {
    label: "Bethlehem Authority",
    value: "Bethlehem Authority"
  }, {
    label: "Bloomfield Township",
    value: "Bloomfield Township"
  }, {
    label: "Blue Mountain Area School District",
    value: "Blue Mountain Area School District"
  }, {
    label: "Blue Mountain School District",
    value: "Blue Mountain School District"
  }, {
    label: "Board Of Education Of The City Of Chicago",
    value: "Board Of Education Of The City Of Chicago"
  }, {
    label: "Board Of Trustees Of Grand Valley State University",
    value: "Board Of Trustees Of Grand Valley State University"
  }, {
    label: "Borough Of Bell Acres",
    value: "Borough Of Bell Acres"
  }, {
    label: "Borough Of Green Tree",
    value: "Borough Of Green Tree"
  }, {
    label: "Borough Of Mercer",
    value: "Borough Of Mercer"
  }, {
    label: "Borough Of New Wilmington",
    value: "Borough Of New Wilmington"
  }, {
    label: "Bradford City Water Authority",
    value: "Bradford City Water Authority"
  }, {
    label: "Bridgewater-Raritan Regional School District",
    value: "Bridgewater-Raritan Regional School District"
  }, {
    label: "Bucks County Community College Authority",
    value: "Bucks County Community College Authority"
  }, {
    label: "Burlington County Bridge Commission",
    value: "Burlington County Bridge Commission"
  }, {
    label: "Butler County General Authority",
    value: "Butler County General Authority"
  }, {
    label: "Butler Township",
    value: "Butler Township"
  }, {
    label: "Canon-Mcmillan School District",
    value: "Canon-Mcmillan School District"
  }, {
    label: "Catawba County",
    value: "Catawba County"
  }, {
    label: "Cheltenham Township ",
    value: "Cheltenham Township "
  }, {
    label: "Chester County",
    value: "Chester County"
  }, {
    label: "Chicago Park District",
    value: "Chicago Park District"
  }, {
    label: "Chicago Transit Authority",
    value: "Chicago Transit Authority"
  }, {
    label: "City Of Akron",
    value: "City Of Akron"
  }, {
    label: "City Of Asheville",
    value: "City Of Asheville"
  }, {
    label: "City Of Atlanta, Georgia",
    value: "City Of Atlanta, Georgia"
  }, {
    label: "City Of Butler",
    value: "City Of Butler"
  }, {
    label: "City Of Charlotte",
    value: "City Of Charlotte"
  }, {
    label: "City Of Chesterfield",
    value: "City Of Chesterfield"
  }, {
    label: "City Of Chicago",
    value: "City Of Chicago"
  }, {
    label: "City Of Chicago - Chicago Midway Airport ",
    value: "City Of Chicago - Chicago Midway Airport "
  }, {
    label: "City Of Cincinnati",
    value: "City Of Cincinnati"
  }, {
    label: "City Of Cleveland, Ohio",
    value: "City Of Cleveland, Ohio"
  }, {
    label: "City Of Columbus, Ohio",
    value: "City Of Columbus, Ohio"
  }, {
    label: "City Of Durham",
    value: "City Of Durham"
  }, {
    label: "City Of Erie Higher Education Building Authority",
    value: "City Of Erie Higher Education Building Authority"
  }, {
    label: "City Of Kannapolis, Nc",
    value: "City Of Kannapolis, Nc"
  }, {
    label: "City Of New York",
    value: "City Of New York"
  }, {
    label: "City Of Pataskala",
    value: "City Of Pataskala"
  }, {
    label: "City Of Pembroke Pines",
    value: "City Of Pembroke Pines"
  }, {
    label: "City Of Pittsburgh",
    value: "City Of Pittsburgh"
  }, {
    label: "City Of Pompano Beach, Florida",
    value: "City Of Pompano Beach, Florida"
  }, {
    label: "City Of Reading",
    value: "City Of Reading"
  }, {
    label: "City Of Ventnor",
    value: "City Of Ventnor"
  }, {
    label: "City Of Westerville",
    value: "City Of Westerville"
  }, {
    label: "City Of Wilkes Barre",
    value: "City Of Wilkes Barre"
  }, {
    label: "City Of Wilkes-Barre Finance Authority",
    value: "City Of Wilkes-Barre Finance Authority"
  }, {
    label: "City Of Wilmington",
    value: "City Of Wilmington"
  }, {
    label: "Cleveland Municipal School District",
    value: "Cleveland Municipal School District"
  }, {
    label: "Cleveland-Cuyahoga County Port Authority",
    value: "Cleveland-Cuyahoga County Port Authority"
  }, {
    label: "Clinton County",
    value: "Clinton County"
  }, {
    label: "Coatesville Area School District",
    value: "Coatesville Area School District"
  }, {
    label: "Collier Township",
    value: "Collier Township"
  }, {
    label: "Columbus City School District",
    value: "Columbus City School District"
  }, {
    label: "Commonwealth Financing Authority",
    value: "Commonwealth Financing Authority"
  }, {
    label: "Community Health Network, Inc.",
    value: "Community Health Network, Inc."
  }, {
    label: "Conneaut School District",
    value: "Conneaut School District"
  }, {
    label: "Corry Area School District",
    value: "Corry Area School District"
  }, {
    label: "County Of Allegheny",
    value: "County Of Allegheny"
  }, {
    label: "County Of Brunswick",
    value: "County Of Brunswick"
  }, {
    label: "County Of Chatham, Nc",
    value: "County Of Chatham, Nc"
  }, {
    label: "County Of Fairfield, Ohio",
    value: "County Of Fairfield, Ohio"
  }, {
    label: "County Of Franklin, Ohio",
    value: "County Of Franklin, Ohio"
  }, {
    label: "County Of Monroe",
    value: "County Of Monroe"
  }, {
    label: "County Of Osceola, Fl ",
    value: "County Of Osceola, Fl "
  }, {
    label: "County Of Pitt",
    value: "County Of Pitt"
  }, {
    label: "County Of Somerset",
    value: "County Of Somerset"
  }, {
    label: "County Of York",
    value: "County Of York"
  }, {
    label: "Cranberry Township",
    value: "Cranberry Township"
  }, {
    label: "Cumberland County Municipal Authority",
    value: "Cumberland County Municipal Authority"
  }, {
    label: "Cumberland Valley School District",
    value: "Cumberland Valley School District"
  }, {
    label: "Dallas Area Municipal Authority",
    value: "Dallas Area Municipal Authority"
  }, {
    label: "Dallas School District",
    value: "Dallas School District"
  }, {
    label: "Dayton City School District",
    value: "Dayton City School District"
  }, {
    label: "Delaware Valley School District",
    value: "Delaware Valley School District"
  }, {
    label: "Dormatory Authority Of The State Of Ny",
    value: "Dormatory Authority Of The State Of Ny"
  }, {
    label: "Forest Hills Local School District",
    value: "Forest Hills Local School District"
  }, {
    label: "Girard School District",
    value: "Girard School District"
  }, {
    label: "Grand Valley State University",
    value: "Grand Valley State University"
  }, {
    label: "Greater Cleveland Regional Transit Authority",
    value: "Greater Cleveland Regional Transit Authority"
  }, {
    label: "Greater Nanticoke Area School District",
    value: "Greater Nanticoke Area School District"
  }, {
    label: "Hamilton Township Municipal Authority",
    value: "Hamilton Township Municipal Authority"
  }, {
    label: "Hanover Borough",
    value: "Hanover Borough"
  }, {
    label: "Health And Educational Facilities Authority Of Missouri",
    value: "Health And Educational Facilities Authority Of Missouri"
  }, {
    label: "Hermitage Municipal Authority",
    value: "Hermitage Municipal Authority"
  }, {
    label: "Hermitage School District",
    value: "Hermitage School District"
  }, {
    label: "Housing Authority Of The City Of Milwaukee",
    value: "Housing Authority Of The City Of Milwaukee"
  }, {
    label: "Huntingdon Area School District",
    value: "Huntingdon Area School District"
  }, {
    label: "Huron Valley School District",
    value: "Huron Valley School District"
  }, {
    label: "Illinois Finance Authority",
    value: "Illinois Finance Authority"
  }, {
    label: "Illinois Housing Development Authority",
    value: "Illinois Housing Development Authority"
  }, {
    label: "Illinois Municipal Electric Agency",
    value: "Illinois Municipal Electric Agency"
  }, {
    label: "Illinois State Toll Highway Authority",
    value: "Illinois State Toll Highway Authority"
  }, {
    label: "Indiana County Hospital Authority",
    value: "Indiana County Hospital Authority"
  }, {
    label: "Indiana Finance Authority",
    value: "Indiana Finance Authority"
  }, {
    label: "Indiana Municipal Power Agency",
    value: "Indiana Municipal Power Agency"
  }, {
    label: "Indiana State University Board Of Trustees",
    value: "Indiana State University Board Of Trustees"
  }, {
    label: "Indianapolis Local Public Improvement Bond Bank",
    value: "Indianapolis Local Public Improvement Bond Bank"
  }, {
    label: "Kalamazoo Public Schools",
    value: "Kalamazoo Public Schools"
  }, {
    label: "Keep Memory Alive",
    value: "Keep Memory Alive"
  }, {
    label: "Kent State University",
    value: "Kent State University"
  }, {
    label: "Kentucky AssetLiability Commission",
    value: "Kentucky AssetLiability Commission"
  }, {
    label: "Kentucky Housing Corporation",
    value: "Kentucky Housing Corporation"
  }, {
    label: "Kentucky Public Transportation Infrastructure Authority",
    value: "Kentucky Public Transportation Infrastructure Authority"
  }, {
    label: "Metropolitan Transportation Authority",
    value: "Metropolitan Transportation Authority"
  }, {
    label: "Miami-Dade County Industrial Development Authority",
    value: "Miami-Dade County Industrial Development Authority"
  }, {
    label: "Michigan Finance Authority",
    value: "Michigan Finance Authority"
  }, {
    label: "Michigan Strategic Fund",
    value: "Michigan Strategic Fund"
  }, {
    label: "Monmouth County Improvement Authority",
    value: "Monmouth County Improvement Authority"
  }, {
    label: "Monroe Twp Boe (Gloucester County)",
    value: "Monroe Twp Boe (Gloucester County)"
  }, {
    label: "Montgomery Co. Higher Ed & Health Auth",
    value: "Montgomery Co. Higher Ed & Health Auth"
  }, {
    label: "Montrose Area School District",
    value: "Montrose Area School District"
  }, {
    label: "Mt. Lebanon, Pennsylvania",
    value: "Mt. Lebanon, Pennsylvania"
  }, {
    label: "Multi-Purpose Stadium Authority Of Lackawanna County",
    value: "Multi-Purpose Stadium Authority Of Lackawanna County"
  }, {
    label: "Municipal Authority Of Washington Township",
    value: "Municipal Authority Of Washington Township"
  }, {
    label: "Municipality Of Kingston",
    value: "Municipality Of Kingston"
  }, {
    label: "New Hanover County, Nc",
    value: "New Hanover County, Nc"
  }, {
    label: "New Jersey Building Authority",
    value: "New Jersey Building Authority"
  }, {
    label: "New Jersey Health Care Facilities Financing Authority",
    value: "New Jersey Health Care Facilities Financing Authority"
  }, {
    label: "New Jersey Turnpike Authority",
    value: "New Jersey Turnpike Authority"
  }, {
    label: "New York City Transitional Finance Authority",
    value: "New York City Transitional Finance Authority"
  }, {
    label: "New York Cty G.O.",
    value: "New York Cty G.O."
  }, {
    label: "North Allegheny School District",
    value: "North Allegheny School District"
  }, {
    label: "Northeast Ohio Medical University",
    value: "Northeast Ohio Medical University"
  }, {
    label: "Northwest Area School District",
    value: "Northwest Area School District"
  }, {
    label: "Ny Metropolitan Transportation Authority",
    value: "Ny Metropolitan Transportation Authority"
  }, {
    label: "Oakmont Municipal Authority",
    value: "Oakmont Municipal Authority"
  }, {
    label: "Ohio Higher Edcuational Facility Comission",
    value: "Ohio Higher Edcuational Facility Comission"
  }, {
    label: "Ohio Higher Educational Facility Commission",
    value: "Ohio Higher Educational Facility Commission"
  }, {
    label: "Ohio University",
    value: "Ohio University"
  }, {
    label: "Onslow County Public Facilities Company",
    value: "Onslow County Public Facilities Company"
  }, {
    label: "Palmerton Borough",
    value: "Palmerton Borough"
  }, {
    label: "Penn Hills School District",
    value: "Penn Hills School District"
  }, {
    label: "Pennsylvania Economic Development Financing Authority",
    value: "Pennsylvania Economic Development Financing Authority"
  }, {
    label: "Pennsylvania Higher Educational Facilities Authority",
    value: "Pennsylvania Higher Educational Facilities Authority"
  }, {
    label: "Pennsylvania Turnpike Commission",
    value: "Pennsylvania Turnpike Commission"
  }, {
    label: "Penn-Trafford School District",
    value: "Penn-Trafford School District"
  }, {
    label: "Philadelphia Authority For Industrial Development",
    value: "Philadelphia Authority For Industrial Development"
  }, {
    label: "Pine-Richland School District",
    value: "Pine-Richland School District"
  }, {
    label: "Pittsburgh Water & Sewer Authority",
    value: "Pittsburgh Water & Sewer Authority"
  }, {
    label: "Rocky River City School District",
    value: "Rocky River City School District"
  }, {
    label: "Ross Township",
    value: "Ross Township"
  }, {
    label: "Sales Tax Asset Receivable Corporation",
    value: "Sales Tax Asset Receivable Corporation"
  }, {
    label: "School District Of Hillsborough County",
    value: "School District Of Hillsborough County"
  }, {
    label: "School District Of Pasco County, Fl",
    value: "School District Of Pasco County, Fl"
  }, {
    label: "School District Of Pittsburgh",
    value: "School District Of Pittsburgh"
  }, {
    label: "Somerset County",
    value: "Somerset County"
  }, {
    label: "South Carolina Jobs-Economic Development Authority",
    value: "South Carolina Jobs-Economic Development Authority"
  }, {
    label: "Sports & Exhibition Authority Of Pittsburgh And Allegheny County",
    value: "Sports & Exhibition Authority Of Pittsburgh And Allegheny County"
  }, {
    label: "Springboro Community City School District",
    value: "Springboro Community City School District"
  }, {
    label: "State Of Michigan",
    value: "State Of Michigan"
  }, {
    label: "State Of Ohio",
    value: "State Of Ohio"
  }, {
    label: "State Of Ohio - Treasurer Of State",
    value: "State Of Ohio - Treasurer Of State"
  }, {
    label: "State Of Ohio  Ohio Higher Educational Facility Commission",
    value: "State Of Ohio  Ohio Higher Educational Facility Commission"
  }, {
    label: "State Of Wisconsin",
    value: "State Of Wisconsin"
  }, {
    label: "State Public School Building Authority",
    value: "State Public School Building Authority"
  }, {
    label: "Sumter County (Florida) Industrial Development Authority",
    value: "Sumter County (Florida) Industrial Development Authority"
  }, {
    label: "Sumter County Industrial Authority",
    value: "Sumter County Industrial Authority"
  }, {
    label: "Swatara Township",
    value: "Swatara Township"
  }, {
    label: "The Board Of Education Of The Township Of Monroe",
    value: "The Board Of Education Of The Township Of Monroe"
  }, {
    label: "The Board Of Education Of The Township Of Sparta",
    value: "The Board Of Education Of The Township Of Sparta"
  }, {
    label: "The Board Of Education Of The Upper Freehold Rsd",
    value: "The Board Of Education Of The Upper Freehold Rsd"
  }, {
    label: "The Boe Of The Borough Of Haddonfield",
    value: "The Boe Of The Borough Of Haddonfield"
  }, {
    label: "The Boe Of The Township Of Clinton",
    value: "The Boe Of The Township Of Clinton"
  }, {
    label: "The Boe Of The Township Of Hamilton",
    value: "The Boe Of The Township Of Hamilton"
  }, {
    label: "The Boe Of The Township Of Moorestown",
    value: "The Boe Of The Township Of Moorestown"
  }, {
    label: "The Boe Of The Township Of Randolph",
    value: "The Boe Of The Township Of Randolph"
  }, {
    label: "The Boe Of The Township Of Robbinsville",
    value: "The Boe Of The Township Of Robbinsville"
  }, {
    label: "The Burlington County Bridge Commission",
    value: "The Burlington County Bridge Commission"
  }, {
    label: "The Camden County Improvement Authority",
    value: "The Camden County Improvement Authority"
  }, {
    label: "The City Of New York",
    value: "The City Of New York"
  }, {
    label: "The City Of Philadelphia",
    value: "The City Of Philadelphia"
  }, {
    label: "The City Of Philadelphia ",
    value: "The City Of Philadelphia "
  }, {
    label: "The County Of Cook, Illinois",
    value: "The County Of Cook, Illinois"
  }, {
    label: "The Gloucester County Improvement Authority",
    value: "The Gloucester County Improvement Authority"
  }, {
    label: "The Indianapolis Local Public Improvement Bond Bank",
    value: "The Indianapolis Local Public Improvement Bond Bank"
  }, {
    label: "The Monmouth County Improvement Authority",
    value: "The Monmouth County Improvement Authority"
  }, {
    label: "The School Board Of Broward County",
    value: "The School Board Of Broward County"
  }, {
    label: "The School Board Of Pasco County",
    value: "The School Board Of Pasco County"
  }, {
    label: "The School District Of Philadelphia",
    value: "The School District Of Philadelphia"
  }, {
    label: "The School District Of The City Of Harrisburg",
    value: "The School District Of The City Of Harrisburg"
  }, {
    label: "The Township Of Mount Laurel",
    value: "The Township Of Mount Laurel"
  }, {
    label: "The University Of Akron",
    value: "The University Of Akron"
  }, {
    label: "Titusville Asd",
    value: "Titusville Asd"
  }, {
    label: "Town Of Davie, Florida",
    value: "Town Of Davie, Florida"
  }, {
    label: "Town Of Oak Island",
    value: "Town Of Oak Island"
  }, {
    label: "Township Of Heidelberg",
    value: "Township Of Heidelberg"
  }, {
    label: "Township Of Indiana",
    value: "Township Of Indiana"
  }, {
    label: "Township Of Ohara",
    value: "Township Of Ohara"
  }, {
    label: "Triborough Bridge And Tunnel Authority",
    value: "Triborough Bridge And Tunnel Authority"
  }, {
    label: "Tulpehocken Area School District",
    value: "Tulpehocken Area School District"
  }, {
    label: "Union Co Hospital Authority",
    value: "Union Co Hospital Authority"
  }, {
    label: "Union County",
    value: "Union County"
  }, {
    label: "University Area Joint Authority",
    value: "University Area Joint Authority"
  }, {
    label: "University Of Toledo",
    value: "University Of Toledo"
  }, {
    label: "Upper Arlington",
    value: "Upper Arlington"
  }, {
    label: "Village Of Hawthorn Woods",
    value: "Village Of Hawthorn Woods"
  }, {
    label: "Virginia Small Business Financing Authority",
    value: "Virginia Small Business Financing Authority"
  }, {
    label: "Wayne County Airport Authority",
    value: "Wayne County Airport Authority"
  }, {
    label: "West Allegheny School District",
    value: "West Allegheny School District"
  }, {
    label: "Westmoreland County Airport Authority",
    value: "Westmoreland County Airport Authority"
  }, {
    label: "Wilkes-Barre Area School District",
    value: "Wilkes-Barre Area School District"
  }, {
    label: "Winston Salem State University ",
    value: "Winston Salem State University "
  }, {
    label: "Wright State University",
    value: "Wright State University"
  }, {
    label: "Wyoming County",
    value: "Wyoming County"
  }, {
    label: "York County Industrial Development Authority",
    value: "York County Industrial Development Authority"
  }
]
module.exports.LKUPLEGALDOCS = [
  {
    label: "Underwriters certificate",
    value: "Underwriters certificate"
  }, {
    label: "Document CD (in folder)",
    value: "Document CD (in folder)"
  }
]
module.exports.LKUPLEGALOPINIONS = [
  {
    label: "FinancialsProjectionsCoverage Analysis",
    value: "FinancialsProjectionsCoverage Analysis"
  }, {
    label: "Due Diligence Meeting Dates, Questions and answers",
    value: "Due Diligence Meeting Dates, Questions and answers"
  }, {
    label: "Completed Analyst task list (see TRG Notification 3192013)",
    value: "Completed Analyst task list (see TRG Notification 3192013)"
  }
]
module.exports.LKUPMAEXEMPTION = [
  {
    label: "IRMA",
    value: "IRMA"
  }, {
    label: "RFP",
    value: "RFP"
  }, {
    label: "Underwriting",
    value: "Underwriting"
  }, {
    label: "NA",
    value: "NA"
  }
]
module.exports.LKUPMARKETINGMATERIALS = [
  {
    label: "PresentationsProposals",
    value: "PresentationsProposals"
  }, {
    label: "RFP  RFQ Responses",
    value: "RFP  RFQ Responses"
  }, {
    label: "Refunding Letters",
    value: "Refunding Letters"
  }
]
module.exports.LKUPMONEYTRANSFERTYPE = [
  {
    label: "Wire",
    value: "Wire"
  }, {
    label: "Check",
    value: "Check"
  }, {
    label: "Others",
    value: "Others"
  }, {
    label: "NA",
    value: "NA"
  }
]
module.exports.LKUPNAMEPREFIX = [
  {
    label: "Mr",
    value: "Mr"
  }, {
    label: "Mrs",
    value: "Mrs"
  }, {
    label: "Ms",
    value: "Ms"
  }, {
    label: "Hon",
    value: "Hon"
  }, {
    label: "Blank",
    value: "Blank"
  }
]
module.exports.LKUPNAMESUFFIX = [
  {
    label: "I",
    value: "I"
  }, {
    label: "II",
    value: "II"
  }, {
    label: "III",
    value: "III"
  }, {
    label: "IV",
    value: "IV"
  }, {
    label: "Jr",
    value: "Jr"
  }, {
    label: "Sr",
    value: "Sr"
  }, {
    label: "Blank",
    value: "Blank"
  }
]
module.exports.LKUPNUMOFPEOPLE = [
  {
    label: "NA",
    value: "NA"
  }, {
    label: "less than 5",
    value: "< 5"
  }, {
    label: "5 To 20",
    value: "5 to 20"
  }, {
    label: "more than 20",
    value: "20 +"
  }, {
    label: "less than 50",
    value: "less than 50"
  }, {
    label: "more than 50",
    value: "more than 50"
  }, {
    label: "more than 100",
    value: "more than 100"
  }
]
module.exports.LKUPOUTLOOK = [
  {
    label: "Moodys-Positive",
    value: "Moodys-Positive"
  }, {
    label: "Moodys-Negative",
    value: "Moodys-Negative"
  }, {
    label: "Moodys-Stable",
    value: "Moodys-Stable"
  }, {
    label: "Moodys-Developing",
    value: "Moodys-Developing"
  }, {
    label: "S&P-Positive",
    value: "S&P-Positive"
  }, {
    label: "S&P-Negative",
    value: "S&P-Negative"
  }, {
    label: "S&P-Stable",
    value: "S&P-Stable"
  }, {
    label: "S&P-Developing",
    value: "S&P-Developing"
  }, {
    label: "S&P-Not Meaningful",
    value: "S&P-Not Meaningful"
  }, {
    label: "Kroll-Positive",
    value: "Kroll-Positive"
  }, {
    label: "Kroll-Negative",
    value: "Kroll-Negative"
  }, {
    label: "Kroll-Stable",
    value: "Kroll-Stable"
  }, {
    label: "Kroll-Developing",
    value: "Kroll-Developing"
  }, {
    label: "Fitch-Positive",
    value: "Fitch-Positive"
  }, {
    label: "Fitch-Negative",
    value: "Fitch-Negative"
  }, {
    label: "Fitch-Stable",
    value: "Fitch-Stable"
  }, {
    label: "Fitch-Evolving",
    value: "Fitch-Evolving"
  }
]
module.exports.LKUPPARTICIPANTTYPE = [
  {
    label: "Issuer",
    value: "Issuer"
  }, {
    label: "Borrower",
    value: "Borrower"
  }, {
    label: "Obligor",
    value: "Obligor"
  }, {
    label: "Guarantor",
    value: "Guarantor"
  }, {
    label: "Credit Enhancement Provider",
    value: "Credit Enhancement Provider"
  }, {
    label: "Bond Counsel",
    value: "Bond Counsel"
  }, {
    label: "Bond Insurer",
    value: "Bond Insurer"
  }, {
    label: "Underwriter Counsel",
    value: "Underwriter Counsel"
  }, {
    label: "Verification Agent",
    value: "Verification Agent"
  }, {
    label: "Paying Agent",
    value: "Paying Agent"
  }, {
    label: "Escrow Agent",
    value: "Escrow Agent"
  }, {
    label: "Rating Agency",
    value: "Rating Agency"
  }, {
    label: "Underwriter",
    value: "Underwriter"
  }, {
    label: "Syndicate Member",
    value: "Syndicate Member"
  }, {
    label: "Insurance Provider",
    value: "Insurance Provider"
  }, {
    label: "Conduit Borrower",
    value: "Conduit Borrower"
  }, {
    label: "Lending Bank",
    value: "Lending Bank"
  }, {
    label: "Other",
    value: "Other"
  }
]
module.exports.LKUPPHONETYPE = [
  {
    label: "Office",
    value: "Office"
  }, {
    label: "Cell",
    value: "Cell"
  }, {
    label: "Landline",
    value: "Landline"
  }, {
    label: "Home",
    value: "Home"
  }, {
    label: "Other",
    value: "Other"
  }
]
module.exports.LKUPPRESALEDOCTYPE = [
  {
    label: "Blank",
    value: "Blank"
  }, {
    label: "Presentations",
    value: "Presentations"
  }, {
    label: "RFP Responses",
    value: "RFP Responses"
  }, {
    label: "Refunding Letters",
    value: "Refunding Letters"
  }, {
    label: "Proposals",
    value: "Proposals"
  }, {
    label: "RFQ Responses",
    value: "RFQ Responses"
  }
]
module.exports.LKUPPRICINGSTATUS = [
  {
    label: "Initial Pricing",
    value: "Initial Pricing"
  }, {
    label: "Pre-Pricing",
    value: "Pre-Pricing"
  }, {
    label: "Re-Pricing",
    value: "Re-Pricing"
  }, {
    label: "Final Pricing",
    value: "Final Pricing"
  }
]
module.exports.LKUPRAINSURERDOCS = [
  {
    label: "Credit Report",
    value: "Credit Report"
  }, {
    label: "Insurance Letter",
    value: "Insurance Letter"
  }, {
    label: "LOC documents",
    value: "LOC documents"
  }
]
module.exports.LKUPRATETYPE = [
  {
    label: "Fixed",
    value: "Fixed"
  }, {
    label: "Variable",
    value: "Variable"
  }, {
    label: "Other",
    value: "Other"
  }
]
module.exports.LKUPRATING = [
  {
    label: "Moodys-Blank",
    value: "Moodys-Blank"
  }, {
    label: "Moodys-Aaa",
    value: "Moodys-Aaa"
  }, {
    label: "Moodys-Aa1",
    value: "Moodys-Aa1"
  }, {
    label: "Moodys-Aa2",
    value: "Moodys-Aa2"
  }, {
    label: "Moodys-Aa3",
    value: "Moodys-Aa3"
  }, {
    label: "Moodys-Aa",
    value: "Moodys-Aa"
  }, {
    label: "Moodys-A1",
    value: "Moodys-A1"
  }, {
    label: "Moodys-A2",
    value: "Moodys-A2"
  }, {
    label: "Moodys-A3",
    value: "Moodys-A3"
  }, {
    label: "Moodys-A",
    value: "Moodys-A"
  }, {
    label: "Moodys-Baa1",
    value: "Moodys-Baa1"
  }, {
    label: "Moodys-Baa2",
    value: "Moodys-Baa2"
  }, {
    label: "Moodys-Baa3",
    value: "Moodys-Baa3"
  }, {
    label: "Moodys-Baa",
    value: "Moodys-Baa"
  }, {
    label: "Moodys-Ba1",
    value: "Moodys-Ba1"
  }, {
    label: "Moodys-Ba2",
    value: "Moodys-Ba2"
  }, {
    label: "Moodys-Ba3",
    value: "Moodys-Ba3"
  }, {
    label: "Moodys-Ba",
    value: "Moodys-Ba"
  }, {
    label: "Moodys-B1",
    value: "Moodys-B1"
  }, {
    label: "Moodys-B2",
    value: "Moodys-B2"
  }, {
    label: "Moodys-B3",
    value: "Moodys-B3"
  }, {
    label: "Moodys-B",
    value: "Moodys-B"
  }, {
    label: "Moodys-Caa",
    value: "Moodys-Caa"
  }, {
    label: "Moodys-Caa1",
    value: "Moodys-Caa1"
  }, {
    label: "Moodys-Caa2",
    value: "Moodys-Caa2"
  }, {
    label: "Moodys-Caa3",
    value: "Moodys-Caa3"
  }, {
    label: "Moodys-Ca",
    value: "Moodys-Ca"
  }, {
    label: "Moodys-C",
    value: "Moodys-C"
  }, {
    label: "Moodys-N.A.",
    value: "Moodys-N.A."
  }, {
    label: "Moodys-NR",
    value: "Moodys-NR"
  }, {
    label: "S&P-Blank",
    value: "S&P-Blank"
  }, {
    label: "S&P-AAA",
    value: "S&P-AAA"
  }, {
    label: "S&P-AA+",
    value: "S&P-AA+"
  }, {
    label: "S&P-AA",
    value: "S&P-AA"
  }, {
    label: "S&P-AA-",
    value: "S&P-AA-"
  }, {
    label: "S&P-A+",
    value: "S&P-A+"
  }, {
    label: "S&P-A",
    value: "S&P-A"
  }, {
    label: "S&P-A-",
    value: "S&P-A-"
  }, {
    label: "S&P-BBB+",
    value: "S&P-BBB+"
  }, {
    label: "S&P-BBB",
    value: "S&P-BBB"
  }, {
    label: "S&P-BBB-",
    value: "S&P-BBB-"
  }, {
    label: "S&P-BB+",
    value: "S&P-BB+"
  }, {
    label: "S&P-BB",
    value: "S&P-BB"
  }, {
    label: "S&P-BB-",
    value: "S&P-BB-"
  }, {
    label: "S&P-B+",
    value: "S&P-B+"
  }, {
    label: "S&P-B",
    value: "S&P-B"
  }, {
    label: "S&P-B-",
    value: "S&P-B-"
  }, {
    label: "S&P-CCC+",
    value: "S&P-CCC+"
  }, {
    label: "S&P-CCC",
    value: "S&P-CCC"
  }, {
    label: "S&P-CCC-",
    value: "S&P-CCC-"
  }, {
    label: "S&P-CC",
    value: "S&P-CC"
  }, {
    label: "S&P-C",
    value: "S&P-C"
  }, {
    label: "S&P-R",
    value: "S&P-R"
  }, {
    label: "S&P-SD",
    value: "S&P-SD"
  }, {
    label: "S&P-D",
    value: "S&P-D"
  }, {
    label: "S&P-N.A.",
    value: "S&P-N.A."
  }, {
    label: "S&P-WR",
    value: "S&P-WR"
  }, {
    label: "S&P-NR",
    value: "S&P-NR"
  }, {
    label: "Kroll-Blank",
    value: "Kroll-Blank"
  }, {
    label: "Kroll-AAA",
    value: "Kroll-AAA"
  }, {
    label: "Kroll-AA+",
    value: "Kroll-AA+"
  }, {
    label: "Kroll-AA",
    value: "Kroll-AA"
  }, {
    label: "Kroll-AA-",
    value: "Kroll-AA-"
  }, {
    label: "Kroll-A+",
    value: "Kroll-A+"
  }, {
    label: "Kroll-A",
    value: "Kroll-A"
  }, {
    label: "Kroll-A-",
    value: "Kroll-A-"
  }, {
    label: "Kroll-BBB+",
    value: "Kroll-BBB+"
  }, {
    label: "Kroll-BBB",
    value: "Kroll-BBB"
  }, {
    label: "Kroll-BBB-",
    value: "Kroll-BBB-"
  }, {
    label: "Kroll-BB+",
    value: "Kroll-BB+"
  }, {
    label: "Kroll-BB",
    value: "Kroll-BB"
  }, {
    label: "Kroll-BB-",
    value: "Kroll-BB-"
  }, {
    label: "Kroll-B+",
    value: "Kroll-B+"
  }, {
    label: "Kroll-B",
    value: "Kroll-B"
  }, {
    label: "Kroll-B-",
    value: "Kroll-B-"
  }, {
    label: "Kroll-CCC+",
    value: "Kroll-CCC+"
  }, {
    label: "Kroll-CCC",
    value: "Kroll-CCC"
  }, {
    label: "Kroll-CCC-",
    value: "Kroll-CCC-"
  }, {
    label: "Kroll-CC",
    value: "Kroll-CC"
  }, {
    label: "Kroll-C",
    value: "Kroll-C"
  }, {
    label: "Kroll-D",
    value: "Kroll-D"
  }, {
    label: "Fitch-Blank",
    value: "Fitch-Blank"
  }, {
    label: "Fitch-AAA",
    value: "Fitch-AAA"
  }, {
    label: "Fitch-AA+",
    value: "Fitch-AA+"
  }, {
    label: "Fitch-AA",
    value: "Fitch-AA"
  }, {
    label: "Fitch-AA-",
    value: "Fitch-AA-"
  }, {
    label: "Fitch-A+",
    value: "Fitch-A+"
  }, {
    label: "Fitch-A",
    value: "Fitch-A"
  }, {
    label: "Fitch-A-",
    value: "Fitch-A-"
  }, {
    label: "Fitch-BBB+",
    value: "Fitch-BBB+"
  }, {
    label: "Fitch-BBB",
    value: "Fitch-BBB"
  }, {
    label: "Fitch-BBB-",
    value: "Fitch-BBB-"
  }, {
    label: "Fitch-BB+",
    value: "Fitch-BB+"
  }, {
    label: "Fitch-BB",
    value: "Fitch-BB"
  }, {
    label: "Fitch-BB-",
    value: "Fitch-BB-"
  }, {
    label: "Fitch-B+",
    value: "Fitch-B+"
  }, {
    label: "Fitch-B",
    value: "Fitch-B"
  }, {
    label: "Fitch-B-",
    value: "Fitch-B-"
  }, {
    label: "Fitch-CCC",
    value: "Fitch-CCC"
  }, {
    label: "Fitch-CC",
    value: "Fitch-CC"
  }, {
    label: "Fitch-C",
    value: "Fitch-C"
  }, {
    label: "Fitch-RD",
    value: "Fitch-RD"
  }, {
    label: "Fitch-D",
    value: "Fitch-D"
  }, {
    label: "Fitch-N.A.",
    value: "Fitch-N.A."
  }, {
    label: "Fitch-NR",
    value: "Fitch-NR"
  }, {
    label: "Fitch-WD",
    value: "Fitch-WD"
  }
]
module.exports.LKUPRATINGTERM = [
  {
    label: "Short Term Underlying",
    value: "Short Term Underlying"
  }, {
    label: "Short Term Enhanced",
    value: "Short Term Enhanced"
  }, {
    label: "Long Term Underlying",
    value: "Long Term Underlying"
  }, {
    label: "Long Term Enhanced",
    value: "Long Term Enhanced"
  }
]
module.exports.LKUPREGISTRANTTYPE = [
  {
    label: "FA-MA-Municipal Advisor",
    value: "FA-MA-Municipal Advisor"
  }, {
    label: "FA-MA-Municipal AdvisorBroker Dealer",
    value: "FA-MA-Municipal AdvisorBroker Dealer"
  }, {
    label: "FA-MA-Municipal AdvisorBank Dealer",
    value: "FA-MA-Municipal AdvisorBank Dealer"
  }
]
module.exports.LKUPDEBTTYPE = [
  {
    label: "ULT G.O.",
    value: "ULT G.O."
  }, {
    label: "LT G.O.",
    value: "LT G.O."
  }, {
    label: "REVENUE",
    value: "REVENUE"
  }, {
    label: "TAX ALLOCATION",
    value: "TAX ALLOCATION"
  }, {
    label: "APPROPRIATIONS",
    value: "APPROPRIATIONS"
  }
]
module.exports.LKUPRETAILORDERPERIODRELATED = [
  {
    label: "Issuer Required Retail Order Period",
    value: "Issuer Required Retail Order Period"
  }, {
    label: "Issuer Definition of Retail on File",
    value: "Issuer Definition of Retail on File"
  }
]
module.exports.LKUPROLEWITHENTITY = [
  {
    label: "Senior Vice President",
    value: "Senior Vice President"
  }, {
    label: "Vice President",
    value: "Vice President"
  }, {
    label: "Associate Vice President",
    value: "Associate Vice President"
  }, {
    label: "Analyst",
    value: "Analyst"
  }, {
    label: "Admin",
    value: "Admin"
  }, {
    label: "Other",
    value: "Other"
  }, {
    label: "Financial Controller",
    value: "Financial Controller"
  }, {
    label: "Treasurer",
    value: "Treasurer"
  }, {
    label: "CAO",
    value: "CAO"
  }, {
    label: "COO",
    value: "COO"
  }, {
    label: "CEO",
    value: "CEO"
  }, {
    label: "Managing Director",
    value: "Managing Director"
  }, {
    label: "Executive Director",
    value: "Executive Director"
  }, {
    label: "Director",
    value: "Director"
  }, {
    label: "Employee",
    value: "Employee"
  }, {
    label: "Quant Analyst",
    value: "Quant Analyst"
  }, {
    label: "Underwriter",
    value: "Underwriter"
  }, {
    label: "Banker",
    value: "Banker"
  }, {
    label: "Lead Banker",
    value: "Lead Banker"
  }
]
module.exports.LKUPSALEPRICINGDOCS = [
  {
    label: "Tan Sheet",
    value: "Tan Sheet"
  }, {
    label: "Committee Approval",
    value: "Committee Approval"
  }, {
    label: "Desk Wires",
    value: "Desk Wires"
  }, {
    label: "Pricing book andor final #s",
    value: "Pricing book andor final #s"
  }, {
    label: "Blue Sky Memo",
    value: "Blue Sky Memo"
  }, {
    label: "Deal information sheet  (DIS)",
    value: "Deal information sheet  (DIS)"
  }
]
module.exports.LKUPSECTORS = [
  {
    label: "General Obligation -State GO -State GO",
    value: "General Obligation -State GO -State GO"
  }, {
    label: "General Obligation -Local GO -Local GO",
    value: "General Obligation -Local GO -Local GO"
  }, {
    label: "Advance Refunded -Prerefunded -Prerefunded",
    value: "Advance Refunded -Prerefunded -Prerefunded"
  }, {
    label: "Advance Refunded -Escrowed to Maturity -Escrowed to Maturity",
    value: "Advance Refunded -Escrowed to Maturity -Escrowed to Maturity"
  }, {
    label: "State Appropriated Tobacco -State Appropriated Tobacco -State Appropriated Tobac" +
        "co",
    value: "State Appropriated Tobacco -State Appropriated Tobacco -State Appropriated Tobac" +
        "co"
  }, {
    label: "Non-state Appropriated Tobacco -Non-state Appropriated Tobacco -Non-state Approp" +
        "riated Tobacco",
    value: "Non-state Appropriated Tobacco -Non-state Appropriated Tobacco -Non-state Approp" +
        "riated Tobacco"
  }, {
    label: "Education -Gen Education -LibrariesMuseums",
    value: "Education -Gen Education -LibrariesMuseums"
  }, {
    label: "Education -Gen Education -LibrariesMuseums",
    value: "Education -Gen Education -LibrariesMuseums"
  }, {
    label: "Education -Gen Education -Charter Schools",
    value: "Education -Gen Education -Charter Schools"
  }, {
    label: "Education -Gen Education -Other Gen Education",
    value: "Education -Gen Education -Other Gen Education"
  }, {
    label: "Education -Higher Education -Private Higher Education",
    value: "Education -Higher Education -Private Higher Education"
  }, {
    label: "Education -Higher Education -Public Higher Education",
    value: "Education -Higher Education -Public Higher Education"
  }, {
    label: "Education -Higher Education -Student Loans",
    value: "Education -Higher Education -Student Loans"
  }, {
    label: "Education -Higher Education -Other Higher Education",
    value: "Education -Higher Education -Other Higher Education"
  }, {
    label: "Education -Other Education-Other Education",
    value: "Education -Other Education-Other Education"
  }, {
    label: "Health -Hospitals -Hospitals",
    value: "Health -Hospitals -Hospitals"
  }, {
    label: "Health -Hospitals -Hospital Equip Loans",
    value: "Health -Hospitals -Hospital Equip Loans"
  }, {
    label: "Health -Nursing HomesRetire Care -Nursing Homes",
    value: "Health -Nursing HomesRetire Care -Nursing Homes"
  }, {
    label: "Health -Nursing HomesRetire Care -Life careRetire Centers",
    value: "Health -Nursing HomesRetire Care -Life careRetire Centers"
  }, {
    label: "Health -Nursing HomesRetire Care -Other Health",
    value: "Health -Nursing HomesRetire Care -Other Health"
  }, {
    label: "Housing -Housing -Single-Family Housing",
    value: "Housing -Housing -Single-Family Housing"
  }, {
    label: "Housing -Housing -Multi-Family Housing",
    value: "Housing -Housing -Multi-Family Housing"
  }, {
    label: "Housing -Housing -Public Housing",
    value: "Housing -Housing -Public Housing"
  }, {
    label: "Housing -Housing -Other Housing",
    value: "Housing -Housing -Other Housing"
  }, {
    label: "Industrial -Industrial Dev -Industrial Dev",
    value: "Industrial -Industrial Dev -Industrial Dev"
  }, {
    label: "Industrial -Pollution Control -Pollution Control",
    value: "Industrial -Pollution Control -Pollution Control"
  }, {
    label: "Industrial -Other Industrial -Other Industrial",
    value: "Industrial -Other Industrial -Other Industrial"
  }, {
    label: "Transportation -Airlines -Secured Airlines",
    value: "Transportation -Airlines -Secured Airlines"
  }, {
    label: "Transportation -Airlines -Unsecured Airlines",
    value: "Transportation -Airlines -Unsecured Airlines"
  }, {
    label: "Transportation -Street Infrastructure -Toll Roads",
    value: "Transportation -Street Infrastructure -Toll Roads"
  }, {
    label: "Transportation -Street Infrastructure -StreetsHighways",
    value: "Transportation -Street Infrastructure -StreetsHighways"
  }, {
    label: "Transportation -Street Infrastructure -Bridges",
    value: "Transportation -Street Infrastructure -Bridges"
  }, {
    label: "Transportation -Street Infrastructure -Tunnels",
    value: "Transportation -Street Infrastructure -Tunnels"
  }, {
    label: "Transportation -Street Infrastructure -Parking Facilities",
    value: "Transportation -Street Infrastructure -Parking Facilities"
  }, {
    label: "Transportation -Ports-Airport",
    value: "Transportation -Ports-Airport"
  }, {
    label: "Transportation -Ports-SeaportsMarine Terminals",
    value: "Transportation -Ports-SeaportsMarine Terminals"
  }, {
    label: "Transportation -Other Transportation -Other Transportation",
    value: "Transportation -Other Transportation -Other Transportation"
  }, {
    label: "Utilities -Utilities -ElectricityPublic Power",
    value: "Utilities -Utilities -ElectricityPublic Power"
  }, {
    label: "Utilities -Utilities -Gas",
    value: "Utilities -Utilities -Gas"
  }, {
    label: "Utilities -Utilities -Telephone",
    value: "Utilities -Utilities -Telephone"
  }, {
    label: "Utilities -Utilities -Other Utilities",
    value: "Utilities -Utilities -Other Utilities"
  }, {
    label: "WaterSewer -WaterSewer -WaterSewer",
    value: "WaterSewer -WaterSewer -WaterSewer"
  }, {
    label: "Misc. Revenue -Misc. Revenue -Misc. Revenue",
    value: "Misc. Revenue -Misc. Revenue -Misc. Revenue"
  }
]
module.exports.LKUPSECTYPEGENERAL = [
  {
    label: "Other",
    value: "Other"
  }, {
    label: "ULTGO",
    value: "ULTGO"
  }, {
    label: "LTGO",
    value: "LTGO"
  }, {
    label: "REV",
    value: "REV"
  }, {
    label: "TAX ALLOCATION",
    value: "TAX ALLOCATION"
  }, {
    label: "APPROPRIATIONS",
    value: "APPROPRIATIONS"
  }
]
module.exports.LKUPSTATE = [
  {
    label: "Blank",
    value: "Blank"
  }, {
    label: "AL",
    value: "AL"
  }, {
    label: "AR",
    value: "AR"
  }, {
    label: "AK",
    value: "AK"
  }, {
    label: "AZ",
    value: "AZ"
  }, {
    label: "CA",
    value: "CA"
  }, {
    label: "CO",
    value: "CO"
  }, {
    label: "CT",
    value: "CT"
  }, {
    label: "DC",
    value: "DC"
  }, {
    label: "DE",
    value: "DE"
  }, {
    label: "FL",
    value: "FL"
  }, {
    label: "GA",
    value: "GA"
  }, {
    label: "GU",
    value: "GU"
  }, {
    label: "HI",
    value: "HI"
  }, {
    label: "IA",
    value: "IA"
  }, {
    label: "ID",
    value: "ID"
  }, {
    label: "IL",
    value: "IL"
  }, {
    label: "IN",
    value: "IN"
  }, {
    label: "KS",
    value: "KS"
  }, {
    label: "KY",
    value: "KY"
  }, {
    label: "LA",
    value: "LA"
  }, {
    label: "MA",
    value: "MA"
  }, {
    label: "MD",
    value: "MD"
  }, {
    label: "ME",
    value: "ME"
  }, {
    label: "MI",
    value: "MI"
  }, {
    label: "MN",
    value: "MN"
  }, {
    label: "MO",
    value: "MO"
  }, {
    label: "MS",
    value: "MS"
  }, {
    label: "MT",
    value: "MT"
  }, {
    label: "NC",
    value: "NC"
  }, {
    label: "ND",
    value: "ND"
  }, {
    label: "NE",
    value: "NE"
  }, {
    label: "NH",
    value: "NH"
  }, {
    label: "NJ",
    value: "NJ"
  }, {
    label: "NM",
    value: "NM"
  }, {
    label: "NV",
    value: "NV"
  }, {
    label: "NY",
    value: "NY"
  }, {
    label: "OH",
    value: "OH"
  }, {
    label: "OK",
    value: "OK"
  }, {
    label: "OR",
    value: "OR"
  }, {
    label: "PA",
    value: "PA"
  }, {
    label: "PR",
    value: "PR"
  }, {
    label: "RI",
    value: "RI"
  }, {
    label: "SC",
    value: "SC"
  }, {
    label: "SD",
    value: "SD"
  }, {
    label: "TN",
    value: "TN"
  }, {
    label: "TX",
    value: "TX"
  }, {
    label: "UT",
    value: "UT"
  }, {
    label: "VA",
    value: "VA"
  }, {
    label: "VT",
    value: "VT"
  }, {
    label: "WA",
    value: "WA"
  }, {
    label: "WI",
    value: "WI"
  }, {
    label: "WV",
    value: "WV"
  }, {
    label: "WY",
    value: "WY"
  }
]
module.exports.LKUPSTATETAX = [
  {
    label: "Taxable",
    value: "Taxable"
  }, {
    label: "Tax-Exempt",
    value: "Tax-Exempt"
  }, {
    label: "Other",
    value: "Other"
  }
]
module.exports.LKUPTAXSTATUS = [
  {
    label: "Tax Exempt",
    value: "Tax Exempt"
  }, {
    label: "Taxable",
    value: "Taxable"
  }, {
    label: "Subject to AMT",
    value: "Subject to AMT"
  }, {
    label: "Standard",
    value: "Standard"
  }
]
module.exports.LKUPTRANSACTION = [
  {
    label: "Opportunity",
    value: "Opportunity"
  }, {
    label: "RFP",
    value: "RFP"
  }, {
    label: "Issue",
    value: "Issue"
  }, {
    label: "Other",
    value: "Other"
  }
]
module.exports.LKUPTRANSACTIONSTATUS = [
  {
    label: "Planned",
    value: "Planned"
  }, {
    label: "Active",
    value: "Active"
  }, {
    label: "Complete",
    value: "Complete"
  }, {
    label: "On-hold",
    value: "On-hold"
  }, {
    label: "Dropped",
    value: "Dropped"
  }, {
    label: "Other",
    value: "Other"
  }
]
module.exports.LKUPTRANSACTIONTYPE = [
  {
    label: "Financial Advisory",
    value: "Financial Advisory"
  }, {
    label: "Continuing Disclosure Compliance",
    value: "Continuing Disclosure Compliance"
  }, {
    label: "Arbitrage Rebate Compliance",
    value: "Arbitrage Rebate Compliance"
  }, {
    label: "Underwriting",
    value: "Underwriting"
  }, {
    label: "Consulting",
    value: "Consulting"
  }, {
    label: "Introductory Meeting",
    value: "Introductory Meeting"
  }, {
    label: "Market Update",
    value: "Market Update"
  }, {
    label: "Refunding Update",
    value: "Refunding Update"
  }, {
    label: "RFP",
    value: "RFP"
  }, {
    label: "Underwriting",
    value: "Underwriting"
  }, {
    label: "Others",
    value: "Others"
  }
]
module.exports.LKUPUNDERWRITER = [
  {
    label: "A4779-1st Discount Brokerage, Inc.",
    value: "A4779-1st Discount Brokerage, Inc."
  }, {
    label: "A4432-1st Global Capital Corp",
    value: "A4432-1st Global Capital Corp"
  }, {
    label: "A7454-280 Securities LLC",
    value: "A7454-280 Securities LLC"
  }, {
    label: "A4892-A.Bridge-Realvest Securities Corp.",
    value: "A4892-A.Bridge-Realvest Securities Corp."
  }, {
    label: "A6373-A.P. Securities, Inc.",
    value: "A6373-A.P. Securities, Inc."
  }, {
    label: "A4327-Abacus International Capital Corp.",
    value: "A4327-Abacus International Capital Corp."
  }, {
    label: "A4454-Abacus Investments, Inc.",
    value: "A4454-Abacus Investments, Inc."
  }, {
    label: "A7467-Abel Noser, LLC.",
    value: "A7467-Abel Noser, LLC."
  }, {
    label: "A0005-Abner, Herrman & Brock, LLC",
    value: "A0005-Abner, Herrman & Brock, LLC"
  }, {
    label: "A6224-ACAPrudent Investors Planning Corporation",
    value: "A6224-ACAPrudent Investors Planning Corporation"
  }, {
    label: "A3858-Academy Securities, Inc.",
    value: "A3858-Academy Securities, Inc."
  }, {
    label: "A5929-Accelerated Capital Group",
    value: "A5929-Accelerated Capital Group"
  }, {
    label: "A4335-Access Financial Group, Inc.",
    value: "A4335-Access Financial Group, Inc."
  }, {
    label: "A6746-ACP Securities, LLC",
    value: "A6746-ACP Securities, LLC"
  }, {
    label: "A7076-Actinver Securities, Inc.",
    value: "A7076-Actinver Securities, Inc."
  }, {
    label: "A5361-AdCap Securities, LLC.",
    value: "A5361-AdCap Securities, LLC."
  }, {
    label: "A6783-Advanced Advisor Group,LLC",
    value: "A6783-Advanced Advisor Group,LLC"
  }, {
    label: "A6855-Advantage Securities LLC",
    value: "A6855-Advantage Securities LLC"
  }, {
    label: "A5434-Advisors Asset Management, Inc.",
    value: "A5434-Advisors Asset Management, Inc."
  }, {
    label: "A5235-Advisory Group Equity Svcs Ltd",
    value: "A5235-Advisory Group Equity Svcs Ltd"
  }, {
    label: "A3490-Aegis Capital Corp.",
    value: "A3490-Aegis Capital Corp."
  }, {
    label: "A7017-Agency Trading Group, Inc.",
    value: "A7017-Agency Trading Group, Inc."
  }, {
    label: "A7015-AK Capital LLC",
    value: "A7015-AK Capital LLC"
  }, {
    label: "A3686-Alamo Capital",
    value: "A3686-Alamo Capital"
  }, {
    label: "A0538-Albert Fried & Company, LLC.",
    value: "A0538-Albert Fried & Company, LLC."
  }, {
    label: "A4524-Alerus Securities",
    value: "A4524-Alerus Securities"
  }, {
    label: "A6968-Alexander Capital, L.P.",
    value: "A6968-Alexander Capital, L.P."
  }, {
    label: "A6379-Alexander Investment Services Co.",
    value: "A6379-Alexander Investment Services Co."
  }, {
    label: "A0022-Allegheny Investments, Ltd.",
    value: "A0022-Allegheny Investments, Ltd."
  }, {
    label: "A5754-Allegiance Capital, LLC",
    value: "A5754-Allegiance Capital, LLC"
  }, {
    label: "A7382-Allegis Investment Services, LLC",
    value: "A7382-Allegis Investment Services, LLC"
  }, {
    label: "A0024-Allen & Company LLC",
    value: "A0024-Allen & Company LLC"
  }, {
    label: "A0025-Allen & Company of Florida,Inc",
    value: "A0025-Allen & Company of Florida,Inc"
  }, {
    label: "A6805-Allen, Mooney & Barnes Brokerage Srvs, LLC",
    value: "A6805-Allen, Mooney & Barnes Brokerage Srvs, LLC"
  }, {
    label: "A3906-Alliance Financial Group, Inc.",
    value: "A3906-Alliance Financial Group, Inc."
  }, {
    label: "A5694-AllianceBernstein Investments, Inc.",
    value: "A5694-AllianceBernstein Investments, Inc."
  }, {
    label: "A0166-Alliant Securities Inc.",
    value: "A0166-Alliant Securities Inc."
  }, {
    label: "A5968-Allianz Global Investors Distributors LLC",
    value: "A5968-Allianz Global Investors Distributors LLC"
  }, {
    label: "A7258-Allied Millennial Partners, LLC",
    value: "A7258-Allied Millennial Partners, LLC"
  }, {
    label: "A5706-Allstate Financial Svcs., LLC",
    value: "A5706-Allstate Financial Svcs., LLC"
  }, {
    label: "A6857-Alluvion Securities, LLC",
    value: "A6857-Alluvion Securities, LLC"
  }, {
    label: "A6833-Ambassador Financial Group, Inc.",
    value: "A6833-Ambassador Financial Group, Inc."
  }, {
    label: "A6006-American Capital Partners, LLC",
    value: "A6006-American Capital Partners, LLC"
  }, {
    label: "A6158-American Century Investment Services, Inc.",
    value: "A6158-American Century Investment Services, Inc."
  }, {
    label: "A3666-AMERICAN ENTERPRISE INVESTMENT SERVICES INC.",
    value: "A3666-AMERICAN ENTERPRISE INVESTMENT SERVICES INC."
  }, {
    label: "A7079-American Equity Investment Corporation",
    value: "A7079-American Equity Investment Corporation"
  }, {
    label: "A6233-American Financial Associates, Inc.",
    value: "A6233-American Financial Associates, Inc."
  }, {
    label: "A5979-American Fund Distributors, Inc.",
    value: "A5979-American Fund Distributors, Inc."
  }, {
    label: "A4167-American Heritage Secs., Inc.",
    value: "A4167-American Heritage Secs., Inc."
  }, {
    label: "A6672-American Independent Securities Grp,LLC",
    value: "A6672-American Independent Securities Grp,LLC"
  }, {
    label: "A0036-American Investors Company",
    value: "A0036-American Investors Company"
  }, {
    label: "A4944-American Portfolios Financial Services, Inc.",
    value: "A4944-American Portfolios Financial Services, Inc."
  }, {
    label: "A6446-American Trust Investment Services",
    value: "A6446-American Trust Investment Services"
  }, {
    label: "A3901-American Wealth Management Inc",
    value: "A3901-American Wealth Management Inc"
  }, {
    label: "A2663-Ameriprise Financial Services, Inc.",
    value: "A2663-Ameriprise Financial Services, Inc."
  }, {
    label: "A2110-Ameritas Investment Corp.",
    value: "A2110-Ameritas Investment Corp."
  }, {
    label: "A7312-Amherst Pierpont Securities LLC",
    value: "A7312-Amherst Pierpont Securities LLC"
  }, {
    label: "A0039-Amuni Financial Inc.",
    value: "A0039-Amuni Financial Inc."
  }, {
    label: "A6521-Anchor Bay Securities, LLC",
    value: "A6521-Anchor Bay Securities, LLC"
  }, {
    label: "A6670-Andes Capital Group, LLC",
    value: "A6670-Andes Capital Group, LLC"
  }, {
    label: "A4131-Andrew Garrett Inc.",
    value: "A4131-Andrew Garrett Inc."
  }, {
    label: "A6646-Anovest Financial Services, Inc.",
    value: "A6646-Anovest Financial Services, Inc."
  }, {
    label: "A6138-Aon Securities Inc.",
    value: "A6138-Aon Securities Inc."
  }, {
    label: "A6577-AOS, Inc.",
    value: "A6577-AOS, Inc."
  }, {
    label: "A7272-Apex Clearing Corporation",
    value: "A7272-Apex Clearing Corporation"
  }, {
    label: "A7449-Apexus Capital LLC",
    value: "A7449-Apexus Capital LLC"
  }, {
    label: "A7182-Aprio STRATEGIC PARTNERS, LLC",
    value: "A7182-Aprio STRATEGIC PARTNERS, LLC"
  }, {
    label: "A7075-Arbor Research & Trading, LLC",
    value: "A7075-Arbor Research & Trading, LLC"
  }, {
    label: "A7381-ARCADIA SECURITIES LLC",
    value: "A7381-ARCADIA SECURITIES LLC"
  }, {
    label: "A6910-Arete Wealth Management, LLC",
    value: "A6910-Arete Wealth Management, LLC"
  }, {
    label: "A1178-Arive Capital Markets LLC",
    value: "A1178-Arive Capital Markets LLC"
  }, {
    label: "A7413-ARK Global LLC",
    value: "A7413-ARK Global LLC"
  }, {
    label: "A7471-Arkadios Capital",
    value: "A7471-Arkadios Capital"
  }, {
    label: "A5294-Arlington Securities Inc.",
    value: "A5294-Arlington Securities Inc."
  }, {
    label: "A6770-Arque Capital, Ltd.",
    value: "A6770-Arque Capital, Ltd."
  }, {
    label: "A0758-Arthur W Wood Company",
    value: "A0758-Arthur W Wood Company"
  }, {
    label: "A5030-Arvest Wealth Management",
    value: "A5030-Arvest Wealth Management"
  }, {
    label: "A5709-Ascensus Broker Dealer Services, Inc.",
    value: "A5709-Ascensus Broker Dealer Services, Inc."
  }, {
    label: "A4307-Asia Pacific Financial Management Group, Inc.",
    value: "A4307-Asia Pacific Financial Management Group, Inc."
  }, {
    label: "A0277-Associated Investment Services",
    value: "A0277-Associated Investment Services"
  }, {
    label: "A6984-ATIS, Inc.",
    value: "A6984-ATIS, Inc."
  }, {
    label: "A7231-Auriga USA, LLC",
    value: "A7231-Auriga USA, LLC"
  }, {
    label: "A5380-Aurora Capital LLC",
    value: "A5380-Aurora Capital LLC"
  }, {
    label: "A7175-Aurora Securities",
    value: "A7175-Aurora Securities"
  }, {
    label: "A0069-Ausdal Financial Partners, Inc.",
    value: "A0069-Ausdal Financial Partners, Inc."
  }, {
    label: "A6215-Avalon Investment & Securities Group, Inc.",
    value: "A6215-Avalon Investment & Securities Group, Inc."
  }, {
    label: "A7184-Avatar Capital Group LLC",
    value: "A7184-Avatar Capital Group LLC"
  }, {
    label: "A6808-Aventura Securities, LLC",
    value: "A6808-Aventura Securities, LLC"
  }, {
    label: "A6362-Avisen Securities, Inc.",
    value: "A6362-Avisen Securities, Inc."
  }, {
    label: "A6194-AVM, L.P.",
    value: "A6194-AVM, L.P."
  }, {
    label: "A6722-Avondale Partners, LLC",
    value: "A6722-Avondale Partners, LLC"
  }, {
    label: "A2690-AXA Advisors LLC.",
    value: "A2690-AXA Advisors LLC."
  }, {
    label: "A3696-Axiom Capital Management, Inc.",
    value: "A3696-Axiom Capital Management, Inc."
  }, {
    label: "A5498-B. B. Graham & Co., Inc.",
    value: "A5498-B. B. Graham & Co., Inc."
  }, {
    label: "A7423-B. Riley & Co., LLC",
    value: "A7423-B. Riley & Co., LLC"
  }, {
    label: "A6298-Backstrom McCarley Berry & Co., LLC",
    value: "A6298-Backstrom McCarley Berry & Co., LLC"
  }, {
    label: "A0082-Baker & Co., Incorporated",
    value: "A0082-Baker & Co., Incorporated"
  }, {
    label: "A0081-Baker Group LP",
    value: "A0081-Baker Group LP"
  }, {
    label: "A6069-Baker Tilly Capital, LLC",
    value: "A6069-Baker Tilly Capital, LLC"
  }, {
    label: "A4301-Ballew Investments, Inc.",
    value: "A4301-Ballew Investments, Inc."
  }, {
    label: "A5394-BancWest Investment Svcs. Inc.",
    value: "A5394-BancWest Investment Svcs. Inc."
  }, {
    label: "A7322-Bank Fund Equities, Inc.",
    value: "A7322-Bank Fund Equities, Inc."
  }, {
    label: "B0019-Bank of America, NA  MSD",
    value: "B0019-Bank of America, NA  MSD"
  }, {
    label: "B0027-Bank of Oklahoma, N.A.",
    value: "B0027-Bank of Oklahoma, N.A."
  }, {
    label: "B0467-Bank of the West",
    value: "B0467-Bank of the West"
  }, {
    label: "A7412-Bankers Life Securities Inc.",
    value: "A7412-Bankers Life Securities Inc."
  }, {
    label: "B0368-Bankers Bank Investment Dept.",
    value: "B0368-Bankers Bank Investment Dept."
  }, {
    label: "A3988-Bankoh Investment Services, Inc.",
    value: "A3988-Bankoh Investment Services, Inc."
  }, {
    label: "A6604-Banorte-Ixe Securities International Ltd.",
    value: "A6604-Banorte-Ixe Securities International Ltd."
  }, {
    label: "A7109-Barclay Investments, LLC",
    value: "A7109-Barclay Investments, LLC"
  }, {
    label: "A6566-Barclays Capital Inc.",
    value: "A6566-Barclays Capital Inc."
  }, {
    label: "A0097-Barrett & Company",
    value: "A0097-Barrett & Company"
  }, {
    label: "A6121-Bates Securities, Inc.",
    value: "A6121-Bates Securities, Inc."
  }, {
    label: "A7462-Bay Crest Partners LLC",
    value: "A7462-Bay Crest Partners LLC"
  }, {
    label: "A4350-BB & T Investment Services,Inc",
    value: "A4350-BB & T Investment Services,Inc"
  }, {
    label: "A6823-BB&T Securities, LLC",
    value: "A6823-BB&T Securities, LLC"
  }, {
    label: "A7035-BBVA Securities Inc.",
    value: "A7035-BBVA Securities Inc."
  }, {
    label: "A0775-BC Ziegler and Company",
    value: "A0775-BC Ziegler and Company"
  }, {
    label: "A0085-BCG Securities, Inc.",
    value: "A0085-BCG Securities, Inc."
  }, {
    label: "A1809-Beaconsfield Financial Services, Inc.",
    value: "A1809-Beaconsfield Financial Services, Inc."
  }, {
    label: "A6759-Bear Creek Securities, LLC",
    value: "A6759-Bear Creek Securities, LLC"
  }, {
    label: "A1572-BedRok Securities LLC",
    value: "A1572-BedRok Securities LLC"
  }, {
    label: "A3445-Beech Hill Securities",
    value: "A3445-Beech Hill Securities"
  }, {
    label: "A4477-Beekman Securities, Inc.",
    value: "A4477-Beekman Securities, Inc."
  }, {
    label: "A6669-Bel Air Securities LLC",
    value: "A6669-Bel Air Securities LLC"
  }, {
    label: "A3977-Belle Haven Investments, LP",
    value: "A3977-Belle Haven Investments, LP"
  }, {
    label: "A6574-Benchmark Investments, Inc.",
    value: "A6574-Benchmark Investments, Inc."
  }, {
    label: "A7242-Benchmark Securities LLC",
    value: "A7242-Benchmark Securities LLC"
  }, {
    label: "A6963-Benjamin F. Edwards & Company",
    value: "A6963-Benjamin F. Edwards & Company"
  }, {
    label: "A0118-Benjamin Securities, Inc.",
    value: "A0118-Benjamin Securities, Inc."
  }, {
    label: "A0647-Bernard Herold & Co., Inc.",
    value: "A0647-Bernard Herold & Co., Inc."
  }, {
    label: "A2043-Bernardi Securities, Inc.",
    value: "A2043-Bernardi Securities, Inc."
  }, {
    label: "A1765-Berthel Fisher & Co.Fin.Serv.",
    value: "A1765-Berthel Fisher & Co.Fin.Serv."
  }, {
    label: "A4867-BestVest Investments, Ltd.",
    value: "A4867-BestVest Investments, Ltd."
  }, {
    label: "A6568-Beta Capital Securities LLC",
    value: "A6568-Beta Capital Securities LLC"
  }, {
    label: "A1784-Bettinger & Leech Financial Co",
    value: "A1784-Bettinger & Leech Financial Co"
  }, {
    label: "A6253-BFT Financial Group, LLC",
    value: "A6253-BFT Financial Group, LLC"
  }, {
    label: "A7021-BGC Financial, LP",
    value: "A7021-BGC Financial, LP"
  }, {
    label: "A6680-BHK Securities, LLC",
    value: "A6680-BHK Securities, LLC"
  }, {
    label: "A4991-Bill Few Securities, Inc.",
    value: "A4991-Bill Few Securities, Inc."
  }, {
    label: "A7422-Biltmore International Corp.",
    value: "A7422-Biltmore International Corp."
  }, {
    label: "A7038-BlackRock Investments, LLC",
    value: "A7038-BlackRock Investments, LLC"
  }, {
    label: "A3616-Blakeslee & Blakeslee Inc.",
    value: "A3616-Blakeslee & Blakeslee Inc."
  }, {
    label: "A6906-Blaylock Van, LLC",
    value: "A6906-Blaylock Van, LLC"
  }, {
    label: "A4229-Bley Investment Group, Inc.",
    value: "A4229-Bley Investment Group, Inc."
  }, {
    label: "A7103-BMA Securities",
    value: "A7103-BMA Securities"
  }, {
    label: "A6696-BMO Harris Financial Advisors, Inc.",
    value: "A6696-BMO Harris Financial Advisors, Inc."
  }, {
    label: "A7320-BNP PARIBAS PRIME BROKERAGE, INC.",
    value: "A7320-BNP PARIBAS PRIME BROKERAGE, INC."
  }, {
    label: "A6172-BNP Paribas Securities Corp.",
    value: "A6172-BNP Paribas Securities Corp."
  }, {
    label: "A4685-BNY Mellon Capital Markets, LLC",
    value: "A4685-BNY Mellon Capital Markets, LLC"
  }, {
    label: "A0155-Bodell Overcash Anderson & Co.",
    value: "A0155-Bodell Overcash Anderson & Co."
  }, {
    label: "A0156-Boenning & Scattergood, Inc.",
    value: "A0156-Boenning & Scattergood, Inc."
  }, {
    label: "A4700-BOK Financial Securities, Inc.",
    value: "A4700-BOK Financial Securities, Inc."
  }, {
    label: "A4603-Bolton Global Capital",
    value: "A4603-Bolton Global Capital"
  }, {
    label: "A7013-Bolton Securities",
    value: "A7013-Bolton Securities"
  }, {
    label: "A6749-Bostonia Global Securities LLC",
    value: "A6749-Bostonia Global Securities LLC"
  }, {
    label: "A7388-Boustead Securities, LLC",
    value: "A7388-Boustead Securities, LLC"
  }, {
    label: "A2288-BPU Investment Management, Inc.",
    value: "A2288-BPU Investment Management, Inc."
  }, {
    label: "A6198-Brandis Tallman LLC",
    value: "A6198-Brandis Tallman LLC"
  }, {
    label: "A6398-Brandt, Kelly & Simmons Securities, LLC",
    value: "A6398-Brandt, Kelly & Simmons Securities, LLC"
  }, {
    label: "A6430-Brazos Securities, Inc.",
    value: "A6430-Brazos Securities, Inc."
  }, {
    label: "A3814-Brean Capital, LLC.",
    value: "A3814-Brean Capital, LLC."
  }, {
    label: "A0174-Brighton Securities Corp.",
    value: "A0174-Brighton Securities Corp."
  }, {
    label: "A5343-Brinker Cap. Securities, Inc.",
    value: "A5343-Brinker Cap. Securities, Inc."
  }, {
    label: "A6731-Bristol Financial Services, Inc.",
    value: "A6731-Bristol Financial Services, Inc."
  }, {
    label: "A5618-Broad Street Capital Markets LLC",
    value: "A5618-Broad Street Capital Markets LLC"
  }, {
    label: "A3998-Broker Dealer Fin. Serv. Corp.",
    value: "A3998-Broker Dealer Fin. Serv. Corp."
  }, {
    label: "A6990-BrokerageSelect",
    value: "A6990-BrokerageSelect"
  }, {
    label: "A6776-Brokers International Financial Services, LLC",
    value: "A6776-Brokers International Financial Services, LLC"
  }, {
    label: "A7395-Bronfman E.L. Rothschild Capital, LLC",
    value: "A7395-Bronfman E.L. Rothschild Capital, LLC"
  }, {
    label: "A2362-Brooklight Place Securities, Inc.",
    value: "A2362-Brooklight Place Securities, Inc."
  }, {
    label: "A2125-Brown & Brown Securities, Inc.",
    value: "A2125-Brown & Brown Securities, Inc."
  }, {
    label: "A6164-Brown Advisory Securities, LLC",
    value: "A6164-Brown Advisory Securities, LLC"
  }, {
    label: "A0189-Brown Associates, Inc.",
    value: "A0189-Brown Associates, Inc."
  }, {
    label: "A0196-Brown, LisleCummings, Inc.",
    value: "A0196-Brown, LisleCummings, Inc."
  }, {
    label: "A7236-Brownstone Investment Group, LLC",
    value: "A7236-Brownstone Investment Group, LLC"
  }, {
    label: "A5860-Bruce A. Lefavi Securities, Inc.",
    value: "A5860-Bruce A. Lefavi Securities, Inc."
  }, {
    label: "A7393-Bruderman Brothers, LLC",
    value: "A7393-Bruderman Brothers, LLC"
  }, {
    label: "B0510-Bryant Bank Capital Markets Division",
    value: "B0510-Bryant Bank Capital Markets Division"
  }, {
    label: "A3317-Buckman, Buckman & Reid, Inc.",
    value: "A3317-Buckman, Buckman & Reid, Inc."
  }, {
    label: "A6410-Buckram Securities Ltd.",
    value: "A6410-Buckram Securities Ltd."
  }, {
    label: "A0198-Buell Securities Corp.",
    value: "A0198-Buell Securities Corp."
  }, {
    label: "A6336-Bull & Bear Brokerage Services, Inc.",
    value: "A6336-Bull & Bear Brokerage Services, Inc."
  }, {
    label: "A0205-Burke, Lawton,Brewer & Burke LLC",
    value: "A0205-Burke, Lawton,Brewer & Burke LLC"
  }, {
    label: "A0212-Butler Muni LLC",
    value: "A0212-Butler Muni LLC"
  }, {
    label: "A3743-Buttonwood Partners, Inc.",
    value: "A3743-Buttonwood Partners, Inc."
  }, {
    label: "A0165-CA Botzum & Co.",
    value: "A0165-CA Botzum & Co."
  }, {
    label: "A7244-Cabot Lodge Securities LLC",
    value: "A7244-Cabot Lodge Securities LLC"
  }, {
    label: "A4470-Cabrera Capital Markets, LLC",
    value: "A4470-Cabrera Capital Markets, LLC"
  }, {
    label: "A1558-Cadaret, Grant & Co., Inc.",
    value: "A1558-Cadaret, Grant & Co., Inc."
  }, {
    label: "A1793-Cain Brothers & Co.",
    value: "A1793-Cain Brothers & Co."
  }, {
    label: "A7050-Caldwell Sutter Capital, Inc.",
    value: "A7050-Caldwell Sutter Capital, Inc."
  }, {
    label: "A6077-Callaway Financial Services, Inc.",
    value: "A6077-Callaway Financial Services, Inc."
  }, {
    label: "A2912-Calton & Associates, Inc.",
    value: "A2912-Calton & Associates, Inc."
  }, {
    label: "A6947-Cambria Capital",
    value: "A6947-Cambria Capital"
  }, {
    label: "A4785-Cambridge Investment Research",
    value: "A4785-Cambridge Investment Research"
  }, {
    label: "A7262-Camden Financial Services",
    value: "A7262-Camden Financial Services"
  }, {
    label: "A7287-Canaccord Genuity Inc.",
    value: "A7287-Canaccord Genuity Inc."
  }, {
    label: "A0230-Cantella & Co., Inc.",
    value: "A0230-Cantella & Co., Inc."
  }, {
    label: "A3664-Cantone Research, Inc.",
    value: "A3664-Cantone Research, Inc."
  }, {
    label: "A6429-Cantor Fitzgerald & Co.",
    value: "A6429-Cantor Fitzgerald & Co."
  }, {
    label: "A0232-Cape Securities, Inc.",
    value: "A0232-Cape Securities, Inc."
  }, {
    label: "A6439-CapFinancial Securities, LLC",
    value: "A6439-CapFinancial Securities, LLC"
  }, {
    label: "A6913-Capital City Securities, LLC",
    value: "A6913-Capital City Securities, LLC"
  }, {
    label: "A4639-Capital Financial Services,Inc",
    value: "A4639-Capital Financial Services,Inc"
  }, {
    label: "A0236-Capital Institutional Services, Inc.",
    value: "A0236-Capital Institutional Services, Inc."
  }, {
    label: "A5064-Capital Investment Brokerage",
    value: "A5064-Capital Investment Brokerage"
  }, {
    label: "A3355-Capital Investment Group, Inc.",
    value: "A3355-Capital Investment Group, Inc."
  }, {
    label: "A0231-Capital Management Securities,",
    value: "A0231-Capital Management Securities,"
  }, {
    label: "A3581-Capital One Investing, LLC",
    value: "A3581-Capital One Investing, LLC"
  }, {
    label: "A4008-Capital Portfolio Management,",
    value: "A4008-Capital Portfolio Management,"
  }, {
    label: "A7189-Capital Research Brokerage Services, LLC",
    value: "A7189-Capital Research Brokerage Services, LLC"
  }, {
    label: "A6979-Capital Synergy Partners, Inc.",
    value: "A6979-Capital Synergy Partners, Inc."
  }, {
    label: "A2210-Capitol Securities Management, Inc.",
    value: "A2210-Capitol Securities Management, Inc."
  }, {
    label: "A6844-CapWealth Investment Services",
    value: "A6844-CapWealth Investment Services"
  }, {
    label: "A4529-Cardinal Investments, Inc.",
    value: "A4529-Cardinal Investments, Inc."
  }, {
    label: "A0643-Carl M Hennig, Inc.",
    value: "A0643-Carl M Hennig, Inc."
  }, {
    label: "A7407-Carnes Capital Corporation",
    value: "A7407-Carnes Capital Corporation"
  }, {
    label: "A5718-Carolinas Investment Consulting LLC",
    value: "A5718-Carolinas Investment Consulting LLC"
  }, {
    label: "A3116-Carter, Terry & Company, Inc.",
    value: "A3116-Carter, Terry & Company, Inc."
  }, {
    label: "A0251-Carty and Company, Inc.",
    value: "A0251-Carty and Company, Inc."
  }, {
    label: "A6822-Cary Street Partners LLC",
    value: "A6822-Cary Street Partners LLC"
  }, {
    label: "A6004-Cascade Financial Management, Inc.",
    value: "A6004-Cascade Financial Management, Inc."
  }, {
    label: "A4434-Cascade Investment Group, Inc.",
    value: "A4434-Cascade Investment Group, Inc."
  }, {
    label: "A6897-CastleOak Securities, LP",
    value: "A6897-CastleOak Securities, LP"
  }, {
    label: "A7121-CAVU Securities, LLC.",
    value: "A7121-CAVU Securities, LLC."
  }, {
    label: "A2206-CBIZ Financial Solutions, Inc.",
    value: "A2206-CBIZ Financial Solutions, Inc."
  }, {
    label: "A6724-CCF Investments, Inc.",
    value: "A6724-CCF Investments, Inc."
  }, {
    label: "A4831-Celadon Financial Group LLC",
    value: "A4831-Celadon Financial Group LLC"
  }, {
    label: "A5276-Centaurus Financial, Inc.",
    value: "A5276-Centaurus Financial, Inc."
  }, {
    label: "A0255-Centennial Securities Co., Inc",
    value: "A0255-Centennial Securities Co., Inc"
  }, {
    label: "A6942-Center Street Securities, Inc",
    value: "A6942-Center Street Securities, Inc"
  }, {
    label: "B0470-CenterState Bank of Florida",
    value: "B0470-CenterState Bank of Florida"
  }, {
    label: "A7140-Central States Capital Markets, LLC",
    value: "A7140-Central States Capital Markets, LLC"
  }, {
    label: "B0429-Central Trust Bank",
    value: "B0429-Central Trust Bank"
  }, {
    label: "A3914-Century Securities Associates",
    value: "A3914-Century Securities Associates"
  }, {
    label: "A7019-Ceros Financial Services, Inc.",
    value: "A7019-Ceros Financial Services, Inc."
  }, {
    label: "A2021-Cetera Advisor LLC",
    value: "A2021-Cetera Advisor LLC"
  }, {
    label: "A1673-Cetera Advisor Networks LLC",
    value: "A1673-Cetera Advisor Networks LLC"
  }, {
    label: "A1898-Cetera Financial Specialists LLC",
    value: "A1898-Cetera Financial Specialists LLC"
  }, {
    label: "A2132-Cetera Investment Services LLC",
    value: "A2132-Cetera Investment Services LLC"
  }, {
    label: "A7461-CF Secured, LLC",
    value: "A7461-CF Secured, LLC"
  }, {
    label: "A5772-CFD Investments, Inc.",
    value: "A5772-CFD Investments, Inc."
  }, {
    label: "A6824-CFG Capital Markets, LLC",
    value: "A6824-CFG Capital Markets, LLC"
  }, {
    label: "A5729-CFS Securities, Inc.",
    value: "A5729-CFS Securities, Inc."
  }, {
    label: "A5719-CFT Securities, LLC",
    value: "A5719-CFT Securities, LLC"
  }, {
    label: "A5234-CG Capital Markets, LLC",
    value: "A5234-CG Capital Markets, LLC"
  }, {
    label: "A7387-CGA Securities LLC",
    value: "A7387-CGA Securities LLC"
  }, {
    label: "A7166-CGIS SECURITIES LLC",
    value: "A7166-CGIS SECURITIES LLC"
  }, {
    label: "A3840-Chapin Davis, INC",
    value: "A3840-Chapin Davis, INC"
  }, {
    label: "A1271-Charles Schwab & Co., Inc.",
    value: "A1271-Charles Schwab & Co., Inc."
  }, {
    label: "A6106-Chelsea Financial Services",
    value: "A6106-Chelsea Financial Services"
  }, {
    label: "A0625-Chester Harris and Co., Inc.",
    value: "A0625-Chester Harris and Co., Inc."
  }, {
    label: "A6485-Chickasaw Securities, LLC",
    value: "A6485-Chickasaw Securities, LLC"
  }, {
    label: "A6760-Chrysalis Capital Group, LLC",
    value: "A6760-Chrysalis Capital Group, LLC"
  }, {
    label: "A6564-CIG Securities, Inc.",
    value: "A6564-CIG Securities, Inc."
  }, {
    label: "B0504-Citibank, N.A., Municipal Securities Division",
    value: "B0504-Citibank, N.A., Municipal Securities Division"
  }, {
    label: "A1487-Citigroup Global Markets Inc.",
    value: "A1487-Citigroup Global Markets Inc."
  }, {
    label: "A4803-Citizens Securities, Inc.",
    value: "A4803-Citizens Securities, Inc."
  }, {
    label: "A5564-City National Securities, Inc.",
    value: "A5564-City National Securities, Inc."
  }, {
    label: "A0836-CL King and Associates, Inc.",
    value: "A0836-CL King and Associates, Inc."
  }, {
    label: "A7259-Classic, LLC",
    value: "A7259-Classic, LLC"
  }, {
    label: "A7398-Clearview Trading Advisors, Inc.",
    value: "A7398-Clearview Trading Advisors, Inc."
  }, {
    label: "A7099-Client One Securities, LLC",
    value: "A7099-Client One Securities, LLC"
  }, {
    label: "A6282-CliftonLarsonAllen Wealth Advisors, LLC",
    value: "A6282-CliftonLarsonAllen Wealth Advisors, LLC"
  }, {
    label: "A6543-CNS Securities, LLC",
    value: "A6543-CNS Securities, LLC"
  }, {
    label: "A3303-Coastal Equities, Inc.",
    value: "A3303-Coastal Equities, Inc."
  }, {
    label: "A0286-Coburn & Meredith, Inc.",
    value: "A0286-Coburn & Meredith, Inc."
  }, {
    label: "A3989-Coker & Palmer, Inc.",
    value: "A3989-Coker & Palmer, Inc."
  }, {
    label: "A7432-Coldstream Securities Inc.",
    value: "A7432-Coldstream Securities Inc."
  }, {
    label: "A0296-Colonial Securities, Inc.",
    value: "A0296-Colonial Securities, Inc."
  }, {
    label: "A6750-Colorado Financial Service Corp.",
    value: "A6750-Colorado Financial Service Corp."
  }, {
    label: "A7084-Columbia Management Investment Distributors, Inc.",
    value: "A7084-Columbia Management Investment Distributors, Inc."
  }, {
    label: "A7071-Columbus Advisory Group, Ltd.",
    value: "A7071-Columbus Advisory Group, Ltd."
  }, {
    label: "A2845-Comerica Securities, Inc.",
    value: "A2845-Comerica Securities, Inc."
  }, {
    label: "B0069-Commerce Bank",
    value: "B0069-Commerce Bank"
  }, {
    label: "A4039-Commerce Brokerage Services, Inc",
    value: "A4039-Commerce Brokerage Services, Inc"
  }, {
    label: "A6856-Commerce Street Capital, LLC",
    value: "A6856-Commerce Street Capital, LLC"
  }, {
    label: "A6197-Commonwealth Financial Group, Inc.",
    value: "A6197-Commonwealth Financial Group, Inc."
  }, {
    label: "A3246-Commonwealth Financial Network",
    value: "A3246-Commonwealth Financial Network"
  }, {
    label: "K0880-Commonwealth National Bank",
    value: "K0880-Commonwealth National Bank"
  }, {
    label: "A7346-CommunityAmerica Financial Solutions, LLC",
    value: "A7346-CommunityAmerica Financial Solutions, LLC"
  }, {
    label: "A5503-Comprehensive Asset Management and Servicing, Inc.",
    value: "A5503-Comprehensive Asset Management and Servicing, Inc."
  }, {
    label: "A7202-Concorde Investment Services LLC",
    value: "A7202-Concorde Investment Services LLC"
  }, {
    label: "A0304-Conners & Company, Inc.",
    value: "A0304-Conners & Company, Inc."
  }, {
    label: "A4062-Conover Securities Corporation",
    value: "A4062-Conover Securities Corporation"
  }, {
    label: "A3188-Consolidated Financial Investments, Inc.",
    value: "A3188-Consolidated Financial Investments, Inc."
  }, {
    label: "A4047-Continental Investor Services",
    value: "A4047-Continental Investor Services"
  }, {
    label: "A3434-Cooper Malone McClain, Inc.",
    value: "A3434-Cooper Malone McClain, Inc."
  }, {
    label: "A3020-Coordinated Capital Securities",
    value: "A3020-Coordinated Capital Securities"
  }, {
    label: "A5948-COR Clearing, LLC",
    value: "A5948-COR Clearing, LLC"
  }, {
    label: "A5325-CoreCap Investments, Inc.",
    value: "A5325-CoreCap Investments, Inc."
  }, {
    label: "A4916-Corinthian Partners, L.L.C.",
    value: "A4916-Corinthian Partners, L.L.C."
  }, {
    label: "A5688-Cornerstone Capital Corporation",
    value: "A5688-Cornerstone Capital Corporation"
  }, {
    label: "A5964-Correll Co. Investment Services Corp.",
    value: "A5964-Correll Co. Investment Services Corp."
  }, {
    label: "A0321-CosseInternational Securities",
    value: "A0321-CosseInternational Securities"
  }, {
    label: "A0323-Coughlin & Company Inc.",
    value: "A0323-Coughlin & Company Inc."
  }, {
    label: "A6755-COUNTRY Capital Manangement Company",
    value: "A6755-COUNTRY Capital Manangement Company"
  }, {
    label: "B0350-Country Club Bank",
    value: "B0350-Country Club Bank"
  }, {
    label: "A5878-Country Club Financial Services, Inc.",
    value: "A5878-Country Club Financial Services, Inc."
  }, {
    label: "A7478-Cowen and Company, LLC",
    value: "A7478-Cowen and Company, LLC"
  }, {
    label: "A5255-Cowen Execution Services LLC",
    value: "A5255-Cowen Execution Services LLC"
  }, {
    label: "A7100-Cowen Prime Services LLC",
    value: "A7100-Cowen Prime Services LLC"
  }, {
    label: "A7408-Craft Capital Management LLC",
    value: "A7408-Craft Capital Management LLC"
  }, {
    label: "A3238-Credit Agricole Securities (USA) Inc.",
    value: "A3238-Credit Agricole Securities (USA) Inc."
  }, {
    label: "A0481-Credit Suisse Securities (USA) LLC",
    value: "A0481-Credit Suisse Securities (USA) LLC"
  }, {
    label: "A7480-Creditex Securities Corporation",
    value: "A7480-Creditex Securities Corporation"
  }, {
    label: "A3600-Cresap, Inc.",
    value: "A3600-Cresap, Inc."
  }, {
    label: "A6530-Crescent Securities Group, Inc.",
    value: "A6530-Crescent Securities Group, Inc."
  }, {
    label: "A5625-Crestone Securities LLC",
    value: "A5625-Crestone Securities LLC"
  }, {
    label: "A0333-Crews & Associates, Inc.",
    value: "A0333-Crews & Associates, Inc."
  }, {
    label: "A3589-CRI Securities, LLC",
    value: "A3589-CRI Securities, LLC"
  }, {
    label: "A7026-Cross Point Capital LLC",
    value: "A7026-Cross Point Capital LLC"
  }, {
    label: "A5423-Crown Capital Securities, L.P.",
    value: "A5423-Crown Capital Securities, L.P."
  }, {
    label: "A6793-CSSC Brokerage Services, Inc.",
    value: "A6793-CSSC Brokerage Services, Inc."
  }, {
    label: "A7317-CU Investment Solutions LLC",
    value: "A7317-CU Investment Solutions LLC"
  }, {
    label: "A2954-Cullen Investment Group, Ltd.",
    value: "A2954-Cullen Investment Group, Ltd."
  }, {
    label: "A4634-CUNA Brokerage Services, Inc.",
    value: "A4634-CUNA Brokerage Services, Inc."
  }, {
    label: "A7112-Curran Advisory Services",
    value: "A7112-Curran Advisory Services"
  }, {
    label: "A4995-CUSO Financial Services, LP",
    value: "A4995-CUSO Financial Services, LP"
  }, {
    label: "A3216-Cutter and Company Brokerage,Inc",
    value: "A3216-Cutter and Company Brokerage,Inc"
  }, {
    label: "A7145-CV Brokerage Inc",
    value: "A7145-CV Brokerage Inc"
  }, {
    label: "A6304-CW Securities, LLC",
    value: "A6304-CW Securities, LLC"
  }, {
    label: "A6051-D.B. McKenna & Co., Inc.",
    value: "A6051-D.B. McKenna & Co., Inc."
  }, {
    label: "A6768-D.H. Hill Securities, LLLP",
    value: "A6768-D.H. Hill Securities, LLLP"
  }, {
    label: "A0365-DA Davidson & Company",
    value: "A0365-DA Davidson & Company"
  }, {
    label: "A0359-Dart, Papesh & Company, Inc.",
    value: "A0359-Dart, Papesh & Company, Inc."
  }, {
    label: "A0360-Darwood Associates, Inc.",
    value: "A0360-Darwood Associates, Inc."
  }, {
    label: "A0364-Davenport & Company LLC",
    value: "A0364-Davenport & Company LLC"
  }, {
    label: "A1061-David A Noyes & Company",
    value: "A1061-David A Noyes & Company"
  }, {
    label: "A5604-David Harris & Co., Inc.",
    value: "A5604-David Harris & Co., Inc."
  }, {
    label: "A0887-David Lerner Associates, Inc",
    value: "A0887-David Lerner Associates, Inc"
  }, {
    label: "A6113-DaVinci Capital Management",
    value: "A6113-DaVinci Capital Management"
  }, {
    label: "A6982-Davis Securities LLC",
    value: "A6982-Davis Securities LLC"
  }, {
    label: "A6548-Dawson James Securities, Inc.",
    value: "A6548-Dawson James Securities, Inc."
  }, {
    label: "A7211-Dealerweb Inc.",
    value: "A7211-Dealerweb Inc."
  }, {
    label: "A6812-Dempsey Lord Smith, LLC",
    value: "A6812-Dempsey Lord Smith, LLC"
  }, {
    label: "A4800-DesPain Financial Corporation",
    value: "A4800-DesPain Financial Corporation"
  }, {
    label: "A6273-Destiny Capital Securities Corporation",
    value: "A6273-Destiny Capital Securities Corporation"
  }, {
    label: "A7234-Destra Capital Investments LLC",
    value: "A7234-Destra Capital Investments LLC"
  }, {
    label: "A5559-Detalus Securities LLC",
    value: "A5559-Detalus Securities LLC"
  }, {
    label: "A0869-Deutsche Bank Securities Inc.",
    value: "A0869-Deutsche Bank Securities Inc."
  }, {
    label: "A7339-DFPG Investments Inc",
    value: "A7339-DFPG Investments Inc"
  }, {
    label: "A0381-Diamant Investment Corporation",
    value: "A0381-Diamant Investment Corporation"
  }, {
    label: "A6392-Dinosaur Financial Group, LLC",
    value: "A6392-Dinosaur Financial Group, LLC"
  }, {
    label: "A6316-Diversified Resources, LLC",
    value: "A6316-Diversified Resources, LLC"
  }, {
    label: "A6992-Divine Capital Markets LLC",
    value: "A6992-Divine Capital Markets LLC"
  }, {
    label: "A4743-DM Kelly & Company",
    value: "A4743-DM Kelly & Company"
  }, {
    label: "A2995-Dominion Investor Services",
    value: "A2995-Dominion Investor Services"
  }, {
    label: "A6480-Donegal Securities, Inc.",
    value: "A6480-Donegal Securities, Inc."
  }, {
    label: "A0400-Dorn & Co., Inc.",
    value: "A0400-Dorn & Co., Inc."
  }, {
    label: "A0401-Dorsey & Company, Inc.",
    value: "A0401-Dorsey & Company, Inc."
  }, {
    label: "A1329-Dougherty & Company LLC",
    value: "A1329-Dougherty & Company LLC"
  }, {
    label: "A7115-Drexel Hamilton, LLC",
    value: "A7115-Drexel Hamilton, LLC"
  }, {
    label: "A0748-Duncan-Williams, Inc.",
    value: "A0748-Duncan-Williams, Inc."
  }, {
    label: "A7326-DV Financial Services LLC",
    value: "A7326-DV Financial Services LLC"
  }, {
    label: "A4526-E*Trade Securities LLC",
    value: "A4526-E*Trade Securities LLC"
  }, {
    label: "A3636-E-W Investments, Inc.",
    value: "A3636-E-W Investments, Inc."
  }, {
    label: "A5072-EarlyBirdCapital Inc.",
    value: "A5072-EarlyBirdCapital Inc."
  }, {
    label: "B0446-Eastern Bank",
    value: "B0446-Eastern Bank"
  }, {
    label: "A0425-Economy Securities, Incorporated",
    value: "A0425-Economy Securities, Incorporated"
  }, {
    label: "A0799-Edward Jones",
    value: "A0799-Edward Jones"
  }, {
    label: "A7416-EF Legacy Securities, LLC",
    value: "A7416-EF Legacy Securities, LLC"
  }, {
    label: "A4865-EFG Capital International",
    value: "A4865-EFG Capital International"
  }, {
    label: "A7034-Eide Bailly Securities LLC",
    value: "A7034-Eide Bailly Securities LLC"
  }, {
    label: "A6189-EK Riley Investments, LLC",
    value: "A6189-EK Riley Investments, LLC"
  }, {
    label: "A3735-Elish & Elish Inc.",
    value: "A3735-Elish & Elish Inc."
  }, {
    label: "A1137-Elmer E Powell & Company",
    value: "A1137-Elmer E Powell & Company"
  }, {
    label: "A6524-Emerson Equity LLC",
    value: "A6524-Emerson Equity LLC"
  }, {
    label: "A2082-Emmet & Co., Inc.",
    value: "A2082-Emmet & Co., Inc."
  }, {
    label: "A6852-Empire Asset Management Company",
    value: "A6852-Empire Asset Management Company"
  }, {
    label: "A7296-EMPIRE STATE FINANCIAL, INC.",
    value: "A7296-EMPIRE STATE FINANCIAL, INC."
  }, {
    label: "A5960-Equable Securities Corporation",
    value: "A5960-Equable Securities Corporation"
  }, {
    label: "A2272-Equity Services, Inc.",
    value: "A2272-Equity Services, Inc."
  }, {
    label: "A6751-ESL Investment Services, LLC",
    value: "A6751-ESL Investment Services, LLC"
  }, {
    label: "A6993-Esposito Securities, LLC",
    value: "A6993-Esposito Securities, LLC"
  }, {
    label: "A6423-Essex Financial Services, Inc.",
    value: "A6423-Essex Financial Services, Inc."
  }, {
    label: "A6248-Essex Securities LLC",
    value: "A6248-Essex Securities LLC"
  }, {
    label: "A3631-Estrada Hinojosa & Co., Inc.",
    value: "A3631-Estrada Hinojosa & Co., Inc."
  }, {
    label: "A6980-ETC Brokerage Services, LLC",
    value: "A6980-ETC Brokerage Services, LLC"
  }, {
    label: "A5045-Euro Pacific Capital, Inc.",
    value: "A5045-Euro Pacific Capital, Inc."
  }, {
    label: "A5449-Evertrade Direct Brokerage Inc",
    value: "A5449-Evertrade Direct Brokerage Inc"
  }, {
    label: "A4676-Excel Securities & Associates,Inc.",
    value: "A4676-Excel Securities & Associates,Inc."
  }, {
    label: "A7436-Exotix USA Inc.",
    value: "A7436-Exotix USA Inc."
  }, {
    label: "A5915-Fairport Capital, Inc.",
    value: "A5915-Fairport Capital, Inc."
  }, {
    label: "A7303-Falcon Square Capital, LLC",
    value: "A7303-Falcon Square Capital, LLC"
  }, {
    label: "A7386-FallLine Securities LLC",
    value: "A7386-FallLine Securities LLC"
  }, {
    label: "A0457-Family Investors Company",
    value: "A0457-Family Investors Company"
  }, {
    label: "A5219-Family Mgmt. Securities LLC",
    value: "A5219-Family Mgmt. Securities LLC"
  }, {
    label: "A5857-Farmers Financial Solution LLC",
    value: "A5857-Farmers Financial Solution LLC"
  }, {
    label: "A6486-FBL Marketing Services, LLC",
    value: "A6486-FBL Marketing Services, LLC"
  }, {
    label: "A7477-FBR Capital Markets & Co.",
    value: "A7477-FBR Capital Markets & Co."
  }, {
    label: "A4866-FCG Advisors, LLC",
    value: "A4866-FCG Advisors, LLC"
  }, {
    label: "A6214-Federated Securities Corp.",
    value: "A6214-Federated Securities Corp."
  }, {
    label: "A5351-Federated Securities, Inc.",
    value: "A5351-Federated Securities, Inc."
  }, {
    label: "A3995-Feldman, Ingardona & Co.",
    value: "A3995-Feldman, Ingardona & Co."
  }, {
    label: "A5859-Feldstein Financial Group, LLC",
    value: "A5859-Feldstein Financial Group, LLC"
  }, {
    label: "A6072-Feltl & Company",
    value: "A6072-Feltl & Company"
  }, {
    label: "A7181-FFC Capital Advisors, LLC",
    value: "A7181-FFC Capital Advisors, LLC"
  }, {
    label: "A0463-Fidelity Brokerage Services LLC",
    value: "A0463-Fidelity Brokerage Services LLC"
  }, {
    label: "A5752-Fidelity Investments Institutional Services Company, Inc",
    value: "A5752-Fidelity Investments Institutional Services Company, Inc"
  }, {
    label: "A3290-Fieldpoint Private Securities, LLC.",
    value: "A3290-Fieldpoint Private Securities, LLC."
  }, {
    label: "A1070-Fifth Third Securities, Inc.",
    value: "A1070-Fifth Third Securities, Inc."
  }, {
    label: "A6957-FIG Partners LLC",
    value: "A6957-FIG Partners LLC"
  }, {
    label: "A4729-Finance 500, Inc.",
    value: "A4729-Finance 500, Inc."
  }, {
    label: "A3302-Financial Northeastern Sec.Inc",
    value: "A3302-Financial Northeastern Sec.Inc"
  }, {
    label: "A0468-Financial Planning Consultants",
    value: "A0468-Financial Planning Consultants"
  }, {
    label: "A5952-Financial Security Management, Inc.",
    value: "A5952-Financial Security Management, Inc."
  }, {
    label: "A5582-Financial Sense Securities, Inc.",
    value: "A5582-Financial Sense Securities, Inc."
  }, {
    label: "A5874-Financial Telesis Inc.",
    value: "A5874-Financial Telesis Inc."
  }, {
    label: "A2291-Financial West Group",
    value: "A2291-Financial West Group"
  }, {
    label: "A6937-FinTech Securities, LLC",
    value: "A6937-FinTech Securities, LLC"
  }, {
    label: "A6623-FinTrust Brokerage Services, LLC",
    value: "A6623-FinTrust Brokerage Services, LLC"
  }, {
    label: "A4599-First Allied Securities, Inc.",
    value: "A4599-First Allied Securities, Inc."
  }, {
    label: "A6753-First Asset Financial Inc.",
    value: "A6753-First Asset Financial Inc."
  }, {
    label: "A6337-First Ballantyne, LLC",
    value: "A6337-First Ballantyne, LLC"
  }, {
    label: "A1942-First Bankers Banc Securities, Inc.",
    value: "A1942-First Bankers Banc Securities, Inc."
  }, {
    label: "A4066-First Brokers Securities, LLC",
    value: "A4066-First Brokers Securities, LLC"
  }, {
    label: "A6426-First Capital Equities Ltd.",
    value: "A6426-First Capital Equities Ltd."
  }, {
    label: "A5189-First Citizens Investor Svcs.",
    value: "A5189-First Citizens Investor Svcs."
  }, {
    label: "A6145-First Command Financial Planning, Inc.",
    value: "A6145-First Command Financial Planning, Inc."
  }, {
    label: "A3613-First Dallas Securities, Inc.",
    value: "A3613-First Dallas Securities, Inc."
  }, {
    label: "A2469-First Empire Securities, Inc.",
    value: "A2469-First Empire Securities, Inc."
  }, {
    label: "A5890-First Financial Equity Corporation",
    value: "A5890-First Financial Equity Corporation"
  }, {
    label: "A5879-First Financial Securities of America, Inc.",
    value: "A5879-First Financial Securities of America, Inc."
  }, {
    label: "A1452-First Georgetown Securities,Inc",
    value: "A1452-First Georgetown Securities,Inc"
  }, {
    label: "A4455-First Heartland Capital",
    value: "A4455-First Heartland Capital"
  }, {
    label: "A0695-First Honolulu Securities, Inc",
    value: "A0695-First Honolulu Securities, Inc"
  }, {
    label: "A6908-First Integrity Capital Partners, Corp.",
    value: "A6908-First Integrity Capital Partners, Corp."
  }, {
    label: "A0487-First Kentucky Securities Corporation.",
    value: "A0487-First Kentucky Securities Corporation."
  }, {
    label: "A1877-First Liberties Securities, Inc.",
    value: "A1877-First Liberties Securities, Inc."
  }, {
    label: "A0488-First Manhattan Co.",
    value: "A0488-First Manhattan Co."
  }, {
    label: "A0495-First Midstate Inc.",
    value: "A0495-First Midstate Inc."
  }, {
    label: "B0360-First National Bankers Bank",
    value: "B0360-First National Bankers Bank"
  }, {
    label: "A5914-First National Capital Markets, Inc.",
    value: "A5914-First National Capital Markets, Inc."
  }, {
    label: "A6497-First Public, LLC",
    value: "A6497-First Public, LLC"
  }, {
    label: "A5730-First Republic Secs. Co., LLC",
    value: "A5730-First Republic Secs. Co., LLC"
  }, {
    label: "A7475-First South Carolina Securities,Inc.",
    value: "A7475-First South Carolina Securities,Inc."
  }, {
    label: "A5297-First Southeast Investor Svcs., Inc.",
    value: "A5297-First Southeast Investor Svcs., Inc."
  }, {
    label: "A7093-First Southern Securities, LLC",
    value: "A7093-First Southern Securities, LLC"
  }, {
    label: "A7472-First Southern, LLC,",
    value: "A7472-First Southern, LLC,"
  }, {
    label: "A7435-FIRST STANDARD FINANCIAL COMPANY , LLC",
    value: "A7435-FIRST STANDARD FINANCIAL COMPANY , LLC"
  }, {
    label: "A2263-First State Financial Management",
    value: "A2263-First State Financial Management"
  }, {
    label: "A3898-First Trust Portfolios L.P.",
    value: "A3898-First Trust Portfolios L.P."
  }, {
    label: "A4993-First Tryon Securities, LLC",
    value: "A4993-First Tryon Securities, LLC"
  }, {
    label: "A2155-First Western Advisors",
    value: "A2155-First Western Advisors"
  }, {
    label: "A2800-First Western Securities, Inc.",
    value: "A2800-First Western Securities, Inc."
  }, {
    label: "A7016-FirstBank Puerto Rico Securities Corp.",
    value: "A7016-FirstBank Puerto Rico Securities Corp."
  }, {
    label: "A5409-Firstrade Securities, Inc.",
    value: "A5409-Firstrade Securities, Inc."
  }, {
    label: "A7352-FIS Brokerage & Securities Services LLC",
    value: "A7352-FIS Brokerage & Securities Services LLC"
  }, {
    label: "A5125-Florida Atlantic Secs. Corp.",
    value: "A5125-Florida Atlantic Secs. Corp."
  }, {
    label: "A7440-Flow Traders U.S., LLC",
    value: "A7440-Flow Traders U.S., LLC"
  }, {
    label: "A6091-FMN Capital Corporation",
    value: "A6091-FMN Capital Corporation"
  }, {
    label: "A0489-fmsbonds, Inc.",
    value: "A0489-fmsbonds, Inc."
  }, {
    label: "A6676-FNBB Capital Markets, LLC",
    value: "A6676-FNBB Capital Markets, LLC"
  }, {
    label: "A0516-Folger Nolan Fleming Douglas",
    value: "A0516-Folger Nolan Fleming Douglas"
  }, {
    label: "A6843-Folio Investments, Inc.",
    value: "A6843-Folio Investments, Inc."
  }, {
    label: "A6650-Forest Securities, Inc",
    value: "A6650-Forest Securities, Inc"
  }, {
    label: "A5919-Foresters Equity Services, Inc.",
    value: "A5919-Foresters Equity Services, Inc."
  }, {
    label: "A5053-Foresters Financial Services, Inc.",
    value: "A5053-Foresters Financial Services, Inc."
  }, {
    label: "A5128-Forte Securities, LLC",
    value: "A5128-Forte Securities, LLC"
  }, {
    label: "A7147-Fortune Financial Services, Inc.",
    value: "A7147-Fortune Financial Services, Inc."
  }, {
    label: "A6692-Fortune Securities, Inc.",
    value: "A6692-Fortune Securities, Inc."
  }, {
    label: "A6727-Founders Financial Securities, LLC",
    value: "A6727-Founders Financial Securities, LLC"
  }, {
    label: "A7298-Four Points Capital Partners LLC",
    value: "A7298-Four Points Capital Partners LLC"
  }, {
    label: "A6542-Fox Chase Capital Partners LLC",
    value: "A6542-Fox Chase Capital Partners LLC"
  }, {
    label: "A5769-Franklin Templeton Distributors, Inc.",
    value: "A5769-Franklin Templeton Distributors, Inc."
  }, {
    label: "A0529-Frazer Lanier Company,Inc.(The",
    value: "A0529-Frazer Lanier Company,Inc.(The"
  }, {
    label: "A1617-Freimark Blair & Company, Inc.",
    value: "A1617-Freimark Blair & Company, Inc."
  }, {
    label: "B0358-Frost Bank Capital Markets",
    value: "B0358-Frost Bank Capital Markets"
  }, {
    label: "A5240-Frost Brokerage Services, Inc.",
    value: "A5240-Frost Brokerage Services, Inc."
  }, {
    label: "A2290-FSB Premier Wealth Management, Inc.",
    value: "A2290-FSB Premier Wealth Management, Inc."
  }, {
    label: "A0453-FSC Securities Corporation",
    value: "A0453-FSC Securities Corporation"
  }, {
    label: "A4650-FSIC",
    value: "A4650-FSIC"
  }, {
    label: "A4773-FTB Advisors",
    value: "A4773-FTB Advisors"
  }, {
    label: "B0144-FTN Financial Capital Markets",
    value: "B0144-FTN Financial Capital Markets"
  }, {
    label: "A7363-FTN Financial Securities Corp",
    value: "A7363-FTN Financial Securities Corp"
  }, {
    label: "A7438-Fusion Analytics Securities LLC",
    value: "A7438-Fusion Analytics Securities LLC"
  }, {
    label: "A6257-G.L.S. & Associates, Inc.",
    value: "A6257-G.L.S. & Associates, Inc."
  }, {
    label: "A2729-GA Repple & Company",
    value: "A2729-GA Repple & Company"
  }, {
    label: "A0549-Gage-Wiley & Co., Inc.",
    value: "A0549-Gage-Wiley & Co., Inc."
  }, {
    label: "A6576-Gar Wood Securities, LLC",
    value: "A6576-Gar Wood Securities, LLC"
  }, {
    label: "A5399-Garden State Securities, Inc.",
    value: "A5399-Garden State Securities, Inc."
  }, {
    label: "A3682-Gardner Financial Services,Inc",
    value: "A3682-Gardner Financial Services,Inc"
  }, {
    label: "A1036-Garrett Nagle & Co., Inc.",
    value: "A1036-Garrett Nagle & Co., Inc."
  }, {
    label: "A4020-Gates Capital Corporation",
    value: "A4020-Gates Capital Corporation"
  }, {
    label: "A6363-GBS Retirement Services, Inc.",
    value: "A6363-GBS Retirement Services, Inc."
  }, {
    label: "A3882-GE Capital Markets, Inc.",
    value: "A3882-GE Capital Markets, Inc."
  }, {
    label: "A6087-Geneos Wealth Management, Inc.",
    value: "A6087-Geneos Wealth Management, Inc."
  }, {
    label: "A2167-General Securities Corp.",
    value: "A2167-General Securities Corp."
  }, {
    label: "A6924-Genesis Global Trading, Inc.",
    value: "A6924-Genesis Global Trading, Inc."
  }, {
    label: "A6340-Geoffrey Richards Securities Corp.",
    value: "A6340-Geoffrey Richards Securities Corp."
  }, {
    label: "A0104-George K Baum & Company",
    value: "A0104-George K Baum & Company"
  }, {
    label: "A0958-George McKelvey Co., Inc.",
    value: "A0958-George McKelvey Co., Inc."
  }, {
    label: "A6618-GF Investment Services, LLC",
    value: "A6618-GF Investment Services, LLC"
  }, {
    label: "A6673-GFI Securities, LLC",
    value: "A6673-GFI Securities, LLC"
  }, {
    label: "A0566-Gilbert, Doniger & Co., Inc.",
    value: "A0566-Gilbert, Doniger & Co., Inc."
  }, {
    label: "A1819-Gill Capital Partners",
    value: "A1819-Gill Capital Partners"
  }, {
    label: "A1519-GIT Investment Services, Inc.",
    value: "A1519-GIT Investment Services, Inc."
  }, {
    label: "A5315-Gladowsky Capital Mgmt. Corp.",
    value: "A5315-Gladowsky Capital Mgmt. Corp."
  }, {
    label: "A6432-Glen Eagle Wealth, LLC",
    value: "A6432-Glen Eagle Wealth, LLC"
  }, {
    label: "A7129-Glendale Securities, Inc.",
    value: "A7129-Glendale Securities, Inc."
  }, {
    label: "A4970-Global Brokerage Services, Inc",
    value: "A4970-Global Brokerage Services, Inc"
  }, {
    label: "A4446-Global Financial Services, LLC",
    value: "A4446-Global Financial Services, LLC"
  }, {
    label: "A6343-Global Investor Services, L.C.",
    value: "A6343-Global Investor Services, L.C."
  }, {
    label: "A6781-GlobaLink Securities, Inc",
    value: "A6781-GlobaLink Securities, Inc"
  }, {
    label: "A5854-GMP Securities, LLC.",
    value: "A5854-GMP Securities, LLC."
  }, {
    label: "A5880-Gold Coast Securities, Inc.",
    value: "A5880-Gold Coast Securities, Inc."
  }, {
    label: "A0576-Goldman Sachs & Co. LLC",
    value: "A0576-Goldman Sachs & Co. LLC"
  }, {
    label: "A6111-Googins Advisors, Inc.",
    value: "A6111-Googins Advisors, Inc."
  }, {
    label: "A5412-Government Capital Securities Corporation",
    value: "A5412-Government Capital Securities Corporation"
  }, {
    label: "A4961-Government Perspectives, LLC",
    value: "A4961-Government Perspectives, LLC"
  }, {
    label: "A7139-Gradient Securities, LLC",
    value: "A7139-Gradient Securities, LLC"
  }, {
    label: "A6560-GRB Financial, LLC",
    value: "A6560-GRB Financial, LLC"
  }, {
    label: "A4869-Great American Advisors, Inc.",
    value: "A4869-Great American Advisors, Inc."
  }, {
    label: "A4571-Great American Investors, Inc.",
    value: "A4571-Great American Investors, Inc."
  }, {
    label: "A4303-Great Pacific Securities",
    value: "A4303-Great Pacific Securities"
  }, {
    label: "A4880-Greenberg Financial Group",
    value: "A4880-Greenberg Financial Group"
  }, {
    label: "A6022-Greenbrier Diversified, Inc.",
    value: "A6022-Greenbrier Diversified, Inc."
  }, {
    label: "A7447-Greentree Brokerage Services, Inc.",
    value: "A7447-Greentree Brokerage Services, Inc."
  }, {
    label: "A1272-Gregory J Schwartz & Co., Inc.",
    value: "A1272-Gregory J Schwartz & Co., Inc."
  }, {
    label: "A7469-Greystone Broker Dealer Corp",
    value: "A7469-Greystone Broker Dealer Corp"
  }, {
    label: "A3985-Grodsky Associates, Inc.",
    value: "A3985-Grodsky Associates, Inc."
  }, {
    label: "A4820-Guggenheim Funds Distributors, LLC.",
    value: "A4820-Guggenheim Funds Distributors, LLC."
  }, {
    label: "A6789-Guggenheim Securities, LLC",
    value: "A6789-Guggenheim Securities, LLC"
  }, {
    label: "A2938-Guzman & Company",
    value: "A2938-Guzman & Company"
  }, {
    label: "A6029-GVC Capital LLC",
    value: "A6029-GVC Capital LLC"
  }, {
    label: "A3556-GW & Wade Asset Mgt.Co.",
    value: "A3556-GW & Wade Asset Mgt.Co."
  }, {
    label: "A6505-GWN Securities, Inc.",
    value: "A6505-GWN Securities, Inc."
  }, {
    label: "A2183-H Beck, Inc.",
    value: "A2183-H Beck, Inc."
  }, {
    label: "A1681-H.D. Vest Investment Securities, Inc.",
    value: "A1681-H.D. Vest Investment Securities, Inc."
  }, {
    label: "A7304-H2C Securities Inc.",
    value: "A7304-H2C Securities Inc."
  }, {
    label: "A6399-Hall & Romkema Financial Services, LLC",
    value: "A6399-Hall & Romkema Financial Services, LLC"
  }, {
    label: "A6580-Halliday Financial, LLC",
    value: "A6580-Halliday Financial, LLC"
  }, {
    label: "A6787-Hamilton Cavanaugh Investment Brokers, Inc.",
    value: "A6787-Hamilton Cavanaugh Investment Brokers, Inc."
  }, {
    label: "A4871-Hancock Investment Services",
    value: "A4871-Hancock Investment Services"
  }, {
    label: "A6040-Hand Securities, Inc.",
    value: "A6040-Hand Securities, Inc."
  }, {
    label: "A6871-Hanson McClain",
    value: "A6871-Hanson McClain"
  }, {
    label: "A5525-Hantz Financial Services, Inc.",
    value: "A5525-Hantz Financial Services, Inc."
  }, {
    label: "A0441-Hapoalim Securities USA, Inc.",
    value: "A0441-Hapoalim Securities USA, Inc."
  }, {
    label: "A3627-Harbor Financial Services, L.L.C.",
    value: "A3627-Harbor Financial Services, L.L.C."
  }, {
    label: "A7043-Harbor Investment Advisory, LLC",
    value: "A7043-Harbor Investment Advisory, LLC"
  }, {
    label: "A3229-Harbour Investments, Inc.",
    value: "A3229-Harbour Investments, Inc."
  }, {
    label: "A4542-Harger & Company, Inc.",
    value: "A4542-Harger & Company, Inc."
  }, {
    label: "A0626-Hartfield,Titus & Donnelly LLC",
    value: "A0626-Hartfield,Titus & Donnelly LLC"
  }, {
    label: "A7266-Hartford Funds Distributors, LLC",
    value: "A7266-Hartford Funds Distributors, LLC"
  }, {
    label: "A1963-Harvest Financial Corporation",
    value: "A1963-Harvest Financial Corporation"
  }, {
    label: "A4382-Harvestons Securities, Inc.",
    value: "A4382-Harvestons Securities, Inc."
  }, {
    label: "A1577-Haverford Trust Securities, Inc.",
    value: "A1577-Haverford Trust Securities, Inc."
  }, {
    label: "A0631-Hazard & Siegel, Inc.",
    value: "A0631-Hazard & Siegel, Inc."
  }, {
    label: "A0632-Hazlett, Burt & Watson, Inc.",
    value: "A0632-Hazlett, Burt & Watson, Inc."
  }, {
    label: "A0377-HC Denison Company",
    value: "A0377-HC Denison Company"
  }, {
    label: "A7353-Headlands Tech Global Markets, LLC",
    value: "A7353-Headlands Tech Global Markets, LLC"
  }, {
    label: "A0635-Hefren-Tillotson, Inc.",
    value: "A0635-Hefren-Tillotson, Inc."
  }, {
    label: "A4981-Heim, Young & Associates, Inc.",
    value: "A4981-Heim, Young & Associates, Inc."
  }, {
    label: "A6579-Henley & Company LLC",
    value: "A6579-Henley & Company LLC"
  }, {
    label: "A3574-Hennion & Walsh Inc.",
    value: "A3574-Hennion & Walsh Inc."
  }, {
    label: "A1481-Herbert J Sims & Co., Inc.",
    value: "A1481-Herbert J Sims & Co., Inc."
  }, {
    label: "A6627-Heritage Financial Systems, Inc.",
    value: "A6627-Heritage Financial Systems, Inc."
  }, {
    label: "A5316-Herndon Plant Oakley Ltd.",
    value: "A5316-Herndon Plant Oakley Ltd."
  }, {
    label: "A6107-Hewitt Financial Services LLC",
    value: "A6107-Hewitt Financial Services LLC"
  }, {
    label: "A5061-Higgins Capital Management",
    value: "A5061-Higgins Capital Management"
  }, {
    label: "A2723-Highlander Capital Group, Inc.",
    value: "A2723-Highlander Capital Group, Inc."
  }, {
    label: "A6950-HighTower Securities, LLC",
    value: "A6950-HighTower Securities, LLC"
  }, {
    label: "A1290-Hilltop Securities Inc.",
    value: "A1290-Hilltop Securities Inc."
  }, {
    label: "A2447-Hilltop Securities Independent Network Inc.",
    value: "A2447-Hilltop Securities Independent Network Inc."
  }, {
    label: "A1852-Home Financial Services, Inc.",
    value: "A1852-Home Financial Services, Inc."
  }, {
    label: "A7448-Honey Badger Investment Securities, LLC",
    value: "A7448-Honey Badger Investment Securities, LLC"
  }, {
    label: "A6356-Horace Mann Investors, Inc.",
    value: "A6356-Horace Mann Investors, Inc."
  }, {
    label: "A6691-Horan Securities, Inc.",
    value: "A6691-Horan Securities, Inc."
  }, {
    label: "A1729-Hornor, Townsend & Kent, Inc.",
    value: "A1729-Hornor, Townsend & Kent, Inc."
  }, {
    label: "A0563-HSBC Securities (USA) Inc.",
    value: "A0563-HSBC Securities (USA) Inc."
  }, {
    label: "A6254-Hudson Heritage Capital Management, Inc.",
    value: "A6254-Hudson Heritage Capital Management, Inc."
  }, {
    label: "A4099-Hunter Associates, LLC",
    value: "A4099-Hunter Associates, LLC"
  }, {
    label: "A2328-Huntington Investment Company",
    value: "A2328-Huntington Investment Company"
  }, {
    label: "A0850-Huntleigh Securities Corporation",
    value: "A0850-Huntleigh Securities Corporation"
  }, {
    label: "A0693-Hutchinson,Shockey,Erley & Co.",
    value: "A0693-Hutchinson,Shockey,Erley & Co."
  }, {
    label: "A6114-IBN Financial Services, Inc.",
    value: "A6114-IBN Financial Services, Inc."
  }, {
    label: "A6419-ICAP Corporates LLC",
    value: "A6419-ICAP Corporates LLC"
  }, {
    label: "A3341-ICBA Securities",
    value: "A3341-ICBA Securities"
  }, {
    label: "A6202-IDB Capital Corp.",
    value: "A6202-IDB Capital Corp."
  }, {
    label: "A6998-IFS Securities",
    value: "A6998-IFS Securities"
  }, {
    label: "A5148-Imperial Capital, LLC",
    value: "A5148-Imperial Capital, LLC"
  }, {
    label: "A4596-IMS Securities, Inc.",
    value: "A4596-IMS Securities, Inc."
  }, {
    label: "A5756-Incapital LLC",
    value: "A5756-Incapital LLC"
  }, {
    label: "A6427-Independence Capital Co., Inc.",
    value: "A6427-Independence Capital Co., Inc."
  }, {
    label: "A0606-Independent Financial Group, LLC",
    value: "A0606-Independent Financial Group, LLC"
  }, {
    label: "A5280-Indiana Securities, LLC",
    value: "A5280-Indiana Securities, LLC"
  }, {
    label: "A7131-Industrial and Commerical Bank of China Financial Services LLC",
    value: "A7131-Industrial and Commerical Bank of China Financial Services LLC"
  }, {
    label: "A4421-Infinex Investments, Inc.",
    value: "A4421-Infinex Investments, Inc."
  }, {
    label: "A6883-Infinity Financial Services",
    value: "A6883-Infinity Financial Services"
  }, {
    label: "A0706-Ingalls & Snyder LLC",
    value: "A0706-Ingalls & Snyder LLC"
  }, {
    label: "A0682-Insight Securities, Inc.",
    value: "A0682-Insight Securities, Inc."
  }, {
    label: "A7419-Institutional Bond Network LLC.",
    value: "A7419-Institutional Bond Network LLC."
  }, {
    label: "A5797-Institutional Securities Corp.",
    value: "A5797-Institutional Securities Corp."
  }, {
    label: "A6690-Integral Financial LLC",
    value: "A6690-Integral Financial LLC"
  }, {
    label: "A6541-Integrated Financial Planning Services",
    value: "A6541-Integrated Financial Planning Services"
  }, {
    label: "A7430-Integrity Brokerage Sevices, Inc.",
    value: "A7430-Integrity Brokerage Sevices, Inc."
  }, {
    label: "A6912-Interactive Brokers LLC",
    value: "A6912-Interactive Brokers LLC"
  }, {
    label: "A2868-Intercarolina Financial Svs,In",
    value: "A2868-Intercarolina Financial Svs,In"
  }, {
    label: "A0117-Intercoastal Capital Markets, Inc.",
    value: "A0117-Intercoastal Capital Markets, Inc."
  }, {
    label: "A3707-Intercontinental Asset Mgmt,Gr",
    value: "A3707-Intercontinental Asset Mgmt,Gr"
  }, {
    label: "A4089-International Assets Advisory, LLC",
    value: "A4089-International Assets Advisory, LLC"
  }, {
    label: "A2442-International Money Management Group, Inc",
    value: "A2442-International Money Management Group, Inc"
  }, {
    label: "A2822-International Research Sec.Inc",
    value: "A2822-International Research Sec.Inc"
  }, {
    label: "A5789-Intervest International Equities Corporation",
    value: "A5789-Intervest International Equities Corporation"
  }, {
    label: "A7443-INTL Custody & Clearing Solutions Inc.",
    value: "A7443-INTL Custody & Clearing Solutions Inc."
  }, {
    label: "A7327-INTL FCSTONE FINANCIAL INC.",
    value: "A7327-INTL FCSTONE FINANCIAL INC."
  }, {
    label: "A0712-Invemed Associates LLC.",
    value: "A0712-Invemed Associates LLC."
  }, {
    label: "A6522-Inverness Securities, LLC",
    value: "A6522-Inverness Securities, LLC"
  }, {
    label: "A1401-Invesco Capital Markets, Inc.",
    value: "A1401-Invesco Capital Markets, Inc."
  }, {
    label: "A5938-Invesco Distributors, Inc",
    value: "A5938-Invesco Distributors, Inc"
  }, {
    label: "A1514-Invest Financial Corp.",
    value: "A1514-Invest Financial Corp."
  }, {
    label: "A0719-Investacorp, Inc.",
    value: "A0719-Investacorp, Inc."
  }, {
    label: "A2458-Investment Center, Inc.(The)",
    value: "A2458-Investment Center, Inc.(The)"
  }, {
    label: "A3690-Investment Centers of America",
    value: "A3690-Investment Centers of America"
  }, {
    label: "A6473-Investment Network, Inc",
    value: "A6473-Investment Network, Inc"
  }, {
    label: "A3944-Investment Placement Group",
    value: "A3944-Investment Placement Group"
  }, {
    label: "A5065-Investment Planners, Inc.",
    value: "A5065-Investment Planners, Inc."
  }, {
    label: "A4134-Investment Professionals, Inc.",
    value: "A4134-Investment Professionals, Inc."
  }, {
    label: "A7299-Investment Security Corporation",
    value: "A7299-Investment Security Corporation"
  }, {
    label: "A5181-Investments Architects, Inc.",
    value: "A5181-Investments Architects, Inc."
  }, {
    label: "A6933-Investments For You, Inc.",
    value: "A6933-Investments For You, Inc."
  }, {
    label: "A2080-Investors Brokerage of Texas, Ltd.",
    value: "A2080-Investors Brokerage of Texas, Ltd."
  }, {
    label: "A0727-Isaak Bond Investments, Inc.",
    value: "A0727-Isaak Bond Investments, Inc."
  }, {
    label: "A4978-J Alden Associates",
    value: "A4978-J Alden Associates"
  }, {
    label: "A6948-J K Financial Services, Inc.",
    value: "A6948-J K Financial Services, Inc."
  }, {
    label: "A3035-J.A. Glynn Investments, LLC.",
    value: "A3035-J.A. Glynn Investments, LLC."
  }, {
    label: "K0478-J.P. Morgan Institutional Investments Inc.",
    value: "K0478-J.P. Morgan Institutional Investments Inc."
  }, {
    label: "A0105-J.P. Morgan Securities LLC",
    value: "A0105-J.P. Morgan Securities LLC"
  }, {
    label: "A7114-J.V.B. Financial Group, LLC",
    value: "A7114-J.V.B. Financial Group, LLC"
  }, {
    label: "A6306-J.W. Cole Financial, Inc.",
    value: "A6306-J.W. Cole Financial, Inc."
  }, {
    label: "A0215-Jack V Butterfield Invest.",
    value: "A0215-Jack V Butterfield Invest."
  }, {
    label: "A5962-Jacques Financial LLC",
    value: "A5962-Jacques Financial LLC"
  }, {
    label: "A0136-James I Black & Company",
    value: "A0136-James I Black & Company"
  }, {
    label: "A6140-James T. Borello & Co.",
    value: "A6140-James T. Borello & Co."
  }, {
    label: "A7305-Jane Street Capital, LLC",
    value: "A7305-Jane Street Capital, LLC"
  }, {
    label: "A0785-Janney Montgomery Scott LLC",
    value: "A0785-Janney Montgomery Scott LLC"
  }, {
    label: "A6925-JBS Liberty Securities, Inc.",
    value: "A6925-JBS Liberty Securities, Inc."
  }, {
    label: "A1287-JD Seibert & Company, Inc.",
    value: "A1287-JD Seibert & Company, Inc."
  }, {
    label: "A3843-JDL Securities Corporation",
    value: "A3843-JDL Securities Corporation"
  }, {
    label: "A5232-Jefferies LLC",
    value: "A5232-Jefferies LLC"
  }, {
    label: "A5141-JH Darbie & Co., Inc.",
    value: "A5141-JH Darbie & Co., Inc."
  }, {
    label: "A0668-JJB Hilliard, WL Lyons, LLC",
    value: "A0668-JJB Hilliard, WL Lyons, LLC"
  }, {
    label: "A5967-JK Securities, Inc.",
    value: "A5967-JK Securities, Inc."
  }, {
    label: "A0786-JKR & Co., Inc.",
    value: "A0786-JKR & Co., Inc."
  }, {
    label: "A0797-Joe Jolly & Co., Inc.",
    value: "A0797-Joe Jolly & Co., Inc."
  }, {
    label: "A5840-John Hancock Distributors LLC",
    value: "A5840-John Hancock Distributors LLC"
  }, {
    label: "A6971-John Hancock Funds, LLC",
    value: "A6971-John Hancock Funds, LLC"
  }, {
    label: "A2402-John W Loofbourow Associates, Inc.",
    value: "A2402-John W Loofbourow Associates, Inc."
  }, {
    label: "A7377-Jones Lang LaSalle Securities, LLC",
    value: "A7377-Jones Lang LaSalle Securities, LLC"
  }, {
    label: "A6658-Joseph Grace",
    value: "A6658-Joseph Grace"
  }, {
    label: "A5164-Joseph Gunnar & Co. LLC",
    value: "A5164-Joseph Gunnar & Co. LLC"
  }, {
    label: "A7417-JOSEPH STONE CAPITAL, LLC",
    value: "A7417-JOSEPH STONE CAPITAL, LLC"
  }, {
    label: "K0403-JPMorgan Distribution Services, Inc.",
    value: "K0403-JPMorgan Distribution Services, Inc."
  }, {
    label: "A7224-JRL CAPITAL CORPORATION",
    value: "A7224-JRL CAPITAL CORPORATION"
  }, {
    label: "A7209-JSL Securities, Inc.",
    value: "A7209-JSL Securities, Inc."
  }, {
    label: "A3698-JW Korth & Company",
    value: "A3698-JW Korth & Company"
  }, {
    label: "A6367-K.W. Chambers & Co.",
    value: "A6367-K.W. Chambers & Co."
  }, {
    label: "A4274-KA Associates, Inc.",
    value: "A4274-KA Associates, Inc."
  }, {
    label: "A0806-Kahn Brothers LLC",
    value: "A0806-Kahn Brothers LLC"
  }, {
    label: "A6482-Kalos Capital, Inc.",
    value: "A6482-Kalos Capital, Inc."
  }, {
    label: "A6440-KCD Financial, Inc.",
    value: "A6440-KCD Financial, Inc."
  }, {
    label: "A7052-KCG Securities, LLC",
    value: "A7052-KCG Securities, LLC"
  }, {
    label: "A6383-KCOE Capital Advisors, LLC",
    value: "A6383-KCOE Capital Advisors, LLC"
  }, {
    label: "A2764-Kedem Capital Corporation",
    value: "A2764-Kedem Capital Corporation"
  }, {
    label: "A6518-Keel Point Capital, LLC",
    value: "A6518-Keel Point Capital, LLC"
  }, {
    label: "A3440-Kenneth, Jerome & Company, Inc",
    value: "A3440-Kenneth, Jerome & Company, Inc"
  }, {
    label: "A0823-Kensington Capital Corp.",
    value: "A0823-Kensington Capital Corp."
  }, {
    label: "A2049-Kercheville & Company, Inc.",
    value: "A2049-Kercheville & Company, Inc."
  }, {
    label: "A5273-Kestra Investment Services, LLC",
    value: "A5273-Kestra Investment Services, LLC"
  }, {
    label: "A2840-Kevin Hart Kornfield & Company",
    value: "A2840-Kevin Hart Kornfield & Company"
  }, {
    label: "A6708-Key Investment Services",
    value: "A6708-Key Investment Services"
  }, {
    label: "A7055-Key West Investments, LLC",
    value: "A7055-Key West Investments, LLC"
  }, {
    label: "A0953-KeyBanc Capital Markets Inc.",
    value: "A0953-KeyBanc Capital Markets Inc."
  }, {
    label: "A4684-Kiley Partners, Inc.",
    value: "A4684-Kiley Partners, Inc."
  }, {
    label: "A0835-Kimelman & Baird, LLC",
    value: "A0835-Kimelman & Baird, LLC"
  }, {
    label: "A5966-Kingsbury Capital, Inc.",
    value: "A5966-Kingsbury Capital, Inc."
  }, {
    label: "A6881-Kipling Jones & Co., Ltd.",
    value: "A6881-Kipling Jones & Co., Ltd."
  }, {
    label: "A2856-KJM Securities, Inc.",
    value: "A2856-KJM Securities, Inc."
  }, {
    label: "A3316-KMS Financial Services, Inc.",
    value: "A3316-KMS Financial Services, Inc."
  }, {
    label: "A7310-Kovack International Securities, Inc",
    value: "A7310-Kovack International Securities, Inc"
  }, {
    label: "A5250-Kovack Securities, Inc.",
    value: "A5250-Kovack Securities, Inc."
  }, {
    label: "A6559-Kovitz Securities, LLC",
    value: "A6559-Kovitz Securities, LLC"
  }, {
    label: "A5374-KR Securities, LLC",
    value: "A5374-KR Securities, LLC"
  }, {
    label: "A4635-Kuehl Capital Corporation",
    value: "A4635-Kuehl Capital Corporation"
  }, {
    label: "A0854-Kuykendall & Schneider, Inc.",
    value: "A0854-Kuykendall & Schneider, Inc."
  }, {
    label: "A0805-KW Securities Corporation",
    value: "A0805-KW Securities Corporation"
  }, {
    label: "A4071-L.J. Hart & Company",
    value: "A4071-L.J. Hart & Company"
  }, {
    label: "A4547-L.M. Kohn & Company",
    value: "A4547-L.M. Kohn & Company"
  }, {
    label: "A6055-LaBrunerie Financial Services, Inc.",
    value: "A6055-LaBrunerie Financial Services, Inc."
  }, {
    label: "A0860-Ladenburg, Thalmann & Co., Inc",
    value: "A0860-Ladenburg, Thalmann & Co., Inc"
  }, {
    label: "A3082-Lafayette Investments, Inc",
    value: "A3082-Lafayette Investments, Inc"
  }, {
    label: "A6038-Laidlaw and Company, UK, Ltd",
    value: "A6038-Laidlaw and Company, UK, Ltd"
  }, {
    label: "A2869-Lamon & Stern, Inc.",
    value: "A2869-Lamon & Stern, Inc."
  }, {
    label: "A5579-Lampert Capital Markets Inc.",
    value: "A5579-Lampert Capital Markets Inc."
  }, {
    label: "A3381-Lancaster Pollard & Co., LLC",
    value: "A3381-Lancaster Pollard & Co., LLC"
  }, {
    label: "A5490-LANDAAS & COMPANY",
    value: "A5490-LANDAAS & COMPANY"
  }, {
    label: "A3899-Landolt Securities, Inc.",
    value: "A3899-Landolt Securities, Inc."
  }, {
    label: "A4253-Lantern Investments, Inc.",
    value: "A4253-Lantern Investments, Inc."
  }, {
    label: "A6927-Lara, May & Associates, LLC",
    value: "A6927-Lara, May & Associates, LLC"
  }, {
    label: "A2649-Larimer Capital Corporation",
    value: "A2649-Larimer Capital Corporation"
  }, {
    label: "A7083-Larson Financial Securities, LLC",
    value: "A7083-Larson Financial Securities, LLC"
  }, {
    label: "A0859-LaSalle Street Securities, LLC",
    value: "A0859-LaSalle Street Securities, LLC"
  }, {
    label: "A2841-LB Fisher & Company",
    value: "A2841-LB Fisher & Company"
  }, {
    label: "A6907-Lebenthal & Co., LLC",
    value: "A6907-Lebenthal & Co., LLC"
  }, {
    label: "A5262-Leerink Partners LLC",
    value: "A5262-Leerink Partners LLC"
  }, {
    label: "A6450-Legacy Asset Securities, Inc.",
    value: "A6450-Legacy Asset Securities, Inc."
  }, {
    label: "A6707-Legg Mason Investor Services, LLC",
    value: "A6707-Legg Mason Investor Services, LLC"
  }, {
    label: "A4752-Leigh Baldwin & Co., LLC",
    value: "A4752-Leigh Baldwin & Co., LLC"
  }, {
    label: "A6686-Lek Securities Corporation",
    value: "A6686-Lek Securities Corporation"
  }, {
    label: "A4502-Lenox Financial Services, Inc.",
    value: "A4502-Lenox Financial Services, Inc."
  }, {
    label: "A5322-Lesko Securities, Inc.",
    value: "A5322-Lesko Securities, Inc."
  }, {
    label: "A5733-Leumi Investment Services Inc.",
    value: "A5733-Leumi Investment Services Inc."
  }, {
    label: "A4774-Lewis Young Robertson & Burningham, Inc.",
    value: "A4774-Lewis Young Robertson & Burningham, Inc."
  }, {
    label: "A3774-Lexington Investment Company,",
    value: "A3774-Lexington Investment Company,"
  }, {
    label: "B0465-Liberty Bank and Trust Company",
    value: "B0465-Liberty Bank and Trust Company"
  }, {
    label: "A3572-Liberty Capital Investment Corporation",
    value: "A3572-Liberty Capital Investment Corporation"
  }, {
    label: "A5742-Liberty Group, LLC",
    value: "A5742-Liberty Group, LLC"
  }, {
    label: "A6581-Liberty Partners Financial Services, LLC",
    value: "A6581-Liberty Partners Financial Services, LLC"
  }, {
    label: "A5088-Lieblong & Associates, Inc.",
    value: "A5088-Lieblong & Associates, Inc."
  }, {
    label: "A5760-LifeMark Securities Corp",
    value: "A5760-LifeMark Securities Corp"
  }, {
    label: "A7179-LINCOLN DOUGLAS INVESTMENTS, LLC",
    value: "A7179-LINCOLN DOUGLAS INVESTMENTS, LLC"
  }, {
    label: "A2842-Lincoln Financial Advisors Corporation",
    value: "A2842-Lincoln Financial Advisors Corporation"
  }, {
    label: "A0269-Lincoln Financial Securities Corporation",
    value: "A0269-Lincoln Financial Securities Corporation"
  }, {
    label: "A4178-Lincoln Investment Planning, LLC",
    value: "A4178-Lincoln Investment Planning, LLC"
  }, {
    label: "A7372-Lion Street Financial, LLC",
    value: "A7372-Lion Street Financial, LLC"
  }, {
    label: "A6945-Livingston Securities,LLC.",
    value: "A6945-Livingston Securities,LLC."
  }, {
    label: "A1349-LO Thomas & Co., Inc.",
    value: "A1349-LO Thomas & Co., Inc."
  }, {
    label: "A3836-Lombard Securities Inc.",
    value: "A3836-Lombard Securities Inc."
  }, {
    label: "A6599-Long Island Financial Group Inc.",
    value: "A6599-Long Island Financial Group Inc."
  }, {
    label: "A5139-Loop Capital Markets, L.L.C.",
    value: "A5139-Loop Capital Markets, L.L.C."
  }, {
    label: "A6193-Loria Financial Group, LLC",
    value: "A6193-Loria Financial Group, LLC"
  }, {
    label: "A5419-Loring Ward Securities Inc.",
    value: "A5419-Loring Ward Securities Inc."
  }, {
    label: "A4928-Lowell & Company",
    value: "A4928-Lowell & Company"
  }, {
    label: "A6011-LPE Securities, LLC",
    value: "A6011-LPE Securities, LLC"
  }, {
    label: "A1145-LPL Financial LLC",
    value: "A1145-LPL Financial LLC"
  }, {
    label: "A7167-LPS Partners Inc.",
    value: "A7167-LPS Partners Inc."
  }, {
    label: "A7159-Lucia Securities, LLC",
    value: "A7159-Lucia Securities, LLC"
  }, {
    label: "A6328-Lucien, Stirling & Gray Financial Corp.",
    value: "A6328-Lucien, Stirling & Gray Financial Corp."
  }, {
    label: "A0594-M Griffith Investment Services Inc.",
    value: "A0594-M Griffith Investment Services Inc."
  }, {
    label: "A5560-M Holdings Securities, Inc.",
    value: "A5560-M Holdings Securities, Inc."
  }, {
    label: "A5241-M&T SECURITES INC",
    value: "A5241-M&T SECURITES INC"
  }, {
    label: "A6223-M.J. Whitman LLC",
    value: "A6223-M.J. Whitman LLC"
  }, {
    label: "A2421-Mack Investment Securities,Inc",
    value: "A2421-Mack Investment Securities,Inc"
  }, {
    label: "A6703-Madison Avenue Securities",
    value: "A6703-Madison Avenue Securities"
  }, {
    label: "A2986-Maine Securities Corporation",
    value: "A2986-Maine Securities Corporation"
  }, {
    label: "A6935-Mainline West Municipal Securities, LLC",
    value: "A6935-Mainline West Municipal Securities, LLC"
  }, {
    label: "A7247-Maitland Securities, Inc.",
    value: "A7247-Maitland Securities, Inc."
  }, {
    label: "A6641-Managed Account Services, LLC",
    value: "A6641-Managed Account Services, LLC"
  }, {
    label: "B0125-Manufacturers and Traders Trust Co.",
    value: "B0125-Manufacturers and Traders Trust Co."
  }, {
    label: "A5574-Maplewood Investment Advisors",
    value: "A5574-Maplewood Investment Advisors"
  }, {
    label: "A2857-Marc J Lane and Company",
    value: "A2857-Marc J Lane and Company"
  }, {
    label: "A7097-MarketAxess Corporation",
    value: "A7097-MarketAxess Corporation"
  }, {
    label: "A2770-Marsco Investment Corporation",
    value: "A2770-Marsco Investment Corporation"
  }, {
    label: "A1037-Martin Nelson & Co., Inc.",
    value: "A1037-Martin Nelson & Co., Inc."
  }, {
    label: "A4465-Mason Securities, Inc.",
    value: "A4465-Mason Securities, Inc."
  }, {
    label: "A7418-Match-Point Securities, LLC",
    value: "A7418-Match-Point Securities, LLC"
  }, {
    label: "A6154-Maxim Group LLC",
    value: "A6154-Maxim Group LLC"
  }, {
    label: "A7132-MBS Capital Markets, LLC",
    value: "A7132-MBS Capital Markets, LLC"
  }, {
    label: "A4545-MBSC Securities Corporation",
    value: "A4545-MBSC Securities Corporation"
  }, {
    label: "A7442-MCAP LLC",
    value: "A7442-MCAP LLC"
  }, {
    label: "A2389-McClurg Capital Corporation",
    value: "A2389-McClurg Capital Corporation"
  }, {
    label: "A0951-McCourtney-Breckenridge Company",
    value: "A0951-McCourtney-Breckenridge Company"
  }, {
    label: "A7134-McDermott Investment Services, LLC",
    value: "A7134-McDermott Investment Services, LLC"
  }, {
    label: "A6687-McDonald Partners, LLC",
    value: "A6687-McDonald Partners, LLC"
  }, {
    label: "A7264-MCG Securities LLC",
    value: "A7264-MCG Securities LLC"
  }, {
    label: "A7120-McLaughlin Ryder Investments, Inc.",
    value: "A7120-McLaughlin Ryder Investments, Inc."
  }, {
    label: "A0962-McLiney and Company",
    value: "A0962-McLiney and Company"
  }, {
    label: "A6180-McNally Financial Services Corporation",
    value: "A6180-McNally Financial Services Corporation"
  }, {
    label: "A0028-ME Allison & Co., Inc.",
    value: "A0028-ME Allison & Co., Inc."
  }, {
    label: "A6835-Meadowvale Advisors, LLC",
    value: "A6835-Meadowvale Advisors, LLC"
  }, {
    label: "A0970-Means Wealth Management",
    value: "A0970-Means Wealth Management"
  }, {
    label: "A7059-Melio Securities Company, LLC",
    value: "A7059-Melio Securities Company, LLC"
  }, {
    label: "A4105-Melvin Securities, LLC",
    value: "A4105-Melvin Securities, LLC"
  }, {
    label: "A6540-Mercadien Securities, LLC",
    value: "A6540-Mercadien Securities, LLC"
  }, {
    label: "A6523-Mercantil CommerceBank Investment Services",
    value: "A6523-Mercantil CommerceBank Investment Services"
  }, {
    label: "A5885-Meridien Financial Group, Inc.",
    value: "A5885-Meridien Financial Group, Inc."
  }, {
    label: "A2141-Merrill Lynch Prof. Clearing",
    value: "A2141-Merrill Lynch Prof. Clearing"
  }, {
    label: "A0976-Merrill Lynch, Pierce, Fenner",
    value: "A0976-Merrill Lynch, Pierce, Fenner"
  }, {
    label: "A6350-Merrion Securities, LLC",
    value: "A6350-Merrion Securities, LLC"
  }, {
    label: "A0980-Mesirow Financial Inc",
    value: "A0980-Mesirow Financial Inc"
  }, {
    label: "A5839-MFS Fund Distributors, Inc.",
    value: "A5839-MFS Fund Distributors, Inc."
  }, {
    label: "A6185-MGO Securities Corp.",
    value: "A6185-MGO Securities Corp."
  }, {
    label: "A5567-Michigan Securities, Inc.",
    value: "A5567-Michigan Securities, Inc."
  }, {
    label: "A1965-Mid Atlantic Capital Corp.",
    value: "A1965-Mid Atlantic Capital Corp."
  }, {
    label: "A3040-Mid-Atlantic Securities, Inc.",
    value: "A3040-Mid-Atlantic Securities, Inc."
  }, {
    label: "A3089-Middlegate Securities, Ltd.",
    value: "A3089-Middlegate Securities, Ltd."
  }, {
    label: "A3509-Midland Securities, Ltd.",
    value: "A3509-Midland Securities, Ltd."
  }, {
    label: "A5508-Midwestern Secs Trading Co LLC",
    value: "A5508-Midwestern Secs Trading Co LLC"
  }, {
    label: "A7334-Millennium Advisors LLC",
    value: "A7334-Millennium Advisors LLC"
  }, {
    label: "A7348-Millington Investments, LLC",
    value: "A7348-Millington Investments, LLC"
  }, {
    label: "A7319-Ministry Partners Securities, LLC",
    value: "A7319-Ministry Partners Securities, LLC"
  }, {
    label: "A7020-MINT Brokers",
    value: "A7020-MINT Brokers"
  }, {
    label: "A7465-Mirae Asset Securities (USA) Inc",
    value: "A7465-Mirae Asset Securities (USA) Inc"
  }, {
    label: "A4652-Mischler Financial Group, Inc.",
    value: "A4652-Mischler Financial Group, Inc."
  }, {
    label: "A7394-Mizuho Securities USA LLC",
    value: "A7394-Mizuho Securities USA LLC"
  }, {
    label: "A5594-MMA Securities LLC",
    value: "A5594-MMA Securities LLC"
  }, {
    label: "A1893-MML Investors Services, LLC",
    value: "A1893-MML Investors Services, LLC"
  }, {
    label: "A5260-Moloney Securities Co., Inc.",
    value: "A5260-Moloney Securities Co., Inc."
  }, {
    label: "A6421-Monarch Capital Group, LLC",
    value: "A6421-Monarch Capital Group, LLC"
  }, {
    label: "A7411-Monere Investments, Inc.",
    value: "A7411-Monere Investments, Inc."
  }, {
    label: "A3326-Money Concepts Capital Corp",
    value: "A3326-Money Concepts Capital Corp"
  }, {
    label: "A1000-Monness, Crespi, Hardt & Co.,",
    value: "A1000-Monness, Crespi, Hardt & Co.,"
  }, {
    label: "A7341-Monroe Financial Partners, Inc.",
    value: "A7341-Monroe Financial Partners, Inc."
  }, {
    label: "A7142-Montage Securities, LLC",
    value: "A7142-Montage Securities, LLC"
  }, {
    label: "A1007-Moors & Cabot, Inc.",
    value: "A1007-Moors & Cabot, Inc."
  }, {
    label: "A7349-Mora WM Securities, LLC",
    value: "A7349-Mora WM Securities, LLC"
  }, {
    label: "A7424-MORETON CAPITAL MARKETS, LLC",
    value: "A7424-MORETON CAPITAL MARKETS, LLC"
  }, {
    label: "A1012-Morgan Stanley & Co. LLC",
    value: "A1012-Morgan Stanley & Co. LLC"
  }, {
    label: "A7007-Morgan Stanley Smith Barney LLC",
    value: "A7007-Morgan Stanley Smith Barney LLC"
  }, {
    label: "A6406-Morgan Wilshire Securities, Inc.",
    value: "A6406-Morgan Wilshire Securities, Inc."
  }, {
    label: "A5906-Morris Group Inc.",
    value: "A5906-Morris Group Inc."
  }, {
    label: "A1288-Morton Seidel & Company, Inc",
    value: "A1288-Morton Seidel & Company, Inc"
  }, {
    label: "A6479-Moss Adams Securities & Insurance LLC",
    value: "A6479-Moss Adams Securities & Insurance LLC"
  }, {
    label: "A5638-MS Howells & Co.",
    value: "A5638-MS Howells & Co."
  }, {
    label: "A6885-MTS Markets International, Inc.",
    value: "A6885-MTS Markets International, Inc."
  }, {
    label: "A7046-MUFG Securities Americas Inc.",
    value: "A7046-MUFG Securities Americas Inc."
  }, {
    label: "A3678-Multi-Bank Securities, Inc.",
    value: "A3678-Multi-Bank Securities, Inc."
  }, {
    label: "A7173-Mundial Financial Group, LLC",
    value: "A7173-Mundial Financial Group, LLC"
  }, {
    label: "A5738-Municipal Capital Markets Grp., Inc.",
    value: "A5738-Municipal Capital Markets Grp., Inc."
  }, {
    label: "A1475-Muriel Siebert & Co., Inc.",
    value: "A1475-Muriel Siebert & Co., Inc."
  }, {
    label: "A6819-Murray Securities, Inc.",
    value: "A6819-Murray Securities, Inc."
  }, {
    label: "A5988-Mutual Funds Associates, Inc.",
    value: "A5988-Mutual Funds Associates, Inc."
  }, {
    label: "A5798-Mutual of Omaha Investor Svcs",
    value: "A5798-Mutual of Omaha Investor Svcs"
  }, {
    label: "A1806-Mutual Securities, Inc.",
    value: "A1806-Mutual Securities, Inc."
  }, {
    label: "A2596-Mutual Trust Co.of America Sec",
    value: "A2596-Mutual Trust Co.of America Sec"
  }, {
    label: "A4870-MV Securities Group, Inc.",
    value: "A4870-MV Securities Group, Inc."
  }, {
    label: "A5820-MWA Financial Services, Inc.",
    value: "A5820-MWA Financial Services, Inc."
  }, {
    label: "A6841-N.E. Private Client Advisors, Ltd.",
    value: "A6841-N.E. Private Client Advisors, Ltd."
  }, {
    label: "A7453-nabSecurities, LLC",
    value: "A7453-nabSecurities, LLC"
  }, {
    label: "A6643-NAFA Capital Markets, LLC",
    value: "A6643-NAFA Capital Markets, LLC"
  }, {
    label: "A4075-Nancy Barron & Associates Inc.",
    value: "A4075-Nancy Barron & Associates Inc."
  }, {
    label: "A7036-Nathan Hale Capital",
    value: "A7036-Nathan Hale Capital"
  }, {
    label: "A5959-National Alliance Securities, LLC",
    value: "A5959-National Alliance Securities, LLC"
  }, {
    label: "A1562-National Financial Services LLC",
    value: "A1562-National Financial Services LLC"
  }, {
    label: "A5231-National Planning Corporation",
    value: "A5231-National Planning Corporation"
  }, {
    label: "A1031-National Securities Corp.",
    value: "A1031-National Securities Corp."
  }, {
    label: "A5177-Nations Financial Group, Inc.",
    value: "A5177-Nations Financial Group, Inc."
  }, {
    label: "A6103-Nationwide Planning Associates, Inc",
    value: "A6103-Nationwide Planning Associates, Inc"
  }, {
    label: "A6153-Nationwide Securities, LLC",
    value: "A6153-Nationwide Securities, LLC"
  }, {
    label: "A7223-Natixis Securities Americas LLC",
    value: "A7223-Natixis Securities Americas LLC"
  }, {
    label: "A5070-Navaid Financial Services,Inc.",
    value: "A5070-Navaid Financial Services,Inc."
  }, {
    label: "A6737-Navy Federal Brokerage Services, LLC",
    value: "A6737-Navy Federal Brokerage Services, LLC"
  }, {
    label: "A4744-NBC Securities, Inc.",
    value: "A4744-NBC Securities, Inc."
  }, {
    label: "A1621-Neidiger, Tucker, Bruner, Inc.",
    value: "A1621-Neidiger, Tucker, Bruner, Inc."
  }, {
    label: "A6944-Neighborly Securities, Inc.",
    value: "A6944-Neighborly Securities, Inc."
  }, {
    label: "A6178-Nelson Invest Brokerage Services, Inc.",
    value: "A6178-Nelson Invest Brokerage Services, Inc."
  }, {
    label: "A5975-Nelson Securities, Inc.",
    value: "A5975-Nelson Securities, Inc."
  }, {
    label: "A1038-Nestlerode & Loy, Inc.",
    value: "A1038-Nestlerode & Loy, Inc."
  }, {
    label: "A1761-Network 1 Financial Securities",
    value: "A1761-Network 1 Financial Securities"
  }, {
    label: "A1039-Neuberger Berman BD LLC",
    value: "A1039-Neuberger Berman BD LLC"
  }, {
    label: "A5833-Newbridge Securities Corp.",
    value: "A5833-Newbridge Securities Corp."
  }, {
    label: "A6400-NewOak Capital Markets LLC",
    value: "A6400-NewOak Capital Markets LLC"
  }, {
    label: "A5384-Next Financial Group, Inc.",
    value: "A5384-Next Financial Group, Inc."
  }, {
    label: "A6868-NGC Financial, LLC",
    value: "A6868-NGC Financial, LLC"
  }, {
    label: "A7118-NI Advisors Inc.",
    value: "A7118-NI Advisors Inc."
  }, {
    label: "A6718-Niagara International Capital Limited",
    value: "A6718-Niagara International Capital Limited"
  }, {
    label: "A6267-Nicol Investors Corporation",
    value: "A6267-Nicol Investors Corporation"
  }, {
    label: "A6702-Nikoh Securities Corporation",
    value: "A6702-Nikoh Securities Corporation"
  }, {
    label: "A7281-NMS Capital Advisors, LLC",
    value: "A7281-NMS Capital Advisors, LLC"
  }, {
    label: "A4017-Noble Capital Markets, Inc.",
    value: "A4017-Noble Capital Markets, Inc."
  }, {
    label: "A3711-North Ridge Securities Corp.",
    value: "A3711-North Ridge Securities Corp."
  }, {
    label: "A6973-North South Capital, LLC",
    value: "A6973-North South Capital, LLC"
  }, {
    label: "A3563-Northeast Securities, Inc.",
    value: "A3563-Northeast Securities, Inc."
  }, {
    label: "A4890-Northern Capital Securities",
    value: "A4890-Northern Capital Securities"
  }, {
    label: "A7368-Northern Lights Distributors, LLC",
    value: "A7368-Northern Lights Distributors, LLC"
  }, {
    label: "A0654-Northern Trust Securities,Inc.",
    value: "A0654-Northern Trust Securities,Inc."
  }, {
    label: "A4861-Northland Securities, Inc.",
    value: "A4861-Northland Securities, Inc."
  }, {
    label: "A6916-Northwest Financial Group, LLC",
    value: "A6916-Northwest Financial Group, LLC"
  }, {
    label: "A6137-Northwest Investment Advisors, Inc.",
    value: "A6137-Northwest Investment Advisors, Inc."
  }, {
    label: "A5864-Northwestern Mutual Invmt Svcs",
    value: "A5864-Northwestern Mutual Invmt Svcs"
  }, {
    label: "A6720-NPB Financial Group, LLC",
    value: "A6720-NPB Financial Group, LLC"
  }, {
    label: "A1062-Nuveen Securities, LLC",
    value: "A1062-Nuveen Securities, LLC"
  }, {
    label: "A2409-NW Capital Markets Inc.",
    value: "A2409-NW Capital Markets Inc."
  }, {
    label: "A5877-NYLIFE Distributors LLC",
    value: "A5877-NYLIFE Distributors LLC"
  }, {
    label: "A4166-NYLife Securities LLC",
    value: "A4166-NYLife Securities LLC"
  }, {
    label: "A6402-NYPPEX, LLC",
    value: "A6402-NYPPEX, LLC"
  }, {
    label: "A6930-OConnor & Company Securities, Inc.",
    value: "A6930-OConnor & Company Securities, Inc."
  }, {
    label: "A3378-O.N. Equity Sales Company",
    value: "A3378-O.N. Equity Sales Company"
  }, {
    label: "A3870-Oak Tree Securities, Inc.",
    value: "A3870-Oak Tree Securities, Inc."
  }, {
    label: "A5021-Oberweis Securities, Inc.",
    value: "A5021-Oberweis Securities, Inc."
  }, {
    label: "A6457-OBEX Securities LLC",
    value: "A6457-OBEX Securities LLC"
  }, {
    label: "A7033-Odeon Capital Group LLC",
    value: "A7033-Odeon Capital Group LLC"
  }, {
    label: "A4154-OFG Financial Services, Inc.",
    value: "A4154-OFG Financial Services, Inc."
  }, {
    label: "A3492-OhanesianLecours, Inc.",
    value: "A3492-OhanesianLecours, Inc."
  }, {
    label: "A1073-Olmsted & Mulhall, Inc.",
    value: "A1073-Olmsted & Mulhall, Inc."
  }, {
    label: "A5882-Omega Securities, Inc.",
    value: "A5882-Omega Securities, Inc."
  }, {
    label: "A5227-Oneamerica Securities, Inc.",
    value: "A5227-Oneamerica Securities, Inc."
  }, {
    label: "A0455-Oppenheimer & Co. Inc.",
    value: "A0455-Oppenheimer & Co. Inc."
  }, {
    label: "A5846-OppenheimerFunds Distributor Inc.",
    value: "A5846-OppenheimerFunds Distributor Inc."
  }, {
    label: "A4113-Oriental Financial Services Corp.",
    value: "A4113-Oriental Financial Services Corp."
  }, {
    label: "A2176-Pacific Investment Secs. Corp.",
    value: "A2176-Pacific Investment Secs. Corp."
  }, {
    label: "A5843-Packerland Brokerage Services",
    value: "A5843-Packerland Brokerage Services"
  }, {
    label: "A1507-Painter, Smith and Amberg Inc.",
    value: "A1507-Painter, Smith and Amberg Inc."
  }, {
    label: "A6730-Papamarkou Wellner & Company, Inc.",
    value: "A6730-Papamarkou Wellner & Company, Inc."
  }, {
    label: "A6117-Paradigm Equities, Inc.",
    value: "A6117-Paradigm Equities, Inc."
  }, {
    label: "A7028-Pariter Securities, LLC",
    value: "A7028-Pariter Securities, LLC"
  }, {
    label: "A5383-Park Avenue Securities",
    value: "A5383-Park Avenue Securities"
  }, {
    label: "A5924-Parkland Securities, LLC",
    value: "A5924-Parkland Securities, LLC"
  }, {
    label: "A6875-Parsonex Securities, Inc.",
    value: "A6875-Parsonex Securities, Inc."
  }, {
    label: "A7428-Paulson Investment LLC",
    value: "A7428-Paulson Investment LLC"
  }, {
    label: "A5784-PCBB Capital Markets, LLC",
    value: "A5784-PCBB Capital Markets, LLC"
  }, {
    label: "A3649-Peachtree Capital Corporation",
    value: "A3649-Peachtree Capital Corporation"
  }, {
    label: "A7188-Peak Brokerage Services, LLC",
    value: "A7188-Peak Brokerage Services, LLC"
  }, {
    label: "A5194-Penates Group, Inc.",
    value: "A5194-Penates Group, Inc."
  }, {
    label: "A3419-Penrod Financial Services, Inc",
    value: "A3419-Penrod Financial Services, Inc"
  }, {
    label: "A6983-Penserra Securities LLC",
    value: "A6983-Penserra Securities LLC"
  }, {
    label: "A7459-PENSIONMARK SECURITIES, LLC.",
    value: "A7459-PENSIONMARK SECURITIES, LLC."
  }, {
    label: "A1692-Peoples Securities, Inc.",
    value: "A1692-Peoples Securities, Inc."
  }, {
    label: "A6735-Percival Financial Partners, Ltd.",
    value: "A6735-Percival Financial Partners, Ltd."
  }, {
    label: "A4481-Performance Trust Capital Partners, LLC",
    value: "A4481-Performance Trust Capital Partners, LLC"
  }, {
    label: "A5989-Perryman Securities, Inc.",
    value: "A5989-Perryman Securities, Inc."
  }, {
    label: "A6341-Pershing Advisor Solutions LLC",
    value: "A6341-Pershing Advisor Solutions LLC"
  }, {
    label: "A0399-Pershing LLC",
    value: "A0399-Pershing LLC"
  }, {
    label: "A5681-Petersen Investments Inc.",
    value: "A5681-Petersen Investments Inc."
  }, {
    label: "A1116-Pflueger & Baerwald, Inc.",
    value: "A1116-Pflueger & Baerwald, Inc."
  }, {
    label: "A5904-PFM Fund Distributors, Inc.",
    value: "A5904-PFM Fund Distributors, Inc."
  }, {
    label: "A5828-PFS Investments Inc.",
    value: "A5828-PFS Investments Inc."
  }, {
    label: "A6171-Philip J. Greenblatt Securities Ltd.",
    value: "A6171-Philip J. Greenblatt Securities Ltd."
  }, {
    label: "A4649-Philips & Company Securities",
    value: "A4649-Philips & Company Securities"
  }, {
    label: "A7227-PHX Financial, Inc.",
    value: "A7227-PHX Financial, Inc."
  }, {
    label: "A7265-Piedmont Securities LLC",
    value: "A7265-Piedmont Securities LLC"
  }, {
    label: "A6918-Pinnacle Investments LLC",
    value: "A6918-Pinnacle Investments LLC"
  }, {
    label: "A1126-Piper Jaffray & Co.",
    value: "A1126-Piper Jaffray & Co."
  }, {
    label: "A6370-Place Trade Financial, Inc.",
    value: "A6370-Place Trade Financial, Inc."
  }, {
    label: "A5799-PlanMember Securities Corp.",
    value: "A5799-PlanMember Securities Corp."
  }, {
    label: "A6377-Planned Financial Programs, Inc.",
    value: "A6377-Planned Financial Programs, Inc."
  }, {
    label: "A1128-Planned Investment Co., Inc.",
    value: "A1128-Planned Investment Co., Inc."
  }, {
    label: "A6513-Planners Financial Services, Inc.",
    value: "A6513-Planners Financial Services, Inc."
  }, {
    label: "A4912-PMA Securities, Inc.",
    value: "A4912-PMA Securities, Inc."
  }, {
    label: "A2091-PNC Capital Markets LLC",
    value: "A2091-PNC Capital Markets LLC"
  }, {
    label: "A6495-PNC Investments LLC",
    value: "A6495-PNC Investments LLC"
  }, {
    label: "A2578-Podesta & Co.",
    value: "A2578-Podesta & Co."
  }, {
    label: "A5489-Polar Investment Counsel, Inc.",
    value: "A5489-Polar Investment Counsel, Inc."
  }, {
    label: "A1555-POPULAR SECURITIES, LLC",
    value: "A1555-POPULAR SECURITIES, LLC"
  }, {
    label: "A5238-Port Securities, Inc.",
    value: "A5238-Port Securities, Inc."
  }, {
    label: "A6097-Portfolio Resources Group, Inc.",
    value: "A6097-Portfolio Resources Group, Inc."
  }, {
    label: "A1723-Portsmouth Financial Services",
    value: "A1723-Portsmouth Financial Services"
  }, {
    label: "A7162-Potomac Investment Co",
    value: "A7162-Potomac Investment Co"
  }, {
    label: "A3877-Powell Capital Markets, Inc.",
    value: "A3877-Powell Capital Markets, Inc."
  }, {
    label: "A0649-PR Herzig & Co., Inc.",
    value: "A0649-PR Herzig & Co., Inc."
  }, {
    label: "A2940-Prager & Co., LLC",
    value: "A2940-Prager & Co., LLC"
  }, {
    label: "A5767-Preferred Client Group, Inc.",
    value: "A5767-Preferred Client Group, Inc."
  }, {
    label: "A3945-Presidential Brokerage, Inc.",
    value: "A3945-Presidential Brokerage, Inc."
  }, {
    label: "A4144-Principal Securities, Inc.",
    value: "A4144-Principal Securities, Inc."
  }, {
    label: "A7023-Private Client Services, LLC",
    value: "A7023-Private Client Services, LLC"
  }, {
    label: "A7087-Private Portfolio of San Diego, Inc.",
    value: "A7087-Private Portfolio of San Diego, Inc."
  }, {
    label: "A2137-ProEquities,Inc",
    value: "A2137-ProEquities,Inc"
  }, {
    label: "A2876-Progressive Asset Management, Inc.",
    value: "A2876-Progressive Asset Management, Inc."
  }, {
    label: "A0137-Prospera Financial Services, Inc.",
    value: "A0137-Prospera Financial Services, Inc."
  }, {
    label: "A5286-Pruco Securities, LLC",
    value: "A5286-Pruco Securities, LLC"
  }, {
    label: "A5937-Prudential Investment Management Services, LLC",
    value: "A5937-Prudential Investment Management Services, LLC"
  }, {
    label: "A5779-PTI Securities & Futures, L.P.",
    value: "A5779-PTI Securities & Futures, L.P."
  }, {
    label: "A6234-PTS Brokerage, LLC",
    value: "A6234-PTS Brokerage, LLC"
  }, {
    label: "A6943-Puritan Brokerage Services, Inc.",
    value: "A6943-Puritan Brokerage Services, Inc."
  }, {
    label: "A4549-Purshe Kaplan Sterling Invs.",
    value: "A4549-Purshe Kaplan Sterling Invs."
  }, {
    label: "A6483-Pursuit Partners, LLC",
    value: "A6483-Pursuit Partners, LLC"
  }, {
    label: "A5665-Putnam Retail Management Limited Partnership",
    value: "A5665-Putnam Retail Management Limited Partnership"
  }, {
    label: "A6882-PWA Securities, Inc.",
    value: "A6882-PWA Securities, Inc."
  }, {
    label: "A4326-PWMCO, LLC",
    value: "A4326-PWMCO, LLC"
  }, {
    label: "A6742-Quayle & Co., Securities",
    value: "A6742-Quayle & Co., Securities"
  }, {
    label: "A5673-Queens Road Securities",
    value: "A5673-Queens Road Securities"
  }, {
    label: "A3190-Quest Capital Strategies, Inc.",
    value: "A3190-Quest Capital Strategies, Inc."
  }, {
    label: "A5475-Questar Capital Corporation",
    value: "A5475-Questar Capital Corporation"
  }, {
    label: "A1157-Quincy Cass Associates, Inc.",
    value: "A1157-Quincy Cass Associates, Inc."
  }, {
    label: "A7474-Quint Capital Corporation",
    value: "A7474-Quint Capital Corporation"
  }, {
    label: "A6847-Quoin Capital LLC",
    value: "A6847-Quoin Capital LLC"
  }, {
    label: "A1887-R Seelaus & Co., Inc.",
    value: "A1887-R Seelaus & Co., Inc."
  }, {
    label: "A3333-R.M. Duncan Securities, Inc.",
    value: "A3333-R.M. Duncan Securities, Inc."
  }, {
    label: "A3380-Rafferty Capital Markets, LLC",
    value: "A3380-Rafferty Capital Markets, LLC"
  }, {
    label: "A6549-Rainier Securities, LLC",
    value: "A6549-Rainier Securities, LLC"
  }, {
    label: "A1172-Raymond James & Assoc., Inc.",
    value: "A1172-Raymond James & Assoc., Inc."
  }, {
    label: "A7269-Raymond James (USA) Ltd.",
    value: "A7269-Raymond James (USA) Ltd."
  }, {
    label: "A0717-Raymond James Financial Svcs. Inc.",
    value: "A0717-Raymond James Financial Svcs. Inc."
  }, {
    label: "A7254-RBC Capital Markets Arbitrage S.A.",
    value: "A7254-RBC Capital Markets Arbitrage S.A."
  }, {
    label: "A4258-RBC Capital Markets LLC",
    value: "A4258-RBC Capital Markets LLC"
  }, {
    label: "A4655-RD Capital Group",
    value: "A4655-RD Capital Group"
  }, {
    label: "A3567-Red Capital Markets, LLC",
    value: "A3567-Red Capital Markets, LLC"
  }, {
    label: "A2014-Regal Securities, Inc.",
    value: "A2014-Regal Securities, Inc."
  }, {
    label: "A4118-Regional Brokers, Inc.",
    value: "A4118-Regional Brokers, Inc."
  }, {
    label: "A4320-Register Financial Associates, Inc.",
    value: "A4320-Register Financial Associates, Inc."
  }, {
    label: "A7338-Regulus Advisors LLC",
    value: "A7338-Regulus Advisors LLC"
  }, {
    label: "A6791-Reliance Worldwide Investments, LLC",
    value: "A6791-Reliance Worldwide Investments, LLC"
  }, {
    label: "A4393-Rensselaer Securities Corp.",
    value: "A4393-Rensselaer Securities Corp."
  }, {
    label: "A1162-Repex & Co., Inc.",
    value: "A1162-Repex & Co., Inc."
  }, {
    label: "A2725-Revere Securities LLC",
    value: "A2725-Revere Securities LLC"
  }, {
    label: "A0862-RF Lafferty and Company",
    value: "A0862-RF Lafferty and Company"
  }, {
    label: "A1600-RH Investment Corporation",
    value: "A1600-RH Investment Corporation"
  }, {
    label: "A2796-Rhodes Securities, Inc.",
    value: "A2796-Rhodes Securities, Inc."
  }, {
    label: "A2966-Rice Securities, LLC",
    value: "A2966-Rice Securities, LLC"
  }, {
    label: "A1193-Richards, Merrill & Peterson, Inc",
    value: "A1193-Richards, Merrill & Peterson, Inc"
  }, {
    label: "A4199-Riedl First Securities Co. of Kansas",
    value: "A4199-Riedl First Securities Co. of Kansas"
  }, {
    label: "A2781-RIM Securities LLC",
    value: "A2781-RIM Securities LLC"
  }, {
    label: "A1196-RJJ Pasadena Securities, Inc.",
    value: "A1196-RJJ Pasadena Securities, Inc."
  }, {
    label: "A4590-RM Stark & Co.",
    value: "A4590-RM Stark & Co."
  }, {
    label: "A6002-RNR Securities, L.L.C.",
    value: "A6002-RNR Securities, L.L.C."
  }, {
    label: "A4721-Robert Blum Municipals, Inc.",
    value: "A4721-Robert Blum Municipals, Inc."
  }, {
    label: "A3991-Robert R Meredith & Co., Inc.",
    value: "A3991-Robert R Meredith & Co., Inc."
  }, {
    label: "A0079-Robert W Baird & Co., Inc.",
    value: "A0079-Robert W Baird & Co., Inc."
  }, {
    label: "A2790-Roberts & Ryan Investments Inc",
    value: "A2790-Roberts & Ryan Investments Inc"
  }, {
    label: "A1205-Robinson & Robinson Inc.",
    value: "A1205-Robinson & Robinson Inc."
  }, {
    label: "A6991-Rockfleet Financial Services, Inc.",
    value: "A6991-Rockfleet Financial Services, Inc."
  }, {
    label: "A2148-Rodgers Brothers, Inc.",
    value: "A2148-Rodgers Brothers, Inc."
  }, {
    label: "A6032-Rogan & Associates, Inc.",
    value: "A6032-Rogan & Associates, Inc."
  }, {
    label: "A1214-Romano Brothers & Company",
    value: "A1214-Romano Brothers & Company"
  }, {
    label: "A1218-Roosevelt & Cross Incorporated",
    value: "A1218-Roosevelt & Cross Incorporated"
  }, {
    label: "A3543-Ross, Sinclaire & Associates, LLC",
    value: "A3543-Ross, Sinclaire & Associates, LLC"
  }, {
    label: "A1231-Rothschild Investment Corp.",
    value: "A1231-Rothschild Investment Corp."
  }, {
    label: "A3249-Royal Alliance Associates Inc.",
    value: "A3249-Royal Alliance Associates Inc."
  }, {
    label: "A6238-Russell Investments Implementation Services, LLC.",
    value: "A6238-Russell Investments Implementation Services, LLC."
  }, {
    label: "A3942-RW Pressprich & Co.",
    value: "A3942-RW Pressprich & Co."
  }, {
    label: "A2246-RW Smith & Associates, LLC",
    value: "A2246-RW Smith & Associates, LLC"
  }, {
    label: "A4827-Ryan Financial, Inc.",
    value: "A4827-Ryan Financial, Inc."
  }, {
    label: "A6330-S.L. Reed & Company",
    value: "A6330-S.L. Reed & Company"
  }, {
    label: "A2689-SA Stone Wealth Management, Inc",
    value: "A2689-SA Stone Wealth Management, Inc"
  }, {
    label: "A6970-Sabadell Securities USA, Inc.",
    value: "A6970-Sabadell Securities USA, Inc."
  }, {
    label: "A7350-Safra Securities LLC",
    value: "A7350-Safra Securities LLC"
  }, {
    label: "A1249-Sage, Rutty & Co., Inc.",
    value: "A1249-Sage, Rutty & Co., Inc."
  }, {
    label: "A6645-Sagepoint Financial, Inc.",
    value: "A6645-Sagepoint Financial, Inc."
  }, {
    label: "A7280-Salomon Whitney Financial",
    value: "A7280-Salomon Whitney Financial"
  }, {
    label: "A6695-SAMCO CAPITAL MARKETS, INC.",
    value: "A6695-SAMCO CAPITAL MARKETS, INC."
  }, {
    label: "A1165-SAMUEL A. RAMIREZ & COMPANY INC.",
    value: "A1165-SAMUEL A. RAMIREZ & COMPANY INC."
  }, {
    label: "A5732-Sanderlin Securities LLC",
    value: "A5732-Sanderlin Securities LLC"
  }, {
    label: "A2908-Sanders Morris Harris LLC",
    value: "A2908-Sanders Morris Harris LLC"
  }, {
    label: "A7325-SANDLAPPER Securities LLC",
    value: "A7325-SANDLAPPER Securities LLC"
  }, {
    label: "A4361-Sandler ONeill & Partners, LP",
    value: "A4361-Sandler ONeill & Partners, LP"
  }, {
    label: "A0125-Sanford C Bernstein & Co.,LLC",
    value: "A0125-Sanford C Bernstein & Co.,LLC"
  }, {
    label: "A4973-Santander Securities LLC",
    value: "A4973-Santander Securities LLC"
  }, {
    label: "A5215-Saperston Asset Mngmt., Inc.",
    value: "A5215-Saperston Asset Mngmt., Inc."
  }, {
    label: "A2625-Saturna Brokerage Services, Inc.",
    value: "A2625-Saturna Brokerage Services, Inc."
  }, {
    label: "A3653-Sauer, Dazey Investment Co.",
    value: "A3653-Sauer, Dazey Investment Co."
  }, {
    label: "A5916-Saxony Securities, Inc.",
    value: "A5916-Saxony Securities, Inc."
  }, {
    label: "A5965-SCF Securities, Inc.",
    value: "A5965-SCF Securities, Inc."
  }, {
    label: "A6961-SCH Enterprises, Inc.",
    value: "A6961-SCH Enterprises, Inc."
  }, {
    label: "A3346-Schlitt Investor Services, Inc",
    value: "A3346-Schlitt Investor Services, Inc"
  }, {
    label: "A5740-Scott James Group, Inc.",
    value: "A5740-Scott James Group, Inc."
  }, {
    label: "A5546-Scottrade Inc",
    value: "A5546-Scottrade Inc"
  }, {
    label: "A5991-Scottsdale Capital Advisors",
    value: "A5991-Scottsdale Capital Advisors"
  }, {
    label: "A3533-Scully Capital Securities Corp.",
    value: "A3533-Scully Capital Securities Corp."
  }, {
    label: "A5940-Seaport Global Securities LLC",
    value: "A5940-Seaport Global Securities LLC"
  }, {
    label: "A2027-Seaport Securities Corporation",
    value: "A2027-Seaport Securities Corporation"
  }, {
    label: "A1776-Searle & Co.",
    value: "A1776-Searle & Co."
  }, {
    label: "A6969-SECU Brokerage Services",
    value: "A6969-SECU Brokerage Services"
  }, {
    label: "A6456-Secure Planning, Inc.",
    value: "A6456-Secure Planning, Inc."
  }, {
    label: "A0998-SecureVest Financial Group, Inc.",
    value: "A0998-SecureVest Financial Group, Inc."
  }, {
    label: "A3800-Securian Financial Svcs., Inc.",
    value: "A3800-Securian Financial Svcs., Inc."
  }, {
    label: "A3215-Securities America, Inc.",
    value: "A3215-Securities America, Inc."
  }, {
    label: "A4605-Securities Capital Corporation",
    value: "A4605-Securities Capital Corporation"
  }, {
    label: "A5444-Securities Equity Group",
    value: "A5444-Securities Equity Group"
  }, {
    label: "A6288-Securities Management Research, Inc.",
    value: "A6288-Securities Management Research, Inc."
  }, {
    label: "A0623-Securities Research, Inc.",
    value: "A0623-Securities Research, Inc."
  }, {
    label: "A3497-Securities Service Network,Inc",
    value: "A3497-Securities Service Network,Inc"
  }, {
    label: "A7241-Security Capital Brokerage, Inc.",
    value: "A7241-Security Capital Brokerage, Inc."
  }, {
    label: "A5185-SEI Investments Distribution Co",
    value: "A5185-SEI Investments Distribution Co"
  }, {
    label: "A2055-Selkirk Investments, Inc.",
    value: "A2055-Selkirk Investments, Inc."
  }, {
    label: "A7470-Senate Securities",
    value: "A7470-Senate Securities"
  }, {
    label: "A6926-Sendero Securities, LLC",
    value: "A6926-Sendero Securities, LLC"
  }, {
    label: "A4887-Sentinel Brokers Company",
    value: "A4887-Sentinel Brokers Company"
  }, {
    label: "A6013-Sentinel Securities, Inc.",
    value: "A6013-Sentinel Securities, Inc."
  }, {
    label: "A7463-Sentinus Securities, LLC",
    value: "A7463-Sentinus Securities, LLC"
  }, {
    label: "A5290-Sequoia Investments, Inc.",
    value: "A5290-Sequoia Investments, Inc."
  }, {
    label: "A4230-Seslia Securities",
    value: "A4230-Seslia Securities"
  }, {
    label: "A1330-SF Investments, Inc.",
    value: "A1330-SF Investments, Inc."
  }, {
    label: "A3771-SF Sentry Securities, Inc.",
    value: "A3771-SF Sentry Securities, Inc."
  }, {
    label: "A7108-SFA Financial, LLC",
    value: "A7108-SFA Financial, LLC"
  }, {
    label: "K0674-SG Americas Securities, LLC",
    value: "K0674-SG Americas Securities, LLC"
  }, {
    label: "A5476-SG Long & Company",
    value: "A5476-SG Long & Company"
  }, {
    label: "A6333-Shareholders Service Group, Inc.",
    value: "A6333-Shareholders Service Group, Inc."
  }, {
    label: "A1466-Shearman, Ralston Inc.",
    value: "A1466-Shearman, Ralston Inc."
  }, {
    label: "A4749-Shearson Financial Services LLC",
    value: "A4749-Shearson Financial Services LLC"
  }, {
    label: "A5074-Siebert Cisneros Shank & Co., L.L.C.",
    value: "A5074-Siebert Cisneros Shank & Co., L.L.C."
  }, {
    label: "A7137-Sierra Pacific Securities, LLC",
    value: "A7137-Sierra Pacific Securities, LLC"
  }, {
    label: "A4441-SIGMA Financial Corporation",
    value: "A4441-SIGMA Financial Corporation"
  }, {
    label: "A3373-Signal Securities, Inc.",
    value: "A3373-Signal Securities, Inc."
  }, {
    label: "A4437-Signator Investors Inc.",
    value: "A4437-Signator Investors Inc."
  }, {
    label: "A5671-Signature Securities Group Corp",
    value: "A5671-Signature Securities Group Corp"
  }, {
    label: "A7122-Signet Securities, LLC",
    value: "A7122-Signet Securities, LLC"
  }, {
    label: "A1662-SII Investments, Inc.",
    value: "A1662-SII Investments, Inc."
  }, {
    label: "A7249-Silber Bennett Financial, Inc.",
    value: "A7249-Silber Bennett Financial, Inc."
  }, {
    label: "A5446-Silver Oak Securities, Incorporated",
    value: "A5446-Silver Oak Securities, Incorporated"
  }, {
    label: "A5457-Simmons First Investment Group, Inc.",
    value: "A5457-Simmons First Investment Group, Inc."
  }, {
    label: "A3587-Sisung Securities Corporation",
    value: "A3587-Sisung Securities Corporation"
  }, {
    label: "A6094-SKA Securities, Inc.",
    value: "A6094-SKA Securities, Inc."
  }, {
    label: "A4418-SMBC Nikko Securities America, Inc.",
    value: "A4418-SMBC Nikko Securities America, Inc."
  }, {
    label: "A0193-Smith, Brown and Groover, Inc.",
    value: "A0193-Smith, Brown and Groover, Inc."
  }, {
    label: "A1492-Smith, Moore and Company",
    value: "A1492-Smith, Moore and Company"
  }, {
    label: "A7307-Snowden Account Services",
    value: "A7307-Snowden Account Services"
  }, {
    label: "A6449-Sorrento Pacific Financial, LLC",
    value: "A6449-Sorrento Pacific Financial, LLC"
  }, {
    label: "A4911-Sorsby Financial Corp.",
    value: "A4911-Sorsby Financial Corp."
  }, {
    label: "A5111-Southeast Investments, N.C. Inc.",
    value: "A5111-Southeast Investments, N.C. Inc."
  }, {
    label: "A6404-Southern Trust Securities, Inc.",
    value: "A6404-Southern Trust Securities, Inc."
  }, {
    label: "A7429-Sovereign Global Advisors, LLC",
    value: "A7429-Sovereign Global Advisors, LLC"
  }, {
    label: "A6901-Spartan Capital Securities, LLC",
    value: "A6901-Spartan Capital Securities, LLC"
  }, {
    label: "A4243-Spencer-Winston Securities Co.",
    value: "A4243-Spencer-Winston Securities Co."
  }, {
    label: "A6862-Spire Securities, LLC",
    value: "A6862-Spire Securities, LLC"
  }, {
    label: "A4565-St. Bernard Financial Services, Inc.",
    value: "A4565-St. Bernard Financial Services, Inc."
  }, {
    label: "A6879-St. Germain Securities Inc.",
    value: "A6879-St. Germain Securities Inc."
  }, {
    label: "A4760-Stanford Group Company",
    value: "A4760-Stanford Group Company"
  }, {
    label: "A6319-Stannard Financial Services, LLC",
    value: "A6319-Stannard Financial Services, LLC"
  }, {
    label: "A3528-Stark Municipal Brokers",
    value: "A3528-Stark Municipal Brokers"
  }, {
    label: "A3857-Starshak Winzenburg & Co",
    value: "A3857-Starshak Winzenburg & Co"
  }, {
    label: "A6386-State Farm VP Management Corp.",
    value: "A6386-State Farm VP Management Corp."
  }, {
    label: "B0503-State Street Bank and Trust Company",
    value: "B0503-State Street Bank and Trust Company"
  }, {
    label: "A7460-State Street Global Markets, LLC",
    value: "A7460-State Street Global Markets, LLC"
  }, {
    label: "A5865-StateTrust Investments, Inc.",
    value: "A5865-StateTrust Investments, Inc."
  }, {
    label: "A4908-Stephen A Kohn & Associates",
    value: "A4908-Stephen A Kohn & Associates"
  }, {
    label: "A1303-Stephens Inc.",
    value: "A1303-Stephens Inc."
  }, {
    label: "B0464-Sterling Investments, a division of Sterling Bank",
    value: "B0464-Sterling Investments, a division of Sterling Bank"
  }, {
    label: "A6374-Sterling Monroe Securities, LLC",
    value: "A6374-Sterling Monroe Securities, LLC"
  }, {
    label: "A1306-Sterling, Grace Municipal Sec.",
    value: "A1306-Sterling, Grace Municipal Sec."
  }, {
    label: "A2178-Stern Brothers & Co.",
    value: "A2178-Stern Brothers & Co."
  }, {
    label: "A1313-Stifel, Nicolaus & Company,Inc",
    value: "A1313-Stifel, Nicolaus & Company,Inc"
  }, {
    label: "A5902-Stinson Securities, LLC",
    value: "A5902-Stinson Securities, LLC"
  }, {
    label: "A6070-StockCross Financial Services, Inc.",
    value: "A6070-StockCross Financial Services, Inc."
  }, {
    label: "A1319-Stoever Glass & Company, Inc.",
    value: "A1319-Stoever Glass & Company, Inc."
  }, {
    label: "A7042-StoneCastle Securities, LLC",
    value: "A7042-StoneCastle Securities, LLC"
  }, {
    label: "A7441-STONECREST CAPITAL MARKETS, INC.",
    value: "A7441-STONECREST CAPITAL MARKETS, INC."
  }, {
    label: "A5214-Stonewall Investments, Inc.",
    value: "A5214-Stonewall Investments, Inc."
  }, {
    label: "A6666-Stonnington Group, LLC",
    value: "A6666-Stonnington Group, LLC"
  }, {
    label: "A7095-StormHarbour Securities LP",
    value: "A7095-StormHarbour Securities LP"
  }, {
    label: "A4369-Strategic Alliance Corporation",
    value: "A4369-Strategic Alliance Corporation"
  }, {
    label: "A5953-Sturdivant & Co., Inc.",
    value: "A5953-Sturdivant & Co., Inc."
  }, {
    label: "A2359-Sturges Company (The)",
    value: "A2359-Sturges Company (The)"
  }, {
    label: "A5720-Summit Brokerage Services, Inc",
    value: "A5720-Summit Brokerage Services, Inc"
  }, {
    label: "A4146-Summit Equities, Inc.",
    value: "A4146-Summit Equities, Inc."
  }, {
    label: "A7126-Sumridge Partners, LLC",
    value: "A7126-Sumridge Partners, LLC"
  }, {
    label: "A6587-Sunbelt Securities, Inc.",
    value: "A6587-Sunbelt Securities, Inc."
  }, {
    label: "A6834-Sunstreet Securities, LLC",
    value: "A6834-Sunstreet Securities, LLC"
  }, {
    label: "B0512-SunTrust Bank, Municipal Securities Division",
    value: "B0512-SunTrust Bank, Municipal Securities Division"
  }, {
    label: "A2748-SunTrust Investment Services, Inc.",
    value: "A2748-SunTrust Investment Services, Inc."
  }, {
    label: "A0438-SunTrust Robinson Humphrey, Inc.",
    value: "A0438-SunTrust Robinson Humphrey, Inc."
  }, {
    label: "A5647-Superior Financial Services, Inc.",
    value: "A5647-Superior Financial Services, Inc."
  }, {
    label: "A6739-Susquehanna Financial Group, LLLP",
    value: "A6739-Susquehanna Financial Group, LLLP"
  }, {
    label: "A6622-SWBC Investment Services, LLC",
    value: "A6622-SWBC Investment Services, LLC"
  }, {
    label: "A1335-Sweney Cartwright & Co.",
    value: "A1335-Sweney Cartwright & Co."
  }, {
    label: "A1768-Sycamore Financial Group",
    value: "A1768-Sycamore Financial Group"
  }, {
    label: "A6839-Symphonic Securities, LLC",
    value: "A6839-Symphonic Securities, LLC"
  }, {
    label: "A4458-Syndicated Capital, Inc.",
    value: "A4458-Syndicated Capital, Inc."
  }, {
    label: "A1715-Synovus Securities Inc.",
    value: "A1715-Synovus Securities Inc."
  }, {
    label: "A2602-T Rowe Price Investment Serv.",
    value: "A2602-T Rowe Price Investment Serv."
  }, {
    label: "A7405-T.E. Laird Securities, LLC",
    value: "A7405-T.E. Laird Securities, LLC"
  }, {
    label: "A1573-T.R. Winston & Company, LLC",
    value: "A1573-T.R. Winston & Company, LLC"
  }, {
    label: "A6302-T.S. Phillips Investments, Inc.",
    value: "A6302-T.S. Phillips Investments, Inc."
  }, {
    label: "A7194-TAG Wealth Management, Inc.",
    value: "A7194-TAG Wealth Management, Inc."
  }, {
    label: "A6321-Taglich Brothers, Inc.",
    value: "A6321-Taglich Brothers, Inc."
  }, {
    label: "A6988-Tandem Securities, Inc.",
    value: "A6988-Tandem Securities, Inc."
  }, {
    label: "A6123-Tavenner Company, (The)",
    value: "A6123-Tavenner Company, (The)"
  }, {
    label: "A7197-Taylor Capital Management, Inc.,",
    value: "A7197-Taylor Capital Management, Inc.,"
  }, {
    label: "A3674-Taylor Securities, Inc.",
    value: "A3674-Taylor Securities, Inc."
  }, {
    label: "A5667-TCAdvisors Network, Inc.",
    value: "A5667-TCAdvisors Network, Inc."
  }, {
    label: "A7268-TCFG Wealth Management, LLC",
    value: "A7268-TCFG Wealth Management, LLC"
  }, {
    label: "A0498-TD AMERITRADE Clearing, Inc.",
    value: "A0498-TD AMERITRADE Clearing, Inc."
  }, {
    label: "A1432-TD Ameritrade, Inc.",
    value: "A1432-TD Ameritrade, Inc."
  }, {
    label: "A7285-TD Private Client Wealth LLC",
    value: "A7285-TD Private Client Wealth LLC"
  }, {
    label: "A6962-TD Securities (USA) LLC",
    value: "A6962-TD Securities (USA) LLC"
  }, {
    label: "A4768-Teckmeyer Financial Services",
    value: "A4768-Teckmeyer Financial Services"
  }, {
    label: "A7403-Telsey Advisory Group LLC",
    value: "A7403-Telsey Advisory Group LLC"
  }, {
    label: "A3684-TFS Securities, Inc.",
    value: "A3684-TFS Securities, Inc."
  }, {
    label: "A5121-The Benchmark Company, LLC",
    value: "A5121-The Benchmark Company, LLC"
  }, {
    label: "A6086-The Capital Group Securities, Inc.",
    value: "A6086-The Capital Group Securities, Inc."
  }, {
    label: "A3650-The Garbacz Group ,Inc.",
    value: "A3650-The Garbacz Group ,Inc."
  }, {
    label: "A1006-The GMS Group, LLC",
    value: "A1006-The GMS Group, LLC"
  }, {
    label: "A4990-The Jeffrey Matthews Fin. Grp.",
    value: "A4990-The Jeffrey Matthews Fin. Grp."
  }, {
    label: "A5930-The Leaders Group, Inc.",
    value: "A5930-The Leaders Group, Inc."
  }, {
    label: "A5073-The Oak Ridge Finl Serv. Group, Inc.",
    value: "A5073-The Oak Ridge Finl Serv. Group, Inc."
  }, {
    label: "A5639-The Rockwell Financial Group Inc,",
    value: "A5639-The Rockwell Financial Group Inc,"
  }, {
    label: "A6415-The Strategic Financial Alliance, Inc.",
    value: "A6415-The Strategic Financial Alliance, Inc."
  }, {
    label: "A6322-The Williams Capital Group, L.P.",
    value: "A6322-The Williams Capital Group, L.P."
  }, {
    label: "A7302-Third Party Trade LLC",
    value: "A7302-Third Party Trade LLC"
  }, {
    label: "A6101-THOMPSON DAVIS &CO. INC..",
    value: "A6101-THOMPSON DAVIS &CO. INC.."
  }, {
    label: "A3081-Thornhill Securities Inc.",
    value: "A3081-Thornhill Securities Inc."
  }, {
    label: "A1357-Thornton Farish Inc.",
    value: "A1357-Thornton Farish Inc."
  }, {
    label: "A5497-Thoroughbred Financial Services, LLC",
    value: "A5497-Thoroughbred Financial Services, LLC"
  }, {
    label: "A6652-Thrasher and Chambers, Inc",
    value: "A6652-Thrasher and Chambers, Inc"
  }, {
    label: "A5034-Thrivent Investment Management, Inc.",
    value: "A5034-Thrivent Investment Management, Inc."
  }, {
    label: "A1355-Thurston,Springer,Miller,Herd & Titak, Inc.",
    value: "A1355-Thurston,Springer,Miller,Herd & Titak, Inc."
  }, {
    label: "A5341-TIAA -CREF Indivdl Instl Svcs, LLC",
    value: "A5341-TIAA -CREF Indivdl Instl Svcs, LLC"
  }, {
    label: "B0270-TIB The Independent BankersBank, National Association",
    value: "B0270-TIB The Independent BankersBank, National Association"
  }, {
    label: "A7385-TIGRESS FINANCIAL PARTNERS LLC",
    value: "A7385-TIGRESS FINANCIAL PARTNERS LLC"
  }, {
    label: "A1358-Timecapital Securities Corp.",
    value: "A1358-Timecapital Securities Corp."
  }, {
    label: "A6723-Titan Securities",
    value: "A6723-Titan Securities"
  }, {
    label: "A6438-Titleist Asset Management, Ltd.",
    value: "A6438-Titleist Asset Management, Ltd."
  }, {
    label: "A5736-TMC Bonds, L.L.C.",
    value: "A5736-TMC Bonds, L.L.C."
  }, {
    label: "A3638-Tocqueville Securities L.P.",
    value: "A3638-Tocqueville Securities L.P."
  }, {
    label: "A6624-Toussaint Capital Partners, LLC",
    value: "A6624-Toussaint Capital Partners, LLC"
  }, {
    label: "A5402-Trade-PMR, Inc.",
    value: "A5402-Trade-PMR, Inc."
  }, {
    label: "A6803-TradeKing Securities, LLC",
    value: "A6803-TradeKing Securities, LLC"
  }, {
    label: "A4971-TradeStation Securities",
    value: "A4971-TradeStation Securities"
  }, {
    label: "A5773-Tradeweb Direct LLC",
    value: "A5773-Tradeweb Direct LLC"
  }, {
    label: "A6797-Tradeweb LLC",
    value: "A6797-Tradeweb LLC"
  }, {
    label: "A5644-Tradition Securities and Derivatives Inc",
    value: "A5644-Tradition Securities and Derivatives Inc"
  }, {
    label: "A6020-TransAm Securities, Inc.",
    value: "A6020-TransAm Securities, Inc."
  }, {
    label: "A3831-Transamerica Financial Advisors, Inc.",
    value: "A3831-Transamerica Financial Advisors, Inc."
  }, {
    label: "A7027-Transwestern Capital Markets, LLC",
    value: "A7027-Transwestern Capital Markets, LLC"
  }, {
    label: "A4165-Triad Advisors, Inc.",
    value: "A4165-Triad Advisors, Inc."
  }, {
    label: "A7230-Triangle Securities LLC",
    value: "A7230-Triangle Securities LLC"
  }, {
    label: "A7406-Tribal Capital Markets, LLC",
    value: "A7406-Tribal Capital Markets, LLC"
  }, {
    label: "A5689-Trident Partners Ltd.",
    value: "A5689-Trident Partners Ltd."
  }, {
    label: "A1375-Trubee, Collins & Co., Inc.",
    value: "A1375-Trubee, Collins & Co., Inc."
  }, {
    label: "A2431-TrustCore Investments, Inc.",
    value: "A2431-TrustCore Investments, Inc."
  }, {
    label: "A4764-TrustFirst, Inc.",
    value: "A4764-TrustFirst, Inc."
  }, {
    label: "A3315-Trustmont Financial Group, Inc.",
    value: "A3315-Trustmont Financial Group, Inc."
  }, {
    label: "A5649-Tryco Securities, Inc",
    value: "A5649-Tryco Securities, Inc"
  }, {
    label: "A7058-Tullett Prebon Financial Services LLC",
    value: "A7058-Tullett Prebon Financial Services LLC"
  }, {
    label: "A2159-Twenty-First Securities Corp.",
    value: "A2159-Twenty-First Securities Corp."
  }, {
    label: "B0475-U.S. Bank Municipal Products Group",
    value: "B0475-U.S. Bank Municipal Products Group"
  }, {
    label: "A5836-U.S. Brokerage, Inc.",
    value: "A5836-U.S. Brokerage, Inc."
  }, {
    label: "A6436-U.S. Investors, Inc.",
    value: "A6436-U.S. Investors, Inc."
  }, {
    label: "A1689-UBS Financial Serives Inc. of Puerto Rico",
    value: "A1689-UBS Financial Serives Inc. of Puerto Rico"
  }, {
    label: "A1091-UBS Financial Services",
    value: "A1091-UBS Financial Services"
  }, {
    label: "A1381-UBS Securities LLC",
    value: "A1381-UBS Securities LLC"
  }, {
    label: "A5142-Uhlmann Price Securities, LLC",
    value: "A5142-Uhlmann Price Securities, LLC"
  }, {
    label: "A6025-UHY ADVISORS CORPORATE FINANCE, LLC",
    value: "A6025-UHY ADVISORS CORPORATE FINANCE, LLC"
  }, {
    label: "B0286-UMB Bank, n.a.",
    value: "B0286-UMB Bank, n.a."
  }, {
    label: "A4589-UMB Financial Services, Inc.",
    value: "A4589-UMB Financial Services, Inc."
  }, {
    label: "A0149-Umpqua Investments, Inc.",
    value: "A0149-Umpqua Investments, Inc."
  }, {
    label: "A6134-Union Capital Company",
    value: "A6134-Union Capital Company"
  }, {
    label: "A1847-UnionBanc Investment Services, LLC",
    value: "A1847-UnionBanc Investment Services, LLC"
  }, {
    label: "B0171-United Bankers Bank",
    value: "B0171-United Bankers Bank"
  }, {
    label: "A4786-United Brokerage Services,Inc.",
    value: "A4786-United Brokerage Services,Inc."
  }, {
    label: "A2873-United PlannersFin.Svc.of Ame",
    value: "A2873-United PlannersFin.Svc.of Ame"
  }, {
    label: "A5468-Univest Investments, Inc.",
    value: "A5468-Univest Investments, Inc."
  }, {
    label: "A3500-US Bancorp Investments, Inc.",
    value: "A3500-US Bancorp Investments, Inc."
  }, {
    label: "A5786-US Boston Capital Corporation",
    value: "A5786-US Boston Capital Corporation"
  }, {
    label: "A3712-US Securities Intl, Corp.",
    value: "A3712-US Securities Intl, Corp."
  }, {
    label: "A4467-US Sterling Securities, Inc.",
    value: "A4467-US Sterling Securities, Inc."
  }, {
    label: "A5759-USA Financial Securities Corporation",
    value: "A5759-USA Financial Securities Corporation"
  }, {
    label: "A6688-USAA Financial Advisors, Inc.",
    value: "A6688-USAA Financial Advisors, Inc."
  }, {
    label: "A1711-USAA Investment Management Company",
    value: "A1711-USAA Investment Management Company"
  }, {
    label: "A5571-USCA Securities LLC",
    value: "A5571-USCA Securities LLC"
  }, {
    label: "A5246-USI Securities, Inc.",
    value: "A5246-USI Securities, Inc."
  }, {
    label: "A4637-Valdes & Moreno, Inc.",
    value: "A4637-Valdes & Moreno, Inc."
  }, {
    label: "A5652-VALIC Financial Advisors, Inc",
    value: "A5652-VALIC Financial Advisors, Inc"
  }, {
    label: "A2742-Valley National Investments",
    value: "A2742-Valley National Investments"
  }, {
    label: "A5223-ValMark Securities, Inc.",
    value: "A5223-ValMark Securities, Inc."
  }, {
    label: "A7039-Valor Financial Securities LLC",
    value: "A7039-Valor Financial Securities LLC"
  }, {
    label: "A1194-Vanderbilt Securities, LLC",
    value: "A1194-Vanderbilt Securities, LLC"
  }, {
    label: "A3104-Vanguard Marketing Corporation",
    value: "A3104-Vanguard Marketing Corporation"
  }, {
    label: "A2650-Vaughan and Company Securities",
    value: "A2650-Vaughan and Company Securities"
  }, {
    label: "A1399-VBC Securities LLC",
    value: "A1399-VBC Securities LLC"
  }, {
    label: "A6551-VCA Securities, LP",
    value: "A6551-VCA Securities, LP"
  }, {
    label: "A7044-VectorGlobal WMG",
    value: "A7044-VectorGlobal WMG"
  }, {
    label: "A7473-Veritas Independent Partners LLC",
    value: "A7473-Veritas Independent Partners LLC"
  }, {
    label: "A5755-Verity Investments, Inc.",
    value: "A5755-Verity Investments, Inc."
  }, {
    label: "A6210-Vestech Securities, Inc.",
    value: "A6210-Vestech Securities, Inc."
  }, {
    label: "A3853-Vestor Capital Securities, LLC",
    value: "A3853-Vestor Capital Securities, LLC"
  }, {
    label: "A7143-VFG Securities, Inc.",
    value: "A7143-VFG Securities, Inc."
  }, {
    label: "A5253-Vfinance Investments, Inc.",
    value: "A5253-Vfinance Investments, Inc."
  }, {
    label: "A7456-ViewTrade Securities, Inc.",
    value: "A7456-ViewTrade Securities, Inc."
  }, {
    label: "A3792-Vining Sparks",
    value: "A3792-Vining Sparks"
  }, {
    label: "A7128-Virtu Americas LLC",
    value: "A7128-Virtu Americas LLC"
  }, {
    label: "A5803-VISION",
    value: "A5803-VISION"
  }, {
    label: "A6815-Vision Financial Markets LLC",
    value: "A6815-Vision Financial Markets LLC"
  }, {
    label: "A7466-VISIONS ANALYSIS LLC",
    value: "A7466-VISIONS ANALYSIS LLC"
  }, {
    label: "A5299-Vista Securities, Inc.",
    value: "A5299-Vista Securities, Inc."
  }, {
    label: "A0923-VM Manning & Co.",
    value: "A0923-VM Manning & Co."
  }, {
    label: "A5421-Vorpahl Wing Securities",
    value: "A5421-Vorpahl Wing Securities"
  }, {
    label: "A3389-Voya Financial Advisors, Inc.",
    value: "A3389-Voya Financial Advisors, Inc."
  }, {
    label: "A7201-VOYA INVESTMENTS DISTRIBUTOR, LLC",
    value: "A7201-VOYA INVESTMENTS DISTRIBUTOR, LLC"
  }, {
    label: "A7380-W&S Brokerage Services, Inc.",
    value: "A7380-W&S Brokerage Services, Inc."
  }, {
    label: "A6243-W.H. Colson Securities, Inc.",
    value: "A6243-W.H. Colson Securities, Inc."
  }, {
    label: "A6349-Waddell & Reed, Inc.",
    value: "A6349-Waddell & Reed, Inc."
  }, {
    label: "A1418-Wall Street Access",
    value: "A1418-Wall Street Access"
  }, {
    label: "A7291-Wallachbeth Capital, llc",
    value: "A7291-Wallachbeth Capital, llc"
  }, {
    label: "A5397-Washington Securities Corp.",
    value: "A5397-Washington Securities Corp."
  }, {
    label: "A0692-Wayne Hummer Investments LLC",
    value: "A0692-Wayne Hummer Investments LLC"
  }, {
    label: "A5984-WBB Securities, LLC",
    value: "A5984-WBB Securities, LLC"
  }, {
    label: "A6534-Wealth Enhancement Brokerage Services, LLC",
    value: "A6534-Wealth Enhancement Brokerage Services, LLC"
  }, {
    label: "A7401-Wealthfront Brokerage Corporation",
    value: "A7401-Wealthfront Brokerage Corporation"
  }, {
    label: "A1436-Wedbush Securities Inc.",
    value: "A1436-Wedbush Securities Inc."
  }, {
    label: "A7037-Weeden & Co., L.P.",
    value: "A7037-Weeden & Co., L.P."
  }, {
    label: "A7345-Weitzel Financial Services, Inc.",
    value: "A7345-Weitzel Financial Services, Inc."
  }, {
    label: "A7053-Wellington Shields & Co., LLC",
    value: "A7053-Wellington Shields & Co., LLC"
  }, {
    label: "A2353-Wells Fargo Advisors Financial Network, LLC",
    value: "A2353-Wells Fargo Advisors Financial Network, LLC"
  }, {
    label: "B0474-Wells Fargo Bank, N.A. Municipal Products Group",
    value: "B0474-Wells Fargo Bank, N.A. Municipal Products Group"
  }, {
    label: "A3741-Wells Fargo Clearing Services, LLC",
    value: "A3741-Wells Fargo Clearing Services, LLC"
  }, {
    label: "A6366-Wells Fargo Securities, LLC",
    value: "A6366-Wells Fargo Securities, LLC"
  }, {
    label: "A5646-Wells Nelson & Associates, LLC",
    value: "A5646-Wells Nelson & Associates, LLC"
  }, {
    label: "A5098-WesBanco Securities, Inc.",
    value: "A5098-WesBanco Securities, Inc."
  }, {
    label: "A6516-Wescom Financial Services LLC",
    value: "A6516-Wescom Financial Services LLC"
  }, {
    label: "A7392-Westco Investment Corp.",
    value: "A7392-Westco Investment Corp."
  }, {
    label: "A6575-Western Equity Group, Inc.",
    value: "A6575-Western Equity Group, Inc."
  }, {
    label: "A0734-Western Financial Corporation",
    value: "A0734-Western Financial Corporation"
  }, {
    label: "A6230-Western Growers Financial Services",
    value: "A6230-Western Growers Financial Services"
  }, {
    label: "A4767-Western International Sec.,Inc",
    value: "A4767-Western International Sec.,Inc"
  }, {
    label: "A7235-Westfield Investment Group, Inc.",
    value: "A7235-Westfield Investment Group, Inc."
  }, {
    label: "A3527-Westhoff, Cone & Holmstedt",
    value: "A3527-Westhoff, Cone & Holmstedt"
  }, {
    label: "A2960-Westminster Finl. Secs., Inc.",
    value: "A2960-Westminster Finl. Secs., Inc."
  }, {
    label: "A6821-Westminster Investment Group, Inc.",
    value: "A6821-Westminster Investment Group, Inc."
  }, {
    label: "A6229-Weston Securities Corporation",
    value: "A6229-Weston Securities Corporation"
  }, {
    label: "A6144-WestPark Capital, Inc.",
    value: "A6144-WestPark Capital, Inc."
  }, {
    label: "A4989-Westport Capital Markets, LLC",
    value: "A4989-Westport Capital Markets, LLC"
  }, {
    label: "A3412-Westport Resources Invest.Serv",
    value: "A3412-Westport Resources Invest.Serv"
  }, {
    label: "A6128-WFS, LLC",
    value: "A6128-WFS, LLC"
  }, {
    label: "A3590-WH Mell Associates",
    value: "A3590-WH Mell Associates"
  }, {
    label: "A5211-WH Reaves & Co., Inc.",
    value: "A5211-WH Reaves & Co., Inc."
  }, {
    label: "A7421-Wharton MidMarket Securities Inc.",
    value: "A7421-Wharton MidMarket Securities Inc."
  }, {
    label: "A6996-Wheelhouse Securities Corporation",
    value: "A6996-Wheelhouse Securities Corporation"
  }, {
    label: "A6922-Whitaker Securities LLC",
    value: "A6922-Whitaker Securities LLC"
  }, {
    label: "A5676-White Mountain Capital, LLC",
    value: "A5676-White Mountain Capital, LLC"
  }, {
    label: "A4069-Whitehall-Parker Securities, Inc.",
    value: "A4069-Whitehall-Parker Securities, Inc."
  }, {
    label: "A2395-WIC Corp.",
    value: "A2395-WIC Corp."
  }, {
    label: "A0747-Wiley Bros-Aintree Capital LLC",
    value: "A0747-Wiley Bros-Aintree Capital LLC"
  }, {
    label: "A0147-William Blair & Company, L.L.C.",
    value: "A0147-William Blair & Company, L.L.C."
  }, {
    label: "A6285-William C. Burnside & Company, Inc.",
    value: "A6285-William C. Burnside & Company, Inc."
  }, {
    label: "A6874-Wilmington Capital Securities, LLC",
    value: "A6874-Wilmington Capital Securities, LLC"
  }, {
    label: "A2197-Wilson Parker Connally Stephenson Inc",
    value: "A2197-Wilson Parker Connally Stephenson Inc"
  }, {
    label: "A3338-Wilson-Davis & Co., Inc.",
    value: "A3338-Wilson-Davis & Co., Inc."
  }, {
    label: "A2507-Windmill Group, Inc.(The)",
    value: "A2507-Windmill Group, Inc.(The)"
  }, {
    label: "A4378-Windsor Street Capital LP",
    value: "A4378-Windsor Street Capital LP"
  }, {
    label: "A4080-Winslow, Evans & Crocker, Inc.",
    value: "A4080-Winslow, Evans & Crocker, Inc."
  }, {
    label: "A2605-Wisconsin Discount Securities Corporation",
    value: "A2605-Wisconsin Discount Securities Corporation"
  }, {
    label: "A7324-WNJ Capital Inc",
    value: "A7324-WNJ Capital Inc"
  }, {
    label: "A7404-Wolverine Trading, LLC",
    value: "A7404-Wolverine Trading, LLC"
  }, {
    label: "A4193-Woodbury Financial Svcs., Inc.",
    value: "A4193-Woodbury Financial Svcs., Inc."
  }, {
    label: "A3311-Woodlands Securities Corporation",
    value: "A3311-Woodlands Securities Corporation"
  }, {
    label: "A6071-Woodmen Financial Services, Inc.",
    value: "A6071-Woodmen Financial Services, Inc."
  }, {
    label: "A4853-Woodstock Financial Group, Inc.",
    value: "A4853-Woodstock Financial Group, Inc."
  }, {
    label: "A7335-Worden Capital Management LLC",
    value: "A7335-Worden Capital Management LLC"
  }, {
    label: "A5796-World Capital Brokerage, Inc.",
    value: "A5796-World Capital Brokerage, Inc."
  }, {
    label: "A5405-World Choice Securities, Inc.",
    value: "A5405-World Choice Securities, Inc."
  }, {
    label: "A5441-World Equity Group, Inc.",
    value: "A5441-World Equity Group, Inc."
  }, {
    label: "A0932-World First Financial Services, Inc.",
    value: "A0932-World First Financial Services, Inc."
  }, {
    label: "A6360-World Trend Financial Planning Services, Ltd.",
    value: "A6360-World Trend Financial Planning Services, Ltd."
  }, {
    label: "A6536-Worth Financial Group, Inc.",
    value: "A6536-Worth Financial Group, Inc."
  }, {
    label: "A0763-Wulff, Hansen & Company",
    value: "A0763-Wulff, Hansen & Company"
  }, {
    label: "A0877-Wunderlich Securities, Inc.",
    value: "A0877-Wunderlich Securities, Inc."
  }, {
    label: "A6057-WWK Investments, Inc.",
    value: "A6057-WWK Investments, Inc."
  }, {
    label: "A1537-York Securities Inc.",
    value: "A1537-York Securities Inc."
  }, {
    label: "A7434-YR Securities LLC",
    value: "A7434-YR Securities LLC"
  }, {
    label: "B0300-ZB, N.AMSD",
    value: "B0300-ZB, N.AMSD"
  }, {
    label: "A7464-Zeus Securities, Inc.",
    value: "A7464-Zeus Securities, Inc."
  }, {
    label: "A3962-Zions Direct, Inc.",
    value: "A3962-Zions Direct, Inc."
  }, {
    label: "A0777-Ziv Investment Company",
    value: "A0777-Ziv Investment Company"
  }
]
module.exports.LKUPUNDERWRITERROLE = [
  {
    label: "Sole Manager ",
    value: "Sole Manager "
  }, {
    label: "Senior Manager ",
    value: "Senior Manager "
  }, {
    label: "Joint Senior Mgr BookRunning ",
    value: "Joint Senior Mgr BookRunning "
  }, {
    label: "Joint Senior Mgr Non BookRunning ",
    value: "Joint Senior Mgr Non BookRunning "
  }, {
    label: "Co-Senior Mgr",
    value: "Co-Senior Mgr"
  }, {
    label: "Co-Manager              ",
    value: "Co-Manager              "
  }, {
    label: "Selling group",
    value: "Selling group"
  }, {
    label: "Placement agent",
    value: "Placement agent"
  }, {
    label: "Remarketing Takeover",
    value: "Remarketing Takeover"
  }, {
    label: "SWAP Counterparty",
    value: "SWAP Counterparty"
  }, {
    label: "Remarket AgentDealer",
    value: "Remarket AgentDealer"
  }
]
module.exports.LKUPUSEOFPROCEED = [
  {
    label: "Other",
    value: "Other"
  }, {
    label: "New Money",
    value: "New Money"
  }, {
    label: "Refund",
    value: "Refund"
  }, {
    label: "RefundNew Money",
    value: "RefundNew Money"
  }
]
module.exports.LKUPUWCOMMITTMENTDOCS = [
  {
    label: "Bond Purchase Contract",
    value: "Bond Purchase Contract"
  }, {
    label: "Agreement Among Underwriters -NA if Sole",
    value: "Agreement Among Underwriters -NA if Sole"
  }, {
    label: "Underwriters Counsel Fee Paid",
    value: "Underwriters Counsel Fee Paid"
  }
]
module.exports.LKUPUWCOUNSELDOCS = [
  {
    label: "Documentation associated with the selection of underwriters counsel, if applicab" +
        "le.",
    value: "Documentation associated with the selection of underwriters counsel, if applicab" +
        "le."
  }, {
    label: "Documentation that underwriters counsel was vetted through bank legal department" +
        ".",
    value: "Documentation that underwriters counsel was vetted through bank legal department" +
        "."
  }, {
    label: "Documentation of disclosure in OS, that Underwriters Counsel was retained option" +
        "al",
    value: "Documentation of disclosure in OS, that Underwriters Counsel was retained option" +
        "al"
  }
]
module.exports.LKUPUWREVENUEDOCS = [
  {
    label: "Profit Statement",
    value: "Profit Statement"
  }, {
    label: "Allocation letter",
    value: "Allocation letter"
  }, {
    label: "Designation letter",
    value: "Designation letter"
  }
]
module.exports.LKUPYESNO = [
  {
    label: "Yes",
    value: "Yes"
  }, {
    label: "No",
    value: "No"
  }, {
    label: "NA",
    value: "NA"
  }, {
    label: "Other",
    value: "Other"
  }
]
module.exports.LKUPSCHEDULEOFEVENTS = [
  {
    label: "100 Determine Par Amount, Issue Type and Sale Type",
    value: "100 Determine Par Amount, Issue Type and Sale Type"
  }, {
    label: "200 Determine Sale Date",
    value: "200 Determine Sale Date"
  }, {
    label: "300 Determine Closing Date",
    value: "300 Determine Closing Date"
  }, {
    label: "400 Distribution List",
    value: "400 Distribution List"
  }, {
    label: "500 Request unavailable information from the client",
    value: "500 Request unavailable information from the client"
  }, {
    label: "600 Distribute first draft of Preliminary Official Statement POS for comments",
    value: "600 Distribute first draft of Preliminary Official Statement POS for comments"
  }, {
    label: "700 Deadline to provide comments on the first draft of the POS",
    value: "700 Deadline to provide comments on the first draft of the POS"
  }, {
    label: "800 Distribute updated draft POS",
    value: "800 Distribute updated draft POS"
  }, {
    label: "900 Distribute updated draft POS",
    value: "900 Distribute updated draft POS"
  }, {
    label: "1000 Distribute updated draft POS (if requesting insurance quotes)",
    value: "1000 Distribute updated draft POS (if requesting insurance quotes)"
  }, {
    label: "1100 RFQ from paying agent for services",
    value: "1100 RFQ from paying agent for services"
  }, {
    label: "1200 RFQ from insurance companies for services",
    value: "1200 RFQ from insurance companies for services"
  }, {
    label: "1300 Rating Conference Call",
    value: "1300 Rating Conference Call"
  }, {
    label: "1400 Underwriter Due Diligence Call",
    value: "1400 Underwriter Due Diligence Call"
  }, {
    label: "1500 Dealine for second round of comments on POS",
    value: "1500 Dealine for second round of comments on POS"
  }, {
    label: "1600 Client receives rating",
    value: "1600 Client receives rating"
  }, {
    label: "1700 Post POS to electronic disclosure system",
    value: "1700 Post POS to electronic disclosure system"
  }, {
    label: "1800 Confirm fees from all parties to firm up cost of issuance ",
    value: "1800 Confirm fees from all parties to firm up cost of issuance "
  }, {
    label: "1900 Pricing with Underwriter",
    value: "1900 Pricing with Underwriter"
  }, {
    label: "2000 Bond Purchase Agreement BPA executed",
    value: "2000 Bond Purchase Agreement BPA executed"
  }, {
    label: "2100 Request invoices from all parties (as if applicale)",
    value: "2100 Request invoices from all parties (as if applicale)"
  }, {
    label: "2200 Final Official Statement FOS distributed",
    value: "2200 Final Official Statement FOS distributed"
  }, {
    label: "2300 Closing Memo",
    value: "2300 Closing Memo"
  }, {
    label: "2400 Attorney General approves sale",
    value: "2400 Attorney General approves sale"
  }, {
    label: "2500 Request confirmation that the deal or issue is closed",
    value: "2500 Request confirmation that the deal or issue is closed"
  }, {
    label: "2600 Send back Good Faith Check",
    value: "2600 Send back Good Faith Check"
  }, {
    label: "2700 Closing and delivery of funds to Escrow Agent",
    value: "2700 Closing and delivery of funds to Escrow Agent"
  }, {
    label: "2800 Final interest and principal payment for refunding bonds",
    value: "2800 Final interest and principal payment for refunding bonds"
  }, {
    label: "2900 Redemption of refunded bonds",
    value: "2900 Redemption of refunded bonds"
  }, {
    label: "3000 Receive transcript from Bond Counsel",
    value: "3000 Receive transcript from Bond Counsel"
  }, {
    label: "3100 Finalize digital deal file",
    value: "3100 Finalize digital deal file"
  }, {
    label: "3200 Notice of Sale NOS Request bid from purchasers for Competitive Sale",
    value: "3200 Notice of Sale NOS Request bid from purchasers for Competitive Sale"
  }, {
    label: "3300 Bid Form Request bid from purchasers for Competitive Sale",
    value: "3300 Bid Form Request bid from purchasers for Competitive Sale"
  }, {
    label: "3400 Order CUSIPs for Competitive Sale",
    value: "3400 Order CUSIPs for Competitive Sale"
  }, {
    label: "3500 Instruct CUSIP to reinvoice to purchaser for Competitive Sale",
    value: "3500 Instruct CUSIP to reinvoice to purchaser for Competitive Sale"
  }, {
    label: "3600 Term Sheet",
    value: "3600 Term Sheet"
  }, {
    label: "3700 Record disclosure items",
    value: "3700 Record disclosure items"
  }
]
