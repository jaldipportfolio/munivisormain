const Joi = require("joi")
const dateFormat = require("dateformat")

const emailRegex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i


const userEmailsSChema = Joi.object().keys({
  _id: Joi.string().allow("").optional(),
  emailId: Joi.string().email().required().label("Email Id").optional(),
  emailPrimary:Joi.boolean().required().optional()
})

const userPhoneSchema = Joi.object().keys({
  _id: Joi.string().allow("").optional(),
  phoneNumber:  Joi.string().allow(["",null]).label("Phone Number").optional(),
  extension:  Joi.number().allow(["",null]).label("Extension").optional(),
  phonePrimary: Joi.boolean().allow(["",null]).optional()
})

const userFaxSchema = Joi.object().keys({
  _id: Joi.string().allow("").optional(),
  faxNumber: Joi.string().allow("").label("Fax Number").optional(),
  faxPrimary: Joi.boolean().allow(["",null]).optional()
})

const entityAddressSchema = Joi.object().keys({
  _id: Joi.string().allow("").optional(),
  addressName: Joi.string().allow("").label("Address Name").optional(),
  addressType: Joi.string().allow("").label("AddressType").optional(),
  isPrimary:  Joi.boolean().required().optional(), // Office address, headquarter or residence
  isActive: Joi.boolean().required().optional(),
  addressLine1: Joi.string().required().label("Address Line 1").optional(),
  addressLine2: Joi.string().allow("").label("Address Line 2").optional(),
  country: Joi.string().required().label("Country").optional(),
  state: Joi.string().required().label("State").optional(),
  city: Joi.string().required().label("City").optional(),
  zipCode: {
    zip1:Joi.string().min(5).required().label("Zip Code 1").optional(),
    zip2:Joi.string().min(4).allow("").label("Zip Code 2").optional()
  },
  formatted_address:Joi.string().allow("").label("Formatted Address").optional(),
  url:Joi.string().allow("").label("URL").optional(),
  location:{
    _id: Joi.string().allow("").optional(),
    longitude: Joi.string().allow("").label("Longitude").optional(),
    latitude: Joi.string().allow("").label("Latitude").optional()    
  } 
})

const userAddOnsSchema = Joi.object().keys({
  _id: Joi.string().allow("").optional(),
  serviceType: Joi.string().required().label("Service Type").optional(),
  serviceEnabled: Joi.boolean().required().optional()
})

const userFlagsSchema = Joi.object().keys({
  _id: Joi.string().allow("").optional(),
  lebel: Joi.string().required().label("Service Type").optional(),
  status: Joi.boolean().required().optional()
})

const userJoiCredentialsSchema = Joi.object().keys({
  userEmailId:Joi.string().allow("").optional(), // We can get rid of this if we are able to just use the email ID.
  password:Joi.string().allow(["",null]).optional(),  // This is the salted password.
  onboardingStatus:Joi.string().allow(["",null]).optional(),
  userEmailConfirmString:Joi.string().allow(["",null]).optional(),
  userEmailConfirmExpiry:Joi.string().allow(["",null]).optional(),
  passwordConfirmString:Joi.string().allow(["",null]).optional(),
  passwordConfirmExpiry:Joi.string().allow(["",null]).optional(),
  isUserSTPEligible:Joi.boolean().allow([null]).optional(),
  authSTPToken:Joi.string().allow(["",null]).optional(),
  authSTPPassword:Joi.string().allow(["",null]).optional(),
  passwordResetIteration:Joi.string().allow(["",null]).optional(),
  passwordResetStatus:Joi.string().allow(["",null]).optional(),
  passwordResetDate:Joi.string().allow(["",null]).optional()
})

// eslint-disable-next-line no-unused-vars
const userDetailSchema = Joi.object().keys({
  _id: Joi.string().allow("").optional(),
  entityId:Joi.string().required().label("Firm Id").optional(),
  userFlags: Joi.array().items(Joi.string().required().optional()).min(1).unique().required().label("User Flag").optional(),// [series50,msrbcontact, emmacontact, supprinsipal, compofficer, mfp, primarycontact, procurementcont, fincontact, distmember, electedofficial, invstcontact, attorney, cpa ]
  userRole: Joi.string().required().label("User Role").optional(), // [ Admin, ReadOnly, Backup ]
  userEntitlement: Joi.string().required().label("User Entitlement").optional(), // [ Global, Transaction ]
  userFirmName:Joi.string().required().label("Firm Name").optional(),
  userFirstName: Joi.string().required().label("First Name").optional(),
  userMiddleName: Joi.string().allow("").label("Middle Name").optional(),
  userLastName: Joi.string().required().label("Last Name").optional(),
  userEmails: Joi.array().items(userEmailsSChema).optional(),
  userPhone: Joi.array().items(userPhoneSchema).optional(),
  // userLoginCredentials:userJoiCredentialsSchema,
  userFax: Joi.array().items(userFaxSchema).optional(),
  userEmployeeID: Joi.string().allow([null,""]).label("Employee Id").optional(),
  userEmployeeType: Joi.string().allow("").label("Employee Type").optional(),
  userDepartment: Joi.string().allow("").label("Department").optional(),
  userJobTitle: Joi.string().allow("").label("Job Title").optional(),
  userManagerEmail: Joi.string().email().allow("").label("Manager Email").optional(),
  userJoiningDate: Joi.date().allow([null,""]).example(new Date("2005-01-01")).allow("", null).label("Joining Date").optional(),
  userExitDate: Joi.date().example(new Date("2016-01-01")).min(Joi.ref("userJoiningDate")).allow("", null).label("Exit Date").optional(),
  userCostCenter: Joi.string().allow("").label("Cost Center").optional(),
  userAddresses: Joi.array().items(entityAddressSchema).required().optional(),
  userAddOns: Joi.array().items(userAddOnsSchema).required().optional(),
  userAddDate: Joi.date().min(dateFormat(new Date(), "yyyy-mm-dd")).required().label("User Add Date").optional(),
  userUpdateDate: Joi.date().min(dateFormat(new Date(), "yyyy-mm-dd")).required().label("User Update").optional(),
  __v: Joi.number().integer().optional()
})

export const validateUserDetail = (inputUserDetail) => Joi.validate(inputUserDetail, userDetailSchema, { abortEarly: false, stripUnknown: false })
