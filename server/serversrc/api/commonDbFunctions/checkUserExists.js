import {
  EntityUser
} from "./../appresources/entityUser/entityUser.model"


export const checkUserExists = async (email) => {
  const regexemail = new RegExp(email,"i")
  console.log("THE REGEX EMAIL IS AS FOLLOWS",regexemail)

  let returnObject
  try{
    returnObject = await EntityUser.aggregate([{
      $match: {
        "userLoginCredentials.userEmailId": {$regex:regexemail,$options:"i"}
      }
    },
    { $addFields: { "foundemail":true,"error":"" } },
    {
      $project: {
        "_id":0,
        "foundemail":1,
        "error":1
      }
    },
    ])
  }
  catch(err) {
    console.log(err)
    returnObject = [{foundemail:false, error:"CHECKUSER EXISTS:There is an error in finding whether the email exists"}]
  }
  console.log("RETURNED DATA FROM CHECK USER EXISTS", returnObject)
  return returnObject.length === 0 ?  {} : returnObject[0]
}