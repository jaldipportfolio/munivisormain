import {
  EntityUser
} from "./../appresources/entityUser/entityUser.model"

export const getAllUsersForFirm = async (entityId) => {
  const users = await EntityUser.find({ entityId })
  return users
}
