import {
  EntityUser, EntityRel
} from "./../appresources/models"

export const findUsersForSignUpByType = async(msrbType) => {
  let matchParam = null
  let returnObject

  console.log("1. ENTERED FUNCTION", msrbType)

  if( msrbType.includes("Municipal Advisor") )
  {
    matchParam = {"relationshipType":"Self"}
  }
  else if (msrbType.includes("Issuer")) {
    matchParam = {"relationshipType":{$in:["Client","Prospect"]}}
  }
  else {
    matchParam = {"relationshipType":"Third Party"}
  }

  console.log("2. CONSTRUCTED QUERY", matchParam)

  try{
    returnObject = await EntityRel.aggregate([
      {
        $match:{...matchParam}
      },
      {
        $lookup:{
          from: "entityusers",
          localField: "entityParty2",
          foreignField: "entityId",
          as: "users"
        }
      },
      {
        $unwind:"$users"
      },
      {
        $match: {
          "users.userLoginCredentials.onboardingStatus": { $in: ["created", "validate"] }
        }
      },
      {
        $project: {
          "userId":"$_id",
          "entityId":"$users.entityId",
          "userFirstName":"$users.userFirstName",
          "userLastName":"$users.userLastName",
          "userJobTitle":"$users.userJobTitle",
          "loginEmailId":"$users.userLoginCredentials.userEmailId"
        }
      },
      {
        $lookup: {
          "from": "entities",
          "localField": "entityId",
          "foreignField": "_id",
          "as": "entityInfo"
        }
      },
      { $unwind : "$entityInfo"},
      {
        $project: {
          "userId":1,
          "entityId":1,
          "userFirstName":1,
          "userLastName":1,
          "loginEmailId":1,
          "userJobTitle":1,
          "entityInfo.msrbName":"$entityInfo.msrbFirmName",
          "entityInfo.msrbType":"$entityInfo.msrbRegistrantType",
          "entityInfo.firmName":"$entityInfo.firmName",
          "entityInfo.munivisorClient":"$entityInfo.isMuniVisorClient"
        }

      },
    ])
    console.log("3. SIGNUP USERS",returnObject)
  }
  catch(err) {
    console.log("There is an error",err)
  }
  return returnObject
}

export const findUsersForSignUpByType_Old = async (msrbType) => {
  let queryParam = null
  let returnObject

  console.log("1. ENTERED FUNCTION", msrbType)

  if( msrbType.includes("Municipal Advisor") || msrbType.includes("Issuer") )
  {
    queryParam = {"$in": [...msrbType]}
  }
  else {
    queryParam = {"$nin": ["Issuer","Municipal Advisor"]}
  }
  console.log("2. CONSTRUCTED QUERY", queryParam)

  try{
    returnObject = await EntityUser.aggregate(
      [{
        $match: {
          "userLoginCredentials.onboardingStatus": "created"
        }
      },
      {
        $project: {
          "userId":"$_id",
          "entityId":1,
          "userFirstName":1,
          "userLastName":1,
          "userJobTitle":1,
          "loginEmailId":"$userLoginCredentials.userEmailId"
        }
      },
      {
        $lookup: {
          "from": "entities",
          "localField": "entityId",
          "foreignField": "_id",
          "as": "entityInfo"
        }
      },
      { $unwind : "$entityInfo"},
      {
        $project: {
          "userId":1,
          "entityId":1,
          "userFirstName":1,
          "userLastName":1,
          "loginEmailId":1,
          "userJobTitle":1,
          "entityInfo.msrbName":"$entityInfo.msrbFirmName",
          "entityInfo.msrbType":"$entityInfo.msrbRegistrantType",
          "entityInfo.firmName":"$entityInfo.firmName",
          "entityInfo.munivisorClient":"$entityInfo.isMuniVisorClient"
        }

      },
      {
        $match: {
          "entityInfo.msrbType": { ...queryParam }
        }
      },

      ])
    console.log("3. SIGNUP USERS",returnObject)
  }
  catch(err) {
    console.log("There is an error",err)
  }

  return returnObject
}
