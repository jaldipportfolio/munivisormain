import express from "express"
import fs from "fs"
import path from "path"
import proxy from "http-proxy-middleware"
import cors from "cors"
import setupMiddware from "./middleware"
import { restRouter, apiErrorHandler } from "./api"
import { authorizationRouter } from "./authorization"
import { connect } from "./db"
import logger from "./api/modules/logger"
import docAuthMiddleware from "./middlewares/docInfoAuth"
import { getDetailsForDocParsr } from "./api/elasticsearch/elasticsearch.controller"
import searchMiddleware from "./middlewares/searchMiddleware"

console.log("THIS IS THE ENVIRONMENT", process.env.NODE_ENV)
// Declare an app from express
const app = express()
app.use(cors())

setupMiddware(app)

require("dotenv").config()

/* if (process.env.NODE_ENV === "production") {
  app.get("*.js", (req, res, next) => {
    req.url += ".gz"
    res.set("Content-Encoding", "gzip")
    res.set("Content-Type", "text/javascript")
    next()
  })
  app.get("*.css", (req, res, next) => {
    req.url += ".gz"
    res.set("Content-Encoding", "gzip")
    res.set("Content-Type", "text/css")
    next()
  })
} */

// Testing the Loggers

logger.debug("Debugging info")
logger.verbose("Verbose info")
logger.info("Hello world")
logger.warn("Warning message")
logger.error("Error info")

connect()
const env = process.env.NODE_ENV || "development"

app.post("/doc-meta-info", [docAuthMiddleware], async (req, res) => {
  try {
    const { docIds } = req.body
    if (!docIds || !docIds.length) {
      res
        .status(500)
        .send({
          success: false,
          error: "Please add doc ids"
        })
        .end()
    } else {
      const normalizedDataForDocParser = await getDetailsForDocParsr(docIds)
      res.send({
        success: true,
        data: normalizedDataForDocParser
      })
    }
  } catch (error) {
    res
      .status(500)
      .send({
        success: false,
        error
      })
      .end()
  }
})

app.use("/auth", (req, res, next) => {
  // res.header("Access-Control-Allow-Origin", process.env.FRONTEND_URL ) // Access-Control-Allow-Origin
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS")
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Authorization, Accept, Access-Control-Al" +
      "low-Methods"
  )
  res.header("X-Frame-Options", "deny")
  res.header("X-Content-Type-Options", "nosniff")

  next()
})
app.use("/auth", authorizationRouter)

app.use("/api", (req, res, next) => {
  // res.header("Access-Control-Allow-Origin", process.env.FRONTEND_URL)
  res.header("Access-Control-Allow-Origin", "*")
  res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS")
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Authorization, Accept, Access-Control-Al" +
      "low-Methods"
  )
  res.header("X-Frame-Options", "deny")
  res.header("X-Content-Type-Options", "nosniff")
  next()
})
app.use("/api", restRouter)

const searchProxyOptions = {
  target: process.env.ES_HOST,
  changeOrigin: true,
  logLevel: "debug",
  xfwd: false,
  preserveHeaderKeyCase: true,
  pathRewrite: {
    "/search": ""
  },
  onProxyReq: (proxyReq, req) => {
    /* transform the req body back from text */
    const { body } = req
    if (body) {
      if (typeof body === "object") {
        proxyReq.write(JSON.stringify(body))
      } else {
        proxyReq.write(body)
      }
    }
  }
}

/* Parse the ndjson as text */
app.use("/search", [searchMiddleware], proxy(searchProxyOptions))

app.use(apiErrorHandler)

const appRoot = fs.realpathSync(process.cwd())
if (env === "production") {
  console.log("The root of the application is", appRoot)
  const clientPath = path.resolve(appRoot, "build/client")
  const indexPath = path.join(clientPath, "index.html")
  console.log(clientPath)
  console.log(indexPath)

  app.use(express.static(clientPath))
  app.get("/*", (req, res) => {
    res.sendFile(indexPath)
  })
}

export default app
