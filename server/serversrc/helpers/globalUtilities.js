import isEmpty from "lodash/isEmpty"

export const fnIsNull = (obj, keys) => keys.reduce ( (ac,k) => ac && (!obj[k] || obj[k] === "" || isEmpty(obj[k])), true)
export const fnIsNotNull = (obj, keys) => keys.reduce ( (ac,k) => ac && (obj[k] || obj[k] !== "" || !isEmpty(obj[k])), true)
