import AWS from "aws-sdk"
import fileSize from "file-size"
import { HistoricalDocs } from "../../serversrc/api/appresources/models"

const contextType = "HISTORICALDOCS"
require("dotenv").config()

export const s3 = new AWS.S3({
  region : process.env.SESREGION,
  bucket: process.env.S3BUCKET,
  accessKeyId: process.env.S3ACCESSKEYID,
  secretAccessKey: process.env.S3SECRETKEY
})

const getAttachment = (fileName) => new Promise(async (resolve, reject) => {
  const params = {
    Bucket: process.env.S3BUCKET,
    Key: fileName
  }
  s3.getObject(params, (err, data) => {
    if (err) reject(err)
    else resolve({
      ...data,
      Name: fileName
    })
  })
})

let allKeys = []

const getAWSFile = async (dir, token, callback) => {
  const params = {
    Bucket: process.env.S3BUCKET,
    Prefix: `${dir}`
  }
  if(token) params.ContinuationToken = token
  const data = await s3.listObjectsV2(params).promise()
  allKeys = allKeys.concat(data.Contents)
  if(data.IsTruncated) {
    await getAWSFile(dir, data.NextContinuationToken, callback)
  }
  return allKeys
}

const listAllMeta = (path) => new Promise(async (res) => {
  const allDocs = await getAWSFile(path)
  res(allDocs)
})

const getAWSFiles = async () => {
  const args = process.argv.slice(2)
  const tenantId = (args && args.length) ? args[0] : ""
  const path = (args && args.length) ? args[1] : ""
  const insertIntoDocs = []
  const promises = []
  const allAWSFiles = []
  if (!tenantId || !path) {
    return console.log("===============Plaease provide tenantId & path================>")
  }
  const docInfo = await listAllMeta(path) || []
  console.log("===========Files meta successfully get from AWS S3===========>")
  // const docInfo = (allDocs && Object.keys(allDocs).length) ? (allDocs.Contents) : []

  Promise.all(docInfo).then(async () => {
    if (docInfo && docInfo.length) {
      for(let z=0; z < docInfo.length; z++){
        const fileInfo = await getAttachment(docInfo[z].Key)
        allAWSFiles.push(fileInfo)
        console.log("=====get File======>", fileInfo.Name)
      }
      console.log("===========Files successfully get from AWS S3===========>")

      let allDocs = await HistoricalDocs.aggregate([
        { $match: { tenantId } },
        { $project: { versionId: { $arrayElemAt: ["$meta.versions.versionId", 0] } } }
      ])
      allDocs = (allDocs && allDocs.length) ? allDocs.map(e => e.versionId) : []

      allAWSFiles.forEach(d => {
        if ((allDocs.indexOf(d.VersionId) === -1)) {
          promises.push(d)
        }
      })

      for(let z=0; z < promises.length; z++){
        console.log("===========Version Id===========>", promises[z].VersionId)
        const parts = promises[z].Name.split("/")
        const name = parts && parts[parts.length - 1]
        const docDetails = {
          tenantId,
          name,
          "originalName" : name,
          meta: {
            path: promises[z].Name,
            versions: {
              "versionId" : promises[z].VersionId,
              "name" : name,
              "originalName" : name,
              "size": fileSize(promises[z].ContentLength).human("jedec"),
              "uploadDate" : new Date(),
              "uploadedByUserTenantId" : tenantId
            }
          },
          contextType,
          createdAt: new Date(),
          updatedAt: new Date()
        }
        insertIntoDocs.push(docDetails)
      }
      const result = await HistoricalDocs.insertMany(insertIntoDocs)
      console.log("===========Files successfully insert into Historical Collection===========>")
      console.log(JSON.stringify({ files: result && result.map(d => d && d.name) || []}))
      console.log("success")
    }
  }).catch((error) => {
    console.log(error)
  })
}

getAWSFiles()

// COMMAND RUN :- npm run getFile [TENANT_ID] [S3_PATH]
