import fs from "fs"
import AWS from "aws-sdk"
import fileSize from "file-size"

import { HistoricalDocs } from "../../serversrc/api/appresources/models"
export const s3 = new AWS.S3({
  region: process.env.SESREGION,
  bucket: process.env.S3BUCKET,
  accessKeyId: process.env.S3ACCESSKEYID,
  secretAccessKey: process.env.S3SECRETKEY
})

/* export const s3 = new AWS.S3({
  region: process.env.SESREGION,
  bucket: "munivisor-docs-test1",
  accessKeyId: "AKIAJZCLWSEOGEVPFPIQ",
  secretAccessKey: "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"
}) */
const contextType = "HISTORICALDOCS"

const getAttachment = (fileName) => {
  const params ={
    Bucket: process.env.S3BUCKET,
    Key: fileName
  }
  return s3.getObject(params).promise()
}

const insertSyncFilesIntoDocs = async (data) => {
  const insertIntoDocs = data.filter(d => d.isUpload) || []
  const failedDocs = data.filter(d => !d.isUpload) || []

  const promises = []
  for(let z=0; z<failedDocs.length; z++){
    promises.push(getAttachment(failedDocs[z].meta.path))
  }
  Promise.all(promises).then(async (data) => {
    if (promises && promises.length) {
      for(let z=0; z < failedDocs.length; z++){
        if (data[z]) {
          insertIntoDocs.push({
            ...failedDocs[z],
            isUpload: true,
            meta: {
              ...failedDocs[z].meta,
              versions: {
                "versionId" : data[z].VersionId,
                "name" : failedDocs[z].name,
                "originalName" : failedDocs[z].name,
                "size": fileSize(failedDocs[z].size).human("jedec"),
                "uploadDate" : new Date(),
                "uploadedByUserTenantId" : failedDocs[z].tenantId,
              }
            }
          })
        }
      }
    }

    const result = await HistoricalDocs.insertMany(insertIntoDocs)
    console.log("===========Files successfully upload on Docs Collection===========>")
    console.log(JSON.stringify({fail: failedDocs,
      // successInS3: insertIntoDocs && insertIntoDocs.map(d => d && d.name) || [],
      successInDocs: result && result.map(d => d && d.name) || []}))
    console.log("success")
    return result
  }).catch((error) => {
    console.log(error)
  })
}

const getFileNamesFromPath = (dir) => fs.readdirSync(dir)

const getFile = async (dir) => new Promise(async (resolve, reject) => {
  fs.readFile(dir, (err, data) => {
    if (err) reject(err)
    else resolve(data)
  })
})

const getFileSize = async (dir) => new Promise(async (resolve) => {
  const stats = fs.statSync(dir)
  resolve(stats.size)
})

export const syncFiles = async () => {
  // const dir = "D:/files"
  const fileList = []
  const docList = []

  const args = process.argv.slice(2)
  const tenantId = (args && args.length) ? args[0] : ""
  const dir = (args && args.length) ? args[1] : ""
  const isSlash = dir && dir.slice(-1)
  if (!tenantId || !dir) {
    return console.log("===============Plaease provide tenantId & path================>")
  }

  const files = getFileNamesFromPath(dir)

  // console.log("===============files================>", files)
  // const dir = "C:/Users/Admin/Downloads/6582.png"
  // const data = await getFile(dir)

  for(const i in files) {
    const file = files[i]
    if (fs.statSync(`${dir}/${file}`).isDirectory()) {
      const secondLevel = getFileNamesFromPath(`${dir}/${file}`)

      for(const j in secondLevel) {
        const file1 = secondLevel[j]
        if (fs.statSync(`${dir}/${file}/${file1}`).isDirectory()) {
          const thirdLevel = getFileNamesFromPath(`${dir}/${file}/${file1}`)

          for(const k in thirdLevel) {
            const file2 = thirdLevel[k]
            if (fs.statSync(`${dir}/${file}/${file1}/${file2}`).isDirectory()) {
              const fourthLevel = getFileNamesFromPath(`${dir}/${file}/${file1}/${file2}`)

              for(const l in fourthLevel) {
                const file3 = fourthLevel[l]
                if (fs.statSync(`${dir}/${file}/${file1}/${file2}/${file3}`).isDirectory()) {
                  const fifthLevel = getFileNamesFromPath(`${dir}/${file}/${file1}/${file2}/${file3}`)

                  for(const m in fifthLevel) {
                    const file4 = fifthLevel[m]
                    if (fs.statSync(`${dir}/${file}/${file1}/${file2}/${file3}/${file4}`).isDirectory()) {
                      console.log("=============beyond set range============>")
                    }
                    else {
                      try {
                        const data = await getFile(`${dir}/${file}/${file1}/${file2}/${file3}/${file4}`)
                        const fileSize = await getFileSize(`${dir}/${file}/${file1}/${file2}/${file3}/${file4}`)
                        // console.log("=====file=====>", data)
                        fileList.push({
                          name: file4,
                          type: "file",
                          file: data,
                          fileSize,
                          path: `${dir}/${file}/${file1}/${file2}/${file3}/${file4}`
                        })
                      } catch (e) {
                        console.log(e)
                      }
                    }
                  }
                }
                else {
                  try {
                    const data = await getFile(`${dir}/${file}/${file1}/${file2}/${file3}`)
                    const fileSize = await getFileSize(`${dir}/${file}/${file1}/${file2}/${file3}`)
                    // console.log("=====file=====>", data)
                    fileList.push({
                      name: file3,
                      type: "file",
                      file: data,
                      fileSize,
                      path: `${dir}/${file}/${file1}/${file2}/${file3}`
                    })
                  } catch (e) {
                    console.log(e)
                  }
                }
              }
            }
            else {
              try {
                const data = await getFile(`${dir}/${file}/${file1}/${file2}`)
                const fileSize = await getFileSize(`${dir}/${file}/${file1}/${file2}`)
                // console.log("=====file=====>", data)
                fileList.push({
                  name: file2,
                  type: "file",
                  file: data,
                  fileSize,
                  path: `${dir}/${file}/${file1}/${file2}`
                })
              } catch (e) {
                console.log(e)
              }
            }
          }
        }
        else {
          try {
            const data = await getFile(`${dir}/${file}/${file1}`)
            const fileSize = await getFileSize(`${dir}/${file}/${file1}`)
            // console.log("=====file=====>", data)
            fileList.push({
              name: file1,
              type: "file",
              file: data,
              fileSize,
              path: `${dir}/${file}/${file1}`
            })
          } catch (e) {
            console.log(e)
          }
        }
      }
    }
    else {
      try {
        const data = await getFile(`${dir}/${file}`)
        const fileSize = await getFileSize(`${dir}/${file}`)
        // console.log("=====file=====>", data)
        fileList.push({
          name: file,
          type: "file",
          file: data,
          fileSize,
          path: `${dir}/${file}`
        })
      } catch (e) {
        console.log(e)
      }
    }
  }

  // console.log("===============fileList================>", fileList)
  console.log("===========Files successfully get from Directory===========>")
  // return fileList

  if (fileList && fileList.length) {
    for(const i in fileList) {
      const file = fileList[i]
      const subPath = file.path.replace(`${dir}${isSlash === "/" ? "" : "/"}`,"")
      const path = `${tenantId}/${contextType}/${subPath}`
      const params = {Bucket: process.env.S3BUCKET, Key: path, Body: file.file, ContentType: "text/plain"}
      setTimeout( async () => {
        await s3.upload(params, async (err, data) => {
          // console.log(err, data)
          console.log(err, data && data.VersionId)

          const parts = path.split("/")
          const name = parts && parts[parts.length - 1]
          const docDetails = {
            tenantId,
            name,
            "originalName" : name,
            meta: {
              path,
              versions: {
                "versionId" : data.VersionId,
                "name" : name,
                "originalName" : name,
                "size": fileSize(file.fileSize).human("jedec"),
                "uploadDate" : new Date(),
                "uploadedByUserTenantId" : tenantId,
              }
            },
            contextType,
            createdAt: new Date(),
            updatedAt: new Date()
          }

          docDetails.isUpload = (data && data.VersionId) ? true : !(data && data.VersionId)
          docList.push(docDetails)

          if(docList && fileList && docList.length === fileList.length) {
            console.log("===========Files successfully upload on S3===========>")
            const resultDocs = await insertSyncFilesIntoDocs(docList)
            if (resultDocs && resultDocs.length) {
              process.exit()
            }
          }
        })
      }, 500)
    }
  }
}

syncFiles()
// COMMAND RUN :- npm run syncFile [TENANT_ID] [LOCAL_FILE_PATH]
