
import { MongoClient } from "mongodb"

require("dotenv").config()


const collectionNames = [
  "actbusdev",
  "actmarfps",
  "auditlogs",
  "buscondactivities",
  "cmplsupervisor",
  "cmplsupervisors",
  "configs",
  "controls",
  "controlsactions",
  "docs",
  "entities",
  "entityrels",
  "entityusers",
  "generaladmin",
  "generaladmins",
  "giftsAndGrautities",
  "giftsandgrautities",
  "initial",
  "messages",
  "mvtasks",
  "notifications",
  "policontributiondetails",
  "politicalcontributions",
  "professionalqualifications",
  "supervisoryobligations",
  "svcomplaintdetails",
  "tenantservicebillings",
  "tranagencydeals",
  "tranagencyothers",
  "tranagencyrfps",
  "tranbankloans",
  "tranderivatives",
  "tranotherprojects",
  "users",
  "usersearchprefs"
]
console.log(process.env.MONGODB_NAME)


MongoClient.connect(process.env.DB_URL,{ useNewUrlParser: true }, (err, db) => {
  if (err) throw err
  // db pointing to newdb
  const dbase = db.db(process.env.MONGODB_NAME)// here
  console.log(`Switched to ${dbase} database`)
  // create 'users' collection in newdb database
  collectionNames.forEach ( (c, i) => {
    dbase.createCollection(c, (errCol, coll) => {
      console.log(`The DB was created ${c}`)
      coll.deleteMany({}, (e, r) => {
        if(e) {
          console.log(`The collection ${c} doesn't exist moving to next`)
        } else {
          console.log(`deleted collection${c}`)
        }
        if(i === (collectionNames.length - 1)) {
          db.close()
          process.exit(0)
        }
      })
    })
  })
  console.log("done")
})

