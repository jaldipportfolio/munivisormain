
import { MongoClient } from "mongodb"

require("dotenv").config()

const collectionNames = [
  "actbusdev",
  "actbusdevs",
  "actmarfps",
  "auditlogs",
  "buscondactivities",
  "cmplsupervisor",
  "cmplsupervisors",
  "configs",
  "controls",
  "controlsactions",
  "docfolders",
  "docs",
  "entities",
  "entityrels",
  "entityusers",
  "generaladmin",
  "generaladmins",
  "giftsAndGrautities",
  "giftsandgrautities",
  "initial",
  "messages",
  "mvtasks",
  "notifications",
  "policontributiondetails",
  "politicalcontributions",
  "professionalqualifications",
  "supervisoryobligations",
  "svcomplaintdetails",
  "tenantonboardings",
  "tenantuserentitlements",
  "tenantservicebillings",
  "tranagencydeals",
  "tranagencyothers",
  "tranagencyrfps",
  "tranbankloans",
  "tranderivatives",
  "tranotherprojects",
  "tranuserentitlements",
  "users",
  "usersearchprefs",
  "globalrefentities"
]
console.log(process.env.MONGODB_NAME)

MongoClient.connect(process.env.DB_URL,{ useNewUrlParser: true }, (err, db) => {
  if (err) throw err
  // db pointing to newdb
  const dbase = db.db(process.env.MONGODB_NAME)// here
  console.log(`Switched to ${dbase} database`)
  // create 'users' collection in newdb database
  collectionNames.forEach ( (c, i) => {
    dbase.collection(c, (errCol, coll) => {
      console.log(`Removing Collectiion ${c}`)
      coll.deleteMany({}, (e, r) => {
        if(e) {
          console.log(`The collection ${c} doesn't exist moving to next`)
        } else {
          console.log(`deleted collection${c}`)
        }
        if(i === (collectionNames.length - 1)) {
          console.log("Done with Removing all documents from collection")
          db.close()
          process.exit(0)
        }
      })
    })
  })
})
