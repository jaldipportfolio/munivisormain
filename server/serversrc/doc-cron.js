
import { putData } from "./api/elasticsearch/esHelper"
import { docParserRequest }from "./api/elasticsearch/docParser"

require("dotenv").config()

const CRON_TIME = parseInt(process.env.DOC_INDEX_INTERVAL || 5, 10) * 60 * 1000

setInterval(async () => {
  await putData("cron-keeper", "last_cron", {
    time: new Date(),
  })
  docParserRequest()
}, CRON_TIME)
