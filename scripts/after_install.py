#!/usr/bin/python

import os
import subprocess
import shlex
import shutil

SCRIPT_DIR = os.path.abspath(os.path.dirname(__file__) + '/..')
COMMAND_LIST = ["yarn configalllinux","yarn industryreferencelinux"]
env_command = "grep FRONTEND_URL .env"
#COMMAND_LIST = []
def run_command(cmd):
    """given shell command, returns communication tuple of stdout and stderr"""
    args = shlex.split(cmd)
    return subprocess.Popen(args,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE,
                            stdin=subprocess.PIPE).communicate()

# Clean-up the files first to save disk space
try:
    fullList = os.listdir(SCRIPT_DIR)

    for exclude in ['final.tar', 'appspec.yml', 'scripts', 'start_application.sh', 'stop_application.sh']:
        if exclude in fullList:
            print('Skipping {0}'.format(exclude))
            fullList.remove(exclude)

    for item in fullList:
        item_full_path = SCRIPT_DIR + '/' + item
        if os.path.isfile(item_full_path):
            print('Deleting file {0}'.format(item))
            os.remove(item_full_path)
        else: 
            print('Deleting folder {0}'.format(item))
            shutil.rmtree(item_full_path)

except Exception as e:
    print("Failed to clean-up files, which were not required.")
    print(e)

try:
    print os.getcwd()
    os.chdir('/var/www')
    print os.getcwd()
    output = run_command(env_command)
    if output[1] == '':
        if "app.munivisor.com" not in output[0]:
            for command_str in COMMAND_LIST:
                print('Running command: {0}'.format(command_str))
                run_command(command_str)
        else:
            print("Skipping initialization command executions for Production")
except Exception as e:
    print("Failed to execute post deployment commands.")
    print(e)